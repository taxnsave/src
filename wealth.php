<section id="content" class="register_box">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="aligncenter"><br />
                    <h2 class="aligncenter" style="margin-top:30px;">my<b class="b_text">Taxsave</b> </h2>
                    <p>Login/Register to exploer application</p>
                </div>

            </div>
        </div>
    </div>
</section>
<section id="content" class="welth-content">
<h1 class="headLine">Single Window To Manage Your Wealth</h1>
<div class="container-fluid">
    <div class="site-panel">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2">
                    <div class="sidebar">
                        <div class="list-group">
                            <a href="#" class="list-group-item" id="retireRich">Retire Rich</a>
                            <a href="#" class="list-group-item" id="grandWedding">Grand Wedding</a>
                            <a href="#" class="list-group-item" id="higherEdu">Higher Education</a>
                            <a href="#" class="list-group-item" id="ownHouse">Own A House</a>
                            <a href="#" class="list-group-item" id="buyCar">Buy A Car</a>
                            <a href="#" class="list-group-item" id="vacPlan">Vacation Plan</a>
                            <a href="#" class="list-group-item" id="emerFund">Emergency Fund</a>
                            <a href="#" class="list-group-item" id="uniGoal">Unique Goal</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                <div class="content-box well wealthHide retireRich" id="retireRichShow">
                    <!-- <legend>Retire rich </legend> -->
                    <div class="panel-group" id="retRichAcc">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" data-parent="#retRichAcc" href="#retRiccollapse1">Retire rich</a>
                                </h4>
                            </div>
                            <div id="retRiccollapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form-horizontal" action="/action_page.php">
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">I am</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="Ret_age" name="" placeholder="" required>
                                            </div>
                                            <label class="control-label col-sm-1" for="">Old</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">I want to retire at</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="Ret_retireAge" name="" placeholder="" required>
                                            </div>
                                            <label class="control-label col-sm-1" for="">Year</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">My life expectancy</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="Ret_expectancy" name="" placeholder="" required>
                                            </div>
                                            <label class="control-label col-sm-1" for="">Year</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Risk</label>
                                            <div class="col-sm-2">
                                                <select id="Ret_risk" name="" class="form-control">
                                                    <option value="0">Select</option>
                                                    <option value="7">Low</option>
                                                    <option value="12">Moderate</option>
                                                    <option value="15">High</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Expected rate of return</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="Ret_expRateReturn" name="" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">My current lumpsum investment</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="Ret_currentLumpsum" name="" placeholder="" required>
                                            </div>
                                            <label class="control-label col-sm-3" for="">for retirement plan</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">My expenses per month is</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="Ret_expPerMonth" name="" placeholder="" required>
                                            </div>
                                            <label class="control-label col-sm-3" for="">(i.e, after retirement)</label>
                                        </div>
                                        <div class="form-group">
                                            <a data-toggle="collapse" data-parent="#retRichAcc" href="#retRiccollapse2" class="btn btn-success pull-right" id="retirerich">Process</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#retRichAcc" href="#retRiccollapse2" >Summary</a>
        </h4>
                            </div>
                            <div id="retRiccollapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <!-- <legend>Summary</legend> -->
                                        <div class="col-md-5">
                                            <form class="form-horizontal" action="/action_page.php">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">My current age</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="Scurrent_age" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">My retirement age</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="Sretirement_age" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">My life expectancy</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="Slife_expectancy" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">My current lumpsum investment</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="Scurrent_lumpsum" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Monthly contribution to be done</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="SMonthly_contribution" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Risk currently undertaken</label>
                                                    <div class="col-sm-3">
                                                        <select name="" id="SRisk_curr_undertaken" class="form-control">
                                                            <option value="7">Low</option>
                                                            <option value="12">Moderate</option>
                                                            <option value="15">High</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Rate of return</label>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="form-control" id="SRate_return" name="" placeholder="" required>
                                                    </div>
                                                    <label class="control-label col-sm-1" for="">%</label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Risk undertaken after retirement</label>
                                                    <div class="col-sm-3">
                                                        <select name="" id="SRisk_undertaken_aft" class="form-control">
                                                            <option value="7">Low</option>
                                                            <option value="12">Moderate</option>
                                                            <option value="15">High</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Rate of return</label>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="form-control" id="SRate_return_aft" name="" placeholder="" required>
                                                    </div>
                                                    <label class="control-label col-sm-1" for="">%</label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Monthly expenses after retirement</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="SMonthly_expenses_aft" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Rate of inflation</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="SRate_inflation" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        Your retirement corpus could be *** to reach this goal SIP amount to be invested *** per month.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <div class="graph-box border-1 pad-20">
                                                        <!-- <h3>95</h3> -->
                                                        <h5>Graph</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        <ul>
                                                            <li>I can invest SIP of *** per month.</li>
                                                            <li>I can invest SIP of *** per month At current SIP value, your retirement fund(including current lumpsum investment) is Rs. ***</li>
                                                            <li>Increase SIP Investment to *** .</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <a class="btn btn-success">Email report</a>
                                                    <a class="btn btn-primary" data-toggle="collapse" data-parent="#retRichAcc" href="#retRiccollapse3">Invest now</a>
                                                    <a class="btn btn-info">Revise your goal</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#retRichAcc" href="#retRiccollapse3">Recomended lumpsum/SIP saving schemes</a>
        </h4>
                            </div>
                            <div id="retRiccollapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <!-- <legend>Recomended lumpsum/SIP saving schemes</legend> -->
                                        <div class="col-md-3">
                                            <!-- <h5>Investment allocation</h5>
                                            <img src="https://images.freeimages.com/images/premium/large-thumbs/1975/19756028-blank-pie-chart-isolated-on-white-background.jpg" class="img-responsive" alt="Pi chart">
                                            <p>Equity</p>
                                            <p>Debt</p> -->
                                            <div id="piechart"></div>
                                        </div>
                                        <div class="col-md-9">
                                            <p>Top mutual fund schemes</p>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>SL No.</th>
                                                        <th>Scheme</th>
                                                        <th>Category</th>
                                                        <th>Amount(INR)</th>
                                                        <th>Alocation</th>
                                                        <th>Return</th>
                                                        <th>Compare</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>ICICI prudential long term fund - Regular plan (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>15.93%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>ICICI prudential balance fund(G)</td>
                                                        <td>Balanced</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>28.31%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Aditya Birla Sun Life frontline equity fund (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>25.62%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Total</td>
                                                        <td colspan="5">9000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>More scheme</a>
                                                </label>
                                                <label class="control-label col-sm-3 col-sm-offset-5" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>Start investments</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-box well wealthHide grandWedding" id="grandWeddingShow">
                    <div class="panel-group" id="grandWedAcc">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#grandWedAcc" href="#grandWedcollapse1">Grand Weeding</a>
        </h4>
                            </div>
                            <div id="grandWedcollapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form-horizontal" action="/action_page.php">
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Current age</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="gndWedCurrAge" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Marriage age</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="gndWedMariageAge" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Cost of similar grand wedding as on today</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="gndWedAmt" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">My current lumpsum investment for wedding</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="gndWedLumpsumAmt" name="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Risk undertaken</label>
                                            <div class="col-sm-3">
                                                <select class="form-control" id="gndWedRiskTaken">
                                                    <option value="7">Low - 7%</option>
                                                    <option value="12">Moderate - 12%</option>
                                                    <option value="15">High - 15%</option>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Rate of return</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="gndWedIntRate" value="7" name="" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Inflation Rate (%)</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="gndWedInfRate" name="" value="5" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <a data-toggle="collapse" data-parent="#grandWedAcc" href="#grandWedcollapse2" class="btn btn-success pull-right" id="gndWedProcessBtn1">Process</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#grandWedAcc" href="#grandWedcollapse2">Summary</a>
        </h4>
                            </div>
                            <div id="grandWedcollapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <!-- <legend>Summary</legend> -->
                                        <div class="col-md-5">
                                            <form class="form-horizontal" action="/action_page.php">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Number of years to go for wedding</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="gndWedNumYears" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Cost of similar grand wedding as on today</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="gndWedAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Current lumpsum investment for wedding</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="gndWedLumpsumAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Monthly contribution to be done</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="gndWedInvPerMonth" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Risk undertaken</label>
                                                    <div class="col-sm-3">
                                                        <select name="" id="gndWedRiskTaken2" class="form-control">
                                                            <option value="7">Low</option>
                                                            <option value="12">Moderate</option>
                                                            <option value="15">High</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Rate of return</label>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="form-control" id="gndWedIntRate2" name="" placeholder="" >
                                                    </div>
                                                    <label class="control-label col-sm-1" for="">%</label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Inflation rate</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="gndWedInfRate2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        Future value of wedding costs <span id="gndWedFvAmt"></span> to reach this goal SIP amount to be invested <span id="gndWedSipAmt"></span> per month.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <div class="graph-box border-1 pad-20">
                                                        <!-- <h3>95</h3> -->
                                                        <h5>Graph</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        <ul>
                                                            <li>I can invest SIP of <span id="gndWedSipAmt2"></span> per month.</li>
                                                            <li>At current SIP value, your grand wedding available fund(including current lumpsum saving) is Rs. <span id="gndWedAvilAmt"></span> Increase SIP Investment to <span id="gndWedSipInv"></span> .</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <a class="btn btn-success">Email report</a>
                                                    <a class="btn btn-primary" data-toggle="collapse" data-parent="#grandWedAcc" href="#grandWedcollapse3">Invest now</a>
                                                    <a class="btn btn-info">Revise your goal</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#grandWedAcc" href="#grandWedcollapse3">Recomended lumpsum/SIP saving schemes</a>
        </h4>
                            </div>
                            <div id="grandWedcollapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <!-- <legend>Recomended lumpsum/SIP saving schemes</legend> -->
                                        <div class="col-md-3">
                                            <legend>Investment allocation</legend>
                                            <img src="https://images.freeimages.com/images/premium/large-thumbs/1975/19756028-blank-pie-chart-isolated-on-white-background.jpg" class="img-responsive" alt="Pi chart">
                                        </div>
                                        <div class="col-md-9">
                                            <p>Top mutual fund schemes</p>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>SL No.</th>
                                                        <th>Scheme</th>
                                                        <th>Category</th>
                                                        <th>Amount(INR)</th>
                                                        <th>Alocation</th>
                                                        <th>Return</th>
                                                        <th>Compare</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>ICICI prudential long term fund - Regular plan (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>15.93%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>ICICI prudential balance fund(G)</td>
                                                        <td>Balanced</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>28.31%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Aditya Birla Sun Life frontline equity fund (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>25.62%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Total</td>
                                                        <td colspan="5">9000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>More scheme</a>
                                                </label>
                                                <label class="control-label col-sm-3 col-sm-offset-5" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>Start investments</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <legend>Grand wedding</legend> -->
                </div>
                <div class="content-box well wealthHide higherEdu" id="higherEduShow">
                    <div class="panel-group" id="highEduAcc">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#highEduAcc" href="#highEducollapse1">Higher education</a>
        </h4>
                            </div>
                            <div id="highEducollapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <!-- <legend>Higher education</legend> -->
                                    <form class="form-horizontal" action="/action_page.php">
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Current age</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="highEduCurrAge" name="" placeholder="" required>
                                            </div>
                                        </div>
                                    <form class="form-horizontal" action="/action_page.php">
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Higher education will start at the age of</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="highEduSrtAge" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">I have estimated the fee and expences</label>
                                            <div class="col-sm-3">
                                                <select name="" id="highEduEstExpns" class="form-control">
                                                    <option value="">Yes</option>
                                                    <option value="">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Amount required for fee and expenses</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="highEduAmt" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">My current lumpsum investment for higher education</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="highEduLumpsumAmt" name="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Risk undertaken</label>
                                            <div class="col-sm-3">
                                                <select class="form-control" id="highEduRiskTaken">
                                                    <option value="7">Low - 7%</option>
                                                    <option value="12">Moderate - 12%</option>
                                                    <option value="15">High - 15%</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Rate of return</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="highEduIntRate" value="7" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Inflation Rate (%)</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="highEduInfRate" name="" value="5" placeholder="">
                                            </div>
                                        </div>
                                        <legend>fee expenses calculator(If no)</legend>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Choice of degree</label>
                                            <div class="col-sm-3">
                                                <select name="" id="" class="form-control">
                                                    <option value="">Engineering and technology</option>
                                                    <option value="">Fashion designing</option>
                                                    <option value="">Finance</option>
                                                    <option value="">Hotel Management</option>
                                                    <option value="">LAW and science</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Education level</label>
                                            <div class="col-sm-3">
                                                <select name="" id="" class="form-control">
                                                    <option value="">PG</option>
                                                    <option value="">UG</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Country of study</label>
                                            <div class="col-sm-3">
                                                <select name="" id="" class="form-control">
                                                    <option value="">India</option>
                                                    <option value="">Other</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Category of institution</label>
                                            <div class="col-sm-3">
                                                <select name="" id="" class="form-control">
                                                    <option value="">Top</option>
                                                    <option value="">Mid</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Institution type</label>
                                            <div class="col-sm-3">
                                                <select name="" id="" class="form-control">
                                                    <option value="">Private</option>
                                                    <option value="">Public</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 col-sm-offset-3">
                                                <button class="btn btn-primary">Calculate</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <a data-toggle="collapse" data-parent="#highEduAcc" href="#highEducollapse2" class="btn btn-success pull-right" id="highEduProcessBtn1">Process</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#highEduAcc" href="#highEducollapse2">Summary</a>
        </h4>
                            </div>
                            <div id="highEducollapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <!-- <legend>Summary</legend> -->
                                        <div class="col-md-5">
                                            <form class="form-horizontal" action="/action_page.php">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Number of years to start higher education</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="highEduNumYears" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Education cost</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="highEduAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Current lumpsum investement</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="highEduLumpsumAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Monthly contribution (SIP) to be done</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="highEduInvPerMonth" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Risk undertaken</label>
                                                    <div class="col-sm-3">
                                                        <select name="" id="highEduRiskTaken2" class="form-control">
                                                            <option value="7">Low</option>
                                                            <option value="12">Moderate</option>
                                                            <option value="15">High</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Rate of return</label>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="form-control" id="highEduIntRate2" name="" placeholder="" required>
                                                    </div>
                                                    <label class="control-label col-sm-1" for="">%</label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Inflation rate</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="highEduInfRate2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        Your higher education corpus could be <span id="highEduFvAmt"></span> to reach this goal SIP amount to be invested monthly is <span id="highEduSipAmt"></span>.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <div class="graph-box border-1 pad-20">
                                                        <!-- <h3>95</h3> -->
                                                        <h5>Graph</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        <ul>
                                                            <li>I can invest SIP of <span id="highEduSipAmt2"></span> per month</li>
                                                            <li>At current SIP value, your fund for education(including current lumpsum investment) is Rs. <span id="highEduAvilAmt"></span> </li>
                                                            <li>Increase SIP Investment to <span id="highEduSipInv"></span> .</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <a class="btn btn-success">Email report</a>
                                                    <a class="btn btn-primary" data-toggle="collapse" data-parent="#highEduAcc" href="#highEducollapse3">Invest Now</a>
                                                    <a class="btn btn-info">Revise your goal</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#highEduAcc" href="#highEducollapse3">Recomended lumpsum/SIP saving schemes</a>
        </h4>
                            </div>
                            <div id="highEducollapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <!-- <legend>Recomended lumpsum/SIP saving schemes</legend> -->
                                        <div class="col-md-3">
                                            <legend>Investment allocation</legend>
                                            <img src="https://images.freeimages.com/images/premium/large-thumbs/1975/19756028-blank-pie-chart-isolated-on-white-background.jpg" class="img-responsive" alt="Pi chart">
                                        </div>
                                        <div class="col-md-9">
                                            <p>Top mutual fund schemes</p>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>SL No.</th>
                                                        <th>Scheme</th>
                                                        <th>Category</th>
                                                        <th>Amount(INR)</th>
                                                        <th>Alocation</th>
                                                        <th>Return</th>
                                                        <th>Compare</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>ICICI prudential long term fund - Regular plan (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>15.93%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>ICICI prudential balance fund(G)</td>
                                                        <td>Balanced</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>28.31%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Aditya Birla Sun Life frontline equity fund (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>25.62%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Total</td>
                                                        <td colspan="5">9000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>More scheme</a>
                                                </label>
                                                <label class="control-label col-sm-3 col-sm-offset-5" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>Start investments</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-box well wealthHide ownHouse" id="ownHouseShow">
                    <div class="panel-group" id="ownHouseAcc">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#ownHouseAcc" href="#ownHousecollapse1">Own A House</a>
        </h4>
                            </div>
                            <div id="ownHousecollapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form-horizontal" action="/action_page.php">
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Number of years to buy a house</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="ownHousNoOfYear" name="" placeholder="" required>
                                            </div>
                                            <label class="control-label col-sm-1" for="">years</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Amount required to buy</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="ownHousAmtReq" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">To complete the interior work(Estimated 20%)</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="ownHousIntWork" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Total cost</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="ownHousTotalAmt" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Risk undertaken</label>
                                            <div class="col-sm-3">
                                                <select class="form-control" id="ownHousRiskTaken">
                                                    <option value="7">Low - 7%</option>
                                                    <option value="12">Moderate - 12%</option>
                                                    <option value="15">High - 15%</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Rate of return</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="ownHousIntRate" name="" value="7" placeholder="" required >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Inflation Rate (%)</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="ownHousInfRate" name="" value="5" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Current lumpsum investment for purchase of house</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="ownHousLumpsumAmt" name="" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <a data-toggle="collapse" data-parent="#ownHouseAcc" href="#ownHousecollapse2" class="btn btn-success pull-right" id="ownHousProcessBtn1">Process</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#ownHouseAcc" href="#ownHousecollapse2">Summary</a>
        </h4>
                            </div>
                            <div id="ownHousecollapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <!-- <legend>Summary</legend> -->
                                        <div class="col-md-5">
                                            <form class="form-horizontal" action="/action_page.php">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Number of years</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="ownHousNumYears" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Amount required to buy</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="ownHousAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">My Current lumpsum investment</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="ownHousLumpsumAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Monthly Contribution (SIP) to be done</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="ownHousInvPerMonth" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Risk undertaken</label>
                                                    <div class="col-sm-3">
                                                        <select name="" id="ownHousRiskTaken2" class="form-control">
                                                            <option value="7">Low</option>
                                                            <option value="12">Moderate</option>
                                                            <option value="15">High</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Rate of return</label>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="form-control" id="ownHousIntRate2" name="" placeholder="" required>
                                                    </div>
                                                    <label class="control-label col-sm-1" for="">%</label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Inflation rate</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="ownHousInfRate2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        Future value of house costs <span id="ownHousFvAmt"></span> to reach this goal SIP amount to be invested <span id="ownHousSipAmt"></span> per month.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <div class="graph-box border-1 pad-20">
                                                        <!-- <h3>95</h3> -->
                                                        <h5>Graph</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        <ul>
                                                            <li>I can invest SIP of <span id="ownHousSipAmt2"></span> per month</li>
                                                            <li>At current SIP value, your fund for house(including current lumpsum saving) is Rs. <span id="ownHousAvilAmt"></span>.</li>
                                                            <li>Increase SIP Investment to RS. <span id="ownHousSipInv"></span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <a class="btn btn-success">Email report</a>
                                                    <a class="btn btn-primary" data-toggle="collapse" data-parent="#ownHouseAcc" href="#ownHousecollapse3">Invest now</a>
                                                    <a class="btn btn-info">Revise your goal</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#ownHouseAcc" href="#ownHousecollapse3">Recomended lumpsum/SIP saving schemes</a>
        </h4>
                            </div>
                            <div id="ownHousecollapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <!-- <legend>Recomended lumpsum/SIP saving schemes</legend> -->
                                        <div class="col-md-3">
                                            <legend>Investment allocation</legend>
                                            <img src="https://images.freeimages.com/images/premium/large-thumbs/1975/19756028-blank-pie-chart-isolated-on-white-background.jpg" class="img-responsive" alt="Pi chart">
                                        </div>
                                        <div class="col-md-9">
                                            <p>Top mutual fund schemes</p>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>SL No.</th>
                                                        <th>Scheme</th>
                                                        <th>Category</th>
                                                        <th>Amount(INR)</th>
                                                        <th>Alocation</th>
                                                        <th>Return</th>
                                                        <th>Compare</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>ICICI prudential long term fund - Regular plan (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>15.93%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>ICICI prudential balance fund(G)</td>
                                                        <td>Balanced</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>28.31%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Aditya Birla Sun Life frontline equity fund (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>25.62%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Total</td>
                                                        <td colspan="5">9000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>More scheme</a>
                                                </label>
                                                <label class="control-label col-sm-3 col-sm-offset-5" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>Start investments</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <legend>Own a house</legend> -->
                </div>
                <div class="content-box well wealthHide buyCar" id="buyCarShow">
                    <div class="panel-group" id="buyCarAcc">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#buyCarAcc" href="#buyCarcollapse1">Buy A Car</a>
        </h4>
                            </div>
                            <div id="buyCarcollapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form-horizontal" action="/action_page.php">
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Number of years to buy a car</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="buyACarNoOfYear" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Cost of the car</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="buyACarAmtReq" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">My current lumpsum investment for purchase of car</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="buyACarLumpsumAmt" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Risk undertaken</label>
                                            <div class="col-sm-3">
                                                <select class="form-control" id="buyACarRiskTaken">
                                                    <option value="7">Low - 7%</option>
                                                    <option value="12">Moderate - 12%</option>
                                                    <option value="15">High - 15%</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Rate of return</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="buyACarIntRate" name="" placeholder="" >
                                            </div>
                                            <label class="control-label col-sm-1" for="">%</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Inflation Rate (%)</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="buyACarInfRate" name="" value="5" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <a data-toggle="collapse" data-parent="#buyCarAcc" href="#buyCarcollapse2" class="btn btn-success pull-right" id="buyACarProcessBtn1">Process</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#buyCarAcc" href="#buyCarcollapse2">Summary</a>
        </h4>
                            </div>
                            <div id="buyCarcollapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <legend>Summary</legend>
                                        <div class="col-md-5">
                                            <form class="form-horizontal" action="/action_page.php">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Number of years</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="buyACarNumYears" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Cost of the car</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="buyACarAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Current lumpsum Investment</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="buyACarLumpsumAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Risk undertaken</label>
                                                    <div class="col-sm-3">
                                                        <select name="" id="buyACarRiskTaken2" class="form-control">
                                                            <option value="7">Low</option>
                                                            <option value="12">Moderate</option>
                                                            <option value="15">High</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Rate of return</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="buyACarIntRate2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Inflation rate</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="buyACarInfRate2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Monthly contribution to be done</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="buyACarInvPerMonth" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        Future value of car <span id="buyACarFvAmt"></span> to reach the goal SIP Rs. <span id="buyACarSipAmt"></span> to be invested per month.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <div class="graph-box border-1 pad-20">
                                                        <!-- <h3>95</h3> -->
                                                        <h5>Graph</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        <ul>
                                                            <li>I can invest SIP of <span id="buyACarSipAmt2"></span> per month</li>
                                                            <li>At current SIP value, your retirement fund(including current lumpsum saving) is Rs. <span id="buyACarAvilAmt"></span> </li>
                                                            <li>Increase SIP Investment to <span id="buyACarSipInv"></span> .</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <a class="btn btn-success">Email report</a>
                                                    <a class="btn btn-primary" data-toggle="collapse" data-parent="#buyCarAcc" href="#buyCarcollapse3">Invest now</a>
                                                    <a class="btn btn-info">Revise your goal</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#buyCarAcc" href="#buyCarcollapse3">Recomended lumpsum/SIP saving schemes</a>
        </h4>
                            </div>
                            <div id="buyCarcollapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <!-- <legend>Recomended lumpsum/SIP saving schemes</legend> -->
                                        <div class="col-md-3">
                                            <legend>Investment allocation</legend>
                                            <img src="https://images.freeimages.com/images/premium/large-thumbs/1975/19756028-blank-pie-chart-isolated-on-white-background.jpg" class="img-responsive" alt="Pi chart">
                                        </div>
                                        <div class="col-md-9">
                                            <p>Top mutual fund schemes</p>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>SL No.</th>
                                                        <th>Scheme</th>
                                                        <th>Category</th>
                                                        <th>Amount(INR)</th>
                                                        <th>Alocation</th>
                                                        <th>Return</th>
                                                        <th>Compare</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>ICICI prudential long term fund - Regular plan (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>15.93%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>ICICI prudential balance fund(G)</td>
                                                        <td>Balanced</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>28.31%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Aditya Birla Sun Life frontline equity fund (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>25.62%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Total</td>
                                                        <td colspan="5">9000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="">
                                                <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>More scheme</a>
                                            </label>
                                            <label class="control-label col-sm-3 col-sm-offset-5" for="">
                                                <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>Start investments</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-box well wealthHide vacPlan" id="vacPlanShow">
                    <div class="panel-group" id="vacPlanAcc">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#vacPlanAcc" href="#vacPlancollapse1">Vacation plan</a>
        </h4>
                            </div>
                            <div id="vacPlancollapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form-horizontal" action="/action_page.php">
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">After how many years, you are planning for vacation</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="vacaPlanNoOfYear" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Cost of vacation</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="vacaPlanAmtReq" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">My current lumpsum investment for vacation</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="vacaPlanLumpsumAmt" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Risk undertaken</label>
                                            <div class="col-sm-3">
                                                <select class="form-control" id="vacaPlanRiskTaken">
                                                    <option value="7">Low - 7%</option>
                                                    <option value="12">Moderate - 12%</option>
                                                    <option value="15">High - 15%</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Rate of return</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="vacaPlanIntRate" value="7" name="" placeholder="" required>
                                            </div>
                                            <label class="control-label col-sm-1" for="">%</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Inflation Rate (%)</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="vacaPlanInfRate" name="" value="5" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <a data-toggle="collapse" data-parent="#vacPlanAcc" href="#vacPlancollapse2" class="btn btn-success pull-right" id="vacaPlanProcessBtn1">Process</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#vacPlanAcc" href="#vacPlancollapse2">Summary</a>
        </h4>
                            </div>
                            <div id="vacPlancollapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <form class="form-horizontal" action="/action_page.php">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Number of years</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="vacaPlanNumYears" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Amount</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="vacaPlanAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Current lumpsum investment</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="vacaPlanLumpsumAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Risk undertaken</label>
                                                    <div class="col-sm-3">
                                                        <select name="" id="vacaPlanRiskTaken2" class="form-control">
                                                            <option value="7">Low</option>
                                                            <option value="12">Moderate</option>
                                                            <option value="15">High</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Rate of return</label>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="form-control" id="vacaPlanIntRate2" name="" placeholder="" required>
                                                    </div>
                                                    <label class="control-label col-sm-1" for="">%</label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Rate of inflation</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="vacaPlanInfRate2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Monthly contribution to be done rs.</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="vacaPlanInvPerMonth" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        Future value of vacation <span id="vacaPlanFvAmt"></span> to reach this goal SIP of Rs. <span id="vacaPlanSipAmt"></span> to be invested per month.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <div class="graph-box border-1 pad-20">
                                                        <!-- <h3>95</h3> -->
                                                        <h5>Graph</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        <ul>
                                                            <li>I can invest SIP of <span id="vacaPlanSipAmt2"></span> per month</li>
                                                            <li>At current SIP value, your fund for vacation plan(including current lumpsum saving) is Rs. <span id="vacaPlanAvilAmt"></span></li>
                                                            <li>Increase SIP Investment to <span id="vacaPlanSipInv"></span> .</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <a class="btn btn-success">Email report</a>
                                                    <a class="btn btn-primary" data-toggle="collapse" data-parent="#vacPlanAcc" href="#vacPlancollapse3">Invest now</a>
                                                    <a class="btn btn-info">Revise your goal</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#vacPlanAcc" href="#vacPlancollapse3">Recomended lumpsum/SIP saving schemes</a>
        </h4>
                            </div>
                            <div id="vacPlancollapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <legend>Investment allocation</legend>
                                            <img src="https://images.freeimages.com/images/premium/large-thumbs/1975/19756028-blank-pie-chart-isolated-on-white-background.jpg" class="img-responsive" alt="Pi chart">
                                        </div>
                                        <div class="col-md-9">
                                            <p>Top mutual fund schemes</p>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>SL No.</th>
                                                        <th>Scheme</th>
                                                        <th>Category</th>
                                                        <th>Amount(INR)</th>
                                                        <th>Alocation</th>
                                                        <th>Return</th>
                                                        <th>Compare</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>ICICI prudential long term fund - Regular plan (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>15.93%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>ICICI prudential balance fund(G)</td>
                                                        <td>Balanced</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>28.31%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Aditya Birla Sun Life frontline equity fund (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>25.62%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Total</td>
                                                        <td colspan="5">9000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="">

                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>More scheme</a>
                                                </label>
                                                <label class="control-label col-sm-3 col-sm-offset-5" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>Start investments</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-box well wealthHide emerFund" id="emerFundShow">
                    <div class="panel-group" id="emergFundAcc">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#emergFundAcc" href="#emergFundcollapse1">Emergency fund</a>
        </h4>
                            </div>
                            <div id="emergFundcollapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form-horizontal" action="/action_page.php">
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Amount required in case of emergency</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="emeFundAmtReq" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">In how many years you want to realize</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="emeFundNoOfYear" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">My current lumpsum investment</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="emeFundLumpsumAmt" name="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Risk undertaken</label>
                                            <div class="col-sm-3">
                                                <select class="form-control" id="emeFundRiskTaken">
                                                    <option value="7">Low - 7%</option>
                                                    <option value="12">Moderate - 12%</option>
                                                    <option value="15">High - 15%</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Rate of return</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="emeFundIntRate" value="7" name="" placeholder="">
                                            </div>
                                            <label class="control-label col-sm-1" for="">%</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Inflation Rate (%)</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="emeFundInfRate" name="" value="5" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <a data-toggle="collapse" data-parent="#emergFundAcc" href="#emergFundcollapse2" class="btn btn-success pull-right" id="emeFundProcessBtn1">Process</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#emergFundAcc" href="#emergFundcollapse2">Summary</a>
        </h4>
                            </div>
                            <div id="emergFundcollapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <form class="form-horizontal" action="/action_page.php">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Build emergency fund in year</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="emeFundNumYears" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Amount required</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="emeFundAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">My current lumpsum investment</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="emeFundLumpsumAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Risk undertaken</label>
                                                    <div class="col-sm-3">
                                                        <select name="" id="emeFundRiskTaken2" class="form-control">
                                                            <option value="7">Low</option>
                                                            <option value="12">Moderate</option>
                                                            <option value="15">High</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Rate of return</label>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="form-control" id="emeFundIntRate2" name="" placeholder="" required>
                                                    </div>
                                                    <label class="control-label col-sm-1" for="">%</label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Rate of inflation</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="emeFundInfRate2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Monthly contribution to be done</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="emeFundInvPerMonth" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        Future value of emergency fund <span id="emeFundFvAmt"></span> to reach this goal SIP of Rs. <span id="emeFundSipAmt"></span> to be invested.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <div class="graph-box border-1 pad-20">
                                                        <!-- <h3>95</h3> -->
                                                        <h5>Graph</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        <ul>
                                                            <li>I can invest SIP of <span id="emeFundSipAmt2"></span> per month</li>
                                                            <li>At current SIP value, your fund for emergency(including current lumpsum investment) is Rs. <span id="emeFundAvilAmt"></span> </li>
                                                            <li>Increase SIP Investment to <span id="emeFundSipInv"></span> .</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <a class="btn btn-success">Email report</a>
                                                    <a class="btn btn-primary" data-toggle="collapse" data-parent="#emergFundAcc" href="#emergFundcollapse3">Invest now</a>
                                                    <a class="btn btn-info">Revise your goal</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#emergFundAcc" href="#emergFundcollapse3">Recomended lumpsum/SIP saving schemes</a>
        </h4>
                            </div>
                            <div id="emergFundcollapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <!-- <legend>Recomended lumpsum/SIP saving schemes</legend> -->
                                        <div class="col-md-3">
                                            <legend>Investment allocation</legend>
                                            <img src="https://images.freeimages.com/images/premium/large-thumbs/1975/19756028-blank-pie-chart-isolated-on-white-background.jpg" class="img-responsive" alt="Pi chart">
                                        </div>
                                        <div class="col-md-9">
                                            <p>Top mutual fund schemes</p>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>SL No.</th>
                                                        <th>Scheme</th>
                                                        <th>Category</th>
                                                        <th>Amount(INR)</th>
                                                        <th>Alocation</th>
                                                        <th>Return</th>
                                                        <th>Compare</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>ICICI prudential long term fund - Regular plan (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>15.93%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>ICICI prudential balance fund(G)</td>
                                                        <td>Balanced</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>28.31%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Aditya Birla Sun Life frontline equity fund (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>25.62%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Total</td>
                                                        <td colspan="5">9000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>More scheme</a>
                                                </label>
                                                <label class="control-label col-sm-3 col-sm-offset-5" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>Start investments</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-box well wealthHide uniGoal" id="uniGoalShow">
                    <div class="panel-group" id="uniGoalAcc">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#uniGoalAcc" href="#uniGoalcollapse1">Unieque Goal</a>
        </h4>
                            </div>
                            <div id="uniGoalcollapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form-horizontal" action="/action_page.php">
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">How long will you take to achive your goal</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="uniqGoalNoOfYear" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">How much amount do you need to achieve this goal</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="uniqGoalAmtReq" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">My current lumpsum investment for this goal</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="uniqGoalLumpsumAmt" name="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Risk undertaken</label>
                                            <div class="col-sm-3">
                                                <select class="form-control" id="uniqGoalRiskTaken">
                                                    <option value="7">Low - 7%</option>
                                                    <option value="12">Moderate - 12%</option>
                                                    <option value="15">High - 15%</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Rate of return</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="uniqGoalIntRate" name="" placeholder="">
                                            </div>
                                            <label class="control-label col-sm-1" for="">%</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Inflation Rate (%)</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="uniqGoalInfRate" name="" value="5" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <a data-toggle="collapse" data-parent="#uniGoalAcc" href="#uniGoalcollapse2" class="btn btn-success pull-right" id="uniqGoalProcessBtn1">Process</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#uniGoalAcc" href="#uniGoalcollapse2">Summary</a>
        </h4>
                            </div>
                            <div id="uniGoalcollapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <!-- <legend>Summary</legend> -->
                                        <div class="col-md-5">
                                            <form class="form-horizontal" action="/action_page.php">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Number of years</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="uniqGoalNumYears" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Amount required</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="uniqGoalAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Current lumpsum investment</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="uniqGoalLumpsumAmt2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Risk undertaken</label>
                                                    <div class="col-sm-3">
                                                        <select name="" id="uniqGoalRiskTaken2" class="form-control">
                                                            <option value="7">Low</option>
                                                            <option value="12">Moderate</option>
                                                            <option value="15">High</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Rate of return</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="uniqGoalIntRate2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Rate of inflation</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="uniqGoalInfRate2" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-9" for="">Monthly contribution to be done</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="uniqGoalInvPerMonth" name="" placeholder="" required>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        Future value of your goal is <span id="uniqGoalFvAmt"></span> to reach this goal SIP of Rs. <span id="uniqGoalSipAmt"></span> to be invested.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <div class="graph-box border-1 pad-20">
                                                        <!-- <h3>95</h3> -->
                                                        <h5>Graph</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="alert alert-info">
                                                        <ul>
                                                            <li>I can invest SIP of <span id="uniqGoalSipAmt2"></span> per month</li>
                                                            <li>At current SIP value, your retirement fund(including current lumpsum saving) is Rs. <span id="uniqGoalAvilAmt"></span> </li>
                                                            <li>Increase SIP Investment to <span id="uniqGoalSipInv"></span> .</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <a class="btn btn-success">Email report</a>
                                                    <a class="btn btn-primary" data-toggle="collapse" data-parent="#uniGoalAcc" href="#uniGoalcollapse3">Invest now</a>
                                                    <a class="btn btn-info">Revise your goal</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#uniGoalAcc" href="#uniGoalcollapse3">Recomended lumpsum/SIP saving schemes</a>
        </h4>
                            </div>
                            <div id="uniGoalcollapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <!-- <legend>Recomended lumpsum/SIP saving schemes</legend> -->
                                        <div class="col-md-3">
                                            <legend>Investment allocation</legend>
                                            <img src="https://images.freeimages.com/images/premium/large-thumbs/1975/19756028-blank-pie-chart-isolated-on-white-background.jpg" class="img-responsive" alt="Pi chart">
                                        </div>
                                        <div class="col-md-9">
                                            <p>Top mutual fund schemes</p>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>SL No.</th>
                                                        <th>Scheme</th>
                                                        <th>Category</th>
                                                        <th>Amount(INR)</th>
                                                        <th>Alocation</th>
                                                        <th>Return</th>
                                                        <th>Compare</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>ICICI prudential long term fund - Regular plan (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>15.93%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>ICICI prudential balance fund(G)</td>
                                                        <td>Balanced</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>28.31%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Aditya Birla Sun Life frontline equity fund (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>25.62%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Total</td>
                                                        <td colspan="5">9000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>More scheme</a>
                                                </label>
                                                <label class="control-label col-sm-3 col-sm-offset-5" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-plus"></span>Start investments</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Learn and plan screen start -->
                <div class="content-box well learnPlanAndInvHide riskProfShow">
                    riskProfshow
                </div>
                <div class="content-box well learnPlanAndInvHide calCulatorsShow">
                    <h2>All calculators</h2>
                    <div class="list-group">
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#sipCal">SIP calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#lumpsumCal">Lumpsum calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#taxCal">TAX calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#fixdepCal">Fixed deposit calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#recdepCal">Recurring deposit calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#emiHomeCal">EMI Home Loan calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#emiCarCal">EMI Car Loan calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#emiPerCal">EMI Personal Loan calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#ppfCal">PPF calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#epfCal">EPF calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#npsCal">NPS calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#ssyCal">Sukanya Samriddhi Yojana(SSJ)</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#swpCal">SWP calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#hraCal">HRA calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#sipInsCal">SIP Installment Calculator</a>
                    </div>
                </div>
                <div class="content-box well learnPlanAndInvHide portAllocShow">
                    portAllocShow
                </div>
                <!-- Learn and plan screen end -->
                <!-- Investment screen start -->
                <div class="content-box well learnPlanAndInvHide sipSTPShow">
                    <div class="panel-group" id="sipAcc">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#sipAcc" href="#sipcollapse1">SIP</a>
        </h4>
                            </div>
                            <div id="sipcollapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form-horizontal" action="/action_page.php">
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">How much money do you want to invest in SIP per month</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">How many years you are planning to invest in SIP</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Risk undertaken</label>
                                            <div class="col-sm-2">
                                                <input type="text" class="form-control" id="" name="" placeholder="" value="Low" readonly>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" class="form-control" id="" name="" placeholder="" value="Moderate" readonly>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" class="form-control" id="" name="" placeholder="" value="High" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Rate of return</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="" name="" placeholder="" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" for="">Purpose of investment</label>
                                            <div class="col-sm-2">
                                                <input type="text" class="form-control" id="" name="" placeholder="" value="Tax saving" readonly>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" class="form-control" id="" name="" placeholder="" value="Wealth creation" readonly>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" class="form-control" id="" name="" placeholder="" value="Goal based planning" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <a class="btn btn-success pull-right" data-toggle="collapse" data-parent="#sipAcc" href="#sipcollapse3">Recomended SIP schemes</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#sipAcc" href="#sipcollapse3">Recomended SIP schemes</a>
        </h4>
                            </div>
                            <div id="sipcollapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <form class="form-horizontal" action="/action_page.php">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <legend>Investment allocation</legend>
                                                <img src="https://images.freeimages.com/images/premium/large-thumbs/1975/19756028-blank-pie-chart-isolated-on-white-background.jpg" class="img-responsive" alt="Pi chart">
                                            </div>
                                            <div class="col-md-9">
                                                <p>Hand picked SIP Schemees</p>
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>SL No.</th>
                                                            <th>Scheme</th>
                                                            <th>Category</th>
                                                            <th>Amount(INR)</th>
                                                            <th>Alocation</th>
                                                            <th>Return</th>
                                                            <th>Compare</th>
                                                            <th>Delete</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>ICICI prudential long term fund - Regular plan (G)</td>
                                                            <td>Debt long term</td>
                                                            <td>3000</td>
                                                            <td>33.33%</td>
                                                            <td>15.93%</td>
                                                            <td>
                                                                <select name="" id="" class="form-control">
                                                                    <option value="">Select 1</option>
                                                                    <option value="">Select 2</option>
                                                                    <option value="">Select 3</option>
                                                                </select>
                                                            </td>
                                                            <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>ICICI prudential balance fund(G)</td>
                                                            <td>Balanced</td>
                                                            <td>3000</td>
                                                            <td>33.33%</td>
                                                            <td>28.31%</td>
                                                            <td>
                                                                <select name="" id="" class="form-control">
                                                                    <option value="">Select 1</option>
                                                                    <option value="">Select 2</option>
                                                                    <option value="">Select 3</option>
                                                                </select>
                                                            </td>
                                                            <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Aditya Birla Sun Life frontline equity fund (G)</td>
                                                            <td>Debt long term</td>
                                                            <td>3000</td>
                                                            <td>33.33%</td>
                                                            <td>25.62%</td>
                                                            <td>
                                                                <select name="" id="" class="form-control">
                                                                    <option value="">Select 1</option>
                                                                    <option value="">Select 2</option>
                                                                    <option value="">Select 3</option>
                                                                </select>
                                                            </td>
                                                            <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">Total</td>
                                                            <td colspan="5">9000</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="form-group">
                                                    <div class="col-sm-9 col-md-offset-2">
                                                        <div class="alert alert-info">
                                                            If you have invested Rs. *** per month in this portfolio per month in this portfolio for last *** Years, it could be worth of Rs. *** with return of ** % .
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="">
                                                        <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-eye-open"></span> Select your own</a>
                                                    </label>
                                                    <label class="control-label col-sm-3 col-sm-offset-5" for="">
                                                        <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-save"></span> Start investments</a>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-box well learnPlanAndInvHide lumpSumShow">
                    <div class="panel-group" id="lumpSumAcc">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#lumpSumAcc" href="#lumpSumcollapse1">Lumpsum</a>
        </h4>
                            </div>
                            <div id="lumpSumcollapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">Recomended</div>
                                                <div class="panel-body">
                                                    <ul>
                                                        <li><a href="#">Select After Analysing</a></li>
                                                        <li><a href="">Top best schemes</a></li>
                                                    </ul>
                                                </div>
                                                <div class="panel-footer">
                                                    <button class="btn btn-default">Invest</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">Select on your own</div>
                                                <div class="panel-body">Search for 5000 schemes
                                                    <br> and
                                                    <br> do anlysis</div>
                                                <div class="panel-footer">
                                                    <button class="btn btn-default">Explore</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <a href="#" class="btn btn-default col-md-offset-4" data-toggle="modal" data-target="#lumpsumCal">Lumpsum calculator</a>
                                    <a class="btn btn-success pull-right" data-toggle="collapse" data-parent="#lumpSumAcc" href="#lumpSumcollapse3">Process</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#lumpSumAcc" href="#lumpSumcollapse3">Recomended schemes</a>
        </h4>
                            </div>
                            <div id="lumpSumcollapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <legend>Recomended schemes</legend>
                                        <div class="col-md-3">
                                            <h3>Assot Allocation</h3>
                                            <img src="https://images.freeimages.com/images/premium/large-thumbs/1975/19756028-blank-pie-chart-isolated-on-white-background.jpg" class="img-responsive" alt="Pi chart">
                                        </div>
                                        <div class="col-md-9">
                                            <p>Hand picked Schemees</p>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>SL No.</th>
                                                        <th>Scheme</th>
                                                        <th>Category</th>
                                                        <th>Amount(INR)</th>
                                                        <th>Alocation</th>
                                                        <th>Return</th>
                                                        <th>Compare</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>ICICI prudential long term fund - Regular plan (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>15.93%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>ICICI prudential balance fund(G)</td>
                                                        <td>Balanced</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>28.31%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Aditya Birla Sun Life frontline equity fund (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>25.62%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Total</td>
                                                        <td colspan="5">9000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="form-group">
                                                <div class="col-sm-9 col-md-offset-2">
                                                    <div class="alert alert-info">
                                                        If you have invested Rs. *** per month in this portfolio for last *** Years, it could be worth of Rs. *** with return of ** % .
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-6" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-eye-open"></span>Select your own</a>
                                                </label>
                                                <label class="control-label col-sm-6" for="">
                                                    <a class="btn icon-btn btn-success" href="#"><span class="glyphicon btn-glyphicon glyphicon-save"></span>Invest now</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-box well learnPlanAndInvHide saveTaxShow">
                    <div class="panel-group" id="taxSavingAcc">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#taxSavingAcc" href="#taxSavingcollapse1">Tax Saving</a>
        </h4>
                            </div>
                            <div id="taxSavingcollapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form-horizontal" action="/action_page.php">
                                        <div class="form-group">
                                            <label class="control-label col-md-8" for="">Do you know much to invest for saving taxes this year ?</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-3" for="">
                                                <button class="btn btn-primary">Yes, I know</button>
                                            </label>
                                            <label class="control-label col-sm-3 col-sm-offset-5" for="">
                                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#taxCal">No, help me</a>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-sm-4" for="">
                                                    <button class="btn btn-primary">Lump sum</button>
                                                </label>
                                                <label class="control-label col-sm-4" for="">
                                                    <button class="btn btn-primary">SIP</button>
                                                </label>
                                            </div>
                                        </div>
                                    </form>
                                    <a class="btn btn-success pull-right" data-toggle="collapse" data-parent="#taxSavingAcc" href="#taxSavingcollapse3">Process</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#taxSavingAcc" href="#taxSavingcollapse3">Recomended schemes</a>
        </h4>
                            </div>
                            <div id="taxSavingcollapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <form class="form-horizontal" action="/action_page.php">
                                        <div class="form-group">
                                            <legend>Hand picked SIP Schemees</legend>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>SL No.</th>
                                                        <th>Scheme</th>
                                                        <th>Category</th>
                                                        <th>Amount(INR)</th>
                                                        <th>Alocation</th>
                                                        <th>Return</th>
                                                        <th>Compare</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>ICICI prudential long term fund - Regular plan (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>15.93%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>ICICI prudential balance fund(G)</td>
                                                        <td>Balanced</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>28.31%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Aditya Birla Sun Life frontline equity fund (G)</td>
                                                        <td>Debt long term</td>
                                                        <td>3000</td>
                                                        <td>33.33%</td>
                                                        <td>25.62%</td>
                                                        <td>
                                                            <select name="" id="" class="form-control">
                                                                <option value="">Select 1</option>
                                                                <option value="">Select 2</option>
                                                                <option value="">Select 3</option>
                                                            </select>
                                                        </td>
                                                        <td><span class="glyphicon glyphicon-remove-circle"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Total</td>
                                                        <td colspan="5">9000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Investment screen end -->
                <div class="content-box well wealthDashMain">
                    <legend>Learn and plan </legend>
                    <div class="row" id="rowLearnPlan">
                        <div class="col-md-4 text-center">
                            <a href="#">
                                <div class="stats-box border-1 pad-20" id="riskProf">
                                    <!-- <h3>95</h3> -->
                                     <h3>Risk Profile</h3>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil deleniti deserunt, possimus cum harum maxime necessitatibus a, fugiat consectetur corporis, eos, rem perspiciatis quaerat neque reiciendis. Cum laborum suscipit minus.</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-4 text-center">
                            <a href="#">
                                <div class="stats-box border-1 pad-20" id="calCulators">
                                    <!-- <h3>115</h3> -->
                                     <h3>Calculators</h3>
                                                    <p><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/cal_box.png" alt="Tax save calCulators" style="width: 110px; height: 110px;"></p>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-4 text-center">
                            <a href="#">
                                <div class="stats-box border-1 pad-20" id="portAlloc">
                                    <!-- <h3>45</h3> -->
                                    <h3>Portfolio Allocation</h3>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil deleniti deserunt, possimus cum harum maxime necessitatibus a, fugiat consectetur corporis, eos, rem perspiciatis quaerat neque reiciendis. Cum laborum suscipit minus.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <legend>Investment</legend>
                    <div class="row" id="rowInvestment">
                        <div class="col-md-4 text-center">
                            <a href="#">
                                <div class="stats-box border-1 pad-20" id="sipSTP">
                                     <h3>SIP/STP</h3>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil deleniti deserunt, possimus cum harum maxime necessitatibus a, fugiat consectetur corporis, eos, rem perspiciatis quaerat neque reiciendis. Cum laborum suscipit minus.</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-4 text-center">
                            <a href="#">
                                <div class="stats-box border-1 pad-20" id="lumpSum">
                                    <!-- <h3>45</h3> -->
                                   <h3>Lump sum</h3>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil deleniti deserunt, possimus cum harum maxime necessitatibus a, fugiat consectetur corporis, eos, rem perspiciatis quaerat neque reiciendis. Cum laborum suscipit minus.</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-4 text-center">
                            <a href="#">
                                <div class="stats-box border-1 pad-20" id="saveTax">
                                    <!-- <h3>95</h3> -->
                                   <h3>Tax save</h3>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil deleniti deserunt, possimus cum harum maxime necessitatibus a, fugiat consectetur corporis, eos, rem perspiciatis quaerat neque reiciendis. Cum laborum suscipit minus.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
                            <div class="col-md-2">
                                <div class="sidebarR">
                                    <div class="list-group">
                                        <a href="#" class="list-group-item" id="#retireRich">eKYC</a>
                                        <a href="#" class="list-group-item" id="#grandWedding">Know your taxes</a>
                                        <a href="#" class="list-group-item" id="#higherEdu">Print rent receipts</a>
                                        <a href="#" class="list-group-item" id="#ownHouse">Pay income tax</a>
                                        <a href="#" class="list-group-item" id="#buyCar">Tax heading</a>
                                        <a href="#" class="list-group-item" id="#vacPlan">Tax heading</a>
                                        <a href="#" class="list-group-item" id="#emerFund">Tax heading</a>
                                        <a href="#" class="list-group-item" id="#uniGoal">Tax heading</a>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-2">
                                <div class="content-box well" style="min-height: 390px;">
                                    <div class="ekycDiv">
                                        <ul>
                                            <li>eKYC</li>
                                            <li>Know your taxes</li>
                                            <li>Print Rent Receipts</li>
                                            <li>Pay income tax</li>
                                            <li></li>
                                        </ul>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
<!-- All calculators modal start here -->
<!-- SIP calculator -->
<div class="modal fade" id="sipCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">SIP calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">SIP amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-3" for="">Monthly invested</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Invested for</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">Year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Expected rate of return</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Adjust for inflation</label>
                        <div class="col-sm-3">
                            <select name="" id="" class="form-control">
                                <option value="">Yes</option>
                                <option value="">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Inflation rate</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Future value investment of</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                If you invest Rs.***** per month for *** years @ **% P.A expected rate of return, you will accumulate Rs. ***** at the end of the **<sup>th</sup> year.
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <p>If you invest Rs.***** per month for *** years @ **% P.A expected rate of return, you will accumulate Rs. ***** at the end of the **<sup>th</sup> year</p>
                    </div> -->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- Lumpsum calculator -->
<div class="modal fade" id="lumpsumCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Lumpsum calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Lumpsum amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Expected rate of return</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Time period</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">Year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Adjust for inflation</label>
                        <div class="col-sm-3">
                            <select name="" id="" class="form-control">
                                <option value="">Yes</option>
                                <option value="">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Inflation rate</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Future value of investment</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                If you invest Rs.***** per month for *** years @ **% P.A expected rate of return, you will accumulate Rs. ***** at the end of the **<sup>th</sup> year.
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- TAX calculator -->
<div class="modal fade" id="taxCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">TAX calculator</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-9 col-md-offset-2">
                    <p><strong>Investment limit under Sec 80C Rs.1,50,000</strong></p>
                <p><strong>how much are you investing annually in the following</strong></p>
                </div>
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Age</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxAgeNum" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Equity linked saving scheme(ELSS)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxElssSchemeAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">life insurance premimum paid for self, spouse and children</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxLICSchemeAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Sukanya Samriddhi Yojana(SSY)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxSSYSchemeAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">5 years fixed deposit</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxFDAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">PPF investment</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxPPFAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">unit linked insurance plan</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxInsAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Any other 80C</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxOtherAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Your annual salary</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxAnnSal" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7">Check</label>
                        <div class="col-sm-3">
                            <input type="button" name="" id="taxCheckBtn" value="Check Tax" class="btn btn-primary">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Your current investment are</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxTotalInvAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Further investment opportunity</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxRemInvAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Tax saved through further investment</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxSaveAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- Fixed deposit calculator -->
<div class="modal fade" id="fixdepCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Fixed deposit calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="fixdepAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Invested for number of</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="fixdepNum" name="" placeholder="" required>
                        </div>
                        <div class="col-sm-3">
                            <select name="" id="fixdepNumType" class="form-control">
                                <option value="1">Years</option>
                                <option value="12">Months</option>
                                <option value="365">Days</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest rate</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="fixdepIntRate" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Frequency</label>
                        <div class="col-sm-3">
                            <select name="" id="fixdepIntType" class="form-control">
                                <option value="simpleInterest">Simple interest</option>
                                <option value="12">Monthly</option>
                                <option value="4">Quarterly</option>
                                <option value="2">Half yearly</option>
                                <option value="1">Annually</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" name="" class="btn btn-primary" value="Check Fixed Deposit" id="fdCheckBtn">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Maturity value of investment at the end of <span class="fixdepYearShow">**</span> Rs. </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="fixdepMatAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest earned </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="fixdepTotalInt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                If you invest Rs.<span id="fixdepInvestAmt">*****</span> per <span id="fixdepTimeShow">month</span> for <span class="fixdepYearShow">***</span> @ <span id="fixdepIntShow"></span>% P.A expected rate of return, you will accumulate Rs. <span id="fixdepTotalAmt">****</span> at the end of the <span class="fixdepYearShow">**</span>.
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Earn More then FD's by Investing MF's ".
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- Recurring deposit calculator -->
<div class="modal fade" id="recdepCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Recurring deposit calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount invested monthly</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="rdAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Invested for no. of</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="rdNumYears" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">Year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest rate</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="rdIntRate" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="btn btn-primary" id="rdCheckBtn" name="" value="Check RD" placeholder="" >
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Maturity value of investment at the end of <span id="rdYearShow">**</span><sup>th</sup> year is Rs. </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="rdTotalAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Earn More then RD's by Investing MF's ".
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- EMI calculator for home loan-->
<div class="modal fade" id="emiHomeCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">EMI calculator for home loan</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Housing Loan Amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="homeLoanAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest rate(P.A.)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="homeLoanIntRate" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Tenure (Yrs)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="homeLoanNumYears" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="homeLoanNumYears">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="btn btn-primary" id="homeLoanCheck" name="" placeholder="" value="Check Home Loan" required>
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">EMI amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="homeLoanEmiAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total amount payable</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="homeLoanTotalPayAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest component</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="homeLoanTotalInt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                <strong>Suggestion:</strong>
                                <br> Keep your loan tenure as low as possible, as the tenure increases interest component increases significantly.
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Invest in SIP's to reach your own house goal ".
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- EMI calculator for car loan-->
<div class="modal fade" id="emiCarCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">EMI calculator for car loan</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Car Loan Amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="carLoanAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest rate(P.A.)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="carLoanIntRate" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Tenure (Yrs)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="carLoanNumYears" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="btn btn-primary" id="carLoanCheck" name="" placeholder="" value="Check Car Loan" required>
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">EMI amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="carLoanEmiAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total payble amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="carLoanPayAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="carLoanTotalInt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                <strong>Suggestion:</strong>
                                <br> Keep your loan tenure as low as possible, as the tenure increases interest component increases significantly.
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Invest in SIP's to reach your car goal ".
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- EMI calculator for personal loan-->
<div class="modal fade" id="emiPerCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">EMI calculator for personal loan</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Loan amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="perLoanAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest rate(P.A.)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="perLoanIntRate" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Tenure (Yrs)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="perLoanNumYears" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="btn btn-primary" id="perLoanCheck" name="" placeholder=""
                            value="Check Personal Loan">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">EMI amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="perLoanEmiAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total amount payable</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="perLoanPayAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest component</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="perLoanTotalInt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                <strong>Suggestion:</strong>
                                <br> Keep your loan tenure as low as possible, as the tenure increases interest component increases significantly.
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Invest in SIP's for emergency fund with instant withdraw option".
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- PPF(Public Provident Fund) calculator-->
<div class="modal fade" id="ppfCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">PPF(Public Provident Fund) calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">PPF Interest Rate</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ppfIntRate" name="" placeholder="7.8 %" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ppfAmt" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">Per year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">No. of years</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ppfNumYear" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Investment made at</label>
                        <div class="col-sm-3">
                            <select name="" id="ppfRateType" class="form-control">
                                <option value="7.6">End of the period</option>
                                <option value="7.8">Begining of the period</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="form-control btn btn-primary" id="ppfCheck" name="" value="Check PPF">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total investment</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ppfTotalInvest" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total interest earned</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ppfTotalIntEarn" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total maturity amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ppfTotalMatAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                <strong>TAX saving:</strong>
                                <br> Under Sec 80C.
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " ELSS(Tax Saving MF) is also exempted like PPF and can generate better return then PPF "
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- EPF(Employee Provident Fund) calculator-->
<div class="modal fade" id="epfCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">EPF(Employee Provident Fund) calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <legend>Calculate maturity value</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Basic salary per month</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Your age</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Your retirement age</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Assuming</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-3" for="">% contribution</label>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Your contribution</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Employer's contribution (EPF+EPS)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total interest earned</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total maturity amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- NPS(National Pension Scheme) calculator-->
<div class="modal fade" id="npsCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">NPS(National Pension Scheme) calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Current age</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Retirement age</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-1" for="">year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total investing period</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-1" for="">year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">monthly contribution to be done</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Expected rate of return</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Principal amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest earned on investment</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Pension wealth generated</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- SSY(Sukanya Samriddhi Yojana) Calculator-->
<div class="modal fade" id="ssyCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">SSY(Sukanya Samriddhi Yojana) Calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ssyAmt" name="" placeholder="" required>
                        </div>
                        <div class="col-sm-3">
                            <select name="" id="ssyYear" class="form-control">
                                <option value="">Year</option>
                                <!-- <option value="">Month</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest rate</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ssyIntRate" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Investment started at year of</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ssyStartyear" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="form-control btn btn-primary" id="ssyCheck" name="" value="Check SSY">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Maturity year</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ssyMatYear" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total maturity amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ssyTotalAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Invest in top MF's for child education, child marriage and for bright future of child . "
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Browse MF'S</button>
                <button type="button" class="btn btn-default">Email</button>
            </div>
        </div>
    </div>
</div>
<!-- SWP(Systematic Withdrawal Plans) Calculator-->
<div class="modal fade" id="swpCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">SWP(Systematic Withdrawal Plans) Calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total Investment Amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Withdrawal per month</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Expected return</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Tenure (Yrs)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Value of the end of ** <sup>th</sup> year</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Invest Now</button>
                <button type="button" class="btn btn-default">Email</button>
            </div>
        </div>
    </div>
</div>
<!-- HRA calculator-->
<div class="modal fade" id="hraCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">HRA calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Basic salary received</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="hraBscSal" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Dearness Allowance(DA) Received</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="hraDA" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">HRA received</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="hraRec" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Actual Rent Paid</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="hraActRentPad" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Select city</label>
                        <div class="col-sm-3">
                            <select name="" id="hraCity" class="form-control">
                                <option value="dl">Delhi</option>
                                <option value="mu">Mumbai</option>
                                <option value="ko">Kolkata</option>
                                <option value="ch">Chennai</option>
                                <option value="oth">Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="form-control btn btn-primary" id="HRA_Check" name="" value="Check">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">HRA exemption</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="hraExempt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">HRA taxable</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="hrataxable" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Invest in Tax saving mutual funds for saving TAX . "
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Invest Now</button>
                <button type="button" class="btn btn-default">Email</button>
            </div>
        </div>
    </div>
</div>
<!-- SIP installment calculator -->
<div class="modal fade" id="sipInsCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">SIP installment calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount you want to achieve</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">With in Number of Years</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Risk undertaken</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="" name="" placeholder="" value="Low" readonly>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="" name="" placeholder="" value="Moderate" readonly>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="" name="" placeholder="" value="High" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Rate of return</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-9" for="">Monthly SIP Amount to be invested to reach your goal</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" class="btn btn-default">Invest Now</button>
                <button type="button" class="btn btn-default">Email</button>
            </div>
        </div>
    </div>
</div>
<!-- All calculators modal end here -->
            </div>
        </section>