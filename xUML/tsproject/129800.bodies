class PHPExcel_Style
!!!181000.php!!!	__construct(in isSupervisor : boolean = false)
    	// Supervisor?
		$this->_isSupervisor = $isSupervisor;

		// Initialise values
    	$this->_conditionalStyles 	= array();
		$this->_font				= new PHPExcel_Style_Font($isSupervisor);
		$this->_fill				= new PHPExcel_Style_Fill($isSupervisor);
		$this->_borders				= new PHPExcel_Style_Borders($isSupervisor);
		$this->_alignment			= new PHPExcel_Style_Alignment($isSupervisor);
		$this->_numberFormat		= new PHPExcel_Style_NumberFormat($isSupervisor);
		$this->_protection			= new PHPExcel_Style_Protection($isSupervisor);

		// bind parent if we are a supervisor
		if ($isSupervisor) {
			$this->_font->bindParent($this);
			$this->_fill->bindParent($this);
			$this->_borders->bindParent($this);
			$this->_alignment->bindParent($this);
			$this->_numberFormat->bindParent($this);
			$this->_protection->bindParent($this);
		}
!!!181128.php!!!	bindParent(in parent : PHPExcel) : PHPExcel_Style
		$this->_parent = $parent;
		return $this;
!!!181256.php!!!	getIsSupervisor() : boolean
		return $this->_isSupervisor;
!!!181384.php!!!	getSharedComponent() : PHPExcel_Style
		$activeSheet = $this->getActiveSheet();
		$selectedCell = $this->getActiveCell(); // e.g. 'A1'

		if ($activeSheet->cellExists($selectedCell)) {
			$cell = $activeSheet->getCell($selectedCell);
			$xfIndex = $cell->getXfIndex();
		} else {
			$xfIndex = 0;
		}

		$activeStyle = $this->_parent->getCellXfByIndex($xfIndex);
		return $activeStyle;
!!!181512.php!!!	getActiveSheet() : PHPExcel_Worksheet
		return $this->_parent->getActiveSheet();
!!!181640.php!!!	getSelectedCells() : string
		return $this->_parent->getActiveSheet()->getSelectedCells();
!!!181768.php!!!	getActiveCell() : string
		return $this->_parent->getActiveSheet()->getActiveCell();
!!!181896.php!!!	getParent() : PHPExcel
		return $this->_parent;
!!!182024.php!!!	applyFromArray(in pStyles : array = null, in pAdvanced : boolean = true) : PHPExcel_Style
		if (is_array($pStyles)) {
			if ($this->_isSupervisor) {

				$pRange = $this->getSelectedCells();

				// Uppercase coordinate
				$pRange = strtoupper($pRange);

				// Is it a cell range or a single cell?
				$rangeA 	= '';
				$rangeB 	= '';
				if (strpos($pRange, ':') === false) {
					$rangeA = $pRange;
					$rangeB = $pRange;
				} else {
					list($rangeA, $rangeB) = explode(':', $pRange);
				}

				// Calculate range outer borders
				$rangeStart = PHPExcel_Cell::coordinateFromString($rangeA);
				$rangeEnd 	= PHPExcel_Cell::coordinateFromString($rangeB);

				// Translate column into index
				$rangeStart[0]	= PHPExcel_Cell::columnIndexFromString($rangeStart[0]) - 1;
				$rangeEnd[0]	= PHPExcel_Cell::columnIndexFromString($rangeEnd[0]) - 1;

				// Make sure we can loop upwards on rows and columns
				if ($rangeStart[0] > $rangeEnd[0] && $rangeStart[1] > $rangeEnd[1]) {
					$tmp = $rangeStart;
					$rangeStart = $rangeEnd;
					$rangeEnd = $tmp;
				}

				// ADVANCED MODE:

				if ($pAdvanced && isset($pStyles['borders'])) {

					// 'allborders' is a shorthand property for 'outline' and 'inside' and
					//		it applies to components that have not been set explicitly
					if (isset($pStyles['borders']['allborders'])) {
						foreach (array('outline', 'inside') as $component) {
							if (!isset($pStyles['borders'][$component])) {
								$pStyles['borders'][$component] = $pStyles['borders']['allborders'];
							}
						}
						unset($pStyles['borders']['allborders']); // not needed any more
					}

					// 'outline' is a shorthand property for 'top', 'right', 'bottom', 'left'
					//		it applies to components that have not been set explicitly
					if (isset($pStyles['borders']['outline'])) {
						foreach (array('top', 'right', 'bottom', 'left') as $component) {
							if (!isset($pStyles['borders'][$component])) {
								$pStyles['borders'][$component] = $pStyles['borders']['outline'];
							}
						}
						unset($pStyles['borders']['outline']); // not needed any more
					}

					// 'inside' is a shorthand property for 'vertical' and 'horizontal'
					//		it applies to components that have not been set explicitly
					if (isset($pStyles['borders']['inside'])) {
						foreach (array('vertical', 'horizontal') as $component) {
							if (!isset($pStyles['borders'][$component])) {
								$pStyles['borders'][$component] = $pStyles['borders']['inside'];
							}
						}
						unset($pStyles['borders']['inside']); // not needed any more
					}

					// width and height characteristics of selection, 1, 2, or 3 (for 3 or more)
					$xMax = min($rangeEnd[0] - $rangeStart[0] + 1, 3);
					$yMax = min($rangeEnd[1] - $rangeStart[1] + 1, 3);

					// loop through up to 3 x 3 = 9 regions
					for ($x = 1; $x <= $xMax; ++$x) {
						// start column index for region
						$colStart = ($x == 3) ?
							PHPExcel_Cell::stringFromColumnIndex($rangeEnd[0])
								: PHPExcel_Cell::stringFromColumnIndex($rangeStart[0] + $x - 1);

						// end column index for region
						$colEnd = ($x == 1) ?
							PHPExcel_Cell::stringFromColumnIndex($rangeStart[0])
								: PHPExcel_Cell::stringFromColumnIndex($rangeEnd[0] - $xMax + $x);

						for ($y = 1; $y <= $yMax; ++$y) {

							// which edges are touching the region
							$edges = array();

							// are we at left edge
							if ($x == 1) {
								$edges[] = 'left';
							}

							// are we at right edge
							if ($x == $xMax) {
								$edges[] = 'right';
							}

							// are we at top edge?
							if ($y == 1) {
								$edges[] = 'top';
							}

							// are we at bottom edge?
							if ($y == $yMax) {
								$edges[] = 'bottom';
							}

							// start row index for region
							$rowStart = ($y == 3) ?
								$rangeEnd[1] : $rangeStart[1] + $y - 1;

							// end row index for region
							$rowEnd = ($y == 1) ?
								$rangeStart[1] : $rangeEnd[1] - $yMax + $y;

							// build range for region
							$range = $colStart . $rowStart . ':' . $colEnd . $rowEnd;

							// retrieve relevant style array for region
							$regionStyles = $pStyles;
							unset($regionStyles['borders']['inside']);

							// what are the inner edges of the region when looking at the selection
							$innerEdges = array_diff( array('top', 'right', 'bottom', 'left'), $edges );

							// inner edges that are not touching the region should take the 'inside' border properties if they have been set
							foreach ($innerEdges as $innerEdge) {
								switch ($innerEdge) {
									case 'top':
									case 'bottom':
										// should pick up 'horizontal' border property if set
										if (isset($pStyles['borders']['horizontal'])) {
											$regionStyles['borders'][$innerEdge] = $pStyles['borders']['horizontal'];
										} else {
											unset($regionStyles['borders'][$innerEdge]);
										}
										break;
									case 'left':
									case 'right':
										// should pick up 'vertical' border property if set
										if (isset($pStyles['borders']['vertical'])) {
											$regionStyles['borders'][$innerEdge] = $pStyles['borders']['vertical'];
										} else {
											unset($regionStyles['borders'][$innerEdge]);
										}
										break;
								}
							}

							// apply region style to region by calling applyFromArray() in simple mode
							$this->getActiveSheet()->getStyle($range)->applyFromArray($regionStyles, false);
						}
					}
					return;
				}

				// SIMPLE MODE:

				// Selection type, inspect
				if (preg_match('/^[A-Z]+1:[A-Z]+1048576$/', $pRange)) {
					$selectionType = 'COLUMN';
				} else if (preg_match('/^A[0-9]+:XFD[0-9]+$/', $pRange)) {
					$selectionType = 'ROW';
				} else {
					$selectionType = 'CELL';
				}

				// First loop through columns, rows, or cells to find out which styles are affected by this operation
				switch ($selectionType) {
					case 'COLUMN':
						$oldXfIndexes = array();
						for ($col = $rangeStart[0]; $col <= $rangeEnd[0]; ++$col) {
							$oldXfIndexes[$this->getActiveSheet()->getColumnDimensionByColumn($col)->getXfIndex()] = true;
						}
						break;

					case 'ROW':
						$oldXfIndexes = array();
						for ($row = $rangeStart[1]; $row <= $rangeEnd[1]; ++$row) {
							if ($this->getActiveSheet()->getRowDimension($row)->getXfIndex() == null) {
								$oldXfIndexes[0] = true; // row without explicit style should be formatted based on default style
							} else {
								$oldXfIndexes[$this->getActiveSheet()->getRowDimension($row)->getXfIndex()] = true;
							}
						}
						break;

					case 'CELL':
						$oldXfIndexes = array();
						for ($col = $rangeStart[0]; $col <= $rangeEnd[0]; ++$col) {
							for ($row = $rangeStart[1]; $row <= $rangeEnd[1]; ++$row) {
								$oldXfIndexes[$this->getActiveSheet()->getCellByColumnAndRow($col, $row)->getXfIndex()] = true;
							}
						}
						break;
				}

				// clone each of the affected styles, apply the style arrray, and add the new styles to the workbook
				$workbook = $this->getActiveSheet()->getParent();
				foreach ($oldXfIndexes as $oldXfIndex => $dummy) {
					$style = $workbook->getCellXfByIndex($oldXfIndex);
					$newStyle = clone $style;
					$newStyle->applyFromArray($pStyles);

					if ($existingStyle = $workbook->getCellXfByHashCode($newStyle->getHashCode())) {
						// there is already such cell Xf in our collection
						$newXfIndexes[$oldXfIndex] = $existingStyle->getIndex();
					} else {
						// we don't have such a cell Xf, need to add
						$workbook->addCellXf($newStyle);
						$newXfIndexes[$oldXfIndex] = $newStyle->getIndex();
					}
				}

				// Loop through columns, rows, or cells again and update the XF index
				switch ($selectionType) {
					case 'COLUMN':
						for ($col = $rangeStart[0]; $col <= $rangeEnd[0]; ++$col) {
							$columnDimension = $this->getActiveSheet()->getColumnDimensionByColumn($col);
							$oldXfIndex = $columnDimension->getXfIndex();
							$columnDimension->setXfIndex($newXfIndexes[$oldXfIndex]);
						}
						break;

					case 'ROW':
						for ($row = $rangeStart[1]; $row <= $rangeEnd[1]; ++$row) {
							$rowDimension = $this->getActiveSheet()->getRowDimension($row);
							$oldXfIndex = $rowDimension->getXfIndex() === null ?
								0 : $rowDimension->getXfIndex(); // row without explicit style should be formatted based on default style
							$rowDimension->setXfIndex($newXfIndexes[$oldXfIndex]);
						}
						break;

					case 'CELL':
						for ($col = $rangeStart[0]; $col <= $rangeEnd[0]; ++$col) {
							for ($row = $rangeStart[1]; $row <= $rangeEnd[1]; ++$row) {
								$cell = $this->getActiveSheet()->getCellByColumnAndRow($col, $row);
								$oldXfIndex = $cell->getXfIndex();
								$cell->setXfIndex($newXfIndexes[$oldXfIndex]);
							}
						}
						break;
				}

			} else {
				// not a supervisor, just apply the style array directly on style object
				if (array_key_exists('fill', $pStyles)) {
					$this->getFill()->applyFromArray($pStyles['fill']);
				}
				if (array_key_exists('font', $pStyles)) {
					$this->getFont()->applyFromArray($pStyles['font']);
				}
				if (array_key_exists('borders', $pStyles)) {
					$this->getBorders()->applyFromArray($pStyles['borders']);
				}
				if (array_key_exists('alignment', $pStyles)) {
					$this->getAlignment()->applyFromArray($pStyles['alignment']);
				}
				if (array_key_exists('numberformat', $pStyles)) {
					$this->getNumberFormat()->applyFromArray($pStyles['numberformat']);
				}
				if (array_key_exists('protection', $pStyles)) {
					$this->getProtection()->applyFromArray($pStyles['protection']);
				}
			}
		} else {
			throw new Exception("Invalid style array passed.");
		}
		return $this;
!!!182152.php!!!	getFill() : PHPExcel_Style_Fill
		return $this->_fill;
!!!182280.php!!!	getFont() : PHPExcel_Style_Font
		return $this->_font;
!!!182408.php!!!	setFont(in font : PHPExcel_Style_Font) : PHPExcel_Style
		$this->_font = $font;
		return $this;
!!!182536.php!!!	getBorders() : PHPExcel_Style_Borders
		return $this->_borders;
!!!182664.php!!!	getAlignment() : PHPExcel_Style_Alignment
		return $this->_alignment;
!!!182792.php!!!	getNumberFormat() : PHPExcel_Style_NumberFormat
		return $this->_numberFormat;
!!!182920.php!!!	getConditionalStyles() : PHPExcel_Style_Conditional[]
		return $this->getActiveSheet()->getConditionalStyles($this->getActiveCell());
!!!183048.php!!!	setConditionalStyles(in pValue : PHPExcel_Style_Conditional[] = null) : PHPExcel_Style
		if (is_array($pValue)) {
			foreach (PHPExcel_Cell::extractAllCellReferencesInRange($this->getSelectedCells()) as $cellReference) {
				$this->getActiveSheet()->setConditionalStyles($cellReference, $pValue);
			}
		}
		return $this;
!!!183176.php!!!	getProtection() : PHPExcel_Style_Protection
		return $this->_protection;
!!!183304.php!!!	getHashCode() : string
		$hashConditionals = '';
		foreach ($this->_conditionalStyles as $conditional) {
			$hashConditionals .= $conditional->getHashCode();
		}

    	return md5(
    		  $this->getFill()->getHashCode()
    		. $this->getFont()->getHashCode()
    		. $this->getBorders()->getHashCode()
    		. $this->getAlignment()->getHashCode()
    		. $this->getNumberFormat()->getHashCode()
    		. $hashConditionals
    		. $this->getProtection()->getHashCode()
    		. __CLASS__
    	);
!!!183432.php!!!	getIndex() : int
		return $this->_index;
!!!183560.php!!!	setIndex(in pValue : int)
		$this->_index = $pValue;
!!!183688.php!!!	__clone()
		$vars = get_object_vars($this);
		foreach ($vars as $key => $value) {
			if ((is_object($value)) && ($key != '_parent')) {
				$this->$key = clone $value;
			} else {
				$this->$key = $value;
			}
		}
