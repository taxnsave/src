
var WSExtensionConfig = {
	//	Invoke Protocol
	getInvokeProtocol : function() {
		return "kvpro://" ;	
	},
	
	//	Download Button Position
	getDownloadButtonPosition : function () {
		return AttachStyleEnum.AttachOuterTopRight ;
	},
};
