/*global jQuery:false */
jQuery(document).ready(function ($) {
    "use strict";
    //add some elements with custom JS and AJAX effect 
	
	function financial(x) {
		return Number.parseFloat(x).toFixed(2);
	}
		
	function totalOfSchemeAmount(thisObj){
		var calculated_total_sum = 0;
		thisObj.parents('.calculateGoal').find(".tbl_posts_body tr.active .sipamount").each(function () {
			var get_textbox_value = $(this).val();
			if ($.isNumeric(get_textbox_value)) {
				calculated_total_sum += parseFloat(get_textbox_value);
				}                  
            });
		thisObj.parents('.calculateGoal').find(".tbl_posts_foot .schemeTotal").html(financial(calculated_total_sum));	
		// console.log(calculated_total_sum);
        return true;
	}

	function totalOfSchemeAmount1(tableID){
		var calculated_total_sum = 0;
		$("#tbl_posts_body tr.active .sipamount").each(function () {
			var get_textbox_value = $(this).val();
			if ($.isNumeric(get_textbox_value)) {
				calculated_total_sum += parseFloat(get_textbox_value);
				}                  
            });
		$("#tbl_posts_foot .schemeTotal").html(financial(calculated_total_sum));	
		// console.log(calculated_total_sum);
        return true;
	}	
	
	$('select#buyACarRiskTaken,#buyACarSipAmt,#buyACarRiskTaken2').on('change', function() {
		//alert( this.value );
		var riskType ;
		var riskPercentage = $('#buyACarIntRate2').val();
		if(riskPercentage == null && riskPercentage == '') {
			riskType = $('select#buyACarRiskTaken').val();
		}else{
			riskType = riskPercentage;
		}	
		
		$('#buyACarRiskTaken2').val($('select#buyACarRiskTaken').val());
		//alert(riskType);
		//$("#buyACarRiskTaken2 option[value="+ riskType +"]").attr('selected', 'selected');
		var sipTotalAmount = $("#buyACarSipAmt").val();
		// console.log(sipTotalAmount);
		// console.log(riskType);
		getMutualFundShemes(riskType , sipTotalAmount);
		$('button#moreScheme.btn-success').prop('disabled', false);
		
	});
	//$('select#buyACarRiskTaken,#buyACarSipAmt,#buyACarRiskTaken2').trigger("change");

	// $(document).on('click', '#investNow', function(e) {
		// $('select#buyACarRiskTaken,#buyACarSipAmt,#buyACarRiskTaken2').trigger("change");
	// });
	
	$(document).on('click', '.investNow', function(e) {
		var schemeList ='';
		var riskType = 0;
		var riskPercentage = $(this).parents('.calculateGoal').find('.riskRateT').val();
		if(riskPercentage == null && riskPercentage == '') {
			riskType = $(this).parents('.calculateGoal').find('select.riskRateS').val();
		}else{
			riskType = riskPercentage;
		}			
		var sipTotalAmount = $(this).parents('.calculateGoal').find(".mySIPAmount").val();
		getMutualFundShemes($(this), riskType , sipTotalAmount);

	});
	
	$('button.startInvestment').click(function(){
		var sipamount = $(this).parents('.calculateGoal').find('input.mySIPAmount').val();
		placeSIPOrderBSE(sipamount);
	});	
	
	$(document).on('keyup mouseup', 'input.sipamount', function(e) {
		var amount = parseInt ($(this).val());
		var minVal = $(this).attr('min');
		if(amount < minVal){
			$(this).val(minVal);
			$(this).after('<span class="errorAmount">Min amount = '+minVal+'</span>');
			setTimeout(function(){$('.errorAmount').remove();}, 2000);
		}
		totalOfSchemeAmount($(this));	
	});
	
	
	$(document).on('click', 'a.delete-scheme', function(e) {
		
		var mainBlock = $(this).parents('.calculateGoal');
		e.preventDefault();   
		
		
		var didConfirm = confirm("Are you sure You want to remove?");
		if (didConfirm == true) {
			
			var id = $(this).attr('data-id');
			$(this).parents('.tbl_posts_body').find('tr.rec-' + id).remove();
			
			var sipTotalAmount = mainBlock.find(".mySIPAmount").val();	// $("#buyACarSipAmt").val();
			var schemeCount = mainBlock.find('.tbl_posts_body tr.active').length;
			var newAmount = Math.round( sipTotalAmount / schemeCount ) ; 
			//console.log ( " sipTotalAmount " + sipTotalAmount + " schemeCount " + schemeCount + " newAmount " + newAmount );
			
			//regnerate index number on table
			mainBlock.find('.tbl_posts_body tr').each(function(index){
				$(this).find('span.sn').html(index+1);;
			});
			//reallocate amout to mutual fund
			mainBlock.find('.tbl_posts_body tr.active').each(function(index){
				$(this).find('input.sipamount').val(newAmount);
			});
			totalOfSchemeAmount(mainBlock.find('.tbl_posts_body'));
			return true;
		} else {
			return false;
		} 
			
	});
	$(document).on('click', 'button.moreScheme.btn-success', function(e) {
		e.preventDefault();
		$(this).parents('.calculateGoal').find('.tbl_posts_body tr').each(function(index){
			$(this).show().addClass('active');
		});
		totalOfSchemeAmount($(this));
		$(this).prop('disabled', true);
		//alert($(this).parents('.calculateGoal').attr('id'));
	});	
	// $(document).on('click', 'button#moreScheme.btn-success', function(e) {
		// e.preventDefault();
		// $('#tbl_posts_body tr').each(function(index){
			// $(this).show().addClass('active');
		// });
		// totalOfSchemeAmount1();
		// $(this).prop('disabled', true);
	// });	
	
});