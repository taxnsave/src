// funnyordie.com
var WSCommonAnalyster = {};

(function() {
	var videoURL;
	var downloadBtnDiv;
	//请求,网页请求
    var xhr = new XMLHttpRequest();
    var facebookDom;
    var isRequesting;
    var requestHtml; 

// 针 对 Vimeo 网 站
	function htmlFive(element) {
		var videoURL = document.location.href;
		var url = videoURL;
		if(url.indexOf("vimeo.com") != -1 && isHTML5 == true)
		{
			vplayerDiv = element.parentNode.parentNode.parentNode;
			var encodeData = "pageUrl=" + videoURL + "-WS-GUES-cookies=" + document.cookie + "-WS-GUES-browser=" + WSCore.getBrowserIndent();
			vbtnDownload = WSCore.attachButtonDiv(vplayerDiv, WSExtensionConfig.getDownloadButtonPosition(), "", WSExtensionConfig.getInvokeProtocol() + WSCore.base64Encode(encodeData));
			WSCore.bind_mouseover(vplayerDiv, vbtnDownload);
		}	      
    }

// 针 对 lynda 网 站
    function islynda(event)
    {
        var videoURL = document.location.href;
        if (videoURL.indexOf("lynda.com") == -1) return false;
	    if (WSCore.getBrowserIndent() != "firefox") 
	        return (event.target instanceof HTMLDivElement) && (event.target.getAttribute("class").indexOf("ToggleOverlay") != -1);
		else
		    return (event.target.toString().indexOf("HTMLDivElement") != -1) && (event.target.getAttribute("class").indexOf("ToggleOverlay") != -1);
    }

    var currentUrl = null;
	function handleMouseoverEvent(event) {
		if (location.href.indexOf("facebook.com")>=0) {
            if(event.target.toString().indexOf('HTMLIFrameElement') >= 0){
                //keepvidHref
                var savedUrl = event.target.parentNode.parentNode.getAttribute('keepvidHref');
                var downloadUrlId = event.target.parentNode.parentNode.getAttribute('downloadButtonId');
                currentUrl = savedUrl;
                //如果下载按钮已经创建过的，直接显示
                if(savedUrl !==null && savedUrl !== undefined && downloadUrlId!==null && downloadUrlId!==undefined){
                    //alert($(event.target).parent()[0].id);
                    var downloadButton = document.getElementById(downloadUrlId);
                    downloadButton.style.visibility = "visible";
                    downloadButton.style.left = event.target.parentNode.parentNode.getAttribute('downloadButtonleft');
                    downloadButton.style.top = event.target.parentNode.parentNode.getAttribute('downloadButtontop');
                    return;
                }
            }
            if(event.target.toString().indexOf('HTMLVideoElement') >= 0){
                var destUrl = event.target.parentNode.parentNode.getAttribute('keepvidHref');
                var video_id = event.target.parentNode.parentNode.getAttribute('video_id');
                if(destUrl!=null && destUrl!= undefined){
                    var downloadUrlId = 'FaceBookVideo_' + video_id;
                    if(downloadUrlId!==null && downloadUrlId!==undefined){   
                        document.getElementById(downloadUrlId).style.visibility = 'visible';                   
                    }
                    else{
                        var buttonLink = "pageUrl=" + destURL + "-WS-GUES-browser=" + WSCore.getBrowserIndent() + "-WS-GUES-directedDownload=" + document.domain;
                        var downloadDiv = WSCore.attachButtonDiv(event.target, AttachStyleEnum.AttachOuterTopRight, "", WSExtensionConfig.getInvokeProtocol() + WSCore.base64Encode(buttonLink), undefined,undefined);
                        WSCore.bind_mouseover(event.target, downloadDiv);
                    }
                    return;
                }
                else{
                    facebook(event.target);
                }
            }
            if((event.target.tagName=="DIV" || event.target.tagName=="A") && location.href.indexOf("facebook.com")>=0){
                facebookOutLink(event.target);
            }

            return;
        }

		var validTarget = false ;
		if (WSCore.getBrowserIndent() != "firefox") {	//Chrome & Safari
			validTarget = event.target instanceof HTMLEmbedElement || event.target instanceof HTMLObjectElement || event.target instanceof HTMLIFrameElement || event.target instanceof HTMLVideoElement || islynda(event);
		}
		else {
			// try {
			// 	validTarget = event.originalTarget instanceof HTMLEmbedElement || event.originalTarget instanceof HTMLObjectElement || event.originalTarget instanceof HTMLIFrameElement || event.originalTarget instanceof HTMLVideoElement ;
			// }
			// catch(ex) {
				validTarget = (event.target.toString().indexOf("HTMLEmbedElement") != -1) || (event.target.toString().indexOf("HTMLObjectElement") != -1) || (event.target.toString().indexOf("HTMLIFrameElement") != -1) || (event.target.toString().indexOf("HTMLVideoElement") != -1) || islynda(event);
			//}
		}
			
		if (validTarget) {
       		// 233/1397 的 比 例 是 为 了 过 滤 掉 veoh 网 站 的 一 个 高 宽 比 为233/1397 的 flash
			var flash = WSCore.targetObject(event);
			videoURL = document.location.href;
			var condition = false;
			if (videoURL.indexOf("facebook.com") != -1)
				condition = flash.offsetHeight > 180 && flash.offsetWidth > 160 && (flash.offsetHeight / flash.offsetWidth > 255 / 960);
			else 
				condition = flash.offsetHeight > 180 && flash.offsetWidth > 160 && (flash.offsetHeight / flash.offsetWidth > 255 / 960 && flash.offsetHeight / flash.offsetWidth < 1.2)
            if (condition)
			{
            	if (downloadBtnDiv) {
					WSCore.unbind_mouseover(playerDiv111, downloadBtnDiv);
                	WSCore.deleteAttachedButton(downloadBtnDiv);
					playerDiv111 = null;
					downloadBtnDiv = null;
				}

				var invokeURL = "pageUrl=" + videoURL ;
				//if(videoURL.indexOf("nicovideo.jp") != -1)
				{
					invokeURL += "-WS-GUES-";
					invokeURL += "cookies=" + document.cookie ;
					invokeURL += "-WS-GUES-browser=" + WSCore.getBrowserIndent();
				}
				//console.log(invokeURL);
				downloadBtnDiv = WSCore.attachButtonDiv(WSCore.targetObject(event), WSExtensionConfig.getDownloadButtonPosition(), "", WSExtensionConfig.getInvokeProtocol() + WSCore.base64Encode(invokeURL));
				playerDiv111 = WSCore.targetObject(event).parentNode;
				if (WSCore.getBrowserIndent() != "firefox") {
					if (playerDiv111 instanceof HTMLObjectElement || playerDiv111 instanceof HTMLEmbedElement) {
						playerDiv111 = playerDiv111.parentNode;
					}
				}else{
					if ((playerDiv111.toString().indexOf("HTMLObjectElement")!= -1) || (playerDiv111.toString().indexOf("HTMLEmbedElement") != -1)) {
						playerDiv111 = playerDiv111.parentNode;
					}
				}
				WSCore.bind_mouseover(playerDiv111, downloadBtnDiv);
				WSCore.showElement(downloadBtnDiv);
			}
		}
    }

    function facebook(targetDom){

        if(isRequesting) return;
        var destURL;
        facebookDom = targetDom;
        //这里要获取地址
        var parentNode = targetDom.parentNode;
        var currentVideoDiv;
        while (parentNode) {
        	if ((parentNode.getAttribute('class') == null || parentNode.getAttribute('class') == undefined)
        		&& (parentNode.getAttribute('data-ft') == null || parentNode.getAttribute('data-ft') == undefined)) {
        		currentVideoDiv = parentNode;
        		break;
        	};
            parentNode = parentNode.parentNode;
        }
        var spanList = currentVideoDiv.getElementsByTagName('span');
        var spanDom;
        for(var i=0;i<spanList.length;i++){
            if(spanList[i].getAttribute('class')=='timestampContent'){
                spanDom = spanList[i];
                break;
            }
        }

        var videoUrl = 'https://www.facebook.com' + spanDom.parentNode.parentNode.getAttribute('href');
        var checkCurrentResult = false;
        if(requestHtml!= undefined && requestHtml!= null){
            checkCurrentResult = facebookInnerLinkCheck(requestHtml,videoUrl);
        }
        if(checkCurrentResult== false){
            RequestFaceBookDownloadUrl(videoUrl);
        }
        //只接受facebook的网站的东西
        //if(location.href.indexOf("facebook.com")>=0){
        //    //这里要区分两类，第一类是直接从页面上能找到的，找不到的时候再去保存链接里面找，再找不到就没办法了
        //  //1找到当前Video标签的父节点中包含有data-testid="fbfeed_story"属性的div
        //  var hyperfeedId;
        //  var divList = $(targetDom).parents();
        //  for(var i=0;i<divList.length;i++){
        //      if($(divList[i]).attr('data-referrer')==$(divList[i]).attr('id') && $(divList[i]).attr('data-referrer')!= undefined && $(divList[i]).attr('data-referrer')!= null){
        //          hyperfeedId = $(divList[i]).attr('id');
        //          break;
        //      }
        //  }
        //    if(hyperfeedId!=null && hyperfeedId!= undefined){
        //      var marksScriptIndex;
        //      //遍历所有script
        //      for(var i=0;i<document.scripts.length;i++){
        //          if(document.scripts[i].innerHTML.indexOf(hyperfeedId)>=0){
        //              marksScriptIndex = i;
        //              break;
        //          }
        //      }
        //      var sourceUrlScript= document.scripts[marksScriptIndex + 1].innerHTML;
        //      if(sourceUrlScript.indexOf('"'+$(targetDom).attr('id')+'"')<0){
        //          return;
        //      }
        //        var pattern = new RegExp('sd_src_no_ratelimit:".*?",','gi');  //匹配单个script标签内容的写法
        //      var matchArr = pattern.exec(sourceUrlScript);
        //      if(matchArr!=null && matchArr.length>0){
        //          destURL= matchArr[0].replace('sd_src_no_ratelimit:"','').replace('",','');
        //      }
        //      if(destURL==null ||destURL == undefined){
        //          pattern = new RegExp('hd_src_no_ratelimit:".*?",','gi');
        //          var matchArr = pattern.exec(sourceUrlScript);
        //          if(matchArr!=null && matchArr.length>0){
        //              destURL = matchArr[0].replace('hd_src_no_ratelimit:"','').replace('",','');
        //          }
        //      }
        //      if(destURL==null ||destURL == undefined){
        //          pattern = new RegExp('hd_src:".*?",','gi');
        //          var matchArr = pattern.exec(sourceUrlScript);
        //          if(matchArr!=null && matchArr.length>0){
        //              destURL = matchArr[0].replace('hd_src:"','').replace('",','');
        //          }
        //      }
        //      if(destURL==null ||destURL == undefined){
        //          pattern = new RegExp('sd_src:".*?",','gi');
        //          var matchArr = pattern.exec(sourceUrlScript);
        //          if(matchArr!=null && matchArr.length>0){
        //              destURL = matchArr[0].replace('sd_src:"','').replace('",','');
        //          }
        //      }
        //      if(destURL==null ||destURL == undefined){
        //          destURL = location.href;
        //      }
        //        //alert("添加事件");出来的链接直接传递到按钮，跳转，Keepvid网站可能需要修改来检测下载资源。
        //        //客户端同步需要能够直接接受http://www.facebook.com/video/wanglihong.mp4?参数1=0&参数2=0这种地址
        //        var downloadDiv = keepvid.attachButtonDiv(targetDom, AttachStyleEnum.AttachOuterTopRight, "", destURL,undefined,undefined,'FaceBookVideo_' + $(targetDom).attr('id'));
        //        keepvid.bindEvent(targetDom, downloadDiv);
        //  }
        //}
    }
    
    function RequestFaceBookDownloadUrl(currentUrl){
        if (xhr) {
            //xhr.overrideMimeType("text/html");
            xhr.open('GET', currentUrl, true);
            xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
            xhr.setRequestHeader("Accept","*/*");
            xhr.setRequestHeader("Accept-Language","zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
            xhr.onreadystatechange = handler;
            xhr.send();
            isRequesting = true;
        }
        else {
            //如果不行，就要找别的办法了。
            //document.getElementById("content").innerHTML = "can not create XMLHttpRequest";
        }
    }

    function handler(evtXHR){
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                var response = xhr.responseText;
                requestHtml = xhr.responseText;
                if(xhr.responseURL.indexOf('facebook.com')>=0){
                    facebookInnerLinkCheck(requestHtml,xhr.responseURL);
                }
            } 
            else {
                console.log('can not cross domain');
                //document.getElementById("content").innerHTML = "can not cross domain";
            }
            isRequesting = false;
        }
        else {
            console.log(xhr.readyState);
            //document.getElementById("content").innerHTML += "<br/>exceute status readyState" + xhr.readyState;
        }
    }

    function facebookInnerLinkCheck(response,currentUrl){
        //拿着返回值去解析，从里面找出progressive,这里面有各种信息(url,quality,id等)
        //AnalysisVimeoDownloadLink(response);
        //从response里面找video_id找出下载地址。
        //var parten = new RegExp('/videos\/(\d+)?','gi');
        try{
            var video_id;
            var matcharr = currentUrl.match(/videos\/(\S*)/);
            if(matcharr.length<2){
                return false;
            }
            video_id = matcharr[1].replace('/','');
            var downloadUrlParten = new RegExp('"videoData":\\[{"is_hds":(false|true),"video_id":"' + video_id + '",.*,"sd_src_no_ratelimit":.*?,"sd_src":.*?,"hd_tag":','gi');
            var downloadJsonArr = response.match(downloadUrlParten);
            var downloadJson;
            if(downloadJsonArr==null || downloadJsonArr.length<0){
                //他们返回有两种不同格式的Json
                downloadUrlParten= new RegExp('videoData:\\[{is_hds:(false|true),video_id:"' + video_id + '",.*,sd_src_no_ratelimit:.*?,sd_src:.*?,hd_tag:','gi');
                downloadJsonArr = response.match(downloadUrlParten);
                downloadJson = downloadJsonArr[0];
                downloadJson = downloadJson.replace(/:/g,'":');
                downloadJson = downloadJson.replace(/https":/g,'https:');
                downloadJson = downloadJson.replace(/,/g,',"');
                downloadJson = '"' + downloadJson;
                downloadJson = downloadJson.replace('"videoData":[{','');
                downloadJson = '{"' + downloadJson;
                downloadJson = downloadJson.replace(',"hd_tag":','}');
                downloadJson = downloadJson.substr(0,downloadJson.indexOf('}')+1);
            }
            else{
                downloadJson = downloadJsonArr[0];
                //去头去尾
                downloadJson = downloadJson.replace('"videoData":[','');
                downloadJson = downloadJson.replace(',"hd_tag":','}');
                downloadJson = downloadJson.substr(0,downloadJson.indexOf('}') + 1);
                //downloadJson = downloadJson.substr(0,downloadJson.indexOf('}"sve_hd"') + 1);
            }
            if(downloadJsonArr==null || downloadJsonArr.length<0){
                RequestFaceBookDownloadUrl(currentUrl);
                return false;
            }
            var json = JSON.parse(downloadJson);
            //获取下载地址
            var destUrl = json.hd_src_no_ratelimit;
            if(destUrl==undefined || destUrl== null){
                destUrl = json.sd_src_no_ratelimit;
            }
            if(destUrl==undefined || destUrl== null){
                destUrl = json.hd_src;
            }
            if(destUrl==undefined || destUrl== null){
                destUrl = json.sd_src;
            }
            if(destUrl==undefined || destUrl== null){
                destUrl = location.href;
            }
            //保存到facebookDom的parent的parent节点
            facebookDom.parentNode.parentNode.setAttribute('keepvidHref',destUrl);
            facebookDom.parentNode.parentNode.setAttribute('video_id',video_id);
            var buttonLink = "pageUrl=" + destUrl + "-WS-GUES-browser=" + WSCore.getBrowserIndent() + "-WS-GUES-directedDownload=" + document.domain;    
            buttonLink = WSExtensionConfig.getInvokeProtocol() + WSCore.base64Encode(buttonLink);
            var downloadDiv = WSCore.attachButtonDiv(facebookDom, AttachStyleEnum.AttachOuterTopRight, "", buttonLink,undefined,undefined);
            WSCore.bind_mouseover(facebookDom, downloadDiv);
        }
        catch(err){
            return false;
        }
        return true;
    }

    function facebookOutLink(targetDom){
        var viemoLinkUrl;
        var videoImage = targetDom.getElementsByTagName('img');
        var createDivId;
        
        //alert($(targetDom).attr('id'));
        //这是针对youtube的分析
        if(targetDom.tagName=="A")
        {
            //alert("isComming");
            var youtubeAElement = targetDom.getElementsByTagName('img');
            var youtubeUrl = targetDom.getAttribute('href');
            var youtubeAjaxify = targetDom.getAttribute('ajaxify');
            if(youtubeAElement !== undefined && youtubeAElement.length > 0 && youtubeAjaxify !== undefined && youtubeAjaxify.indexOf('/ajax/flash/expand_inline.php')>=0){
                var currentUrl = decodeURIComponent(youtubeUrl);
                if(currentUrl.indexOf("youtube.com")>=0){
                    currentUrl = currentUrl.substr(currentUrl.indexOf('u=/')+3);
                    youtubeUrl= 'https://www.youtube.com/'+currentUrl;
                    if (targetDom.parentNode.parentNode != null && targetDom.parentNode.parentNode != undefined) {
                        //将分析出来的url和我们创建的div的id保存，后面检查到iframe时直接根据id去展示按钮
                        targetDom.parentNode.parentNode.setAttribute('keepvidHref',youtubeUrl);
                        createDivId = 'downloadUrl_'+ targetDom.parentNode.id;
                        targetDom.parentNode.parentNode.setAttribute('downloadButtonId',createDivId);
                    }
                }
                //这是针对其他网站你的分析，url带有video的就认为是视频
                else if(currentUrl.indexOf("/video") >= 0 || currentUrl.indexOf("vine.co")){

                    targetDom.parentNode.parentNode.setAttribute('keepvidHref',youtubeUrl);
                    createDivId = 'downloadUrl_'+ targetDom.parentNode.id;
                    targetDom.parentNode.parentNode.setAttribute('downloadButtonId',createDivId);
                }
                else{
                    return;
                }
                //如果是CNN网站的
                //else if(currentUrl.indexOf("www.cnn.com/video")>=0){
                //    if($(targetDom).parent().parent()[0] !== null &&  $(targetDom).parent().parent()[0]!== undefined){
                //        $($(targetDom).parent().parent()[0]).attr('keepvidHref',youtubeUrl);
                //        createDivId = 'downloadUrl_'+ $(targetDom).parent()[0].id;
                //        $($(targetDom).parent().parent()[0]).attr('downloadButtonId',createDivId);
                //    }
                //}
                //alert("添加事件");出来的链接直接传递到按钮，跳转，Keepvid网站可能需要修改来检测下载资源。
                //客户端同步需要能够直接接受http://www.facebook.com/video/wanglihong.mp4?参数1=0&参数2=0这种地址
                
                var buttonLink = "pageUrl=" + decodeURIComponent(youtubeUrl) + "-WS-GUES-browser=" + WSCore.getBrowserIndent();
                buttonLink = WSExtensionConfig.getInvokeProtocol() + WSCore.base64Encode(buttonLink);
                var downloadDiv = WSCore.attachButtonDiv(targetDom, AttachStyleEnum.AttachOuterTopRight, "", buttonLink,createDivId);
                WSCore.unbind_mouseover(targetDom, downloadDiv);
                //计算按钮显示位置，facebook分享的youtube有点不一样
                var width = targetDom.parentNode.parentNode.parentNode.parentNode.parentNode.offsetWidth;
                
                var obj = targetDom;
                var left = obj.offsetLeft;
                while (obj = obj.offsetParent) {
                    left += obj.offsetLeft;
                }
                
                left =left + width - downloadDiv.offsetWidth;
                downloadDiv.style.left = left + 'px';
                targetDom.parentNode.parentNode.setAttribute('downloadButtonleft', downloadDiv.style.left);
                targetDom.parentNode.parentNode.setAttribute('downloadButtontop', downloadDiv.style.top);
                
                WSCore.bind_mouseover(targetDom, downloadDiv);
            }
        }
        //这是其他网站的分析
        else{
            if(location.href.indexOf("facebook.com")>=0 && videoImage !== null && videoImage !== undefined && videoImage.length>0){
                var aLink = targetDom.parentNode;
                var href = aLink.getAttribute('href');
                var ajaxify = aLink.getAttribute('ajaxify');
                if(ajaxify !== undefined && ajaxify.indexOf('/ajax/flash/expand_inline.php')>=0){
                    viemoLinkUrl = href;
                    //console.log($(targetDom).parent().parent()[0].id);
                    if( targetDom.parentNode.parentNode.parentNode !== null &&   targetDom.parentNode.parentNode.parentNode !== undefined){
                        //将分析出来的url和我们创建的div的id保存，后面检查到iframe时直接根据id去展示按钮
                        targetDom.parentNode.parentNode.parentNode.setAttribute('keepvidHref',viemoLinkUrl);
                        createDivId = 'downloadUrl_'+ targetDom.parentNode.parentNode.id;
                        targetDom.parentNode.parentNode.parentNode.setAttribute('downloadButtonId',createDivId);
                    }
                    //alert("添加事件");出来的链接直接传递到按钮，跳转，Keepvid网站可能需要修改来检测下载资源。
                    //客户端同步需要能够直接接受http://www.facebook.com/video/wanglihong.mp4?参数1=0&参数2=0这种地址
                    var buttonLink = "pageUrl=" + viemoLinkUrl + "-WS-GUES-browser=" + WSCore.getBrowserIndent();
                    buttonLink = WSExtensionConfig.getInvokeProtocol() + WSCore.base64Encode(buttonLink);
                    var otherLinkDownloadDiv = WSCore.attachButtonDiv(targetDom, AttachStyleEnum.AttachOuterTopRight, "", buttonLink,createDivId);
                    targetDom.parentNode.parentNode.parentNode.setAttribute('downloadButtonleft', otherLinkDownloadDiv.style.left);
                    targetDom.parentNode.parentNode.parentNode.setAttribute('downloadButtontop', otherLinkDownloadDiv.style.top);
                    WSCore.bind_mouseover(targetDom, otherLinkDownloadDiv);
                }
                else if((ajaxify === undefined || ajaxify===null) && href !== undefined && (href.indexOf('soundcloud.com')>=0 || href.indexOf('mixcloud.com')>=0 || href.indexOf('/video')>0)){
                    var buttonLink = "pageUrl=" + href + "-WS-GUES-browser=" + WSCore.getBrowserIndent();
                    buttonLink = WSExtensionConfig.getInvokeProtocol() + WSCore.base64Encode(buttonLink);
                    var musicLinkDownloadDiv = WSCore.attachButtonDiv(targetDom, AttachStyleEnum.AttachOuterTopRight, "", buttonLink);
                    WSCore.bind_mouseover(targetDom, musicLinkDownloadDiv);
                }
            }
        }
    }
    
    function handleBeforeLoadEvent(event)
    {
	    element = WSCore.targetObject(event);
		try {
			if (isHTML5 == false)
				isHTML5 = (element.toString().indexOf("HTMLVideoElement")!= -1) ;
		}
		catch(ex) {
		}
	    htmlFive(element);
    }

    var url = document.location.href;
    var isHTML5 = false;
	
    if(url.indexOf("vimeo.com") != -1)
    {
        document.addEventListener("beforeload", handleBeforeLoadEvent, true); 
        document.addEventListener("mouseover", handleMouseoverEvent, false);
    }
    else
    {
        document.addEventListener("mouseover", handleMouseoverEvent, false);
    }

})();