<?php 

	header("Access-Control-Allow-Origin: *");
	include("__lib.includes/config.inc.php"); 
	
	$_SESSION['oPageAccess'] = 1;

	$pagename = strtolower($_GET[page]);
    
//echo $pagename;
if ($pagename == "") {
		$pagename = "home";
	}
	$_SESSION[$CONFIG->sessionPrefix.'page_name'] = $pagename;
	//print_r($_SESSION);
	//echo $htmlPageSource;		
	$page = $pagename.".php";
?>

<!DOCTYPE html>
<html lang="en">
    <?php 
        include("__lib.includes/header_includes.inc.php"); 
    ?>
    <body>
    	<div id="preloader"></div>
        <div id="wrapper" class="home-page">
<?php 
	
	//include("__lib.includes/header.inc.php"); 

	include('__lib.htmlPages/__preLoginPages/'.$page);
	
	include("__lib.includes/footer.inc.php"); 
?>
        </div>
    </body>
</html>