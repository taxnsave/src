import sys
import zeep
from zeep import xsd
import calendar
import time
ts = calendar.timegm(time.gmtime())

bse_order = sys.argv[1].replace("\"", "")

LIVE = 1 
MEMBERID = ['11003', '15133']
USERID = ['1100301', '1513301'] 
PASSWORD = ['123456', '123456*']
PASSKEY = [ts, ts]

# url for BSEStar order entry webservice which is used to create or cancel transactions
WSDL_ORDER_URL = [
    'http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc?singleWsdl', 
    'http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc?singleWsdl'
]
SVC_ORDER_URL = [
    'http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc', 
    'http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc'
]
METHOD_ORDER_URL = [
    'http://bsestarmfdemo.bseindia.com/MFOrderEntry/',
    'http://bsestarmf.in/MFOrderEntry/'
]

# every soap query to bse must have wsa headers set 
def soap_set_wsa_headers(method_url, svc_url):
    header = xsd.Element('bse', xsd.ComplexType([
        xsd.Element('{http://www.w3.org/2005/08/addressing}Action', xsd.String()),
        xsd.Element('{http://www.w3.org/2005/08/addressing}To', xsd.String())
        ])
    )
    header_value = header(Action=method_url, To=svc_url)
    return header_value


# set logging such that its easy to debug soap queries
def set_soap_logging():
    import logging.config
    logging.config.dictConfig({
        'version': 1,
        'formatters': {
            'verbose': {
                'format': '%(name)s: %(message)s'
            }
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'verbose',
            },
        },
        'loggers': {
            'zeep.transports': {
                'level': 'DEBUG',
                'propagate': True,
                'handlers': ['console'],
            },
        }
    })

def soap_get_password_order(client):
    method_url = METHOD_ORDER_URL[LIVE] + 'getPassword'
    svc_url = SVC_ORDER_URL[LIVE]
    header_value = soap_set_wsa_headers(method_url, svc_url)
    response = client.service.getPassword(
        UserId=USERID[LIVE], 
        Password=PASSWORD[LIVE], 
        PassKey=PASSKEY[LIVE], 
        _soapheaders=[header_value]
    )
    print
    response = response.split('|')
    status = response[0]
    if (status == '100'):
        # login successful
        pass_dict = {'password': response[1], 'passkey': PASSKEY[LIVE]}
        return pass_dict
    else:
        raise Exception(
            "BSE error 640: Login unsuccessful for Order API endpoint"
        )


'''  ABOVE CODE WILL BE SAME IN ALL ACTIVITY '''

def create_transaction_bse(bse_order,transaction):
	
	## initialise the zeep client for order wsdl
	client = zeep.Client(wsdl=WSDL_ORDER_URL[LIVE])
	set_soap_logging()

	## get the password for posting order
	pass_dict = soap_get_password_order(client)
	
	## prepare order, post it to BSE and save response
	## for lumpsum transaction 
	if (transaction == '1'):
		## post the transaction
		order_id = soap_post_order(client, bse_order, pass_dict, '', '', '')

	else:
		raise Exception(
			"Internal error 630: Invalid order_type in transaction table"
		)

	return order_id



def soap_post_order(client, bse_order, pass_dict, param1, param2, param3):
	method_url = METHOD_ORDER_URL[LIVE] + 'orderEntryParam'
	header_value = soap_set_wsa_headers(method_url, SVC_ORDER_URL[LIVE])
	bse_order = bse_order.split('|')
    
	response = client.service.orderEntryParam(
	bse_order[0],
        bse_order[1],
        bse_order[2],
        bse_order[3],
        bse_order[4],
        bse_order[5],
        bse_order[6],
        bse_order[7],
        bse_order[8],
        bse_order[9],
        bse_order[10],
        bse_order[11],
        bse_order[12],
        bse_order[13],
        bse_order[14],
        bse_order[15],
        bse_order[16],
        bse_order[17],
        bse_order[18],
        bse_order[19],
        bse_order[20],
        bse_order[21],
        bse_order[22],
	pass_dict['password'],
	pass_dict['passkey'],
        param1,
        param2,
        param3,
	_soapheaders=[header_value]
	)
	
	## this is a good place to put in a slack alert
	
	response = response.split('|')
	## store the order response in a table
	
	status = response[7]
	if (status == '0'):
		# order successful
		return response
	else:
		raise Exception(
			"BSE error 641: %s" % response
		)

order_details = create_transaction_bse(bse_order,'1')
print (order_details)
