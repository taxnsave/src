import sys
import zeep
from zeep import Client
from zeep import xsd
from zeep.transports import Transport
import calendar
import time
ts = calendar.timegm(time.gmtime())
          
bse_user = sys.argv[1].replace("\"", "")
bse_fatca = sys.argv[2].replace("\"", "")

'''print bse_user'''

LIVE = 1 
MEMBERID = ['11003', '15133']
USERID = ['1100301', '1513301'] 
PASSWORD = ['123456', '123456*']
PASSKEY = [ts, ts]

# url for BSEStar order entry webservice which is used to create or cancel transactions
WSDL_ORDER_URL = [
    'http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc?singleWsdl', 
    'http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc?singleWsdl'
]
SVC_ORDER_URL = [
    'http://bsestarmfdemo.bseindia.com/MFOrderEntry/MFOrder.svc', 
    'http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc'
]
METHOD_ORDER_URL = [
    'http://bsestarmfdemo.bseindia.com/MFOrderEntry/',
    'http://bsestarmf.in/MFOrderEntry/'
]
# url for BSEStar upload webservice which is used to do everything besides creating/cancelling 
# transactions like create user (transactions on bse can only be placed after this) etc
WSDL_UPLOAD_URL = [
    'http://bsestarmfdemo.bseindia.com/MFUploadService/MFUploadService.svc?singleWsdl', 
    'http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc?singleWsdl'
]
SVC_UPLOAD_URL = [
    'http://bsestarmfdemo.bseindia.com/MFUploadService/MFUploadService.svc/Basic', 
    'http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic'
]
METHOD_UPLOAD_URL = [
    'http://bsestarmfdemo.bseindia.com/2016/01/IMFUploadService/',
    'http://www.bsestarmf.in/2016/01/IStarMFWebService/'
]

# every soap query to bse must have wsa headers set 
def soap_set_wsa_headers(method_url, svc_url):
    header = xsd.Element('bse', xsd.ComplexType([
        xsd.Element('{http://www.w3.org/2005/08/addressing}Action', xsd.String()),
        xsd.Element('{http://www.w3.org/2005/08/addressing}To', xsd.String())
        ])
    )
    header_value = header(Action=method_url, To=svc_url)
    return header_value


# set logging such that its easy to debug soap queries
def set_soap_logging():
    import logging.config
    logging.config.dictConfig({
        'version': 1,
        'formatters': {
            'verbose': {
                'format': '%(name)s: %(message)s'
            }
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'verbose',
            },
        },
        'loggers': {
            'zeep.transports': {
                'level': 'DEBUG',
                'propagate': True,
                'handlers': ['console'],
            },
        }
    })

def soap_get_password_upload(client):
    method_url = METHOD_UPLOAD_URL[LIVE] + 'getPassword'
    svc_url = SVC_UPLOAD_URL[LIVE]   
    header_value = soap_set_wsa_headers(method_url, svc_url)
    
    response = client.service.getPassword(
        MemberId=MEMBERID[LIVE], 
        UserId=USERID[LIVE],
        Password=PASSWORD[LIVE], 
        PassKey=PASSKEY[LIVE], 
        _soapheaders=[header_value]
    )
    print
    response = response.split('|')
    status = response[0]
    if (status == '100'):
        # login successful
        pass_dict = {'password': response[1], 'passkey': PASSKEY[LIVE]}
        return pass_dict
    else:
        raise Exception(
            "BSE error 640: Login unsuccessful for upload API endpoint"
        )

'''  ABOVE CODE WILL BE SAME IN ALL ACTIVITY '''

def create_user_bse(bse_user, bse_fatca):
   
    client = zeep.Client(wsdl=WSDL_UPLOAD_URL[LIVE])
    set_soap_logging()
    pass_dict = soap_get_password_upload(client)
    
    ## GET THE PIPE SEPERATED VALUE FROM PHP
    '''bse_user = prepare_user_param(client_code)'''
    ## post the user creation request
    user_response = soap_create_user(client, bse_user, pass_dict)
    
    ## TODO: Log the soap request and response post the user creation request
    pass_dict = soap_get_password_upload(client)
    ## GET THE PIPE SEPERATED VALUE FROM PHP
    '''bse_fatca = prepare_fatca_param(client_code)'''
    fatca_response = soap_create_fatca(client, bse_fatca, pass_dict)
    ## TODO: Log the soap request and response post the fatca creation request

## fire SOAP query to create a new user on bsestar
def soap_create_user(client, user_param, pass_dict):
    method_url = METHOD_UPLOAD_URL[LIVE] + 'MFAPI'
    header_value = soap_set_wsa_headers(method_url, SVC_UPLOAD_URL[LIVE])
    response = client.service.MFAPI(
        '02',
        USERID[LIVE],
        pass_dict['password'],
        user_param,
        _soapheaders=[header_value]
    )
    
    ## this is a good place to put in a slack alert

    response = response.split('|')
    status = response[0]
    if (status == '100'):
        # User creation successful
        pass
    else:
        raise Exception(
            "BSE error 644: User creation unsuccessful: %s" % response[1]
        )


## fire SOAP query to craete fatca record of user on bsestar
def soap_create_fatca(client, fatca_param, pass_dict):
    method_url = METHOD_UPLOAD_URL[LIVE] + 'MFAPI'
    header_value = soap_set_wsa_headers(method_url, SVC_UPLOAD_URL[LIVE])
    response = client.service.MFAPI(
        '01',
        USERID[LIVE],
        pass_dict['password'],
        fatca_param,
        _soapheaders=[header_value]
    )
    
    ## this is a good place to put in a slack alert

    response = response.split('|')
    status = response[0]
    if (status == '100'):
        # Fatca creation successful
        pass
    else:
        raise Exception(
            "BSE error 645: Fatca creation unsuccessful: %s" % response[1]
        )

create_user_bse(bse_user, bse_fatca)

'''          
client = zeep.Client(wsdl=WSDL_UPLOAD_URL[LIVE])
set_soap_logging()
## get the password 
pass_dict = soap_get_password_upload(client)

print(pass_dict)'''
