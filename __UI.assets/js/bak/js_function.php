<?php
include("../../__lib.includes/config.inc.php");

Header("content-type: application/x-javascript");
?>

var http_server_base = '<?php echo $CONFIG->siteurl; ?>';

jQuery(function($) {

$('.show-details-btn').on('click', function(e) {
    e.preventDefault();
    $(this).closest('tr').next().toggleClass('open');
    $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
});
 $(".trClick").click(function() {
        window.location = $(this).data("href");
    });
<?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "wealth") {?>
 		$('#gndWedRiskTaken').change(function(){
    $('#gndWedIntRate').val($(this).val());
  });

  $('#gndWedProcessBtn1').click(function(){
    var currentAge      = $('#gndWedCurrAge').val();
    var marrigeAge      = $('#gndWedMariageAge').val();
    var requiredAmount  = $('#gndWedAmt').val();
    var lumsumAmount    = $('#gndWedLumpsumAmt').val();
    var rate            = $('#gndWedIntRate').val();
    var inflationRate   = $('#gndWedInfRate').val();
    var noOfYrs = marrigeAge - currentAge;
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum ;
    var gap2 = fv - fvSipLum;
    $('#gndWedNumYears').val(noOfYrs);
    $('#gndWedAmt2').val(requiredAmount);
    $('#gndWedLumpsumAmt2').val(lumsumAmount);
    $('#gndWedInvPerMonth').val(Math.round(sipAmount));
    $('#gndWedIntRate2').val(rate);
    $('#gndWedInfRate2').val(inflationRate);
    $('#gndWedFvAmt').text(Math.round(fv));
    $('#gndWedSipAmt').text(Math.round(sipAmount));
    $('#gndWedSipAmt2').text(Math.round(sipAmount));
    $('#gndWedAvilAmt').text(Math.round(fvSipLum));
    $('#gndWedSipInv').text(gap2);
  });

  $('#gndWedRiskTaken2').change(function(){
    $('#gndWedIntRate2').val($(this).val());
    var requiredAmount  = $('#gndWedAmt2').val();
    var lumsumAmount    = $('#gndWedLumpsumAmt2').val();
    var rate            = $('#gndWedIntRate2').val();
    var inflationRate   = $('#gndWedInfRate2').val();
    var noOfYrs         = $('#gndWedNumYears').val();
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum ;
    var gap2 = fv - fvSipLum;
    $('#gndWedNumYears').val(noOfYrs);
    $('#gndWedAmt2').val(requiredAmount);
    $('#gndWedLumpsumAmt2').val(lumsumAmount);
    $('#gndWedInvPerMonth').val(Math.round(sipAmount));
    $('#gndWedInfRate2').val(inflationRate);
    $('#gndWedFvAmt').text(Math.round(fv));
    $('#gndWedSipAmt').text(Math.round(sipAmount));
    $('#gndWedSipAmt2').text(Math.round(sipAmount));
    $('#gndWedAvilAmt').text(Math.round(fvSipLum));
    $('#gndWedSipInv').text(gap2);
   });
   // Grand Wedding end

   //Higher Education Start
  $('#highEduRiskTaken').change(function(){
    $('#highEduIntRate').val($(this).val());
  });

  $('#highEduProcessBtn1').click(function(){
    var currentAge      = $('#highEduCurrAge').val();
    var highEduSrtAge   = $('#highEduSrtAge').val();
    var requiredAmount  = $('#highEduAmt').val();
    var lumsumAmount    = $('#highEduLumpsumAmt').val();
    var rate            = $('#highEduIntRate').val();
    var inflationRate   = $('#highEduInfRate').val();
    var noOfYrs = highEduSrtAge - currentAge;
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum ;
    var gap2 = fv - fvSipLum;
    $('#highEduNumYears').val(noOfYrs);
    $('#highEduAmt2').val(requiredAmount);
    $('#highEduLumpsumAmt2').val(lumsumAmount);
    $('#highEduInvPerMonth').val(Math.round(sipAmount));
    $('#highEduIntRate2').val(rate);
    $('#highEduInfRate2').val(inflationRate);
    $('#highEduFvAmt').text(Math.round(fv));
    $('#highEduSipAmt').text(Math.round(sipAmount));
    $('#highEduSipAmt2').text(Math.round(sipAmount));
    $('#highEduAvilAmt').text(Math.round(fvSipLum));
    $('#highEduSipInv').text(gap2);
  });

  $('#highEduRiskTaken2').change(function(){
    $('#highEduIntRate2').val($(this).val());
    var requiredAmount  = $('#highEduAmt2').val();
    var lumsumAmount    = $('#highEduLumpsumAmt2').val();
    var rate            = $('#highEduIntRate2').val();
    var inflationRate   = $('#highEduInfRate2').val();
    var noOfYrs         = $('#highEduNumYears').val();
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum ;
    var gap2 = fv - fvSipLum;
    $('#highEduNumYears').val(noOfYrs);
    $('#highEduAmt2').val(requiredAmount);
    $('#highEduLumpsumAmt2').val(lumsumAmount);
    $('#highEduInvPerMonth').val(Math.round(sipAmount));
    $('#highEduInfRate2').val(inflationRate);
    $('#highEduFvAmt').text(Math.round(fv));
    $('#highEduSipAmt').text(Math.round(sipAmount));
    $('#highEduSipAmt2').text(Math.round(sipAmount));
    $('#highEduAvilAmt').text(Math.round(fvSipLum));
    $('#highEduSipInv').text(gap2);
  });
  //Higher Education End

  // Own a House start
  $('#ownHousRiskTaken').change(function(){
    $('#ownHousIntRate').val($(this).val());
  });

  $('#ownHousAmtReq').blur(function(){
    var amtReq = $(this).val();
    var IntWork = (amtReq*20/100);
    $('#ownHousIntWork').val(amtReq*20/100);
    var totalAmount =  parseInt(amtReq) + parseInt(IntWork);
    $('#ownHousTotalAmt').val(totalAmount);
  });

  $('#ownHousProcessBtn1').click(function(){
    var noOfYrs         = $('#ownHousNoOfYear').val();
    var requiredAmount  = $('#ownHousTotalAmt').val();
    var lumsumAmount    = $('#ownHousLumpsumAmt').val();
    var rate            = $('#ownHousIntRate').val();
    var inflationRate   = $('#ownHousInfRate').val();
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum ;
    var gap2 = fv - fvSipLum;
    $('#ownHousNumYears').val(noOfYrs);
    $('#ownHousAmt2').val(requiredAmount);
    $('#ownHousLumpsumAmt2').val(lumsumAmount);
    $('#ownHousInvPerMonth').val(Math.round(sipAmount));
    $('#ownHousIntRate2').val(rate);
    $('#ownHousInfRate2').val(inflationRate);
    $('#ownHousFvAmt').text(Math.round(fv));
    $('#ownHousSipAmt').text(Math.round(sipAmount));
    $('#ownHousSipAmt2').text(Math.round(sipAmount));
    $('#ownHousAvilAmt').text(Math.round(fvSipLum));
    $('#ownHousSipInv').text(gap2);
  });

  $('#ownHousRiskTaken2').change(function(){
    $('#ownHousIntRate2').val($(this).val());
    var requiredAmount  = $('#ownHousAmt2').val();
    var lumsumAmount    = $('#ownHousLumpsumAmt2').val();
    var rate            = $('#ownHousIntRate2').val();
    var inflationRate   = $('#ownHousInfRate2').val();
    var noOfYrs         = $('#ownHousNumYears').val();
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum ;
    var gap2 = fv - fvSipLum;
    $('#ownHousNumYears').val(noOfYrs);
    $('#ownHousAmt2').val(requiredAmount);
    $('#ownHousLumpsumAmt2').val(lumsumAmount);
    $('#ownHousInvPerMonth').val(Math.round(sipAmount));
    $('#ownHousInfRate2').val(inflationRate);
    $('#ownHousFvAmt').text(Math.round(fv));
    $('#ownHousSipAmt').text(Math.round(sipAmount));
    $('#ownHousSipAmt2').text(Math.round(sipAmount));
    $('#ownHousAvilAmt').text(Math.round(fvSipLum));
    $('#ownHousSipInv').text(gap2);
  });
  // Own a House end

  // Buy a Car start
  $('#buyACarRiskTaken').change(function(){
    $('#buyACarIntRate').val($(this).val());
  });

  $('#buyACarProcessBtn1').click(function(){
    var noOfYrs          = $('#buyACarNoOfYear').val();
    var requiredAmount  = $('#buyACarAmtReq').val();
    var lumsumAmount    = $('#buyACarLumpsumAmt').val();
    var rate            = $('#buyACarIntRate').val();
    var inflationRate   = $('#buyACarInfRate').val();
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum ;
    var gap2 = fv - fvSipLum;
    $('#buyACarNumYears').val(noOfYrs);
    $('#buyACarAmt2').val(requiredAmount);
    $('#buyACarLumpsumAmt2').val(lumsumAmount);
    $('#buyACarInvPerMonth').val(Math.round(sipAmount));
    $('#buyACarIntRate2').val(rate);
    $('#buyACarInfRate2').val(inflationRate);
    $('#buyACarFvAmt').text(Math.round(fv));
    $('#buyACarSipAmt').text(Math.round(sipAmount));
    $('#buyACarSipAmt2').text(Math.round(sipAmount));
    $('#buyACarAvilAmt').text(Math.round(fvSipLum));
    $('#buyACarSipInv').text(gap2);
  });

  $('#buyACarRiskTaken2').change(function(){
    $('#buyACarIntRate2').val($(this).val());
    var requiredAmount  = $('#buyACarAmt2').val();
    var lumsumAmount    = $('#buyACarLumpsumAmt2').val();
    var rate            = $('#buyACarIntRate2').val();
    var inflationRate   = $('#buyACarInfRate2').val();
    var noOfYrs         = $('#buyACarNumYears').val();
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum 
    var gap2 = fv - fvSipLum;
    $('#buyACarNumYears').val(noOfYrs);
    $('#buyACarAmt2').val(requiredAmount);
    $('#buyACarLumpsumAmt2').val(lumsumAmount);
    $('#buyACarInvPerMonth').val(Math.round(sipAmount));
    $('#buyACarInfRate2').val(inflationRate);
    $('#buyACarFvAmt').text(Math.round(fv));
    $('#buyACarSipAmt').text(Math.round(sipAmount));
    $('#buyACarSipAmt2').text(Math.round(sipAmount));
    $('#buyACarAvilAmt').text(Math.round(fvSipLum));
    $('#buyACarSipInv').text(gap2);
  });
  // Buy a Car end

  // vacation plan START

  $('#vacaPlanRiskTaken').change(function(){
    $('#vacaPlanIntRate').val($(this).val());
  });

  $('#vacaPlanProcessBtn1').click(function(){
    var noOfYrs          = $('#vacaPlanNoOfYear').val();
    var requiredAmount  = $('#vacaPlanAmtReq').val();
    var lumsumAmount    = $('#vacaPlanLumpsumAmt').val();
    var rate            = $('#vacaPlanIntRate').val();
    var inflationRate   = $('#vacaPlanInfRate').val();
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum ;
    var gap2 = fv - fvSipLum;
    $('#vacaPlanNumYears').val(noOfYrs);
    $('#vacaPlanAmt2').val(requiredAmount);
    $('#vacaPlanLumpsumAmt2').val(lumsumAmount);
    $('#vacaPlanInvPerMonth').val(Math.round(sipAmount));
    $('#vacaPlanIntRate2').val(rate);
    $('#vacaPlanInfRate2').val(inflationRate);
    $('#vacaPlanFvAmt').text(Math.round(fv));
    $('#vacaPlanSipAmt').text(Math.round(sipAmount));
    $('#vacaPlanSipAmt2').text(Math.round(sipAmount));
    $('#vacaPlanAvilAmt').text(Math.round(fvSipLum));
    $('#vacaPlanSipInv').text(gap2);
  });

  $('#vacaPlanRiskTaken2').change(function(){
    $('#vacaPlanIntRate2').val($(this).val());
    var requiredAmount  = $('#vacaPlanAmt2').val();
    var lumsumAmount    = $('#vacaPlanLumpsumAmt2').val();
    var rate            = $('#vacaPlanIntRate2').val();
    var inflationRate   = $('#vacaPlanInfRate2').val();
    var noOfYrs         = $('#vacaPlanNumYears').val();
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum ;
    var gap2 = fv - fvSipLum;
    $('#vacaPlanNumYears').val(noOfYrs);
    $('#vacaPlanAmt2').val(requiredAmount);
    $('#vacaPlanLumpsumAmt2').val(lumsumAmount);
    $('#vacaPlanInvPerMonth').val(Math.round(sipAmount));
    $('#vacaPlanInfRate2').val(inflationRate);
    $('#vacaPlanFvAmt').text(Math.round(fv));
    $('#vacaPlanSipAmt').text(Math.round(sipAmount));
    $('#vacaPlanSipAmt2').text(Math.round(sipAmount));
    $('#vacaPlanAvilAmt').text(Math.round(fvSipLum));
    $('#vacaPlanSipInv').text(gap2);
  });
  // vacation plan End

  // emergency fund started
  $('#emeFundRiskTaken').change(function(){
    $('#emeFundIntRate').val($(this).val());
  });

  $('#emeFundProcessBtn1').click(function(){
    var noOfYrs          = $('#emeFundNoOfYear').val();
    var requiredAmount  = $('#emeFundAmtReq').val();
    var lumsumAmount    = $('#emeFundLumpsumAmt').val();
    var rate            = $('#emeFundIntRate').val();
    var inflationRate   = $('#emeFundInfRate').val();
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum ;
    var gap2 = fv - fvSipLum;
    $('#emeFundNumYears').val(noOfYrs);
    $('#emeFundAmt2').val(requiredAmount);
    $('#emeFundLumpsumAmt2').val(lumsumAmount);
    $('#emeFundInvPerMonth').val(Math.round(sipAmount));
    $('#emeFundIntRate2').val(rate);
    $('#emeFundInfRate2').val(inflationRate);
    $('#emeFundFvAmt').text(Math.round(fv));
    $('#emeFundSipAmt').text(Math.round(sipAmount));
    $('#emeFundSipAmt2').text(Math.round(sipAmount));
    $('#emeFundAvilAmt').text(Math.round(fvSipLum));
    $('#emeFundSipInv').text(gap2);
  });

  $('#emeFundRiskTaken2').change(function(){
    $('#emeFundIntRate2').val($(this).val());
    var requiredAmount  = $('#emeFundAmt2').val();
    var lumsumAmount    = $('#emeFundLumpsumAmt2').val();
    var rate            = $('#emeFundIntRate2').val();
    var inflationRate   = $('#emeFundInfRate2').val();
    var noOfYrs         = $('#emeFundNumYears').val();
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum ;
    var gap2 = fv - fvSipLum;
    $('#emeFundNumYears').val(noOfYrs);
    $('#emeFundAmt2').val(requiredAmount);
    $('#emeFundLumpsumAmt2').val(lumsumAmount);
    $('#emeFundInvPerMonth').val(Math.round(sipAmount));
    $('#emeFundInfRate2').val(inflationRate);
    $('#emeFundFvAmt').text(Math.round(fv));
    $('#emeFundSipAmt').text(Math.round(sipAmount));
    $('#emeFundSipAmt2').text(Math.round(sipAmount));
    $('#emeFundAvilAmt').text(Math.round(fvSipLum));
    $('#emeFundSipInv').text(gap2);
  });

  // emergency fund END

  // unique Goal Start
  $('#uniqGoalRiskTaken').change(function(){
    $('#uniqGoalIntRate').val($(this).val());
  });

  $('#uniqGoalProcessBtn1').click(function(){
    var noOfYrs          = $('#uniqGoalNoOfYear').val();
    var requiredAmount  = $('#uniqGoalAmtReq').val();
    var lumsumAmount    = $('#uniqGoalLumpsumAmt').val();
    var rate            = $('#uniqGoalIntRate').val();
    var inflationRate   = $('#uniqGoalInfRate').val();
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum ;
    var gap2 = fv - fvSipLum;
    $('#uniqGoalNumYears').val(noOfYrs);
    $('#uniqGoalAmt2').val(requiredAmount);
    $('#uniqGoalLumpsumAmt2').val(lumsumAmount);
    $('#uniqGoalInvPerMonth').val(Math.round(sipAmount));
    $('#uniqGoalIntRate2').val(rate);
    $('#uniqGoalInfRate2').val(inflationRate);
    $('#uniqGoalFvAmt').text(Math.round(fv));
    $('#uniqGoalSipAmt').text(Math.round(sipAmount));
    $('#uniqGoalSipAmt2').text(Math.round(sipAmount));
    $('#uniqGoalAvilAmt').text(Math.round(fvSipLum));
    $('#uniqGoalSipInv').text(gap2);
  });

  $('#uniqGoalRiskTaken2').change(function(){
    $('#uniqGoalIntRate2').val($(this).val());
    var requiredAmount  = $('#uniqGoalAmt2').val();
    var lumsumAmount    = $('#uniqGoalLumpsumAmt2').val();
    var rate            = $('#uniqGoalIntRate2').val();
    var inflationRate   = $('#uniqGoalInfRate2').val();
    var noOfYrs         = $('#uniqGoalNumYears').val();
    var rateOfReturn = rate/100;
    var fv = requiredAmount * Math.pow((1+ inflationRate/100), (noOfYrs));
    var a = Math.pow((1+ rateOfReturn), (1/noOfYrs));
    var nominal = noOfYrs * (a-1)/12;
    var lumsum = lumsumAmount * Math.pow((1+ rateOfReturn), (noOfYrs));
    var gap = fv - lumsum;
    var b = Math.pow((1+ nominal), (noOfYrs*12));
    var sipAmount = gap * nominal/(b-1);
    sipAmount;
    var c = Math.pow((1+ nominal), (noOfYrs*12));
    var fvSipInvestment = sipAmount * (c-1)/nominal;
    var fvSipLum = fvSipInvestment + lumsum ;
    var gap2 = fv - fvSipLum;
    $('#uniqGoalNumYears').val(noOfYrs);
    $('#uniqGoalAmt2').val(requiredAmount);
    $('#uniqGoalLumpsumAmt2').val(lumsumAmount);
    $('#uniqGoalInvPerMonth').val(Math.round(sipAmount));
    $('#uniqGoalInfRate2').val(inflationRate);
    $('#uniqGoalFvAmt').text(Math.round(fv));
    $('#uniqGoalSipAmt').text(Math.round(sipAmount));
    $('#uniqGoalSipAmt2').text(Math.round(sipAmount));
    $('#uniqGoalAvilAmt').text(Math.round(fvSipLum));
    $('#uniqGoalSipInv').text(gap2);
  });
  // unique Goal End

  //PPF START
  $('#ppfRateType').change(function(){
    $('#ppfIntRate').val($(this).val()); 
  });
  $('#ppfCheck').click(function(){
    var principal =$('#ppfAmt').val(); 
    var rateType =$('#ppfRateType').val(); 
    var time =$('#ppfNumYear').val(); 
    var ppfTotalMatAmt ;
    $('#ppfIntRate').val(rateType+'%'); 
    if(rateType == 7.6)
    {
      var ppfTotalMatAmt = clc_PPF(principal,rateType,time);
    }
    else if(rateType == 7.8)
    {
      var ppfTotalMatAmt = clc_PPF(principal,rateType,time);
    }
    var  ppfTotalMatAmt = (parseInt(ppfTotalMatAmt));
    var  ppfTotalInvest = (parseInt(principal) * parseInt(time));
    var  ppfTotalIntEarn =(ppfTotalMatAmt -  ppfTotalInvest);
    $('#ppfTotalMatAmt').val(ppfTotalMatAmt);
    $('#ppfTotalInvest').val(ppfTotalInvest);
    $('#ppfTotalIntEarn').val(ppfTotalIntEarn);
  });
  //PPF END

  //HRA Start
  $('#HRA_Check').click(function(){
    var sal_rec     =  parseInt($('#hraBscSal').val());
    var da          =  parseInt($('#hraDA').val());
    var hra_rec     =  parseInt($('#hraRec').val());
    var rent_paid   =  parseInt($('#hraActRentPad').val());
    var city        =  $('#hraCity').val();
    hra_rec;
    rent_paid = rent_paid-((sal_rec + da)*(10/100));
    metro_nonmetro = 0;
    if(city == 'oth')
    {
        metro_nonmetro = ((sal_rec + da)*(40/100));
    }
    else
    {
        metro_nonmetro = ((sal_rec + da)*(50/100))
    }
    var HRA_Exemptions = 0;
    if(hra_rec < rent_paid && hra_rec < metro_nonmetro)
    {
        HRA_Exemptions = hra_rec;
    }
    else if(rent_paid < hra_rec && rent_paid < metro_nonmetro)
    {
        HRA_Exemptions = rent_paid;
    }
    else if(metro_nonmetro < hra_rec && metro_nonmetro < rent_paid)
    {
        HRA_Exemptions = metro_nonmetro;
    }
    $('#hraExempt').val(HRA_Exemptions);
    $('#hrataxable').val((hra_rec - HRA_Exemptions));
  });
  //HRA end

  //SSY start
  $('#ssyCheck').click(function(){
    var principalVal   =  parseFloat($('#ssyAmt').val());
    var intRate        =  parseFloat($('#ssyIntRate').val());
    var startYears     =  parseInt($('#ssyStartyear').val());
    var ssyTotalMatAmt =  clc_SSY(principalVal,intRate);
    var ssyMatYears    =  startYears + 15;
    $('#ssyMatYear').val(ssyMatYears);
    $('#ssyTotalAmt').val(ssyTotalMatAmt);
  });
  //SSY end

  //Home Loan start
  $('#homeLoanCheck').click(function(){
    var P   = parseFloat($("#homeLoanAmt").val());
    var r   = parseFloat(parseFloat($("#homeLoanIntRate").val()) / 100/12);
    var n   = parseFloat($("#homeLoanNumYears").val() * 12);
    var emi = clc_emi(P,n,r);
    var totalPayAmt = Math.round(emi*n);
    var totalPayInt = totalPayAmt - P;
    emi = emi.toFixed(2);
    $('#homeLoanEmiAmt').val(emi);
    $('#homeLoanTotalPayAmt').val(totalPayAmt);
    $('#homeLoanTotalInt').val(totalPayInt);
  });
  //Home Loan end

  //car loan start
  $('#carLoanCheck').click(function(){
    var P   = parseFloat($("#carLoanAmt").val());
    var r   = parseFloat(parseFloat($("#carLoanIntRate").val()) / 100 / 12);
    var n   = parseFloat($("#carLoanNumYears").val() * 12);
    var emi = clc_emi(P,n,r);
    var totalPayAmt = Math.round(emi*n);
    var totalPayInt = totalPayAmt - P;
    emi = emi.toFixed(2);
    $('#carLoanEmiAmt').val(emi);
    $('#carLoanPayAmt').val(totalPayAmt);
    $('#carLoanTotalInt').val(totalPayInt);
  });
  //car loan end

  //Personal Loan start
  $('#perLoanCheck').click(function(){
    var P   = parseFloat($("#perLoanAmt").val());
    var r   = parseFloat(parseFloat($("#perLoanIntRate").val()) / 100 / 12);
    var n   = parseFloat($("#perLoanNumYears").val() * 12);
    var emi = clc_emi(P,n,r);
    var totalPayAmt = Math.round(emi*n);
    var totalPayInt = totalPayAmt - P;
    emi = emi.toFixed(2);
    $('#perLoanEmiAmt').val(emi);
    $('#perLoanPayAmt').val(totalPayAmt);
    $('#perLoanTotalInt').val(totalPayInt);
  });
  //Personal Loan end

  // Rd start
  $('#rdCheckBtn').click(function(){
    var monthlyInstallment =  $('#rdAmt').val();
    var numberOfYears      =  $('#rdNumYears').val();
    var rateOfInterest     =  $('#rdIntRate').val();
    var numberOfMonths     =  numberOfYears * 12;
    var amt = Math.round(clc_recurrInt(monthlyInstallment,numberOfMonths,rateOfInterest));
    $('#rdTotalAmt').val(amt);
    $('#rdYearShow').text(numberOfYears);
  });
  // RD end

  //Fixed Deposit start
  $('#fdCheckBtn').click(function(){
    var principal    =  $('#fixdepAmt').val();
    var rate         =  $('#fixdepIntRate').val();
    var time         =  $('#fixdepNum').val();
    var time_period  =  $('#fixdepNumType').val();
    var intType      =  $('#fixdepIntType').val();
    var amt;
    var totalInt;
     if(intType == 'simpleInterest')
      {
        amt = clc_simpleInt(principal,1,time,rate,time_period); 
      }
      else
      {
        amt = (principal* Math.pow((1 + (rate/(intType*100))), (intType*time/time_period)));
      }
      amt = Math.round(amt);
      totalInt = amt - principal;
      var show_time = calc_time(time_period);
      $('#fixdepMatAmt').val(amt);
      $('#fixdepTotalInt').val(totalInt);
      $('.fixdepYearShow').text(time+" "+show_time);
      $('#fixdepInvestAmt').text(principal);
      $('#fixdepTotalAmt').text(amt);
      $('#fixdepTimeShow').text(show_time);
  });
  //Fixed Deposti end

  // Tax start
  $('#taxCheckBtn').click(function(){
    var age = parseInt($('#taxAgeNum').val());
    var elss = parseFloat($('#taxElssSchemeAmt').val()) || 0;
    var lic = parseFloat($('#taxLICSchemeAmt').val()) || 0;
    var ssy = parseFloat($('#taxSSYSchemeAmt').val()) || 0;
    var fd = parseFloat($('#taxFDAmt').val()) || 0;
    var ppf = parseFloat($('#taxPPFAmt').val()) || 0;
    var insurance = parseFloat($('#taxInsAmt').val()) || 0;
    var otherAmt = parseFloat($('#taxOtherAmt').val()) || 0;
    var otherAmt = parseFloat($('#taxOtherAmt').val()) || 0;
    var salary = parseFloat($('#taxAnnSal').val()) || 0;
    var totalInvest = (elss + lic + ssy + fd + ppf + insurance + otherAmt);
    var remainingSalary = salary - totalInvest;
    var tax = 0;
    var furInvest;
    var taxSavedFromFurInv;
    if (totalInvest>150000) 
    {
      fur_invest = 0;
    }
    else
    {
      fur_invest = 150000 - totalInvest;
    }

    if(age >0 && age <= 60) 
    {
       if (remainingSalary>250000 && remainingSalary<=500000) 
       {
          tax=((remainingSalary-250000)*5/100);
       }
       if (remainingSalary>500000 && remainingSalary <=1000000) 
       {
          tax= 12500+((remainingSalary-500000)*20/100);
       }
       if (remainingSalary >1000000) 
       {
          tax= (12500+100000)+((remainingSalary-500000)*30/100);
       }
       if (salary<=250000) 
       {
          taxSavedFromFurInv = (fur_invest*0/100);
       }
       if (salary>250000 && salary <=500000) 
       {
          taxSavedFromFurInv = (fur_invest*5/100);
       }
       if (salary>500000 && salary<=1000000) 
       {
          taxSavedFromFurInv = (fur_invest*20/100);
       }
       if (salary>1000000) 
       {
          taxSavedFromFurInv = (fur_invest*30/100);
       }
    }
    else if(age >= 61 && age <= 80)
    {
      if (remainingSalary>300000 && remainingSalary<=500000) 
      {
          tax=((remainingSalary-300000)*5/100);
      }
      if (remainingSalary>500000 && remainingSalary <=1000000) 
      {
          tax= 10000+((remainingSalary-500000)*20/100);
      }
      if (remainingSalary >1000000) 
      {
          tax= (10000+100000)+((remainingSalary-500000)*30/100);
      }

      if (salary<=300000) 
      {
          taxSavedFromFurInv = (fur_invest*0/100);
      }
      if (salary>300000 && salary <=500000) 
      {
          taxSavedFromFurInv = (fur_invest*5/100);
      }
      if (salary>500000 && salary<=1000000) 
      {
          taxSavedFromFurInv = (fur_invest*20/100);
      }
      if (salary>1000000) 
      {
          taxSavedFromFurInv = (fur_invest*30/100);
      }

    }
    else if(age >=81)
    {
      if (remainingSalary>500000 && remainingSalary <=1000000) 
      {
          tax= (remainingSalary-500000)*20/100;
      }
      if (remainingSalary >1000000) 
      {
          tax= (100000)+((remainingSalary-500000)*30/100);
      }

      if (salary<=500000) 
      {
          taxSavedFromFurInv = (fur_invest*0/100);
      }
      if (salary>500000 && salary<=1000000) 
      {
          taxSavedFromFurInv = (fur_invest*20/100);
      }
      if (salary>1000000) 
      {
          taxSavedFromFurInv = (fur_invest*30/100);
      }
    }
    $('#taxTotalInvAmt').val(totalInvest);
    $('#taxRemInvAmt').val(fur_invest);
    $('#taxSaveAmt').val(taxSavedFromFurInv);
  });
        /*google.charts.load('current', { 'packages': ['corechart'] });
		google.charts.setOnLoadCallback(drawChart);*/

        $(".wealthDashMain").show();
        $(".wealthHide").hide();
        $("#retireRichShow").show();
        $('.sidebar').click(function(event) {
            var wealthDivClick = $(event.target).attr('id');
            // alert(wealthDivClick);
            // $(".wealthDashMain").hide();
            $('.learnPlanAndInvHide').hide();
            $(".wealthHide").hide();
            $("#" + wealthDivClick + "Show").show();
        });


        $('.riskProfShow, .calCulatorsShow, .portAllocShow, .sipSTPShow, .lumpSumShow, .saveTaxShow').hide();
        $('#rowLearnPlan>div>a>div, #rowInvestment>div>a>div').click(function(event) {
            var allLearnPlanAndInv = $(this).attr('id');
            // alert(allLearnPlanAndInv);
            $(".wealthHide").hide();
            // var test = '.' + allLearnPlanAndInv + 'show';
            $('.calCulatorsShow').show();
            // $('.learnPlanAndInvHide').hide();
            // alert(test);
            // $('.' + allLearnPlanAndInv + 'show').show();
            // $('.' + allLearnPlanAndInv + 'show').show();
        });
                $(".wealthDashMain").show();
        $(".wealthHide").hide();
        // $("#retireRichShow").show();
        $('.sidebar').click(function(event) {
            var wealthDivClick = $(event.target).attr('id');
            // alert(wealthDivClick);
            // $(".wealthDashMain").hide();
            $('.learnPlanAndInvHide').hide();
            $(".wealthHide").hide();
            $("#" + wealthDivClick + "Show").show();
        });

        $('.learnPlanAndInvHide').hide();
        $('#rowLearnPlan>div>a>div, #rowInvestment>div>a>div').click(function(event) {
            var allLearnPlanAndInv = $(this).attr('id');
            // alert(allLearnPlanAndInv);
            $(".wealthHide").hide();
            $('.learnPlanAndInvHide').hide();
            if (allLearnPlanAndInv == 'riskProf') {
                $('.riskProfShow').show();
            }else if (allLearnPlanAndInv == 'calCulators') {
                $('.calCulatorsShow').show();
            }else if (allLearnPlanAndInv == 'portAlloc') {
                $('.portAllocShow').show();
            }else if (allLearnPlanAndInv == 'sipSTP') {
                $('.sipSTPShow').show();
            }else if (allLearnPlanAndInv == 'lumpSum') {
                $('.lumpSumShow').show();
            }else if (allLearnPlanAndInv == 'saveTax') {
                $('.saveTaxShow').show();
            }
        });

<?php } ?>    
<?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "mf_offer_buy" || $_SESSION[$CONFIG->sessionPrefix.'page_name'] == "mf_buy_sell") { ?>
	
    $('#edit-modal').on('show.bs.modal', function(e) {            
        var $modal = $(this),
        esseyId = e.relatedTarget.id;
        titleId = e.relatedTarget.title;
        //alert(esseyId);
        $.ajax({
                cache: false,
                type: 'POST',
                url: '../ajax-request/mf_details.php',
                data: 'MFID='+esseyId,
                success: function(data) 
                {
                	//console.log(data);
                    $modal.find('.modal-title').html(titleId);
		            $modal.find('.edit-content').html(data);
                }
        });            
   })
<?php } ?>
<?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "create_will") { ?>
 
    $(".tab_content").hide(); //Hide all content
    $("ul.nav-tabs li:first").addClass("active").show(); //Activate first tab
    $(".tab_content:first").show(); //Show first tab content

    $("ul.nav-tabs li").click(function() {

        $("ul.nav-tabs li").removeClass("active"); //Remove any "active" class
        $(this).addClass("active"); //Add "active" class to selected tab
        $(".tab-pane").hide(); //Hide all tab content

        var activeTab = $(this).find("a").attr("href"); 
        var activeTabURL = $(this).find("a").attr("data-url");
        //if($(activeTab).html() == '') 
        //{              	
          $.ajax({
            url: activeTabURL,
            type: 'post',
            dataType: 'html',
            success: function(content) {
              $(activeTab).html(content);
              //alert("dd");
            }
          });
        //}

        $(activeTab).fadeIn(); //Fade in the active ID content
        return false;
    });
    
    /*************************WILL PAGES**********************************/
        
        $('#btn-update').click(function(){
            $.validate({form:"#frmProfiles", onSuccess:validate_stocksbonds});
        });
        $('#btn-add').click(function(){
            $.validate({form:"#frmProfiles", onSuccess:validate_stocksbonds});
        });
      $("#chkcorrespondence").click(function() {
        if($(this).is(":checked")){
            $('#correspondence_address_line1').val($('#permanent_address_line1').val());
            $('#correspondence_address_line2').val($('#permanent_address_line2').val());
            $('#correspondence_address_line3').val($('#permanent_address_line3').val());
            $('#correspondence_city_village_town').val($('#permanent_city_village_town').val());
            $('#correspondence_state').val($('#permanent_state').val());
            $('#correspondence_zipcode').val($('#permanent_zipcode').val());

            $('.correspondenceAdd input, .correspondenceAdd select').attr('disabled','disabled');
            // alert("hi");
        } else {
            $('.correspondenceAdd input, .correspondenceAdd select').removeAttr('disabled');
        }
        // alert('you are unchecked ' + $(this).val());
    });
    
    /*************************WILL PAGES**********************************/
    
<?php }?>
<?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "home") {?>
			  var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
			  var data = [
				{ label: "2012-2013",  data: 38.7, color: "#68BC31"},
				{ label: "2013-2014",  data: 24.5, color: "#2091CF"},
				{ label: "2014-2015",  data: 8.2, color: "#AF4E96"},
				{ label: "2015-2016",  data: 18.6, color: "#DA5430"},
				{ label: "2016-2017",  data: 10, color: "#FEE074"}
			  ]
			  function drawPieChart(placeholder, data, position) {
			 	  $.plot(placeholder, data, {
					series: {
						pie: {
							show: true,
							tilt:0.8,
							highlight: {
								opacity: 0.25
							},
							stroke: {
								color: '#fff',
								width: 2
							},
							startAngle: 2
						}
					},
					legend: {
						show: true,
						position: position || "ne", 
						labelBoxBorderColor: null,
						margin:[-30,15]
					}
					,
					grid: {
						hoverable: true,
						clickable: true
					}
				 })
			 }
			 drawPieChart(placeholder, data);
			
			 /**
			 we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
			 so that's not needed actually.
			 */
			 placeholder.data('chart', data);
			 placeholder.data('draw', drawPieChart);
			
			
			  //pie chart tooltip example
			  var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
			  var previousPoint = null;
			
			  placeholder.on('plothover', function (event, pos, item) {
				if(item) {
					if (previousPoint != item.seriesIndex) {
						previousPoint = item.seriesIndex;
						var tip = item.series['label'] + " : " + item.series['percent'].toFixed()+'%';
						$tooltip.show().children(0).text(tip);
					}
					$tooltip.css({top:pos.pageY + 10, left:pos.pageX + 10});
				} else {
					$tooltip.hide();
					previousPoint = null;
				}
				
			 });
			 $('#searchForm').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: true,
					ignore: "",
					rules: {						
						search_input: {
							required: true
						}
					},
			
					messages: {
						search_input: {
							required: "Please provide a valid search text.",
							email: "Please provide a valid search text."
						}
					},			
					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},
			
					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
						$(e).remove();
					},
			
					errorPlacement: function (error, element) {
						if(element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},
			
					submitHandler: function (form) {
						form.submit();
					},
					invalidHandler: function (form) {
					}
				});
			
			 <?php }?>   
<?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "profile") { ?>
 //editables on first profile page
				$.fn.editable.defaults.mode = 'inline';
				$.fn.editableform.loading = "<div class='editableform-loading'><i class='ace-icon fa fa-spinner fa-spin fa-2x light-blue'></i></div>";
			    $.fn.editableform.buttons = '<button type="submit" class="btn btn-info editable-submit"><i class="ace-icon fa fa-check"></i></button>'+
			                                '<button type="button" class="btn editable-cancel"><i class="ace-icon fa fa-times"></i></button>';    
				
				//editables 				
                $('#cust_name').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'cust_name',
					send: 'always'		
			    });
			     $('#alternet_email_id').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'alternet_email_id',
					send: 'always'		
			    });
			     $('#city').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'city',
					send: 'always'		
			    });
			     $('#change_pass').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'password',
					name: 'change_pass',
					send: 'always'		
			    });
				 $('#company_name').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'company_name',
					send: 'always'		
			    });
				$('#fath_name').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'fath_name',
					send: 'always'		
			    });
                $('#dob').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'dob',
                    title: 'Enter username',
					send: 'always',
                    success: function(response, newValue) {                      
                         $("#calcAge").html(response);
                    }                    
			    });
				$('#contact_no').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'contact_no',
					send: 'always'		
			    });
				$('#profession').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'profession',
					send: 'always'		
			    });
				$('#address1').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'textarea',
					name: 'address1',
					send: 'always'		
			    });				
				$('#city1').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'city',
					send: 'always'		
			    });
				$('#state').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'state',
					send: 'always'		
			    });
				$('#pincode').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'pincode',
					send: 'always'		
			    });
				$('#country').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'country',
					send: 'always'		
			    });
                $('#aadhar').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'aadhar',
					send: 'always'		
			    });
                $('#pan').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'pan',
					send: 'always'		
			    });
				$('#alternet_email_id1').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'alternet_email_id',
					send: 'always'		
			    });
				$('#bank_name').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'bank_name',
					send: 'always'		
			    });
                $('#acc_no').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'acc_no',
					send: 'always'		
			    });
                $('#ifsc_code').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'ifsc_code',
					send: 'always'		
			    });
                $('#swift_code').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'swift_code',
					send: 'always'		
			    });
                $('#bank_address').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'textarea',
					name: 'bank_address',
					send: 'always'		
			    });
                $('#bank_city').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'bank_city',
					send: 'always'		
			    });
                $('#bank_state').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'bank_state',
					send: 'always'		
			    });
                 $('#bank_country').editable({
					url: '../ajax-request/post_login_response.php',
					type: 'text',
					name: 'bank_country',
					send: 'always'		
			    });
                // *** editable avatar *** //
				try {//ie8 throws some harmless exceptions, so let's catch'em
			
					//first let's add a fake appendChild method for Image element for browsers that have a problem with this
					//because editable plugin calls appendChild, and it causes errors on IE at unpredicted points
					try {
						document.createElement('IMG').appendChild(document.createElement('B'));
					} catch(e) {
						Image.prototype.appendChild = function(el){}
					}
			
					var last_gritter
					$('#avatar').editable({
						type: 'image',
						name: 'avatar',
						value: null,
						//onblur: 'ignore',  //don't reset or hide editable onblur?!
						image: {
							//specify ace file input plugin's options here
							btn_choose: 'Change Profile Photo',
							droppable: true,
							maxSize: 1010000,//~100Kb
			
							//and a few extra ones here
							name: 'avatar',//put the field name here as well, will be used inside the custom plugin
							on_error : function(error_type) {//on_error function will be called when the selected file has a problem
								if(last_gritter) $.gritter.remove(last_gritter);
								if(error_type == 1) {//file format error
									last_gritter = $.gritter.add({
										title: 'File is not an image!',
										text: 'Please choose a jpg|gif|png image!',
										class_name: 'gritter-error gritter-center'
									});
								} else if(error_type == 2) {//file size rror
									last_gritter = $.gritter.add({
										title: 'File too big!',
										text: 'Image size should not exceed 100Kb!',
										class_name: 'gritter-error gritter-center'
									});
								}
								else {//other error
								}
							},
							on_success : function() {
								$.gritter.removeAll();
							}
						},
					    url: function(params) {
						// ***UPDATE AVATAR HERE*** //
						var submit_url = '../ajax-request/profile-image-upload.php';//please modify submit_url accordingly
						var deferred = null;
						var avatar = '#avatar';

						//if value is empty (""), it means no valid files were selected
						//but it may still be submitted by x-editable plugin
						//because "" (empty string) is different from previous non-empty value whatever it was
						//so we return just here to prevent problems
						var value = $(avatar).next().find('input[type=hidden]:eq(0)').val();
						if(!value || value.length == 0) {
							deferred = new $.Deferred
							deferred.resolve();
							return deferred.promise();
						}

						var $form = $(avatar).next().find('.editableform:eq(0)')
						var file_input = $form.find('input[type=file]:eq(0)');
						var pk = $(avatar).attr('data-pk');//primary key to be sent to server

						var ie_timeout = null


						if( "FormData" in window ) {
							var formData_object = new FormData();//create empty FormData object
							
							//serialize our form (which excludes file inputs)
							$.each($form.serializeArray(), function(i, item) {
								//add them one by one to our FormData 
								formData_object.append(item.name, item.value);							
							});
							//and then add files
							$form.find('input[type=file]').each(function(){
								var field_name = $(this).attr('name');
								var files = $(this).data('ace_input_files');
								if(files && files.length > 0) {
									formData_object.append(field_name, files[0]);
								}
							});

							//append primary key to our formData
							formData_object.append('pk', pk);

							deferred = $.ajax({
										url: submit_url,
									   type: 'POST',
								processData: false,//important
								contentType: false,//important
								   dataType: 'json',//server response type
									   data: formData_object
							})
						}
						else {
							deferred = new $.Deferred

							var temporary_iframe_id = 'temporary-iframe-'+(new Date()).getTime()+'-'+(parseInt(Math.random()*1000));
							var temp_iframe = 
									$('<iframe id="'+temporary_iframe_id+'" name="'+temporary_iframe_id+'" \
									frameborder="0" width="0" height="0" src="about:blank"\
									style="position:absolute; z-index:-1; visibility: hidden;"></iframe>')
									.insertAfter($form);
									
							$form.append('<input type="hidden" name="temporary-iframe-id" value="'+temporary_iframe_id+'" />');
							
							//append primary key (pk) to our form
							$('<input type="hidden" name="pk" />').val(pk).appendTo($form);
							
							temp_iframe.data('deferrer' , deferred);
							//we save the deferred object to the iframe and in our server side response
							//we use "temporary-iframe-id" to access iframe and its deferred object

							$form.attr({
									  action: submit_url,
									  method: 'POST',
									 enctype: 'multipart/form-data',
									  target: temporary_iframe_id //important
							});

							$form.get(0).submit();

							//if we don't receive any response after 30 seconds, declare it as failed!
							ie_timeout = setTimeout(function(){
								ie_timeout = null;
								temp_iframe.attr('src', 'about:blank').remove();
								deferred.reject({'status':'fail', 'message':'Timeout!'});
							} , 30000);
						}


						//deferred callbacks, triggered by both ajax and iframe solution
						deferred
						.done(function(result) {//success
							var res = result[0];//the `result` is formatted by your server side response and is arbitrary
							if(res.status == 'OK') 
                            {
                            	$(avatar).get(0).src = res.url;
                                $("#profile_header_img").attr('src', res.url);
							}
                            else alert(res.message);
						})
						.fail(function(result) {//failure
							alert("There was an error");
						})
						.always(function() {//called on both success and failure
							if(ie_timeout) clearTimeout(ie_timeout)
							ie_timeout = null;	
						});

						return deferred.promise();
						// ***END OF UPDATE AVATAR HERE*** //
					},
						
						success: function(response, newValue) {
                      
                      
						}
					})
				}catch(e) {}
<?php } ?>
<?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "ac_stmt") { ?>
    $('.scrollable').each(function () {
        var $this = $(this);
        $(this).ace_scroll({
            size: $this.attr('data-size') || 100,
            //styleClass: 'scroll-left scroll-margin scroll-thin scroll-dark scroll-light no-track scroll-visible'
        });
    });	
 	$('.input-daterange').datepicker({autoclose:true});
    $.ajax({
        url: "../ajax-request/ac_stmt_html.php",
        type: "GET",
        data: {name:"first_time"},
        success: function(response) {
                $("#fetchProgressbarInner_ac").css("display", "none");
                $("#ac_stmt").append(response);        
        }            
    });
<?php } ?> 
<?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "folio_summary") { ?>
    $('.scrollable').each(function () {
        var $this = $(this);
        $(this).ace_scroll({
            size: $this.attr('data-size') || 100,
            //styleClass: 'scroll-left scroll-margin scroll-thin scroll-dark scroll-light no-track scroll-visible'
        });
    });	
 	$('.input-daterange').datepicker({autoclose:true});
    $.ajax({
        url: "../ajax-request/portfolio_summary_html.php",
        type: "GET",
        data: {name:"first_time"},
        success: function(response) {
                $("#fetchProgressbarInner_ac").css("display", "none");
                $("#ac_stmt").append(response);        
        }            
    });
<?php } ?>   
<?php if($CONFIG->loggedUserId) { ?>
    //Android's default browser somehow is confused when tapping on label which will lead to dragging the task
    //so disable dragging when clicking on label
    var agent = navigator.userAgent.toLowerCase();
    if(ace.vars['touch'] && ace.vars['android']) {
      $('#tasks').on('touchstart', function(e){
        var li = $(e.target).closest('#tasks li');
        if(li.length == 0)return;
        var label = li.find('label.inline').get(0);
        if(label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation() ;
      });
    }
<?php } ?>	
<?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "process_forms" || $_SESSION[$CONFIG->sessionPrefix.'page_name'] == "itr_forms") { ?>
/*****************************ITR************************************/
	
	var fourteen_digit_no =  /^[1-9]{1}[0-9]{0,13}$/;	
	var validation_regex = {
		  "PAN" : /^[A-Za-z]{3}[pP]{1}[A-Za-z]{1}[0-9]{4}[A-Za-z]{1}$/,		  
		  "AadhaarCardNo" : /^[0-9]{12}$/,
		  "AadhaarEnrolmentId" : /^[0-9]{28}$/,
		  "MobileNo" : /^[1-9]{1}[0-9]{9}$/, 
		  "EmailAddress" : /^([\.a-zA-Z0-9_\-])+@([a-zA-Z0-9_\-])+(([a-zA-Z0-9_\-])*\.([a-zA-Z0-9_\-])+)+$/, 
		  "PinCode" : /^[1-9]{1}[0-9]{5}$/,	  
		  "ReceiptNo" : /[0-9]{15}/,
		  "Salary" : fourteen_digit_no,
		  "AlwnsNotExempt" : fourteen_digit_no,
		  "PerquisitesValue" : fourteen_digit_no,
		  "ProfitsInSalary" : fourteen_digit_no,
		  "DeductionUs16" : fourteen_digit_no,
		  "IncomeFromSal" : fourteen_digit_no,
		  "14DigitNumber" : fourteen_digit_no,
		  "TAN" :  /^[A-Z]{4}[0-9]{5}[A-Z]$/,
		  "Number" : /^[1-9]{1}[0-9]*$/,
		  "Percentage" : /^[1-9][0-9]?$|^100$/,
		  "Alphabets" : /^[a-zA-Z ]+$/,
		  "BankAccountNo" : /^[a-zA-Z0-9]([/-]?(((\d*[1-9]\d*)*[a-zA-Z/-])|(\d*[1-9]\d*[a-zA-Z]*))+)*[0-9]*$/,
		  "IFSCCode" : /^[A-Z]{4}[0][A-Z0-9]{6}$/
		};
		
		var dependent_elements = ['sou_oth_oi_agriinc','sou_oth_oi_diviinc','sou_oth_exi_agriinc','sou_oth_exi_diviinc'];
		var $error_ele = $('<span>',{class : 'red error-msg'});
		
		//Validations for personal details

		$("#tabUserProf,#tabOther").on('focusout','input,select',function(e){
		
			var $ele = $(e.target);
			
			var $form = $ele.closest('form');
			
			var validation_name = $ele.data('validation');
			var ele_value = $ele.val().trim();
			
			if(ele_value)
			{	
				$ele.removeClass('required');
				
				if( validation_regex[validation_name] && !validation_regex[validation_name].test(ele_value))
				{
					var ele_name = $ele.closest('.form-group').find('label').text();					
					$error_ele.text(ele_name+' is invalid');
					$ele.next('span.red').remove().end().addClass('error regex').after($error_ele);
				}
				else
				{
					$ele.removeClass('error').next('span.red').remove();
					
					if(dependency = $ele.data('dependency'))
					{
						$("[data-xml='"+dependency+"']",$form).removeClass('error').next('span.red').remove();
					}	
				}		
			}
			else
			{
				if(!$ele.hasClass('required'))
				{
					$ele.removeClass('error').next('span.red').remove();						
				}						
			}			
		});
		
		$("#form_1").on('submit',function(e){
			e.preventDefault();
			
			var $form = $(this);
			var has_error = false;	

			var error_classes = "error required";	
			
			if($form.find('.error').length)
			{
				$form.find('.ajaxResClass').text("Please correct all errors before submitting");
			}
			else
			{
				
				$form.find('.error').removeClass('error').next('span.red').remove();	
				
				if($("select[data-xml='ReturnType']",$form).val() == 'Revised')
				{
					if(!$("[data-xml='ReceiptNo']",$form).val().trim())
					{
						$error_ele.text('Acknowledgement No of original return is required');
						$("[data-xml='ReceiptNo']",$form).addClass(error_classes).after($error_ele.clone());

						has_error = true;		
					}

					if(!$("[data-xml='OrigRetFiledDate']",$form).val().trim())
					{
						$error_ele.text('Date of filing original return is required');
						$("[data-xml='OrigRetFiledDate']",$form).addClass(error_classes).after($error_ele.clone());		
						
						has_error = true;							
					}			

				}

				if(!$("[data-xml='AadhaarCardNo']").val().trim() && !$("[data-xml='AadhaarEnrolmentId']").val().trim())
				{
					$error_ele.text('Either aadhar card number or aadhar enrollment number is required');
					$("[data-xml='AadhaarCardNo']",$form).addClass(error_classes).after($error_ele.clone());
					
					$("[data-xml='AadhaarEnrolmentId']",$form).addClass(error_classes).after($error_ele.clone());	

					has_error = true;					
				}

				if(!has_error)
				{
					ajaxFormSubmit(this,'','');
				}		
			}		
		});
		
		//Validation end
		
		//Resident status helper form
		
		$("#res-status-decide").on('click',function(){
			$("#resStatusHelp").find(".res-ques:gt(0)").hide().end().find('input').prop("checked",false);
		});
		
		$("#resStatusHelp").on('click','input',function(e){
			
			var $ele = $(e.target);
			
			var $group_ele =  $ele.closest('.res-ques');
			var question_no = $group_ele.data('no');
			var answer = $ele.val();
			
			var next_question = '';
			
			$group_ele.nextAll('.res-ques').hide();
			
			switch(question_no)
			{
				case 1 : 
					next_question =  ((answer == "1") ? '1d' : '1a'); break;
				case '1a' : 
					next_question = ((answer == "1") ? 'NRI' : '1c'); break;
				case '1c' : 
					next_question = ((answer == "1") ? '1d' : 'NRI'); break;
				case '1d' : 
					next_question = ((answer == "1") ? '1e' : 'NRO'); break;
				case '1e' : 
					next_question = ((answer == "1") ? 'RES' : 'NRO'); break;
			}
			
			if(next_question.length > 2)
			{
				$("[name='itr_pd_resi_sta'][value='"+next_question+"']").prop('checked',true);
				
				$("#resStatusHelp").modal('hide');
			}
			else
			{
				$('.res-ques-'+next_question).show();			
			}	
			
		});
		//resident status helper end	

		//Functions, validations and calculations for salary details
		
		$(".add_sou_salaryy_btn").on('click',function(e){
			var $original_form = $(".add_sou_salaryy_div.hide");
			$(this).closest(".form-group").before($original_form.clone().removeClass('hide'));		
		});	
		
		if($(".add_sou_salaryy_div:not(.hide)").length == 0)
		{
			$(".add_sou_salaryy_btn").trigger('click');
		}	

		$("#tabFromSalary").on('focusout','input,select',function(e){
		
			var $ele = $(e.target);
			
			var $form = $("#form-income-salary");
			var $container = $ele.closest('.add_sou_salaryy_div');
			
			var required_fields = $form.data('required').split(",");
			var check_fields = $form.data('check').split(",");	
			
			var validation_name = $ele.data('validation');
			var ele_value = $ele.val().trim();
			
			if(ele_value)
			{	
				$ele.removeClass('required');
				
				if( validation_regex[validation_name] && !validation_regex[validation_name].test(ele_value))
				{
					var ele_name = $ele.closest('.form-group').find('label').text();					
					$error_ele.text(ele_name+' is invalid');
					$ele.next('span.red').remove().end().addClass('error regex').after($error_ele);
				}
				else
				{
					$ele.removeClass('error').next('span.red').remove();
					
					recalculateSalary($ele,$container);
					
					setRequired(check_fields,required_fields,$container);
				}		
			}
			else
			{
				if(!$ele.hasClass('required'))
				{
					$ele.removeClass('error').next('span.red').remove();						
				}

				recalculateSalary($ele,$container);	
				
				setRequired(check_fields,required_fields,$container);				
			}			
		});
		
		$("#form-income-salary").on('submit',function(e){
			
			e.preventDefault();
			
			var $form = $(this);
			var has_error = false;	
			var have_data = false;	

			var error_classes = "error required";	
			
			if($form.find('.error').length)
			{
				$form.find('.ajaxResClass').text("Please correct all errors before submitting");
			}
			else
			{
				$form.find('.error').removeClass('error').next('span.red').remove();
				
				var check_fields = $form.data('check').split(",");	

				$(".add_sou_salaryy_div").each(function(i,item){
					
					$container = $(item);
					var has_value = false;
					
					$(check_fields).each(function(i,field){
						
						var field_val = $("[name='"+field+"']",$container).val().trim();
						
						if(field_val && field_val != "0")
						{
							has_value = true;
							return false;	
						}	
					});
					
					if(!has_value)
					{
						$container.remove();
					}
					else
					{
						have_data = true;
					}	
				});	
				
				if(!has_error)
				{
					ajaxFormSubmit(this,'','');
				}		
			}		
		});
		
		function recalculateSalary($ele,$form)
		{
			var salary_fields = 'sou_sa_profits[],sou_sa_deduction[],sou_sa_allowance[],sou_sa_perquisite[],sou_sa_salary[]';
			
			var name_prop = $ele.attr('name');
			
			if(salary_fields.indexOf(name_prop) != -1)
			{
				var profits,allowance,perquisites,salary,deduction;
				
				var salary_total = parseInt(((profits = $("[name='sou_sa_profits[]']",$form).val().trim()) ? profits : 0)) + parseInt(((allowance = $("[name='sou_sa_allowance[]']",$form).val().trim()) ? allowance : 0)) + parseInt(((perquisites = $("[name='sou_sa_perquisite[]']",$form).val().trim()) ? perquisites : 0)) + parseInt(((salaray = $("[name='sou_sa_salary[]']",$form).val().trim()) ? salaray : 0)) - parseInt(((deduction = $("[name='sou_sa_deduction[]']",$form).val().trim()) ? deduction : 0)); 
				
				$("[name='sou_sa_ntslary[]']",$form).val(salary_total);
			}			
		}		
		
		//Salary income end
		
		//Self occupied property form
		
		$("#selfOccPropertyShow").on('focusout','input,select',function(e){
		
			var $ele = $(e.target);
			
			var $form = $("#form-income-selfoccupied");	
			
			var validation_name = $ele.data('validation');
			var ele_value = $ele.val().trim();
			
			if(ele_value)
			{	
				$ele.removeClass('required');

				
				if( validation_regex[validation_name] && !validation_regex[validation_name].test(ele_value))
				{
					var ele_name = $ele.closest('.form-group').find('label').text();					
					$error_ele.text(ele_name+' is invalid');
					$ele.next('span.red').remove().end().addClass('error regex').after($error_ele);
				}
				else
				{
					$ele.removeClass('error').next('span.red').remove();
					
					recalculateSelfOccHouseInterest();
				}		
			}
			else
			{
				if(!$ele.hasClass('required'))
				{
					$ele.removeClass('error').next('span.red').remove();						
				}

				recalculateSelfOccHouseInterest();	
			}			
		});
		
		$("#form-income-selfoccupied").on('submit',function(e){
			
			e.preventDefault();
			
			var $form = $(this);
			var has_error = false;	
			var have_data = false;	

			var error_classes = "error required";	
			
			if($form.find('.error').length)
			{
				$form.find('.ajaxResClass').text("Please correct all errors before submitting");
			}
			else
			{
				$form.find('.error').removeClass('error').next('span.red').remove();
				
				if(!parseInt($("[name='self_con_income[]']").val()))
				{
					has_error = true;
				}	
				
				if(!has_error)
				{
					ajaxFormSubmit(this,'','');
				}		
			}		
		});	

		function recalculateSelfOccHouseInterest()
		{
			var loan_interest = $("[name='self_hloan_int[]']").val() ? $("[name='self_hloan_int[]']").val() : 0;
			var pre_interest = $("[name='self_con_per_int[]']").val() ? $("[name='self_con_per_int[]']").val() : 0;
			
			var total_interest = (total_interest = parseInt(loan_interest) + parseInt(pre_interest)) <= 200000 ? total_interest : 200000;
			
			$("[name='self_con_income[]']").val(total_interest);			
		}		
		
		//Self occupied property form end
		
		//Let-out property form
		
		$("#letOutProprtyShow").on('focusout','input,select',function(e){
		
			var $ele = $(e.target);
			
			var $form = $("#form-income-letout");	
			
			var validation_name = $ele.data('validation');
			var ele_value = $ele.val().trim();
			
			if(ele_value)
			{	
				$ele.removeClass('required');

				
				if( validation_regex[validation_name] && !validation_regex[validation_name].test(ele_value))
				{
					var ele_name = $ele.closest('.form-group').find('label').text();					
					$error_ele.text(ele_name+' is invalid');
					$ele.next('span.red').remove().end().addClass('error regex').after($error_ele);
				}
				else
				{
					$ele.removeClass('error').next('span.red').remove();
					
					recalculateLetOutHouseInterest();
					recalculateLetOutHouseDeduction();
				}		
			}
			else
			{
				if(!$ele.hasClass('required'))
				{
					$ele.removeClass('error').next('span.red').remove();						
				}

				recalculateLetOutHouseInterest();	
				recalculateLetOutHouseDeduction();
			}			
		});
		
		$("#form-income-letout").on('submit',function(e){
			
			e.preventDefault();
			
			var $form = $(this);
			var has_error = false;	
			var have_data = false;	

			var error_classes = "error required";	
			
			if($form.find('.error').length)
			{
				$form.find('.ajaxResClass').text("Please correct all errors before submitting");
			}
			else
			{
				$form.find('.error').removeClass('error').next('span.red').remove();
				
				if(!parseInt($("[name='let_con_income[]']").val()))
				{
					has_error = true;
				}	
				
				if(!has_error)
				{
					ajaxFormSubmit(this,'','');
				}		
			}		
		});	

		function recalculateLetOutHouseInterest()
		{
			var pre_interest = $("[name='let_pre_cons_per_int[]']").val() ? $("[name='let_pre_cons_per_int[]']").val() : 0;
			var loan_interest = $("[name='let_hloan_int[]']").val() ? $("[name='let_hloan_int[]']").val() : 0;
			var deduction = $("[name='let_st_dedu[]']").val() ? $("[name='let_st_dedu[]']").val() : 0;
			var property_tax = $("[name='let_proptex_pad[]']").val() ? $("[name='let_proptex_pad[]']").val() : 0;
			var rental_income = $("[name='let_ren_inc[]']").val() ? $("[name='let_ren_inc[]']").val() : 0;			
			
			var total_income = parseInt(rental_income) - parseInt(property_tax) - parseInt(deduction) - parseInt(loan_interest) - parseInt(pre_interest); 
			
			$("[name='let_con_income[]']").val(total_income);			
		}	

		function recalculateLetOutHouseDeduction()
		{

			var property_tax = $("[name='let_proptex_pad[]']").val() ? $("[name='let_proptex_pad[]']").val() : 0;
			var rental_income = $("[name='let_ren_inc[]']").val() ? $("[name='let_ren_inc[]']").val() : 0;			
			
			if(property_tax && rental_income)
			{
				var deduction = Math.ceil(parseInt(rental_income) - parseInt(property_tax)) * (3/10); 
				
				$("[name='let_st_dedu[]']").val(deduction);					
			}			
		}			
		
		//Let out property form end	

		//Other income form
		
		$("#tabOther").on('focusout','input,select',function(e){
		
			var $ele = $(e.target);
			
			var $form = $("#form-income-other");	
			
			var validation_name = $ele.data('validation');
			var ele_value = $ele.val().trim();
			
			if(ele_value)
			{	
				$ele.removeClass('required');

				
				if( validation_regex[validation_name] && !validation_regex[validation_name].test(ele_value))
				{
					var ele_name = $ele.closest('.form-group').find('label').text();					
					$error_ele.text(ele_name+' is invalid');
					$ele.next('span.red').remove().end().addClass('error regex').after($error_ele);
				}
				else
				{
					$ele.removeClass('error').next('span.red').remove();
					
					recalculateTotalOtherIncome();
					recalculateTotalExemptedIncome();
				}		
			}
			else
			{
				if(!$ele.hasClass('required'))
				{
					$ele.removeClass('error').next('span.red').remove();						
				}

				recalculateTotalOtherIncome();	
				recalculateTotalExemptedIncome();
			}			
		});
		
		$("#form-income-other").on('submit',function(e){
			
			e.preventDefault();
			
			var $form = $(this);
			var has_error = false;	
			var have_data = false;	

			var error_classes = "error required";	
			
			if($form.find('.error').length)
			{
				$form.find('.ajaxResClass').text("Please correct all errors before submitting");
			}
			else
			{
				$form.find('.error').removeClass('error').next('span.red').remove();
				
				if(!parseInt($("[name='let_con_income[]']").val()))
				{
					has_error = true;
				}	
				
				if(!has_error)
				{
					ajaxFormSubmit(this,'','');
				}		
			}		
		});	

		function recalculateTotalOtherIncome()
		{
			var other_income = $("[name='sou_oth_oi_othinc']").val() ? $("[name='sou_oth_oi_othinc']").val() : 0;
			var other_interest = $("[name='sou_oth_oi_othint']").val() ? $("[name='sou_oth_oi_othint']").val() : 0;
			var bank_interest = $("[name='sou_oth_oi_bnkint']").val() ? $("[name='sou_oth_oi_bnkint']").val() : 0;			
			
			var total_income = parseInt(other_income) + parseInt(other_interest) + parseInt(bank_interest); 
			
			$("[name='sou_oth_oi_totothinc']").val(total_income);			
		}	

		function recalculateTotalExemptedIncome()
		{

			var other_income = $("[name='sou_oth_exi_othinc']").val() ? $("[name='sou_oth_exi_othinc']").val() : 0;
			var ltcg = $("[name='sou_oth_exi_ltcg']").val() ? $("[name='sou_oth_exi_ltcg']").val() : 0;
			var dividend = $("[name='sou_oth_exi_diviinc']").val() ? $("[name='sou_oth_exi_diviinc']").val() : 0;	
			var agri_income = $("[name='sou_oth_exi_agriinc']").val() ? $("[name='sou_oth_exi_agriinc']").val() : 0;				
			
			var total_income = parseInt(other_income) + parseInt(ltcg) + parseInt(dividend) + parseInt(agri_income); 
			
			$("[name='sou_oth_exi_totexinc']").val(total_income);				
		}			
		
		//Other income form end	

		//General deduction
		$("#genDeduction").on('focusout','input,select',function(e){
		
			var $ele = $(e.target);
			
			var $form = $("#form-income-other");	
			
			var validation_name = $ele.data('validation');
			var ele_value = $ele.val().trim();
			
			if(ele_value)
			{	
				$ele.removeClass('required');

				
				if( validation_regex[validation_name] && !validation_regex[validation_name].test(ele_value))
				{
					var ele_name = $ele.closest('.form-group').find('label').text();					
					$error_ele.text(ele_name+' is invalid');
					$ele.next('span.red').remove().end().addClass('error regex').after($error_ele);
				}
				else
				{
					$ele.removeClass('error').next('span.red').remove();
					
					recalculateTotalOtherIncome();
					recalculateTotalExemptedIncome();
				}		
			}
			else
			{
				if(!$ele.hasClass('required'))
				{
					$ele.removeClass('error').next('span.red').remove();						
				}

				recalculateTotalOtherIncome();	
				recalculateTotalExemptedIncome();
			}			
		});
		
		$("#form-income-other").on('submit',function(e){
			
			e.preventDefault();
			
			var $form = $(this);
			var has_error = false;	
			var have_data = false;	

			var error_classes = "error required";	
			
			if($form.find('.error').length)
			{
				$form.find('.ajaxResClass').text("Please correct all errors before submitting");
			}
			else
			{
				$form.find('.error').removeClass('error').next('span.red').remove();
				
				if(!parseInt($("[name='let_con_income[]']").val()))
				{
					has_error = true;
				}	
				
				if(!has_error)
				{
					ajaxFormSubmit(this,'','');
				}		
			}		
		});
		//End general deduction	
		
		
		function setRequired(check_fields,required_fields,$container)
		{
			var has_value = false;
			var field_val = null;
			
			$(check_fields).each(function(i,item){
				
				field_val = $("[name='"+item+"']",$container).val().trim();
				
				if(field_val && field_val != "0")
				{
					has_value = true;
					return false;	
				}	
			});
			
			if(has_value)
			{
				$(required_fields).each(function(j,field){
					$("[name='"+field+"']",$container).prop('required',true);
				});
			}
			else
			{
				$(":required",$container).prop('required',false);
			}		
		}	
		
        $('.date-picker').datepicker({ 
			autoclose: true,
			todayHighlight: true                    
		})
		//show datepicker when clicking on the icon
		.next().on(ace.click_event, function(){
			$(this).prev().focus();
		});

        $(".capGainRadHide").hide();
        $("#salOfLandPropShow").show();
        $("input[name$='capGainOptRadio']").click(function() {
            var capGainRad = $(this).val();
            $(".capGainRadHide").hide();
            $("#" + capGainRad + "Show").show();
            // alert(capGainRad);
        });
        $(".housePropertyHide").hide();
        $("#selfOccPropertyShow").show();
        $("input[name$='housePropertyRadio']").click(function() {
            var houseProperty = $(this).val();
            $(".housePropertyHide").hide();
            $("#" + houseProperty + "Show").show();
            // alert(houseProperty);
        });
        $(".dedDonationHide").hide();
        $("#charity100DedShow").show();
        $("input[name$='deductionDonRadio']").click(function() {
            var deductionDon = $(this).val();
            $(".dedDonationHide").hide();
            $("#" + deductionDon + "Show").show();
            // alert(deductionDon);
        });
        $(".foreignAssetsHide").hide();
        $("#foreignAssetsShow").show();
        $("input[name$='foreignAssetsOtptRadio']").click(function() {
            var foreignAss = $(this).val();
            $(".foreignAssetsHide").hide();
            $("#" + foreignAss + "Show").show();
            // alert(foreignAss);
        });


     //self occ start
    var propidslf = 0; var flagslf = 0; var newpropcountslf =0;  var sharecount = 0; var newslfflag = 0;
    $(document).on("blur",".selfocc_prop_share", function(){
        sharecount = sharecount + parseInt($(this).val());
        newslfflag = parseInt($(this).attr('newslfflag'));
        if(sharecount < 100)
        {  
          if(newslfflag == 1){ newpropcountslf++; propidslf = 'N_'+newpropcountslf;}else{ propidslf = $(this).attr('propidslf');}
          if(flagslf == 1){propidslf = 'N_'+newpropcountslf;}else{ propidslf = $(this).attr('propidslf');}
          reportRow2 = '<input type="hidden" name="hidcheck_coowner_selfocc[]" value="0"/><input type="hidden" name="hidcheck_subpart_slfoccid[]" value="'+propidslf+'"/><div class="form-group"> <label class="control-label col-sm-3" for="Co-Owner Details">Co-owner detail</label> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Co-Owner Name">Name of co-owner</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="coow_name" name="coow_name[]" placeholder="Co-Owner Name"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Co-Owner PAN">PAN of co-owner</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="coow_pan_no" name="coow_pan_no[]" placeholder="Co-Owner PAN"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="share of propery">Share of property(%)</label> </div> <div class="col-sm-5"> <input type="text" propidslf="'+propidslf+'" class="form-control selfocc_prop_share" id="scoow_sha_prop" name="coow_sha_prop[]" placeholder="Share of property(%)"> </div> </div>';
          $(this).parent().parent().parent().append(reportRow2); 
        }
    });
    
    $(".add_selfocc_prop_btn").click(function () {
        propidslf = 0;
        sharecount = 0;
        newpropcountslf++;
        if (newpropcountslf > 0) {propidslf = 'N_'+newpropcountslf; var slfnpid = propidslf; flagslf = 1;}
         reportRow3 = '<br><br><br><input type="hidden" name="hidcheckslfoccprop[]" value="'+propidslf+'"/><div class="form-group"> <label class="control-label col-sm-6" for="inputTANNumber">Interest paid on home loan of self occupied property</label> <div class="col-sm-4"> <input type="text" class="form-control" id="" name="self_hloan_int[]" placeholder="Interest of self occupied property" required> </div></div><div class="form-group"> <label class="control-label col-sm-6" for="">Pre construction period Interest</label> <div class="col-sm-4"> <input type="text" class="form-control" id="" name="self_con_per_int[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="Address">Address of property</label> </div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="address">Flat/Door/Block No.</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="self_fl_do_bl_no[]" placeholder="Flat/Door/Block"> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="permisesBuilding">Name of Permises/Building</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="self_bu_name[]" placeholder="Name of Permises/Building"> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="roadStreetPost">Road/Street/Postoffice</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="self_ro_st_po[]" placeholder="Road/Street/Postoffice"> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="areaLocality">Area/Locality</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="self_area_loc[]" placeholder="Area/Locality"> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="city">City/Town/District</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="self_city[]" placeholder="City"> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="State">State</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="self_state[]" placeholder="State"> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Country">Country</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="self_country[]" placeholder="Country"> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="pinCode">Pin Code</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="self_pincode[]" placeholder="pin Code"> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="share of propery">Share of property(%)</label> </div><div class="col-sm-5"> <input type="text" propidslf="'+slfnpid+'" class="form-control selfocc_prop_share" id="" name="self_sh_prop[]" placeholder="Share of property(%)"> </div></div>';  
         $('.add_self_occ_prop').append(reportRow3);                           
    });
    //self occ end
    
    //let out start
    var propidlot = 0; var flaglot = 0; var newpropcountlot =0; var sharecountlot = 0; var newlotflag = 0;
    $(document).on("blur",".letout_prop_share", function(){
        sharecountlot = sharecountlot + parseInt($(this).val());
        newlotflag = parseInt($(this).attr('newlotflag'));
	    if(sharecountlot < 100)
	    {
            if(newlotflag == 1){ newpropcountlot++; propidlot = 'N_'+newpropcountlot; }else{ propidlot = $(this).attr('propidlot');}
            if(flaglot == 1){propidlot = 'N_'+newpropcountlot;}else{ propidlot = $(this).attr('propidlot');}
	    	reportRow4 = '<input type="hidden" name="hidcheck_coowner_lot[]" value="0"/> <input type="hidden" name="hidcheck_subpart_lotid[]" value="'+propidlot+'"/> <div class="form-group"> <label class="control-label col-sm-3" for="Co-Owner Details">Co-owner detail</label> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Co-Owner Name">Name of co-owner</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="nameOfCoOwner" name="lot_coow_name[]" placeholder="Co-Owner Name"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Co-Owner PAN">PAN of co-owner</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="PANOfCoOwner"  name="lot_coow_pan_no[]" placeholder="Co-Owner PAN"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="share of propery">Share of property(%)</label> </div> <div class="col-sm-5"> <input type="text" propidlot="'+propidlot+'" class="form-control letout_prop_share" id="share"  name="lot_coow_sha_prop[]" placeholder="Share of property(%)"  > </div> </div>';	
	    	$(this).parent().parent().parent().append(reportRow4);
	    } 
	});

    $(".add_let_out_prop_btn").click(function () {
        propidlot = 0;   sharecountlot = 0; newpropcountlot++;
        if (newpropcountlot > 0) {propidlot = 'N_'+newpropcountlot; var lotnpid = propidlot; flaglot = 1;}
         reportRow55 = '<br><br><br><input type="hidden" name="hidcheckletoutprop[]" value="'+propidlot+'"/> <div class="form-group"> <label class="control-label col-sm-6" for="Total Setal Income">Total Rental Income in assesement year</label> <div class="col-sm-6"> <input type="text" class="form-control" id="" name="let_ren_inc[]" placeholder="Total Setal Income" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-6" for="Property Tax Paid">Property Tax Paid</label> <div class="col-sm-6"> <input type="text" class="form-control" id="pTaxPaid" name="let_proptex_pad[]" placeholder="Property Tax Paid" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-6" for="Standard Deduction">Standard Deduction</label> <div class="col-sm-6"> <input type="text" class="form-control" id="stndDeduc" name="let_st_dedu[]" placeholder="Standard Deduction" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-6" for="intPaidLetProp">Interest paid on home loan of let out property</label> <div class="col-sm-6"> <input type="text" class="form-control" id="intPaidLetProp" name="let_hloan_int[]" placeholder="int Paid Let Prop property" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-6" for="">Pre construction period Interest</label> <div class="col-sm-4"> <input type="text" class="form-control" id="" name="let_pre_cons_per_int[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Address">Address of property</label> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="address">Flat/Door/Block No.</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="houseNumber" name="let_fl_do_bu[]" placeholder="Flat/Door/Block"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="permisesBuilding">Name of Permises/Building</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="permisesBuilding" name="let_bu_name[]" placeholder="Name of Permises/Building"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="roadStreetPost">Road/Street/Postoffice</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="roadStreetPost" name="let_ro_st_po[]" placeholder="Road/Street/Postoffice"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="areaLocality">Area/Locality</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="areaLocality" name="let_area_loc[]" placeholder="Area/Locality"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="city">City/Town/District</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="city" name="let_city[]" placeholder="City"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="State">State</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="state" name="let_state[]" placeholder="State"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Country">Country</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="country" name="let_country[]" placeholder="Country"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="pinCode">Pin Code</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="pinCode" name="let_pincode[]" placeholder="pin Code"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Name of the tenant</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="let_nameotenant[]" placeholder=""> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="share of propery">Share of property(%)</label> </div> <div class="col-sm-5"> <input type="text" propidlot="'+lotnpid+'" class="form-control letout_prop_share" id="share" name="let_sh_prop[]" placeholder="Share of property(%)"> </div> </div>';  
         $('.add_let_out_prop').append(reportRow55);                           
    });

   //let out end


    var propid = 0; var flag = 0; var newpropflag = 0;	var	newpropcount =0;
	 $(document).on("click",".add_purchase_improv_btn", function(){
        newpropflag = parseInt($(this).attr('newpropflag'));
        if(newpropflag == 1){
        	newpropcount++; 
            propid = 'N_'+newpropcount;
            }
            else
            {
            	var  propid = $(this).attr('propid');
           }
	 	if(flag == 1){propid = 'N_'+newpropcount; }else{var	propid = $(this).attr('propid'); }
   	   
        reportRow5 = '<br><br><br><input type="hidden" name="hidcheckpurchimpro[]" value="0"/> <input type="hidden" name="hidcheck_subpart_propid[]" value="'+propid+'"/> <div class="form-group"> <label class="control-label col-sm-3" for="">Date of purchase</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_datopur[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Purchase cost</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_cost[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Type of capital gain</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_tyocg[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Index cost of purchase</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_indcoprch[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Cost of improvement</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_cosoimp[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Date of improvement</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_dateoimp[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Index cost of improvement</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_indcooimp[]" placeholder="" required> </div> </div>';  
        $(this).parent().parent().prev().append(reportRow5);                         
    });
  
    $(".add_detail_of_otherproperty_btn").click(function () {
    	propid = 0;
    	newpropcount++;
    	if (newpropcount > 0) {propid = 'N_'+newpropcount; var npid = propid; flag = 1;}
	   	 reportRow6 = '<br><br><br><input type="hidden" name="hidcheckslndorprop[]" value="'+propid+'"/> <div class="form-group"> <label class="control-label col-sm-5" for="">Sale details</label> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Sale date">Date of Sale</label> <div class="col-sm-9"> <input type="text" class="form-control" id="dateSal" name="sallndpro_sdt_dateosal[]"  placeholder="Sale date" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Sale value</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_sdt_salvalue[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Expenses for sale</label> <div class="col-sm-9"> <input type="text" class="" name="sallndpro_sdt_expfosal[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Stamp duty value</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_sdt_stmpduval[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Sale price considered</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_sdt_slprconsi[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Decription of asset sold</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_sdt_decassold[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-5" for="">Purchase and improvement</label> </div> <!-- raj add_purchase_improv_div in insert land --> <div class="add_purchase_improv_div"> <input type="hidden" name="hidcheckpurchimpro[]" value="0"/> <input type="hidden" name="hidcheck_subpart_propid[]" value="'+propid+'"/> <div class="form-group"> <label class="control-label col-sm-3" for="">Date of purchase</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_datopur[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Purchase cost</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_cost[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Type of capital gain</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_tyocg[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Index cost of purchase</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_indcoprch[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Cost of improvement</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_cosoimp[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Date of improvement</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_dateoimp[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Index cost of improvement</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_purimp_indcooimp[]" placeholder="" required> </div> </div> </div> <div class="form-group"> <label class="control-label col-sm-3 col-sm-offset-2" for=""> <button type="button" class="btn icon-btn btn-success add_purchase_improv_btn" propid="'+npid+'" ><span class="glyphicon btn-glyphicon glyphicon-plus"></span>Add other cost-of improvement</button> </label> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Total Purchase & Improvement cost</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_topuimpcost[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Capital Gain</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_cg[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Tax on capital gain</label> <div class="col-sm-9"> <input type="text" class="form-control" id=""  name="sallndpro_taxoncg[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-5" for="">Tax Saving Investments:-(Capital gains)</label> <div class="col-sm-3"> <label class="radio-inline"> <input type="radio" name="sallndpro_taxsavinvest" value="Yes" checked>Yes </label> <label class="radio-inline"> <input type="radio" name="sallndpro_taxsavinvest"  value="No">No </label> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54</label> <div class="col-sm-9"> <select name="sallndpro_invundsec54[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54EC</label> <div class="col-sm-9"> <select name="sallndpro_invundsec54ec[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54EE</label> <div class="col-sm-9"> <select name="sallndpro_invundsec54ee[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54F</label> <div class="col-sm-9"> <select name="sallndpro_invundsec54f[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div>';  
	     $('.add_detail_of_otherproperty_div').append(reportRow6);                           
    });
  

    $(".add_cg_saleomutualfund_btn").click(function () {
	   	 reportRow7 = '<br><br><br><input type="hidden" name="hidcheckslmutfnd[]" value="0"/> <div class="form-group"> <label class="control-label col-sm-3" for="">Type of mutual fund</label> <div class="col-sm-9"> <select name="somuf_typemutf[]" class="form-control"> <option value="Equity oriented">Equity oriented</option> <option value="Other then equity">Other then equity</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Description of asset sold</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="somuf_decoasssold[]" placeholder="" required> </div> </div> <div class="row"> <div class="col-md-4 col-md-offset-3"> <div class="form-group"> <label class="control-label col-sm-12" for="">Sale details</label> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Sale date</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="somuf_saldet_saledate[]" placeholder="" required> </div> </div> </div> <div class="col-md-4"> <div class="form-group"> <label class="control-label col-sm-12" for="">Purchase details</label> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Purchase date</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="somuf_purcdet_purcdate[]" placeholder="" required> </div> </div> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-6"> <input type="text" class="form-control" id="" name="aaa" placeholder="Long Term captal Gain" required readonly> </div> </div> <div class="row"> <div class="col-md-4 col-md-offset-3"> <div class="form-group"> <label class="control-label col-sm-7" for="">Sale value</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="somuf_saldet_salevalue[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Expenses on sale</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="somuf_saldet_exponsale[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Security Transaction Tax</label> <div class="col-sm-5"> <select name="somuf_saldet_sectratax[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">STT paid</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="somuf_saldet_sttpaid[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Option 1</label> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Capital gain without benefit of indexation</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="somuf_op1_cgwoutbeindx[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Tax on capital gain@10%</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="somuf_taxocg10p[]" placeholder="" required> </div> </div> </div> <div class="col-md-4"> <div class="form-group"> <label class="control-label col-sm-7" for="">Purchase price</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="somuf_purcdet_purcprice[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Indexed cost of purchase</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="somuf_purcdet_indcopurc[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Capital gain without benefit of indexation</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="somuf_purcdet_cgwoutbeindx[]" placeholder="" required> </div> </div> <br> <br> <div class="form-group"> <label class="control-label col-sm-7" for="">Option 2</label> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Capital gain with indexation</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="somuf_op2_cgwithindx[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Tax on capital gain@20%</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="somuf_taxocg20p[]" placeholder="" required> </div> </div> </div> </div> <div class="form-group"> <label class="control-label col-sm-5" for="">Tax Saving Investments:-(Capital gains)</label> <div class="col-sm-3"> <label class="radio-inline"> <input type="radio" name="somuf_taxsavinvest">Yes </label> <label class="radio-inline"> <input type="radio" name="somuf_taxsavinvest">No </label> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54</label> <div class="col-sm-9"> <select name="somuf_invundsec54[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54EC</label> <div class="col-sm-9"> <select name="somuf_invundsec54ec[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54EE</label> <div class="col-sm-9"> <select name="somuf_invundsec54ee[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54F</label> <div class="col-sm-9"> <select name="somuf_invundsec54f[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div>';  
	     $('.add_cg_saleomutualfund_div').append(reportRow7);                           
    });

    $(".add_cg_saleshaodeb_btn").click(function () {
         reportRow8 = '<br><br><br><input type="hidden" name="hidcheckslshdeb[]" value="0"/> <div class="form-group"> <label class="control-label col-sm-3" for="">Type of share</label> <div class="col-sm-9"> <select name="sosh_typeosha[]" class="form-control"> <option value="Listed share">Listed share</option> <option value="Non listed share">Non listed share</option> <option value="Listed debentures">Listed debentures</option> <option value="Non listed debentures">Non listed debentures</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Description of asset sold</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="sosh_decoasssold[]" placeholder="" required> </div> </div> <div class="row"> <div class="col-md-4 col-md-offset-3"> <div class="form-group"> <label class="control-label col-sm-12" for="">Sale details</label> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Sale date</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sosh_saldet_saledate[]" placeholder="" required> </div> </div> </div> <div class="col-md-4"> <div class="form-group"> <label class="control-label col-sm-12" for="">Purchase details</label> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Purchase date</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sosh_purcdet_purcdate[]" placeholder="" required> </div> </div> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-6"> <input type="text" class="form-control" id="" name="lll" placeholder="Long Term captal Gain" required readonly> </div> </div> <div class="row"> <div class="col-md-4 col-md-offset-3"> <div class="form-group"> <label class="control-label col-sm-7" for="">Sale value</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sosh_saldet_salevalue[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Expenses on sale</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sosh_saldet_exponsale[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Security Transaction Tax</label> <div class="col-sm-5"> <select name="sosh_saldet_sectratax[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">STT paid</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sosh_saldet_sttpaid[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Option 1</label> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Capital gain without benefit of indexation</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sosh_op1_cgwoutbeindx[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Tax on capital gain@10%</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sosh_taxocg10p[]" placeholder="" required> </div> </div> </div> <div class="col-md-4"> <div class="form-group"> <label class="control-label col-sm-7" for="">Purchase price</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sosh_purcdet_purcprice[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Indexed cost of purchase</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sosh_purcdet_indcopurc[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Capital gain without benefit of indexation</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sosh_purcdet_cgwoutbeindx[]" placeholder="" required> </div> </div> <br> <br> <div class="form-group"> <label class="control-label col-sm-7" for="">Option 2</label> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Capital gain with indexation</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sosh_op2_cgwithindx[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Tax on capital gain@20%</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sosh_taxocg20p[]" placeholder="" required> </div> </div> </div> </div> <div class="form-group"> <label class="control-label col-sm-5" for="">Tax Saving Investments:-(Capital gains)</label> <div class="col-sm-3"> <label class="radio-inline"> <input type="radio" name="sosh_taxsavinvest" value="Yes">Yes </label> <label class="radio-inline"> <input type="radio" name="sosh_taxsavinvest" value="No">No </label> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54</label> <div class="col-sm-9"> <select name="sosh_invundsec54[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54EC</label> <div class="col-sm-9"> <select name="sosh_invundsec54ec[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54EE</label> <div class="col-sm-9"> <select name="sosh_invundsec54ee[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54F</label> <div class="col-sm-9"> <select name="sosh_invundsec54f[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div>';  
         $('.add_cg_saleshaodeb_div').append(reportRow8);                           
    });
    
    $(".add_cg_saleotherassets_btn").click(function () {
         reportRow9 = '<br><br><br><input type="hidden" name="hidcheckslothass[]" value="0"/> <div class="form-group"> <label class="control-label col-sm-3" for="">Description of asset sold</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="soa_decoasssold[]" placeholder="" required> </div> </div> <div class="row"> <div class="col-md-4 col-md-offset-3"> <div class="form-group"> <label class="control-label col-sm-12" for="">Sale details</label> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Sale date</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="soa_saldet_saledate[]" placeholder="" required> </div> </div> </div> <div class="col-md-4"> <div class="form-group"> <label class="control-label col-sm-12" for="">Purchase details</label> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Purchase date</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="soa_purcdet_purcdate[]" placeholder="" required> </div> </div> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-6"> <input type="text" class="form-control" id=""  placeholder="Long Term captal Gain" required readonly> </div> </div> <div class="row"> <div class="col-md-4 col-md-offset-3"> <div class="form-group"> <label class="control-label col-sm-7" for="">Sale price</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="soa_saldet_saleprice[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Expenses on sale</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="soa_saldet_exponsale[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Security Transaction Tax</label> <div class="col-sm-5"> <select name="soa_saldet_sectratax[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> </div> <div class="col-md-4"> <div class="form-group"> <label class="control-label col-sm-7" for="">Purchase cost</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="soa_purcdet_purccost[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Indexed cost of purchase</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="soa_purcdet_indcopurc[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Cost of improvement</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="soa_purcdet_cosoimpro[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-7" for="">Indexed cost of improvement</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="soa_purcdet_indcosoimpro[]" placeholder="" required> </div> </div> </div> </div> <div class="col-md-9 col-sm-offset-3"> <div class="form-group"> <label class="control-label col-sm-3" for="">Capital gain</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="soa_capgain[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Tax on capital gain</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="soa_taxoncapgain[]" placeholder="" required> </div> </div> </div> <div class="form-group"> <label class="control-label col-sm-5" for="">Tax Saving Investments:-(Capital gains)</label> <div class="col-sm-3"> <label class="radio-inline"> <input type="radio" name="soa_taxsavinvest" value="Yes">Yes </label> <label class="radio-inline"> <input type="radio" name="soa_taxsavinvest" value="No">No </label> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54</label> <div class="col-sm-9"> <select name="soa_invundsec54[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54EC</label> <div class="col-sm-9"> <select name="soa_invundsec54ec[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54EE</label> <div class="col-sm-9"> <select name="soa_invundsec54ee[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Investment under section 54F</label> <div class="col-sm-9"> <select name="soa_invundsec54f[]" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div>';  
         $('.add_cg_saleotherassets_div').append(reportRow9);                           
    });
    
    $(".add_sou_bop_btn").click(function () {
	   	 reportRow10 = '<br><br><br><input type="hidden" name="hidchecksoubop[]" value="0"/> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Nature of income</label> </div> <div class="col-sm-5"> <select name="sou_bop_natuoinco[]" id="" class="form-control"> <option value="">Select</option> <option value="Profession">Profession</option> <option value="Business">Business</option> </select> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Address">Financial Particulars</label> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Sundry debtors">Sundry debtors</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sou_bop_fp_sundeb[]" placeholder="Sundry debtors"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Sundry Creditors">Sundry Creditors</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sou_bop_fp_suncre[]" placeholder="Sundry Creditors"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Stock in trade">Stock in trade</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sou_bop_fp_stoitra[]" placeholder="Stock in trade"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Cash balance">Cash balance</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sou_bop_fp_casbal[]" placeholder="Cash balance"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Co-Owner Details">Profit Particulars</label> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Gross Receipt">Gross Receipt</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sou_bop_pp_grorec[]" placeholder="Gross Receipt"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Direct Expenses">Direct Expenses</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sou_bop_pp_direxp[]" placeholder="Direct Expenses"> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Gross Profit">Gross Profit</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sou_bop_pp_groprof[]" placeholder="Gross Profit"> </div> </div> <div class="form-group"> <label class="control-label col-sm-4" for="Indirect Expenses">Indirect Expenses</label> <div class="col-sm-5 col-sm-offset-2"> <input type="text" class="form-control" id="" name="sou_bop_pp_indexp[]" placeholder="Indirect Expenses" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-4" for="Net Profit">Net Profit</label> <div class="col-sm-5 col-sm-offset-2"> <input type="text" class="form-control" id="" name="sou_bop_pp_netprof[]" placeholder="Net Profit" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Income From Business">Income from Business or Profession</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="sou_bop_incfbusoprof[]" placeholder="Income from Business or Profession" required> </div> </div>';  
	     $('.add_sou_bop_div').append(reportRow10);                           
    });
    
    $(".add_presumptive_vechile_btn").click(function () {
         reportRow11 = '<tr> <td> <input type="hidden" name="hidcheckpervec[]" value="0"/>**</td> <td> <input type="text" class="form-control" id="" name="pre_44ae_holdvechile[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="pre_44ae_incearned[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="pre_44ae_demedinco[]" readonly> </td> </tr>';  
         $('.add_presumptive_vechile_div').append(reportRow11);                           
    });
    
    $(".add_partner_ship_btn").click(function () {
	   	 reportRow12 = '<br><br><br><input type="hidden" name="hidcheckpts[]" value="0"/><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Name of the partnership firm</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="sou_pts_nameoptsfir[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">PAN of the partnership firm</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="sou_pts_panoptsfir[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">The firm is liable for audit</label> </div><div class="col-sm-5"> <select name="sou_pts_firlibaud[]" id="" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Cash balance">Transfer price audit is applicable for the firm</label> </div><div class="col-sm-5"> <select name="sou_pts_trprauapfir[]" id="" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="Gross Receipt">Capital balance of the firm as on 31<sup>st</sup> March</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="sou_pts_capbal31mar[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Share of profit(%)</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="sou_pts_shaoproft[]" placeholder=""> </div></div><div class="col-sm-9 col-sm-offset-2"> <div class="form-group"> <label class="control-label col-sm-5" for="Co-Owner Details">Income Received from the Firm</label> </div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Interest on capital</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="sou_pts_invoncap[]" placeholder=""> </div></div><div class="form-group"> <label class="control-label col-sm-4" for="">Remuneration</label> <div class="col-sm-5 col-sm-offset-2"> <input type="text" class="form-control" id="" name="sou_pts_remunera[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-4" for="">Profit / Loss</label> <div class="col-sm-5 col-sm-offset-2"> <input type="text" class="form-control" id="" name="sou_pts_prftloss[]" placeholder="" required> </div></div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Expenses (if any)</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="sou_pts_expenses[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Expencess Eligible for deduction U/S 35AC</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="sou_pts_exp35ac[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Net taxable income received from partnership firm</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="sou_pts_nettaxincrec[]" placeholder="" required> </div></div></div>';  
	     $('.add_partner_ship_div').append(reportRow12);                           
    });
    

    $(".add_foreign_assets_btn").click(function () {
	   	 reportRow13 = '<br><br><br><input type="hidden" name="hidcheckfoa[]" value="0"/><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Country</label> </div><div class="col-sm-5"> <select name="foa_fa_country[]" id="" class="form-control"> <option value="India">India</option> <option value="Other">Other</option> </select> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Bank Name</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_fa_bname[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Address of the bank</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_fa_addobank[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Zip code</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_fa_zipcod[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Account holder name</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_fa_acchname[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Ownership</label> </div><div class="col-sm-5"> <select name="foa_fa_onership[]" id="" class="form-control"> <option value="Owner">Owner</option> <option value="Benifical owner">Benifical owner</option> <option value="Benificary">Benificary</option> </select> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Account number</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_fa_accno[]" placeholder=""> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Account opening date</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_fa_accopdate[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">SWIFT/IBAN code</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_fa_swiftcode[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Peak balance during the year</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_fa_pakbdyear[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Interest accured during the year</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_fa_intaccdyear[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Taxable Interest</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_fa_txablint[]" placeholder="" required> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Schedule in which the Interest is disclosed</label> </div><div class="col-sm-5"> <select name="foa_fa_intdis[]" id="" class="form-control"> <option value="Foreign income">Foreign income</option> <option value="Other income">Other income</option> </select> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Item number in the schedule</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_fa_itmnosdu[]" placeholder="" required> </div></div>';  
	     $('.add_foreign_assets_div').append(reportRow13);                           
    });


    $(".add_financial_interest_btn").click(function () {
	   	 reportRow14 = '<br><br><br><input type="hidden" name="hidcheckfoafi[]" value="0"/><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Country</label> </div><div class="col-sm-5"> <select name="foa_fi_country[]" id="" class="form-control"> <option value="India">India</option> <option value="Other">Other</option> </select> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Zip code</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_fi_zipcod[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Nature of entity</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_fi_natuentity[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Name of entity</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_fi_nameentity[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Address of entity</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_fi_addentity[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Nature of interest</label> </div><div class="col-sm-5"> <select name="foa_fi_natuinte[]" id="" class="form-control"> <option value="Direct">Direct</option> <option value="Benifical owner">Benifical owner</option> <option value="Benificary">Benificary</option> </select> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Date since held</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_fi_datesh[]" placeholder=""> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Total Investment at cost</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_fi_toinvscost[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Interest accrued from such Interest</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_fi_intacc[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Nature of income</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_fi_natuoinc[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Amount taxable and offered in the return</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_fi_amttaxoff[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Schedule in which the Interest is disclosed</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_fi_sduintdisc[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Item number in the schedule</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_fi_itmnosdu[]" placeholder="" required> </div></div>';  
	     $('.add_financial_interest_div').append(reportRow14);                           
    });


    $(".add_immovable_property_btn").click(function () {
	   	 reportRow15 = '<br><br><br><input type="hidden" name="hidcheckfoaip[]" value="0"/><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Country</label> </div><div class="col-sm-5"> <select name="foa_ip_country[]" id="" class="form-control"> <option value="India">India</option> <option value="Other">Other</option> </select> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Zip code</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_ip_zipcod[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Address of the property</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_ip_addoprop[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Ownership</label> </div><div class="col-sm-5"> <select name="foa_ip_ownership[]" id="" class="form-control"> <option value="Direct">Direct</option> <option value="Benifical owner">Benifical owner</option> <option value="Benificary">Benificary</option> </select> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Date of acquisition</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_ip_dateoacqui[]" placeholder=""> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Total Investment at cost</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_ip_toinvscost[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Income from property</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_ip_incfprop[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Nature of income</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_ip_natuoinc[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Amount taxable and offered in the return</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_ip_amttaxoff[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Schedule in which the Income  is disclosed</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_ip_sduintdisc[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Item number in the schedule</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_ip_itmnosdu[]" placeholder="" required> </div></div>';  
	     $('.add_immovable_property_div').append(reportRow15);                           
    });


    $(".add_other_capital_assets_btn").click(function () {
	   	 reportRow16 = '<br><br><br><input type="hidden" name="hidcheckfoaoca[]" value="0"/><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Country</label> </div><div class="col-sm-5"> <select name="foa_oca_country[]" id="" class="form-control"> <option value="India">India</option> <option value="Other">Other</option> </select> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Zip code</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_oca_zipcod[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Nature of asset</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_oca_natuoass[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Ownership</label> </div><div class="col-sm-5"> <select name="foa_oca_ownership[]" id="" class="form-control"> <option value="Direct">Direct</option> <option value="Benifical owner">Benifical owner</option> <option value="Benificary">Benificary</option> </select> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Date of acquisition</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_oca_dateoacqui[]" placeholder=""> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Total Investment at cost</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_oca_toinvscost[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Income derived from the asset</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_oca_incderisasset[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Nature of income</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_oca_natuoinc[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Amount taxable and offered in the return</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_oca_amttaxoff[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Schedule in which the Income is disclosed</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_oca_sduintdisc[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Item number in the schedule</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_oca_itmnosdu[]" placeholder="" required> </div></div>';  
	     $('.add_other_capital_assets_div').append(reportRow16);                           
    });


    $(".add_signing_authority_btn").click(function () {
	   	 reportRow17 = '<br><br><br><input type="hidden" name="hidcheckfoasig[]" value="0"/><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Name of the institution in which you have sign in authority</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_sig_nameoinstitu[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Address of institution</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_sig_addoinstitu[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Country</label> </div><div class="col-sm-5"> <select name="foa_sig_country[]" id="" class="form-control"> <option value="India">India</option> <option value="Other">Other</option> </select> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Zip code</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_sig_zipcod[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Name of the account holder</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_sig_accholdname[]" placeholder=""> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Account number</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_sig_accno[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Peak balance / Investment during year</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_sig_pakbal[]" placeholder="" required> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Whether income accured is taxable in your hand</label> </div><div class="col-sm-5"> <select name="foa_sig_inctaxablinhand[]" id="" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Income accured in your account</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_sig_incinacc[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Amount taxable and offered in the return</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_sig_amttaxoff[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Schedule in which the Interest is disclosed</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_sig_sduintdisc[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Item number in the schedule</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_sig_itmnosdu[]" placeholder="" required> </div></div>';  
	     $('.add_signing_authority_div').append(reportRow17);                           
    });

    $(".add_details_of_trust_btn").click(function () {
	   	 reportRow18 = '<br><br><br><input type="hidden" name="hidcheckfoadot[]" value="0"/><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Country</label> </div><div class="col-sm-5"> <select name="foa_dot_country[]" id="" class="form-control"> <option value="India">India</option> <option value="Other">Other</option> </select> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Zip code</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_dot_zipcod[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Name of trust</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_dot_nameotrust[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Address of trust</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_dot_addotrust[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Name of trustees</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_dot_nameotrustees[]" placeholder=""> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Address of trustees</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_dot_addotrustees[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Name of settlor</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_dot_nameosettlor[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Address of settlor</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_dot_addosettlor[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Name of Beneficiaries</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_dot_nameobeni[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Address of Beneficiaries</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_dot_addobeni[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Date since position held</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_dot_dateposih[]" placeholder="" required> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Whether income derived is taxable in your hand</label> </div><div class="col-sm-5"> <select name="foa_dot_taxbleinhand[]" id="" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Income derived from trust</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_dot_incftrst[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Amount taxable and offered in the return</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_dot_amttaxoff[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Schedule in which the amount is disclosed</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_dot_sduamtdis[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Item number in the schedule</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_dot_itmnosdu[]" placeholder="" required> </div></div>';  
	     $('.add_details_of_trust_div').append(reportRow18);                           
    });


    $(".add_other_income_derived_btn").click(function () {
	   	 reportRow19 = '<br><br><br><input type="hidden" name="hidcheckfoaoid[]" value="0"/><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Country</label> </div><div class="col-sm-5"> <select name="foa_oid_country[]" id="" class="form-control"> <option value="India">India</option> <option value="Other">Other</option> </select> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Zip code</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_oid_zipcod[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Name of the persion from whom derived</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_oid_nameopderi[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Address of the persion from whom derived</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_oid_addopderi[]" placeholder=""> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Income derived</label> </div><div class="col-sm-5"> <input type="text" class="form-control" id="" name="foa_oid_incderi[]" placeholder=""> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Nature of income</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_oid_natuoinc[]" placeholder="" required> </div></div><div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Whether income taxable in your hand</label> </div><div class="col-sm-5"> <select name="foa_oid_inctaxbleinhnd[]" id="" class="form-control"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Amount taxable and offered in the return</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_oid_amttaxoff[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Schedule in which the Interest is disclosed</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_oid_sduintdis[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-3" for="">Item number in the schedule</label> <div class="col-sm-8"> <input type="text" class="form-control" id="" name="foa_oid_itmnosdu[]" placeholder="" required> </div></div>';  
	     $('.add_other_income_derived_div').append(reportRow19);                           
    });

    $(".add_foreignincomee_btn").click(function () {
         reportRow20 = '<br><br><br><input type="hidden" name="hidcheckfaofoi[]" value="0"/> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Country</label> </div> <div class="col-sm-5"> <select name="sou_foi_coun[]" id="" class="form-control"> <option value="India">India</option> <option value="Other">Other</option> </select> </div> </div> <div class="form-group"> <div class="col-sm-4 col-sm-offset-2"> <label class="control-label" for="">Tax payers identification number</label> </div> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="sou_foi_taxpid[]" placeholder=""> </div> </div> <div class="form-group"> <div class="col-sm-3"> <label class="control-label" for="">Tax and Relief</label> </div> </div> <div class="form-group"> <table class="table table-bordered"> <thead> <tr> <th>Sl no.</th> <th>Heads of income</th> <th>Income from outside India</th> <th>tax Paid outside India</th> <th>Tax Payble in India</th> <th>Tax Relief available in India</th> <th>Relevant article of DATA</th> </tr> </thead> <tbody> <tr> <td>1</td> <td>Salary</td> <td> <input type="text" class="form-control" id="" name="sou_foi_sal_incoutind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_sal_taxpoutind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_sal_taxpinind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_sal_taxrelavainind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_sal_releartodata[]" placeholder=""> </td> </tr> <tr> <td>2</td> <td>House property</td> <td> <input type="text" class="form-control" id="" name="sou_foi_houp_incoutind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_houp_taxpoutind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_houp_taxpinind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_houp_taxrelavainind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_houp_releartodata[]" placeholder=""> </td> </tr> <tr> <td>3</td> <td>Business income</td> <td> <input type="text" class="form-control" id="" name="sou_foi_businc_incoutind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_businc_taxpoutind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_businc_taxpinind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_businc_taxrelavainind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_businc_releartodata[]" placeholder=""> </td> </tr> <tr> <td>4</td> <td>Capital gain</td> <td> <input type="text" class="form-control" id="" name="sou_foi_capg_incoutind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_capg_taxpoutind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_capg_taxpinind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_capg_taxrelavainind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_capg_releartodata[]" placeholder=""> </td> </tr> <tr> <td>5</td> <td>Other sources</td> <td> <input type="text" class="form-control" id="" name="sou_foi_otsou_incoutind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_otsou_taxpoutind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_otsou_taxpinind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_otsou_taxrelavainind[]" placeholder=""> </td> <td> <input type="text" class="form-control" id="" name="sou_foi_otsou_releartodata[]" placeholder=""> </td> </tr> </tbody> </table> </div>';  
         $('.add_foreignincomee_div').append(reportRow20);                           
    });

    $(".add_don100_btn").click(function () {
         reportRow21 = '<br><br><br><input type="hidden" name="hidcheckdon100[]" value="0"/> <div class="form-group"> <label class="control-label col-sm-7" for="">Charitable Donation (80G) with 100% deduction</label> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Donee name">Name of the Donee</label> <div class="col-sm-9"> <input type="text" class="form-control" id="doneName" name="dona_80g_dname[]" placeholder="Donee name" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="PAN of the Donee">PAN of the Donee</label> <div class="col-sm-9"> <input type="text" class="form-control" id="doneePAN" name="dona_80g_dpan[]" placeholder="PAN of the Donee" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Address">Address</label> <div class="col-sm-9"> <input type="text" class="form-control" id="address" name="dona_80g_daddr[]" placeholder="Address"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Town/City/District">Town/City/District</label> <div class="col-sm-9"> <input type="text" class="form-control" id="twnCityDist" name="dona_80g_dcity[]" placeholder="Town/City/District"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="State">State</label> <div class="col-sm-9"> <input type="text" class="form-control" id="yearImprovement" name="dona_80g_dstate[]" placeholder="State"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Pin code">Pin code</label> <div class="col-sm-9"> <input type="text" class="form-control" id="pinCode" name="dona_80g_dpincode[]" placeholder="Pin code"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Donation Amount">Donation Amount</label> <div class="col-sm-9"> <input type="text" class="form-control" id="donationAmount" name="dona_80g_damount[]" placeholder="Donation Amount"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Donation Eligibility</label> <div class="col-sm-9"> <select name="dona_80g_deligilibity[]" id="" class="form-control"> <option value="without qualifing limit">without qualifing limit</option> <option value=">with qualifing limit">with qualifing limit</option> </select> </div> </div>';  
         $('.add_don100_div').append(reportRow21);                           
    });

    $(".add_don50_btn").click(function () {
         reportRow22 = '<br><br><br><input type="hidden" name="hidcheckdon50[]" value="0"/> <div class="form-group"> <label class="control-label col-sm-7" for="">Charitable Donation (80G) with 50% deduction</label> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Donee name">Name of the Donee</label> <div class="col-sm-9"> <input type="text" class="form-control" id="doneName" name="dona_80g_dname[]" placeholder="Donee name" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="PAN of the Donee">PAN of the Donee</label> <div class="col-sm-9"> <input type="text" class="form-control" id="doneePAN" name="dona_80g_dpan[]" placeholder="PAN of the Donee" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Address">Address</label> <div class="col-sm-9"> <input type="text" class="form-control" id="address" name="dona_80g_daddr[]" placeholder="Address"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Town/City/District">Town/City/District</label> <div class="col-sm-9"> <input type="text" class="form-control" id="twnCityDist" name="dona_80g_dcity[]" placeholder="Town/City/District"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="State">State</label> <div class="col-sm-9"> <input type="text" class="form-control" id="yearImprovement" name="dona_80g_dstate[]" placeholder="State"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Pin code">Pin code</label> <div class="col-sm-9"> <input type="text" class="form-control" id="pinCode" name="dona_80g_dpincode[]" placeholder="Pin code"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="Donation Amount">Donation Amount</label> <div class="col-sm-9"> <input type="text" class="form-control" id="donationAmount" name="dona_80g_damount[]" placeholder="Donation Amount"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Donation Eligibility</label> <div class="col-sm-9"> <select name="dona_80g_deligilibity[]" id="" class="form-control"> <option value="without qualifing limit">without qualifing limit</option> <option value=">with qualifing limit">with qualifing limit</option> </select> </div> </div>';  
         $('.add_don50_div').append(reportRow22);                           
    });

    $(".add_taxrecotds_btn").click(function () {
         reportRow23 = '<br><br><br><input type="hidden" name="hidchecktdsothsal[]" value="0"/> <div class="form-group"> <label class="control-label col-sm-3" for="">TAN of the deductor</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_tdsothsal_tanoded[]" placeholder="TAN"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Name Of the deductor</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_tdsonsal_nameodedu[]" placeholder=""> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">TDS deducted</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_tdsothsal_tdsdeduc[]" placeholder=""> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">TDS claimed</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_tdsothsal_tdsclaim[]"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Gross receipts as per 26AS</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_tdsothsal_rec26as[]" placeholder="" required> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Year in which TDS deducted</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_tdsothsal_yeartdsdedu[]" placeholder="YYYY"> </div> </div>';  
         $('.add_taxrecotds_div').append(reportRow23);                           
    });

    $(".add_taxrecotdsimoprop_btn").click(function () {
         reportRow24 = '<br><br><br><input type="hidden" name="hidcheckimoprop[]" value="0"/> <div class="form-group"> <label class="control-label col-sm-3" for="">PAN of the buyer</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_tdsimoprop_panobuyer[]" placeholder="TAN"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Name Of the buyer</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_tdsimoprop_nameobuyer[]" placeholder=""> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">TDS deducted</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_tdsimoprop_tdsdeduct[]" placeholder=""> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">TDS claimed</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_tdsimoprop_tdsclaimed[]" /> </div> </div>';  
         $('.add_taxrecotdsimoprop_div').append(reportRow24);                           
    });
 
    $(".add_taxrecotaxpaidadvan_btn").click(function () {
         reportRow25 = '<br><br><br><input type="hidden" name="hidchecktaxadvan[]" value="0"/> <div class="form-group"> <label class="control-label col-sm-3" for="">BSR code of the bank</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_txpaidadv_bsrcodobnk[]" placeholder=""> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Date of deposit</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_txpaidadv_dateodepos[]" placeholder=""> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Challan Serial Number</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_txpaidadv_challsrno[]"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Amount</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_txpaidadv_amount[]"> </div> </div>';  
         $('.add_taxrecotaxpaidadvan_div').append(reportRow25);                           
    });
    
    $(".add_taxrecoselftxpid_btn").click(function () {
         reportRow26 = '<br><br><br><input type="hidden" name="hidchecselfasspd[]" value="0"/> <div class="form-group"> <label class="control-label col-sm-3" for="">BSR code of the bank</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_selfasstxpd_bsrcodobnk[]" placeholder=""> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Date of deposit</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_selfasstxpd_dateodepos[]" placeholder=""> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Challan Serial Number</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_selfasstxpd_challsrno[]"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Amount</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_selfasstxpd_amount[]"> </div> </div>';  
         $('.add_taxrecoselftxpid_div').append(reportRow26);                           
    });

    $(".add_taxreconcil_btn").click(function () {
         reportRow27 = '<br><br><br><input type="hidden" name="hidcheckreconcil[]" value="0"/> <div class="form-group"> <label class="control-label col-sm-3" for="">Name of the deductor</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_reconci_nameodeduc[]" placeholder=""> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">TAN of the deductor</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_reconci_tanodeduc[]" placeholder="TAN"> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Total amount credited</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_reconci_totamtcre[]" placeholder=""> </div> </div> <div class="form-group"> <label class="control-label col-sm-3" for="">Total TDS Deposited</label> <div class="col-sm-9"> <input type="text" class="form-control" id="" name="reco_reconci_tottdsdop[]" placeholder=""> </div> </div>';  
         $('.add_taxreconcil_div').append(reportRow27);                           
    });

    $(".add_taxfill_btn").click(function () {
	   	 reportRow28 = '<br><br><br><input type="hidden" name="hidchecktaxfilland[]" value="0"/> <div class="form-group"> <label class="control-label col-sm-5" for="">Flat/Door/Block no</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="tax_re_m50lakhs_fldoblno[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-5" for="">Area/location</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="tax_re_m50lakhs_areloc[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-5" for="">Town/City/District</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="tax_re_m50lakhs_tocidi[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-5" for="">State</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="tax_re_m50lakhs_state[]" placeholder="" required> </div></div><div class="form-group"> <label class="control-label col-sm-5" for="">Country</label> <div class="col-sm-5"> <input type="text" class="form-control" id="" name="tax_re_m50lakhs_count[]" placeholder="" required> </div></div>';  
	     $('.add_taxfill_div').append(reportRow28);                           
    });

/*****************************ITR************************************/        
	$('.scrollable').each(function () {
        var $this = $(this);
        $(this).ace_scroll({
            size: $this.attr('data-size') || 100,
            //styleClass: 'scroll-left scroll-margin scroll-thin scroll-dark scroll-light no-track scroll-visible'
        });
    });	
    
    var rowCount =  1;	//$('#condition_ids').val().split(',').length;
	var reportRow = '';
    
	$(".add_income").click(function () {
            reportRow = '<tr id="_tr_row_'+rowCount+'"><td width="1%" align="left"><b class="green">'+rowCount+'</b>.&nbsp;</td><td width="2%" align="left"><input type="text" name="_other_income_'+rowCount+'" id="_other_income_'+rowCount+'" value="" /></td><td width="2%" align="left"><input type="text" name="_other_income_amount_'+rowCount+'" id="_other_income_amount_'+rowCount+'" value="" /></td><td width="2%" align="left"><a title="Delete Condition" href="#" onclick="$(\'#_tr_row_'+rowCount+'\').remove();"><i class="ace-icon fa fa-times red"></i></a></td></tr>';  
             $("#other_income_table").removeClass("hide");  
             $('#_row_condition').append(reportRow);            
             rowCount++;   
           // alert(rowCount);               
    });
<?php } ?>	

<?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "uploadform" || $_SESSION[$CONFIG->sessionPrefix.'page_name'] == "itr_forms") { 
if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "itr_forms") $dotDot="../";
?>
	/**********************UPLOADER***************************************/
    	if(location.protocol == 'file:') alert("For retrieving data from server, you should access this page using a webserver.");
	
	var uploader = new plupload.Uploader({
		runtimes : 'gears,html5,flash,silverlight,browserplus',
		browse_button : 'pickfiles',
		container : 'container',
		max_file_size : '20mb',
		url : '<?php echo $dotDot;?>ajax-request/upload.php',
		flash_swf_url : '<?php echo $CONFIG->siteurl;?>__UI.assets/plupload/js/plupload.flash.swf',
		silverlight_xap_url : '<?php echo $CONFIG->siteurl;?>__UI.assets/plupload/js/plupload.silverlight.xap',
		filters : [
			{title : "PDF Files", extensions : "pdf"}
		],
		resize : {width : 320, height : 240, quality : 90}
	});
	
	uploader.bind('Init', function(up, params) {
		//$('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
	});
	
	uploader.bind('BeforeUpload', function(up, files) {
		$('input.'+files.id).attr('readonly','readonly');
		up.settings.multipart_params = {"custom_name": $('input.'+files.id).val() };
        $('#pickfiles').addClass('hide');
	});

	$('#uploadfiles').click(function(e) {
		uploader.start();
		e.preventDefault();
	});

	uploader.init();

	uploader.bind('FilesAdded', function(up, files) {
		$('#filelist').html('');
		if(files.length<=10)
		{
			$.each(files, function(i, file) {
			
				$('#filelist').append(
					'<div class="upload_box"><div id="' + file.id + '" class="file_list"><p class="first"><strong>File Name:</strong> ' +
					file.name + ' </p><p><strong>File Size:</strong> ' + plupload.formatSize(file.size) + '</p><div class="progress"><div class="bar bar-success bar_'+file.id+'"><b></b></div></div><div style="margin-top:10px;"><input type="hidden" style="width:94%"/ class="' + file.id + '"></div></div></div>');
			});
            uploader.start();
		}
		else
		{
			up.splice('0',(files.length+1));
			alert('Please Select 1 Documents.');
		}

		up.refresh(); // Reposition Flash/Silverlight
	});

	uploader.bind('UploadComplete', function(up, file) {
	
		var total_files_uploaded = file.length;
		//$('#filelist').prepend('<p><b>'+total_files_uploaded+' File(s) Uploaded Successfully</b></p>');
		up.splice('0',file.length);
       var salary_id = $("input[name='hidchecksalary[]']").val();
        <?php if($CONFIG->loggedUserId != '') { ?>               
                $('#fetchProgressbarInner').css('width',"99%");               
               location.href='<?php echo $CONFIG->siteurl;?>mySaveTax/?salary_id='+salary_id+'&formsDataID='+$('#form_data_id').val()+'&module_interface=<?php echo $commonFunction->setPage('middle_layer_redirection'); ?>';
		<?php }else{ ?>
            $('#signup').addClass('show');
        <?php } ?>          
	});

	uploader.bind('UploadProgress', function(up, file) {
		$('#' + file.id + " b").html(file.percent + "%");
		$('.bar_'+file.id).css('width',file.percent+"%");
        if(file.percent == 100)
        {
			<?php if($CONFIG->loggedUserId) { ?>               
                $('#form_text').addClass('show');
            <?php }else{ ?>
                $('#signup').addClass('show');
            <?php } ?>        
        }
	});

	uploader.bind('Error', function(up, err) {
		$('#filelist').append("<div>Error Message: " + err.message +
			(err.file ? ", File: " + err.file.name : "") +
			"</div>"
		);
		up.refresh(); // Reposition Flash/Silverlight
	});
	
	uploader.bind('FileUploaded', function(up, file,info) {
        $('#' + file.id + " b").html("100%");		
        //$('#' + file.id).parent().css('display','none');
        var response = JSON.parse(info.response);
        $('#form_data_id').val(response.id);
        //alert(response.id);
	});
    /**********************UPLOADER***************************************/
    
    
   <?php } ?>
});
<?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "profile") { ?>
	function ajaxCallKeyValue(key,value)
	{
		$.ajax( {
		  type: "POST",
		  url: "../ajax-request/post_login_response.php",
		  data: {"name":key,"value":value},
		  success: function( response ) {
			$('#package_response').append(response);
		  }
		});
	}
    function calcAge(getDOB)
    {
        var mdate = getDOB.toString();
        var yearThen = parseInt(mdate.substring(0,4), 10);
        var monthThen = parseInt(mdate.substring(5,7), 10);
        var dayThen = parseInt(mdate.substring(8,10), 10);
        
        var today = new Date();
        var birthday = new Date(yearThen, monthThen-1, dayThen);
        
        var differenceInMilisecond = today.valueOf() - birthday.valueOf();
        
        var year_age = Math.floor(differenceInMilisecond / 31536000000);
        var day_age = Math.floor((differenceInMilisecond % 31536000000) / 86400000);
        
        if ((today.getMonth() == birthday.getMonth()) && (today.getDate() == birthday.getDate())) {
           // alert("Happy B'day!!!");
        }
        
        var month_age = Math.floor(day_age/30);
        
        day_age = day_age % 30;
        
        if (isNaN(year_age) || isNaN(month_age) || isNaN(day_age)) {
            $("#calcAge").html("Invalid birthday - Please try again!");
        }
        else {
            $("#calcAge").html(year_age + " years " + month_age + " months " + day_age );
        }
    }
<?php } ?>


function signupJS(form)
{
            
    if(!(/^[0-9\-]+$/.test($("#mobile").val())))
    {
        $("#signup_invalid_email").html('Please enter valid mobile no.');
        $("#signup_invalid_email").css("display", "block");
        return false;
    }
    if($("#mobile").val().length != 10)
    {
        $("#signup_invalid_email").html('Please enter valid mobile no.');
        $("#signup_invalid_email").css("display", "block");
        return false;
    }
    if($("#reg_passwd").val().length < 6)
    {
        $("#signup_invalid_email").html('Password must be 6 character long.');
        $("#signup_invalid_email").css("display", "block");
        return false;
    }		
    if($("#reg_passwd").val() != $("#repasswd").val())
    {
        $("#signup_invalid_email").html('Password Mismatch.');
        $("#signup_invalid_email").css("display", "block");
        return false;
    }
            if($("#otp").val().length != 5) 
            {
                $("#signup_invalid_email").html('Please enter OTP.');
                $("#signup_invalid_email").css("display", "block");
                $("#otp").focus();
                return false;
            }
    $("#signup_invalid_email").html('');		
    $("#signup_loader").css("display", "block");	
    $("#signup_invalid_email").css("display", "none");
    $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize(),
        success: function(response) {
            if(response.includes('WRONG_PASSCODE'))
            {
                $("#verifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
                $("#signup_loader").css("display", "none");
                $("#signup_invalid_email").html('Invalid security code.');
                $("#signup_invalid_email").css("display", "block");
                $("#email").focus();
            }
            else if(response.includes('WRONG_OTP'))
            {
                $("#signup_invalid_email").html('Invalid OTP.');
                $("#signup_invalid_email").css("display", "block");
                $("#otp").focus();
            }
            else if(response.includes('REGISTER_DONE'))
            {									
                window.location.href='<?php echo $CONFIG->siteurl;?>mySaveTax/';				
            }
            else
            {
                $("#verifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
                $("#signup_loader").css("display", "none");
                $("#signup_invalid_email").html('Email Already Exists.');    
                $("#signup_invalid_email").css("display", "block");
                $("#email").focus();
            //html('Email Already Exists.');
            }
        }            
    });
    return false;
}
            
function sendOTPJS(form) 
{
    if((!(/^[0-9\-]+$/.test($("#mobile").val())))||($("#mobile").val().length != 10))
    {
        //$("#signup_invalid_mob").html('Please enter valid mobile no.');
        $("#signup_invalid_mob").css("display", "block");
        return false;
    }   
            else 
            {
            $("#signup_invalid_mob").css("display", "none");
            }
            
            $mobileno = $("#mobile").val();
            //alert($mobileno);
            
    $("#signup_invalid_mob").html('OTP is being sent... please wait...');       
    $("#signup_invalid_mob").css("display", "block");
    $.ajax({
        url: 'ajax-request/ajax_response.php?action=doSendOTP',
        type: 'POST',
        data: "mobile=" + $("#mobile").val(), //$(form).serialize(),
        success: function(response) {      
            //$("#signup_invalid_mob").html(response);  
            if(response.includes('OTP_SENT'))
            {   
                $("#signup_invalid_mob").html('OTP is sent.Please wait for 2 to 3 minutes before requesting new OTP.');       
                $("#signup_invalid_mob").css("display", "block");
            } else {
                $("#signup_invalid_mob").html(response);  
                $("#signup_invalid_mob").css("display", "block");
            }
        }            
    });
            return false;
            
}

function attachePAN(form)
{
	 $('#attach_status').html('<img src="<?php echo $CONFIG->staticURL.$CONFIG->theme;?>img/formsubmitpreloader.gif">');
	 $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize(),
        success: function(response) {
        	$('#attach_status').html(response);
        }            
    });
}

function ajaxFormSubmit(form,tabGroup,nxtTab)
{ 
    var $form = $(form);
    var msgRes = $form.find('.ajaxResClass');
    
    msgRes.html('<img src="<?php echo $CONFIG->staticURL.$CONFIG->theme;?>img/formsubmitpreloader.gif">');
    $.ajax({
        url: form.action,
        type: form.method,
        data: $form.serialize(),
        success: function(response) {
            
            msgRes.html("Data updated successfully.");
            
            var next_tab = $form.closest('.tab-pane').data('next-tab');
            
            if(next_tab)
            {
                $('a[href="'+next_tab+'"]').tab('show');
            }    
        }            
    });
}

function loginJS(form)
{		
    $("#login_loader").css("display", "block");	
    $("#login_invalid_email").css("display", "none");
    $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize(),
        success: function(response) {
            if(response.indexOf('PASS')==0)
                location.href='<?php echo $CONFIG->siteurl;?>mySaveTax/';
            else
            {
                $("#login_loader").css("display", "none");
                $("#login_invalid_email").css("display", "block");
                $("#email").focus();
            }
        }            
    });
    return false;
}		
function doResetPasswordJS(form)
{		
    $("#reset_invalid_email").html('');		
    $("#reset_loader").css("display", "block");	
    $("#reset_invalid_email").css("display", "none");
    $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize(),
        success: function(response) {
            if(response.indexOf('RESET_DONE')==0)
            {			
                $("#reset_email").val('');		
                $("#reset_sec_code").val('');	
                $("#resetverifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
                $("#reset_loader").css("display", "none");
                $("#reset_invalid_email").html('Please check your mail, to change password.');
                $("#reset_invalid_email").css("display", "block");
            }
            else if(response.indexOf('WRONG_PASSCODE')==0)
            {
                $("#resetverifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
                $("#reset_loader").css("display", "none");
                $("#reset_invalid_email").html('Wrong security code.');
                $("#reset_invalid_email").css("display", "block");
                $("#reset_sec_code").focus();
            }				
            else
            {
                $("#resetverifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
                $("#reset_loader").css("display", "none");
                $("#reset_invalid_email").html('Email Not Found.');
                $("#reset_invalid_email").css("display", "block");
                $("#reset_email").focus();
            }
        }            
    });
    return false;
}		
function doRecoverPasswordJS(form)
{		
	if($("#form_pass").val().length < 6)
    {
        $("#reset_invalid_email").html('Password must be 6 character long.');
        $("#reset_invalid_email").css("display", "block");
        return false;
    }	
	if($("#form_pass").val() != $("#form_cpass").val())
    {
        $("#reset_invalid_email").html('Password and Confirm Password Mismatch.');
        $("#reset_invalid_email").css("display", "block");
        return false;
    }
    $("#reset_invalid_email").html('');		
    $("#reset_loader").css("display", "block");	
    $("#reset_invalid_email").css("display", "none");
    $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize(),
        success: function(response) {
            if(response.indexOf('PASSWORD_CHANGED')==0)
            {			
                $("#reset_email").val('');		
                $("#reset_sec_code").val('');	
                $("#resetCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
                $("#reset_loader").css("display", "none");
                $("#reset_invalid_email").html('Password has been changed successfully.');
                $("#reset_invalid_email").css("display", "block");
            }
            else if(response.indexOf('WRONG_PASSCODE')==0)
            {
                $("#resetCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
                $("#reset_loader").css("display", "none");
                $("#reset_invalid_email").html('Wrong security code.');
                $("#reset_invalid_email").css("display", "block");
                $("#reset_sec_code").focus();
            }				
            else
            {
                $("#resetCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
                $("#reset_loader").css("display", "none");
                $("#reset_invalid_email").html('Email Not Found.');
                $("#reset_invalid_email").css("display", "block");
                $("#reset_email").focus();
            }
        }            
    });
    return false;
}		
function randomString()
{
	var rsa = new RSAKey();								
    rsa.setPublic('83a1f4c17bfaca642ec309cbf5d575a8c311cecaf1cf943aa0eb656f71c957e23240282d01090c7d8e56b16d9957ade3acf31fda040d68fdb7b15cf8f6890513', '10001');
	var password=rsa.encrypt(document.getElementById('password').value);
	document.getElementById('password').value = password +'83a1f4'+"08a3d83c3641d199560a3d27edb72461";
}

function placeBSEOrder(form)
{
	$("#nav_error").css("display", "none");	//$('.error').addClass('hide');	//
    
    if($("#nav_amount").val() =='')
    {
    	$("#nav_error").css("display", "block");
         return false;
    }
    if(!(/^[0-9\-]+$/.test($("#nav_amount").val())))
    {
        $("#nav_error").css("display", "block");  
        return false;
    }
    if($("#nav_amount").val() >= $("#jsAMT").val())
    {
    	$("#nav_error").css("display", "none;");  //alert($("#nav_amount").val()); 
    }
    else
    {
       $("#nav_error").css("display", "block");     
        return false;
    }
    $("#bse_order_loading").removeClass("hide");
    $("#bse_order_response").html('');
	 $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize(),
        success: function(response) {
        	$("#bse_order_loading").addClass("hide");
            $("#bse_order_response").html(response);
            $("#bse_order_response").addClass("show");
        }            
    });
}

function drawChart() {

    var data = google.visualization.arrayToDataTable([
        ['Amount', 'Toatal'],
        ['Equity', 33],
        ['Debt', 77]
    ]);

    var options = {
        title: 'Investment allocation',
        left:20,
        top:0
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
}
function calc_time(time)
{
  var time_period;
  if (time == 1) {
    time_period = "years";
  }else if(time == 12){
    time_period = "months";
  }else if(time_period == 365 || time_period == 366){
    time_period = "days";
  }
  return time_period;
}


//Fixed deposit simple interest
function clc_simpleInt(principal,n,time,rate,aa)
{
  var  rate2 = parseFloat(rate)/100;
  var amount_interest = parseFloat(principal) * (1+ (parseFloat(rate2/aa)*time));
  return amount_interest;
}

// Recurring Deposit
function clc_recurrInt(monthlyInstallment,numberOfMonths,rateOfInterest)
{
  var frequency = Math.floor(numberOfMonths/3); // Quarterly
  var accumulateMonthlyAmount = parseInt(monthlyInstallment) * ((Math.pow(rateOfInterest / 400 + 1, frequency) - 1) / (1-(Math.pow(rateOfInterest / 400 + 1,(-1/3)))));
  var finalInterestGain = accumulateMonthlyAmount - monthlyInstallment * numberOfMonths;
  var depositedAmount = monthlyInstallment * numberOfMonths;
  return accumulateMonthlyAmount;
}

//Home loan
function clc_emi(P,n,r)
{
  var x = Math.pow(1 + r, n); //Math.pow computes powers
  var emi = (P*x*r)/(x-1);
  return emi;
}

//ssy function
function clc_SSY(principalVal,intRate)
{
  var amt = 0;
  for (var i = 1; i <= 21; i++)
  { var show = ''; 
    if(i>=1 &&i<=15)
    {
        amt = (amt + principalVal);
    }
    amt = Math.round(amt + (amt*intRate/100));
  }
  return amt;
}

//ppf function
function clc_PPF(principalVal,intRate,years)
{
  principalVal = parseFloat(principalVal);
  intRate      = parseFloat(intRate);
  years        = parseInt(years);
  var amt = 0;
  for (var i = 1; i <= years; i++)
  { var show = '';
    if(i>=1 &&i<=15)
    {
        amt = (amt + principalVal);
    }
    amt = Math.round(amt + Math.round((amt*intRate/100)));
  }
  return amt;
}
