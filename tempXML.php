<?php
	include("__lib.includes/config.inc.php");
	if(!($_SESSION['oPageAccess'])) { header("HTTP/1.1 401 Unauthorized");header("Location: $CONFIG->siteurl");exit;}
	ini_set("display_errors",1);
	error_reporting(E_ALL);
	//include("__lib.ajax/inc.summary_calc.php");

if(isset($_POST['xml-data']))
{
	
	$xml_string = '<?xml version="1.0" encoding="UTF-8" standalone="no"?><ITRETURN:ITR xmlns:ITRETURN="http://incometaxindiaefiling.gov.in/main" xmlns:ITR1FORM="http://incometaxindiaefiling.gov.in/ITR1" xmlns="http://incometaxindiaefiling.gov.in/master" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><ITR1FORM:ITR1><CreationInfo><SWVersionNo>101</SWVersionNo><SWCreatedBy>SW10001628</SWCreatedBy><XMLCreatedBy>SW10001628</XMLCreatedBy><XMLCreationDate>'.date("Y-m-d").'</XMLCreationDate><IntermediaryCity>BANGALORE</IntermediaryCity><Digest>-</Digest></CreationInfo><Form_ITR1><FormName>ITR-1</FormName><Description>For Indls having Income from Salary, Pension, family pension and Interest</Description><AssessmentYear>2018</AssessmentYear><SchemaVer>Ver1.1</SchemaVer><FormVer>Ver1.0</FormVer></Form_ITR1>'.$_POST['xml-data'].'</ITR1FORM:ITR1></ITRETURN:ITR>';
	
	//Replace & with &amp; to avoid XML validation issues
	$xml_string = str_replace("&","&amp;",$xml_string);
	
	$doc = new DOMDocument();
	$doc->preserveWhiteSpace = false;
	$doc->formatOutput = true;	
	$doc->loadXML($xml_string);	
	
	if(isset($_POST['pan']))
	{
		$xml_name = $_POST['pan']."_ITR-1_".date('Y')."_N_8824";
		
		$xml_path = "/home/itrxmls/".$xml_name.".xml";
		
		if($doc->save($xml_path))
		{
			$command = 'java -classpath .:"/home/lib/*:/usr/local/src" bulkItrService.BulkItrService_Client '.$xml_name.' 2>&1';
			
			try
			{
				ob_start();
				$output = shell_exec($command);
				ob_end_clean(); 	
				echo $output;					
			}
			catch(Exception $e)
			{
				echo 'Message: ' .$e->getMessage();
			}	
		}	
		else
		{
			echo "Error processing xml file";
		}		
	}
	else
	{
		header("Content-Type: application/xml");
		header('Content-Disposition: attachment; filename="ITR.xml"');
		
		echo $doc->saveXML();		
	}			
}
else
{
	echo 'Invalid request';
}	

?>