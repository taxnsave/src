<?php
	//echo "<pre>";print_r($_SESSION);
	//$bankInfo = $customerProfile->getCustomerBankInfo();
	//print_r($bankInfo);
	//$mutualFund->updatePrice();
?>
<div class="main-content">
    <div class="main-content-inner">
        <!-- #section:basics/content.breadcrumbs -->
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="<?php echo $CONFIG->siteurl;?>mySaveTax/">Home</a>
                </li>
                <li class="active">Mutual Fund</li><li class="active">Nav Master</li>
            </ul>
            <?php include("form.search.php");?>           
        </div>
        <div class="page-content">						
            <div class="row">
                <div class="col-xs-12">                            
                    <div class="row">
                        <div class="widget-box">
                            <div class="widget-header">
                                <h4 class="widget-title">Search</h4>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <form class="form-inline" method="get" action="">
                                   <label class="inline" style="padding:4px;">                                                
                                    <input type="text" class="ace" placeholder="Search......" id="search_str" name="search_str" value="<?php echo $_REQUEST[search_str];?>"  />
                                    <SELECT name="Scheme_Plan" id="Scheme_Plan">
                                    <option value="">-- Select Scheme Plan --</option>
                                    <?php
										$getSCHList		= $mutualFund->getSchemePlan();
										while(list($schKey,$schVal) = each($getSCHList))
										{
											if($schVal[Scheme_Plan] == $_REQUEST[Scheme_Plan])
												echo '<option value="'.$schVal[Scheme_Plan].'" SELECTED>'.$schVal[Scheme_Plan].'</option>';		
											else
												echo '<option value="'.$schVal[Scheme_Plan].'">'.$schVal[Scheme_Plan].'</option>';		
										}
									?>
                                    </SELECT>
                                     <SELECT name="Scheme_Type" id="Scheme_Type">
                                    <option value="">-- Select Scheme Type --</option>
                                    <?php
										$getSCHList		= $mutualFund->getSchemeType();
										while(list($schKey,$schVal) = each($getSCHList))
										{
											if($schVal[Scheme_Type] == $_REQUEST[Scheme_Type])
												echo '<option value="'.$schVal[Scheme_Type].'" SELECTED>'.$schVal[Scheme_Type].'</option>';
											else
												echo '<option value="'.$schVal[Scheme_Type].'">'.$schVal[Scheme_Type].'</option>';		
										}
									?>
                                    </SELECT>
                                    <SELECT name="Minimum_Purchase_Amount" id="Minimum_Purchase_Amount">
                                    <option value="">-- Select Min. Purchase Amount --</option>
                                    <?php
										$getSCHList		= $mutualFund->getPurchaseAmount();
										while(list($schKey,$schVal) = each($getSCHList))
										{
											if($schVal[Minimum_Purchase_Amount] > 1)
											{
												if($schVal[Minimum_Purchase_Amount] == $_REQUEST[Minimum_Purchase_Amount])											
													echo '<option value="'.$schVal[Minimum_Purchase_Amount].'" SELECTED>'.$schVal[Minimum_Purchase_Amount].'</option>';
												else
													echo '<option value="'.$schVal[Minimum_Purchase_Amount].'">'.$schVal[Minimum_Purchase_Amount].'</option>';		
											}
										}
									?>
                                    </SELECT>
                                    <input type="hidden" id="module_interface" name="module_interface" value="<?php echo $commonFunction->setPage('nav_master');?>" />
                                         </label>                                                   
                                        <button class="btn btn-info btn-sm" type="submit">
                                            <i class="ace-icon fa fa-search bigger-110"></i><strong>Search</strong>
                                        </button>
                                    </form>
                                </div>                                                										
                            </div>
                        </div>
                    </div>                              
                     <div class="space-4"></div>
                     <form class="form-horizontal" action="../ajax-request/admin_response.php?action=recomend_nav" method="POST" onSubmit="recomendNAV(this);return false;">
                    <div class="row">
                        <div class="col-xs-12">		
                            <div class="pull-left">
                            	 <button class="btn btn-inverse btn-xs" type="submit">Recomend MF</button>
                               	 <!--<input type="text" name="userid" value="" placeholder="Enter User Name" class="input-large" />
                                 <button class="btn btn-inverse btn-xs">Recomend To User</button>-->
                                 <label class="red" id="recomend_status"></label>
                            </div>
                        </div>
                    </div>
                     <div class="space-4"></div>
                    <div class="row">
     <table id="simple-table" class="table table-bordered table-striped table-hover dataTable" style="cursor: pointer;">
         <thead class="thin-border-bottom">                                                 
            <tr>
            	<th><input name="master-field-checkbox" class="" type="checkbox"></th>
                <th <?php echo $commonFunction->orderByToggle($_REQUEST,'mf_nav_master.Unique_No','nav_master'); ?> >Unique No.</th>																
                <th <?php echo $commonFunction->orderByToggle($_REQUEST,'mf_nav_master.Scheme_Code','nav_master'); ?>>Scheme Code</th>
                <th <?php echo $commonFunction->orderByToggle($_REQUEST,'mf_nav_master.Scheme_Name','nav_master'); ?>>Scheme Name</th>
                <th <?php echo $commonFunction->orderByToggle($_REQUEST,'mf_nav_price.net_asset_value','nav_master'); ?>>Asset Value</th>	
                <th <?php echo $commonFunction->orderByToggle($_REQUEST,'mf_nav_price.repurchase_price','nav_master'); ?>>Repurchase Price</th>	
                 <th <?php echo $commonFunction->orderByToggle($_REQUEST,'mf_nav_price.sale_price','nav_master'); ?>>Sale Price</th>																
                <th <?php echo $commonFunction->orderByToggle($_REQUEST,'mf_nav_master.ISIN','nav_master'); ?>>ISIN</th>																
                <th <?php echo $commonFunction->orderByToggle($_REQUEST,'mf_nav_master.Scheme_Type','nav_master'); ?>>Scheme Type</th>																
                <th <?php echo $commonFunction->orderByToggle($_REQUEST,'mf_nav_master.Scheme_Plan','nav_master'); ?>>Scheme Plan</th>
                <th <?php echo $commonFunction->orderByToggle($_REQUEST,'mf_nav_master.Purchase_Allowed','nav_master'); ?>>Puchase Status</th>
            </tr>
        </thead>
    <tbody>
<?php

	if($_REQUEST[whichPage] == '')
		$page = 1;
	else
		$page = $_REQUEST[whichPage];
	
	$currentPage  	= $page;
	$totalItems		= $mutualFund->navMasterListCount($_REQUEST);
	$itemsPerPage	= $CONFIG->paginationPageItem;
	$getMFList		= $mutualFund->navMasterList($_REQUEST,($currentPage*$itemsPerPage)-$itemsPerPage);
	
	if(in_array('MF_NONE',$getMFList))
		echo $fileHTML = '<tr><td class="center red" colspan="11"> No Row(s) Found.</td></tr>';
	else
	{				
		$urlPattern = '?whichPage=(:num)&search_str='.$_REQUEST['search_str'].'&module_interface='.$_REQUEST['module_interface'];
		$paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
		
		while(list($logKey,$logVal) = each($getMFList))
		{	
			$recomended = $buySell->isRecomended($logVal[pk_nav_id]);
			if($recomended)
				$recText = "Checked";
			else
				$recText = "";	 	
			
			$mfPrice = $buySell->getNAVAllPrices($logVal[pk_nav_id]);
			if($mfPrice[0][net_asset_value] > $mfPrice[1][net_asset_value])
			{
				$arrow = "success";
				$n = $mfPrice[0][net_asset_value] - $mfPrice[1][net_asset_value];
				$pn = $n/$mfPrice[1][net_asset_value]*100;
			}
			else
			{
				$arrow = "important";
				$n =  $mfPrice[1][net_asset_value] - $mfPrice[0][net_asset_value];
				$pn = $n/$mfPrice[1][net_asset_value]*100;
			}		
			//echo count($mfPrice).'<br><div class="infobox-container stat stat-success">8%</div>';print_r($mfPrice);
?>
        <tr title="Click To Expand">
        	<td><input name="recomend[]" type="checkbox" class="" value="<?php echo $logVal[pk_nav_id];?>" <?php echo $recText; ?> /></td>
            <td class="blue"><b class="blue"><?php echo str_replace('"','',$logVal[Unique_No]); ?></b></td>
            <td>
                <?php echo str_replace('"','',$logVal[Scheme_Code]); ?>
            </td>
             <td class="show-details-btn"><b class="orange"><?php echo str_replace('"','',$logVal[Scheme_Name]); ?></b></td>
            <td><div class="infobox" style="width:90px; background-color:#F5F5F5;"><div style="margin-top:20px;">
                <?php echo str_replace('"','',$logVal[net_asset_value]); ?>
            </div><div class="stat stat-<?php echo $arrow;?>"><?=number_format($pn,2);?>%</div></div></td>
            <td><?php echo str_replace('"','',$logVal[repurchase_price]); ?></td>
            <td><?php echo str_replace('"','',$logVal[sale_price]); ?></td>
            <td><?php echo str_replace('"','',$logVal[ISIN]); ?></td>
            <td><?php echo str_replace('"','',$logVal[Scheme_Type]); ?></td>
            <td><?php echo str_replace('"','',$logVal[Scheme_Plan]); ?></td>           
            <td><?php echo str_replace('"','',$logVal[Purchase_Allowed]); ?></td> 
        </tr>
        <tr class="detail-row">
        	<td colspan="11" bgcolor="#FFFFFF">
            	<div class="col-xs-12 col-sm-12">
                    <div class="space visible-xs"></div>    
                    <div class="profile-user-info profile-user-info-striped">
                    	<div class="profile-info-row">
                            <div class="profile-info-name"> RTA Scheme Code </div>
                            <div class="profile-info-value" style="vertical-align: middle;">
                                <span><?php echo str_replace('"','',$logVal[RTA_Scheme_Code]); ?></span>
                            </div>
                            <div class="profile-info-name"> AMC Scheme Code </div>
                            <div class="profile-info-value" style="vertical-align: middle;">                                
                                <span><?php echo str_replace('"','',$logVal[AMC_Scheme_Code]); ?></span>
                            </div>
                             <div class="profile-info-name"> RTA Agent Code </div>
                            <div class="profile-info-value" style="vertical-align: middle;">
                                <span><?php echo str_replace('"','',$logVal[RTA_Agent_Code]); ?></span>
                            </div>
                            <div class="profile-info-name"> Purchase Transaction Mode </div>
                            <div class="profile-info-value" style="vertical-align: middle;">
                                <span><?php echo str_replace('"','',$logVal[Purchase_Transaction_mode]); ?></span>
                            </div>
                        </div>
                        <div class="profile-info-row">                            
                            <div class="profile-info-name"> Minimum Purchase Amount </div>
                            <div class="profile-info-value" style="vertical-align: middle;">                                
                                <span><?php echo str_replace('"','',$logVal[Minimum_Purchase_Amount]); ?></span>
                            </div>
                             <div class="profile-info-name"> Additional Purchase Amount </div>
                            <div class="profile-info-value" style="vertical-align: middle;">
                                <span><?php echo str_replace('"','',$logVal[Additional_Purchase_Amount]); ?></span>
                            </div>
                             <div class="profile-info-name"> Maximum Purchase Amount </div>
                            <div class="profile-info-value" style="vertical-align: middle;">                                
                                <span><?php echo str_replace('"','',$logVal[Maximum_Purchase_Amount]); ?></span>
                            </div>
                            <div class="profile-info-name"> Purchase Amount Multiplier </div>
                            <div class="profile-info-value" style="vertical-align: middle;">                               
                                <span><?php echo str_replace('"','',$logVal[Purchase_Amount_Multiplier]); ?></span>
                            </div>
                        </div>
                        <div class="profile-info-row">                                                                                  
                             <div class="profile-info-name"> Redemption Allowed </div>
                            <div class="profile-info-value" style="vertical-align: middle;">                                
                                <span><?php echo str_replace('"','',$logVal[Redemption_Allowed]); ?></span>
                            </div>
                            <div class="profile-info-name"> Minimum Redemption Qty </div>    
                            <div class="profile-info-value" style="vertical-align: middle;">
                                <span><?php echo str_replace('"','',$logVal[Minimum_Redemption_Qty]); ?></span>
                            </div>
                            <div class="profile-info-name"> Redemption Qty Multiplier </div>    
                            <div class="profile-info-value" style="vertical-align: middle;">
                                <span><?php echo str_replace('"','',$logVal[Redemption_Qty_Multiplier]); ?></span>
                            </div>
                            <div class="profile-info-name"> Redemption Amount Minimum </div>
                            <div class="profile-info-value" style="vertical-align: middle;">
                                <span><?php echo str_replace('"','',$logVal[Redemption_Amount_Minimum]); ?></span>
                            </div>
                        </div>                                             
                        <div class="profile-info-row">                            
                            <div class="profile-info-name">Redemption Amount Maximum</div>
                            <div class="profile-info-value" style="vertical-align: middle;">
                                <span><?php echo str_replace('"','',$logVal[Redemption_Amount_Maximum]); ?></span>
                            </div>                             
                              <div class="profile-info-name"> Start Date</div>
                            <div class="profile-info-value" style="vertical-align: middle;">
                                <span><?php echo str_replace('"','',$logVal[Start_Date]); ?></span>
                            </div>
                            <div class="profile-info-name">End Date</div>
                            <div class="profile-info-value" style="vertical-align: middle;">
                                <span><?php echo str_replace('"','',$logVal[End_Date]); ?></span>
                            </div>
                            <div class="profile-info-name">Lock in Period Flag</div>
                            <div class="profile-info-value" style="vertical-align: middle;">
                                <span><?php echo str_replace('"','',$logVal[Lock_in_Period_Flag]); ?></span>
                            </div>
                        </div>                                              
                    </div>
                </div>
            </td>
        </tr>
<?php
		}
		echo '<tr><td class="center red" colspan="11">'.$paginator.'</td></tr>'; 
	}
?>

            </tbody>
        </table>                                                
     </div>
 </form>    
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
			</div>
