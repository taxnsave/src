<?php

class commonFunction{

	function commonFunction($getDB)
	{
		global $CONFIG,$_SESSION;
		$this->db = $getDB;
		$CONFIG->dbLink = $this->db->connect();
		$this->CONFIG = $CONFIG;
		$this->SESSION = $_SESSION;
	}
	function runInShell($command)
	{
		ob_start();
		$output = shell_exec($command);
		ob_end_clean(); 	
		return $output;
	}
	function doLogin($getCustomerID)
	{
		global $_SESSION,$customerProfile;								//print_r($this->SESSION);
		
		$customerDetails										= $customerProfile->getCustomerInfo($getCustomerID);
		$_SESSION[securimage_code_value] 						= "";
		$_SESSION[$this->CONFIG->sessionPrefix.'loginstatus']	= true;
		$_SESSION[$this->CONFIG->sessionPrefix.'user_id']		= $customerDetails['pk_user_id'];
		$_SESSION[$this->CONFIG->sessionPrefix.'customer_id']	= $customerDetails['fr_customer_id'];
		$_SESSION[$this->CONFIG->sessionPrefix.'email_id']		= $customerDetails['login_id'];
		$_SESSION[$this->CONFIG->sessionPrefix.'user_name']		= $customerDetails['cust_name'];			
		
		$_SESSION[$this->CONFIG->sessionPrefix.'_AY_TEXT'] = $_SESSION[$this->CONFIG->sessionPrefix.'_AY'] = (date("n") < 4) ? (date("Y")-1)."-".date("Y") : date("Y")."-".(date("Y")+1);
			
		
		$this->CONFIG->loggedUserId								= $_SESSION[$this->CONFIG->sessionPrefix."user_id"];
		$this->CONFIG->customerId								= $_SESSION[$this->CONFIG->sessionPrefix."customer_id"];
		$this->CONFIG->loggedUserEmail							= $_SESSION[$this->CONFIG->sessionPrefix.'email_id'];	
		$this->CONFIG->loggedUserName							= $_SESSION[$this->CONFIG->sessionPrefix.'user_name'];
		
		return $_SESSION;		//$this->SESSION;			exit;											
	}
	function requestEncodeDecode($getEDString,$whatToDO='')
	{
		$returnEDString = '';

		if($whatToDO == '')
			$returnEDString = strtr(base64_encode(addslashes(gzcompress(serialize($getEDString),9))), '+/=', '-_,');	
		else if($whatToDO == 'encode')
			$returnEDString = strtr(base64_encode(addslashes(gzcompress(serialize($getEDString),9))), '+/=', '-_,');	
		else if($whatToDO == 'decode')
		{
			$returnEDString = unserialize(gzuncompress(stripslashes(base64_decode(strtr($getEDString, '+/=','-_,')))));
			$returnEDString = str_replace($this->CONFIG->requestPrefixStart,"",$returnEDString);
			$returnEDString = str_replace($this->CONFIG->requestPrefixEnd,"",$returnEDString);
		}
		return $returnEDString;		
	}
	function setPage($setPageName)
	{
		return $this->encodeDecode($setPageName,'encode');
	}
	
	function getPage($getPageName)
	{
		return $this->encodeDecode($getPageName,'decode');
	}
	
	function encodeDecode($getEncodeString,$whatToDO='')
	{
		$retturnString = '';
		
		if($whatToDO == '')
			$retturnString = base64_encode($getEncodeString);
		else if($whatToDO == 'encode')
			$retturnString = base64_encode($getEncodeString);
		else if($whatToDO == 'decode')
			$retturnString = base64_decode($getEncodeString);

		return $retturnString;		

	}
	function orderByToggle($getRequest,$getFieldName,$getPageName='')
	{
		if(!empty($getRequest[search_str]))
			$order_by_str = 'search_str='.$getRequest[search_str].'&order_by='.$getFieldName;	
		else
			$order_by_str = 'order_by='.$getFieldName;	
				
		$sortingClass = "asc";
		if($getRequest[order_by] == $getFieldName)
		{			
			if($getRequest[sorting] == 'asc')
				$sortingClass = "desc";
			else
				$sortingClass = "asc";
					
			$orderByScheme = 'class="trClick sorting'."_".$sortingClass.'" data-href="?'.$order_by_str.'&sorting='.$sortingClass.'&module_interface='.$this->setPage($getPageName).'"';
		}
		else
		{
			$orderByScheme = 'class="trClick sorting" data-href="?'.$order_by_str.'&sorting='.$sortingClass.'&module_interface='.$this->setPage($getPageName).'"';
		}
		
		return $orderByScheme;
	}
	function resizeCorpSaveImage($filename,$image_source,$image_source_name,$maxwidth,$maxheight,$quality)
	{
			$zoom=1;
			$src_x=0;
			$src_y=0;

			$imgsrc=$image_source;
			$result_width=$maxwidth;
			$result_height=$maxheight;

			//Checking image dimension
			if(substr($image_source_name,strlen($image_source_name)-3,strlen($image_source_name))=='gif')
			{
				$gif=true;
			}
			else
			{
				$gif=false;
			}
			
			if($gif)
				$image = imagecreatefromgif($imgsrc);
			else
				$image = imagecreatefromjpeg($imgsrc);

			 
			
			$old_width = imagesx($image);
			$old_height = imagesy($image);



			//new width and height
			
			
			//Landscape image
			if($old_width>$old_height)
			{
				$ratio=$old_width/$old_height;
				if($old_height>=$result_height)
				{
					$new_height=$result_height;
					$new_width=$result_height*$ratio;
				}
				elseif($old_width>$result_width)
				{
					$new_width=$result_width;
					$new_height=$result_width/$ratio;
				}
				else
				{
					$new_width=$old_width;
					$new_height=$old_height;
				}
			}
			
			//Vertical image
			elseif($old_width<$old_height)
			{
				$ratio=$old_height/$old_width;
				if($old_width>$result_width)
				{
					$new_width=$result_width;
					$new_height=$result_height*$ratio;
				}
				elseif($old_height>$result_height)
				{
					$new_height=$result_height;
					$new_width=$result_height*$ratio;
				}
				else
				{
					$new_width=$old_width;
					$new_height=$old_height;
				}
			}
			
			//Square image
			elseif($old_width==$old_height)
			{
				$new_width=$old_width;
				$new_height=$old_height;
			}

			
			
			$new_width	=	 $new_width*$zoom;
			$new_height	=	 $new_height*$zoom;
			
			//src_x=
			//$src_y=
			if($new_width>$new_height&&$new_width>$result_width)
			{
				$src_x=($new_width-$result_width)/2;
			}
			elseif($new_width<$new_height&&$new_height>$result_height)
			{
				$src_y=($new_height-$result_height)/2;
			}


			// Resample
			$image_resized = imagecreatetruecolor($result_width, $result_height);
			imagecopyresampled($image_resized,$image,$dest_x, $dest_y, $src_x, $src_y, $new_width, $new_height, $old_width, $old_height);
			

			// Display resized image
			if($gif)
			{
				imagegif($image_resized,$filename,$quality);
			}
			else
			{
				imagejpeg($image_resized,$filename,$quality);
			}
			imagedestroy($image);
			imagedestroy($image_resized);			
			
	}
	function pageAccessTokenCheck($getPageToken,$getTokenString)
	{
		if($getPageToken != md5($getTokenString))
		{
			echo "<div class='main-content'>!!Sorry!! I Can't show anything</div>";
			//$this->jsRedirect($this->CONFIG->siteurl);
			exit;
		}	
	}	
	function mysqlResultIntoArray($getMysqlParam,$paramType='')
	{
		$sqlResultArr = array();
		if($paramType == '' || $paramType == 'SQL')
		{
			$resultSet = $this->db->db_run_query($getMysqlParam);	// or die(mysql_error());    //echo $getMysqlParam;exit;
		}
		else if($paramType == 'RESULT_SET')
		{
			$resultSet = $getMysqlParam;
		}
		if($this->db->db_num_rows($resultSet) > 0)
		{
			$i=0;
			while($resultRows = $this->db->db_fetch_array($resultSet))
			{
				$sqlResultArr[] = $resultRows;				//$i++;		//print_r($resultRows);
			}
		}		
		return $sqlResultArr;
	}
	function convertTOJSONP($arrResult)
	{
		$jsonpArr = array();
		$i=0;
		foreach($arrResult as $element1) 
		{
			foreach($element1 as $element) 
			{
				//print_r($element[0][0]);
				$val = ucwords(strtolower(str_replace("\"","",$element[0])));
				if($val != '')
					$jsonpArr[$i] = array("id" => "$val","name" => "$val","value" => "$val");
				$i++;
			}
		}	
		return json_encode($jsonpArr);	
	}
	function refineGetPOSTRequest($getAllTypeValue)
	{
		return strip_tags(addslashes($getAllTypeValue));
	}
	function getRecordCount($getSQL)
	{
		return $this->db->db_num_rows($this->db->db_run_query($getSQL));
	}
	function addIfNotExist($getTableName)
	{
		$SQL = "SELECT * FROM ".$getTableName." WHERE fr_user_id = '".$this->CONFIG->loggedUserId."'";
		$total = $this->getRecordCount($SQL);
		if($total == 0)
			$this->db->db_run_query("INSERT INTO ".$getTableName." SET fr_user_id = '".$this->CONFIG->loggedUserId."',bank_created_date = now()");
	}
	function array_push_assoc(&$arr)
	{
	   $args = func_get_args();
	   foreach ($args as $arg) {
		   if (is_array($arg)) {
			   foreach ($arg as $key => $value) {
				   $arr[$key] = $value;
				   $ret++;
			   }
		   }else{
			   $arr[$arg] = "";
		   }
	   }
	   return $ret;
	}
	function check_login($email,$password)
	{		
		$query = $this->db->db_run_query("SELECT * FROM ".$this->CONFIG->dbName.".bfsi_user WHERE login_id  = '$email' AND passwd = '".md5($password)."' AND user_status='Active'");
		return $query;
	}	
	function is_email_exist($email)
	{
		//echo "SELECT * FROM ".$this->CONFIG->dbName.".mdoc_user WHERE login_id = '$email'";
		$result = $this->db->db_run_query("SELECT * FROM ".$this->CONFIG->dbName.".bfsi_user WHERE login_id = '$email'");
		$retVal = $this->db->db_num_rows($result);
		return $retVal;
	}	
	function getSingleRow($sql)
	{
		$response = "";
		$result = "";
		//echo $sql;
		$result = $this->db->db_run_query($sql);
		$response = $this->db->db_fetch_assoc($result);				//print_r($response);
		return $response;
	}
	function dynamicUpdate($getTbname,$getArray)
	{
		$fr_user_id = $this->CONFIG->loggedUserId;
		foreach ($getArray as $key => $value) 
		{
			//$value = mysql_real_escape_string($con,$value);
			$value = "'$value'";
			$updates[] = "$key = $value";
		}
		$implodeArray = implode(', ', $updates);
		 $sql = sprintf("UPDATE %s SET %s WHERE fr_user_id='".$this->CONFIG->loggedUserId."' AND fr_itr_id = '".$this->CONFIG->currentITRID."'", $getTbname, $implodeArray);
		$result =  $this->db->db_run_query($sql);
	}
	function dynamicUpdateMultiple($table,$array) 
    { 
    	$sec_arg_value = reset($array);
        $sec_arg_name   = key($array);
        unset($array[$sec_arg_name]);
        $fr_user_id = array_shift($array);
		$fr_user_id = $this->CONFIG->loggedUserId;
		foreach ($array as $key => $value) 
		{			
			$value = "'$value'";
			$updates[] = "$key = $value";
		}
        $implodeArray = implode(', ', $updates);
         $sql = sprintf("UPDATE %s SET %s WHERE  fr_user_id='%s' AND %s = '%s' AND fr_itr_id = '".$this->CONFIG->currentITRID."'", $table, $implodeArray,$fr_user_id,$sec_arg_name,$sec_arg_value);
        $result =  $this->db->db_run_query($sql);
        return $result;      
	}
	function insertMultiple($table,$array)
    {        
		//echo $table;print_r($array);
		$column='';$data ='';$count = 1;
		$user_id =  $this->CONFIG->loggedUserId;
		if(isset($array[0]))
		{
			foreach ($array as $eacharray ) 
			{
				
				$temp ='';
				foreach ($eacharray as $key => $value ) 
				{
					if ($count == 1) 
					{
						$column .= $key.',';
					}
					$temp .="'".$value."',";
				}
				$data .= "(".rtrim($temp,',').'),';
				$count=0;
			}
		}
		else
		{
			$temp ='';
			foreach ($array as $key => $value ) 
			{
				if ($count == 1) 
				{
					$column .= $key.',';
				}
				$temp .="'".$value."',";
			}
			$data .= "(".rtrim($temp,',').'),';
			$count=0;		
		}
		$keys = rtrim($column,',');
		$values   ="values".rtrim($data,',').";";
		 $sql = sprintf("INSERT INTO %s (%s) %s", $table, $keys, $values);
		$result =  $this->db->db_run_query($sql);
		return $result;      
	}
	function latestRowById($table,$fr_user_id,$orderby,$limit) 
	{			
		$fr_user_id = $this->CONFIG->loggedUserId;
		$orderby = $orderby;
		$limit = $limit;
		$sql = sprintf("SELECT * FROM %s WHERE fr_user_id='%s' ORDER BY %s DESC LIMIT %s", $table, $fr_user_id,$orderby,$limit);
		$result = $this->db->db_run_query($sql);
		if($this->db->db_num_rows($result)>0)
		{
			$result = $this->getSingleRow($sql);		  	
		}
		return $result;
	}
	
	function getSingleResult($sql)
	{
		return $this->getSingleRow($sql);
	}
	 
	function getSingleCol($retField,$tableName,$cond='')
	{
		$SQL = "SELECT ".$retField." FROM ".$CONFIG->dbName.$tableName;
		if($cond)
			$SQL .= " WHERE ".$cond;
		else
			$SQL .= " WHERE 1 LIMIT 0,1";
		//echo $SQL;
		$retRes = $this->getSingleRow($SQL);

		if(empty($retRes[$retField]))
			return "Empty Name";
		else
			return $retRes[$retField];
	}	
	
	function date_format1($date,$age='')
	{
		if($date != '')
		{
			$tmp = date("M d, Y",strtotime($date));
			return $tmp ;
		}
	}
	function getTimeDuration($stamp)
	{
		$stamp=time()- $stamp;
		if($stamp/60/60<24) return 'Today';
		else if($stamp/60/60>=24)
		{
			if($stamp/60/60/24==1) return 'Yesterday';
			else			
			return round($stamp/60/60/24).' day(s) ago';
		}
	}	
	
	function convertDateWithTime($str)
	{
		list($date, $time) = explode(' ', $str);
		list($year, $month, $day) = explode('-', $date);
		list($hour, $minute, $second) = explode(':', $time);
		
		$timestamp = mktime($hour, $minute, $second, $month, $day, $year);
		
		return $timestamp;
	}
	function removeCharFromArray($getArray,$getChar)
	{
		$totCol = count($getArray);
		while(list($key,$val)=each($getArray))
		{
			$getArray[$key] = str_replace($getChar,"",trim($val));
		}		
		return $getArray;
	}
	function cleanURL($getText)
	{	
		$retText = str_replace("&","_",str_replace("'","_",str_replace("\"","_",str_replace("?","_",str_replace(".","_",str_replace("(","_",str_replace(")","_",str_replace("{","_",str_replace("}","_",str_replace("[","_",str_replace("]","_",str_replace("@","_",str_replace("#","_",str_replace("$","_",str_replace("$","_",str_replace("$","_",str_replace("%","_",str_replace("^","_",str_replace("~","_",str_replace("!","_",str_replace("*","_",str_replace("+","_",str_replace("\\","_",str_replace("/","_",str_replace(">","_",str_replace("<","_",str_replace(";","_",str_replace(":","_",str_replace(" ","_",str_replace(",","_",strtolower($getText)))))))))))))))))))))))))))))));
	
		return $retText;	
	}
		
	function update($values,$tablename,$key_field,$key_value)
	{
		//print_r($values);
		global $CONFIG;
		$updates='';
		$index=0;
		array_pop($values);			// Remove Action
		foreach($values as $field => $value)
		{
			if(++$index!=count($values))
			{
				$updates.=$field."='".$value."',";
			}
			else
			{
				$updates.=$field."='".$value."'";
			}
		}
		$SQL="UPDATE ".$tablename." SET ".$updates.", last_update='".$CONFIG->timestamp."' WHERE ".$key_field."='".$key_value."'";
		if($this->db->db_run_query($SQL))
		return true;
		else return false;
	}
	function upload($getFileID)
	{
		 if(!$_FILES[$getFileID]['name']) return array('','No file specified');
		 $file_title = microtime(true)."_26AS_".$this->cleanURL($_FILES[$getFileID]['name']).".pdf";
		 $uploadfile = $this->CONFIG->customerFilePath.$file_title;
		 if (!move_uploaded_file($_FILES[$getFileID]['tmp_name'], $uploadfile)) 
		 {
		 }
		 else
		 {
		 	 chmod($uploadfile,0777); //Make it universally writable.
			 return array($file_title);
		 }
	}
	function dateFormat($date)
	{
			return date("M d, Y",$date);
	}

	function dateFormatWithTime($date)
	{
			return date("F j, Y, g:i a",strtotime($date));
	}

	
	function readPHP($getFilenameWithPath)
	{
		//echo $getFilenameWithPath;
		$partialToken = md5('d9c9b9');
		ob_start();
		include($getFilenameWithPath);
		$htmlText = ob_get_contents();
		ob_end_clean();

		return $htmlText;
	}
	
	function send_mail($email_to,$subject,$message,$html=false,$attachment='',$from_email='support@taxsave.in',$from_name='taxSave Support')
	{
		global $mdocmail;
		if($attachment == '')
			$mdocmail->sendHTMLMails($email_to,$from_email,$from_name,$subject,$message,'','html');
		else
			$mdocmail->sendHTMLMails($email_to,$from_email,$from_name,$subject,$message,'','html',$attachment);
		//mail($email_to, $subject, stripslashes($message), $headers);
	}	
	function jsRedirect($url) 
	{
		echo "<script>location.href='".$url."'</script>";
		exit;
	}
	function splitFilenameWithExt($getFilename,$withPath='')
	{
		if($withPath !='')
		{
		}
		
		$f = explode(".",$getFilename);
		$totalCount = count($f)-1;
		$retFilename = '';
		$extOfFile = $f[$totalCount];
		while(list($key,$val) = each($f))
		{
			if($key < $totalCount)
				$retFilename .= $val;
		}
		return array($retFilename,$extOfFile);
	}
	function generateString($length)
	{
		global $CONFIG;
		$t = time();
		$charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
		for($i=0; $i<$length; $i++) $key .= $charset[(mt_rand(0,(strlen($charset)-1)))]; 
	
		return $t."_".$key;
	}
	function require_login() 
	{
		if($this->CONFIG->loggedUserId == '')
			$this->jsRedirect($this->CONFIG->siteurl."logout.php");
	}

	function validate_email($str)
	{ 
        $str = strtolower($str); 
        if(ereg("^([^[:space:]]+)@(.+)\.(ad|ae|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|fi|fj|fk|fm|fo|fr|fx|ga|gb|gov|gd|ge|gf|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nato|nc|ne|net|biz|info|nf|ng|ni|nl|no|np|nr|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$",$str)){ 
        return 1; 
        } else { 
        return 0; 
        } 
    } 
} // End Class
?>

