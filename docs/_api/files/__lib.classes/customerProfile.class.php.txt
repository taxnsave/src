<?php
class customerProfile{

	function customerProfile()
	{
		global $CONFIG,$commonFunction,$permissionSettings;
		global $db;
		$this->db = $db;
		$this->dbName	 = $CONFIG->dbName;	
		$this->commonFunction = $commonFunction;
		$this->permissionSettings = $permissionSettings;
		$this->CONFIG = $CONFIG;
		$this->customerProfile = array();
	}
	function getAllUsers($getRequest,$pageLimit='')
	{
		if($pageLimit != '')
			$limit = " LIMIT ".$pageLimit.",".$this->CONFIG->paginationPageItem;
		else
			$limit='';
			
		if($pageLimit == 0)
			$limit = " LIMIT ".$pageLimit.",".$this->CONFIG->paginationPageItem;
		
		if(!empty($getRequest[order_by]))
			$orderBy = " ORDER BY ".$getRequest[order_by]." ".$getRequest[sorting];
		else
			$orderBy = '';
				
		$userQuery			= "SELECT bfsi_user.*,
									   bfsi_users_details.*
								  FROM bfsi_user bfsi_user
									   INNER JOIN bfsi_users_details bfsi_users_details
										  ON (bfsi_user.pk_user_id = bfsi_users_details.fr_user_id)";

		if($orderBy !='')
			$userQuery .= "  ".$orderBy;
		if($limit !='')
			$userQuery .= "  ".$limit;
		
		$countSearch = $this->getAllUsersCount($getRequest);
		
		//echo $countSearch,$SQL;				
		
		if($countSearch > 0)
		{			
			$customerList		=	$this->commonFunction->mysqlResultIntoArray($userQuery,'SQL');
		}
		else
			$customerList = array("MF_NONE");
						
		return $customerList;
	}	
	function getAllUsersCount($getRequest='')
	{
		$result1 = $this->db->db_run_query("select count(*) count from bfsi_user");
		$r1 = $this->db->db_fetch_array($result1);
		$count1=(int)$r1['count'];
		return $count1;
	}
	function getPotalUserDetails($memberID)
	{
		$query="SELECT * from $this->dbName.bfsi_user WHERE pk_user_id='$memberID'";
		if($result=$this->db->db_run_query($query))
			return $this->db->db_fetch_array($result);	
		else
			return false;
	}
	function customerAge($dob)
	{
		$age = $this->commonFunction->mysqlResultIntoArray("SELECT TIMESTAMPDIFF( YEAR, '".date("Y-m-d",strtotime($dob))."', CURDATE( ) ) AS age");
		return $age[0][age];
	}
	function getCustomerInfo($memberID)
	{
		$customerDetails=array();
		global $commonFunction;
		
		$detailQuery			= "SELECT bfsi_user.*,
									   bfsi_users_details.*
								  FROM bfsi_user bfsi_user
									   INNER JOIN bfsi_users_details bfsi_users_details
										  ON (bfsi_user.pk_user_id = bfsi_users_details.fr_user_id)
								 WHERE (bfsi_user.pk_user_id =  '".$memberID."')";

		$customerDetails		=	$this->commonFunction->getSingleRow($detailQuery);
		return $customerDetails;
	}
	function getCustomerBankInfo($getUserId='')
	{		
		if($getUserId == '')
			$userId = $this->CONFIG->loggedUserId;
		else
			$userId = $getUserId;
	
		$detailQuery		= "SELECT * FROM bfsi_bank_details WHERE fr_user_id  =  '".$userId."'";
		$bankDetails		=	$this->commonFunction->getSingleRow($detailQuery);
		return $bankDetails;
	}
	function getCustomerName($fr_user_id)
	{
		global $commonFunction;
		$name = $commonFunction->getSingleCol('cust_name','bfsi_users_details', "fr_user_id ='$fr_user_id'");
		return trim($name);	
	}
	function setProfileImage($memberID,$imageName)
	{
		$this->db->db_run_query("UPDATE bfsi_user SET profile_image  = '".$imageName."' WHERE pk_user_id  = '".$memberID."'");
		return;
	}
	function getEmail($memberID)
	{
		$email = $this->commonFunction->getSingleRow("SELECT email_id FROM  $this->dbName.bfsi_user WHERE  pk_user_id ='$memberID'");
		return substr($email[email_id],0,strpos($email[email_id],'@'));
	}	
	function newCustomer($getPostedData,$isSubUser='')
	{
		global $commonFunction,$CONFIG;
		//5316071800000001
		$n = 10000000;	
		$fixed = "91".date("dmy");
		
		$SQL 		= "INSERT INTO bfsi_user SET
						login_id = '".$getPostedData['email']."',
						passwd = '".md5(trim($getPostedData['reg_passwd']))."',
						communication_email = 'Permanent',	
						user_status = 'Active',										
						signup_ip = '".$_SERVER['REMOTE_ADDR']."'";				
		
		$custRes	= $this->db->db_run_query($SQL);
		$customerID = $this->db->db_insert_id();
		if($customerID == 0)
		{
			//echo $SQL;
			echo "REGISTER_FAILED";
			return 0;
		}
		$dbCustomerID = $fixed.($n+$customerID);
		
		$customerIDUpdate	= $this->db->db_run_query("UPDATE bfsi_user SET fr_customer_id = '".$dbCustomerID."' WHERE pk_user_id = '".$customerID."'");
		
		$SQL1 		= "INSERT INTO bfsi_users_details SET
						fr_user_id = '".$customerID."',
						cust_name = '".$getPostedData['name']."',
						contact_no = '".$getPostedData['mobile']."'";				
		
		$result1	= $this->db->db_run_query($SQL1);
		
		$SQL2 		= "INSERT INTO bfsi_users_settings SET
						fr_user_id = '".$customerID."',
						setting_create_date = now()";				
		
		$result2	= $this->db->db_run_query($SQL2);
		
		$SQL3 		= "INSERT INTO bfsi_bank_details SET
						fr_user_id = '".$customerID."',
						bank_created_date = now()";				
		
		$result3	= $this->db->db_run_query($SQL3);
		
		$message = $commonFunction->readPHP("../__lib.mailFormats/registration_confirmation_mail.html");
		$message = str_replace("USERNAME",$getPostedData['name'],$message);
		$message = str_replace("ACTIVE_CODE_1","a=".$getPostedData['email'],$message);
		$message = str_replace("ACTIVE_CODE_2","verifyEmail=".md5($getPostedData['email']),$message);
		$this->db->db_run_query("INSERT INTO bfsi_verification SET email_id = '".$getPostedData['email']."', fr_customer_id='".$dbCustomerID."',auth_code='".md5($getPostedData['email'])."',ip_address = '".$_SERVER['REMOTE_ADDR']."',verification_type='Register', create_date=now()");
		$commonFunction->send_mail($getPostedData['email'],"Account Registered with taxsave.in",$message,true);
		return $customerID;
	}	
	function checkVerifyPending($getRequestEmail,$authCode)
	{
		$usagesQuery	= "SELECT * FROM $this->dbName.bfsi_verification WHERE email_id  = '".$getRequestEmail."' 
																		   AND auth_code ='".$authCode."' 
																		   AND verification_type = 'Forget' 
																		   AND action_status = 'Pending'";
		$tagRes	= $this->db->db_run_query($usagesQuery);
		$emailRes = $this->db->db_num_rows($tagRes);		
		
		if($emailRes > 0)
		{
			return 1;
		}
		else
			return 0;
	}
	function verifyCustomer($getRequestEmail,$verificationType='Register')
	{
		global $commonFunction;
		$usagesQuery	= "SELECT * FROM $this->dbName.bfsi_verification WHERE auth_code   = '".$getRequestEmail."' AND verification_type='".$verificationType."'";
		$tagRes	= $this->db->db_run_query($usagesQuery);
		$emailRes = $this->db->db_num_rows($tagRes);		
		
		if($emailRes > 0)
		{
			$getEmail		 = $commonFunction->getSingleRow($usagesQuery);
			$customerDetails = $commonFunction->getSingleRow("SELECT * FROM bfsi_user WHERE login_id = '".$getEmail[email_id]."'");
			$verifyUpdate	 = $this->db->db_run_query("UPDATE bfsi_verification SET auth_code = '' WHERE pk_verify_id = '".$getEmail[pk_verify_id]."'");
			$this->activateAccount($customerDetails[pk_user_id]);
			return $customerDetails[pk_user_id];
		}
		else
			return 0;
	}
	
	function activateAccount($getUserId)
	{
		$subUSerUpdate	= $this->db->db_run_query("UPDATE bfsi_user SET user_status = 'Active' WHERE pk_user_id = '".$getUserId."'");
	}
}  
//class closed

?>

