<!-- Navbar -->
  <nav class="navbar navbar-inverse my-nav">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="<?php echo $CONFIG->siteurl;?>home.html"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png" alt="Logo" width="160"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li> 
        <li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
		<li><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
      </ul>
      <ul class="nav navbar-nav brdr">
        <li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
		<li ><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create Will</a></li>
      </ul>
    </div>
  </div>
</nav>
  <!-- Navbar -->

<!-- Banner -->
<div class="banner_will">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
				<h1><center>Who we are</center><br> <center><span class="span-end">Finance simplified</span></center></h1>
				<center><span class="span-endline"></span></center>
			</div>
			
		</div>
	</div>
</div>
<!-- Banner -->

<style >

    h4 {
        color: darkblue;
    }
    
</style>

<div class="about-area-one section-spacing">
      <div class="inner-about">
        <div class="container">
          <div class="row about-content">
            <div class="col-lg-12 col-md-12 col-sm-12">          
                  <h4>About us </h4>
<p align="justify">Working with clients for more than 15 years is a great sign of trust, confidence and integrity. From a simple tax return filing to completing assessments is quite a journey and gives the strength to decode the complexities further. We are proud of our happy customers who have expressed their great joy and satisfaction in working with us and who have achieved their goals while investing through us. In continuation of our journey, TaxSave is an integrated platform for individual financial management. TaxSave automates each activity with an option to avail advanced and confidential personalised services in a secured and simplified manner. TaxSave and its team of experts brings to you tools and information to help you achieve your financial goals and making compliances, savings and investing profitable, fun and stress free.</p>
                
<h4>Our Core Values &ndash; </h4>
<p align="justify">Customer first, Real Time and Strategic Support, Data and Information confidentiality, sound, transparent and unbiased offering</p>
              
<h4>Our Vision &ndash; </h4>
<p align="justify">To create a global brand&nbsp; respected by all for its integrity and value system, its people and excellence in service through knowledge and perseverance.</p>
              
<h4>Our Mission &ndash;</h4>
<p align="justify">To enable&nbsp; our customers reach their financial independence and achieve their financial goals.</p>
          
<h4>Our Social Initiatives &ndash; </h4>
<p align="justify">Contributing to society and human well being is a passionate drive and no compulsion for us. We believe in empowering youth and unblocking the potential of Generation Y. Our mainstream consists of young workforce, properly trained and organised for highest productivity. Happy customers, happy employees and happy vendors, inclusive workforce, always at par with the industry in terms of standards and technology - This is the most gratifying about work.</p>

    <h4>Our Commitment :</h4>
<ul>
<li>Customer First.</li>
<li>Sound, transparent, and unbiased offering.</li>
<li>Real time strategic support.</li>
<li>Excellence in offering through knowledge and perseverance.</li>
<li>Data information confidentiality.</li>
</ul>
<h4>WHY TAXSAVE :&nbsp;&nbsp; </h4>
<ul>
<li><strong>Customized Solutions</strong></li>
<li><strong>Commitment</strong></li>
<li><strong>Technology</strong></li>
<li><strong>Reliability</strong></li>
<li><strong>Simplicity</strong></li>
</ul>    
                
                <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/aboutus1.png">  
            </div>           
          </div>
             
            <div class="row" >
            
            <h4>PLATFORM PARTNERS :</h4>
<p>Registered e-Return Intermediary : ERIA101037<br>
AMFI Registered Distributor : ARN : 60277<br>
BSE Star Payment Gateway : BSE member code : 15133<br>
N.Tatia & Associates : Firm Registration Number : 011067S</p>
            </div>
   
        </div>
      </div>
      
    </div>
    
<div style="height:50px"></div>
