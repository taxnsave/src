<?php
	include_once("../../../__lib.includes/config.inc.php");
	$wbd_data		= $willProfile->getWillDetails('will_bond_details');
	$bene_data_all	= $willProfile->getWillBenDetails('will_beneficiary');
?>
<form action="data_submit/frmForm.php" name="frmBankAccount" id="frmBankAccount" method="post" class="frmCurrent has-validation-callback">
    <div class="agreement-row">
        <div class="container">
            <div id="mutual_fund">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="bnkacAddWrap">
                            <div>
                                <!-- do not delte this div -->
                                <h3>Bond <span>&nbsp;</span></h3>
                                <div class="panel panel-body sub-panel">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label class="mandatory mandatory_label">Name of Company, Issuing the Bond <span class="mandatory_star">*</span></label>
                                            <br>
                                            <input class="form-control" name="company_name" placeholder="Name of Company, Issuing the Bond" value="<?= $wbd_data->bond_comany_name ; ?>" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Name of Company, Issuing the Bond" title="Please enter Name of Company, Issuing the Bond" alt="Please enter Name of Company, Issuing the Bond" type="text" required>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="mandatory mandatory_label">Scheme Name <span class="mandatory_star">*</span></label><br>
                                            <input class="form-control" name="scheme_details" placeholder="Scheme Name" value="<?= $wbd_data->bond_scheme_name  ; ?>" maxlength="99" data-validation="" data-validation-length="1-99" data-validation-error-msg="Please enter valid Scheme Name" title="Please enter Scheme Name" alt="Please enter Scheme Name" type="text" required>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="mandatory mandatory_label">Demat AIC Number <span class="mandatory_star">*</span></label><br>
                                            <input class="form-control numeric" name="bond_dmat_number" placeholder="Demat AIC Number" value="<?= $wbd_data->bond_demat_no  ; ?>" maxlength="20" title="Please enter Demat AIC Number" alt="Please enter Demat AIC Number" data-validation="" data-validation-length="1-20" data-validation-error-msg="Please enter valid Demat AIC Number" type="text" required>
                                        </div>
                                    </div>
                                        <!-- <div class="row">&nbsp;</div> -->
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <label class="mandatory mandatory_label">Bank name <span class="mandatory_star">*</span></label><br>
                                                <input class="form-control" name="bond_bank_name" placeholder="bank name" value="<?= $wbd_data->bond_bank_name; ?>" maxlength="99" data-validation="" data-validation-length="1-99" data-validation-error-msg="Please enter valid bank name" title="Please enter bank name" alt="Please enter bank name" type="text" required>
                                            </div>
                                            <div class="col-xs-6">
                                                <label class="mandatory mandatory_label">Amount<span class="mandatory_star">*</span></label>
                                                <br>
                                                <input class="form-control" name="bond_amount" placeholder="company name" value="<?= $wbd_data->bond_amount ; ?>" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter bound " title="Please enter Type of share" alt="Please enter company name" type="text" required>
                                            </div>
                                        </div>
                                        <div id="div_beneficiary">
                                            <div id="div_beneficiary_1">
                                                <h4>
                           <strong>Beneficiaries</strong>
                        </h4>
                                                <div class="panel panel-body sub-panel">
                                                    <div class="row">
                                                        <div class="col-xs-4">
                                                            <label class="mandatory mandatory_label">Beneficiary <span class="mandatory_star">*</span></label>
                                                            <select name="sel_beneficiary" id="sel_beneficiary" class="input-select form-control" title="Please select a Beneficiary" alt="Please select a Beneficiary">
                                                                <option value="">Please select</option>
                                                                <?php 
                                                $cnt=1;
                                                while($row = mysql_fetch_array($bene_data_all)) {
                                                    $html = "<option value='" . $cnt . "'>" .$row['f_name'] . "</option>";
                                                    
                                                    $cnt+= 1;
                                                    echo $html;
                                                } 
                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <label class="mandatory mandatory_label">Percentage Share to be alloted <span class="mandatory_star">*</span></label>
                                                            <input class="form-control numeric_share" name="txt_beneficiary_share" id="txt_beneficiary_share" placeholder="Percentage Share to be alloted" maxlength="5" title="Please enter Percentage Share to be alloted" alt="Please enter Percentage Share to be alloted" value="" type="text">
                                                        </div>
                                                        <!-- <div class="col-xs-4">
                                                            <label class="mandatory">Any Other Information</label>
                                                            <input class="form-control" name="txt_other_info" id="txt_other_info" maxlength="200" placeholder="Any Other Information" title="Please enter Any Other Information" alt="Please enter Any Other Information" value="" type="text">
                                                        </div> -->
                                                    </div>
                                                    <div>
                                                        <br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add Beneficiary" title="Add Beneficiary" id="beneficiaryAddBtnNew" class="btn btn-primary btn-sm btn-add-large a_beneficiarynew">+ Add Beneficiary</a></span></div>
                                                    <br>
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Name</th>
                                                                <th>Relation</th>
                                                                <th>Share</th>
                                                                <th><a href="" class="btn btn-danger">Reset all</a></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tb_con_ben">
                                                            <tr id="no_beneficiary">
                                                                <td colspan="5"></td>
                                                            </tr>
                                                            <input name="tot_ben" id="tot_ben" value="1" type="hidden">
                                                            <input id="total_share" value="0" type="hidden">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- row END -->
                                    <div class="row">&nbsp;</div>
                                    <div class="bottom_buttons row">
                                        <div class="div_prev_button">
                                            <input name="previous" id="btn-previous" value="« Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="public-provident-fund" type="button">
                                        </div>
                                        <div class="div_center_buttons">
                                            <!-- <input name="add_bond" id="btn-add" value="Add / Save" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " type="submit"> -->
                                            <!-- <input name="update" id="btn-update" value="Update" title="Click here to Update the changed data" alt="Click here to Update the changed data" class="btn-submit btn-submit-disabled" disabled="" type="submit"> -->
                                        </div>
                                        <div class="div_next_button" style="margin-right: -30px; text-align: right;">
                                            <input name="add_bond" id="btn-add" value="Save & Next »" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " type="submit">
                     <!-- <input name="next" id="btn-next" value="Next »" title="Click here to go to Next page" alt="Click here to go to Next page" class="btn-submit btn-move" next_page="mutual-funds-details" type="button"> -->
                  </div>
                  <br><br><br>
                                    </div>
                                    <!-- <div class="row">
                  <input name="txt_other_submit" id="txt_other_submit" value="0" type="hidden">
                  <input name="txt_redirect_page" id="txt_redirect_page" value="" type="hidden">
                  <input name="sumbit" value="Submit" class="btn-submit btn-submit-disabled" title="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" alt="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" disabled="" type="submit">                            
               </div> -->
</form>
<!-- <script type="text/javascript" src="/js/stocks-bonds-details.js" lang="javascript"></script>
<script src="/js/form-validator/jquery.form-validator.js"></script>
<script src="/js/jquery.numeric.js"></script>
<link media="all" type="text/css" rel="stylesheet" href="/css/jquery-ui.css">
<script type="text/javascript" src="/js/jquery-ui.js" lang="javascript"></script> -->
<script>
account_count = 1;
actual_account_count = 1;
beneficiary_count = 1;
actual_beneficiary_count = 1;

$(window).load(function() {
    for (var i = 1; i <= account_count; i++) {
        var temp = $('#actual_phone_count_' + i).val();
        actual_phone_count[i] = $('#actual_phone_count_' + i).val();
        actual_office_phone_count[i] = $('#actual_office_count_' + i).val();
        actual_mobile_count[i] = $('#actual_mobile_count_' + i).val();
        actual_email_count[i] = $('#actual_email_count_' + i).val();
        actual_fax_count[i] = $('#actual_fax_count_' + i).val();
    }
});
</script>
</div>
