
<?php

/*
     This file is configuration file where we change all the details of server,
	 database,email etc.

*/

class object {};
$CONFIG = new object;

//////////////////////////////////////////////
//  Server Details                         //
////////////////////////////////////////////

$HOST = $_SERVER['HTTP_HOST'];

if($HOST == "localhost" || $HOST == "10.0.0.2")
{
		$CONFIG->error = 0;	// testing mode 0  production mode 1

	//////////////////////////////////////////////
	//  Server Details                         //
	////////////////////////////////////////////

		$CONFIG->wwwroot     	= "c:/wamp3/www/dev/";
		$CONFIG->siteurl 	 	= "http://localhost/dev/";
		$CONFIG->siteurlHttps	= "http://localhost/dev/";
		$CONFIG->dbHost 	 	= "localhost";
		$CONFIG->dbUser      	= "taxsave";
		$CONFIG->dbPassword  	= "taxsave";
		$CONFIG->dbName      	= "taxsave_dev_taxsave";

		$CONFIG->userFilesURL	= "__uploaded.files/";
		$CONFIG->userFilesPath	= $CONFIG->wwwroot.$CONFIG->userFilesURL;
		$CONFIG->staticURL		= $CONFIG->siteurl."static/";
		$CONFIG->theme			= "th_4/";
		$CONFIG->tmpFilesURL	= "__tmp.Generated.Files/";
		$CONFIG->tmpFilesPath	= $CONFIG->wwwroot.$CONFIG->tmpFilesURL;
}
else
{
	$CONFIG->error = 0;
	//////////////////////////////////////////////
	//  Server Details                         //
	////////////////////////////////////////////

		$CONFIG->wwwroot     	= "/home/taxsave/public_html/dev/";
		//$CONFIG->siteurl 	 	= "http://www.dev.taxsave.in/";
		//$CONFIG->siteurlHttps	= "http://www.dev.taxsave.in/";
		$CONFIG->siteurl 	 	= "http://localhost/dev/";
		$CONFIG->siteurlHttps	= "http://localhost/dev/";
		$CONFIG->dbHost 	 	= "localhost";
		$CONFIG->dbUser      	= "taxsave_taxsave";
		$CONFIG->dbPassword  	= "{2RGYy]CcFdq";
		$CONFIG->dbName      	= "taxsave_dev_taxsave";
		$CONFIG->theme			= "th_4/";
		$CONFIG->userFilesURL	= "__uploaded.files/";
		$CONFIG->userFilesPath	= $CONFIG->wwwroot.$CONFIG->userFilesURL;
		//$CONFIG->staticURL		= "http://www.taxsave.in/static/";
		$CONFIG->staticURL		= $CONFIG->siteurl."static/";
		$CONFIG->tmpFilesURL	= "__tmp.Generated.Files/";
		$CONFIG->tmpFilesPath	= $CONFIG->wwwroot.$CONFIG->tmpFilesURL;
}



if($CONFIG->error == 1)
{
	ini_set("display_errors",1);
	error_reporting(E_ALL);
	//error_reporting(-1);	//error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);	//error_reporting(E_ERROR | E_WARNING | E_PARSE);
}
else
{
	ini_set("display_errors",0);
	error_reporting(0);
}

	$CONFIG->apidir      = $CONFIG->wwwroot."__lib.apis/";
	$CONFIG->libdir      = $CONFIG->wwwroot."__lib.includes";
	$CONFIG->imagedir    = $CONFIG->wwwroot."images";
	$CONFIG->classdir	 = $CONFIG->wwwroot."__lib.classes";
	$CONFIG->timestamp	 = time();
	$CONFIG->loggedIP	 = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];

session_start();

	include($CONFIG->classdir."/db.class.php");
	$db = new Db();
	$CONFIG->dbLink = $db->connect();
	$CONFIG->db = $db;

	require("$CONFIG->libdir/commonFunction.php");
	$commonFunction = new commonFunction($db);

	$CONFIG->sessionPrefix			= 'caTAX_';

	$CONFIG->requestPrefixStart		= 'rEqUeSt~_';
	$CONFIG->requestPrefixEnd		= '_~tSeUqEr';
	$CONFIG->requestPrefix			= $CONFIG->requestPrefixStart.'{DATA}'.$CONFIG->requestPrefixEnd;

	$CONFIG->loggedUserId			= $_SESSION[$CONFIG->sessionPrefix."user_id"];
	$CONFIG->customerId				= $_SESSION[$CONFIG->sessionPrefix."customer_id"];
	$CONFIG->loggedUserEmail		= $_SESSION[$CONFIG->sessionPrefix.'email_id'];
	$CONFIG->loggedUserName			= $_SESSION[$CONFIG->sessionPrefix.'user_name'];

	$CONFIG->loggedAdminId			= $_SESSION[$CONFIG->sessionPrefix."a_user_id"];
	$CONFIG->loggedAdminName		= $_SESSION[$CONFIG->sessionPrefix."a_user_name"];
	$CONFIG->currentAY				= $_SESSION[$CONFIG->sessionPrefix."_AY"];
	$CONFIG->currentAYTEXT			= $_SESSION[$CONFIG->sessionPrefix.'_AY_TEXT'];
	$CONFIG->currentITRID			= $_SESSION[$CONFIG->sessionPrefix.'_ITR_ID'];

	if($CONFIG->loggedUserId)
		$htmlPageSource  =  '__postLoginPages';
	else
		$htmlPageSource  =  '__preLoginPages';

	$CONFIG->uploadLinkPoint		= "storage_1/";
	$CONFIG->customerProfileImgURL	= $CONFIG->siteurl.$CONFIG->userFilesURL.$CONFIG->uploadLinkPoint.$CONFIG->customerId."/profile_img/";

	$CONFIG->customerFilePath		= $CONFIG->userFilesPath.$CONFIG->uploadLinkPoint.$CONFIG->customerId."/";
	$CONFIG->customerThumbPath		= $CONFIG->userFilesPath.$CONFIG->uploadLinkPoint.$CONFIG->customerId."/th/";
	$CONFIG->customerProfileImg		= $CONFIG->userFilesPath.$CONFIG->uploadLinkPoint.$CONFIG->customerId."/profile_img/";
	$CONFIG->adminUploadDIR			= "__admin.upload/";
	$CONFIG->adminUploadPath 		= $CONFIG->userFilesPath.$CONFIG->uploadLinkPoint.$CONFIG->adminUploadDIR;

	$CONFIG->paginationPageItem		= 50;
	$CONFIG->RTANames				= array("cam","karvy","franklin","sundram");

	$CONFIG->sourceOfWealth			= array("Salary" => "01", "Business Income" => "02", "Gift" => "03", "Ancestral Property" => "04", "Rental Income" => "05",
											"Prize Money" => "06","Royalty" => "07","Others" => "08");

	$CONFIG->taxStatus				= array("Individual" => "01","On Behalf Of Minor" => "02","HUF" => "03", "Company" => "04","NRE" => "21");
	$CONFIG->clientHolding			= array("Single" => "SI","Joint" => "JO","Anyone or Survivor" => "AS");

	$CONFIG->occupationCode			= array("Business" => "01", "Service" => "02", "Professional" => "03", "Agriculturist" => "04", "Retired" => "05",
											"Housewife" => "06","Student" => "07","Others" => "08","Doctor" => "09","Private Sector Service" => "41",
											"Public Sector Service" => "42","Forex Dealer" => "43","Government Service" => "44");

	$CONFIG->genderArr				= array("Male" =>"M","Female"=>"F");
	$CONFIG->stateCodeBSE			= array("Andaman & Nicobar" => "AN", "Arunachal Pradesh" => "AR", "Andhra Pradesh" => "AP","Assam" => "AS",
											"Bihar" => "BH", "Chandigarh" => "CH", "Chhattisgarh" => "CG", "Delhi" => "DL", "GOA" => "GO",
											"Gujarat" => "GU", "Haryana" => "HA", "Himachal Pradesh" => "HP", "Jammu & Kashmir" => "JM",
											"Jharkhand" => "JK", "Karnataka" => "KA", "Kerala" => "KE", "Madhya Pradesh" => "MP", "Maharashtra" => "MA",
											"Manipur" => "MN", "Meghalaya" => "ME", "Mizoram" => "MI", "Nagaland" => "NA", "New Delhi" => "ND",
											"Orissa" => "OR", "Pondicherry" => "PO", "Punjab" => "PU", "Rajasthan" => "RA", "Sikkim" => "SI", "Telangana" => "TG",
											"Tamil Nadu" => "TN", "Tripura" => "TR", "Uttar Pradesh" => "UP", "Uttaranchal" => "UC", "West Bengal" => "WB",
											"Dadra and Nagar Haveli" => "DN", "Daman and Diu" => "DD", "Others" => "OH");

	include($CONFIG->classdir."/paginator.class.php");

	include($CONFIG->classdir."/customerProfile.class.php");
	$customerProfile = new customerProfile();

	include($CONFIG->classdir."/customerLog.class.php");
	$customerLog = new customerLog();

	include($CONFIG->classdir."/documentFiles.class.php");
	$documentFiles = new documentFiles();

	include($CONFIG->classdir."/search.class.php");
	$search = new search();

	include($CONFIG->classdir."/camail.class.php");
	$mdocmail = new mdocmail();

	include($CONFIG->classdir."/permissionSettings.class.php");
	$permissionSettings = new permissionSettings();

	include($CONFIG->classdir."/runtimeHTML.class.php");
	$runtimeHTML = new runtimeHTML();

	include($CONFIG->classdir."/mutualFund.class.php");
	$mutualFund = new mutualFund();

	include($CONFIG->classdir."/websiteContent.class.php");
	$websiteContent = new websiteContent();

	include($CONFIG->classdir."/itrFill.class.php");
	$itrFill = new itrFill();

	include($CONFIG->classdir."/willProfile.class.php");
	$willProfile = new willProfile();

	include($CONFIG->classdir."/buySell.class.php");
	$buySell = new buySell();

	include($CONFIG->classdir."/bseSync.class.php");
	$bseSync = new bseSync();

	include($CONFIG->classdir."/mfScheme.class.php");
	$mfScheme = new mfScheme();

	//ini_set('post_max_size', '200M');
	ini_set('upload_max_filesize', '200M');

	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
	{
			$CONFIG->newLine="\r\n";
	} else {
			$CONFIG->newLine="\n";
	}
?>
