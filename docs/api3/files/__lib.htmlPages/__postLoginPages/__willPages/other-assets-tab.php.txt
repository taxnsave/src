<ul class="nav nav-tabs bank">
  <li class="active"><a data-toggle="tab" data="../ajax-request/will_request.php?action=sub_other-assets-details">Other Assets</a></li>
  <li><a data-toggle="tab" data="../ajax-request/will_request.php?action=sub_digital-assets-details">IPR</a></li>
  <li><a data-toggle="tab" data="../ajax-request/will_request.php?action=sub_vehicle-details">Vehicle</a></li>
</ul>

<div class="panel-body-small">

</div>

<!-- <script src="js/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7js/bootstrap.min.js"></script> -->
<script type="text/javascript">

function showsubsubtab(urltoload) {
    $.ajax({
        url: urltoload,
        type: 'get',
        success: function(data) {
            $('.panel-body-small').html(data);
        },
        error: function(error) {

        }
    });
}


  $(document).ready(function(){
    // alert("hello");
    $('.bank li a').click(function(){
      var list = $(this).attr("data");
      $.ajax({
        url: list,
        type: 'get',
        success: function(data){
          $('.panel-body-small').html(data);
        },
        error: function(error){

        }
      });
    });

  });

  var redirparam = url.searchParams.get("subpage2");
  $(".bank li").removeClass("active"); //Remove any "active" class
  /* if(redirparam == "mf") {
  	
  } else */ 
  if(redirparam == "ipr") {
  	$(".bank li:eq(1)").addClass("active").show();
  	showsubsubtab("../ajax-request/will_request.php?action=sub_digital-assets-details");
  } else if(redirparam == "veh") {
  	$(".bank li:last").addClass("active").show();
  	showsubsubtab("../ajax-request/will_request.php?action=sub_vehicle-details");
  } else {
	  $(".bank li:first").addClass("active").show();
	  	showsubsubtab("../ajax-request/will_request.php?action=sub_other-assets-details");
  }
</script>

