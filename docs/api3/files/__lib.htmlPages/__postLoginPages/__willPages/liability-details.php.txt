<?php
	include_once("../../../__lib.includes/config.inc.php");
	$wls_data		= $willProfile->getWillDetails('will_liabilities');
	$bene_data_all	= $willProfile->getWillBenDetails('will_beneficiary');
?>
<form action="data_submit/frmForm.php" name="frmLiability" id="frmLiability" method="post" class="frmCurrent">
    <input type="hidden" name="form_id" value="12">
    <div class="agreement-row">
        <div class="container">
            <h3>Liability Details</h3>
            <div class="row">
                <div class="col-xs-12">
                    <div id="bnkacAddWrap">
                        <div>
                            <!-- do not delte this div -->
                            <div class="panel panel-body">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <label class="mandatory mandatory_label">Type of Liability <span class="mandatory_star">*</span></label>
                                        <?php $selected=$wls_data->liabi_type; ?>
                                        <select name="liability_type" id="liability_type" class="input-select form-control" title='Please select Type of liability' alt='Please select Type of liability' alt='Please select a To be Paid by' data-validation='length' data-validation-length='1-3' data-validation-error-msg='Please select Type of Liability' required>
                                            <option value="" <?php echo ($selected=="" ) ? "selected" : "" ; ?> >Please select</option>
                                            <option value="1" <?php echo ($selected==1 ) ? "selected" : "" ; ?> >Loan</option>
                                            <option value="2" <?php echo ($selected==2 ) ? "selected" : "" ; ?> >Hypothecation</option>
                                            <option value="3" <?php echo ($selected==3 ) ? "selected" : "" ; ?> >Mortgage</option>
                                            <option value="4" <?php echo ($selected==4 ) ? "selected" : "" ; ?> >Guarantor</option>
                                            <option value="5" <?php echo ($selected==5 ) ? "selected" : "" ; ?> >Other</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Name of the Individual / Institution <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="institution_name" value="<?= $wls_data->liabi_inst_name; ?>" placeholder="Name of the Individual / Institution" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Name of the Individual / Institution" title="Please enter Name of the Individual / Institution" alt="Please enter Name of the Individual / Institution" required/>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="mandatory mandatory_label">Loan amount <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="lib_amount" value="<?= $wls_data->amount; ?>" placeholder="Name of the Individual / Institution" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Name of the Individual / Institution" title="Please enter Name of the Individual / Institution" alt="Please enter Name of the Individual / Institution" required/>
                                    </div>

                                    <div class="col-xs-3">
                                        <label class="mandatory mandatory_label">Measurement of property <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="lib_prop_mes" value="<?= $wls_data->liabi_prop_mes; ?>" placeholder="Name of the Individual / Institution" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Name of the Individual / Institution" title="Please enter Name of the Individual / Institution" alt="Please enter Name of the Individual / Institution" required/>
                                    </div>
                                    <!-- <div class="col-xs-5" style='display:none;' id="institution_as_guarantor_div">
                                        <label class="mandatory mandatory_label">Name of the Institution where you stand guarantor <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="institution_as_guarantor" value="" placeholder="Name of the Institution where you stand guarantor" maxlength="99" data-validation="" data-validation-length="" data-validation-error-msg="" title="Please enter Name of the Institution where you stand guarantor" alt="Please enter Name of the Institution where you stand guarantor" />
                                    </div> -->
                                    <!--<div class="col-xs-4">
                                                <label class="mandatory">Is this Address a Foreign Address?</label><br>
                                                <input type="checkbox" name="is_foreign_address" id="is_foreign_address" class="is_foreign_address" value="1"  changezip="zipcode" title="Please select if it is a foreign address" alt="Please select if it is a foreign address">
                                            </div>-->
                                </div>
                                <!-- row END -->
                                <div class="divider"></div>
                                <div class="row">
                                    <h4><strong>Address of the property</strong></h4>
                                    <div class="col-xs-4">
                                        <label class="mandatory ">Address Line1
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="address_line1" value="<?= $wls_data->liabi_addr_line1; ?>" placeholder="Address Line1" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1" title="Please enter Address Line1" alt="Please enter Address Line1" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory">Address Line2
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="address_line2" value="<?= $wls_data->liabi_addr_line2; ?>" placeholder="Address Line2" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2" title="Please enter Address Line2" alt="Please enter Address Line2" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        <label>Address Line3</label>
                                        <input type="text" class="form-control" name="address_line3" value="<?= $wls_data->liabi_addr_line3; ?>" placeholder="Address Line3" maxlength="60" title="Please enter Address Line3" alt="Please enter Address Line3" />
                                    </div>
                                </div>
                                <!-- row END -->
                                <div class="divider"></div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="mandatory">City / Village / Town
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="city_village_town" value="<?= $wls_data->liabi_city; ?>" placeholder="City/Town/Village" maxlength="60" title="Please enter City / Village / Town" alt="Please enter City / Village / Town" alt="Please enter City / Village / Town" alt="Please enter City / Village / Town" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town" required>
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="mandatory ">Pincode / Zipcode
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control numeric" name="zipcode" id="zipcode" value="<?= $wls_data->liabi_zipcode; ?>" placeholder="Pincode" maxlength="6" data-validation="" data-validation-length="1-6" data-validation-error-msg="Please enter Pincode" title="Please enter Pincode" alt="Please enter Pincode" required>
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="mandatory">Country
                                            <span class="mandatory_star">*</span></label>
                                        <select name="country" id="country" class="input-select form-control" title='Please select a Country' alt='Please select a Country' onchange="javascript: populate_states(this.value,'state', 'span_state', '0','toggle_other_state(\'state\',\'state_other\')'); toggle_other_state_country(this.value, 'state_other');" required>
                                            <option value="102" selected="selected">India</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-2">
                                    <?php $corre_state=$wls_data->liabi_state; ?>
                                        <label class="mandatory ">State
                                            <!-- <span class="mandatory_star">*</span> --></label> <span id="span_state">
											<select name="state" id="state"
											class="input-select form-control">
											<option value="35" <?php echo ($corre_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($corre_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($corre_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($corre_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($corre_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($corre_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($corre_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($corre_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($corre_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($corre_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($corre_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($corre_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($corre_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($corre_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($corre_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($corre_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($corre_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($corre_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($corre_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($corre_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($corre_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($corre_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($corre_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($corre_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($corre_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($corre_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($corre_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($corre_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($corre_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($corre_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($corre_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($corre_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($corre_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($corre_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($corre_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($corre_state == 19)?"selected":"" ?>>West Bengal</option>
                                        
										</select>
										</span>
									</div>
                                    <div class="col-xs-3">
                                        <span id="state_other" style='display:none;'>
                                                    <label class="cnd-mandatory ">Other State (If not listed) <!-- <span class="mandatory_star">*</span> --></label>
                                        <input type="text" class="form-control" name="state_other" value="" placeholder="Mention State Name" maxlength="60" title="Please enter Other State" alt="Please enter Other State" />
                                        </span>
                                    </div>
                                </div>
                                <!-- row END -->
                                <div class="divider"></div>
                                <div class="row">
                                    <h4><strong>Address of the individual</strong></h4>
                                    <div class="col-xs-4">
                                        <label class="mandatory ">Address Line1
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="ind_address_line1" value="<?= $wls_data->ind_addr_line1; ?>" placeholder="Address Line1" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1" title="Please enter Address Line1" alt="Please enter Address Line1" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory">Address Line2
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="ind_address_line2" value="<?= $wls_data->ind_addr_line2; ?>" placeholder="Address Line2" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2" title="Please enter Address Line2" alt="Please enter Address Line2" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        <label>Address Line3</label>
                                        <input type="text" class="form-control" name="ind_address_line3" value="<?= $wls_data->ind_addr_line3; ?>" placeholder="Address Line3" maxlength="60" title="Please enter Address Line3" alt="Please enter Address Line3" />
                                    </div>
                                </div>
                                <!-- row END -->
                                <div class="divider"></div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="mandatory">City / Village / Town
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="ind_city_village_town" value="<?= $wls_data->ind_city; ?>" placeholder="City/Town/Village" maxlength="60" title="Please enter City / Village / Town" alt="Please enter City / Village / Town" alt="Please enter City / Village / Town" alt="Please enter City / Village / Town" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town" required>
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="mandatory ">Pincode / Zipcode
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control numeric" name="ind_zipcode" id="ind_zipcode" value="<?= $wls_data->ind_zipcode; ?>" placeholder="Pincode" maxlength="6" data-validation="" data-validation-length="1-6" data-validation-error-msg="Please enter Pincode" title="Please enter Pincode" alt="Please enter Pincode" required>
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="mandatory">Country
                                            <span class="mandatory_star">*</span></label>
                                        <select name="ind_country" id="ind_country" class="input-select form-control" title='Please select a Country' alt='Please select a Country' onchange="javascript: populate_states(this.value,'state', 'span_state', '0','toggle_other_state(\'state\',\'state_other\')'); toggle_other_state_country(this.value, 'state_other');" required>
                                            <option value="102" selected="selected">India</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-2">
                                    <?php $corre_state=$wls_data->ind_state; ?>
                                        <label class="mandatory ">State
                                            <!-- <span class="mandatory_star">*</span> --></label> <span id="span_state">
											<select name="ind_state" id="ind_state"
											class="input-select form-control">
												<option value="35" <?php echo ($corre_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($corre_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($corre_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($corre_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($corre_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($corre_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($corre_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($corre_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($corre_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($corre_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($corre_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($corre_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($corre_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($corre_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($corre_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($corre_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($corre_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($corre_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($corre_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($corre_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($corre_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($corre_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($corre_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($corre_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($corre_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($corre_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($corre_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($corre_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($corre_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($corre_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($corre_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($corre_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($corre_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($corre_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($corre_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($corre_state == 19)?"selected":"" ?>>West Bengal</option>
										</select>
										</span>
									</div>
                                    <div class="col-xs-3">
                                        <span id="ind_state_other" style='display:none;'>
                                                    <label class="cnd-mandatory ">Other State (If not listed) <!-- <span class="mandatory_star">*</span> --></label>
                                        <input type="text" class="form-control" name="ind_state_other" value="" placeholder="Mention State Name" maxlength="60" title="Please enter Other State" alt="Please enter Other State" />
                                        </span>
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="mandatory">Loan Starting Date
                                            <span class="mandatory_star">*</span></label>
                                        <input type="date" name="start_date" id="start_date" placeholder="Select Starting Date" class="form-control" value="<?= $wls_data->liabi_start_date; ?>" title="Please enter Loan starting date" alt="Please enter Loan starting date" required>
                                        <!-- <input type="hidden" placeholder="Loan Starting Date" name="start_date" id="start_date" class="form-control" value="0000-00-00" maxlength="10" data-validation="" data-validation-length="10-10" data-validation-error-msg="Please enter valid Loan starting date">
                                        <input type="hidden" id="user_birth_date" value="1997-03-26"> -->
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="mandatory">Loan Closing Date
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" name="closing_date" id="closing_date" placeholder="Select Starting Date" class="form-control" value="<?= $wls_data->liabi_closing_date; ?>" title="Please enter Loan closing date" alt="Please enter Loan closing date" required>
                                        <!-- <input type="hidden" placeholder="Loan Closing Date" name="closing_date" id="closing_date" class="form-control" value="0000-00-00" maxlength="10" data-validation="" data-validation-length="10-10" data-validation-error-msg="Please enter valid Loan closing date"> -->
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="mandatory "><span id="account_num_txt">Account Number</span>
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" placeholder="Account number" name="account_number" class="form-control numeric" value="<?= $wls_data->liabi_account_no; ?>" maxlength="18" data-validation="" data-validation-length="1-18" data-validation-error-msg="Please enter valid Account number" title="Please enter Account number" alt="Please enter Account number" required>
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="mandatory "><span id="account_num_txt">Interest rate</span>
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" placeholder="Account number" name="int_rate" class="form-control numeric" value="<?= $wls_data->liabi_int_rate; ?>" maxlength="18" data-validation="" data-validation-length="1-18" data-validation-error-msg="Please enter valid Account number" title="Please enter Account number" alt="Please enter Account number" required>
                                    </div>
                                </div>
                            </div>
                            <!-- panel-body END -->
                        </div>
                    </div>
                </div>
                <!-- col-xs-12 END -->
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="div_beneficiary">
                        <div id="div_beneficiary_1">
                            <h4><strong>To be Paid by</strong></h4>
                            <div class="panel panel-body sub-panel">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <!-- <input type="radio" name="pay_from_estate" id="pay_from_estate" class="pay_from_estate" value="1"  title='From Estate' alt='From Estate' value="1" > From Estate &nbsp;&nbsp; -->
                                        <input type="radio" name="pay_from_estate" id="pay_from_estate" class="pay_from_estate" value="0" checked title='By Beneficiary listed below' alt='By Beneficiary listed below' value="1"> By Beneficiary listed below
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-body sub-panel " id="div_add_beneficiary">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">To be Paid by 
                                            <!-- <span class="mandatory_star">*</span> -->
                                        </label>
                                        <select name="sel_beneficiary" id="sel_beneficiary" class="input-select form-control" title='Please select a To be Paid by' alt='Please select a To be Paid by' >
                                            <option value="">Please select</option>
                                            <?php 
                                                $cnt=1;
                                                while($row = mysql_fetch_array($bene_data_all)) {
                                                    $html = "<option value='" . $cnt . "'>" .$row['f_name'] . "</option>";
                                                    
                                                    $cnt+= 1;
                                                    echo $html;
                                                } 
                                                ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Relation 
                                            <!-- <span class="mandatory_star">*</span> -->
                                        </label>
                                        <input type="text" class="form-control numeric_share" name="txt_beneficiary_share" id="txt_beneficiary_share" placeholder="Percentage Share to be Paid by" title="Please enter Percentage Share to be Paid by" alt="Please enter Percentage Share to be Paid by" value="" />
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory">Share</label>
                                        <input type="text" class="form-control" name="txt_other_info" id="txt_other_info" maxlength="200" placeholder="Any Other Information" title="Please enter Any Other Information" alt="Please enter Any Other Information" value="" />
                                    </div>
                                </div>
                                <div>
                                    <br><span id="span_beneficiary_1"> <a href="javascript:void(0);" alt="Add To be Paid by" title="Add To be Paid by" id="beneficiaryAddBtnNew" class="btn btn-primary btn-sm btn-add-large a_beneficiarynew">+ Add To be Paid by</a></span></div>
                                <br/>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Relation</th>
                                            <th>Share</th>
                                            <th><a href="" class="btn btn-danger">Reset all</a></th>
                                        </tr>
                                    </thead>
                                    <tbody id="tb_con_ben">
                                        <tr id="no_beneficiary">
                                            <td colspan="5"></td>
                                        </tr>
                                        <input type="hidden" name="tot_ben" id="tot_ben" value="1" />
                                        <input type="hidden" id="total_share" value="0">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- row END -->
            <div class="row">&nbsp;</div>
            <div class="bottom_buttons row">
                <div class="div_prev_button">
                    <input type="button" name="previous" id="btn-previous" value="&laquo; Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="digital-assets-details" />
                </div>
                <div class="div_center_buttons">
                    <!-- <input type="submit" name="add_liability" id="btn-add" value="Add / Save" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " /> -->
                    <!-- <input type="submit" name="update" id="btn-update" value="Update" title="Click here to Update the changed data" alt="Click here to Update the changed data" class="btn-submit btn-submit-disabled" disabled/> -->
                </div>
                <div class="div_next_button" style="margin-right: -30px; text-align: right;">
                    <input type="submit" name="add_liability" id="btn-add" value="Save & Next »" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " />
                    <!-- <input type="button" name="next" id="btn-next" value="Next &raquo;" title="Click here to go to Next page" alt="Click here to go to Next page" class="btn-submit btn-move" next_page="non-beneficiaries" /> -->
                </div>
                <br>
                <br>
                <br>
            </div>
            <!-- <div class="row">
                <input type="hidden" name="txt_other_submit" id="txt_other_submit" value="0">
                <input type="hidden" name="txt_redirect_page" id="txt_redirect_page" value="">
                <input type="submit" name="sumbit" value="Submit" class="btn-submit btn-submit-disabled" title="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" alt="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" disabled/> </div>
            <div class="row">&nbsp;</div> -->
        </div>
    </div>
    <!-- gen-row END -->
    <!-- main END -->
</form>
