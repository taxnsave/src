<?php
	//echo "<pre>";print_r($_SESSION);
	$bankInfo = $customerProfile->getCustomerBankInfo();
	//print_r($bankInfo);
?>
<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo $CONFIG->siteurl;?>mySaveTax/">Home</a>
							</li>
							<li class="active">My Profile</li>
						</ul><!-- /.breadcrumb -->

						<?php include("form.search.php");?>

						<!-- /section:basics/content.searchbox -->
					</div>
					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">						
						<div class="row">
							<div class="col-xs-12">
                            	<div class="space-4"></div><div class="space-8"></div>
								<div class="active">
									<div id="user-profile-2" class="user-profile">
										<div class="tabbable">
											<ul class="nav nav-tabs padding-18">
												<li class="active">
													<a data-toggle="tab" href="#home">
														<i class="green ace-icon fa fa-user bigger-120"></i>
														Profile
													</a>
												</li>

												<li>
													<a data-toggle="tab" href="#feed">
														<i class="orange ace-icon fa fa-book bigger-120"></i>
														Personal Details
													</a>
												</li>

												<li>
													<a data-toggle="tab" href="#friends">
														<i class="blue ace-icon fa fa-hdd-o bigger-120"></i>
														Bank Details
													</a>
												</li>

												<li>
													<a data-toggle="tab" href="#settings">
														<i class="pink ace-icon fa fa-cog bigger-120"></i>
														Settings
													</a>
												</li>
											</ul>

											<div class="tab-content no-border padding-24">
												<div id="home" class="tab-pane in active">
													<div class="row">
														<div class="col-xs-12 col-sm-3 center"><div class="space space-12"></div>
															<span class="profile-picture">
																<img class="editable img-responsive" alt="<?php echo $CONFIG->loggedUserName; ?>" id="avatar" src="<?php echo $loggedUserImage; ?>" />
															</span>

															<div class="space space-4"></div>

															
														</div><!-- /.col -->

														<div class="col-xs-12 col-sm-9">
															<h4 class="orange">																
																<span class="label label-purple arrowed-in-right">
																	<i class="ace-icon fa fa-circle smaller-80 align-middle"></i>
																	&nbsp;<strong>A/c</strong> &nbsp; 
																</span>
                                                                <span class="middle"><?php echo $CONFIG->loggedUserName; ?></span>
															</h4>
															<div class="profile-user-info profile-user-info-striped">
												<div class="profile-info-row">
													<div class="profile-info-name"> Email </div>

													<div class="profile-info-value">
														<span class="editable" id="email"><?php echo $CONFIG->loggedUserEmail; ?></span>
													</div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> Name </div>

													<div class="profile-info-value">
														<span class="editable" id="cust_name"><?php echo $profileInfo[cust_name]; ?></span>
													</div>
												</div>							
												<div class="profile-info-row">
													<div class="profile-info-name"> Alternate Email </div>

													<div class="profile-info-value">
														<span class="editable" id="alternet_email_id"><?php echo trim($profileInfo[alternet_email_id]);?></span>
													</div>
												</div>		
                                                <div class="profile-info-row">
													<div class="profile-info-name"> Mobile </div>
													<div class="profile-info-value">
														<span class="editable" id="contact_no"><?php echo trim($profileInfo[contact_no]); ?></span>
													</div>
												</div>  										
												<div class="profile-info-row">
													<div class="profile-info-name"> Customer ID </div>
													<div class="profile-info-value">
														<span><strong><?php echo trim($profileInfo[fr_customer_id]);?></strong></span>
													</div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> Change Password </div>
													<div class="profile-info-value">
														<span class="editable" id="change_pass">Click To Change</span>
													</div>
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> Joined </div>
													<div class="profile-info-value">
													<span><?php echo $commonFunction->dateFormatWithTime($profileInfo[created_date]);?></span>
													</div>
												</div>

												<div class="profile-info-row">
													<div class="profile-info-name"> Last Login </div>
													<div class="profile-info-value">
														<span><?php echo $commonFunction->dateFormatWithTime($profileInfo[last_login]);?></span>
													</div>
												</div>
											</div>
															<div class="hr hr-8 dotted"></div>															
														</div><!-- /.col -->
													</div><!-- /.row -->

													<div class="space-20"></div>

													
												</div><!-- /#home -->

												<div id="feed" class="tab-pane">
													<div id="" class="tab-pane in ">
													<div class="row">
														<div class="col-xs-12 col-sm-9">
															<h4 class="orange">																
																<span class="label label-purple arrowed-in-right">
																	<i class="ace-icon fa fa-circle smaller-80 align-middle"></i>
																	&nbsp;<strong>A/c</strong> &nbsp; 
																</span>
                                                                <span class="middle"><?php echo $CONFIG->loggedUserName; ?></span>
															</h4>

															<div class="profile-user-info profile-user-info-striped">
												<div class="profile-info-row">
													<div class="profile-info-name"> Father's Name </div>
													<div class="profile-info-value">
														<span class="editable" id="fath_name"><?php echo trim($profileInfo[father_name]); ?></span>
													</div>
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> Date Of Birth </div>
													<div class="profile-info-value">
														<span class="editable" id="dob"><?php echo trim($profileInfo[dob]); ?></span>
													</div>
												</div>                                                
                                                <div class="profile-info-row">
													<div class="profile-info-name"> Age </div>
													<div class="profile-info-value">
														<span id="calcAge"><?php echo $customerProfile->customerAge($profileInfo['dob']); ?></span>
													</div>
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> Gender </div>
													<div class="profile-info-value">
														<span>
														<div class="radio">
													<label>
														<input type="radio" class="ace" name="form-gender-radio" onchange="ajaxCallKeyValue('sex',this.value);" value="Male"  <?php if($profileInfo[sex] == "Male") echo "checked"; ?>>
														<span class="lbl"> Male</span>
													</label>
                                                    <label>
														<input type="radio" class="ace" name="form-gender-radio" onchange="ajaxCallKeyValue('sex',this.value);" value="Female"  <?php if($profileInfo[sex] == "Female") echo "checked"; ?>>
														<span class="lbl"> Female</span>
													</label>                                                    
												</div>
													</span>
													</div>
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> Profession </div>
													<div class="profile-info-value">
														<span class="editable" id="profession"><?php echo trim($profileInfo[profession]); ?></span>
													</div>
												</div>                                              																		
												<div class="profile-info-row">
													<div class="profile-info-name"> Address </div>
													<div class="profile-info-value">
														<span class="editable" id="address1"><?php echo trim($profileInfo[address1]);?></span>
                                                        <!--<span class="editable" id="address2"><?php echo trim($profileInfo[address2]);?></span>-->
													</div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> City </div>

													<div class="profile-info-value">
													<span class="editable" id="city1"><?php echo trim($profileInfo[city]);?></span>
													</div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> State </div>

													<div class="profile-info-value">
														<span class="editable" id="state"><?php echo trim($profileInfo[state]);?></span>
													</div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> Zip </div>

													<div class="profile-info-value">
														<span class="editable" id="pincode"><?php echo trim($profileInfo[pincode]);?></span>
													</div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> Country </div>

													<div class="profile-info-value">
														<span class="editable" id="country"><?php echo trim($profileInfo[country]);?></span>
													</div>
												</div>
                                                 <div class="profile-info-row">
													<div class="profile-info-name"> PAN </div>
													<div class="profile-info-value">
														<span class="editable" id="pan"><?php echo trim($profileInfo[pan_number]); ?></span>
													</div>
												</div>  
                                                 <div class="profile-info-row">
													<div class="profile-info-name"> Aadhaar No. </div>
													<div class="profile-info-value">
														<span class="editable" id="aadhar"><?php echo trim($profileInfo[aadhaar_no]); ?></span>
													</div>
												</div>  
											</div>

															<div class="hr hr-8 dotted"></div>

															
														</div><!-- /.col -->
													</div><!-- /.row -->

													<div class="space-20"></div>

													
												</div><!-- /.row -->

													<div class="space-12"></div>

													
												</div><!-- /#feed -->


												<div id="friends" class="tab-pane">
													<!-- #section:pages/profile.friends -->
													<div class="profile-users clearfix">
														<div class="row">
                                                        <h4 class="orange">																
																<span class="label label-purple arrowed-in-right">
																	<i class="ace-icon fa fa-circle smaller-80 align-middle"></i>
																	&nbsp;<strong>A/c</strong> &nbsp; 
																</span>
                                                                <span class="middle"><?php echo $CONFIG->loggedUserName; ?></span>
															</h4>
                                                        <div class="col-xs-12 col-sm-9">
															<div class="profile-user-info profile-user-info-striped">
												<div class="profile-info-row">
													<div class="profile-info-name"> Bank Name </div>
													<div class="profile-info-value">
														<span class="editable" id="bank_name"><?php echo trim($bankInfo[bank_name]); ?></span>
													</div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> Account Number </div>
													<div class="profile-info-value">
														<span class="editable" id="acc_no"><?php echo trim($bankInfo[acc_no]); ?></span>
													</div>
												</div>  
                                                <div class="profile-info-row">
													<div class="profile-info-name"> IFSC </div>
													<div class="profile-info-value">
														<span class="editable" id="ifsc_code"><?php echo trim($bankInfo[ifsc_code]); ?></span>
													</div>
												</div>    
                                                <div class="profile-info-row">
													<div class="profile-info-name"> IBAN/Swift Code </div>
													<div class="profile-info-value">
														<span class="editable" id="swift_code"><?php echo trim($bankInfo[swift_code]); ?></span>
													</div>
												</div>  
                                                <div class="profile-info-row">
													<div class="profile-info-name"> Address </div>
													<div class="profile-info-value">
														<span class="editable" id="bank_address"><?php echo trim($bankInfo[address1]); ?></span>
													</div>
												</div>    
                                                <div class="profile-info-row">
													<div class="profile-info-name"> City </div>
													<div class="profile-info-value">
														<span class="editable" id="bank_city"><?php echo trim($bankInfo[city]); ?></span>
													</div>
												</div>    
                                                <div class="profile-info-row">
													<div class="profile-info-name"> State </div>
													<div class="profile-info-value">
														<span class="editable" id="bank_state"><?php echo trim($bankInfo[state]); ?></span>
													</div>
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> Country </div>
													<div class="profile-info-value">
														<span class="editable" id="bank_country"><?php echo trim($bankInfo[country]); ?></span>
													</div>
												</div>                                      
											</div>
										</div>
										
													</div>
													</div>
												</div><!-- /#friends -->

												<div id="settings" class="tab-pane">
													<div class="profile-users clearfix">
														<div class="row">
                                                        <h4 class="orange">																
																<span class="label label-purple arrowed-in-right">
																	<i class="ace-icon fa fa-circle smaller-80 align-middle"></i>
																	&nbsp;<strong>A/c</strong> &nbsp; 
																</span>
                                                                <span class="middle"><?php echo $CONFIG->loggedUserName; ?></span>
															</h4>
                                                        <div class="col-xs-4 col-sm-9">															
															<div class="profile-user-info profile-user-info-striped">
												<div class="profile-info-row">
													<div class="profile-info-name"> Communication Email </div>
													<div class="profile-info-value">
														<span class="small">Default is Login id, for communication.</span>
													</div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> LoginId</div>

													<div class="profile-info-value">
														<div class="radio">
													<label>
														<input type="radio" class="ace" name="form-field-radio" onchange="ajaxCallKeyValue('communication_email',this.value);" value="Permanent"  <?php if($profileInfo[communication_email] == "Permanent") echo "checked"; ?>>
														<span class="lbl"> <?php echo $CONFIG->loggedUserEmail; ?></span>
													</label>
												</div>
													</div>
												</div>  
                                                <div class="profile-info-row">
													<div class="profile-info-name"> Alternet Email Id </div>
													<div class="profile-info-value">
														<div class="radio">
													<label>
														<input type="radio" class="ace" name="form-field-radio" onchange="ajaxCallKeyValue('communication_email',this.value);" value="Alternate" <?php if($profileInfo[communication_email] == "Alternate") echo "checked"; ?>>
														<span class="lbl"> <span><?php echo $profileInfo[alternet_email_id]; ?></span></span>
													</label>
												</div>
													</div>
												</div>                                                  
											</div>
										</div>
													</div>
													</div>
												</div><!-- /#pictures -->
											</div>
										</div>
									</div>
								</div>								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div>
           
           
