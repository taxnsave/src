<!-- Navbar -->
<nav class="navbar navbar-inverse my-nav">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand"
				href="<?php echo $CONFIG->siteurl;?>home.html"><img
				src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png"
				alt="Logo" width="160"></a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
				<li class="active"><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
			</ul>
			<ul class="nav navbar-nav brdr">
				<li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create
						Will</a></li>
			</ul>
		</div>
	</div>
</nav>
<!-- Navbar -->

<!-- Banner -->
<div class="banner_will">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
				<h1>
					<center>Sign In for a better future</center>
					<br>
					<center>
						<span class="span-end">SECURE your family's future</span>
					</center>
				</h1>
			</div>

		</div>
	</div>
</div>
<!-- Banner -->

<style>
<!--
.pointer {cursor: pointer;}
-->
</style>

<section id="page_content" class="">
	<div class="container">
			<div class="row">
			<center>
			<div class="col-xs-4"> </div>
				<div class="col-xs-4">
					<div id="login-box" class="login-box visible widget-box no-border">
						<div class="widget-body">
							<div class="widget-main">
								<h4 class="header blue lighter bigger">
									<i class="ace-icon fa fa-sign-in green"></i> Please Login
								</h4>
								<div class="space-6"></div>
								<form class="loginForm" role="form" method="post"
									action="ajax-request/ajax_response.php?action=doLogin&subaction=loginSubmit"
									onSubmit="loginJS(this);return false;" name="login_frm"
									accept-charset="UTF-8" id="login-nav">
									<fieldset>
										<label class="block clearfix">
											<div class="input-group input-group-unstyled">
												<input type="email" name="email" class="form-control"
													id="exampleInputEmail2" placeholder="Email address"
													required> 
													<span class="input-group-addon"> <i
													class="ace-icon fa fa-user"></i>
												</span>
											</div>
										</label>
										<div class="space-6"></div>
										<label class="block clearfix">
										<div class="input-group input-group-unstyled">
										 <input
												type="password" name="passwd" class="form-control"
												placeholder="Password" required> 
												<span class="input-group-addon">
												<i class="ace-icon fa fa-lock"></i>
												</span>
										</div>
										</label>

										<div class="space-22"></div>

										<div class="clearfix">
											<label class="inline"> <!-- <input class="ace" type="checkbox">
                                        <span class="lbl"> Remember Me</span>-->
											</label>

											<button id="loginFrm" name="loginFrm" type="submit"
												class="width-35 pull-center btn btn-sm btn-primary">
												<i class="ace-icon fa fa-key"></i> <span class="bigger-110">Login</span>
											</button>
											<div style="height: 30px;">
												<div style="display: none;" id="signup_loader">
													<img src="static/img/formsubmitpreloader.gif">
												</div>
												<div style="display: none;" id="loginfailed" class="orange">
													<strong>Could not login. Please check if email address and
														password are correct.</strong>
												</div>
											</div>

										</div>


									</fieldset>
								</form>
								<div class="space-8"></div>
							</div>
						</div>
						<a class="pointer" id="forgotpass">Forgot password?</a> <br>
						<a class="pointer" id="registernow">New user? Register Now</a>
						<div class="space-22" style="height:50px"></div>
					</div>

					
					<div id="forget_pass_box" style="display: none">
						<div class="social-or-login center">
							<span class="bigger-110">
							<h4 class="header blue lighter bigger">
									<i class="ace-icon fa fa-repeat green"></i> Recover Password
								</h4>
								</span>
						</div>
						<form
							action="ajax-request/ajax_response.php?action=doResetPassword&subaction=submit"
							method="post" name="reset_frm" id="reset_frm"
							onSubmit="doResetPasswordJS(this);return false;">
							<fieldset>
								<label class="block clearfix">
									<div class="input-group input-group-unstyled">
										<input type="email" id="reset_email" name="reset_email"
											class="form-control required" placeholder="Email" /> <span
											class="input-group-addon"> <i class="ace-icon fa fa-user"></i>
										</span>
									</div>
								</label>
								<div class="space-6"></div>
								<label class="block clearfix">
									<div class="input-group input-group-unstyled">
										<input type="text" id="reset_sec_code" name="reset_sec_code"
											class="form-control required" placeholder="Security Code"
											required /> <span class="input-group-addon"> <i
											class="ace-icon fa fa-key"></i>
										</span> 
										</div>
										<br><img
											src="__lib.apis/captcha/securimage_show.php?tt="
											style="border: 1px dotted #FFBF00" id="resetverifyCaptcha" />
											<br><a id="reloadcaptcharessetpassword">Reload Security Code</a><br>
								
								</label>
								<div class="clearfix">
									<button id="reset_submit" name="reset_submit" type="submit"
										class="width-35 pull-center btn btn-sm btn-primary">
										<i class="ace-icon fa fa-key"></i> <span class="bigger-110">Reset Password</span>
									</button>
									<div style="height: 30px;">
										<div style="display: none;" id="reset_loader">
											<img src="static/img/formsubmitpreloader.gif">
										</div>
										<div style="display: none;" id="reset_invalid_email"
											class="orange">
											<strong>Invalid email.</strong>
										</div>

									</div>
								</div>

							</fieldset>
						</form>
						<div class="space-4"></div>
						
                        <a  class="pointer" id="backtosignin">Back to signin</a> <br>
						<a  class="pointer" id="registernow1">New user? Register Now</a>
						<div class="space-22" style="height:50px"></div>
					</div>
					
					<div id="register_box" style="display: none">
						<div class="social-or-login center">
							<span class="bigger-110">
							<h4 class="header blue lighter bigger">
									<i class="ace-icon fa fa-id-badge green"></i> Register now
								</h4>
								</span>
						</div>
						<form
										action="ajax-request/ajax_response.php?action=doSignup&subaction=submit"
										method="post" name="signup_frm" id="signup_frm"
										onSubmit="signupJSnew(this);return false;">
										<fieldset>
								<label class="block clearfix">
									<div class="input-group input-group-unstyled">
										<input type="email" id="email" name="email"
											class="form-control form-control-lg required"
											placeholder="Email" required autofocus> <span
											class="input-group-addon"> <i class="ace-icon fa fa-envelope"></i>
										</span>
									</div>
								</label>
								<div class="space-6"></div>
								<label class="block clearfix">
									<div class="input-group input-group-unstyled">
										<input type="text" id="name" name="name"
											class="form-control form-control-lg required"
											placeholder="Name" required> <span class="input-group-addon">
											<i class="ace-icon fa fa-user"></i>
										</span>
									</div>

								</label>
								<div class="space-6"></div>
								<label class="block clearfix">
									<div class="input-group input-group-unstyled">
										<input type="mobile" id="mobile" name="mobile"
											class="form-control form-control-lg required"
											placeholder="Mobile Number" required> <span
											class="input-group-addon"> <i
											class="ace-icon fa fa-mobile-phone"></i>
										</span>
									</div>

								</label>
								<div class="space-6"></div>
								<div class="clearfix">
									<button type="button"
										class="width-65 pull-center btn btn-sm btn-success"
										id="otpFrm" name="otpFrm" onclick="sendOTPJS(this)">
										<span class="bigger-110">Send OTP</span> <i
											class="ace-icon fa fa-arrow-right icon-on-right"></i>
									</button>
									<div style="display: none;" id="signup_invalid_mob"
										class="orange">
										<strong>Please enter a valid mobile number to send OTP.</strong>
									</div>
									<div style="display: block;" id="signup_invalid_mob"
										class="orange">
										<br>
										<br>
									</div>
								</div>
								<div class="space-6"></div>
								<label class="block clearfix">
									<div class="input-group input-group-unstyled">
										<input type="text" id="otp" name="otp"
											class="form-control form-control-lg" placeholder="Enter OTP"
											required /><span class="input-group-addon"> <i
											class="ace-icon fa fa-unlock-alt"></i>
										</span>
									</div>
								</label>

								<div class="space-6"></div>

								<label class="block clearfix">
									<div class="input-group input-group-unstyled">
										<input type="password" id="reg_passwd" name="reg_passwd"
											class="form-control form-control-lg required"
											placeholder="Password" required> <span
											class="input-group-addon"> <i class="ace-icon fa fa-lock"></i>
										</span>
									</div>
								</label>
								<div class="space-6"></div>
								<label class="block clearfix"><div
										class="input-group input-group-unstyled">
										<input type="password" id="repasswd" name="repasswd"
											class="form-control form-control-lg  required"
											placeholder="Confirm Password" required> <span
											class="input-group-addon"> <i class="ace-icon fa fa-lock"></i>
										</span>
									</div> </label>
								<div class="space-6"></div>
								<label class="block clearfix">
									<div class="input-group input-group-unstyled">
										<input type="text" id="sec_code" name="sec_code"
											class="form-control form-control-lg  required"
											placeholder="Security Code" required /> <span
											class="input-group-addon"> <i class="ace-icon fa fa-key"></i>
										</span>
									</div>
									<div class="space-6"></div><br> <img
									src="captcha/securimage_show.php?tt="
									style="border: 1px dotted #FFBF00" id="verifyCaptcha" />
									<br><a id="reloadcaptchasingup">Reload Security Code</a><br>
								
								</label>

								<div class="space-6"></div>


											<div class="space-12"></div>

<div> 
                                                By registering on this website you agree to the <a href="termsofuse.html">Terms and Conditions</a> of TaxSave. <br>
                                                </div>

											<div class="clearfix">

												<button type="submit"
													class="width-65 pull-center btn btn-sm btn-success"
													id="signupFrm" name="signupFrm">
													<span class="bigger-110">Register</span> <i
														class="ace-icon fa fa-arrow-right icon-on-right"></i>
												</button>
												<div style="height: 30px;">
													<div style="display: none;" id="signup_loader">
														<img src="static/img/formsubmitpreloader.gif">
													</div>
													<div style="display: none;" id="signup_invalid_email"
														class="orange">
														<strong>Email already exist.</strong>
													</div>
												</div>
                                                
                                                <div style="display: block;" id="signup_invalid_mob" class="orange">
												</div>
											</div>
										</fieldset>
									</form>
						<div class="space-4"></div>
						
                        <a  class="pointer" id="backtosignin1">Back to signin</a> <br>
						<a  class="pointer" id="forgotpass1">Forgot password?</a>
						<div class="space-22" style="height:50px"></div>
					</div>

					<div class="space-4"></div>
					<div class="space-22"></div>
					<div class="space-24"></div>
				</div>
				</center>
			</div>
	</div>
</section>


<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">

	$(document).ready(function(){
		//alert("test");
		$("#forgotpass").click(function(){ 
			//alert("test");
			showforgotpassword();
		}); 
		$("#forgotpass1").click(function(){ 
			//alert("test");
			showforgotpassword();
		});
			
        $("#backtosignin").click(function(){ 
			//alert("test");
        	showsignin();
		}); 	
        $("#backtosignin1").click(function(){ 
			//alert("test");
        	showsignin();
		});
		
		$("#registernow").click(function(){ 
			//alert("test");	
			registernow();
		});  
		$("#registernow1").click(function(){ 
			//alert("test");	
			registernow();
		}); 

		$("#reloadcaptchasingup").click(function(){
			$("#verifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
		});
		
		$("#reloadcaptcharessetpassword").click(function(){
			$("#resetverifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
		});
	}); 
	

	function registernow() {
		var x = document.getElementById("login-box");
        x.style.display="none";
        x = document.getElementById("forget_pass_box");
        x.style.display="none";
        x = document.getElementById("register_box");
        x.style.display="block";
        $("#verifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
        $("#resetverifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
	}
	function showsignin() {
		var x = document.getElementById("login-box");
        x.style.display="block";
        x = document.getElementById("forget_pass_box");
        x.style.display="none";
        x = document.getElementById("register_box");
        x.style.display="none";
        $("#verifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
        $("#resetverifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
	}
	function showforgotpassword() {
		var x = document.getElementById("login-box");
        x.style.display="none";
        x = document.getElementById("forget_pass_box");
        x.style.display="block";
        x = document.getElementById("register_box");
        x.style.display="none";
        $("#verifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
        $("#resetverifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
	}
    
	function looksLikeMail(str) {
	    var lastAtPos = str.lastIndexOf('@');
	    var lastDotPos = str.lastIndexOf('.');
	    return (lastAtPos < lastDotPos && lastAtPos > 0 && str.indexOf('@@') == -1 && lastDotPos > 2 && (str.length - lastDotPos) > 2);
	}
	
	function signupJSnew(form)
	{       
		//var re = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

		if(!(looksLikeMail($("#email").val()))) 
		{
			//alert("test");
			 $("#signup_invalid_email").html('Please enter valid email address.');
		        $("#signup_invalid_email").css("display", "block");
		        return false;
		}    
	    if(!(/^[0-9\-]+$/.test($("#mobile").val())))
	    {
	        $("#signup_invalid_email").html('Please enter valid mobile no.');
	        $("#signup_invalid_email").css("display", "block");
	        return false;
	    }
	    if($("#mobile").val().length != 10)
	    {
	        $("#signup_invalid_email").html('Please enter valid mobile no.');
	        $("#signup_invalid_email").css("display", "block");
	        return false;
	    }
	    if($("#reg_passwd").val().length < 6)
	    {
	        $("#signup_invalid_email").html('Password must be 6 character long.');
	        $("#signup_invalid_email").css("display", "block");
	        return false;
	    }		
	    if($("#reg_passwd").val() != $("#repasswd").val())
	    {
	        $("#signup_invalid_email").html('Password Mismatch.');
	        $("#signup_invalid_email").css("display", "block");
	        return false;
	    }
	            if($("#otp").val().length != 5) 
	            {
	                $("#signup_invalid_email").html('Please enter OTP.');
	                $("#signup_invalid_email").css("display", "block");
	                return false;
	            }
	    $("#signup_invalid_email").html('');		
	    $("#signup_loader").css("display", "block");	
	    $("#signup_invalid_email").css("display", "none");
	    //alert("inside js");
	    $.ajax({
	        cache:false,
	        url: form.action,
	        type: form.method,
	        data: $(form).serialize(),
	        success: function(response) {
	        //alert("success");
	            if(response.indexOf("WRONG_PASSCODE")>=0)
	            {
	                $("#verifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
	                $("#signup_loader").css("display", "none");
	                $("#signup_invalid_email").html('Invalid security code.');
	                $("#signup_invalid_email").css("display", "block");
	                $("#email").focus();
	            }
	            else if(response.indexOf("WRONG_OTP")>=0)
	            {
	                $("#signup_invalid_email").html('Invalid OTP.');
	                $("#signup_invalid_email").css("display", "block");
	                $("#otp").focus();
	            }
	            else if(response.indexOf("REGISTER_DONE")>=0)
	            {									
	                //alert("inside_register");
	                location.href='<?php echo $CONFIG->siteurl;?>mySaveTax/';				
	            }
	            else
	            {
	                $("#verifyCaptcha").attr('src', "captcha/securimage_show.php?"+$.now());
	                $("#signup_loader").css("display", "none");
	                $("#signup_invalid_email").html('Email Already Exists.');    
	                $("#signup_invalid_email").css("display", "block");
	                $("#email").focus();
	                //html('Email Already Exists.');
	            }
	        }            
	    });
	    return false;
	}

	            
	function sendOTPJS(form) 
	{
		$("#signup_invalid_mob").css("display", "none");
		//alert("test");
		if(!(looksLikeMail($("#email").val())))
		{
			//alert("test");
			 $("#signup_invalid_mob").html('Please enter valid email address.');
		        $("#signup_invalid_mob").css("display", "block");
		        return false;
		}  
		
	    if((!(/^[0-9\-]+$/.test($("#mobile").val())))||($("#mobile").val().length != 10))
	    {
	        $("#signup_invalid_mob").html('Please enter valid mobile no.');
	        $("#signup_invalid_mob").css("display", "block");
	        return false;
	    }   
	            
	            $mobileno = $("#mobile").val();
	            //alert($mobileno);
	            
	    $("#signup_invalid_mob").html('OTP is being sent... please wait...');       
	    $("#signup_invalid_mob").css("display", "block");
	    
	    var mobileno = $("#mobile").val();
	    var email = $("#email").val();
	    
	    $.ajax({
	        url: 'ajax-request/ajax_response.php?action=doSendOTP',
	        type: 'POST',
	        data: 'mobile=' + mobileno + '&email=' + email, 
	        success: function(response) {      
	           
	            if(response.includes('OTP_SENT'))
	            {   
	                $("#signup_invalid_mob").html('OTP is sent.Please wait for 2 to 3 minutes before requesting new OTP.');       
	                $("#signup_invalid_mob").css("display", "block");
	            } else if(response.includes('EMAIL_EXISTS')) {
	            	$("#signup_invalid_mob").html('The email already exists.');       
	                $("#signup_invalid_mob").css("display", "block");
	            }
	            else {
	                $("#signup_invalid_mob").html(response);  
	                $("#signup_invalid_mob").css("display", "block");
	            }
	        }            
	    });
	            return false;
	            
	}

	</script>
