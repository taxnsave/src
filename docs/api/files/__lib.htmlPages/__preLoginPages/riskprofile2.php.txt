<!-- Navbar -->
<nav class="navbar navbar-inverse my-nav">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
            </button>
            <a class="navbar-brand" href="<?php echo $CONFIG->siteurl;?>home.html"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png" alt="Logo" width="160"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li ><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
            </ul>
            <ul class="nav navbar-nav brdr">
                <li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
                <li ><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create Will</a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- Navbar -->
<!-- Banner -->
<div class="banner_will">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
                <h1>
                    <center> Calculate your Risk</center>
                </h1>
                <center><span class="span-endline"></span></center>
            </div>
        </div>
    </div>
</div>
<!-- Banner -->
<style >
    h4 {color: darkblue;line-height: 1.3; font-size: 22px;    margin-bottom: 40px; }
</style>
<div class="about-area-one section-spacing">
    <div class="inner-about">
        <div class="container">
            <div class="row about-content">
                <div class="col-lg-12 col-md-12 col-sm-12">
                     <div class="jumbotron-contents">
    <h4>To assess your risk tolerance Seven questions are given below. Each question is followed by three, possible answers. Circle the letter that corresponds to your answer.</h4>
    </div>
	<form action="">
  <div class="content-row">
                  <h2 class="content-row-title">
                  </h2>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="row">
                        
						 <div class="form-group">
						  <label for="exampleInputEmail1">1.What is your Investment Horizon? How long can you keep your money invested in the market before needing access to it? </label>
                          <div class="radio">
                               <input class="calc" type="radio" name="radio1" value="1">A.Up to two years <br>
                               <input class="calc" type="radio" name="radio1" value="2">B.Two to three years<br>
                             <input class="calc" type="radio" name="radio1" value="3">C.Three to five years <br>
							 <input class="calc" type="radio" name="radio1" value="4">D.Five years to Ten years<br>
							 <input class="calc" type="radio" name="radio1" value="5">E.Ten years and more<br>
                          </div>
						  </div>  
                          <div class="form-group">
						  <label for="exampleInputEmail1">2.The age group you belong to:</label>
                               
						  <div class="radio">
                             <input class="calc" type="radio" name="radio2" value="1">A.Less than 25 years <br>
                              <input class="calc" type="radio" name="radio2" value="2">B.25 - 35 years <br>
                               <input class="calc" type="radio" name="radio2" value="3">C.36 - 50 years <br>
							    <input class="calc" type="radio" name="radio2" value="4">D.51 years above<br>
                          </div>
						  
						   <label for="exampleInputEmail1"> 3.How well do you understand investing in the markets?</label>
						   <div class="radio">
                             <input class="calc" type="radio" name="radio3" value="1">A.I am a novice. I don't understand the markets at all. <br>
                               <input class="calc" type="radio" name="radio3" value="2">B.I have basic understanding of investing. I underst and the risks and basic investment concepts like diversification.<br>
                                <input class="calc" type="radio" name="radio3" value="3">C.I have an amateur interest in investing. I have invested earlier on my own. I understand how markets fluctuate and the pros and cons of different investment classes.<br>
								<input class="calc" type="radio" name="radio3" value="4">D.I am an experienced investor. I have invested in different markets 
								and understand different investment strategies. I have my own investment philosophy.<br>


                          </div>
						  </div> 
             <div class="form-group">
			 <label for="exampleInputEmail1">4.My current and future income sources (example: salary, business income, etc) are:?</label>
			  <div class="radio">
                              <input class="calc" type="radio" name="radio4" value="1">A.Very unstable <br>
                               <input class="calc" type="radio" name="radio4" value="2">B.Unstable <br>
                               <input class="calc" type="radio" name="radio4" value="3">C.Some what stable<br>
							   <input class="calc" type="radio" name="radio4" value="4">D.Stable <br>
							    <input class="calc" type="radio" name="radio4" value="5">E.Very Stable<br>
                          </div>
                 </div> 
				 
				 <div class="form-group">
			 <label for="exampleInputEmail1">5.From the following 5 possible investment scenario, please select the option which defines your investment objective?</label>
			  <div class="radio">
                               <input class="calc" type="radio" name="radio5" value="1">A.I cannot consider any Loss<br>
                              <input class="calc" type="radio" name="radio5" value="2">B.I can consider Loss of 4% if the possible Gains are of 10%<br>
                               <input class="calc" type="radio" name="radio5" value="3">C.I can consider Loss of 8% if the possible Gains are of 22%<br>
							   <input class="calc" type="radio" name="radio5" value="4">D.I can consider Loss of 14% if the possible Gains are of 30%<br>
							   <input class="calc" type="radio" name="radio5" value="5">E.I can consider Loss of 25% if the possible gains are of 50%<br>
                          </div>
                 </div> 
				 <div class="form-group">
			 <label for="exampleInputEmail1">6.If your investment outlook is long-term (more than five years), 
			 how long will you hold on to a poorly performing portfolio before cashing in?</label>
			  <div class="radio">
<input class="calc" type="radio" name="radio6" value="1">A.Immediately liquidate if there is an erosion of my capital<br>
                             <input class="calc" type="radio" name="radio6" value="1">B.I'd hold for 3 months <br> 
                             <input class="calc" type="radio" name="radio6" value="2">C.I'd hold for 6 months<br>
							 <input class="calc" type="radio" name="radio6" value="3">D.I'd hold for one year <br>
							 <input class="calc" type="radio" name="radio6" value="4">E.I'd hold for up to two years<br>
                          </div>
                 </div> 
				 <div class="form-group">
			 <label for="exampleInputEmail1">7.Volatile investments usually provide higher returns and tax efficiency. What is your desired balance?</label>
			  <div class="radio">
                             <input class="calc" type="radio" name="radio7" value="1">A.Preferably guaranteed returns, before tax efficiency<br>
                              <input class="calc" type="radio" name="radio7" value="2">B.Stable, reliable returns, minimal tax efficiency<br>
                              <input class="calc" type="radio" name="radio7" value="3">C.Some variability in returns, some tax efficiency<br>
							  <input class="calc" type="radio" name="radio7" value="4">D.Moderate variability in returns, reasonable tax efficiency<br>
							  <input class="calc" type="radio" name="radio7" value="5">E.Unstable, but potentially higher returns, maximizing tax efficiency<br>
                          </div>
						 
                 </div> 
				 <div class="form-group">
			 <label for="exampleInputEmail1">8.If a few months after investing, the value of your investments declines by 20%, what would you do?</label>
			  <div class="radio">
                             <input class="calc" type="radio" name="radio8" value="1">A.Cut losses immediately and liquidate all investments. Capital preservation is paramount.<br>
                              <input class="calc" type="radio" name="radio8" value="2">B.Cut your losses and transfer investments to safer asset classes.<br>
                              <input class="calc" type="radio" name="radio8" value="3">C.You would be worried, but would give your investments a little more time.<br>
							  <input class="calc" type="radio" name="radio8" value="4">D.You are ok with volatility and accept decline in portfolio value as a part of investing. You would keep your investments as they are.<br>
							  <input class="calc" type="radio" name="radio8" value="5">E.You would add to your investments to bring the average buying price lower. You are confident about your investments and are not perturbed by notional losses.<br>
                          </div>
						 
                 </div> 
				 <div class="form-group">
			 <label for="exampleInputEmail1">9.Which of these scenarios best describes your "Risk Range"? What level of losses and profits would you be comfortable with?</label>
			
						  <div class="bs-example">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Select</th>
                            <th>Choice</th>
                            <th>Worst year</th>
                            <th>Best Year</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><input type="radio" class="calc" name="radio9" value="1">A</td>
                            <td>Investment A</td>
                            <td>1 %</td>
                            <td>15%</td>
                          </tr>
                          <tr>
						    <td><input type="radio" class="calc" name="radio9" value="2">B</td>
                            <td>Investment B</td>
                            <td>-5%</td>
                            <td>20%</td>
                          </tr>
                          <tr>
                            <td><input type="radio" class="calc" name="radio9" value="3">C</td>
                            <td>Investment C</td>
                            <td>-10%</td>
                            <td>25%</td>
                          </tr>
						  <tr>
                            <td><input type="radio" class="calc" name="radio9" value="4">D</td>
                            <td>Investment D</td>
                            <td>-14%</td>
                            <td>30%</td>
                          </tr>
                           <tr>
                            <td><input type="radio" class="calc" name="radio9" value="5">E</td>
                            <td>Investment E</td>
                            <td>-18%</td>
                            <td>35%</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
						 
                 </div> 
				
                </div>
				    </div>
					        
                </div>
				    </div>
					
					<button class="btn btn-info" onclick="calcscore()"id="submit"type="submit">Submit</button>
		
		
</div>

	</form>
</div>
	
	<script>
	function calcscore() {
    var score = 0;
    $(".calc:checked").each(function() {
	
        score = score + parseInt($(this).val(), 10);
		$("input[name=sum]").val(score)
		});
		if(score >= "9" && score <= "14")
	{
	
//	document.write("your score is" + score + "Points You may be a Conservative  Investor");
            myFunction1()
	}
	else if(score >= "15" && score <= "21")
	{
	
	 //document.write("your score is" + score + "Points You may be a Moderate Investor");
            myFunction2()
	} 
	else if(score >= "22" && score <= "27")
	{
	
	  //document.write("your score is" + score + "Points You may be a Aggressive  Investor");
           myFunction3()
	} 
	
	function myFunction1() {
    var txt;
    if (confirm("Yu are a Conservative Investor. You are an investor who is prepared to accept lower returns with lower levels of risk in order to preserve your capital. The negative effects of taxation and inflation will not be of concern to you, provided your initial investment is protected. As a conservative investor, you might expect your portfolio to be allocated approximately 15% in growth assets, with the remainder in defensive assets and an allocation to gold."))
    {
        setTimeout("location.href = 'Conservative.html';", 3000);
            //Redirect1()
    } 
    else 
    {
        txt = "You pressed Cancel!";
    }
    
	}

	function Redirect1() {

              setTimeout("location.href = 'Conservative.html';", 3000);
            }

function myFunction2() {
    var txt;
    if (confirm("You are a Moderate Investor.You are an investor who would like to invest in both income and growth assets. You will be<br> comfortable with calculated risks to achieve good returns, however, you require an <br>investment strategy that adequately deals with the effects of inflation and tax. As a<br> moderate investor, you might expect your portfolio to be allocated approximately<br> 45% in growth assets, with the remainder in defensive assets and an allocation to gold."))
    {
        
            Redirect2()
    } 
    else 
    {
        txt = "You pressed Cancel!";
    }
    
	}
			
    function Redirect2() {
                setTimeout("location.href = 'Moderateresult.html';", 3000);
            }
            
            
            function myFunction3() {
    var txt;
    if (confirm("You are a Aggressive investor. You are an investor who is comfortable with a high volatility and high level of risk in order to achieve higher returns over long term. Your objective is to accumulate assets.long term by primarily investing in growth assets. As an aggressive investor, you might expect your portfolio to be allocated up to 75% in growth assets and an allocation to gold."))
    {
        
            Redirect3()
    } 
    else 
    {
        txt = "You pressed Cancel!";
    }
    
	}
	function Redirect3() {
               setTimeout("location.href = 'Aggressiveresult.html';", 3000);
            }
		
	
}


	</script>
	            
	                    </div>
            </div>
        </div>
    </div>
</div>
<div style="height:50px"></div>
	            
