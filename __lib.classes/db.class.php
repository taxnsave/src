<?php
class Db {
	
	function connect(){
		global $CONFIG;
		$mySqlLink = mysql_connect($CONFIG->dbHost,$CONFIG->dbUser,$CONFIG->dbPassword) or die(mysql_error());
		@mysql_select_db($CONFIG->dbName) or die(mysql_error());
		return $mySqlLink;
	}

	function db_run_query($query) {
		global $CONFIG;
		$temp = mysql_query(str_replace("information_schema","",str_replace("mysql","",($query))),$CONFIG->dbLink) ;
		//or die("<font size=2 color=red>You have an error(<font color=black>". mysql_error() . "</font>) in executing query : " . $query . " in file " . __FILE__ . " at line " . __LINE__ . "</font>");
		return $temp;
	//mysql_real_escape_string
		//return mysql_query($query,$CONFIG->dbLink);
	}
	function db_run_query_bene($query) {
	    global $CONFIG;
	    $temp = mysql_query($query,$CONFIG->dbLink);
	    return $temp;
	}
	function db_fetch_array($result) {
		return mysql_fetch_array($result);
	}
	
	function db_fetch_row($result) {
		return mysql_fetch_row($result);
	}
	
	function db_fetch_object($result) {
		return mysql_fetch_object($result);
	}
	function db_fetch_object_item($result, $num) {
	    while($num > 0) {
	        //echo $num;
	        mysql_fetch_object($result);
	        $num = $num - 1;
	    }
	    return mysql_fetch_object($result);
	}
	function db_num_rows($result) {
		return mysql_num_rows($result);
	}
	
	function db_affected_rows() {
		return mysql_affected_rows();
	}
	function db_insert_id() {
		return mysql_insert_id();
	}
	function db_num_fields($result) {
		return mysql_num_fields($result);
	}
	
	function db_field_name($query, $fieldno) {
	return mysql_field_name($query, $fieldno);
	}
	
	function db_fetch_assoc($result) {
		return mysql_fetch_assoc($result);
	}

	function db_close() {
		return mysql_close();
	}

}
?>