<?php
class documentFiles{

	function documentFiles()
	{
		global $CONFIG,$commonFunction,$customerLog;
		global $db;
		$this->db = $db;
		$this->dbName	 = $CONFIG->dbName;	
		$this->commonFunction	 = $commonFunction;	
		$this->CONFIG	 = $CONFIG;	
		$this->customerLog	 = $customerLog;	
		$this->documentFiles = array();
	}
	
	function addDocument($filePath,$fileName,$totalPages,$filesize,$userId='',$customerId='',$filetype='pdf')
	{		
		global $_SESSION;
		if($this->CONFIG->loggedUserId == '')
		{
			$SQL 		= "INSERT INTO bfsi_uploads SET
							file_name = '".$fileName."',
							file_size = '".$filesize."',
							file_type = 'pdf',							
							file_total_pages = '".$totalPages."',
							file_location = '".$this->CONFIG->uploadLinkPoint."'";	
			$docRes	 = $this->db->db_run_query($SQL);
			$docID	 = $this->db->db_insert_id();	
			$_SESSION[$this->CONFIG->sessionPrefix."new_user_upload"] = $docID;	
		}
		else
		{
			$getRows = $this->commonFunction->getSingleRow("SELECT * FROM bfsi_uploads WHERE fr_user_id = '".$this->CONFIG->loggedUserId."' AND 
					asses_year = '".$_SESSION[$this->CONFIG->sessionPrefix.'_AY_TEXT']."'");
			
			$docID	 = $getRows[pk_upload_id];			
			//echo  $filePath,$fileName,$totalPages,$filesize;
			$SQL 		= "UPDATE bfsi_uploads SET
							file_name = '".$fileName."',
							file_size = '".$filesize."',
							file_type = 'pdf',
							fr_customer_id='".$this->CONFIG->customerId."',
							file_total_pages = '".$totalPages."',
							file_location = '".$this->CONFIG->uploadLinkPoint."'
							 WHERE pk_upload_id = '".$docID."'";	
			$docRes	= $this->db->db_run_query($SQL);
		}
		
		$fileText = $this->getFileText($filePath);
		$SQL = "UPDATE bfsi_uploads SET document_text = '".$fileText."' WHERE pk_upload_id  = '".$docID."'";						
		$docRes	= $this->db->db_run_query($SQL);   
		if($this->CONFIG->loggedUserId == '')
		{
			$_SESSION[$this->CONFIG->sessionPrefix."new_user_upload"] = $docID;
		}
		else
		{
			$this->customerLog->activityDocUpload($this->CONFIG->loggedUserId,$this->CONFIG->customerId,$this->CONFIG->loggedUserEmail,$custom_name,$this->CONFIG->loggedIP);
		}
		
		$formDocID	 = $getRows[fr_itr_id];	
				
		if($this->CONFIG->loggedUserId == '')
			return $docID;
		else
		{
			$this->txtToForm16DB($fileText,$formDocID);
			return $formDocID;
		}
	}
	function txtToForm16DB($getFileText,$geFormtDocID)
	{		
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			$newLine="\r\n";;
		} else {
			$newLine="\n";
		}	
		$a = str_replace("(","",str_replace(")","",str_replace(" ","_",str_replace($newLine,"#",trim(strtolower($getFileText))))));
		$a1a = str_replace("(","",str_replace(")","",str_replace(" ","_",str_replace("\n","#",trim(strtolower($getFileText))))));
		$b = str_replace($newLine,"#",trim(strtolower($getFileText)));
		$arr = explode($newLine,$getFileText);
		
		// FORM 16 PART A				print_r($field1[1]);echo "<br>";echo "2.  ";
		preg_match('/#name_and_address_of_the_deductor#(.*?)#name_and_address_of_the_deductee#/', $a, $field1);
		if(count($field1) == 0)
			preg_match('/#name_and_address_of_the_employer#(.*?)#name_and_address_of_the_employee#/', $a, $field1);
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET employer_address = '".$field1[1]."' WHERE pk_form_id  = '".$geFormtDocID."'"); 

		preg_match('/#name_and_address_of_the_deductee#(.*?)#pan_of_the_deductor#/', $a, $field2);
		if(count($field2) == 0) 
			preg_match('/#name_and_address_of_the_employee#(.*?)#pan_of_the_deductor#/', $a, $field2);
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET employee_address = '".$field2[1]."' WHERE pk_form_id  = '".$geFormtDocID."'"); 
		
		preg_match('/#pan_of_the_deductor#(.*?)#tan_of_the_deductor#/', $a, $field3);
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET deductor_pan = '".$field3[1]."' WHERE pk_form_id  = '".$geFormtDocID."'"); 
		
		preg_match('/#tan_of_the_deductor#(.*?)#pan_of_the_deductee#/', $a, $field4);
		if(count($field4) == 0)
			preg_match('/#tan_of_the_deductor#(.*?)#pan_of_the_employee#/', $a, $field4);
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET deductor_tan = '".$field4[1]."' WHERE pk_form_id  = '".$geFormtDocID."'"); 
		
		preg_match('/#pan_of_the_deductee#(.*?)#assessment_year#/', $a, $field5);
		if(count($field5) == 0)
			preg_match('/#pan_of_the_employee#(.*?)#assessment_year#/', $a, $field5);
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET employee_pan = '".$field5[1]."' WHERE pk_form_id  = '".$geFormtDocID."'"); 
		
		preg_match('/#assessment_year#(.*?)#cit_tds#/', $a, $field6);
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET assessment_year = '".$field6[1]."' WHERE pk_form_id  = '".$geFormtDocID."'"); 
		
		preg_match('/#period#(.*?)#summary_of_payment#/', $a, $field7);
		if(count($field7) == 0)
			preg_match('/#period_with_the_employer#(.*?)#summary_of_amount_paid(.*?)#/', $a, $field7);
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET period_with_employer = '".$field7[1]."' WHERE pk_form_id  = '".$geFormtDocID."'"); 
		
		preg_match('/#total_rs(.*?)#summary_of_tax_deducted_at_source_in_respect_of_deductee#/', $a, $field8);
		if(count($field8) == 0)
			preg_match('/#c_total_tax_paid(.*?)#/', $a, $field8);
		if(count($field8) == 0)
			preg_match('/#total_rs(.*?)#legend_used_in_form_16#/', $a, $field8);
		if(count($field8) == 0)
			preg_match('/that_a_sum_of_rs._#(.*?)#_/', $a, $field8);
		
		if(strlen($field8[1]) > 12)
		{			
			//preg_match('/#_in_words]_has_been_deducted_and_a_sum_of_rs._#(.*?)#_[rs._#/', $a1a, $field8);				
			//preg_match('/#total_rs(.*?)#legend_used_in_form_16/', $field8[0], $field88);			
			//print_r($field88);print_r($field8);
			$arr123 = explode(".",$field8[1]);
			$totTDS = number_format($arr123[1],2);
			//echo "UPDATE bfsi_form_16_a SET total_tds_deposited = '".ltrim($totTDS,".")."' WHERE pk_form_id  = '".$geFormtDocID."'";
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET total_tds_deposited = '".ltrim($totTDS,".")."' WHERE pk_form_id  = '".$geFormtDocID."'"); 
		}
		else
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET total_tds_deposited = '".preg_replace("/[^0-9,.]/", "", ltrim($field8[1],"."))."' WHERE pk_form_id  = '".$geFormtDocID."'"); 
		//var_dump($field8);
		//exit;
		// "<br>===================== PART B ========================<br>9. ";
		
		preg_match('/balance_1-2(.*?)#/', $a, $field9);								//var_dump($field9);
		$balance = '';
		if(count($field9) == 0)
		{
			preg_match('/balance_1-2(.*?)._/', $a, $field9);
			$balance = $this->cleanCommaDotInNumbers($field9[1]);					
		}
		if(count($field9) == 0)
		{	
			while(list($key,$val)=each($arr))
			{
				 if(substr_count(strtolower($val), 'balance') != 0)
				 {
					$before = str_replace("_","",str_replace("rs","",str_replace(" ","_",strtolower(trim($arr[$key-1])))));
					$after  = str_replace("_","",str_replace("rs","",str_replace(" ","_",strtolower(trim($arr[$key+1])))));
					preg_match('/[^0-9,.]\D/', $before,$chkString);
					preg_match('/[^0-9,.]\D/', $after,$chkString1);
					
					if(count($chkString) == 0)
						$balance = str_replace(",","",$before);
					else if(count($chkString1) == 0)
						$balance = str_replace(",","",$after);
						
					$this->db->db_run_query("UPDATE bfsi_form_16_a SET balance_1_2 = '".$balance."' WHERE pk_form_id  = '".$geFormtDocID."'");
					break;
				 }
			}
		}	
		else				
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET balance_1_2 = '".$balance."' WHERE pk_form_id  = '".$geFormtDocID."'");
		
		if($balance == '')
		{
			$a = str_replace("(","",str_replace(")","",str_replace(" ","_",str_replace("\r\n","#",trim(strtolower($getFileText))))));			
			preg_match('/balance_1-2(.*?)._/', $a, $field9);
			$balance = $this->cleanCommaDotInNumbers($field9[1]);	
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET balance_1_2 = '".$balance."' WHERE pk_form_id  = '".$geFormtDocID."'");		
		}	
		
		preg_match('/#aentertainment_allowance_(.*?)#/', $a, $field10);				//var_dump($field9);
		if(count($field10) == 0)
			preg_match('/a_entertainment_allowance_(.*?)b_tax_on_employment/', $a, $field10);
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_enter_allowance = '".$this->cleanCommaDotInNumbers($field10[1])."' WHERE pk_form_id  = '".$geFormtDocID."'");
		
		preg_match('/#btax_on_employment(.*?)#/', $a, $field11);					//var_dump($field9);
		if(count($field11) == 0)
			preg_match('/b_tax_on_employment(.*?)5._aggregate/', $a, $field11);		
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_tax_employment = '".$this->cleanCommaDotInNumbers($field11[1])."' WHERE pk_form_id  = '".$geFormtDocID."'");
		
		$c = str_replace(" ","_",str_replace($newLine,"#",$b));

		preg_match('/9._deduction(.*?)11._total/', $a, $ded_80cdg);				//PF
		preg_match('/9._deduction(.*?)11._total/', $c, $ded_80cdg1);				//PF
		
		//print_r($ded_80cdg);
		//print_r($ded_80cdg1);

		$i=2;
		$d = explode(")",$ded_80cdg1[0]);			//print_r($d);
		$section80cTxt = '';
		$section80c=0;
		$section80ccc=0;
		$section80ccd=0;
		$section80ccg=0;
		$section80g=0;
		$section80d=0;
		if(count($d) > 7)
		{
			$amtCount = substr_count($d[2],"amount");		//echo "<br> ....".$amtCount."<br>";
			if($amtCount == 0)							// Calculate with HASH
			{
				$arr1 = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$d[2]);        // 80c array                                                       
				$section80c=0;
				$section80cStr='';						//print_r($arr1);
				while(list($key,$val)=each($arr1))
				{
					if(strstr($val,"section") === false)
					{
						$arr2 = explode("#",$val);		//print_r($arr2);	//echo "<br>dd".trim($val)." ---- ".$key;
						if($key == 1)
						{
							$amt = $arr2[1];
							$section80c += preg_replace("/[^0-9]/", "", $arr2[1]);	
						}
						else if($key == 2)
						{
							$amt = $arr2[1];	
							$section80c += preg_replace("/[^0-9]/", "", $arr2[1]);	
							$section80c += preg_replace("/[^0-9]/", "", $arr2[2]);	
						}
						else
						{
							//echo  $arr2[1];
							$section80c += preg_replace("/[^0-9]/", "", $arr2[1]);	
						}
					}
				}
				if($section80c > 150000)
					$this->db->db_run_query("UPDATE bfsi_form_16_a SET total_80c = '150000' WHERE pk_form_id  = '".$geFormtDocID."'");
				else
					$this->db->db_run_query("UPDATE bfsi_form_16_a SET total_80c = '".$this->cleanCommaDotInNumbers($section80c)."' WHERE pk_form_id  = '".$geFormtDocID."'");
			}
			else
			{
				while(list($key,$val)=each($d))
				{
				if($key > 2)
				{
					$arr2 = explode("#",$val);
					
					if(count($arr2) >= 2)
					{
						//print_r($arr2);
						if(substr_count($val,"_provident") || substr_count($val,"provident"))
						{
							$arr4 = explode(".",$arr2[0]); //print_r($arr4);
							$pf = preg_replace("/[^0-9,.]/", "", $arr4[count($arr4)-2]);
							$pf = $this->cleanCommaDotInNumbers($pf);
							$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80c_pf = '".$pf."' WHERE pk_form_id  = '".$geFormtDocID."'");
							$section80c = $section80c+$pf;
							$section80cTxt .= "PF - ".number_format(preg_replace("/[^0-9]/", "", $pf),2)."<br>";
						}
						if(substr_count($val,"elssrs") || substr_count($val,"elss"))
						{
							$arr4 = explode(".",$arr2[0]); //print_r($arr4);
							$elss = preg_replace("/[^0-9,.]/", "", $arr4[count($arr4)-2]);
							$elss = $this->cleanCommaDotInNumbers($elss);
							$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80c_elss = '".$elss."' WHERE pk_form_id  = '".$geFormtDocID."'");
							$section80c = $section80c+$elss;
							$section80cTxt .= "ELSS - ".number_format(preg_replace("/[^0-9]/", "", $elss),2)."<br>";
						}
						if(substr_count($val,"80_ccg_") || substr_count($val,"80ccg"))
						{
							$arr4 = explode(".",$val); //print_r($arr4);
							$section80ccg = preg_replace("/[^0-9,.]/", "", $arr4[count($arr4)-2]);
							$section80ccg = $this->cleanCommaDotInNumbers($section80ccg);
							$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80ccg = '".$section80ccg."' WHERE pk_form_id  = '".$geFormtDocID."'");
						}
						else if(substr_count($val,"80_d_") || substr_count($val,"80d"))
						{
							$arr4 = explode(".",$val); //print_r($arr4);
							$section80d = preg_replace("/[^0-9,.]/", "", $arr4[count($arr4)-2]);
							$section80d = $this->cleanCommaDotInNumbers($section80d);
							$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80d = '".$section80d."' WHERE pk_form_id  = '".$geFormtDocID."'");
						}
						else if(substr_count($val,"80_dd") || substr_count($val,"80dd"))
						{
							$arr4 = explode(".",$val); //print_r($arr4);
							$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80dd = '".preg_replace("/[^0-9,.]/", "", $arr4[count($arr4)-2])."' WHERE pk_form_id  = '".$geFormtDocID."'");
						}
						else if(substr_count($val,"80_ddb") || substr_count($val,"80ddb"))
						{
							$arr4 = explode(".",$val); //print_r($arr4);
							$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80ddb = '".preg_replace("/[^0-9,.]/", "", $arr4[count($arr4)-2])."' WHERE pk_form_id  = '".$geFormtDocID."'");
						}
						else if(substr_count($val,"80_e") || substr_count($val,"80e"))
						{
							$arr4 = explode(".",$val); //print_r($arr4);
							$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80e = '".preg_replace("/[^0-9,.]/", "", $arr4[count($arr4)-2])."' WHERE pk_form_id  = '".$geFormtDocID."'");
						}
						else if(substr_count($val,"80_u") || substr_count($val,"80u"))
						{
							$arr4 = explode(".",$val); //print_r($arr4);
							$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80u = '".preg_replace("/[^0-9,.]/", "", $arr4[count($arr4)-2])."' WHERE pk_form_id  = '".$geFormtDocID."'");
						}
						else if(substr_count($val,"80_tta") || substr_count($val,"80tta"))
						{
							$arr4 = explode(".",$val); //print_r($arr4);
							$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80tta = '".preg_replace("/[^0-9,.]/", "", $arr4[count($arr4)-2])."' WHERE pk_form_id  = '".$geFormtDocID."'");
						}
						else if(substr_count($val,"80_ee") || substr_count($val,"80ee"))
						{
							$arr4 = explode(".",$val); //print_r($arr4);
							$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80ee = '".preg_replace("/[^0-9,.]/", "", $arr4[count($arr4)-2])."' WHERE pk_form_id  = '".$geFormtDocID."'");
						}
						else if(substr_count($val,"80_gg") || substr_count($val,"80gg"))
						{
							$arr4 = explode(".",$val); //print_r($arr4);
							$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80gg = '".preg_replace("/[^0-9,.]/", "", $arr4[count($arr4)-2])."' WHERE pk_form_id  = '".$geFormtDocID."'");
						}
						else
						{
							$removeChar = preg_replace("/[^0-9,.]/", "", $val);	
							if(substr_count($removeChar,substr($removeChar,0,3)) == $amtCount)
							{
								$valArr = explode(substr($removeChar,0,3),$val);
								$finalVal = preg_replace("/[^0-9.]/", "",substr($removeChar,0,3).$valArr[count($valArr)-1]);
								$section80cTxt .= str_replace("_"," ",ucwords($valArr[0]))." - ".number_format($finalVal,2)."<br>";
								$section80c = $section80c+$finalVal;
							}	
						}
					}
				}
			}
			}
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET total_80c = '".$this->cleanCommaDotInNumbers($section80c)."' WHERE pk_form_id  = '".$geFormtDocID."'");
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET total_80c_txt = '".$section80cTxt."' WHERE pk_form_id  = '".$geFormtDocID."'");
		}
		else
		{
			$amtCount = substr_count($d[1],"amount");		//echo "<br> ....".$amtCount."<br>";
			if($amtCount == 0)							
			{
				$amtCount = substr_count($d[2],"amount");		//echo "<br> ....".$amtCount."<br>";
				if($amtCount != 0)	$i++;	
			}
			if($amtCount == 0)							// Calculate with HASH
			{
				$arr1 = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$d[2]);        // 80c array                                                       
				$section80c=0;
				$section80cStr='';						//print_r($arr1);
				while(list($key,$val)=each($arr1))
				{
					if(strstr($val,"section") === false)
					{
						$arr2 = explode("#",$val);		//print_r($arr2);	//echo "<br>dd".trim($val)." ---- ".$key;
						if($key == 1)
						{
							$amt = $arr2[1];
							$section80c += preg_replace("/[^0-9]/", "", $arr2[1]);	
						}
						else if($key == 2)
						{
							$amt = $arr2[1];	
							$section80c += preg_replace("/[^0-9]/", "", $arr2[1]);	
							$section80c += preg_replace("/[^0-9]/", "", $arr2[2]);	
						}
						else
						{
							//echo  $arr2[1];
							$section80c += preg_replace("/[^0-9]/", "", $arr2[1]);	
						}
					}
				}
				if($section80c > 150000)
					$this->db->db_run_query("UPDATE bfsi_form_16_a SET total_80c = '150000' WHERE pk_form_id  = '".$geFormtDocID."'");
				else
					$this->db->db_run_query("UPDATE bfsi_form_16_a SET total_80c = '".$this->cleanCommaDotInNumbers($section80c)."' WHERE pk_form_id  = '".$geFormtDocID."'");
			}
			else
			{
				$arr1 = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$d[$i]);     // print_r($d);  // 80c array                                                       
				$section80c=0;
				while(list($key,$val)=each($arr1))
				{
					$removeChar = preg_replace("/[^0-9,.]/", "", $val);	
					if(substr_count($removeChar,substr($removeChar,0,3)) == $amtCount)
					{
						$valArr = explode(substr($removeChar,0,3),$val);
						$finalVal = preg_replace("/[^0-9.]/", "",substr($removeChar,0,3).$valArr[count($valArr)-1]);
						$section80cTxt .= str_replace("_"," ",ucwords($valArr[0]))." - ".number_format($finalVal,2)."<br>";
						$section80c = $section80c+$finalVal;
					}	
					else
					{
						$val = str_replace("section","",$val);
						if(strlen($val) > 4)
						{
							$valArr = explode(".",$val);		//print_r($valArr);
							if(count($valArr) > 2)
							{
								$section80cTxt .= str_replace("_"," ",ucwords(preg_replace("/[^a-z_]/", "", $valArr[0]))).
													" - ".number_format($valArr[count($valArr)-2],2)."<br>";
								$section80c = $section80c + str_replace(",","",$valArr[count($valArr)-2]);
							}
						}
					}
				}
				$this->db->db_run_query("UPDATE bfsi_form_16_a SET total_80c = '".$this->cleanCommaDotInNumbers($section80c)."' WHERE pk_form_id  = '".$geFormtDocID."'");
				$this->db->db_run_query("UPDATE bfsi_form_16_a SET total_80c_txt = '".$section80cTxt."' WHERE pk_form_id  = '".$geFormtDocID."'");
			}
		
			$chkElement = substr_count($d[3],"#");
			$section80ccc=0;
			if($chkElement > 1)
			{
				$arr1 = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$d[3]);        //print_r($arr1);// 80c array                                                       
				$section80ccc=0;
				$section80cStr='';
				while(list($key,$val)=each($arr1))
				{
					if(strstr($val,"section") === false)
					{
						$arr2 = explode("#",$val);
						if($key == 1)
						{
							$amt = $arr2[1];
							$section80ccc += preg_replace("/[^0-9]/", "", $arr2[1]);	
						}
						else if($key == 2)
						{
							$amt = $arr2[1];	
							$section80ccc += preg_replace("/[^0-9]/", "", $arr2[1]);	
							$section80ccc += preg_replace("/[^0-9]/", "", $arr2[2]);	
						}
						else
						{
							$section80ccc += preg_replace("/[^0-9]/", "", $arr2[1]);	
						}
					}
				}
				
				$section80ccc = preg_replace("/[^0-9]/", "", $section80ccc);
			}
			else
				$section80ccc=0;	
			
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80ccc = '".$this->cleanCommaDotInNumbers($section80ccc)."' WHERE pk_form_id  = '".$geFormtDocID."'");
				
				
			$chkElement = substr_count($d[4],"#");
			$section80ccd=0;
			if($chkElement > 1)
			{
				preg_match('/_(.*?)#/', $d[4],$e); 		//var_dump($e);
				$arr1 = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$e[1]);       //		print_r($e); // 80ccd array                                                       
				$section80ccd=0;
				$section80cStr='';
				while(list($key,$val)=each($arr1))
				{
					if(strstr($val,"section") === false)
					{
						$arr2 = explode("#",$val);
						if($key == 1)
						{
							$amt = $arr2[1];
							$section80ccd += preg_replace("/[^0-9]/", "", $arr2[1]);	
						}
						else if($key == 2)
						{
							$amt = $arr2[1];	
							$section80ccd += preg_replace("/[^0-9]/", "", $arr2[1]);	
							$section80ccd += preg_replace("/[^0-9]/", "", $arr2[2]);	
						}
						else
						{
							$section80ccd += preg_replace("/[^0-9]/", "", $arr2[1]);	
						}
					}
				}		
				$section80ccd = preg_replace("/[^0-9]/", "", $section80ccd);
			}
			else
				$section80ccd=0;	
			
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80ccd = '".$this->cleanCommaDotInNumbers($section80ccd)."' WHERE pk_form_id  = '".$geFormtDocID."'");
			
			$chkElement = substr_count($d[6],"#");
			$section80g=0;
			if($chkElement > 1)
			{
				$arr1 = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$d[6]);       		//print_r($e); // 80ccd array                                                       
				$section80g=0;
				$section80cStr='';
				while(list($key,$val)=each($arr1))
				{
					if(strstr($val,"under") === false)
					{
						$arr2 = explode("#",$val);	//print_r($arr2);
						if($key == 1)
						{
							$amt = $arr2[1];
							$section80g += preg_replace("/[^0-9]/", "", $arr2[1]);	
						}
						else if($key == 2)
						{
							$amt = $arr2[1];	
							$section80g += preg_replace("/[^0-9]/", "", $arr2[1]);	
							$section80g += preg_replace("/[^0-9]/", "", $arr2[2]);	
						}
						else
						{
							$section80g += preg_replace("/[^0-9]/", "", $arr2[1]);	
						}
					}
				}		
				$section80g = preg_replace("/[^0-9]/", "", $section80g);
			}
			else
			{
				$section80g=0;		
				preg_match('/_under_chapter_via_(.*?)_aggregate_of/', $d[6], $arr1);
				if(substr_count($arr1[1],"section_80d"))
				{			
					//print_r($arr1);	
					$chkAmount = substr_count($arr1[1],"amount");
					$arr2 = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$arr1[1]);     //print_r($arr2);	// print_r($d);  // 80c arra
					$section80d = preg_replace("/[^0-9.]/", "", $arr2[count($arr2)-1]);                                       
					$section80dArr = explode(".",$section80d);					//print_r($section80dArr);
					$section80d=$section80dArr[count($section80dArr)-$chkAmount];
					$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80d = '".$this->cleanCommaDotInNumbers($section80d)."' WHERE pk_form_id  = '".$geFormtDocID."'");
				}
			}					
			
			if($section80g <= 320)
				$section80g = 0;
			else
				$section80g=$section80g;
			
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80g = '".$this->cleanCommaDotInNumbers($section80g)."' WHERE pk_form_id  = '".$geFormtDocID."'");
		}	
		
		
		//////////////////////////////////////  If above code will not scrap value ///////////////////////////////////////////////////
		
		
		preg_match('/ii_lic(.*?)#/', $a, $field13);				//LIC
		if(count($field13) == 0)
		{			
			preg_match('/lic(.*?)b/', $b, $field13);
		}
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80c_lic = '".$this->cleanCommaDotInNumbers($field13[1])."' WHERE pk_form_id  = '".$geFormtDocID."'");
		
		preg_match('/#iii_total_of_80_c_qualifying:(.*?)#/', $a, $field14);				//80C Qualifying
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80c_qualify = '".$this->cleanCommaDotInNumbers($field14[1])."' WHERE pk_form_id  = '".$geFormtDocID."'");
		
		if($section80ccc == '')
		{
			preg_match('/#b_sections_80ccc_(.*?)#/', $a, $field15);				//80Ccc 
			if(count($field15) == 0)
			{
				preg_match('/section 80ccc(.*?)section 80ccd/', $b, $field15);		
			}
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80ccc = '".$this->cleanCommaDotInNumbers($field15[1])."' WHERE pk_form_id  = '".$geFormtDocID."'");
		}
		if($section80ccd == '')
		{		
			preg_match('/#c_sections_80ccd(.*?)#/', $a, $field16);				//80Ccd 
			if(count($field16) == 0)
			{
				preg_match('/section 80ccd(.*?)note/', $b, $field16);		
			}		
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80ccd = '".$this->cleanCommaDotInNumbers($field16[1])."' WHERE pk_form_id  = '".$geFormtDocID."'");
		}
		if($section80d == '')
		{		
			preg_match('/#isection_80_d_-_mediclaim_policy(.*?)#/', $a, $field17);				//Mediclaim 
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80d = '".$this->cleanCommaDotInNumbers($field17[1])."' WHERE pk_form_id  = '".$geFormtDocID."'");
		}	
		if($section80g == '')
		{			
			preg_match('/#iisection__(.*?)#/', $a, $field18);								//80 g donation 
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80g = '".$this->cleanCommaDotInNumbers($field18[1])."' WHERE pk_form_id  = '".$geFormtDocID."'");
		}	
		if($section80ccg == '')
		{	
			preg_match('/#iiisection_80ccg1_rgessrs_(.*?)#/', $a, $field19);				//RGESS
			$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_80ccg = '".$this->cleanCommaDotInNumbers($field19[1])."' WHERE pk_form_id  = '".$geFormtDocID."'");
		}		
		
		preg_match('/#ivsection_total_of_chapter_vi_a_qualifying(.*?)#/', $a, $field20);				//80 g donation 
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_sec_iva = '".$this->cleanCommaDotInNumbers($field20[1])."' WHERE pk_form_id  = '".$geFormtDocID."'");
		
		preg_match('/#10.aggregate_of_deductible_amount_under_chapter_via_&_80c(.*?)#/', $a, $field21);				//80 g donation 
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET ded_via_80c = '".$this->cleanCommaDotInNumbers($field21[1])."' WHERE pk_form_id  = '".$geFormtDocID."'");
		
		//$this->trfrFrm16DataToMainDB($geFormtDocID);
		return;
	}
	function trfrFrm16DataToMainDB($getRequest)
	{
		$getITRID = $getRequest[formsDataID];
		$salary_id = $getRequest[salary_id];
		
		$getSource = $this->commonFunction->getSingleRow("SELECT * FROM bfsi_form_16_a WHERE fr_user_id = '".$this->CONFIG->loggedUserId."' AND 
					fr_itr_id  = '".$getITRID."'");
		
		$employerAddress = 	explode("#",$getSource[employer_address]);	
		$arrBuild = explode(",",$employerAddress[2]);
		$arrCityZip = explode("-",$arrBuild[1]);	
		
		 $SQL 	   = "UPDATE itr_sou_salary SET
						sou_sa_ntslary 				= '".$getSource[balance_1_2]."',
						sou_sa_employer_name 		= '".ucwords(str_replace("_"," ",$employerAddress[0]))."',
						sou_sa_employer_add			= '".ucwords(str_replace("_"," ",$employerAddress[1]))." ".ucwords(str_replace("_"," ",$arrBuild[0]))."',
						sou_sa_employercountry		= 'INDIA',
						sou_sa_employer_city 		= '".trim(ucwords(str_replace("_"," ",$arrCityZip[0])))."',
						sou_sa_employer_state 		= '".ucwords(str_replace("_"," ",$employerAddress[3]))."',
						sou_sa_employer_pincode 	= '".trim(ucwords(str_replace("_"," ",$arrCityZip[1])))."',
						sou_sa_tan_no 				= '".strtoupper($getSource[deductor_tan])."',
						sou_sa_tds_on_sal 			= '".$getSource[total_tds_deposited]."' 
					 WHERE fr_itr_id = '".$getITRID."' AND fr_user_id = '".$this->CONFIG->loggedUserId."' AND pk_sousal_id = '".$salary_id."'";
		
		$SQL1 	   = "INSERT INTO itr_sou_salary SET
						sou_sa_ntslary 				= '".$getSource[balance_1_2]."',
						sou_sa_employer_name 		= '".ucwords(str_replace("_"," ",$employerAddress[0]))."',
						sou_sa_employer_add			= '".ucwords(str_replace("_"," ",$employerAddress[1]))." ".ucwords(str_replace("_"," ",$arrBuild[0]))."',
						sou_sa_employercountry		= 'INDIA',
						sou_sa_employer_city 		= '".trim(ucwords(str_replace("_"," ",$arrCityZip[0])))."',
						sou_sa_employer_state 		= '".ucwords(str_replace("_"," ",$employerAddress[3]))."',
						sou_sa_employer_pincode 	= '".trim(ucwords(str_replace("_"," ",$arrCityZip[1])))."',
						sou_sa_tan_no 				= '".strtoupper($getSource[deductor_tan])."',
						sou_sa_tds_on_sal 			= '".$getSource[total_tds_deposited]."', 
					    fr_itr_id 					= '".$getITRID."',
						fr_user_id 					= '".$this->CONFIG->loggedUserId."'";
					 
		$docRes	= $this->db->db_run_query($SQL);
		//exit;
		$SQL 	   = "UPDATE itr_deduction SET
						ded_gd__80c 		= '".trim($getSource[total_80c])."',
						ded_gd__80gg 		= '".trim($getSource[ded_80gg])."',
						ded_gd__80tta		= '".trim($getSource[ded_80tta])."',
						ded_hi_hip80d_ssc	= '".trim($getSource[ded_80tta])."',
						ded_othd_80ddb 		= '".trim($getSource[ded_80tta])."',
						ded_othd_80e 		= '".trim($getSource[ded_80e])."',
						ded_othd_80ee 		= '".trim($getSource[ded_80ee])."',
						ded_othd_80ccc 		= '".trim($getSource[ded_80ccc])."',
						ded_othd_80ccd1 	= '".trim($getSource[ded_80ccd])."' 
					 WHERE fr_itr_id = '".$getITRID."' AND fr_user_id = '".$this->CONFIG->loggedUserId."'";
		
		$docRes	= $this->db->db_run_query($SQL);
		return;
		//exit;
	}
	function cleanCommaDotInNumbers($getNumeric)
	{
		$withDot = preg_replace("/[^0-9.]/", "", $getNumeric);
		$formatStr = number_format($withDot,2);
		$arrWith = explode(".",$formatStr);
		return preg_replace("/[^0-9]/", "", $arrWith[0]);			//print_r($balance);
	}
	function getFileText($fileName)
	{
		if($this->CONFIG->loggedUserId == '')
			$fileName = str_replace("/\\","",$fileName);
		else
			$fileName = str_replace("/\\","/",$fileName);	
		
		ini_set('pcre.backtrack_limit',100000000000);
		ini_set('pcre.recursion_limit',100000000000); 
	
		include ( $this->CONFIG->wwwroot.'__lib.apis/__PdfToText/PdfToText.phpclass' ) ;
		
		$pdf 		  = new PdfToText ( $fileName ) ;
		$pdfText 	  = $pdf -> Text ;
		return trim(str_replace("'","",str_replace("\"","",$pdfText)));
	}
	function getForm26ASText($getFilename,$getPassword='')
	{
		$retRes = array();
		$javaTool = $this->CONFIG->wwwroot.'__lib.apis/__PdfToText/apBox/';
		$getFilename = $this->CONFIG->customerFilePath.$getFilename;
		//echo 'java -jar "'.$javaTool.'pdfbox-app-2.0.8.jar" ExtractText -console -password '.$getPassword.' "'.$getFilename.'"';
		if($getPassword == '')
			$output = shell_exec('java -jar "'.$javaTool.'pdfbox-app-2.0.8.jar" ExtractText -console "'.$getFilename.'"');
		else
			$output = shell_exec('java -jar "'.$javaTool.'pdfbox-app-2.0.8.jar" ExtractText -console -password '.$getPassword.' "'.$getFilename.'"');
		
		//echo 'java -jar "'.$javaTool.'pdfbox-app-2.0.8.jar" ExtractText -console -password '.$getPassword.' "'.$getFilename.'"';
			
		if($output == '')
			$retRes = array("need_password");
		else
		{
			$output = str_replace("(","",str_replace(")","",str_replace(" ","_",str_replace("\n","#",trim(strtolower($output))))));
			preg_match('/total_tax_deducted#(.*?)sr/', $output, $field1);
			$tot_tds = explode(".00",$field1[1]);
			//print_r($tot_tds);
			//echo "Total TDS : ".number_format($tot_tds[1],2)."<br>";			
			preg_match('/financial_year_assessment_year(.*?)#/', $output, $field2);
			$arr = explode("_",$field2[1]);
			//echo "PAN : ".strtoupper($field2[1])."<br>";
			preg_match('/##_tds_deposited#(.*?)#total_tax_deducted##/', $output, $field3);
			$field3Arr = explode("_",$field3[1]);			
			$clientTAN = strtoupper($field3Arr[count($field3Arr)-2]);
			$totCredited = $field3Arr[count($field3Arr)-1];
			$clientNameArr = array_splice($field3Arr, -2);			
			$clientName = '';
			while(list($key,$val) = each($field3Arr))
			{
				$clientName .= ucwords($val)." ";
			}
			$retRes = array("TEXT" => $output, "TAN" => $clientTAN, "CREDITED" => $totCredited, "CLIENT_NAME" => $clientName, "TDS" => number_format($tot_tds[1],2),
								 "PAN" => strtoupper($arr[0]), "FIN_YEAR" => $arr[1], "ASSESS_YEAR" => $arr[2]);
		}
		
		return $retRes;
	}
	function addForm26AS($getFormFilename,$getFormID,$getFormPDFPass)
	{		
		$form26Txt = $this->getForm26ASText($getFormFilename,$getFormPDFPass);
		//print_r($form26Txt);
		
		$SQL 		= "INSERT INTO bfsi_form_26_as SET
							fr_itr_id = '".$getFormID."',
							form_file_name = '".$getFormFilename."',
							client_name = '".$form26Txt['CLIENT_NAME']."',
							tot_credited = '".$form26Txt['CREDITED']."',
							client_tan = '".$form26Txt['TAN']."',
							form_file_txt = '".str_replace("'","",$form26Txt['TEXT'])."',
							assess_year = '".$form26Txt['ASSESS_YEAR']."',
							pan_no = '".$form26Txt['PAN']."',
							total_tds = '".$form26Txt['TDS']."',
							fr_user_id  = '".$this->CONFIG->loggedUserId."'";	
		$docRes	= $this->db->db_run_query($SQL);
     	$form26ASID = $this->db->db_insert_id();
		$this->db->db_run_query("UPDATE bfsi_form_16_a SET form_26_total_tds = '".$form26Txt['TDS']."',
								form_26_pan = '".$form26Txt['PAN']."',form_26_assess = '".$form26Txt['ASSESS_YEAR']."' WHERE pk_form_id  = '".$getFormID."'");
		
		$SQL 		= "INSERT INTO itr_taxreconciliation SET
							fr_itr_id = '".$getFormID."',							
							reco_reconci_nameodeduc  = '".$form26Txt['CLIENT_NAME']."',
							reco_reconci_totamtcre  = '".$form26Txt['CREDITED']."',
							reco_reconci_tanodeduc  = '".$form26Txt['TAN']."',						
							reco_reconci_tottdsdop  = '".$form26Txt['TDS']."',
							fr_user_id  = '".$this->CONFIG->loggedUserId."'";	
		$docRes	= $this->db->db_run_query($SQL);
		
		//exit;
		return;
	}	
	function getFormData($getFormID)
	{
		$SQL="SELECT * FROM bfsi_form_16_a WHERE pk_form_id = '".$getFormID."' AND fr_user_id  = '".$this->CONFIG->loggedUserId."'";			
		return $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
	}
	function getFormIdOfFile($getFileID)
	{
		$SQL="SELECT pk_form_id,assessment_year FROM bfsi_form_16_a WHERE fr_upload_doc_id = '".$getFileID."' AND fr_user_id  = '".$this->CONFIG->loggedUserId."'";			
		return $this->commonFunction->getSingleRow($SQL);
	}
	function latestUploadedFiles($customerID)
	{
		$SQL="SELECT * FROM bfsi_uploads WHERE fr_user_id  = '".$this->CONFIG->loggedUserId."' ORDER BY pk_upload_id  DESC LIMIT 12";	
		return $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
	}
	function getFileDetail($getFileID,$customerID)
	{
		$SQL="SELECT * FROM bfsi_uploads WHERE fr_customer_id = '".$getFileID."' AND customer_id  = '".$customerID."'";	
		return $this->commonFunction->getSingleRow($SQL);
	}
	function latestUploadedFilesCount($customerID)
	{
		$SQL="SELECT * FROM bfsi_uploads WHERE fr_customer_id  = '".$customerID."' ORDER BY pk_upload_id  DESC LIMIT 5";	
		$docRes	= $this->db->db_run_query($SQL);
		return $this->db->db_num_rows($docRes);
	}	
	function allUploadedFilesCount($customerID)
	{
		$SQL="SELECT * FROM bfsi_uploads WHERE fr_customer_id  = '".$customerID."'";	
		$docRes	= $this->db->db_run_query($SQL);
		return $this->db->db_num_rows($docRes);
	}		
	function getAllFiles($customerID,$folderID='')
	{
		$SQL="SELECT * FROM bfsi_uploads WHERE fr_customer_id  = '".$customerID."' ORDER BY pk_upload_id DESC";	
		
		return $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
	}
}

?>