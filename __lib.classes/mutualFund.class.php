<?php
class mutualFund{

	function mutualFund()
	{
		global $CONFIG,$commonFunction,$documentFiles,$customerProfile;
		global $db;
		$this->db					= $db;
		$this->dbName				= $CONFIG->dbName;	
		$this->commonFunction		= $commonFunction;	
		$this->CONFIG				= $CONFIG;	
		$this->documentFiles		= $documentFiles;	
		$this->customerProfile		= $customerProfile;	
		$this->mutualFund			= array();
	}	
	function updatePrice()
	{
		$sqlNavPrice	= "SELECT * FROM amfi_data WHERE is_updated = 0";
		$navPriceResult = $this->db->db_run_query($sqlNavPrice);
		while($val = $this->db->db_fetch_array($navPriceResult))	
		{
			set_time_limit(0);
			if($this->db->db_num_rows($this->db->db_run_query("SELECT * FROM mf_nav_master WHERE ISIN = '".$val[ISIN]."'")) > 0)
			{
				$update_date = date("Y-m-d",strtotime($val[amfi_update_date]));
				if($this->db->db_num_rows($this->db->db_run_query("SELECT * FROM mf_nav_price WHERE ISIN = '".$val[ISIN]."' AND price_date = '".$update_date."'")) == 0)
				{				
					$navMaster = $this->commonFunction->getSingleRow("SELECT * FROM mf_nav_master WHERE ISIN = '".$val[ISIN]."'");
					$this->db->db_run_query("INSERT INTO mf_nav_price SET fr_nav_id = '".$navMaster[pk_nav_id]."',price_date= '".$update_date."',
										net_asset_value='".$val[net_asset_value]."',repurchase_price='".$val[repurchase_price]."',sale_price='".$val[sale_price]."',
										fr_unique_no='".$navMaster[Unique_No]."',fr_scheme_code='".$navMaster[Scheme_Code]."',
										fr_scheme_name='".addslashes($navMaster[Scheme_Name])."',ISIN = '".$val[ISIN]."'") or die(mysql_query());		
					$this->db->db_run_query("UPDATE amfi_data SET is_updated = 1 WHERE pk_amfi_price_id = '".$val[pk_amfi_price_id]."'") or die(mysql_query());
				}
				else
				{
					// THis date price already in table.
				}
			}
			else
			{
				// Not found in BSE Master Physical, then INSERT into NAV MASTER
			}
		}
	}
	function updateNAVMaster()
	{		
		$sqlNav			= "SELECT * FROM mf_scheme_bse_master WHERE is_updated = 0";
		$navResultSet	= $this->db->db_run_query($sqlNav);
		$insertNavSql	= "INSERT INTO mf_nav_master SET  ";
		$updateNavSql	= "UPDATE mf_nav_master SET  ";
		
		while($val = $this->db->db_fetch_array($navResultSet))	//while(list($key,$val) = each($resArr))
		{
			set_time_limit(0);
			$insertNavSql1	= " Unique_No = '".$val[Unique_No]."', Scheme_Code = '".$val[Scheme_Code]."', RTA_Scheme_Code = '".$val[RTA_Scheme_Code]."',
							   AMC_Scheme_Code = '".$val[AMC_Scheme_Code]."', ISIN = '".$val[ISIN]."', AMC_Code = '".$val[AMC_Code]."',
							   Scheme_Type = '".$val[Scheme_Type]."', Scheme_Plan = '".$val[Scheme_Plan]."', Scheme_Name = '".addslashes($val[Scheme_Name])."',
							   Purchase_Allowed = '".$val[Purchase_Allowed]."', Purchase_Transaction_mode = '".$val[Purchase_Transaction_mode]."', 
							   Minimum_Purchase_Amount = '".$val[Minimum_Purchase_Amount]."',Additional_Purchase_Amount = '".$val[Additional_Purchase_Amount]."', 
							   Maximum_Purchase_Amount = '".$val[Maximum_Purchase_Amount]."', Purchase_Amount_Multiplier = '".$val[Purchase_Amount_Multiplier]."',
							   Purchase_Cutoff_Time = '".$val[Purchase_Cutoff_Time]."', Redemption_Allowed = '".$val[Redemption_Allowed]."', 
							   Redemption_Transaction_Mode = '".$val[Redemption_Transaction_Mode]."',Minimum_Redemption_Qty = '".$val[Minimum_Redemption_Qty]."', 
							   Redemption_Qty_Multiplier = '".$val[Redemption_Qty_Multiplier]."', Maximum_Redemption_Qty = '".$val[Maximum_Redemption_Qty]."',
							   Redemption_Amount_Minimum = '".$val[Redemption_Amount_Minimum]."', Redemption_Amount_Maximum = '".$val[Redemption_Amount_Maximum]."',
							   Redemption_Amount_Multiple = '".$val[Redemption_Amount_Multiple]."', 
							   Redemption_Cut_off_Time = '".$val[Redemption_Cut_off_Time]."', RTA_Agent_Code = '".$val[RTA_Agent_Code]."',
							   AMC_Active_Flag = '".$val[AMC_Active_Flag]."', Dividend_Reinvestment_Flag = '".$val[Dividend_Reinvestment_Flag]."',
							   SIP_FLAG = '".$val[SIP_FLAG]."',SWP_Flag = '".$val[SWP_Flag]."',Switch_FLAG = '".$val[Switch_FLAG]."',
							   SETTLEMENT_TYPE = '".$val[SETTLEMENT_TYPE]."',AMC_IND = '".$val[AMC_IND]."',Face_Value = '".$val[Face_Value]."',
							   Start_Date = '".$val[Start_Date]."',End_Date = '".$val[End_Date]."',Exit_Load_Flag = '".$val[Exit_Load_Flag]."',
							   Exit_Load = '".$val[Exit_Load]."',Lock_in_Period_Flag = '".$val[Lock_in_Period_Flag]."',
							   Lock_in_Period = '".$val[Lock_in_Period]."',Channel_Partner_Code = '".$val[Channel_Partner_Code]."'";
							   
			if($this->db->db_num_rows($this->db->db_run_query("SELECT * FROM mf_nav_master WHERE Unique_No = '".$val[Unique_No]."'")) > 0)
				$insertNavSql1	= $updateNavSql.$insertNavSql1." WHERE Unique_No = '".$val[Unique_No]."'";
			else
				$insertNavSql1	= $insertNavSql.$insertNavSql1;
				
			//echo $insertNavSql1."<br><br>";	
			$this->db->db_run_query($insertNavSql1) or die(mysql_error());	
			$this->db->db_run_query("UPDATE mf_scheme_bse_master SET is_updated = 1 WHERE pk_scheme_id = '".$val[pk_scheme_id]."'");			
			$insertNavSql1 = '';		//exit;
		}
	}	
	function sendMapRequest($getPanNum)
	{
		$SQL = "SELECT * FROM mf_pan_attached_list WHERE fr_user_sender_id = '".$this->CONFIG->loggedUserId."' AND recevier_pan_num = '".$getPanNum."'";
		$checkNum =  $this->db->db_num_rows($this->db->db_run_query($SQL));
		if($checkNum == 0)
		{
			$SQL = "SELECT * FROM bfsi_users_details WHERE pan_number = '".$getPanNum."'";
			$checkPAN =  $this->db->db_num_rows($this->db->db_run_query($SQL));
			if($checkPAN == 0)
			{
				return "PAN Number Does Not Exist.";
			}
			else
			{
				$getDetail = $this->commonFunction->getSingleRow("SELECT * FROM  bfsi_users_details WHERE pan_number = '".$getPanNum."' AND fr_user_id != '".
																	$this->CONFIG->loggedUserId."'");
				$getEmail = $this->commonFunction->getSingleRow("SELECT * FROM  bfsi_user WHERE pk_user_id = '".$getDetail[fr_user_id]."'");
				$this->db->db_run_query("INSERT INTO mf_pan_attached_list SET fr_user_sender_id = '".$this->CONFIG->loggedUserId."',
																			  fr_user_recevier_id = '".$getDetail[fr_user_id]."',
																			  recevier_pan_num = '".$getPanNum."'");
				$message = $this->commonFunction->readPHP("../__lib.mailFormats/pan_request.html");
				$message = str_replace("USERNAME",$getDetail['cust_name'],$message);
				$this->commonFunction->send_mail($getEmail['login_id'],"PAN Request On TaxSave.in",$message,true);
				return "Request has been Sent to user.";
			}
		}
		else
			return "Request already sent.";
	}
	function listAllMappedPAN($getAllAttachRequest='',$attachedUser='',$pendingReq='',$panBasedResultSet='')
	{
		if($getAllAttachRequest != '')
		{
			$SQL = "SELECT * FROM mf_pan_attached_list WHERE fr_user_sender_id = '".$this->CONFIG->loggedUserId."' AND (request_status = 'Pending' OR attach_status = 
						'Rejected') ";
		}
		else if($attachedUser != '')
		{
			$SQL = "SELECT * FROM mf_pan_attached_list WHERE fr_user_sender_id = '".$this->CONFIG->loggedUserId."' AND request_status = 'Accepted' ";
		}
		else if($pendingReq != '')
		{
			$SQL = "SELECT * FROM mf_pan_attached_list WHERE fr_user_sender_id = '".$this->CONFIG->loggedUserId."' AND request_status = 'Pending' ";
		}
		else if($panBasedResultSet != '')
		{
			$SQL = "SELECT * FROM mf_pan_attached_list WHERE fr_user_sender_id = '".$this->CONFIG->loggedUserId."' AND pan_no = '' ";
		}
		else
			$SQL = "SELECT * FROM mf_pan_attached_list WHERE fr_user_sender_id = '".$this->CONFIG->loggedUserId."'";
		
		$returnArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
		return $returnArray;
	}
	function importRTAData($getFilename,$RTAName='')
	{
		$result1 = $this->db->db_run_query("select count(*) count from mf_".strtolower($RTAName));
		$r1 = $this->db->db_fetch_array($result1);
		$count1=(int)$r1['count'];
		$getCSVFile = str_replace("\"","",$getCSVFile);
		$getCSVFile = $this->convertToCSV($getFilename);
		//echo $importSQL = "LOAD DATA INFILE '".$getCSVFile."' INTO TABLE mf_".strtolower($RTAName)." FIELDS TERMINATED BY ',' ENCLOSED BY '\"' IGNORE 1 LINES";
		$importSQL = ' LOAD DATA LOCAL INFILE "'.$getCSVFile.'"
								INTO TABLE mf_'.strtolower($RTAName).'
								FIELDS TERMINATED by \',\' 
								LINES TERMINATED BY \''.$this->CONFIG->newLine.'\' IGNORE 1 LINES;';
		$this->db->db_run_query($importSQL);
		
		$result2 = $this->db->db_run_query("select count(*) count from mf_".strtolower($RTAName));
		$r2 = $this->db->db_fetch_array($result2);
		$count2=(int)$r2['count'];
		
		$count = $count2-$count1;
		return $count;
	}
	function convertToCSV($getExcelFile)
	{
		$excelFile = $getExcelFile;
		$path_parts = pathinfo($getExcelFile);
		$csvFile = $path_parts['dirname']."/".$path_parts['filename'].".csv";
		
		if(strtolower($path_parts['extension']) == 'dbf')
		{
			$dbf2csv = $this->CONFIG->wwwroot.'__lib.apis/__dbf2csv/dbf2csv.py';
			
			shell_exec('python "'.$dbf2csv.'" "'.$getExcelFile.'" "'.$csvFile.'"');
			foreach (glob($path_parts['dirname']."/".$path_parts['filename']."*.csv") as $filename) 
			{
 			   $csvFile = $filename;
			}
			return $csvFile;
		}
		
		ini_set('max_execution_time', 2000);
		ini_set('memory_limit', '281M'); 
		
		require_once $this->CONFIG->wwwroot.'__lib.apis/__excel.Lib/PHPExcel/PHPExcel.php';
		require_once $this->CONFIG->wwwroot.'__lib.apis/__excel.Lib/PHPExcel/PHPExcel/IOFactory.php';
		require_once $this->CONFIG->wwwroot.'__lib.apis/__excel.Lib/PHPExcel/PHPExcel/Writer/CSV.php';						
		
		$objPHPExcel = new PHPExcel();
		try 
		{
			$inputFileType = PHPExcel_IOFactory::identify($excelFile);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($excelFile);
		}
		catch(Exception $e)
		{
			die('Error loading file "'.pathinfo($excelFile,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->setUseBOM(true);
		$objWriter->save($csvFile);
		return $csvFile;
	}
	function listRTA($whichRTA,$pageVal)
	{
		$returnArray = array();
		$countRTA = $this->totalCountRTA($whichRTA);
		if($countRTA > 0)
		{
			$limit = " LIMIT ".$pageVal.",".$this->CONFIG->paginationPageItem;
			if(strtolower($whichRTA) == $this->CONFIG->RTANames[0])		// CAM
				$SQL = "SELECT folio_no,scheme,inv_name,usercode,purprice,units,amount,traddate,brokcode FROM mf_".strtolower($whichRTA).$limit;	
			if(strtolower($whichRTA) == $this->CONFIG->RTANames[1])		// KARVY
				$SQL = "SELECT FMCODE,FUNDDESC,INVNAME,SMCODE,TD_AMT,TD_UNITS,TD_AMT,TD_TRDT,TD_BRANCH FROM mf_".strtolower($whichRTA).$limit;	
			if(strtolower($whichRTA) == $this->CONFIG->RTANames[2])		// FRANKLIN
				$SQL = "SELECT FOLIO_NO,SCHEME_NA1,INVESTOR_2,CHECK_NO,DIVIDEND_4,UNITS,AMOUNT,CREA_DATE,BROK_COMM FROM mf_".strtolower($whichRTA).$limit;	
			if(strtolower($whichRTA) == $this->CONFIG->RTANames[3])		// SUNDRAM
				$SQL = "SELECT FOLIO_NO,SCHEME,INV_NAME,USERCODE,PURPRICE,UNITS,AMOUNT,TRADDATE,BROKCODE FROM mf_".strtolower($whichRTA).$limit;	

			$returnArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
		}
		else
			$returnArray = array("MF_NONE");
		
		return $returnArray;
	}
	function totalCountRTA($whichRTA)
	{
		$result1 = $this->db->db_run_query("select count(*) count from mf_".strtolower($whichRTA));
		$r1 = $this->db->db_fetch_array($result1);
		$count1=(int)$r1['count'];
		return $count1;
	}
	function saveOffer($getPostedData)
	{
		$insertSql = "INSERT INTO mf_nav_offer SET offer_name = '".$getPostedData[offer_name]."', offer_nav = '".implode(",",$getPostedData[duallistbox_demo1])."',
						offer_status = '".$getPostedData[offer_status]."'";
		if($getPostedData[offer_id] != '')
			$insertSql .= " WHERE pk_offer_id = '".$getPostedData[offer_id]."'";
		
		$this->db->db_run_query($insertSql);	
	}
	function getNavOffer($getOfferID='')
	{
		$SQL = "SELECT * FROM mf_nav_offer ";
		if($getOfferID[offer_id] !='')
			$SQL .= " WHERE pk_offer_id = '".$getOfferID[offer_id]."'";
			
		$navArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');// or die(mysql_error());	
		
		return $navArray;				
	}
	function offerListCount($getRequest='')
	{
		$SQL = "SELECT COUNT(*) as count FROM mf_nav_offer ";
		$result1 = $this->db->db_run_query($SQL);	// or die(mysql_error());
		$r1 = $this->db->db_fetch_array($result1);
		$count1=(int)$r1['count'];
		return $count1;
	}
	function offerList($getRequest,$pageLimit='')
	{
		$SQL = "SELECT * FROM mf_nav_offer ";			
		$countSearch = $this->offerListCount($getRequest);					
		
		if($countSearch > 0)
		{			
			$navArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');	
		}
		else
			$navArray = array("MF_NONE");
		
		//print_r($folioArray);	
		return $navArray;
	}	
	function deleteOffer($getRequest)
	{
		$result1 = $this->db->db_run_query("DELETE FROM ".$getRequest['tableName']." WHERE pk_offer_id = '".$getRequest['value']."'");
		return;
	}	
	function getMFName()
	{
		$SQL = "SELECT scheme_name,fr_scheme_code FROM mf_live_table GROUP BY scheme_name";
		$returnArray[] =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
	}
	function mfACStmt()
	{
		$SQL = "SELECT * FROM mf_live_table WHERE pan_no = ''";
		$resultArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');	

		$acStmtArr = array();
		$acStmtArr1 = array();
		while(list($key,$val) = each($resultArray))
		{
			$acValues = array($val[purchase_date],$val[tran_type],$val[purchase_price],$val[amount],$val[unit],$val[balance_unit]);
			$acStmtArr[$val[inv_name]][$val[scheme_name]][] = $acValues;
		}
		return $acStmtArr;
	}
	function portfolioSummary()
	{
		$SQL = "SELECT * FROM mf_live_table WHERE pan_no = ''";
		$resultArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');	

		$acStmtArr = array();
		$acStmtArr1 = array();
		while(list($key,$val) = each($resultArray))
		{
			if (strpos(strtolower($val[scheme_type]), 'equity') !== false) 
				$schemeCategory = "Equity-MF";
			else
				$schemeCategory = "DEBT-MF";
			
			$acValues = array($val[folio],$val[purchase_price],$switchIn,$divReinvAmt,$sold,$switchOut,$bal_unit,$cur_price,$gain,$absRet,$xirr);
			$acStmtArr[$val[inv_name]][$schemeCategory][$val[scheme_name]][] = $acValues;
			$schemeCategory = '';
		}
		return $acStmtArr;
	}
	function navMasterListCount($getRequest='')
	{
		$search_str = $getRequest[search_str];
		
		$searchRAWStr='';
		
		if($getRequest[Scheme_Plan] !='')
			$searchRAWStr = " mf_nav_master.Scheme_Plan = '".$getRequest[Scheme_Plan]."' AND ";
		if($getRequest[Scheme_Type] !='')
			$searchRAWStr .= " mf_nav_master.Scheme_Type = '".$getRequest[Scheme_Type]."' AND ";
		if($getRequest[Minimum_Purchase_Amount] !='')
			$searchRAWStr .= " mf_nav_master.Minimum_Purchase_Amount <= '".$getRequest[Minimum_Purchase_Amount]."' OR ";
			
		if($search_str !='')
			$searchRAWStr .= " (mf_nav_master.ISIN LIKE '%".$search_str."%' OR mf_nav_master.Scheme_Name LIKE '%".$search_str."%' ) ";
		
		$SQL = "SELECT COUNT(*) as count, mf_nav_price.*  FROM mf_nav_master
       				INNER JOIN mf_nav_price
          				ON (mf_nav_master.pk_nav_id = mf_nav_price.fr_nav_id) ";
		
		if($searchRAWStr !='')
			$SQL .= " WHERE ".$searchRAWStr;
	
							
		$result1 = $this->db->db_run_query($SQL);	// or die(mysql_error());
		$r1 = $this->db->db_fetch_array($result1);
		$count1=(int)$r1['count'];
		return $count1;
	}
	function getAllNAV()
	{
		$SQL = "SELECT mf_nav_master.pk_nav_id, mf_nav_master.Unique_No, mf_nav_master.Scheme_Name, mf_nav_master.Purchase_Allowed, mf_nav_price.fr_nav_id, 
				mf_nav_price.price_date, mf_nav_price.net_asset_value,mf_nav_price.repurchase_price,mf_nav_price.sale_price,mf_nav_price.pk_price_id  FROM mf_nav_master
       				INNER JOIN mf_nav_price
          				ON (mf_nav_master.Purchase_Allowed = 'Y' AND mf_nav_master.pk_nav_id = mf_nav_price.fr_nav_id AND mf_nav_price.net_asset_value != 0) 
						GROUP BY mf_nav_master.Scheme_Name ORDER BY mf_nav_price.pk_price_id DESC ";
		$navArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');// or die(mysql_error());	
		
		return $navArray;				
	}
	function getNavWithPriceArray()
	{
		$SQL = "SELECT * FROM mf_nav_master GROUP BY Scheme_Name";
		$navArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');	// or die(mysql_error());
		$priceSql = "SELECT * FROM mf_nav_price GROUP BY fr_nav_id ";
		$priceArray =  $this->commonFunction->mysqlResultIntoArray($priceSql,'SQL');	// or die(mysql_error());
		print_r($priceArray);	exit;
		$navArr = array();
		while(list($navKey,$navVal) = each($navArray))
		{
			$priceSql = "SELECT * FROM mf_nav_price WHERE fr_nav_id = '".$navVal[pk_nav_id]."' ORDER BY pk_price_id  DESC LIMIT 1";
			$result1 = $this->db->db_run_query($priceSql);	// or die(mysql_error());
			if($this->db->db_num_rows($result1) > 0)
			{
				$r1 = $this->db->db_fetch_array($result1);
				$net_asset_value = $r1[net_asset_value];
			}
			else
			{
				$net_asset_value = 0;
			}	
			//print_r($navVal);
			$navVal[net_asset_value] = $net_asset_value;
			//print_r($navVal);		
			$navArr[] = $navVal; 	
		}	
		return $navArr;
	}
	function navMasterList($getRequest,$pageLimit='')
	{
		if($pageLimit != '')
			$limit = " LIMIT ".$pageLimit.",".$this->CONFIG->paginationPageItem;
		else
			$limit='';
			
		if($pageLimit == 0)
			$limit = " LIMIT ".$pageLimit.",".$this->CONFIG->paginationPageItem;
					
		$search_str = $getRequest[search_str];		
		
		if(!empty($getRequest[order_by]))
			$orderBy = " ORDER BY ".$getRequest[order_by]." ".$getRequest[sorting];
		else
			$orderBy = '';
			
		$searchRAWStr='';
		
		if($getRequest[Scheme_Plan] !='')
			$searchRAWStr = " mf_nav_master.Scheme_Plan = '".$getRequest[Scheme_Plan]."' AND ";
		if($getRequest[Scheme_Type] !='')
			$searchRAWStr .= " mf_nav_master.Scheme_Type = '".$getRequest[Scheme_Type]."' AND ";
		if($getRequest[Minimum_Purchase_Amount] !='')
			$searchRAWStr .= " mf_nav_master.Minimum_Purchase_Amount <= '".$getRequest[Minimum_Purchase_Amount]."' OR ";
			
		if($search_str !='')
			$searchRAWStr .= " (mf_nav_master.ISIN LIKE '%".$search_str."%' OR mf_nav_master.Scheme_Name LIKE '%".$search_str."%' ) ";
		
		$SQL = "SELECT mf_nav_master.*,mf_nav_master.pk_nav_id,mf_nav_price.fr_nav_id,mf_nav_price.price_date,mf_nav_price.net_asset_value,
		        mf_nav_price.repurchase_price,mf_nav_price.sale_price,mf_nav_price.pk_price_id  FROM mf_nav_master
       				INNER JOIN mf_nav_price
          				ON (mf_nav_master.pk_nav_id = mf_nav_price.fr_nav_id) ";
		
		if($searchRAWStr !='')
			$SQL .= " WHERE ".$searchRAWStr;
		if($orderBy !='')
			$SQL .= "  ".$orderBy;
		if($limit !='')
			$SQL .= "  ".$limit;
			
		$countSearch = $this->navMasterListCount($getRequest);
		
		//echo $countSearch,$SQL;				
		
		if($countSearch > 0)
		{			
			$navArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');	
		}
		else
			$navArray = array("MF_NONE");
		
		//print_r($folioArray);	
		return $navArray;
	}	
	function getSchemeType()
	{
		$SQL = 'SELECT Scheme_Type FROM mf_nav_master GROUP BY Scheme_Type';
		$schemeTypeArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');	
		return $schemeTypeArray;
	}
	function getSchemePlan()
	{
		$SQL = 'SELECT Scheme_Plan FROM mf_nav_master GROUP BY Scheme_Plan';
		$schemePlanArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');	
		return $schemePlanArray;
	}
	function getPurchaseAmount()
	{
		$SQL = 'SELECT Minimum_Purchase_Amount FROM mf_nav_master GROUP BY Minimum_Purchase_Amount';
		$amtArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');	
		return $amtArray;
	}
	function folioQueryCount($getRequest='')
	{
		$search_str = $getRequest[search_str];
		if(!empty($this->CONFIG->loggedAdminId))
		{
			if($search_str !='')
				$search_str = " WHERE (scheme_name LIKE '%".$search_str."%' OR product_code LIKE '%".$search_str."%' OR folio_no LIKE '%".$search_str."%' OR 
							client_name LIKE '%".$search_str."%' OR pan_no LIKE '%".$search_str."%' OR address1 LIKE '%".$search_str."%')";
			$SQL = "SELECT count(*) count FROM mf_live_investor_folio ".$search_str;				
		}
		else
		{
			if($search_str !='')
				$search_str = " AND (scheme_name LIKE '%".$search_str."%' OR product_code LIKE '%".$search_str."%' OR folio_no LIKE '%".$search_str."%' OR 
							client_name LIKE '%".$search_str."%' OR pan_no LIKE '%".$search_str."%' OR address1 LIKE '%".$search_str."%')";
			$SQL = "SELECT count(*) count FROM mf_live_investor_folio WHERE pan_no = '' ".$search_str;				
		}
		//echo $SQL;			
		$result1 = $this->db->db_run_query($SQL);	// or die(mysql_error());
		$r1 = $this->db->db_fetch_array($result1);
		$count1=(int)$r1['count'];
		return $count1;
	}
	function folioQuery($getRequest,$pageLimit='')
	{
	
		if($pageLimit != '')
			$limit = " LIMIT ".$pageLimit.",".$this->CONFIG->paginationPageItem;
		else
			$limit='';
			
		if($pageLimit == 0)
			$limit = " LIMIT ".$pageLimit.",".$this->CONFIG->paginationPageItem;
			
		$countSearch = $this->folioQueryCount($search_str);
		$search_str = $getRequest[search_str];
		if(!empty($getRequest[order_by]))
			$orderBy = " ORDER BY ".$getRequest[order_by]." ".$getRequest[sorting];
		
		if(!empty($this->CONFIG->loggedAdminId))
		{
			if($search_str !='')
				$searchStr = " WHERE (scheme_name LIKE '%".$search_str."%' OR product_code LIKE '%".$search_str."%' OR folio_no LIKE '%".$search_str."%' OR 
							client_name LIKE '%".$search_str."%' OR pan_no LIKE '%".$search_str."%' OR address1 LIKE '%".$search_str."%')";
			$SQL = "SELECT * FROM mf_live_investor_folio  ".$searchStr." ".$orderBy.$limit;
		}
		else
		{
			if($search_str !='')
				$searchStr = " AND (scheme_name LIKE '%".$search_str."%' OR product_code LIKE '%".$search_str."%' OR folio_no LIKE '%".$search_str."%' OR 
							client_name LIKE '%".$search_str."%' OR pan_no LIKE '%".$search_str."%' OR address1 LIKE '%".$search_str."%')";
			$SQL = "SELECT * FROM mf_live_investor_folio WHERE pan_no = '' ".$searchStr." ".$orderBy.$limit;
		}	
		//echo $countSearch,$SQL;				
		if($countSearch > 0)
		{			
			$folioArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');	
		}
		else
			$folioArray = array("MF_NONE");
		
		//print_r($folioArray);	
		return $folioArray;
	}
	function updateInvestorFolio()
	{
		ini_set('max_execution_time', 2000);
		ini_set('memory_limit', '281M'); 
		$sql1		= "SELECT * FROM mf_live_investor_folio";
		$resultSet	= $this->db->db_run_query($sql1);
		if($this->db->db_num_rows($resultSet) > 0 )
		{
			while($rows = $this->db->db_fetch_array($resultSet))	//while(list($key,$val) = each($resArr))
			{
				if($rows['fr_cam_inv_id'] != 0)
				{
					$sql2 = "SELECT * FROM mf_cam_inv WHERE pk_inv_cam_id = '".$rows['fr_cam_inv_id']."'";
					$checkInCAMMainTable = $this->db->db_num_rows($this->db->db_run_query($sql2));
					if($checkInCAMMainTable == 0)
					{
						$this->db->db_run_query("DELETE FROM mf_live_investor_folio WHERE pk_mf_inv_id = '".$rows['pk_mf_inv_id']."'");
					}
				}
				else if($rows['fr_karvy_inv_id'] != 0)
				{
					$sql21 = "SELECT * FROM mf_karvy_inv WHERE pk_inv_karvy_id = '".$rows['fr_karvy_inv_id']."'";
					$checkInKARVYMainTable = $this->db->db_num_rows($this->db->db_run_query($sql21));
					if($checkInKARVYMainTable == 0)
					{
						$this->db->db_run_query("DELETE FROM mf_live_investor_folio WHERE pk_mf_inv_id = '".$rows['pk_mf_inv_id']."'");
					}				
				}
				else if($rows['fr_franklin_inv_id'] != 0)
				{
					$sql3 = "SELECT * FROM mf_franklin_inv WHERE pk_inv_franklin_id = '".$rows['fr_franklin_inv_id']."'";
					$checkInFRANKLINMainTable = $this->db->db_num_rows($this->db->db_run_query($sql3));
					if($checkInFRANKLINMainTable == 0)
					{
						$this->db->db_run_query("DELETE FROM mf_live_investor_folio WHERE pk_mf_inv_id = '".$rows['pk_mf_inv_id']."'");
					}				
				}
				else if($rows['fr_sundram_inv_id'] != 0)
				{
					$sql4 = "SELECT * FROM mf_sundram_inv WHERE pk_inv_sundram_id = '".$rows['fr_sundram_inv_id']."'";
					$checkInSUNDRAMMainTable = $this->db->db_num_rows($this->db->db_run_query($sql4));
					if($checkInSUNDRAMMainTable == 0)
					{
						$this->db->db_run_query("DELETE FROM mf_live_investor_folio WHERE pk_mf_inv_id = '".$rows['pk_mf_inv_id']."'");
					}				
				}
			}
		}
		$sql5 			= "SELECT * FROM mf_cam_inv WHERE inv_in_report = 0";
		$resultSetCAM	= $this->db->db_run_query($sql5);
		$insertCAMSql	= "INSERT INTO mf_live_investor_folio VALUES ( ";
		if($this->db->db_num_rows($resultSetCAM) > 0 )
		{  
			while($rows = $this->db->db_fetch_array($resultSetCAM))	//while(list($key,$val) = each($resArr))
			{
				set_time_limit(0);
				$rows = $this->commonFunction->removeCharFromArray($rows,'#');
				$rows = $this->commonFunction->removeCharFromArray($rows,'"');
				$rows = $this->commonFunction->removeCharFromArray($rows,'\'');

				$camSql = "'', '".$rows['pk_inv_cam_id']."', '', '', '', '".$rows['sch_name']."','".$rows['product']."', 
						  '".$rows['foliochk']."', '".$rows['inv_name']."', '', '".$rows['pan_no']."', '', '".$rows['address1']."','".$rows['address2']."',
						  '".$rows['address3']."',
						  '', '".$rows['joint1_pan']."', '', '".$rows['joint2_pan']."', '".$rows['nom_name']."', '".$rows['nom2_name']."', '".$rows['nom3_name']."',
						  '".$rows['ac_no']."', '".$rows['ac_type']."', '".$rows['bank_name']."', '".$rows['branch']."', '', '', '',
						  '', '',CURRENT_TIMESTAMP)";				
				$this->db->db_run_query($insertCAMSql.$camSql);		// or die(mysql_error());	
				$this->db->db_run_query("UPDATE mf_cam_inv SET inv_in_report = '1' WHERE  pk_inv_cam_id = '".$rows['pk_inv_cam_id']."'");		 				
			}
		}
		$sql6 			= "SELECT * FROM mf_franklin_inv WHERE inv_in_report = 0";
		$resultSetFRANK	= $this->db->db_run_query($sql6);
		$insertFRANKSql	= "INSERT INTO mf_live_investor_folio VALUES ( ";
		if($this->db->db_num_rows($resultSetFRANK) > 0 )
		{  
			while($rows = $this->db->db_fetch_array($resultSetFRANK))	//while(list($key,$val) = each($resArr))
			{
				set_time_limit(0);
				$rows = $this->commonFunction->removeCharFromArray($rows,'#');
				$rows = $this->commonFunction->removeCharFromArray($rows,'"');
				$rows = $this->commonFunction->removeCharFromArray($rows,'\'');

				$frankSql = "'', '', '', '".$rows['pk_inv_franklin_id']."', '', '".$rows['sch_name']."','".$rows['product']."', 
						  '".$rows['FOLIO_NO']."', '".$rows['INV_NAME']."', '', '".$rows['PANNO1']."', '', '".$rows['ADDRESS1']."','".$rows['ADDRESS2']."',
						  '".$rows['ADDRESS3']."',
						  '', '".$rows['joint1_pan']."', '', '".$rows['joint2_pan']."', '".$rows['NOMINEE1']."', '".$rows['NOMINEE2']."', '".$rows['NOMINEE3']."',
						  '".$rows['ACCNT_NO']."', '".$rows['AC_TYPE']."', '".$rows['BANK_NAME']."', '".$rows['BRANCH']."', '', '', '',
						  '', '',CURRENT_TIMESTAMP)";				
				$this->db->db_run_query($insertFRANKSql.$frankSql) or die(mysql_error());		
				$this->db->db_run_query("UPDATE mf_franklin_inv SET inv_in_report = '1' WHERE  pk_inv_franklin_id = '".$rows['pk_inv_franklin_id']."'");		 				
			}
		}
		$sql7 			= "SELECT * FROM mf_karvy_inv WHERE inv_in_report = 0";
		$resultSetKARVY	= $this->db->db_run_query($sql7);
		$insertKARVYSql	= "INSERT INTO mf_live_investor_folio VALUES ( ";
		if($this->db->db_num_rows($resultSetKARVY) > 0 )
		{  
			while($rows = $this->db->db_fetch_array($resultSetKARVY))	//while(list($key,$val) = each($resArr))
			{
				set_time_limit(0);
				$rows = $this->commonFunction->removeCharFromArray($rows,'#');
				$rows = $this->commonFunction->removeCharFromArray($rows,'"');
				$rows = $this->commonFunction->removeCharFromArray($rows,'\'');

				$karvySql = "'', '', '".$rows['pk_inv_karvy_id']."', '', '', '".$rows['Fund_Description']."','".$rows['Product_Code']."',  
						  '".$rows['Folio']."', '".$rows['Investor_Name']."', '', '".$rows['PAN_Number']."', '', '".$rows['Address__1']."','".$rows['Address__2']."',
						  '".$rows['Address__3']."',
						  '', '".$rows['PAN2']."', '', '".$rows['PAN3']."', '".$rows['Nominee']."', '".$rows['Nominee2']."', '".$rows['Nominee3']."',
						  '".$rows['BankAccno']."', '".$rows['Account_Type']."', '".$rows['Bank_Name']."', '".$rows['Branch']."', '', '', '',
						  '', '',CURRENT_TIMESTAMP)";				
				$this->db->db_run_query($insertKARVYSql.$karvySql) or die(mysql_error());	
				$this->db->db_run_query("UPDATE mf_karvy_inv SET inv_in_report = '1' WHERE  pk_inv_karvy_id = '".$rows['pk_inv_karvy_id']."'");		 				
			}
		}
		$sql8 			= "SELECT * FROM mf_sundram_inv WHERE inv_in_report = 0";
		$resultSetSUND	= $this->db->db_run_query($sql8);
		$insertSUNDSql	= "INSERT INTO mf_live_investor_folio VALUES ( ";
		if($this->db->db_num_rows($resultSetSUND) > 0 )
		{  
			while($rows = $this->db->db_fetch_array($resultSetSUND))	//while(list($key,$val) = each($resArr))
			{
				set_time_limit(0);
				$rows = $this->commonFunction->removeCharFromArray($rows,'#');
				$rows = $this->commonFunction->removeCharFromArray($rows,'"');
				$rows = $this->commonFunction->removeCharFromArray($rows,'\'');

				$sundSql = "'', '', '', '', '".$rows['pk_inv_sundram_id']."', '".$rows['SCHEME']."','".$rows['PRODCODE']."',  
						  '".$rows['FOLIO']."', '".$rows['INVNAME']."', '', '".$rows['PAN']."', '', '".$rows['ADDRESS1']."','".$rows['ADDRESS2']."',
						  '".$rows['ADDRESS3']."',
						  '', '".$rows['JOINT1PAN']."', '', '".$rows['JOINT2PAN']."', '".$rows['NOMNAME']."', '".$rows['Nominee2']."', '".$rows['Nominee3']."',
						  '".$rows['BANKACNO']."', '', '".$rows['BANKNAME']."', '".$rows['BANKBRA']."', '', '', '',
						  '', '',CURRENT_TIMESTAMP)";				
				$this->db->db_run_query($insertSUNDSql.$sundSql) or die(mysql_error());	
				$this->db->db_run_query("UPDATE mf_sundram_inv SET inv_in_report = '1' WHERE  pk_inv_sundram_id = '".$rows['pk_inv_sundram_id']."'");		 				
			}
		}
	}
	function updateLiveMFTable()
	{
		$this->updateKARVYData();
		$this->updateCAMData();
		$this->updateFRANKLINData();
		$this->updateSUNDRAMData();
	}
	function updateSUNDRAMData()
	{
		$sqlSundram			= "SELECT * FROM mf_".$this->CONFIG->RTANames[3]." WHERE in_report = 0";
		//$resArr			=  $this->commonFunction->mysqlResultIntoArray($sqlCam,'SQL');
		$sunResultSet		= $this->db->db_run_query($sqlSundram);
		$insertCamSql	= "INSERT INTO mf_live_table (fr_scheme_code,scheme_name,purchase_date,purchase_price,amount,unit,tran_type,balance_unit,folio,dividend
							,gain,absolute_return,xirr,pan_no,fr_sundram_id,scheme_type,inv_name) VALUES ";
		$insertCamSql1 = '';
		
		while($val = $this->db->db_fetch_array($sunResultSet))	//while(list($key,$val) = each($resArr))
		{
			set_time_limit(0);
			if (str_replace("\"","",$val[SCHEME]) == "") 
			{
     			//Nothing to do 
    		}
			else
			{
				if (strpos($val[TRADDATE], '-') !== false) 
					$tradDateArr = explode("-",str_replace("\"","",$val[TRADDATE]));
				else if (strpos($val[TRADDATE], '/') !== false)
					$tradDateArr = explode("/",str_replace("\"","",$val[TRADDATE]));
				else if (strpos($val[TRADDATE], '.') !== false)
					$tradDateArr = explode(".",str_replace("\"","",$val[TRADDATE]));
	
				if(strlen($tradDateArr[2]) == 2)
				{				
					$tradDate = "20".$tradDateArr[2]."-".$tradDateArr[0]."-".$tradDateArr[1]; 
				}
				else
				{
					$tradDate = $tradDateArr[2]."-".$tradDateArr[0]."-".$tradDateArr[1]; 
				}
	
				$insertCamSql1	= "('".str_replace("\"","",$val[PRODCODE])."','".str_replace("\"","",str_replace("'","",$val[SCHEME]))."','".$tradDate
									."','".str_replace("\"","",$val[PURPRICE])."','".str_replace("\"","",$val[AMOUNT])."','".str_replace("\"","",$val[UNITS])
									."','".str_replace("\"","",$val[TRXN_NATUR])."','','".str_replace("\"","",$val[FOLIO_NO])."','".
									str_replace("\"","",$val[TRXN_NATUR])."','','','','".str_replace("\"","",$val[PAN])."','".$val[pk_sundram_id]."','".
									str_replace("\"","",$val[SCHEME_TYP])."','".str_replace("\"","",$val[INV_NAME])."')";
	
				//echo $insertCamSql.$insertCamSql1."<br><br>";
				$this->db->db_run_query($insertCamSql.$insertCamSql1) or die(mysql_error());
				//exit;
				$insertCamSql1 = '';
				$this->db->db_run_query("UPDATE mf_".$this->CONFIG->RTANames[3]." SET in_report =1 WHERE pk_sundram_id=".$val[pk_sundram_id]);
			}
		}		
	}	
	function updateFRANKLINData()
	{
		$sqlFranklin		= "SELECT * FROM mf_".$this->CONFIG->RTANames[2]." WHERE in_report = 0";
		//$resArr			=  $this->commonFunction->mysqlResultIntoArray($sqlCam,'SQL');
		$frankResultSet		= $this->db->db_run_query($sqlFranklin);
		$insertCamSql	= "INSERT INTO mf_live_table (fr_scheme_code,scheme_name,purchase_date,purchase_price,amount,unit,tran_type,balance_unit,folio,dividend
							,gain,absolute_return,xirr,pan_no,fr_franklin_id,scheme_type,inv_name) VALUES ";
		$insertCamSql1 = '';
		
		while($val = $this->db->db_fetch_array($frankResultSet))		//while(list($key,$val) = each($resArr))
		{
			set_time_limit(0);
			if (str_replace("\"","",$val[SCHEME_NA1]) == "") 
			{
     			//Nothing to do 
    		}
			else
			{
				if (strpos($val[TRXN_DATE], '-') !== false) 
					$tradDateArr = explode("-",str_replace("\"","",$val[TRXN_DATE]));
				else if (strpos($val[TRXN_DATE], '/') !== false)
					$tradDateArr = explode("/",str_replace("\"","",$val[TRXN_DATE]));
				else if (strpos($val[TRXN_DATE], '.') !== false)
					$tradDateArr = explode(".",str_replace("\"","",$val[TRXN_DATE]));
	
				if(strlen($tradDateArr[2]) == 2)
				{				
					$tradDate = "20".$tradDateArr[2]."-".$tradDateArr[0]."-".$tradDateArr[1]; 
				}
				else
				{
					$tradDate = $tradDateArr[2]."-".$tradDateArr[0]."-".$tradDateArr[1]; 
				}
	
				$insertCamSql1	= "('".str_replace("\"","",$val[SCHEME_CO0])."','".str_replace("\"","",str_replace("'","",$val[SCHEME_NA1]))."','".$tradDate
									."','".str_replace("\"","",$val[NAV])."','".str_replace("\"","",$val[AMOUNT])."','".str_replace("\"","",$val[UNITS])
									."','".str_replace("\"","",$val[TRXN_TYPE])."','','".str_replace("\"","",$val[FOLIO_NO])."','".
									str_replace("\"","",$val[DIVIDEND_4])."','','','','".str_replace("\"","",$val[IT_PAN_NO1])."','".$val[pk_franklin_id]."','".
									str_replace("\"","",$val[PLAN_TYPE])."','".str_replace("\"","",$val[INVESTOR_2])."')";
	
				//echo $insertCamSql.$insertCamSql1."<br><br>";
				$this->db->db_run_query($insertCamSql.$insertCamSql1) or die(mysql_error());
				
				$insertCamSql1 = '';
				$this->db->db_run_query("UPDATE mf_".$this->CONFIG->RTANames[2]." SET in_report =1 WHERE pk_franklin_id=".$val[pk_franklin_id]);
			}
		}		
	}
	function updateKARVYData()
	{
		$sqlKarvy			= "SELECT * FROM mf_".$this->CONFIG->RTANames[1]." WHERE in_report = 0";
		//$resArr			=  $this->commonFunction->mysqlResultIntoArray($sqlCam,'SQL');
		$karvyResultSet		= $this->db->db_run_query($sqlKarvy);
		$insertCamSql	= "INSERT INTO mf_live_table (fr_scheme_code,scheme_name,purchase_date,purchase_price,amount,unit,tran_type,balance_unit,folio,dividend
							,gain,absolute_return,xirr,pan_no,fr_karvy_id,scheme_type,inv_name) VALUES ";
		$insertCamSql1 = '';
		
		while($val = $this->db->db_fetch_array($karvyResultSet))			//while(list($key,$val) = each($resArr))
		{
			set_time_limit(0);
			if (str_replace("\"","",$val[FUNDDESC]) == "") 
			{
     			//Nothing to do 
    		}
			else
			{
				if (strpos($val[TD_TRDT], '-') !== false) 
					$tradDateArr = explode("-",str_replace("\"","",$val[TD_TRDT]));
				else if (strpos($val[TD_TRDT], '/') !== false)
					$tradDateArr = explode("/",str_replace("\"","",$val[TD_TRDT]));
				else if (strpos($val[TD_TRDT], '.') !== false)
					$tradDateArr = explode(".",str_replace("\"","",$val[TD_TRDT]));
	
				if(strlen($tradDateArr[2]) == 2)
				{				
					$tradDate = "20".$tradDateArr[2]."-".$tradDateArr[0]."-".$tradDateArr[1]; 
				}
				else
				{
					$tradDate = $tradDateArr[2]."-".$tradDateArr[0]."-".$tradDateArr[1]; 
				}

				$insertCamSql1	= "('".str_replace("\"","",$val[FMCODE])."','".str_replace("\"","",str_replace("'","",$val[FUNDDESC]))."','".$tradDate
									."','".str_replace("\"","",$val[TD_UNITS])."','".str_replace("\"","",$val[TD_AMT])."','".str_replace("\"","",$val[TD_UNITS])
									."','".str_replace("\"","",$val[TRDESC])."','','".str_replace("\"","",$val[TD_ACNO])."','".
									str_replace("\"","",$val[TRDESC])."','','','','".str_replace("\"","",$val[PAN1])."','".$val[pk_karvy_id]."','".
									str_replace("\"","",$val[SCHPLN])."','".str_replace("\"","",$val[INVNAME])."')";
				//echo $insertCamSql.$insertCamSql1."<br><br>";
				$this->db->db_run_query($insertCamSql.$insertCamSql1) or die(mysql_error());
				$insertCamSql1 = '';
				$this->db->db_run_query("UPDATE mf_".$this->CONFIG->RTANames[1]." SET in_report =1 WHERE pk_karvy_id=".$val[pk_karvy_id]);
			}
		}
	}
	function updateCAMData()
	{
		$sqlCam			= "SELECT * FROM mf_".$this->CONFIG->RTANames[0]." WHERE in_report = 0";
		//$resArr			=  $this->commonFunction->mysqlResultIntoArray($sqlCam,'SQL');
		$resultSet 		= $this->db->db_run_query($sqlCam);
		$insertCamSql	= "INSERT INTO mf_live_table (fr_scheme_code,scheme_name,purchase_date,purchase_price,amount,unit,tran_type,balance_unit,folio,dividend
							,gain,absolute_return,xirr,pan_no,fr_cam_id,scheme_type,inv_name) VALUES ";

		while($val = $this->db->db_fetch_array($resultSet))		//while(list($key,$val) = each($resArr))
		{
			set_time_limit(0);
			if (str_replace("\"","",$val[prodcode]) == "") 
			{
     			// Nothing to do
    		}
			else
			{
				if (strpos($val[traddate], '-') !== false) 
					$tradDateArr = explode("-",str_replace("\"","",$val[traddate]));
				else if (strpos($val[traddate], '/') !== false)
					$tradDateArr = explode("/",str_replace("\"","",$val[traddate]));
				else if (strpos($val[traddate], '.') !== false)
					$tradDateArr = explode(".",str_replace("\"","",$val[traddate]));
	
				if(strlen($tradDateArr[2]) == 2)
				{				
					$tradDate = "20".$tradDateArr[2]."-".$tradDateArr[0]."-".$tradDateArr[1]; 
				}
				else
				{
					$tradDate = $tradDateArr[2]."-".$tradDateArr[0]."-".$tradDateArr[1]; 
				}

				$insertCamSql1	= "('".str_replace("\"","",$val[prodcode])."','".str_replace("\"","",str_replace("'","",$val[scheme]))."','".$tradDate
									."','".str_replace("\"","",$val[purprice])."','".str_replace("\"","",$val[amount])."','".str_replace("\"","",$val[units])
									."','".str_replace("\"","",$val[trxn_type_flag])."','','".str_replace("\"","",$val[folio_no])."','".
									str_replace("\"","",$val[trxn_type_flag])."','','','','".str_replace("\"","",$val[pan])."','".$val[pk_cam_id]."','".
									str_replace("\"","",$val[scheme_type])."','".str_replace("\"","",$val[inv_name])."')";
	
				//echo $insertCamSql.$insertCamSql1."<br><br>";
				$this->db->db_run_query($insertCamSql.$insertCamSql1) or die(mysql_error());
				
				$insertCamSql1 = '';
				$this->db->db_run_query("UPDATE mf_".$this->CONFIG->RTANames[0]." SET in_report =1 WHERE pk_cam_id=".$val[pk_cam_id]);
			}
		}
	}
	function singleFieldAutocomplete($searchString,$field)
	{
		$returnArray = array();
		if($field == "scheme")
		{
			$SQL = "SELECT scheme FROM mf_cam WHERE scheme LIKE '%".$searchString."%' GROUP BY scheme";
			$returnArray[] =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
			$SQL = "SELECT FUNDDESC FROM mf_karvy WHERE FUNDDESC LIKE '%".$searchString."%' GROUP BY FUNDDESC";
			$returnArray[] =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
			$SQL = "SELECT SCHEME_NA1 FROM mf_franklin WHERE SCHEME_NA1 LIKE '%".$searchString."%' GROUP BY SCHEME_NA1";
			$returnArray[] =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
			$SQL = "SELECT SCHEME FROM mf_sundram WHERE SCHEME LIKE '%".$searchString."%' GROUP BY SCHEME";
			$returnArray[] =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
		}
		if($field == "inv_name")
		{
			$SQL = "SELECT inv_name FROM mf_cam WHERE inv_name LIKE '%".$searchString."%' GROUP BY inv_name";
			$returnArray[] =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
			$SQL = "SELECT INVNAME FROM mf_karvy WHERE INVNAME LIKE '%".$searchString."%' GROUP BY INVNAME";
			$returnArray[] =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
			$SQL = "SELECT INVESTOR_2 FROM mf_franklin WHERE INVESTOR_2 LIKE '%".$searchString."%' GROUP BY INVESTOR_2";
			$returnArray[] =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
			$SQL = "SELECT INV_NAME FROM mf_sundram WHERE INV_NAME LIKE '%".$searchString."%' GROUP BY INV_NAME";
			$returnArray[] =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
		}
		if($field == "brok_code")
		{
			$SQL = "SELECT brokcode FROM mf_cam WHERE brokcode LIKE '%".$searchString."%' GROUP BY brokcode";
			$returnArray[] =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
			$SQL = "SELECT TD_BRANCH FROM mf_karvy WHERE TD_BRANCH LIKE '%".$searchString."%' GROUP BY TD_BRANCH";
			$returnArray[] =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
			$SQL = "SELECT BROK_COMM FROM mf_franklin WHERE BROK_COMM LIKE '%".$searchString."%' GROUP BY BROK_COMM";
			$returnArray[] =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
			$SQL = "SELECT BROKCODE FROM mf_sundram WHERE BROKCODE LIKE '%".$searchString."%' GROUP BY BROKCODE";
			$returnArray[] =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
		}
		return $returnArray;
	}
	function searchMFLayerOneDB($getSearchString,$pageVal)
	{
		$returnArray = array();
		$sDate		= $getSearchString['start'];
		$eDate		= $getSearchString['end'];
		$scheme		= $getSearchString['scheme'];
		$inv_name	= $getSearchString['inv_name'];
		$brok_code	= $getSearchString['brok_code'];
		
		$whereCam		= "";
		$whereKarvy		= "";
		$whereFranklin	= "";
		$whereSundram	= "";
		
		if($scheme != '')
		{
			$whereCam		.= "  scheme LIKE '%".$scheme."%' OR ";
			$whereKarvy		.= "  FUNDDESC LIKE '%".$scheme."%' OR ";
			$whereFranklin	.= "  SCHEME_NA1 LIKE '%".$scheme."%' OR ";
			$whereSundram	.= "  SCHEME LIKE '%".$scheme."%' OR ";
		}
		if($inv_name != '')
		{
			$whereCam		.= "  inv_name LIKE '%".$inv_name."%' OR ";
			$whereKarvy		.= "  INVNAME LIKE '%".$inv_name."%' OR ";
			$whereFranklin	.= "  INVESTOR_2 LIKE '%".$inv_name."%' OR ";
			$whereSundram	.= "  INV_NAME LIKE '%".$inv_name."%' OR ";
		}
		if($brok_code != '')
		{
			$whereCam		.= "  brokcode LIKE '%".$brok_code."%' OR ";
			$whereKarvy		.= "  TD_BRANCH LIKE '%".$brok_code."%' OR ";
			$whereFranklin	.= "  BROK_COMM LIKE '%".$brok_code."%' OR ";
			$whereSundram	.= "  BROKCODE LIKE '%".$brok_code."%' OR ";
		}
		
		if($sDate != '' && $eDate != '')
		{
			$whereCam		.= "  str_to_date(traddate,'%d/%m/%Y') BETWEEN str_to_date('".$sDate."','%d/%m/%Y') AND str_to_date('".$eDate."','%d/%m/%Y') OR ";
			$whereKarvy		.= "  str_to_date(TD_TRDT,'%d/%m/%Y') BETWEEN str_to_date('".$sDate."','%d/%m/%Y') AND str_to_date('".$eDate."','%d/%m/%Y') OR ";
			$whereFranklin	.= "  str_to_date(CREA_DATE,'%d/%m/%Y') BETWEEN str_to_date('".$sDate."','%d/%m/%Y') AND str_to_date('".$eDate."','%d/%m/%Y') OR ";
			$whereSundram	.= "  str_to_date(TRADDATE,'%d/%m/%Y') BETWEEN str_to_date('".$sDate."','%d/%m/%Y') AND str_to_date('".$eDate."','%d/%m/%Y') OR ";
		}
		else if($sDate != '' && $eDate == '')
		{
			$whereCam		.= "  str_to_date(traddate,'%d/%m/%Y') >= '".$sDate."' OR ";
			$whereKarvy		.= "  str_to_date(TD_TRDT,'%d/%m/%Y') >= '".$sDate."' OR ";
			$whereFranklin	.= "  str_to_date(CREA_DATE,'%d/%m/%Y') >= '".$sDate."' OR ";
			$whereSundram	.= "  str_to_date(TRADDATE,'%d/%m/%Y') >= '".$sDate."' OR ";
		}
		else if($sDate == '' && $eDate != '')
		{
			$whereCam		.= "  str_to_date(traddate,'%d/%m/%Y') <= '".$eDate."' OR ";
			$whereKarvy		.= "  str_to_date(TD_TRDT,'%d/%m/%Y') <= '".$eDate."' OR ";
			$whereFranklin	.= "  str_to_date(CREA_DATE,'%d/%m/%Y') <= '".$eDate."' OR ";
			$whereSundram	.= "  str_to_date(TRADDATE,'%d/%m/%Y') <= '".$eDate."' OR ";
		}
		
		$whereCam		= " WHERE ".substr($whereCam,0,-3);
		$whereKarvy		= " WHERE ".substr($whereKarvy,0,-3);
		$whereFranklin	= " WHERE ".substr($whereFranklin,0,-3);
		$whereSundram	= " WHERE ".substr($whereSundram,0,-3);
		
		$limit = " LIMIT ".$pageVal.",".$this->CONFIG->paginationPageItem;

		$SQL = "SELECT folio_no,scheme,inv_name,usercode,purprice,units,amount,traddate,brokcode FROM mf_".$this->CONFIG->RTANames[0].$whereCam.$limit;	
		$returnArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
		
		$SQL = "SELECT FMCODE,FUNDDESC,INVNAME,SMCODE,TD_AMT,TD_UNITS,TD_AMT,TD_TRDT,TD_BRANCH FROM mf_".$this->CONFIG->RTANames[1].$whereKarvy.$limit;	
		$returnArray =  array_merge($returnArray,$this->commonFunction->mysqlResultIntoArray($SQL,'SQL'));
		
		$SQL = "SELECT FOLIO_NO,SCHEME_NA1,INVESTOR_2,CHECK_NO,DIVIDEND_4,UNITS,AMOUNT,CREA_DATE,BROK_COMM FROM mf_".$this->CONFIG->RTANames[2].$whereFranklin.$limit;	
		$returnArray =  array_merge($returnArray,$this->commonFunction->mysqlResultIntoArray($SQL,'SQL'));
		
		$SQL = "SELECT FOLIO_NO,SCHEME,INV_NAME,USERCODE,PURPRICE,UNITS,AMOUNT,TRADDATE,BROKCODE FROM mf_".$this->CONFIG->RTANames[3].$whereSundram.$limit;	
		$returnArray =  array_merge($returnArray,$this->commonFunction->mysqlResultIntoArray($SQL,'SQL'));
			
		
		if(count($returnArray) > 0)
		{
			$returnArray = array(count($returnArray),$returnArray);
		}
		else
			$returnArray = array("MF_NONE");
		
		return $returnArray;
	}
}

?>