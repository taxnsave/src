<?php
class buySell{

	function buySell()
	{
		global $CONFIG,$commonFunction,$documentFiles,$customerProfile;
		global $db;
		$this->db					= $db;
		$this->dbName				= $CONFIG->dbName;	
		$this->commonFunction		= $commonFunction;	
		$this->CONFIG				= $CONFIG;	
		$this->documentFiles		= $documentFiles;	
		$this->customerProfile		= $customerProfile;	
		$this->buySell				= array();
	}	
	function recomendNAV($REQUEST)
	{
		//print_r($REQUEST);
		while(list($key,$val) = each($REQUEST[recomend]))
		{
			if($this->isRecomended($val) == 0)
			{
				$navDetails = $this->commonFunction->getSingleRow("SELECT * FROM mf_nav_master WHERE pk_nav_id = '".$val."'");
				$this->db->db_run_query("INSERT INTO mf_nav_recomended SET fr_nav_id ='".$navDetails[pk_nav_id].
										"',fr_unique_no='".$navDetails[Unique_No]."',fr_sceme_code='".$navDetails[Scheme_Code]."',
										fr_scheme_name='".$navDetails[Scheme_Name]."',fr_RTA_code='".$navDetails[RTA_Agent_Code]."'");
				
				$insertedRec = $this->db->db_insert_id(); 
				if($this->db->db_num_rows($this->db->db_run_query("SELECT * FROM mf_nav_price WHERE ISIN = '".$navDetails[ISIN]."'")) > 0)
				{
					$navPrice = $this->commonFunction->getSingleRow("SELECT * FROM mf_nav_price WHERE ISIN = '".$navDetails[ISIN]."' ORDER BY pk_price_id DESC");
					$this->db->db_run_query("UPDATE mf_nav_recomended SET net_asset_value = '".$navPrice[net_asset_value]."', 
					repurchase_price = '".$navPrice[repurchase_price]."', sale_price ='".$navPrice[sale_price]."' WHERE pk_recomend_id = '".$insertedRec."'");
				}
			}
		}
		return "MF Recomended.";
	}
	function isRecomended($getNAVId)
	{
		return $this->db->db_num_rows($this->db->db_run_query("SELECT * FROM mf_nav_recomended WHERE fr_nav_id = '".$getNAVId."'"));
	}
	function getAllRecomendedNAV()
	{
		$SQL = "SELECT mf_nav_recomended.*,mf_nav_master.* FROM mf_nav_recomended INNER JOIN mf_nav_master 
						ON ( mf_nav_recomended.fr_nav_id = mf_nav_master.pk_nav_id )";
		// WHERE mf_nav_recomended.fr_nav_id = 1
		$returnArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
		return $returnArray;
	}
	function getAllOfferedNAV($commaSeperatedNavId)
	{
		$SQL = "SELECT * FROM mf_nav_master WHERE pk_nav_id IN (".$commaSeperatedNavId.")";
		// WHERE mf_nav_recomended.fr_nav_id = 1
		$returnArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
		return $returnArray;
	}
	function getNAVAllPrices($getNAVId,$retFields='')
	{
		if($retFields == '')
			$fields = 'net_asset_value,pk_price_id';
		else
			$fields = '*';
				
		$SQL = "SELECT ".$fields." FROM mf_nav_price WHERE fr_nav_id = '".$getNAVId."' ORDER BY pk_price_id DESC";
		$returnArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
		return $returnArray;
	}
	function getNAVPriceWithName($getNAVId,$retFields='')
	{
		if($retFields == '')
			$fields = 'net_asset_value,pk_price_id';
		else
			$fields = '*';
				
		$SQL = "SELECT ".$fields." FROM mf_nav_price WHERE fr_nav_id = '".$getNAVId."' GROUP BY fr_scheme_name ORDER BY pk_price_id DESC";
		$returnArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');
		return $returnArray;
	}
	function getSingleNAVDetails($getNAVId)
	{
		$navDetails = array();
		$SQL = "SELECT * FROM mf_nav_master WHERE pk_nav_id = '".$getNAVId."'";
		$navArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');	// or die(mysql_error());
		$priceSql = "SELECT * FROM mf_nav_price WHERE fr_nav_id = '".$getNAVId."' ORDER BY pk_price_id DESC ";
		$priceArray =  $this->commonFunction->mysqlResultIntoArray($priceSql,'SQL');	// or die(mysql_error());
		$navDetails['MF_DETAILS'] = $navArray;
		$navDetails['MF_PRICE'] = $priceArray;
		return $navDetails;
		//print_r($priceArray);	exit;
	}
	function createOrder($userId,$navDetails,$navPrice,$orderStatus,$amount)
	{
		$retArray = array();
		$SQL = "SELECT * FROM mf_order WHERE fr_user_id = '".$userId."' AND scheme_code = '".$navDetails[0]['Scheme_Code']."' AND trxnstatus = '".$orderStatus."'";
		$query = $this->db->db_run_query($SQL);
		$num = $this->db->db_num_rows($query);
		$unique_id = time();
		
		if($num == 0)
		{
			$this->db->db_run_query("INSERT INTO mf_order (fr_user_id,scheme_code,scheme_name,scheme_type,purprice,amount,trxnstatus) VALUES 
									('".$userId."','".$navDetails[0]['Scheme_Code']."','".$navDetails[0]['Scheme_Name']."','".$navDetails[0]['Scheme_Type']."',
									'".$navPrice[0]['net_asset_value']."','".$amount."','".$orderStatus."')");
			$order_id = $this->db->db_insert_id();
			$ref_no = date("Ymd")."1".$this->CONFIG->loggedUserId.sprintf('%06d', $order_id); 
			$unique_id = $unique_id.sprintf('%06d', $order_id); 
			$retArray = array("unique_id" => $unique_id, "ref_no" => $ref_no, "pk_order_id" => $order_id);
			$this->db->db_run_query("UPDATE mf_order SET order_ref_no = '".$ref_no."',unique_ref_no='".$unique_id."' WHERE pk_order_id = '".$order_id."'");
		}
		else
		{
			$retArray 				= $this->db->db_fetch_array($query);
			$ref_no 				= date("Ymd")."1".$this->CONFIG->loggedUserId.sprintf('%06d', $retArray[pk_order_id]); 
			$unique_id 				= $unique_id.sprintf('%06d', $retArray[pk_order_id]);
			$this->db->db_run_query("UPDATE mf_order SET order_ref_no = '".$ref_no."',amount='".$amount."',unique_ref_no='".$unique_id."'
																						 WHERE pk_order_id = '".$retArray[pk_order_id]."'");
			$retArray[unique_id]	= $unique_id;
			$retArray[ref_no] 		= $ref_no;
		}
		return $retArray;
	}
	function orderListCount($userId='')
	{
		if($userId == '')
			$SQL = "SELECT COUNT(*) as count FROM mf_order ORDER BY pk_order_id DESC";
		else
			$SQL = "SELECT COUNT(*) as count FROM mf_order WHERE fr_user_id = '".$userId."' ORDER BY pk_order_id DESC";
			
		$result1 = $this->db->db_run_query($SQL);	// or die(mysql_error());
		$r1 = $this->db->db_fetch_array($result1);
		$count1=(int)$r1['count'];
		return $count1;
	}
	function orderList($userId='')
	{
		if($userId == '')
			$SQL = "SELECT * FROM mf_order ORDER BY pk_order_id DESC ";	
		else
			$SQL = "SELECT * FROM mf_order WHERE fr_user_id = '".$userId."' ORDER BY pk_order_id DESC";
							
		$countSearch = $this->orderListCount($getRequest);					
		
		if($countSearch > 0)
		{			
			$navArray =  $this->commonFunction->mysqlResultIntoArray($SQL,'SQL');	
		}
		else
			$navArray = array("MF_NONE");
		
		//print_r($folioArray);	
		return $navArray;
	}	
}

?>