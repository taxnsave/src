<?php
class bseSync{

	function bseSync()
	{
		global $CONFIG,$commonFunction,$documentFiles,$customerProfile,$buySell;
		global $db;
		$this->db					= $db;
		$this->dbName				= $CONFIG->dbName;	
		$this->commonFunction		= $commonFunction;	
		$this->CONFIG				= $CONFIG;	
		$this->documentFiles		= $documentFiles;	
		$this->customerProfile		= $customerProfile;	
		$this->bseSync				= array();
	}	
	function prepareUserParamBSE($getUserId='')
	{
		$returnArray = array();
		
		if($getUserId == '')
			$userId = $this->CONFIG->loggedUserId;
		else
			$userId = $getUserId;
		
		$userDetails = 	$this->userKYCData($userId);
		$bankDetails = 	$this->customerProfile->getCustomerBankInfo($userId);
		
		$returnArray['USER_DETAIL'] =  $userDetails;
		$returnArray['BANK_DETAIL'] =  $bankDetails;
		
		return $returnArray;
	}
	function userKYCData($getUserId='')
	{
		if($getUserId == '')
			$userId = $this->CONFIG->loggedUserId;
		else
			$userId = $getUserId;
	
		$user_data = $this->customerProfile->getCustomerInfo($userId);
		return $user_data;
	}
	function createUserBSEString($getUserId='')
	{
		if($getUserId == '')
			$userId = $this->CONFIG->loggedUserId;
		else
			$userId = $getUserId;
		
		$getDetails = $this->prepareUserParamBSE($userId);
		
		if($getDetails[USER_DETAIL][bse_id] == '')
		{
			//echo $getDetails[USER_DETAIL][fr_customer_id];	echo "<br>";
			$client_code = "D".date("y",strtotime($getDetails[USER_DETAIL][created_date])).sprintf('%07d', $getDetails[USER_DETAIL][pk_user_id]);
			$this->db->db_run_query("UPDATE bfsi_user SET bse_id = '".$client_code."' WHERE pk_user_id = '".$getDetails[USER_DETAIL][pk_user_id]."'");
		}
		else
			$client_code = $getDetails[USER_DETAIL][bse_id];
			
		$appname1	= $getDetails[USER_DETAIL][cust_name];
		$dob		= date("d/m/Y",strtotime($getDetails[USER_DETAIL][dob]));		//dd/mm/yyyy
		$gender		= $this->CONFIG->genderArr[$getDetails[USER_DETAIL][sex]];
		
		$acc_no		= strtoupper($getDetails[BANK_DETAIL][acc_no]);
		$ifsc_code	= strtoupper($getDetails[BANK_DETAIL][ifsc_code]);
		$add1		= strtoupper($getDetails[USER_DETAIL][address1]);
		$add2		= strtoupper($getDetails[USER_DETAIL][address2]);
		$add3		= strtoupper($getDetails[USER_DETAIL][address3]);
		$city		= strtoupper($getDetails[USER_DETAIL][city]);
		$state		= $this->CONFIG->stateCodeBSE[ucwords($getDetails[USER_DETAIL][state])];
		$pincode	= strtoupper($getDetails[USER_DETAIL][pincode]);
		$email		= strtoupper($getDetails[USER_DETAIL][login_id]);

		$phone		= strtoupper($getDetails[USER_DETAIL][contact_no]);
		$pan		= strtoupper($getDetails[USER_DETAIL][pan_number]);
		//echo "<pre>";print_r($getDetails);
		$userParam = array( "CODE" => $client_code,
							"HOLDING" => $this->CONFIG->clientHolding['Single'],
							"TAXSTATUS" => $this->CONFIG->taxStatus['Individual'],
							"OCCUPATIONCODE" => $this->CONFIG->occupationCode['Service'],
							"APPNAME1" => $appname1,
							"APPNAME2" => "",
							"APPNAME3" => "",
							"DOB" => $dob,
							"GENDER" => $gender,
							"FATHER/HUSBAND/gurdian" => "01",
							"PAN" => $pan,
							"NOMINEE" => "",
							"NOMINEE_RELATION" => "",
							"GUARDIANPAN" => "",
							"TYPE" => "P",
							"DEFAULTDP" => "NSDL",
							"CDSLDPID" => "",
							"CDSLCLTID" => "",
							"NSDLDPID" => "",						//IN302164
							"NSDLCLTID" => "",						//10295484
							"ACCTYPE_1" => "SB",					//bank.account_type_bse,
							"ACCNO_1" => $acc_no,
							"MICRNO_1" => "",
							"NEFT/IFSCCODE_1" => $ifsc_code,
							"default_bank_flag_1" => "Y",
							"ACCTYPE_2" => "",
							"ACCNO_2" => "",
							"MICRNO_2" => "",
							"NEFT/IFSCCODE_2" => "",
							"default_bank_flag_2" => "",
							"ACCTYPE_3" => "",
							"ACCNO_3" => "",
							"MICRNO_3" => "",
							"NEFT/IFSCCODE_3" => "",
							"default_bank_flag_3" => "",
							"ACCTYPE_4" => "",
							"ACCNO_4" => "",
							"MICRNO_4" => "",
							"NEFT/IFSCCODE_4" => "",
							"default_bank_flag_4" => "",
							"ACCTYPE_5" => "",
							"ACCNO_5" => "",
							"MICRNO_5" => "",
							"NEFT/IFSCCODE_5" => "",
							"default_bank_flag_5" => "",
							"CHEQUENAME" => "",
							"ADD1" => str_replace(","," ",str_replace("-","",$add1)),
							"ADD2" => str_replace(","," ",str_replace("-","",$add2)),
							"ADD3" => str_replace(","," ",str_replace("-","",$add3)),
							"CITY" => $city,
							"STATE" => $state,
							"PINCODE" => $pincode,
							"COUNTRY" => "INDIA",
							"RESIPHONE" => "",
							"RESIFAX" => "",
							"OFFICEPHONE" => "",
							"OFFICEFAX" => "",
							"EMAIL" => $email,
							"COMMMODE" => "M",
							"DIVPAYMODE" => "01",
							"PAN2" => "",
							"PAN3" => "",
							"MAPINNO" => "",
							//"ARNCODE" => "ARN-60277",
							"CM_FORADD1" => "",
							"CM_FORADD2" => "",
							"CM_FORADD3" => "",
							"CM_FORCITY" => "",
							"CM_FORPINCODE" => "",
							"CM_FORSTATE" => "",
							"CM_FORCOUNTRY" => "",
							"CM_FORRESIPHONE" => "",
							"CM_FORRESIFAX" => "",
							"CM_FOROFFPHONE" => "",
							"CM_FOROFFFAX" => "",
							"CM_MOBILE" => $phone);
		$fatcaParam = array("PAN_RP" => $pan,
							"PEKRN" => "",
							"INV_NAME" => $appname1,
							"DOB" => "",
							"FR_NAME" => "",
							"SP_NAME" => "",
							"TAX_STATUS" => $this->CONFIG->taxStatus["Individual"],
							"DATA_SRC" => "E",
							"ADDR_TYPE" => "1",
							"PO_BIR_INC" => "IN",
							"CO_BIR_INC" => "IN",
							"TAX_RES1" => "IN",
							"TPIN1" => $pan,
							"ID1_TYPE" => "C",
							"TAX_RES2" => "",
							"TPIN2" => "",
							"ID2_TYPE" => "",
							"TAX_RES3" => "",
							"TPIN3" => "",
							"ID3_TYPE" => "",
							"TAX_RES4" => "",
							"TPIN4" => "",
							"ID4_TYPE" => "",
							"SRCE_WEALT" => $this->CONFIG->sourceOfWealth["Salary"],
							"CORP_SERVS" => "",
							"INC_SLAB" => "32",
							"NET_WORTH" => "",
							"NW_DATE" => "",
							"PEP_FLAG" => "N",
							"OCC_CODE" => $this->CONFIG->occupationCode["Professional"],
							"OCC_TYPE" => "S",
							"EXEMP_CODE" => "",
							"FFI_DRNFE" => "",
							"GIIN_NO" => "",
							"SPR_ENTITY" => "",
							"GIIN_NA" => "",
							"GIIN_EXEMC" => "",
							"NFFE_CATG" => "",
							"ACT_NFE_SC" => "",
							"NATURE_BUS" => "",
							"REL_LISTED" => "",
							"EXCH_NAME" => "O",
							"UBO_APPL" => "N",
							"UBO_COUNT" => "",
							"UBO_NAME" => "",
							"UBO_PAN" => "",
							"UBO_NATION" => "",
							"UBO_ADD1" => "",
							"UBO_ADD2" => "",
							"UBO_ADD3" => "",
							"UBO_CITY" => "",
							"UBO_PIN" => "",
							"UBO_STATE" => "",
							"UBO_CNTRY" => "",
							"UBO_ADD_TY" => "",
							"UBO_CTR" => "",
							"UBO_TIN" => "",
							"UBO_ID_TY" => "",
							"UBO_COB" => "",
							"UBO_DOB" => "",
							"UBO_GENDER" => "",
							"UBO_FR_NAM" => "",
							"UBO_OCC" => "",
							"UBO_OCC_TY" => "",
							"UBO_TEL" => "",
							"UBO_MOBILE" => "",
							"UBO_CODE" => "",
							"UBO_HOL_PC" => "",
							"SDF_FLAG" => "",
							"UBO_DF" => "",
							"AADHAAR_RP" => "",
							"NEW_CHANGE" => "N",
							"LOG_NAME" => $client_code,
							"DOC1" => "",
							"DOC2" => "");
		
		$strUserParam ='';
		$strFatcaParam ='';
							
		while(list($userKey,$userVal) = each($userParam))
		{
			$strUserParam .= $userVal."|";
		}					
		while(list($fatcaKey,$fatcaVal) = each($fatcaParam))
		{
			$strFatcaParam .= $fatcaVal."|";
		}					
		
		return array("USER_PARAM" => substr($strUserParam,0,-1), "FATCA_PARAM" => substr($strFatcaParam,0,-1));
	}
	function userUpdateBSE($userParam,$fatcaParam)
	{
		$crUserTools = $this->CONFIG->apidir."__ESBTOOL/createUserBSE.py";
		$crUserCMD = "/usr/local/bin/python3.6 \"".$crUserTools."\" \"".$userParam."\" \"".$fatcaParam."\" 2>&1";
		//$crUserCMD = "/usr/local/bin/python2.7 \"".$crUserTools."\" \"".$userParam."\" \"".$fatcaParam."\" 2>&1";
		$output = $this->commonFunction->runInShell($crUserCMD);
		$getError = $this->getBSEErrorMsg($output);
		if($getError !='')
		{
			return $getError;
		}		
		
		
		$pos1 = strpos($output, "Remote end closed connection without response");
		if ($pos1 === false) 
		{
			// do nothing
		}
		else
		{	
			//print_r($output);exit;
			return '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button"><i class="ace-icon fa fa-times"></i>
					</button><strong><i class="ace-icon fa fa-times"></i>Oh snap!</strong> Remote end closed connection without response<br></div>';
		}
		$pos = strpos($output, "BSE error 644:");
		if ($pos === false) {
			return '<div class="alert alert-block alert-success"><button data-dismiss="alert" class="close" type="button"><i class="ace-icon fa fa-times"></i>
					</button><p><strong><i class="ace-icon fa fa-check"></i>Success!</strong> User Updated successfully.</p></div>';
		} else {
			$err = explode("unsuccessful:",$output);
			//print_r($err);
			return '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button"><i class="ace-icon fa fa-times"></i>
					</button><strong><i class="ace-icon fa fa-times"></i>Oh snap!</strong> '.$err[count($err)-1].'<br></div>';
		}		
	}
	function placeOrderBSE($pipeValues,$getOrderDetails)
	{
		$this->db->db_run_query("UPDATE mf_order SET pipe_value = '".$pipeValues."' WHERE pk_order_id = '".$getOrderDetails['pk_order_id']."'");
		
		$crUserTools = $this->CONFIG->apidir."__ESBTOOL/orderBSE.py";
		$crUserCMD = "/usr/local/bin/python3.6 \"".$crUserTools."\" \"".$pipeValues."\" 2>&1";
		//exit;//$crUserCMD = "/usr/local/bin/python2.7 \"".$crUserTools."\" \"".$pipeValues."\" 2>&1";
		$output = $this->commonFunction->runInShell($crUserCMD);
		$getError = $this->getBSEErrorMsg($output,$getOrderDetails['pk_order_id']);
		if($getError !='')
		{
			return $getError;
		}
		//print_r($output);		
		$pos = strpos($output, "100:");
		if ($pos === false) {
			$err = explode("641:",$output);
			$err1 = explode(",",$err[count($err)-1]);		
			$msg = str_replace("'","",$err1[count($err1)-2]);	
			$this->db->db_run_query("UPDATE mf_order SET bse_remarks = '".$msg."' WHERE pk_order_id = '".$getOrderDetails['pk_order_id']."'");
			return '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button"><i class="ace-icon fa fa-times"></i>
					</button><strong><i class="ace-icon fa fa-times"></i>Oh snap!</strong> '.str_replace("'","",$err1[count($err1)-2]).'<br></div>';
		} else {
			$err = explode("[",$output);
			$err1 = explode(",",$err[count($err)-1]);	
			$msg = str_replace("'","",$err1[count($err1)-2]);
			$bse_order_id = str_replace("'","",$err1[2]);
			//print_r($err);
			//print_r($err1);
			$this->db->db_run_query("UPDATE mf_order SET bse_order_id = '".$bse_order_id."',bse_remarks = '".$msg."',trxnstatus = 'Success' WHERE 
																				pk_order_id = '".$getOrderDetails['pk_order_id']."'");
			return '<div class="alert alert-block alert-success"><button data-dismiss="alert" class="close" type="button"><i class="ace-icon fa fa-times"></i>
					</button><p><strong><i class="ace-icon fa fa-check"></i>Success!</strong> '.$msg.'</p></div>';
		}		
	}
	function getBSEErrorMsg($getOutput,$updateStatusBSE='')
	{
		$pos = strpos($getOutput, "640:");
		if ($pos === false) 
		{
			return;
		}
		else
		{
			$err = explode("640:",$getOutput); //print_r($err);
			if($updateStatusBSE != '')
			{
				$this->db->db_run_query("UPDATE mf_order SET bse_remarks = '".str_replace("'","",$err[count($err)-1])."' WHERE	pk_order_id = '".$updateStatusBSE."'");
			}
			return '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button"><i class="ace-icon fa fa-times"></i>
					</button><strong><i class="ace-icon fa fa-times"></i>Oh snap!</strong> '.str_replace("'","",$err[count($err)-1]).'<br></div>';
		}
	}
}

?>