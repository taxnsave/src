<?php
	
class mdocmail{

	function sendHTMLMails($TO,$FROM_EMAIL,$FROM_NAME,$SUBJECT,$HTML_TEXT,$PLAIN_TEXT,$SET_MAIL_FORMAT,$ATTACHEMENT='')
	{
		include_once '../__lib.apis/__lib.mail/class.phpmailer.php';
		//echo $TO,$FROM_EMAIL,$FROM_NAME,$SUBJECT,$HTML_TEXT,$PLAIN_TEXT,$SET_MAIL_FORMAT,$ATTACHEMENT='';
		//exit;
		try {
		
			$mail = new PHPMailer(true); //New instance, with exceptions enabled	
			
			if ($SET_MAIL_FORMAT == 'text') 							
				$body = $PLAIN_TEXT;
			else
				$body = $HTML_TEXT;
						
			$body             = preg_replace('/\\\\/','', $body); //Strip backslashes
		
			//$mail->IsSMTP();                           // tell the class to use SMTP
			//$mail->SMTPAuth   = true;                  // enable SMTP authentication
			//$mail->Port       = 587;                    // set the SMTP server port
			//$mail->Host       = "mail.gmail.com"; 		// SMTP server
			//$mail->Username   = "support@mdocuments.in";     // SMTP server username
			//$mail->Password   = "";            // SMTP server password
		
			$mail->IsMail();	//IsSendmail();  // tell the class to use Sendmail
		
			
		
			$mail->From       = $FROM_EMAIL;
			$mail->FromName   = $FROM_NAME;			
			
			//$mail->SetFrom($FROM_EMAIL, $FROM_NAME);
			//$mail->AddReplyTo("support@mdocuments.in","mDocuments Support");
			$mail->AddAddress($TO);
		
			$mail->Subject  = $SUBJECT;
		
			//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
			$mail->WordWrap   = 80; // set word wrap
		
			if ($SET_MAIL_FORMAT == 'text') 
			{
				$mail->Body = $body;
			}
			else
			{
				$mail->MsgHTML($body);
			}
			
			if(!empty($ATTACHEMENT))
			{
				while(list($KEY,$VAL)=each($ATTACHEMENT))
				{
					//echo $VAL;
					/*$getOnlyFilenameArr = explode("/",$VAL);
					$getOnlyFilename = $getOnlyFilenameArr[count($getOnlyFilenameArr)-1]; 
					$sentFilenameArr = explode("_",$getOnlyFilename);
					$sentFilename='';
					//print_r($sentFilenameArr);
					while(list($KEYsentFilename,$VALsentFilename)=each($sentFilenameArr))
					{
						//echo $KEYsentFilename." : ".$VALsentFilename."<br>";
						if($KEYsentFilename > 0)
							$sentFilename .= $VALsentFilename."_";
					}
					$sentFilename = substr($sentFilename,0,-1);*/
					$mail->AddAttachment($KEY,$VAL); 
					//exit;
				}	
			}			 
			
			if ($SET_MAIL_FORMAT == 'text') 
			{
				$mail->IsHTML(false); // send as HTML		
			}
			else
			{
				$mail->IsHTML(true); // send as HTML		
			}
			
			$mail->Send();
			//echo 'Message has been sent.';
		} catch (phpmailerException $e) {
			//echo $e->errorMessage();
		}		
	}

	function sendMailToMembers($TO,$FROM,$SUBJECT,$PLAIN_TEXT,$HTML_TEXT,$ATTACHEMENT='',$CC='',$BCC='')
	{
		$notice_text = "This is a multi-part message in MIME format.";
		$plain_text = $PLAIN_TEXT;		//"This is a plain text email.\r\nIt is very cool.";
		$html_text = $HTML_TEXT;		//"<html><body>This is an <b style='color:purple'>HTML</b> text email.\r\nIt is very cool.</body></html>";

		$semi_rand = md5(time());
		$mime_boundary = "==MULTIPART_BOUNDARY_$semi_rand";
		$mime_boundary_header = chr(34) . $mime_boundary . chr(34);

		$to = $TO;							
											
		$from = $FROM;						
		$subject = $SUBJECT;				
		
$body = "$notice_text

--$mime_boundary
Content-Type: text/plain; charset=us-ascii
Content-Transfer-Encoding: 7bit

$plain_text

--$mime_boundary
Content-Type: text/html; charset=us-ascii
Content-Transfer-Encoding: 7bit

$html_text

--$mime_boundary--";

		if (@mail($to, $subject, $body,
			"From: " . $from . "\n" .
			//"bcc: " . $bcc . "\n" .
			"MIME-Version: 1.0\n" .
			"Content-Type: multipart/alternative;\n" .
			"     boundary=" . $mime_boundary_header))
			return 1;
		else
			return 0;	
	}
}
?>