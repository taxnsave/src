<?php

	include("../__lib.includes/config.inc.php");
	if(!($_SESSION['oPageAccess'])) { header("HTTP/1.1 401 Unauthorized");header("Location: $CONFIG->siteurl");exit;}
	
    if($_REQUEST['action'] == "st")
	{		
		include("../__lib.htmlPages/__postLoginPages/__willPages/startpage.php");
		exit;	
	}
	if($_REQUEST['action'] == "pi")
	{		
		include("../__lib.htmlPages/__postLoginPages/__willPages/dashboard.php");
		exit;	
	}
	else if($_REQUEST['action'] == "bene")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/beneficiary-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "exec")
	{																//print_r($_REQUEST);print_r($_SESSION);
		include("../__lib.htmlPages/__postLoginPages/__willPages/executor-details.php");
		exit;	
	}
    else if($_REQUEST['action'] == "assets")
	{	
		include("../__lib.htmlPages/__postLoginPages/__willPages/asset-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "ba")
	{	
		include("../__lib.htmlPages/__postLoginPages/__willPages/bank-tab.php");
		exit;	
	}
	else if($_REQUEST['action'] == "smb")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/mutual-tab.php");
		exit;	
	}
	else if($_REQUEST['action'] == "ip")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/immovable-properties-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "rf")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/retire-tab.php");
		exit;	
	}
	else if($_REQUEST['action'] == "lip")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/life-insurance-policies.php");
		exit;	
	}
	else if($_REQUEST['action'] == "liab")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/liabilities-tab.php");
		exit;	
	}
	else if($_REQUEST['action'] == "nb")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/custodian-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_bank-account-details")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/bank-account-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_locker-details")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/locker-details.php");
		exit;	
	}
    else if($_REQUEST['action'] == "sub_fixed_deposit")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/fixed-deposit-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_mutual-funds-details")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/mutual-funds-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_share-details")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/share-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_bond-details")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/bond-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_other-assets-details")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/other-assets-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_digital-assets-details")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/digital-assets-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_vehicle-details")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/vehicle-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_liability-details")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/liability-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_additional-liability")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/additional-liability.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_retirement-plan-details")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/retirement-plan-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_pension-fund")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/pension-fund.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_gratuity-fund")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/gratuity-fund.php");
		exit;	
	}
	else if($_REQUEST['action'] == "add_bussiness")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/business-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "add_bussiness")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/business-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "add_gi")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/general-insurance.php");
		exit;	
	}
	else if($_REQUEST['action'] == "add_jwe")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/jewellery-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "add_bo")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/body-organ.php");
		exit;	
	}
	else if($_REQUEST['action'] == "add_pa")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/pet-animal.php");
		exit;	
	}
	else if($_REQUEST['action'] == "wi")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/witness-details.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_contingency-special-clause")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/contingency-special-clause.php");
		exit;	
	}
	else if($_REQUEST['action'] == "sub_special-clause")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/special-clause.php");
		exit;	
	}
	else if($_REQUEST['action'] == "oa")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/other-assets-tab.php");
		exit;	
	}
	else if($_REQUEST['action'] == "word")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/word.php");
		exit;	
	}
    else if($_REQUEST['action'] == "willpay")
	{
		include("../__lib.htmlPages/__postLoginPages/__willPages/will_payment.php");
		exit;	
	}


?>