<?php

	include("../__lib.includes/config.inc.php");
	if(!($_SESSION['oPageAccess'])) { header("HTTP/1.1 401 Unauthorized");header("Location: $CONFIG->siteurl");exit;}
	
	$getPortFolioArr = $mutualFund->portfolioSummary();
	//echo "<pre>";
	//print_r($getPortFolioArr);	
	//foreach($getAcStmtArr as $v)
	//foreach($v as $v1)	{ echo "d";print_r($v1);}	
?>
<table class="table table-bordered table-striped">
    <thead class="thin-border-bottom">
        <tr>
            <th class="text-primary">Scheme/Company</th>
            <th class="text-primary">Folio</th>
            <th class="text-primary">Purchase</th>
            <th class="text-primary">Switch In</th>
            <th class="text-primary">Dividend Reinvestment</th>
            <th class="text-primary">Sell</th>
            <th class="text-primary">Switch Out</th>
            <th class="text-primary">Balance Unit/No</th>
            <th class="text-primary">Current Value</th>
            <th class="text-primary">Dividend Paid/Interest</th>
            <th class="text-primary">Gain</th>
            <th class="text-primary">Absolute Return %</th>
            <th class="text-primary">XIRR(%)</th>
        </tr>
    </thead>
<tbody class="scrollable" data-size="125">   
<?php
	
	$tot_purchase=0;
	$tot_switchin=0;
	$tot_div_reinv=0;
	$tot_sold=0;
	$tot_curr_price=0;
	$tot_divpaid=0;
	$tot_absret=0;
	$tot_gain=0;
	$tot_xirr=0;
	
	while(list($key,$val)=each($getPortFolioArr))
	{
		echo "<tr><td colspan='13'><span class='label label-warning arrowed-right'><strong>".$key."</strong></span></td></tr>";
		while(list($key1,$val1)=each($val))
		{
			echo "<tr><td colspan='13'><span class='label label-inverse arrowed-right'>".$key1."</span></td></tr>";		//print_r($val1);
			while(list($key2,$val2)=each($val1))
			{
				//echo "<pre>".$key2,$val2;print_r($val2);
				echo "<tr><td>".$key2."</td>";
				$purchase_price = 0;
				$switchIn		= 0;
				$divReinvAmt	= 0;
				$sold			= 0;
				$switchOut		= 0;
				$bal_unit		= 0;
				$cur_price		= 0;
				$gain			= 0;
				$absRet			= 0;
				$xirr			= 0;
				$divPaid		= 0;				
				while(list($key3,$val3)=each($val2))
				{
					//print_r($val3);
					while(list($key4,$val4)=each($val3))
					{
						if($key4 != 0)			// Skip folio number
						{
							$purchase_price += $val3[1];
							$switchIn		+= $val3[2];
							$divReinvAmt	+= $val3[3];
							$sold			+= $val3[4];
							$switchOut		+= $val3[5];
							$bal_unit		+= $val3[6];
							$cur_price		+= $val3[7];
							$gain			+= $val3[8];
							$absRet			+= $val3[9];
							$xirr			+= $val3[10];
							$divPaid		= 100;

							$tot_purchase		+= $val3[1];
							$tot_switchin		+= $val3[2];
							$tot_div_reinv		+= $val3[3];
							$tot_sold			+= $val3[4];
							$tot_curr_price		+= $val3[7];
							$tot_divpaid		+= $val3[8];
							$tot_absret			+= $val3[9];
							$tot_gain			+= $val3[8];
							$tot_xirr			+= $val3[10];							
						}
						else
							$folio_no = $val3[0];							
					}										
				}
				echo "<td>".$folio_no."</td><td>".$purchase_price."</td><td>".$switchIn."</td><td>".$divReinvAmt."</td><td>".$sold."</td><td>".
					 $switchOut."</td><td>".$bal_unit."</td><td>".$cur_price."</td><td>".$divPaid."</td><td>".$gain."</td><td>".$absRet."</td><td>".$xirr."</td>";
				echo "</tr>";
			}
		}	
	}
	echo "<tr style='background-color:#307ecc;color:#ffffff;font-weight:bold;'>";		
	echo "<td>Total</td><td>&nbsp;</td><td>".$tot_purchase."</td><td>".$tot_switchin."</td><td>".$tot_div_reinv."</td><td>".$tot_sold."</td>".
		 "</td><td>&nbsp;</td><td>".$tot_curr_price."</td><td>".$tot_divpaid."</td><td>".$tot_gain."</td><td>".$tot_absret."</td><td>".$tot_xirr."</td>";
	echo "<td>&nbsp;</td></tr>";		
?>                                	
</tbody>
</table>