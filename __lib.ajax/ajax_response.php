<?php

	include("../__lib.includes/config.inc.php");
	include("../__lib.includes/Sms.php");
    

	if(!($_SESSION['oPageAccess'])) { header("HTTP/1.1 401 Unauthorized");header("Location: $CONFIG->siteurl");exit;}
	

	if($_REQUEST['action'] == "doAdminLogin")
	{
		//print_r($_POST);
		if($_POST[username] == '' || $_POST[password] == '')
		{
			echo "Empty Fields Not Allowed";
			exit;
		}
		$result = $db->db_run_query("SELECT * FROM ".$CONFIG->dbName.".bfsi_admin_user WHERE login_id  = '".$_POST[username]."' 
						AND passwd = '".md5($_POST[password])."' AND user_status='Active'");
		$retVal = $db->db_num_rows($result);

		if($retVal > 0)
		{
			$customerDetails = $db->db_fetch_array($result);
			$_SESSION[$CONFIG->sessionPrefix.'adminLoginStatus']		= true;
			$_SESSION[$CONFIG->sessionPrefix.'a_user_id']				= $customerDetails['pk_admin_user_id'];
			$_SESSION[$CONFIG->sessionPrefix.'a_email_id']				= $customerDetails['login_id'];
			$_SESSION[$CONFIG->sessionPrefix.'a_user_name']				= $customerDetails['admin_name'];			

			$_SESSION[$CONFIG->sessionPrefix."user_id"]					= '';
			$_SESSION[$CONFIG->sessionPrefix."customer_id"]				= '';
			$_SESSION[$CONFIG->sessionPrefix.'email_id']				= '';	
			$_SESSION[$CONFIG->sessionPrefix.'user_name']				= '';	
										
			$customerLog->activityLogin($customerDetails['pk_admin_user_id'],'',$customerDetails['login_id'],$CONFIG->loggedIP,'ADMIN');
																	// print_r($_SESSION);exit;
			echo 'PASS';
			exit;
		}
		else
		{
			echo "LOGIN_FAILED";
			exit;
		}
	}
	else if($_REQUEST['action'] == "doLogin")
	{
		//print_r($_POST);
		if($_POST[email] == '' || $_POST[passwd] == '')
		{
			echo "Empty Fields Not Allowed";
			exit;
		}
		$result = $commonFunction->check_login($_POST[email],$_POST[passwd]);	//md5(sha1("09022011 - ").$_POST[txtPassword])
		$retVal = $db->db_num_rows($result);

		if($retVal > 0)
		{
			$customerDetails = $db->db_fetch_array($result);
			$loggedin = $commonFunction->doLogin($customerDetails['pk_user_id']);		
			$customerLog->activityLogin($CONFIG->loggedUserId,$CONFIG->customerId,$CONFIG->loggedUserEmail,$CONFIG->loggedIP);
																	// print_r($_SESSION);exit;
			echo 'PASS';
			exit;
		}
		else
		{
			echo "LOGIN_FAILED";
			exit;
		}
	}
	else if($_REQUEST['action'] == "doSignup")
    {																//print_r($_REQUEST);print_r($_SESSION);
		if(md5(trim($_POST['sec_code'])) != $_SESSION['securimage_code_value'])
		{
			echo "WRONG_PASSCODE";
			exit;
		}
        if($_POST['otp'] != $_SESSION['otp'])
        {
            echo "WRONG_OTP";
            exit;
        }
		if($_POST[email] == '')
		{
			echo "Blank Email not allowed.";
			exit;
		}
        //$result = "111";
		$result = $commonFunction->is_email_exist($_POST[email]);				
		//echo "Rows " . $result;
        
        if($result > 0)
		{
			echo "REGISTER_FAILED";
            exit;
		}
		else
		{            
            //else verify otp and add user
			$getCustomerID = $customerProfile->newCustomer($_POST);
            //echo "sign up started 1";
			$loggedin = $commonFunction->doLogin($getCustomerID);	
            //echo "sign up started 2 " . $getCustomerID;
			
            if($getCustomerID != '')
			{				
				echo "REGISTER_DONE";
			}
			exit;
		}		
	}
    else if($_REQUEST['action'] == "doSendOTP")
    {
        $result = $commonFunction->is_email_exist($_POST[email]);
        if($result > 0)
        {
            echo "EMAIL_EXISTS";
            exit;
        }
        //send OTP here 
        $mobno = $_POST['mobile'];
    
        $otpnew = mt_rand(10000, 99999);
        //echo $otpnew;
        $_SESSION['otp'] = $otpnew; 
        //$_SESSION['otp'] = "12345"; 
        $otp_msg = "Your OTP to Register on TAXSAVE is ".$otpnew." The OTP will be valid for next 15 mins";
        $url = "https://api-alerts.solutionsinfini.com/v4/?api_key=A97ac1e77641316f29e16438656e2cbb4&method=sms&message=".$otp_msg."&to=".$mobno."&sender=TAXSAV";

    
        $result = file_get_contents($url);
        if ($result === FALSE) { echo "OTP_FAILED. Please retry after some time."; }
        else {
            //echo $result;
            //echo return message
            echo "OTP_SENT";
        }
        exit;
    }
    else if($_REQUEST['action'] == "doPayment")
	{
        
        
    }
	else if($_REQUEST['action'] == "doResetPassword")
	{
		//print_r($_REQUEST);
		if(md5(trim($_POST['reset_sec_code'])) != $_SESSION['securimage_code_value'])
		{
			echo "WRONG_PASSCODE";
			exit;
		}	
		if($_POST[reset_email] == '')
		{
			echo "Blank Email not allowed.";
			exit;
		}
		$result = $commonFunction->is_email_exist($_POST[reset_email]);
		if($result > 0)
		{
			//print_r($result);
			$customerDetails = $commonFunction->getSingleRow("SELECT * FROM bfsi_user WHERE login_id = '".$_POST[reset_email]."'");
			$customerDetails1 = $commonFunction->getSingleRow("SELECT * FROM bfsi_users_details WHERE fr_user_id = '".$customerDetails[pk_user_id]."'");
			
			$message = $commonFunction->readPHP("../__lib.mailFormats/forget_password_mail.html");
			
			$message = str_replace("USERNAME",$customerDetails1['name'],$message);
			$message = str_replace("ACTIVE_CODE_1","a=".$_POST['reset_email'],$message);
			$message = str_replace("ACTIVE_CODE_2","forget_password=".md5($_POST['reset_email']),$message);
			
			$db->db_run_query("INSERT INTO bfsi_verification SET email_id = '".$_POST['reset_email']."', fr_customer_id='".$customerDetails[pk_customer_id]."',auth_code='".md5($_POST['reset_email'])."',ip_address = '".$_SERVER['REMOTE_ADDR']."',verification_type='Forget', create_date=now()");
			
			$commonFunction->send_mail($_POST['reset_email'],"Password reset mail from taxsave",$message,true);

			echo "RESET_DONE";
            exit;
		}
		else
		{
			echo "RESET_PASSWORD_FAILED";
            exit;
		}		
	}
	else if($_REQUEST['action'] == "changePassword" && $_REQUEST['password'] != '' && $_REQUEST['user_id'] !='')
	{
		$db->db_run_query("UPDATE bfsi_user SET passwd = '".md5(trim($_REQUEST['password']))."' WHERE pk_user_id  = '".$_REQUEST[user_id]."'");
		echo "PASSWORD_CHANGED";
		exit;
	}
    else if($_REQUEST['action'] == "doChangePassword")
	{
        $email = trim($_POST['resetemail']);
        $code = $_POST['resetcode'];
        
        $customerDetails = $commonFunction->getSingleRow("SELECT auth_code FROM bfsi_verification WHERE email_id = '".$email."'");
        
        var_dump($customerDetails);
        if(($code!=md5($email))||($code!=$customerDetails['auth_code'])) {
            echo "CODE_MISMATCH";
            exit;
        }
        
		$db->db_run_query("UPDATE bfsi_user SET passwd = '".md5(trim($_POST['reset_passwd']))."' WHERE login_id = '".$email."'");
        $db->db_run_query("DELETE from bfsi_verification where email_id = '".$email."'");
		echo "PASSWORD_CHANGED";
		exit;
	}
    else if($_REQUEST['action'] == "contactus") {
        $message = "Mail from : " . $_POST['formname'] . "\nMail id : " . $_POST['formemail'] . "\n" . 
            "Mobile number : " . $_POST['formnumber'] . "\n\n" . 
            "Description : " . $_POST['formmessage'];
        
        //echo $message;
        $commonFunction->send_mail("support@taxsave.in","Contact us mail from taxsave",$message,true);
        echo "CONTACT_SENT";
        exit;
        
    }
?>