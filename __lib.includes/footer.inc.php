<footer class="footer">
<div class="container">
<div class="row">
    <div class="col-xs-12 col-sm-4 col-md-3">
        <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/footer-logo.png" alt="" width="180">
        <h4>Complex options simple solutions</h4>
        <div class="line"></div>
        <h4>A Venture of </h4>
        <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/footer-maintain.png" alt="" width="160">
    </div>

    <div class="col-xs-6 col-sm-3 col-md-2 brdr">
        <div class="footer-div">
            
        </div><!-- Footer Div -->
    </div>
    
    <div class="col-xs-6 col-sm-2 col-md-2 brdr">
        <div class="footer-div">
            <h3>Our Sevices</h3>
            <ul class="footer-ul">
                <a style="color:#cccccc" href="filetax.html">ITR Filing</a>
                <li>NRI Filing</li> 
                <a style="color:#cccccc" href="savetax.html">Mutual Funds</a> <br>  
                <a style="color:#cccccc" href="createwill.html">Will Creation</a>
                
            </ul>
        </div><!-- Footer Div -->
    </div>
    
    <div class="col-xs-12 col-sm-3 col-md-2 brdr">
        <div class="footer-div">
            <h3>Important Tools</h3>
            <ul class="footer-ul">
                <a style="color:#cccccc" href="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>docs/taxsave_rentreceipts.xlsm">Generate Rent Receipt</a>
                <a style="color:#cccccc" href="filetax.html#noticeassist">Notice Assistance</a>
                <a style="color:#cccccc" href="calculators.html">Calculators</a>
                <li class="ftr-height">Top Performing Funds</li>
            </ul>
        </div><!-- Footer Div -->
    </div>
    
    <div class="col-xs-12 col-sm-5 col-md-3 brdr">
        <div class="footer-div">
            <h3 class="less">Get In Touch</h3>
            <p>Stay connected with us in social media</p>
            
            <ul class="social-media">
                <li><a href="https://www.facebook.com/174591863258734" target="_blank"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/face.png" alt=""></a></li>
                <li><a href="https://twitter.com/DevMantraFS" target="_blank"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/twi.png" alt=""></a></li>
                <li><a href="https://devmantra2.blogspot.com" target="_blank"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/blogger.png" alt=""></a></li>
                <li><a href="www.linkedin.com/in/vikas-tatia-a48818a" target="_blank"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/linkedin.png" alt=""></a></li>
            </ul>
        </div><!-- Footer Div -->
    </div>
 
</div>
</div>

 
<div class="lower">
    � Copyrights 2018 | 
    <a style="color:#cccccc" href="home.html">TaxSave</a> | 
    <a style="color:#cccccc" href="about.html">About Us</a> |
    <a style="color:#cccccc" href="termsofuse.html">Terms & Conditions</a> |
    <a style="color:#cccccc" href="privacypolicy.html">Privacy policy</a>
</div>
    
 



	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script
		src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/bootstrap.min.js"></script>

	<script
		src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/jquery.js"></script>
	<script
		src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/jquery.easing.1.3.js"></script>
	<script
		src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/bootstrap.min.js"></script>
	<script
		src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/jquery.fancybox.pack.js"></script>
	<script
		src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/jquery.fancybox-media.js"></script>
	<script
		src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/jquery.flexslider.js"></script>
	<script
		src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/animate.js"></script>
	<script
		src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/modernizr.custom.js"></script>
	<script
		src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/jquery.isotope.min.js"></script>
	<script
		src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/jquery.magnific-popup.min.js"></script>
	<script
		src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/animate.js"></script>
	<script
		src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/custom.js"></script>
	<script src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>js/my.js"></script>	

<?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "wealth") { ?>
<script src="<?php echo $CONFIG->siteurl;?>__UI.assets/js/MyWealth.js"></script>
	<script type="text/javascript"
		src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
			google.charts.load('current', { 'packages': ['corechart'] });
			google.charts.setOnLoadCallback(drawChart);
			
			function drawChart() {
			
				var data = google.visualization.arrayToDataTable([
					['Amount', 'Toatal'],
					['Equity', 33],
					['Debt', 77]
				]);
			
				var options = {
					title: 'Investment allocation',
					left:20,
					top:0
				};
		
				var chart = new google.visualization.PieChart(document.getElementById('piechart'));
			
				chart.draw(data, options);
			}
		</script>
<?php } ?>  


    
<?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "uploadform") { ?>
<script
		src="<?php echo $CONFIG->siteurl;?>__UI.assets/plupload/js/plupload.full.js"></script>
<?php } ?>
<script
		src="<?php echo $CONFIG->siteurl;?>__UI.assets/js/js_function.php"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async
		src="https://www.googletagmanager.com/gtag/js?id=UA-39566008-4"></script>
	<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-39566008-4');
</script>

</footer><!-- Footer -->
 </body>
</html>