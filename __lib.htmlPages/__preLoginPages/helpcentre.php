<!-- Navbar -->
<nav class="navbar navbar-inverse my-nav">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand"
				href="<?php echo $CONFIG->siteurl;?>home.html"><img
				src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png"
				alt="Logo" width="160"></a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li>
				<li class="active"><a
					href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>login.html"
					class="nav-color">Login / Register</a></li>
			</ul>
			<ul class="nav navbar-nav brdr">
				<li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create
						Will</a></li>
			</ul>
		</div>
	</div>
</nav>
<!-- Navbar -->

<!-- Banner -->
<div class="banner_will">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
				<h1>
					<center>How can we help</center>
					<br>
					<center>
						<span class="span-end"></span>
					</center>
				</h1>
				<center>
					<span class="span-endline"></span>
				</center>
			</div>

		</div>
	</div>
</div>
<!-- Banner -->

<style>
.list-group-item {
	background-color: #c1c1c1;
	color: #fff;
	cursor: pointer;
	padding: 5px;
	width: 100%;
	border: none;
	text-align: left;
	outline: none;
	font-size: 12px;
	transition: 0.4s;
	border-radius: 4px;
}

.list-group-item:active {
	background-color: #8e6b37;
}

.accordion {
	background-color: #053002;
	color: #fff;
	cursor: pointer;
	padding: 5px;
	width: 100%;
	border: none;
	text-align: left;
	outline: none;
	font-size: 12px;
	transition: 0.4s;
	border-radius: 4px;
}

.accordion:active, .accordion:hover {
	background-color: #5b9b63;
}

.panel {
	padding: 0 18px;
	display: none;
	background-color: #eee;
	overflow: hidden;
	border-radius: 4px;
}

.panel img {
	left: 50px;
	position: relative;
	top: 5px;
	padding-bottom: 20px;
}

#myBtn {
	display: none;
	position: fixed;
	bottom: 20px;
	right: 30px;
	z-index: 99;
	font-size: 18px;
	border: none;
	outline: none;
	background-color: #777;
	color: white;
	cursor: pointer;
	padding: 15px;
	border-radius: 4px;
}

#myBtn:hover {
	background-color: #222;
}
</style>

<section id="content" class="welth-content">
	<div class="container-fluid">
		<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>

		<div class="site-panel">
			<div class="container-fluid">
				<div class="row"></div>
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-5">

						<h4 id="helphome">
							<center>Please select a topic you would like to know more about</center>
						</h4>
						<div class="list-group" id="topnavigationlist">

							<a href="#howtofileincometax" onclick="doalert(this);" class="list-group-item">How to file
								income tax</a> <a href="#paytaxonline"  onclick="doalert(this);"class="list-group-item">Pay
								Tax online</a> <a href="#viewform26as"  onclick="doalert(this);"class="list-group-item">How
								to view form 26AS</a> <a href="#refundstatus" onclick="doalert(this);"
								class="list-group-item">Knowing Refund Status</a> <a
								href="#everify"  onclick="doalert(this);"class="list-group-item">How to e-Verify</a> 
                            <a href="#file26qb"  onclick="doalert(this);"class="list-group-item">File 26 QB (TDS on Sale of Property)</a> 
                            <a href="#file26qc"  onclick="doalert(this);"class="list-group-item">File 26 QC (TDS on Rent of Property)</a> 
                            <a href="#mandatoryfile" onclick="doalert(this);"
								class="list-group-item">Who should mandatorily file their Income
								Tax Returns</a> <a href="#ifdontfiletax"  onclick="doalert(this);"class="list-group-item">What
								happens if we don't file the return within due date</a> <a
								href="#iftaxliability"  onclick="doalert(this);"class="list-group-item">Can you still
								file your Income Tax Return if there is any tax liability</a> 
                            <a href="#incomebelow"  onclick="doalert(this);"class="list-group-item">If my income is
								below the taxable limits, can I still file my Tax Returns</a>
                            <a href="#linkaadharpan"  onclick="doalert(this);"class="list-group-item">Steps to link Aadhaar with PAN</a>
                            <a href="#resetpasswordit"  onclick="doalert(this);"class="list-group-item">How to reset the password for Income Tax login</a>
                            <a href="#pendingrefunds"  onclick="doalert(this);"class="list-group-item"> How can I get pending refunds?</a>
                            <a href="#defectivenotice"  onclick="doalert(this);"class="list-group-item">I have recevied a defective Notice. What should I do?</a>
                            <a href="#othernotice"  onclick="doalert(this);"class="list-group-item">Answering notices and intimations</a>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h2 id="howtofileincometax">How to file income tax</h2>
						<button class="accordion">How to Register with Income Tax
							Department to filing of Income Tax Returns for the first time?</button>
						<div class="panel"  id="howtofileincometaxcontentsblock">
							<ul>
								<li>Open the Income tax e filing Website by clicking on the
									below link:
									<p>
										<a href="https://incometaxindiaefiling.gov.in/" target="_blank">https://incometaxindiaefiling.gov.in/</a>
									</p>
									<p>You will be taken to the Income tax portal; Click on
										Register Yourself which is on the right hand corner on the
										face of the website.</p>
								</li>
							</ul>
								<img
									src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/howtofiletax1.png">

							<ul>
								<li>You will be taken to the next page which shows you to select
									the user type :
									<p>Select Individual and click on Continue.</p>
								</li>
							</ul>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/howtofiletax2.png">
							<ul>
								<li>Enter Your Basic Details :
									<p>Only the fields marked with asterisk (*) are mandatory; all
										the details whether or not mandatory, should be as per the
										details of PAN card.</p>
									<p>Enter required details and click continue</p>
								</li>
							</ul>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/howtofiletax3.png">
							<ul>
								<li>By clicking continue you will be taken to next page :</li>
							</ul>
							<p>Here the registration details page should be displayed and you
								have to enter all the personal details:</p>
							<p>
								<em><u>Password details:</u></em>
							</p>
							<ul>
								<li>User ID will be automatically filled by the system, which is
									entered in the Basic details.</li>
								<li>Password should be between 8 &ndash; 14 characters and
									should contain one number one alphabet and at least one special
									character.</li>
								<li>Confirm password&ndash; Reenter the password entered above
									to confirm the password.</li>
								<li>Primary Secret Question/ Answer &ndash;select any one
									question from available in the drop down and enter the answer
									for the question in the below field.</li>
								<li>Secondary Secret Question/ Answer&ndash;select any one
									question from available in the drop down.</li>
							</ul>
							<p>Make sure that you remember your password or at least one of
								the secret question answers as they are useful for password
								reset if in case you forget the password.</p>
							<p>
								<em><u>Personal Details:</u></em>
							</p>
							<ul>
								<li>These details can be prefilled from the data entered at the
									time of entering basic information.</li>
							</ul>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/howtofiletax4.png">
							<ul>
								<li>
									<p>
										<em><u>Contact Details &amp; Current Address:</u></em>
									</p>
									<p>Enter all the details in fields marked with asterisk (*):</p>
									<ul>
										<li>Give the mobile number and mail ID which are active as you
											will get OTP for completing the registration process.</li>
										<li>Enable alerts and subscribing to mails will be
											automatically selected and this will be helpful to get alerts
											and mails form Income Tax India.</li>
									</ul>
								</li>
							</ul>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/howtofiletax5.png">

							<ul>
								<li>Finally click on Submit. You will be displayed with a
									message saying Registration is successful and a link has been
									sent to your mail along with a one time password (OTP) for your
									mobile given above.</li>
								<li>Click on the link given in mail you receive and you will be
									directed to Income Tax Department website where you have to
									enter the OTP received to your mobile.</li>
							</ul>
						</div>

						<button class="accordion">What if I have given wrong Mail ID or
							Mobile Number while registering?</button>
						<div class="panel">
							<ul>
								<li>Click on the link given below it will take you to Income Tax
									Website:</li>
							</ul>
							<p>
								<a
									href="https://incometaxindiaefiling.gov.in/e-Filing/UserLogin/LoginHome.html"  target="_blank">https://incometaxindiaefiling.gov.in/e-Filing/UserLogin/LoginHome.html</a>
							</p>
							<ul>
								<li>There you can see an option to resend activation link, click
									on the option available;</li>
							</ul>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/howtofiletax6.png">
							<ul>
								<li>You will be taken to next page where you are asked to select
									the user type select Individual from the drop down menu
									appearing;</li>
								<li>After selecting the User type you have to fill the details
									of your PAN, Date of Birth, Password, New Email ID, New Mobile
									number.</li>
								<li>You will be displayed with a message saying Registration is
									successful and a link has been sent to your mail along with a
									one time password (OTP) for the mobilenumber given above.</li>
								<li>Click on the link given in mail you received and you will be
									directed to Income Tax Department website where you have to
									enter the OTP received to your mobile.</li>
							</ul>
						</div>

						<button class="accordion">I am a Non- Resident User. I do not have
							a Mobile Number in India. How do I register in e-Filing?</button>
						<div class="panel">
							<p>For a Non-Resident, Mobile Number is not mandatory for
								registering in e-Filing portal. A valid email id will be
								sufficient to register. All the relevant verification PINs and
								intimations will be shared to such email id.</p>
						</div>

						<button class="accordion">I'm not able to register, as I get an
							error message &ldquo;Invalid PAN Details&rdquo;. What should I
							do?</button>
						<div class="panel">
							<p>
								It is possible that you're typing in the Name details
								differently from what you have mentioned while applying for PAN
								card. Please call Income Tax Departments toll-free number, <strong>1800
									180 1961</strong>and get the correct details and then try
								registering in the e-Filing application.
							</p>
						</div>
						<button class="accordion">What is tax deducted at source?</button>
						<div class="panel">
							<ul>
								<li>For quick and efficient collection of taxes, the Income-tax
									Law has incorporated a system of deduction of tax at the point
									of generation of income. This system is called as &ldquo;Tax
									Deducted at Source&rdquo;, commonly known as TDS.</li>
								<li>Under this system tax is deducted at the origin of the
									income. Tax is deducted by the payer and is remitted to the
									Government by the payer on behalf of the payee.</li>
								<li>The provisions of deduction of tax at source are applicable
									to several payments such as salary, interest, commission,
									brokerage, professional fees, royalty, contract payments, etc.</li>
								<li>In respect of payments to which the TDS provisions apply,
									the payer has to deduct tax at source on the payments made by
									him and he has to deposit the tax deducted by him to the credit
									of the Government.</li>
							</ul>
						</div>
						<button class="accordion">What is the difference between PAN and
							TAN?</button>
						<div class="panel">
							<ul>
								<li><strong></strong>PAN stands for Permanent Account Number and
									TAN stands for Tax Deduction Account Number. TAN is to be
									obtained by the person responsible to deduct tax, i.e., the
									deductor. In all the documents relating to TDS and all the
									correspondence with the Income-tax Department relating to TDS
									one has to quote his TAN.</li>
								<li>PAN cannot be used for TAN, hence, the deductor has to
									obtain TAN, even if he holds PAN.</li>
								<li>However, in case of TDS on purchase of land and building (as
									per section 194-IA), the deductor is not required to obtain TAN
									and can use PAN for remitting the TDS.</li>
							</ul>
						</div>
						<button class="accordion">What is the validity of PAN?</button>
						<div class="panel">
							<ul>
								<li><strong></strong>PAN obtained once is valid for life-time of
									the PAN-holder throughout India.</li>
								<li>It is not affected by change of address or change of
									Assessing Officer etc.</li>
								<li>However, any change in the PAN database (i.e. details
									provided at the time of obtaining PAN) should be intimated to
									the Income Tax Department by furnishing the details in the form
									for &ldquo;Request For New PAN Card Or/ And Changes or
									Correction in PAN Data&rdquo;.</li>
							</ul>
						</div>
						<button class="accordion">Can a person hold more than one PAN?</button>
						<div class="panel">
							<ul>
								<li>A person cannot hold more than one PAN. If a PAN is allotted
									to a person, then he cannot apply for obtaining another PAN. A
									penalty of Rs. 10,000/- is liable to be imposed under Section
									272B of the Income-tax Act, 1961 for having more than one PAN.</li>
								<li>If a person has been allotted more than one PAN then he
									should immediately surrender the additional PAN card(s).</li>
							</ul>
						</div>
						<button class="accordion">Is it mandatory to file return of income
							after getting PAN?/ Who are required to file Return?</button>
						<div class="panel">
							<ul>
								<li>It is not mandatory to file return of income after getting
									PAN.</li>
								<li>Return is to be filed only if you are liable to file return
									of income as under :
									<ul>
										<li>If your Gross total Income for financial Year 2017-18
											(Income before allowing Sec80 Deductions) is more than
											Rs.2,50,000/-. (The limit is Rs.3, 00,000/- for the persons
											of age between 60-80years and Rs.5, 00,000/- for the persons
											above 80 years age).</li>
										<li>If you wish to claim Refund of taxes deducted/ paid.</li>
										<li>If you wish to carry forward losses to be set off in the
											next years return, then you have to declare losses for the
											current year by filing return.</li>
										<li>If you are resident and having any asset outside India
											orhaving financial interest in any entity outside India then
											you have to file return of income even though you are not
											having any other incomes.</li>
										<li>Return filing is compulsory even if you being a resident
											have signing authority in a foreign account.</li>
										<li>In the above two points resident means only a resident and
											ordinary resident (ROR); it does not include resident but not
											ordinary resident (RNOR) or Non resident (NRI).</li>
										<li>If you are having any exempt long term capital gains of
											more than Rs.2, 50,000/- in a financial year then you have to
											file the return compulsorily even though such gains are
											exempt from tax.(Here exempted long term capital gains
											include sale of equity shares, sale of units in equity
											oriented mutual funds, sale of units of a business trust)-
											effective form FY 2016-17.</li>
										<li>If you are in receipt of any income derived from property
											held under a charitable or religious trust or a political
											party, educational institution, hospital, trade unions, any
											non profit organization, any authority, body or a trust etc.,
											you have to file the income tax return compulsorily.</li>
										<li>If you are planning to get any loan from bank or any other
											financial institution you may be asked copy of Income Tax
											Return.</li>
										<li>In respective of a firm or a company, Return of Income
											needs to be filed irrespective of the income/loss of the firm
											or company during the financial year.</li>
									</ul>
								</li>
							</ul>
						</div>
						<button class="accordion">If I lost my PAN card what should I do?</button>
						<div class="panel">
							<ul>
								<li>If the PAN card is lost then you can apply for duplicate PAN
									card by submitting the Form for "Request for new PAN Card or/
									and Changes or Correction in PAN Data" and a copy of FIR may be
									submitted along with the form.</li>
								<li>If the PAN card is lost and you don't remember your PAN,
									then in such a case, you can know you PAN by using the facility
									of "Know Your PAN" provided by the Income Tax Department. This
									facility can be availed of from the website of Income Tax
									Department - <a href="http://www.incometaxindia.gov.in" target="_blank">incometaxindia.gov.in</a>
								</li>
								<li>You can know your PAN online by providing the core details
									like Name, Father's Name and Date of Birth. After knowing the
									PAN you can apply for duplicate PAN card by submitting the
									"Request for new PAN Card or/ And Changes or Correction in PAN
									Data".</li>
							</ul>
						</div>
						<button class="accordion">How to reset the password for Income Tax
							login?</button>
						<div class="panel">
							<p>There are four ways to reset a password for a registered user:</p>
							<ol>
								<li>Answer secret Question</li>
								<li>Using OTP (PINs)</li>
								<li>Using Aadhaar OTP</li>
								<li>Using DSC</li>
							</ol>
							<p>For all the four Processes we have the following steps as
								common:</p>
							<ul>
								<li>Click on the link given which will take you to income tax
									portal: <a
									href="https://incometaxindiaefiling.gov.in/e-Filing/UserLogin/LoginHome.html" target="_blank">https://incometaxindiaefiling.gov.in/e-Filing/UserLogin/LoginHome.html</a>
								</li>
								<li>There you can see a link for forgot password, clink on the
									link;</li>
							</ul>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/howtofiletax7.png">
							<ul>
								<li>Give the PAN number for User ID; Enter Captcha code and
									mobile number; and click continue;</li>
								<li>Then you will be taken to next page select the appropriate
									option suitable for you and follow the steps given below;</li>
							</ul>
							<p>
								<strong>1. Answer secret Question:</strong>
							</p>
							<ul>
								<li>
									<p>If you select the option of Answering secret question you
										have to follow the below steps:</p>
									<ul>
										<li>Once the password has been changed a success message will
											be displayed. You can login with new password.</li>
										<li>On success, you will be asked to enter the New Password
											and confirm the password; Click on &ldquo;SUBMIT&rdquo;;</li>
										<li>Enter the &lsquo;Secret Answer&rsquo; and Click on
											&ldquo;Submit&rdquo;;</li>
										<li>Select the Secret Question from the drop down options
											available;</li>
										<li>Enter the Date of Birth (date of Incorporation in case of
											other than individual);</li>
										<li>Select &lsquo;Answer Secret Question&rsquo; from the drop
											down options available and click CONTINUE button;</li>
									</ul>
								</li>
							</ul>
							<p>
								<strong>2. Using OTP (PINs):</strong>
							</p>
							<ul>
								<li>
									<p>If you select the option of Using OTP(PINs) you have to
										follow the below steps:</p>
									<ul>
										<li>Select &lsquo;Using OTP (PINs)&rsquo; from the drop down
											options available and click on CONTINUE button;</li>
										<li>You must select one of the options mentioned below</li>
										<li>Registered Email ID and Mobile Number</li>
										<li>New Email ID and Mobile Number</li>
									</ul>
									<ul>
										<li>Let&rsquo;s go for the option of <em><u>Registered mail ID
													and Mobile number</u> first:</em></li>
										<li>You will have to select this option if you have access to
											registered mail and mobile. Click on &ldquo;VALIDATE&rdquo;;</li>
										<li>Enter the PINs received to the registered Email ID and
											Mobile Number and Click on &ldquo;VALIDATE&rdquo;.</li>
										<li>On success, you have to enter the New Password and confirm
											the password and Click on &ldquo;SUBMIT&rdquo;;</li>
										<li>Once the reset password request has been submitted, a
											success message will be displayed. You can login with new
											password after the time specified in Success message
											displayed.</li>
									</ul>
									<ul>
										<li>On success a message will be displayed, you can login with
											new password after the time specified in Success message
											displayed.</li>
										<li>Click on validate and you will get the PIN numbers to your
											new mail and mobile. Reset the password using the PINs.</li>
										<li><em><u>Bank Account Details</u></em>: For this option you
											have to enter the bank account number which you have given
											for filing of Return in the previous year.</li>
										<li><em><u>OLTAS CIN:</u></em> Use this option if you have
											paid Taxes in the previous years, you have to give the
											details of BSR code, Challan Date and Challan serial Number.</li>
										<li><em><u>26 AS TAN</u></em>: Use this option if TDS has been
											deducted during the current or previous financial year. You
											have to give the details of TAN of the deductor, total TDS
											deducted by him and the Assessment year in which TDS is
											deducted.</li>
										<li>Select this option if you don&rsquo;t have access to any
											one of your registered Mail ID or mobile number or both. Give
											the new mobile number and mail ID, however you have to
											validate your details by any of the three options available:</li>
										<li>Now if you opt to go by<em><u>New Email ID and Mobile
													Number:</u></em></li>
									</ul>
								</li>
							</ul>
							<p>
								<strong>3. Using Aadhaar OTP:</strong>
							</p>
							<ul>
								<li>Once the password has been changed a success message will be
									displayed. User can login with new password.</li>
								<li>Enter the OTP you receive to the mobile number registered
									with Aadhaar; Create New password and click submit;</li>
								<li>Select &lsquo;Using Aadhaar OTP&rsquo; from the drop down
									available and click on CONTINUE button; Click Generate Aadhaar
									OTP.</li>
								<li>This option will be useful only if you have linked your
									Aadhaar with PAN card;</li>
							</ul>
							<p>
								<strong>4. Using DSC:</strong>
							</p>
							<ul>
								<li>You have two options in this:
									<ul>
										<li>New DSC: This option will be useful when you have <em>not
												registered</em> your DSC before with Income tax Department
										</li>
										<li>Registered DSC: This option will be useful when you have <em>already
												registered</em> your DSC before with Income tax Department
										</li>
									</ul>
								</li>
								<li>You have to upload Signature File generated using DSC
									Management Utility and click on the &ldquo;VALIDATE&rdquo;
									button. The DSC is validated. Now you can reset the Password.</li>
								<li>Once the password has been changed a success message will be
									displayed. You can login with new password.</li>
							</ul>
						</div>
						<button class="accordion">How to Login to e-filing using Net
							Banking? / How to Change Password using Net Banking?</button>
						<div class="panel">
							<ul>
								<li>Click on the link given: <a
									href="https://incometaxindiaefiling.gov.in/e-Filing/UserLogin/LoginHome.html" target="_blank">https://incometaxindiaefiling.gov.in/e-Filing/UserLogin/LoginHome.html</a></li>
								<li>You will be directed to Income Tax portal; Here select the
									link- e-filing Login Trough Net Banking;</li>
							</ul>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/howtofiletax8.png">
							<ul>
								<li>Select the bank in which you have net banking facility from
									the list of banks displayed.</li>
								<li>After login to Net Banking account, click on the link "Login
									to the IT e-Filing account" e-Filing user Dashboard screen
									shall be displayed.</li>
								<li>To change the password go to the profile settings &gt;&gt;
									Change Password; here you can change the password.</li>
							</ul>
						</div>
						<button class="accordion">What is the last date for posting the
							ITR V signed Document?</button>
						<div class="panel">
							<ul>
								<li>You have to send the ITR V signed copy within 120 days from
									the date of filing the Income tax return. After the expiry of
									120 days the Return will be considered Invalid by the Income
									Tax department.</li>
							</ul>
						</div>
						<button class="accordion">What is the Address for sending the ITR
							V signed copy?</button>
						<div class="panel">
							<ul>
								<li>The ITR V Signed copy has to be sent Speed Post or Ordinary
									Post&nbsp;without Folding&nbsp;- at the following address:</li>
							</ul>
							<p style="padding-left: 120px;">
								<strong>Centralized Processing Centre (CPC),</strong>
							</p>
							<p style="padding-left: 120px;">
								<strong>Income Tax Department, </strong>
							</p>
							<p style="padding-left: 120px;">
								<strong>Bengaluru- 560500</strong>
							</p>
							<p style="padding-left: 120px;">
								<strong>Karnataka</strong>
							</p>
							<p style="padding-left: 120px;">&nbsp;</p>

						</div>
						<button class="accordion">What is the Password to open ITR V
							Document?</button>
						<div class="panel">
							<ul>
								<li>The password to open the ITR V Document is PAN number in
									small letters followed by date of birth in the format ddmmyyyy
									(e.g. If ABCDE1234F is the PAN number and 21-01-1990 then the
									password will be abcde1234f21011990).</li>
							</ul>
						</div>
						<button class="accordion">Can I send more than one ITR V documents
							in one Envelope?</button>
						<div class="panel">
							<ul>
								<li>You can send more than one ITR V in one envelope cover, but
									the thing you have to see is the barcode present on the ITRV
									copy should not be folded. So, you are suggested to send the
									document without folding to the department.</li>
							</ul>
						</div>
						<button class="accordion">What is the password to open Form 26AS?</button>
						<div class="panel">
							<ul>
								<li>The Password to open Form 26AS is your date of birth in the
									format- <em>ddmmyyy</em> (e.g. If your Date of birth is <strong><em>21-01-1990</em></strong>
									then the password to open 26 AS will be <strong><em>21011990</em></strong>)&nbsp;
								</li>
							</ul>
						</div>
						<button class="accordion">When we can get the refund?</button>
						<div class="panel">
							<ul>
								<li>Once the return is filed online and it is e-verified or ITR
									V is sent to the department, the return will be processed, you
									will generally get the refund amount within 2-6 months after
									filing the return of income.</li>
								<li>You can check the refund status by using the following link,
									giving the PAN number and relevant Assessment year: <a
									href="https://tin.tin.nsdl.com/oltas/refundstatuslogin.html" target="_blank">https://tin.tin.nsdl.com/oltas/refundstatuslogin.html</a>
								</li>
							</ul>
						</div>
						<button class="accordion">How to do e-verification?</button>
						<div class="panel">
							<p>Income Tax Return filed by the taxpayer is not treated as
								valid until it is verified by the taxpayer. In the existing
								process, taxpayer can verify the return using Digitally Signed
								Certificate (DSC) or by sending signed ITR-V to CPC. As per Rule
								12 vide Notification No. 41/2015, Income Tax department has
								introduced e-Verification of returns as an alternate for ITR-V.
								Taxpayers who are NOT mandated to use DSC are eligible for
								e-Verification.</p>
							<p>If the return has been e-verified then there is no need to
								send ITR V to CPC.</p>
							<p>You can e-verify your return using any of the following
								options:</p>
							<ol>
								<li>EVC received in Registered Mobile number and e-mail;
									(Electronic Verification Code (EVC) is a 10 digit alphanumeric
									code which can be generated through e-Filing portal and is
									valid for 72 hours).</li>
								<li>Aadhaar OTP;</li>
								<li>Login to e-Filing through Net Banking;</li>
								<li>EVC-Through Bank Account Number;</li>
								<li>EVC-Through Demat Account Number;</li>
								<li>EVC-Through Bank ATM.</li>
							</ol>
							<p>In case of return already filed, you can e verify the return
								by login to your e- filing portal &gt;&gt; My Account &gt;&gt;
								e-verify return</p>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/howtofiletax9.png">
							<p>After you click no the e-verify Return you will get four
								options:</p>
							<p>
								<strong><em><u>Option 1: &ldquo;I already have an EVC to
											e-Verify my return&rdquo;</u></em></strong>
							</p>
							<ul>
								<li>Select this option if you have received an EVC (Electronic
									Verification Code) at the time of e-filing the return, this EVC
									generated at the time of e-filing will be valid up to 72 Hours.</li>
								<li>Click on the link and enter the EVC click on submit; Success
									message will be displayed. No further action is required.</li>
							</ul>
							<p>
								<strong><em><span style="text-decoration: underline;">Option 2:
											&ldquo;I do not have an EVC and I would like to generate EVC
											to e-Verify my return&rdquo;</span></em></strong>
							</p>
							<p>Here in this option you will get three options:</p>
							<ul>
								<li>Through Net Banking</li>
								<li>Through Bank Account Number</li>
								<li>Through Demat Account Number</li>
							</ul>
							<p style="padding-left: 30px;">
								<strong><em><u>Through Net Banking:</u></em></strong>
							</p>
							<ul>
								<li>By clicking on this you will be displayed with steps ,click
									on continue, select the bank from the list of banks available</li>
								<li>Login to net banking and e filing portal, the screen will
									display the returns pending for e verification; click on
									e-verify and the returns will be e-verified automatically</li>
								<li>Success message will be displayed. No further action is
									required.</li>
							</ul>
							<p style="padding-left: 30px;">
								<strong><em><u>Through Bank Account Number:</u></em></strong>
							</p>
							<ul>
								<li>For this step your bank account must be pre-validated before
									you start to e verify the return.</li>
								<li>If the account is Pre-validated then a message will be
									displayed asking to generate EVC click yes;</li>
								<li>You will receive an EVC message to your mobile registered
									wit bank account</li>
								<li>Enter the EVC in the box displayed and click submit;</li>
								<li>Success message will be displayed. No further action is
									required.</li>
							</ul>
							<p style="padding-left: 30px;">
								<strong><em><u>Through Demat Account Number:</u></em></strong>
							</p>
							<ul>
								<li>For this step your demat account must be pre-validated
									before you start to e verify the return.</li>
								<li>You will receive an EVC message to your mobile registered
									with Demat account</li>
								<li>Enter the EVC in the box displayed and click submit;</li>
								<li>Success message will be displayed. No further action is
									required.</li>
							</ul>
							<p>
								<strong><em><u>Option 3: Generate Aadhaar OTP:</u></em></strong>
							</p>
							<ul>
								<li>To generate Aadhaar OTP, PAN and Aadhaar must be linked.</li>
								<li>By clicking on the link you will displayed with a Text box.
									Enter Aadhaar OTP in the text box provided and click on Submit.</li>
								<li>Success message will be displayed. No further action is
									required.</li>
							</ul>
							<p>
								<strong><em><u>Generate EVC through Bank ATM:</u></em></strong>
							</p>
							<ul>
								<li>This is a different method of doing e-verification, as one
									has to go to the bank ATM</li>
								<li>After swiping the ATM card and entering ATM PIN, you are
									provided with an extra option of &ldquo;PIN FOR INCOME TAX
									FILING&rdquo;</li>
								<li>EVC will be sent to the registered mobile number and e-mail
									ID after selecting the option.</li>
								<li>After you get the EVC login to e-filing to portal &gt;&gt;My
									account &gt;&gt;E-Verify return &gt;&gt; Enter EVC.</li>
								<li>Success message will be displayed. No further action is
									required.</li>
							</ul>
						</div>
						<button class="accordion">Who are liable to pay advance tax?</button>
						<div class="panel">
							<p>Every person whose estimated tax liability for the year is Rs.
								10,000 or more, shall pay his tax in advance, in the form of
								&ldquo;advance tax&rdquo;</p>
						</div>
						<button class="accordion">Exemptions in Advance tax Payment:</button>
						<div class="panel">
							<p>A resident senior citizen (i.e., an individual of the age of
								60 years or above during the relevant financial year) if he is
								not having any income from business or profession is not liable
								to pay advance tax.</p>
						</div>
						<button class="accordion">What are the Due dates for payment of
							advance tax?</button>
						<div class="panel">
							<p>Advance tax is to be paid in different instalments. The due
								dates for payment of different instalments of advance tax are as
								follows:</p>
							<table>
								<tbody>
									<tr>
										<td width="187">
											<p>
												<strong>Assessee</strong>
											</p>
										</td>
										<td width="114">
											<p>
												<strong>By 15<sup>th</sup> June
												</strong>
											</p>
										</td>
										<td width="114">
											<p>
												<strong>By 15<sup>th</sup> September
												</strong>
											</p>
										</td>
										<td width="118">
											<p>
												<strong>By 15th December</strong>
											</p>
										</td>
										<td width="122">
											<p>
												<strong>By 15<sup>th</sup> March
												</strong>
											</p>
										</td>
									</tr>
									<tr>
										<td width="187">
											<p>
												<strong>All assesses</strong>
											</p>
											<p>
												<strong>(except those who opted for presumptive taxation)</strong>
											</p>
										</td>
										<td width="114">
											<p>Up to 15% of advance tax</p>
										</td>
										<td width="114">
											<p>Up to 45% of advance tax</p>
										</td>
										<td width="118">
											<p>Up to 75% of advance tax</p>
										</td>
										<td width="122">
											<p>Up to 100% of advance tax</p>
										</td>
									</tr>
									<tr>
										<td width="187">
											<p>
												<strong>Taxpayers who opted for presumptive taxation</strong>
											</p>
										</td>
										<td width="114">
											<p>Nil</p>
										</td>
										<td width="114">
											<p>Nil</p>
										</td>
										<td width="118">
											<p>Nil</p>
										</td>
										<td width="122">
											<p>Up to 100% of</p>
											<p>Advance tax</p>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<button class="accordion">What is the mode of payment of advance
							tax?</button>
						<div class="panel">
							<p>A company and a person whose are required to get audited shall
								pay taxes only through the electronic payment mode using the
								internet banking facility of the authorized banks.</p>
							<p>Any other taxpayer can pay tax either by electronic mode or by
								physical mode by depositing the challan at the receiving bank.</p>
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>

					<div class="col-md-12">
						<h2 id="paytaxonline">Pay Tax online</h2>
						<button class="accordion">How to pay tax online</button>
						<div class="panel" id="paytaxonlinecontentsblock">
							<p>This Payment procedure is for payment of taxes online</p>
							<p>
								<strong>Step 1:</strong>
							</p>
							<p>
								Click on this link <a
									href="https://onlineservices.tin.egov-nsdl.com/etaxnew/tdsnontds.jsp" target="_blank">https://onlineservices.tin.egov-nsdl.com/etaxnew/tdsnontds.jsp</a>
								(Here you will be taken to government website)
							</p>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/view26AS1.png">
							<p>
								<strong>Step 2:</strong>
							</p>
							<p>
								Click on the <strong>CHALLAN NO./ ITNS 280</strong>
							</p>
							<p>You will be displayed the following screen, fill in the
								details as required and click on proceed</p>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/view26AS2.png">
							<p>
								<strong>Step 3:</strong>
							</p>
							<p>After clicking on proceed verify the details as shown on the
								screen and fill in the details of taxes to be paid and proceed
								for payment</p>
							<p>
								<strong>Step 4:</strong>
							</p>
							<p>After payment download the challan (OR) make the print screen
								picture of challan details.</p>
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>

					<div class="col-md-12">
						<h2 id="viewform26as">How to view form 26AS</h2>
						<button class="accordion">How to view form 26AS</button>
						<div class="panel" id="viewform26ascontentsblock">
							<p>The following details have been provided in 26AS statement:</p>
							<ul>
								<li>Advance tax, Self-Assessment Tax and Regular Assessment Tax
									paid by self</li>
								<li>Tax paid through Tax Deducted at Source (TDS) or TCS on
									behalf of users own presence</li>
								<li>Refund issued by the Department to self</li>
								<li>Information received from various agencies on high value
									transaction carried by self.</li>
							</ul>
							<p>This statement is presented yearly, which reflects the
								transaction of the concerned year.&nbsp;</p>
							<p>
								<br /> Perform the following steps to view or download the form:
							</p>
							<p>
								<strong>Step 1: </strong>
							</p>
							<p>
								Logon&nbsp;to the&nbsp;<strong>'e-Filing'</strong>&nbsp;Portal<a
									href="https://www.incometaxindiaefiling.gov.in/" target="_blank">&nbsp;www.incometaxindiaefiling.gov.in</a>
							</p>
							<p>
								<strong>Step2: </strong>
							</p>
							<p>
								Go to the&nbsp;<strong>'My Account'</strong>&nbsp;menu located
								at upper-left side of the page &rArr; Click&nbsp;<strong>'View
									Form 26 AS (Tax Credit)'</strong>, User is redirected to&nbsp;<strong>TDS-CPC
									Portal</strong>
							</p>
							<p>
								<strong>Step 3:</strong>
							</p>
							<p>
								View the disclaimer &rArr; Click<strong>&nbsp;'Confirm'</strong>&nbsp;&rArr;
								Agree the acceptance of usage &rArr; Click&nbsp;<strong>'Proceed'</strong>
							</p>
							<p>
								<strong>Step 4:</strong>
							</p>
							<p>
								Click&nbsp;<strong>'View Tax Credit (Form 26AS)'</strong>
							</p>
							<p>
								<strong>Step 5:</strong>
							</p>
							<p>
								Select the&nbsp;<strong>'Assessment Year'</strong>&nbsp;and&nbsp;<strong>'View
									type'</strong>&nbsp;(HTML or Text ).
							</p>
							<p>
								<strong>Step 6:</strong>
							</p>
							<p>For downloading PDF file, first view as HTML and then click on
								export to PDF.</p>
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>

					<div class="col-md-12">
						<h2 id="refundstatus">Knowing Refund Status</h2>
						<button class="accordion">How to check Refund Status?</button>
						<div class="panel" id="refundstatuscontentsblock">
							<p>
								<strong>Step 1:</strong>
							</p>
							<p>
								Click on this link <a
									href="https://tin.tin.nsdl.com/oltas/refundstatuslogin.html" target="_blank">https://tin.tin.nsdl.com/oltas/refundstatuslogin.html</a>,
								this will take you to NSDL website.
							</p>
							<p>
								<strong>Step 2: </strong>
							</p>
							<p>Enter your PAN; Select the Assessment Year for which you want
								to check the refund status&nbsp; and enter the text as shown in
								the image</p>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/refundstatus1.png">
							<p>
								<strong>Step 3: </strong>
							</p>
							<p>If the refund is processed by the Assessing Officer the status
								of the refund will be shown, if the refund is not processed then
								this below screen will be displayed that means you have to wait
								for some more time.</p>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/refundstatus2.png">
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>

					<div class="col-md-12">
						<h2 id="everify">How to e-Verify</h2>
						<button class="accordion">How to e-verify the Return?</button>
						<div class="panel" id="everifycontentsblock">
							<p>Income Tax Return filed by the taxpayer is not treated as
								valid until it is verified by the taxpayer. In the existing
								process, taxpayer can verify the return using Digitally Signed
								Certificate (DSC) or by sending signed ITR-V to CPC. As per Rule
								12 vide Notification No. 41/2015, Income Tax department has
								introduced e-Verification of returns as an alternate for ITR-V.
								Taxpayers who are NOT mandated to use DSC are eligible for
								e-Verification.</p>
							<p>If the return has been e-verified then there is no need to
								send ITR V to CPC.</p>
							<p>You can e-verify your return using any of the following
								options:</p>
							<ol>
								<li>EVC received in Registered Mobile number and
									e-mail;(Electronic Verification Code (EVC) is a 10 digit
									alphanumeric code which can be generated through e-Filing
									portal and is valid for 72 hours).</li>
								<li>Aadhaar OTP;</li>
								<li>Login to e-Filing through Net Banking;</li>
								<li>EVC-Through Bank Account Number;</li>
								<li>EVC-Through Demat Account Number;</li>
								<li>EVC-Through Bank ATM.</li>
							</ol>
							<p>In case of return already filed, you can e verify the return
								by login to your e- filing portal &gt;&gt; My Account &gt;&gt;
								e-verify return</p>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/everifyreturn1.png">
							<p>After you click no the e-verify Return you will get four
								options:</p>
							<p>
								<strong><em><u>Option 1: &ldquo;I already have an EVC to
											e-Verify my return&rdquo;</u></em></strong>
							</p>
							<ul>
								<li>Select this option if you have received an EVC (Electronic
									Verification Code) at the time of e-filing the return, this EVC
									generated at the time of e-filing will be valid up to 72 Hours.</li>
								<li>Click on the link and enter the EVC click on submit; Success
									message will be displayed. No further action is required.</li>
							</ul>
							<p>
								<span style="text-decoration: underline;"><em><strong>Option 2:
											&ldquo;I do not have an EVC and I would like to generate EVC
											to e-Verify my return&rdquo;</strong></em></span>
							</p>
							<p>Here in this option you will get three options:</p>
							<ul>
								<li>Through Net Banking</li>
								<li>Through Bank Account Number</li>
								<li>Through Demat Account Number</li>
							</ul>
							<p style="padding-left: 30px;">
								<strong><em><u>Through Net Banking:</u></em></strong>
							</p>
							<ul>
								<li>By clicking on this you will be displayed with steps ,click
									on continue, select the bank from the list of banks available</li>
								<li>Login to net banking and e filing portal, the screen will
									display the returns pending for e verification; click on
									e-verify and the returns will be e-verified automatically</li>
								<li>Success message will be displayed. No further action is
									required.</li>
							</ul>
							<p style="padding-left: 30px;">
								<strong><em><u>Through Bank Account Number:</u></em></strong>
							</p>
							<ul>
								<li>For this step your bank account must be pre-validated before
									you start to e verify the return.</li>
								<li>If the account is Pre-validated then a message will be
									displayed asking to generate EVC click yes;</li>
								<li>You will receive an EVC message to your mobile registered
									wit bank account</li>
								<li>Enter the EVC in the box displayed and click submit;</li>
								<li>Success message will be displayed. No further action is
									required.</li>
							</ul>
							<p style="padding-left: 30px;">
								<strong><em><u>Through Demat Account Number:</u></em></strong>
							</p>
							<ul>
								<li>For this step your demat account must be pre-validated
									before you start to e verify the return.</li>
								<li>You will receive an EVC message to your mobile registered
									with Demat account</li>
								<li>Enter the EVC in the box displayed and click submit;</li>
								<li>Success message will be displayed. No further action is
									required.</li>
							</ul>
							<p>
								<strong><em><u>Option 3: Generate Aadhaar OTP:</u></em></strong>
							</p>
							<ul>
								<li>To generate Aadhaar OTP, PAN and Aadhaar must be linked.</li>
								<li>By clicking on the link you will displayed with a Text box.
									Enter Aadhaar OTP in the text box provided and click on Submit.</li>
								<li>Success message will be displayed. No further action is
									required.</li>
							</ul>
							<p>
								<strong><em><u>Generate EVC through Bank ATM:</u></em></strong>
							</p>
							<ul>
								<li>This is a different method of doing e-verification, as one
									has to go to the bank ATM</li>
								<li>After swiping the ATM card and entering ATM PIN, you are
									provided with an extra option of &ldquo;PIN FOR INCOME TAX
									FILING&rdquo;</li>
								<li>EVC will be sent to the registered mobile number and e-mail
									ID after selecting the option.</li>
								<li>After you get the EVC login to e-filing to portal &gt;&gt;My
									account &gt;&gt;E-Verify return &gt;&gt; Enter EVC.</li>
								<li>Success message will be displayed. No further action is
									required.</li>
							</ul>

						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>

					<div class="col-md-12">
						<h2 id="file26qb">File 26 QB (TDS on Sale of Property)</h2>
						<button class="accordion">What is TDS on Sale of Property and when
							this should be deducted?</button>
						<div class="panel" id="file26qbcontentsblock">
							<ul>
								<li>As per Finance Bill of 2013, TDS is applicable on sale of
									immoveable property where in the sale consideration of the
									property exceeds or is equal to Rs.50,00,000 (Rupees Fifty
									Lakhs).</li>
								<li>Sec 194 IA of the Income Tax Act, 1961 states that for all
									transactions with effect from June 1, 2013, Tax @ 1% should be
									deducted by the purchaser (Buyer) of the property at the time
									of making payment of sale consideration.</li>
							</ul>
						</div>
						<button class="accordion">Some important points</button>
						<div class="panel">
							<ul>
								<li>Deduct tax @ 1% from the sale consideration</li>
								<li><strong><em>TDS of 1% should be deducted on amount <u>excluding
												of taxes. </u></em></strong></li>
								<li>As per the CBDT notification no. 30/2016 dated April 29,
									2016, the due date of payment of TDS on transfer of immovable
									property is <strong><u>thirty days</u></strong> from the end of
									the month in which the deduction is made.
								</li>
								<li>Due date of filing will be by 7th of the next month of the
									month for which transaction is reported. If filing has not been
									done with in time then Late filing fee u/s 234E will be charged
									@ Rs.200/- per day.</li>
								<li>Due date of TDS deposit will be by 7th of the next month of
									the month for which transaction is reported. If TDS is not
									deposited with in time then Late Payment Interest u/s 200A and
									&nbsp;u/s 154 read with Sec 201(1A) will be charged @1.5% per
									month on TDS amount.</li>
								<li>Do not commit any error in quoting the PAN or other details
									in the online Form as there is no online mechanism for
									rectification of errors. For the purpose of rectification you
									are required to contact Income Tax Department.</li>
								<li>While completing the Online form, please note the following
									points:-
									<ul>
										<li>Fields marked with * are mandatory</li>
										<li>&nbsp;Do not enter double quotes (&ldquo; ") in any of the
											fields.</li>
									</ul>
								</li>
								<li>TDS amount as per Form 26QB should be entered in the field
									&lsquo;Basic Tax&rsquo; (Income Tax) on the Bank&rsquo;s
									web-portal</li>
								<li>TDS certificate (Form 16B) will be based on &ldquo;Basic
									Tax&rdquo; (Income Tax) only</li>
								<li>Taxpayer/ Buyer are advised to save the Acknowledgement
									Number for downloading the Form 16B from TRACES website</li>
								<li>TDS certificate (Form 16B) will be available for download
									from the TRACES website after at least 2 days of deposit of tax
									amount at the respective Bank.</li>
								<li>Click on View Acknowledgment Number under TDS on Property by
									providing the relevant details for retrieving the
									Acknowledgment Number</li>
							</ul>
							<p>
								&nbsp;Link - <a
									href="https://www.tin-nsdl.com/faqs/tds-on-sale-of-property/TDS-Introduction.html" target="_blank">https://www.tin-nsdl.com/faqs/tds-on-sale-of-property/TDS-Introduction.html</a>
							</p>
						</div>
						<button class="accordion">Procedure for Filing of Form 26QB</button>
						<div class="panel">
							<p>
								<strong><u>STEP -1 </u></strong>
							</p>
							<p>By clicking on the link, the below page will be displayed.
								Click on Form 26QB as highlighted in the image.</p>
							<p>
								Link - <a
									href="https://onlineservices.tin.egov-nsdl.com/etaxnew/tdsnontds.jsp" target="_blank">https://onlineservices.tin.egov-nsdl.com/etaxnew/tdsnontds.jsp</a>
							</p>
							<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qb1.png">
							<p>
								<strong><u>STEP -2</u></strong>
							</p>
							<p>By selecting the Form 26QB the following page will be
								displayed, fill all the mandatory fields and additional fields
								as required with help of hints given.</p>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qb2.png">
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qb3.png">
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qb4.png">
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qb5.png">
							<p>After filling all the details click on proceed, it will
								navigate to confirmation page. Once you had confirmed all the
								details 26QB will be generated and take print of the Form 26QB.
								Regarding mode of payment of taxes has been mentioned below.</p>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qb6.png">
							<p>
								<strong><u>Mode of Payment</u></strong> &ndash; There are two
								options available for payment
							</p>
							<ol>
								<li>e-tax payment immediately</li>
								<li>e-payment on subsequent date</li>
							</ol>
							<p>
								<strong><u>1. e-tax payment immediately (Online mode) : </u></strong>It
								is an online mode where you can make payment immediately. In
								this mode you will get two options for making payment &ndash;
							</p>
							<p>
								i) <em><u>Payment through Net Banking</u></em> &ndash; If you
								select payment through Net banking option at the time of
								payment, it will navigate to your respective banking site. The
								following is the list of banks accepted for Net Banking facility
								-
							</p>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qb7.png">
							<p>
								ii) <em><u>Payment through Debit card</u></em> -&nbsp; The
								following is the list of banks accepted for Debit card paymenet
							</p>
							<table style="height: 101px; width: 697px;" border="1">
<tbody>
<tr>
<td style="width: 675px;" colspan="5">
<p><strong>List of Banks for Payment through Debit Card</strong></p>
</td>
</tr>
<tr>
<td style="width: 119px;">
<p>1.&nbsp;HDFC Bank</p>
</td>
<td style="width: 114px;">
<p>2. ICICI Bank</p>
</td>
<td style="width: 150px;">
<p>3.&nbsp;Indian Bank</p>
</td>
<td style="width: 149px;">
<p>4. Punjab National Bank</p>
</td>
<td style="width: 143px;">
<p>5.&nbsp;State Bank of India</p>
</td>
</tr>
</tbody>
</table>
							<p>
								<strong><u>2. e-payment on subsequent date : </u></strong>Once
								you had selected this option, you will get a Acknowldement
								receipt after confirming the details.&nbsp;&nbsp; There will be
								two options available once you select the e-payment on
								subsequent date:
							</p>
							<p>
								<strong>OPTION 1 :</strong>- <strong><em><u>e-Payment of taxes
											by visiting any of the Bank </u></em></strong>- Take the
								Acknowledgment receipt to any of the authorized bank branches,
								Bank branches would use the URL for e-Tax&nbsp;payment on
								subsequent date to re-enter the required details as provided in
								the acknowledgment receipt. &nbsp;On completion of the above,
								the Bank will make the payment&nbsp;through its net banking
								facility and provide you the challan counterfoil as
								acknowledgment for payment of taxes.
							</p>
							<p>
								<strong>OPTION 2 :- </strong>&nbsp;<strong><em><u>E-tax payment
											on Subsequent Date</u></em></strong><strong>&nbsp; - </strong>&nbsp;In
								case you desire to make the payment through e-tax payment (net
								banking account) subsequently, you may access the link of
								&lsquo;E-tax payment on subsequent date&rsquo; given below. For
								making the payment you have to give the details in your
								acknowledgment receipt.&nbsp; &nbsp;
							</p>
							<p>
								<strong>Link -</strong> <a
									href="https://onlineservices.tin.egov-nsdl.com/etaxnew/PopServletOffline" target="_blank">https://onlineservices.tin.egov-nsdl.com/etaxnew/PopServletOffline</a>
							</p>
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>
                    
                    <div class="col-md-12">
						<h2 id="file26qc">File 26 QC (TDS on Rent of Property)</h2>
						<button class="accordion">What is TDS on Rent of Property and who is reasonable to deduct?</button>
						<div class="panel" id="file26qccontentsblock">
							<p>As per Finance Act, 2017, &ldquo;TDS on Rent&rdquo; under section 194-IB is liable to be deducted by Individuals or HUFs (other than an individual who are not liable for Tax Audit u/s 44AB) who is paying to a resident monthly rent exceeding Rs.50,000. Section 194-IB of the Income Tax Act, 1961 states that for all the transactions with effect from June 1, 2017, tax @5% should be deducted by the Tenant/ Lessee/ Payer of the property at the time of making payment of rent (to Landlord / Lessor/ Payee). Tax so deducted should be deposited to the Government Account through any of the authorized bank branches.</p>
<p><strong><u>Applicability:</u></strong></p>
<p>Individual or HUF who are not liable for tax audit under section 44AB.</p>
<p><strong><u>Limit &amp; Rate</u></strong><u>:</u></p>
<p>Rent of Rs. 50,000 for a month or part of the month @ 5%.</p>
<p>Note - <strong>This section does not applies to those rent which are covered under Section 194-I.</strong></p>
<p><strong><u>Highlights of Section 194-IB</u></strong></p>
<p><strong>No need to obtain TAN</strong> - Under this section, Tax payer need not to obtain TAN.</p>
<p><strong>Deduction, once in a year</strong> - Under this section, Tax payer is required to deduct tax at source only once in a financial.</p>
<p><strong>Time of deduction</strong> - The tax should be deducted at the time of credit or payment (whichever is earlier) of rent for the last month of the tax year or last month of tenancy if the property is vacated during the year, as applicable.</p>
<p><strong><em><u>Example</u></em></strong> &ndash; Mr A is a tenant of a property for which he is paying rent of Rs.60,000/- per Month and Mr A is not liable to tax audit u/s 44AB. Mr A is residing in the same property for whole Financial year 2018-19(April&rsquo;18 to March&rsquo;19).</p>
<p>In the present case Mr A has to deduct TDS @ 5% while paying the rent for the month of March&rsquo;19. The total amount of TDS to be deducted is Rs.7,20,000(60,000*12 = 7,20,000) X 5% is Rs.36,000/- .</p>
<p>If the Rent/Lease agreement has been completed in between the Financial year, TDS should be deducted in last month of tenancy if the property is vacated during the year.</p>
<p>&nbsp;</p>
<p><strong><u>Comparison between 194-IA &amp; 194-IB</u></strong></p>
<table border="1">
<tbody>
<tr>
<td style="text-align: center;" width="205">
<p><strong><u>Basis</u></strong></p>
</td>
<td style="text-align: center;" width="205">
<p><strong><u>194 - IA</u></strong></p>
</td>
<td style="text-align: center;" width="205">
<p><strong>194&nbsp;&nbsp; </strong><strong><u>- IB</u></strong></p>
</td>
</tr>
<tr>
<td width="205">
<p>&nbsp;</p>
<p>Applicability</p>
</td>
<td width="205">
<p>i) Every person paying rent (except individual &amp; HUF)</p>
<p>ii) Individual &amp; HUF if doing business and covered in tax audit</p>
</td>
<td width="205">
<p>Every Individual &amp; HUF not covered in 194-I</p>
</td>
</tr>
<tr>
<td width="205">
<p>&nbsp;</p>
<p>Limit</p>
</td>
<td width="205">
<p>&nbsp;</p>
<p>Rs. 1,80,000 Per annum</p>
</td>
<td width="205">
<p>Rs. 50,000 per month or part of the month during financial year</p>
</td>
</tr>
<tr>
<td width="205">
<p style="text-align: left;">TDS Rate</p>
</td>
<td width="205">
<p>10 %</p>
</td>
<td width="205">
<p>5 %</p>
</td>
</tr>
<tr>
<td width="205">
<p>TAN requirement</p>
</td>
<td width="205">
<p>TAN is required</p>
</td>
<td width="205">
<p>TAN is not required, PAN is enough.</p>
</td>
</tr>
</tbody>
</table>
						</div>
						<button class="accordion">Some important points</button>
						<div class="panel">
							<ol>
<li>All individuals or HUFs (except those liable to audit under clause a and b of section 44AB) paying monthly rent to a resident in excess of Rs. 50,000 are liable to deduct TDS under section 194-IB</li>
<li>Deduct tax @ 5 % from the rent payment made to the Landlord.</li>
<li>Collect the Permanent Account Number (PAN) of the Landlord and verify the same with the Original PAN card.</li>
<li>PAN of the Landlord as well as Tenant should be mandatorily furnished in the online Form for furnishing information regarding the rent.</li>
<li>Do not commit any error in quoting the PAN or other details in the online Form. For the purpose of error rectification you have to contact Income Tax Department.</li>
<li>Download and furnish TDS certificate in Form 16C from TRACES and issue to the Landlord / Lessor/ Payee within 15 days from the due date of furnishing of the challan-cum-statement in Form 26QC.</li>
<li>If the Landlord / Lessor/ Payee is a non-resident, liability to deduct TDS arises under section 195 of the Income-tax Act, 1961</li>
</ol>
						</div>
                        <button class="accordion">Procedure for Filing of Form 26QC</button>
						<div class="panel">
							<p><strong>STEP -1</strong></p>
<p>By clicking on the link, the below page will be displayed. Click on Form 26QC as highlighted in the image.</p>
<p>Link - <a href="https://onlineservices.tin.egov-nsdl.com/etaxnew/tdsnontds.jsp" target="_blank">https://onlineservices.tin.egov-nsdl.com/etaxnew/tdsnontds.jsp</a></p>
                            <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qc1.png">
                            
                            <p><strong>STEP -2 </strong></p>
<p>By selecting the Form 26QC the following page will be displayed, fill all the mandatory fields and additional fields as required with help of hints given.</p>
                            <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qc2.png">
                            <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qc3.png">
                            <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qc4.png">
                            <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qc5.png">
                            
                            <p>After filling all the details click on proceed, it will navigate to confirmation page. Once you had confirmed all the details 26QC will be generated and take print of the Form 26QC. Regarding mode of payment of taxes has been mentioned below.</p>
                            <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qc6.png">
                            
                            <p>
								<strong><u>Mode of Payment</u></strong> &ndash; There are two
								options available for payment
							</p>
							<ol>
								<li>e-tax payment immediately</li>
								<li>e-payment on subsequent date</li>
							</ol>
							<p>
								<strong><u>1. e-tax payment immediately (Online mode) : </u></strong>It
								is an online mode where you can make payment immediately. In
								this mode you will get two options for making payment &ndash;
							</p>
							<p>
								i) <em><u>Payment through Net Banking</u></em> &ndash; If you
								select payment through Net banking option at the time of
								payment, it will navigate to your respective banking site. The
								following is the list of banks accepted for Net Banking facility
								-
							</p>
							<img
								src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/form26qc7.png">
							<p>
								ii) <em><u>Payment through Debit card</u></em> -&nbsp; The
								following is the list of banks accepted for Debit card paymenet
							</p>
							<table style="height: 101px; width: 697px;" border="1">
<tbody>
<tr>
<td style="width: 675px;" colspan="5">
<p><strong>List of Banks for Payment through Debit Card</strong></p>
</td>
</tr>
<tr>
<td style="width: 119px;">
<p>1.&nbsp;HDFC Bank</p>
</td>
<td style="width: 114px;">
<p>2. ICICI Bank</p>
</td>
<td style="width: 150px;">
<p>3.&nbsp;Indian Bank</p>
</td>
<td style="width: 149px;">
<p>4. Punjab National Bank</p>
</td>
<td style="width: 143px;">
<p>5.&nbsp;State Bank of India</p>
</td>
</tr>
</tbody>
</table>
							<p>
								<strong><u>2. e-payment on subsequent date : </u></strong>Once
								you had selected this option, you will get a Acknowldement
								receipt after confirming the details.&nbsp;&nbsp; There will be
								two options available once you select the e-payment on
								subsequent date:
							</p>
							<p>
								<strong>OPTION 1 :</strong>- <strong><em><u>e-Payment of taxes
											by visiting any of the Bank </u></em></strong>- Take the
								Acknowledgment receipt to any of the authorized bank branches,
								Bank branches would use the URL for e-Tax&nbsp;payment on
								subsequent date to re-enter the required details as provided in
								the acknowledgment receipt. &nbsp;On completion of the above,
								the Bank will make the payment&nbsp;through its net banking
								facility and provide you the challan counterfoil as
								acknowledgment for payment of taxes.
							</p>
							<p>
								<strong>OPTION 2 :- </strong>&nbsp;<strong><em><u>E-tax payment
											on Subsequent Date</u></em></strong><strong>&nbsp; - </strong>&nbsp;In
								case you desire to make the payment through e-tax payment (net
								banking account) subsequently, you may access the link of
								&lsquo;E-tax payment on subsequent date&rsquo; given below. For
								making the payment you have to give the details in your
								acknowledgment receipt.&nbsp; &nbsp;
							</p>
							<p>
								<strong>Link -</strong> <a
									href="https://onlineservices.tin.egov-nsdl.com/etaxnew/PopServletOffline" target="_blank">https://onlineservices.tin.egov-nsdl.com/etaxnew/PopServletOffline</a>
							</p>
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>

					<div class="col-md-12">
						<h2 id="mandatoryfile">Who should mandatorily file their Income
							Tax Returns</h2>
						<button class="accordion">Who are required to file Return?/ Is it
							mandatory to file return of income after getting PAN?</button>
						<div class="panel" id="mandatoryfilecontentsblock">
							<p>It is not mandatory to file return of income after getting
								PAN.</p>
							<p>Return is to be filed only if you are liable to file return of
								income as under :</p>
							<ul>
								<li>If your Gross total Income for financial Year 2017-18
									(Income before allowing Sec80 Deductions) is more than
									Rs.2,50,000/-. (The limit is Rs.3, 00,000/- for the persons of
									age between 60-80years and Rs.5, 00,000/- for the persons above
									80 years age).</li>
								<li>If you wish to claim Refund of taxes deducted/ paid.</li>
								<li>If you wish to carry forward losses to be set off in the
									next years return, then you have to declare losses for the
									current year by filing return.</li>
								<li>If you are resident and having any asset outside India
									orhaving financial interest in any entity outside India then
									you have to file return of income even though you are not
									having any other incomes.</li>
								<li>Return filing is compulsory even if you being a resident
									have signing authority in a foreign account.</li>
								<li>In the above two points resident means only a resident and
									ordinary resident (ROR); it does not include resident but not
									ordinary resident (RNOR) or Non resident (NRI).</li>
								<li>If you are having any exempt long term capital gains of more
									than Rs.2, 50,000/- in a financial year then you have to file
									the return compulsorily even though such gains are exempt from
									tax.(Here exempted long term capital gains include sale of
									equity shares, sale of units in equity oriented mutual funds,
									sale of units of a business trust)- effective form FY 2016-17.</li>
								<li>If you are in receipt of any income derived from property
									held under a charitable or religious trust or a political
									party, educational institution, hospital, trade unions, any non
									profit organization, any authority, body or a trust etc., you
									have to file the income tax return compulsorily.</li>
								<li>If you are planning to get any loan from bank or any other
									financial institution you may be asked copy of Income Tax
									Return.</li>
								<li>In respective of a firm or a company, Return of Income needs
									to be filed irrespective of the income/loss of the firm or
									company during the financial year.</li>
							</ul>
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>

					<div class="col-md-12">
						<h2 id="ifdontfiletax">What happens if we don't file the return
							within due date</h2>
						<button class="accordion">What happens if we don’t file the
							return within the due date?</button>
						<div class="panel" id="ifdontfiletaxcontentsblock">
							<p>A new section 234F has been inserted in Income Tax Act, 1961
								with effect from Assessment Year 2018-19 (Financial Year
								2017-18).</p>
							<p>&nbsp;</p>
							<p>Under this section, fee/penalty is levied if the Income-tax
								return is filed after the due date specified by the department.
								Earlier this penalty was levied by Assessing Officer at his
								discretion. But now, the same is payable before filing of
								Income-tax return.</p>
							<p>&nbsp;</p>
							<table border="1" width="0">
								<tbody>
									<tr>
										<td width="175">
											<p style="text-align: center;">
												<strong>Total Income</strong>
											</p>
										</td>
										<td style="text-align: center;" width="366">
											<p>
												<strong>Return Filed</strong>
											</p>
										</td>
										<td width="172">
											<p style="text-align: center;">
												<strong>Fee/Penalty</strong>
											</p>
										</td>
									</tr>
									<tr>
										<td width="175">
											<p>
												<strong>Less than or</strong>
											</p>
											<p>
												<strong>equal to Rs. 5,00,000</strong>
											</p>
										</td>
										<td width="366">
											<p>Return filed any time after due date</p>
										</td>
										<td style="text-align: right;" width="172">
											<p>Rs. 1,000</p>
										</td>
									</tr>
									<tr>
										<td rowspan="2" width="175">
											<p>
												<strong>More than</strong>
											</p>
											<p>
												<strong>Rs.5,00,000</strong>
											</p>
										</td>
										<td width="366">
											<p>
												Return filed on or before 31<sup>st </sup>December of
												Assessment Year
											</p>
										</td>
										<td style="text-align: right;" width="172">
											<p>Rs. 5,000</p>
										</td>
									</tr>
									<tr>
										<td width="366">
											<p>In any other case</p>
										</td>
										<td style="text-align: right;" width="172">
											<p>Rs. 10,000</p>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>

					<div class="col-md-12">
						<h2 id="iftaxliability">Can you still file your Income Tax Return
							if there is any tax liability</h2>
						<button class="accordion">Can you still file your Income Tax
							Return if there is any tax liability ?</button>
						<div class="panel" id="iftaxliabilitycontentsblock">
							<p>If you file the return with Tax Liability, you will receive a
								notice from the department under sectin 139(9) stating the
								return filed as defective. You have to respond to the notice
								with in 15 days from the date of receipt of notice other wise
								the return will be treated as invalid that means the return is
								treated as if it is not filed at all</p>
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>

					<div class="col-md-12">
						<h2 id="incomebelow">If my income is below the taxable limits, can
							I still file my Tax Returns</h2>
						<button class="accordion">If my income is below the taxable
							limits, can I still file my Tax Returns</button>
						<div class="panel" id="incomebelowcontentsblock">
							<p>Yes you can still file your income tax return but it is not
								mandatory</p>
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>
                    
                    <div class="col-md-12">
						<h2 id="linkaadharpan">Steps to link Aadhaar with PAN</h2>
						<button class="accordion"> Steps to link Aadhaar with PAN</button>
						<div class="panel" id="linkaadharpancontentsblock">
							<ul>
<li>Go to E-filing website,&nbsp;<a href="https://incometaxindiaefiling.gov.in/" target="_blank">https://incometaxindiaefiling.gov.in</a></li>
<li>Click on "Link Aadhaar" under services column.</li>
<li>Enter "PAN', Aadhaar Number, Name as per Aadhaar and Captcha Code and click on 'Link Aadhaar".</li>
<li>After submitting these details, a message is displayed stating that an email will be sent about the linking status.</li>
<li>After verification from UIDAI which is the government website for Aadhaar, the linking will be confirmed.&nbsp;In case of Name, Date of Birth or Gender in PAN database does not match with Aadhaar database</li>
</ul>
<p>Note:-</p>
<ul>
<li>Please ensure that the date of birth, Gender and Aadhaar number is per Aadhaar details.</li>
<li>If date of Birth and Gender fully match and Name as per Aadhaar is not exactly matching then user has to additionally provide Aadhaar OTP to proceed with partial Name Matching.</li>
<li>If Your Aadhaar is already linked with PAN with Same name and want to verify the correctness, the alert message will be displayed on to the screen "Your PAN is already linked to the Aadhaar&nbsp; Number"</li>
</ul>
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>
                    
                    <div class="col-md-12">
						<h2 id="resetpasswordit">How to reset the password for Income Tax login?</h2>
						<button class="accordion">How to reset the password for Income Tax login?</button>
						<div class="panel" id="resetpassworditcontentsblock">
							<p>There are four ways to reset a password for a registered user:</p>
<ol>
<li>Answer secret Question</li>
<li>Using OTP (PINs)</li>
<li>Using Aadhaar OTP</li>
<li>Using DSC</li>
</ol>
<p>For all the four Processes we have the following steps as common:</p>
<ul>
<li>Click on the link given which will take you to income tax portal: <a href="https://incometaxindiaefiling.gov.in/e-Filing/UserLogin/LoginHome.html" target="_blank">https://incometaxindiaefiling.gov.in/e-Filing/UserLogin/LoginHome.html</a></li>
<li>There you can see a link for forgot password, clink on the link;</li>
</ul>
                            <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/ressetpassowrdit1.png">
                            
                            <ul>
<li>Give the PAN number for User ID; Enter Captcha code and mobile number; and click continue;</li>
<li>Then you will be taken to next page select the appropriate option suitable for you and follow the steps given below;</li>
</ul>
<ul><strong>1. Answer secret Question:</strong></ul>
<p>If you select the option of Answering secret question you have to follow the below steps:</p>
<ul>
<li>Select &lsquo;Answer Secret Question&rsquo; from the drop down options available and click CONTINUE button;</li>
<li>Enter the Date of Birth (date of Incorporation in case of other than individual);</li>
<li>Select the Secret Question from the drop down options available;</li>
<li>Enter the &lsquo;Secret Answer&rsquo; and Click on &ldquo;Submit&rdquo;;</li>
<li>On success, you will be asked to enter the New Password and confirm the password; Click on &ldquo;SUBMIT&rdquo;;</li>
<li>Once the password has been changed a success message will be displayed. You can login with new password.</li>
</ul>
<ul><strong>2. Using OTP (PINs):</strong></ul>
<p>If you select the option of Using OTP(PINs) you have to follow the below steps:</p>
<ul>
<li>Select &lsquo;Using OTP (PINs)&rsquo; from the drop down options available and click on CONTINUE button;</li>
<li>You must select one of the options mentioned below</li>
<li>Registered Email ID and Mobile Number</li>
<li>New Email ID and Mobile Number</li>
</ul>
<ul>
<li>Let&rsquo;s go for the option of <em><u>Registered mail ID and Mobile number</u> first:</em></li>
<li>You will have to select this option if you have access to registered mail and mobile. Click on &ldquo;VALIDATE&rdquo;;</li>
<li>Enter the PINs received to the registered Email ID and Mobile Number and Click on &ldquo;VALIDATE&rdquo;.</li>
<li>On success, you have to enter the New Password and confirm the password and Click on &ldquo;SUBMIT&rdquo;;</li>
<li>Once the reset password request has been submitted, a success message will be displayed. You can login with new password after the time specified in Success message displayed.</li>
</ul>
<ul>
<li>Now if you opt to go by<em><u>New Email ID and Mobile Number:</u></em></li>
<li>Select this option if you don&rsquo;t have access to any one of your registered Mail ID or mobile number or both. Give the new mobile number and mail ID, however you have to validate your details by any of the three options available:</li>
<li><em><u>26 AS TAN</u></em>: Use this option if TDS has been deducted during the current or previous financial year. You have to give the details of TAN of the deductor, total TDS deducted by him and the Assessment year in which TDS is deducted.</li>
<li><em><u>OLTAS CIN:</u></em> Use this option if you have paid Taxes in the previous years, you have to give the details of BSR code, Challan Date and Challan serial Number.</li>
<li><em><u>Bank Account Details</u></em>: For this option you have to enter the bank account number which you have given for filing of Return in the previous year.</li>
<li>Click on validate and you will get the PIN numbers to your new mail and mobile. Reset the password using the PINs.</li>
<li>On success a message will be displayed, you can login with new password after the time specified in Success message displayed.</li>
</ul>

<ul><strong>3. Using Aadhaar OTP:</strong></ul>

<ul>
<li>This option will be useful only if you have linked your Aadhaar with PAN card;</li>
<li>Select &lsquo;Using Aadhaar OTP&rsquo; from the drop down available and click on CONTINUE button; Click Generate Aadhaar OTP.</li>
<li>Enter the OTP you receive to the mobile number registered with Aadhaar; Create New password and click submit;</li>
<li>Once the password has been changed a success message will be displayed. User can login with new password.</li>
</ul>

<ul><strong>4. Using DSC:</strong></ul>

<ul>
<li>You have two options in this:
<ul>
<li>New DSC: This option will be useful when you have <em>not registered</em> your DSC before with Income tax Department</li>
<li>Registered DSC: This option will be useful when you have <em>already registered</em> your DSC before with Income tax Department</li>
</ul>
</li>
<li>You have to upload Signature File generated using DSC Management Utility and click on the &ldquo;VALIDATE&rdquo; button. The DSC is validated. Now you can reset the Password.</li>
<li>Once the password has been changed a success message will be displayed. You can login with new password.</li>
</ul>
                            
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>
                    
                    <div class="col-md-12">
						<h2 id="pendingrefunds">How can I get pending refunds?</h2>
						<button class="accordion">How can I get pending refunds?</button>
						<div class="panel" id="pendingrefundscontentsblock">
							<p>Step 1: Click on this link https://tin.tin.nsdl.com/oltas/refundstatuslogin.html, this will take you to NSDL website.</p>                             
                            <p>Step 2:Enter your PAN; Select the Assessment Year for which you want to check the refund status and enter the text as shown in the image</p>
                                <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/refund1.png">
                            <p>Step 3:&nbsp;If the refund is processed by the Assessing Officer the status of the refund will be shown, if the refund is not processed then this below screen will be displayed that means you have to wait for some more time.</p>
                                <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/refund2.png">
						</div>
                        
                        <button class="accordion">Is your refund pending for long time!! Don&rsquo;t worry we are here to help you.</button>
						<div class="panel">
							<p>Follow few simple steps we will help you to get the refund&nbsp;</p>
                            <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/refund3.PNG">
                            
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>
                    
                    <div class="col-md-12">
						<h2 id="defectivenotice">I have recevied a defective Notice. What should I do?</h2>
						<button class="accordion">How to reply for Notice u/s 139 (9) ?</button>
						<div class="panel" id="defectivenoticecontentsblock">
							<p>Notice u/s 139(9) is basically intimation from the Income Tax Department for the Income tax return filed for a financial year being defective due to following common reasons:</p>
<ul>
<li>The assessee having filed in the return the details of taxes paid, but failed to provide income details, the Income Tax Department deems the return as defective.</li>
<li>The assessee having claimed the tax deducted at source (TDS) as refund, failed to provide income details in the return. In such cases the return will be deemed to be defective.</li>
<li>The assessee having liability to pay taxes fails to pay the taxes in full before filing the return.</li>
<li>The Assessee who is required to maintain balance sheet and profit and loss statement as per the provisions of Income tax Act, 1961, but fails to provide such statements while preparing the income tax return.</li>
<li>When there is a mismatch of name in the return filed with the name as per PAN card.</li>
<li>In order to clear the defects in the original return filed i.e. in response to the notice sent by the department u/s 139(9), the assessee is required to file response within 15 days of the receipt of intimation or within such other time as extended by the AO on a written request filed for the same. For filing the response one should mention in the return the section under which the return is being filed and the communication reference number which is already there on the intimation given by the department.</li>
</ul>
                        <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/notice1.png">    
                            
						</div>
                        <button class="accordion">What are the consequences if You don’t reply in 15 days?</button>
						<div class="panel">
							<p>If you don&rsquo;t reply with in specified time for the notice under section 139(9), your retrun will be considered as invalid that means it will treated the return is not filed at all</p>
                            
						</div>
                        <button class="accordion">Received a notice from the income tax department!! Don’t worry we are here to help you.</button>
						<div class="panel">
							<p>Talk to our experts, we will help you to resolve the notice</p>
                            <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/notice2.PNG">    
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>
                    
                    <div class="col-md-12">
						<h2 id="othernotice">Answering notices and intimations</h2>
						<button class="accordion">Intimation u/s 143(1) can be considered as Assessment?</button>
						<div class="panel" id="othernoticecontentsblock">
							 <p>Although an intimation received by an assessee u/s 143(1) within a period of 1 Year from the end of Financial Year in which the Return of Income was filed for the year under assessment can be considered as Assessment by the authorized Assessing Officer, however it is a matter of judgment where 143(1) is taken as a mere Intimation rather than Assessment order.</p>
<p><u>Case Law: TATA AIG GENERAL INSURANCE PVT LTD Vs DCIT ITAT (Mumbai)- ITA No.968 (MUM)2015.</u></p>
<p><strong>Assessment under section 143(1)</strong></p>
<p>This is intimation from the CPC and is a computer generated statement. All the data available in the Income Tax Return is validated by CPC with reference to the data available in the records of the income tax department.</p>
<p><strong>Scope of assessment under section 143(1)</strong></p>
<p>Intimation under section 143(1) is like primary verification of the return of income filed. At this stage of verification detailed examination of the return of income is not carried out. At this stage, the total income or loss is computed after making the adjustments (if any). Some of the adjustments made can be for the following matters:-</p>
<ul>
<li>The arithmetical errors in the return of income filed; or</li>
<li>Any incorrect claims made in the return of income;</li>
<li>Any loss claimed will be disallowed, if return of the previous year for which set-off of loss is claimed was furnished after the due date specified for filing the return of income;</li>
<li>Any expenditure mentioned in the audit report will be disallowed if it is not taken into consideration for the computation of total income in the return filed;</li>
<li>Any deduction claimed u/s 10AA, 80IA to 80-IE will also be disallowed, if the return is furnished after the due date specified for filing the return of income; or</li>
<li>Any additional income appearing in Form 26AS which was not included in computing the total income in the return of income.</li>
</ul>
<p>However, no adjustment shall be made by CPC unless intimation is given to the assessee of such adjustment either in writing or in electronic mode. So, the assessee will get the intimation about the adjustment. If the assessee gives any response, such response shall be considered before making any adjustment, and if no response is received within 30 days from the issue of the intimation, adjustments shall be made by the department.</p>

						</div>
                        <button class="accordion">After filing Return of Income u/s 139 for a Financial year, What notice that can be received in its respect?</button>
						<div class="panel" >
							 <p>After successful filing of Return of Income by an assessee u/s 139, the major notices that can be expected to come within a period of 1 year from the end of Financial Year in which the said return was filed is Intimation Notice U/s 143(1), Notice for Scrutiny Assessment u/s 143(2) ( within a period of 6 months from the end of the Financial Year in which the said return was filed) and Notice for Best Judgement Assessment u/s 142(1).</p>
<p>In addition to above, a notice u/s 133C as inserted by the Finance (No.2) Act 2014 w.e.f 01/10/2014 can also be issued by the prescribed Income Tax authority for Return of Income filed for AY 2015-16 and onwards within a time period of 4 years or 6 years as prescribed u/s 153.</p>
<p>Every taxpayer has to furnish the details of his income to the Income-tax Department. These details are to be furnished by filing up his return of income. Once the return of income is filed up by the taxpayer, the next step is the processing of the return of income by the Income Tax Department. The Income Tax Department examines the return of income for its correctness. The process of examining the return of income by the Income Tax department is called as &ldquo;Assessment&rdquo;. Assessment also includes re-assessment and best judgment assessment under section 144.</p>
<p>Under the Income-tax Law, there are four major assessments given below:</p>
<ul>
<li>Assessment under section 143(1), i.e., Summary assessment without calling the assessee.</li>
<li>Assessment under section 143(3), i.e., Scrutiny assessment.</li>
<li>Assessment under section 144, i.e., Best judgment assessment.</li>
<li>Assessment under section 147, i.e., Income escaping assessment.</li>
</ul>
<p>All the above mentioned notices are issued which leads to the given assessment proceedings.</p>
						</div>
                        <button class="accordion">How to reply for Notice u/s 139 (9)?</button>
						<div class="panel" >
							 <p>Notice u/s 139(9) is basically intimation from the Income Tax Department for the Income tax return filed for a financial year being defective due to following common reasons:</p>
<p>The assessee having filed in the return the details of taxes paid, but failed to provide income details, the Income Tax Department deems the return as defective.</p>
<p>The assessee having claimed the tax deducted at source (TDS) as refund, failed to provide income details in the return. In such cases the return will be deemed to be defective.</p>
<p>The assessee having liability to pay taxes fails to pay the taxes in full before filing the return.</p>
<p>The Assessee who is required to maintain balance sheet and profit and loss statement as per the provisions of Income tax Act, 1961, but fails to provide such statements while preparing the income tax return.</p>
<p>When there is a mismatch of name in the return filed with the name as per PAN card.</p>
<p>In order to clear the defects in the original return filed i.e. in response to the notice sent by the department u/s 139(9), the assessee is required to file response within 15 days of the receipt of intimation or within such other time as extended by the AO on a written request filed for the same. For filing the response one should mention in the return the section under which the return is being filed and the communication reference number which is already there on the intimation given by the department.</p>
						</div>
                        <button class="accordion">When can an assessee receive notice for scrutiny assessment u/s 147?</button>
						<div class="panel" >
							 <p>Notice for scrutiny assessment u/s 147 is issued u/s 148 if the Assessing Officer(AO) has reason to believe that any income has escaped assessment which should be chargeable to tax . The main objective of</p>
<p>How the scrutiny assessment can be made by the assessing officer (AO)?</p>
<p>For making the scrutiny assessment the AO has to issue a notice to the assessee (i.e., the tax payer) u/s 148 and assessee should be given an opportunity of being heard. The time-limit for issuance this notice u/s 148 is a period of 4 years from the end of the relevant assessment year. If the income escaped by the assessee is Rs. 1,00,000 or more and certain other conditions are satisfied, then notice can be issued upto 6 years from the end of the relevant assessment year.</p>
<p>In case the income which is escaped relates to any asset located outside India, notice can be issued upto 16 years from the end of the relevant assessment year.</p>
<p>Notice under section 148 can be issued by AO only after getting prior approval from the prescribed authority.</p>
<p>The objective of carrying out assessment under section 147 is to bring under the tax net any income which has escaped assessment in original assessment under sections 143(1), 143(3), 144 &amp; 147 (as the case may be).</p>
<p>In the following cases, it will be considered as income having escaped assessment:</p>
<ul>
<li>Where no return of income has been furnished by the taxpayer, although his total income or the total income of any other person in respect of which he is assessable during the previous year exceeded the maximum amount which is not chargeable to income-tax.</li>
<li>Where a return of income has been furnished by the taxpayer but no assessment has been made and it is noticed by the Assessing Officer that the taxpayer has understated the income or has claimed excessive loss, deduction, allowance or relief in the return.</li>
<li>Where the taxpayer has failed to furnish a report in respect of any international transaction which he was required to do under section 92E.</li>
<li>Where an assessment has been made, but:
<ul>
<li>income chargeable to tax has been under assessed; or</li>
<li>income has been assessed at low rate; or</li>
<li>income has been made the subject of excessive relief; or</li>
<li>excessive loss or depreciation allowance or any other allowance has been computed;</li>
</ul>
</li>
<li>Where a person is found to have any asset (including financial interest in any entity) located outside India.</li>
<li>Where a return of income has not been furnished by the assessee and on the basis of information or document received from the prescribed income-tax authority under section 133C(2), it is noticed by the Assessing Officer that the income of the assessee exceeds the maximum amount not chargeable to tax.</li>
<li>Where a return of income has been furnished by the assessee and on the basis of information or document received from the prescribed income-tax authority under section 133C(2), it is noticed by the Assessing Officer that the assessee has understated the income or has claimed excessive loss, deduction, allowance or relief in the return.</li>
</ul>
						</div>
                        <button class="accordion">Received a notice from the income tax department!! Don’t worry we are here to help you.</button>
						<div class="panel">
							<p>Talk to our experts, we will help you to resolve the notice</p>
                            <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/notice2.png">    
						</div>
					</div>
					<a href="#helphome">Back to help topics</a><br>
                    
                    <div style="height: 60px"></div>
				</div>
			</div>
		</div>
    </div>
</section>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    //alert("ready");
    openneededaccordion();
});

    function doalert(obj) {
        //alert(obj.getAttribute("href"));
        openneededaccordionforthis(obj.getAttribute("href"));
        return false;
    }

    function openneededaccordionforthis(urlstring) {
    //alert(val);
    if(urlstring.indexOf("#")>=0) {
        //var val = window.location.href;
        urlstring=urlstring.substring(urlstring.indexOf("#")+1);
        urlstring = urlstring + 'contentsblock';
        //alert(urlstring);
        var x = document.getElementById(urlstring);
        x.style.display = "block";
    }
}
    
function openneededaccordion() {
    var urlstring = window.location.href;
    //alert(val);
    if(urlstring.indexOf("#")>=0) {
        //var val = window.location.href;
        urlstring=urlstring.substring(urlstring.indexOf("#")+1);
        urlstring = urlstring + 'contentsblock';
        //alert(urlstring);
        var x = document.getElementById(urlstring);
        x.style.display = "block";
    }
}
</script>

<script>
    
    
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}
    
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>