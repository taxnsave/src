<!-- Navbar -->
<nav class="navbar navbar-inverse my-nav">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
            </button>
            <a class="navbar-brand" href="<?php echo $CONFIG->siteurl;?>home.html"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png" alt="Logo" width="160"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li ><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
            </ul>
            <ul class="nav navbar-nav brdr">
                <li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
                <li ><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create Will</a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- Navbar -->
<!-- Banner -->
<div class="banner_will">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
                <h1>
                    <center> Calculate your Risk</center>
                </h1>
                <center><span class="span-endline"></span></center>
            </div>
        </div>
    </div>
</div>
<!-- Banner -->

<style >
    h4 {color: darkblue;line-height: 1.3; font-size: 22px;    margin-bottom: 40px; }
</style>
<div class="about-area-one section-spacing">
    <div class="inner-about">
        <div class="container">
            <div class="row about-content">
                <div class="col-lg-12 col-md-12 col-sm-12">
                     <div class="jumbotron-contents">
    <h4>To assess your risk tolerance Seven questions are given below. Each question is followed by three, possible answers. Circle the letter that corresponds to your answer.</h4>
    </div>
	 <p style="font-size:25px;">You are an investor who is prepared to accept lower returns with lower levels of risk in<br> order to preserve your capital. The negative effects of taxation and inflation will not be of<br> concern to you, provided your initial investment is protected. As a conservative investor,<br> you might expect your portfolio to be allocated approximately 15% in growth assets, with<br> the remainder in defensive assets and an allocation to gold.</p>
		  <img src="RPAssests/Conservative.jpg" alt="exaple-image" class="img-rounded img-responsive">
		  </div>
	
	
	</div>
	
	
	
    

	            
	                    </div>
            </div>
        </div>
  
<div style="height:50px"></div>
	            
