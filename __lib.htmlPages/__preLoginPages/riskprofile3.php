
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script><!-- Navbar -->
<nav class="navbar navbar-inverse my-nav">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
            </button>
            <a class="navbar-brand" href="<?php echo $CONFIG->siteurl;?>home.html"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png" alt="Logo" width="160"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li ><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
            </ul>
            <ul class="nav navbar-nav brdr">
                <li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
                <li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
                <li ><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create Will</a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- Navbar -->
<!-- Banner -->
<div class="banner_will">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
                <h1>
                    <center> Calculate your Risk</center>
                </h1>
                <center><span class="span-endline"></span></center>
            </div>
        </div>
    </div>
</div>
<!-- Banner -->
<style >
    h4 {color: darkblue;line-height: 1.3; font-size: 22px;    margin-bottom: 40px; }
</style>
<div class="about-area-one section-spacing">
    <div class="inner-about">
        <div class="container">
            <div class="row about-content">
                <div class="col-lg-12 col-md-12 col-sm-12">
                     <div class="jumbotron-contents">
    <h4>To assess your risk tolerance Seven questions are given below. Each question is followed by three, possible answers. Circle the letter that corresponds to your answer.</h4>
    </div>
	<form action="">
  <div class="content-row">
                  <h2 class="content-row-title">
                  </h2>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="row">
                        
						 <div class="form-group">
						  <label for="exampleInputEmail1">1.Just six weeks after you invested in a stock, its price declines by 20 percent if the fundamentals of the stock have not changed, what would you do? </label>
                          <div class="radio" id="rg1">
                               <input class="calc" type="radio" name="radio1" value="1">    Sell <br>
                               <input class="calc" type="radio" name="radio1" value="2">    Do nothing <br>
                               <input class="calc" type="radio" name="radio1" value="3">    Buy more <br>
                          </div>
						  </div>  
                          <div class="form-group">
						  <label for="exampleInputEmail1">2.Consider the previous question another way. Your stock dropped 20 percent, but it is part of a portfolio designed to meet investment goals with three different time horizons.</label>
                               <label for="exampleInputEmail1">i.What would you do if your goal were five years away?</label>
						  <div class="radio" id="rg2">
                              <input class="calc" type="radio" name="radio2" value="1">Sell <br>
                              <input class="calc" type="radio" name="radio2" value="2"> Do nothing <br>
                              <input class="calc" type="radio" name="radio2" value="3"> Buy more <br>
                          </div>
						  <label for="exampleInputEmail1">ii.What would you do if the goal were 15 years away?</label>
						  <div class="radio" id="rg3">
                               <input class="calc" type="radio" name="radio3" value="1">Sell <br>
                               <input class="calc" type="radio" name="radio3" value="2"> Do nothing  <br>
                               <input class="calc" type="radio" name="radio3" value="3"> Buy more <br>
                          </div>
						   <label for="exampleInputEmail1"> iii.What would you do if the goal were 30 years away?</label>
						   <div class="radio" id="rg4">
                               <input class="calc" type="radio" name="radio4" value="1"> Sell <br>
                               <input class="calc" type="radio" name="radio4" value="2"> Do nothing<br>
                               <input class="calc" type="radio" name="radio4" value="3">Buy more<br>
                          </div>
						  </div> 
             <div class="form-group">
			 <label for="exampleInputEmail1">3.You have bought a stock as part of your retirement portfolio. Its price rises by 25 percent one month. If the fundamentals of the stock have not changed, what would you do?</label>
			  <div class="radio" id="rg5">
                               <input class="calc" type="radio" name="radio5" value="1"> Sell<br>
                               <input class="calc" type="radio" name="radio5" value="2"> Do nothing<br>
                               <input class="calc" type="radio" name="radio5" value="3">Buy more<br> 
                          </div>
                 </div> 
				 <div class="form-group">
			 <label for="exampleInputEmail1">4.You are investing for retirement which is 15 years away. What would you do?</label>
			  <div class="radio" id="rg6">
                              <input class="calc" type="radio" name="radio6" value="1">Invest in a money market mutual fund or a guaranteed investment contract<br> 
                              <input class="calc" type="radio" name="radio6" value="2"> Invest in a balanced mutual fund that has a stock: bond mix of 50:50<br>
                              <input class="calc" type="radio" name="radio6" value="3"> Invest in an aggressive growth mutual fund<br>
                          </div>
                 </div> 
				 <div class="form-group">
			 <label for="exampleInputEmail1">5.As a prize winner, you have been given some choice. Which one would you choose?</label>
			  <div class="radio" id="rg7">
                               <input class="calc" type="radio" name="radio7" value="1">Rs.50,000 in cash<br>
                               <input class="calc" type="radio" name="radio7" value="2">A 50 percent chance to get Rs. 125,000<br>
                               <input class="calc" type="radio" name="radio7" value="3">A 20 percent chance to get Rs. 375,000<br>
                          </div>
                 </div> 
				 <div class="form-group">
			 <label for="exampleInputEmail1">6.A good investment opportunity has come your way. To participate in it you have to borrow money. Would you take a loan?</label>
			  <div class="radio" id="rg8">
                             <input class="calc" type="radio" name="radio8" value="1">No <br>
                             <input class="calc" type="radio" name="radio8" value="2">Perhaps <br>
                             <input class="calc" type="radio" name="radio8" value="3">Yes <br>
                          </div>
                 </div> 
				 <div class="form-group">
			 <label for="exampleInputEmail1">7.Your company, which is planning to go public after three years, is offering stock to its employees. Until it goes public, you can't sell your shares. Your investment, however, has the potential of multiplying 10 times when the company goes public. How much money would you invest?</label>
			  <div class="radio" id="rg9">
                              <input class="calc" type="radio" name="radio9" value="1">Nothing<br>
                              <input class="calc" type="radio" name="radio9" value="2">Three months' salary<br>
                              <input class="calc" type="radio" name="radio9" value="3">Six months' salary<br>
                          </div>
						 
                 </div> 
				
                </div>
				    </div>
					        
                </div>
				    </div>
					
					<button class="btn btn-info" onclick="calcscore()"id="submit"type="submit">Submit</button>
		
	</form>
	
	


</div>
  

  



    
  	   
	<script>
	function calcscore() {
	    
    var score = 0;
    $(".calc:checked").each(function() {
	
        score = score + parseInt($(this).val(), 10);
		$("input[name=sum]").val(score)
		});
		
		if(score >= "9" && score <= "14")
	{
	    
	    swal({
      title: 'Auto close alert!',
      text: 'I will close in 2 seconds.',
      timer: 2000
    });
	 
            
	}
	else if(score >= "15" && score <= "21")
	{
            myFunction2()
	} 
	else if(score >= "22" && score <= "27")
	{
	
           myFunction3()
	} 
	
	function myFunction1() {
    var txt;
    if (confirm("You are a Conservative Investor. You are an investor who is prepared to accept lower returns with lower levels of risk in order to preserve your capital. The negative effects of taxation and inflation will not be of concern to you, provided your initial investment is protected. As a conservative investor, you might expect your portfolio to be allocated approximately 15% in growth assets, with the remainder in defensive assets and an allocation to gold."))
    {
        setTimeout("location.href = 'Conservative.html';", 3000);
            Redirect1()
    } 
    else 
    {
        txt = "You pressed Cancel!";
    }
    
	}

	function Redirect1() {

              setTimeout("location.href = 'Conservative.html';", 3000);
            }

function myFunction2() {
    var txt;
    if (confirm("You are a Moderate Investor.You are an investor who would like to invest in both income and growth assets. You will be<br> comfortable with calculated risks to achieve good returns, however, you require an <br>investment strategy that adequately deals with the effects of inflation and tax. As a<br> moderate investor, you might expect your portfolio to be allocated approximately<br> 45% in growth assets, with the remainder in defensive assets and an allocation to gold."))
    {
        
            Redirect2()
    } 
    else 
    {
        txt = "You pressed Cancel!";
    }
    
	}
			
    function Redirect2() {
                setTimeout("location.href = 'Moderateresult.html';", 3000);
            }
            
            
            function myFunction3() {
    var txt;
    if (confirm("You are a Aggressive investor. You are an investor who is comfortable with a high volatility and high level of risk in order to achieve higher returns over long term. Your objective is to accumulate assets.long term by primarily investing in growth assets. As an aggressive investor, you might expect your portfolio to be allocated up to 75% in growth assets and an allocation to gold."))
    {
        
            Redirect3()
    } 
    else 
    {
        txt = "You pressed Cancel!";
    }
    
	}
	function Redirect3() {
               setTimeout("location.href = 'Aggressiveresult.html';", 3000);
            }
		
	
} 


	</script>
	            
	                    </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  


		
<div style="height:50px"></div>
	            
               