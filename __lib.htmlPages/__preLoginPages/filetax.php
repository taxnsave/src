<!-- Navbar -->
  <nav class="navbar navbar-inverse my-nav">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="<?php echo $CONFIG->siteurl;?>home.html"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png" alt="Logo" width="160"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li> 
        <li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
		<li><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
      </ul>
      <ul class="nav navbar-nav brdr">
        <li class="active"><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
		<li><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create Will</a></li>
      </ul>
    </div>
  </div>
</nav>
  <!-- Navbar -->
  
<!-- Banner -->
<div class="banner_inner">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
				<h1><center>31st July</center><br> <center><span class="span-end">Due date for Tax Returns</span></center></h1>
				<center><span class="span-endline">An easy and professional way to file Income Tax Returns</span></center>
			</div>
			
		</div>
	</div>
</div>
<!-- Banner -->



<div class="page-wrapper">

<section class="filetax-way">
<div class="container">
<h2 class="title-h2">GET STARTED YOUR <span>CONVENIENT WAY</span></h2>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12" align="center">
		<a class="title-p" href="helpcentre.html#mandatoryfile">Who should mandatorily file their Income Tax Returns ?</a><br/>
        <a class="title-p" href="helpcentre.html#incomebelow">If my income is below the taxable limits, can I still file my Tax Returns ?</a><br/>
        <a class="title-p" href="helpcentre.html#ifdontfiletax">What happens if you do not file the Income Tax Return within the due date ?</a><br/>
        <a class="title-p" href="helpcentre.html#iftaxliability">Can you still file your Income Tax Return if there is any tax liability ?</a></div>
			<div class="col-xs-12 col-sm-6 col-md-6">
                <form>
<button type="button" class="btn btn-upload center-block" onclick="window.location.href='<?php echo $CONFIG->siteurl;?>login.html'">UPLOAD FORM-16</button>
                </form>
	</div>
		<div class="col-xs-12 col-sm-6 col-md-6">
            <form>
<button type="button" class="btn btn-filing center-block" onclick="window.location.href='<?php echo $CONFIG->siteurl;?>login.html'">START FILING</button>
            </form>
	</div>
</div>
</div>
</section>

<section class="need-help">
<div class="container">
<h2 class="title-h2">NEED HELP ?</h2>
<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-4">

                    <button type="button" onclick="window.location='helpcentre.html#file26qb'" class="btn btn-upload center-block" >File 26QB<br/>
(TDS on immovable property)</button>
	</div>
		<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location='helpcentre.html#file26qc'" class="btn btn-upload center-block">File 26QC<br/>
(TDS on rent)</button>
	</div>
		<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location='helpcentre.html#viewform26as'" class="btn btn-upload center-block">View Form 26AS</button>
	</div>
</div>
	<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location='helpcentre.html#paytaxonline'" class="btn btn-upload center-block">Pay Tax online</button>
	</div>
		<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location='helpcentre.html#everify'" class="btn btn-upload center-block">e-verify the return</button>
	</div>
		<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location='helpcentre.html#refundstatus'" class="btn btn-upload center-block">Check Refund status</button>
	</div>
</div>
</div>
</section>

<section class="need-assist">
	<div class="container">
       <div class="row">
       		<div class="col-md-12">
            	<div class="head-title-news">
                	<p id="noticeassist" class="will-title">NOTICES ASSISTANCE</p>
                    
                </div>
            </div>
       </div>
	   <div class="step-sec">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="box">
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/notice1.png" alt="" width="70"></center>
					<p>PENDING REFUNDS<br/>&nbsp;</p>
					<button type="button" onclick="window.location='helpcentre.html#pendingrefunds'" class="btn btn-step center-block">Click here</button>
				</div><!-- Box -->
			</div>
			
			<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="box">
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/notice2.png" alt="" width="70"></center>
					<p>DEFECTIVE NOTICE<br/>
u/s 139(9)</p>
					<button type="button" onclick="window.location='helpcentre.html#defectivenotice'" class="btn btn-step center-block">Respond in 15 days</button>
				
				</div><!-- Box -->
			</div>
	   
	   		<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="box">
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/notice3.png" alt="" width="70"></center>
					<p>OTHER NOTICES<br/>
& INTIMATIONS</p>
					<button type="button" onclick="window.location='helpcentre.html#othernotice'" class="btn btn-step center-block">Respond in 30 days</button>
				
				
				</div><!-- Box -->
			</div>
	   
		</div>
	   </div><!-- Blog Sec -->
    </div>
</section>
<div class="clearfix"></div>

<section class="short-invest">
<div class="container">
<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-6 col-md-offset-1">
		<h3 class="sec-h3">Short of 80C investments?</h3>
		<p class="sec-p">Invest now in Top ELSS Funds to reduce your tax burden </p>
<button type="button" class="btn btn-post">Click here</button>
	</div>
	<div class="col-xs-12 col-sm-5 col-md-3 ">
		<div class="icon-bg">
			<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/tax_calc.png" width="370">
			
		</div>
	</div>
	
</div>
</div>
</section><!-- Support -->
<div class="clearfix"></div>

<section class="tax-step">
<div class="container">
<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location.href='<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/taxsave_rentreceipts.xlsx'" class="btn btn-upload center-block">Print Rent Receipts</button>
	</div>
		<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location.href='<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/taxsave_rentalagreement.docx'" class="btn btn-upload center-block">Make Rental Agreement</button>
	</div>
		<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location.href='<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>helpcentreimg/taxsave_giftdeed.docx'" class="btn btn-upload center-block">Execute Gift Deed</button>
	</div>
</div>
	<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location='calculators.html#hraCal'" class="btn btn-upload center-block">HRA Calculator</button>
	</div>
		<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location='calculators.html#taxsavingCal'" class="btn btn-upload center-block">Tax Saving Calculator</button>
	</div>
		<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location='calculators.html#emiCal'" class="btn btn-upload center-block">EMI Calculator</button>
	</div>
</div>
	<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location='helpcentre.html#linkaadharpan'" class="btn btn-upload center-block">Aadhaar Linking</button>
	</div>
		<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location='helpcentre.html#howtofileincometax'" class="btn btn-upload center-block">Register with income tax e-filing</button>
	</div>
		<div class="col-xs-12 col-sm-4 col-md-4">
<button type="button" onclick="window.location='helpcentre.html#resetpasswordit'" class="btn btn-upload center-block">Reset e-filing password</button>
	</div>
</div>
	
</div>
</section>

<section class="testimonial">
<div class="container">
<h2 class="title-h2">CLIENT TESTIMONIAL</h2>
<div class="row">
	<div class="col-sm-10 col-sm-offset-1 testimonial-list">
	                	<div role="tabpanel">
	                		<!-- Tab panes -->
	                		<div class="tab-content" id="testimonialtabscontent">
	                			<div role="tabpanel" class="tab-pane fade in active" id="tab1">
	                				<div class="testimonial-image">
	                					<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/testimonials/1.jpg" alt="t1">
	                				</div>
	                				<div class="testimonial-text" style="height:250px">
		                                <p>
		                                	“It has been a pleasure working with Devmantra.  I have liaised with him for more than 2.5 years now. His holistic approach to Wealth Management has brought clarity to all my financial goals and aspirations. Their team have done an incredible job of analysing, integrating and managing my personal and corporate needs enabling me to minimise the amount of tax I pay, secure my funds in various mutual fund plans and SIP's to reap great benefits over the years. He has shown me a clear path to secure my retirement too. I greatly value his expertise and simplified approach taken for all my financial planning needs. &quot;Dev Mantra takes pride in seeing his client succeed&quot; <br>
		                                	<p><span>Rajesh Balachandran</span></p>
										<p><span>Manager - Accounts Receivable</span><span class="red"> (Timken Engineering & Research India Pvt. Ltd.)</span></p>
		                                </p>
	                                </div>
	                			</div>
	                			<div role="tabpanel" class="tab-pane fade" id="tab2">
	                				<div class="testimonial-image">
	                					<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/testimonials/2.jpg" alt="t2">
	                				</div>
	                				<div class="testimonial-text" style="height:250px">
		                                <p>"Personalised attention , tailor made schemes, both short term and long term.  Very prompt in sending out the updates."<br>
		                                	<p><span>Jayaram N</span></p>
										<p><span>Vice President – HR & Admin </span><span class="red">(Nitesh Estates Ltd)</span></p>
	                                </div>
	                			</div>
	                			<div role="tabpanel" class="tab-pane fade" id="tab3">
	                				<div class="testimonial-image">
	                					<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/testimonials/3.jpg" alt="t3">
	                				</div>
	                				<div class="testimonial-text" style="height:250px">
		                                <p>
		                                	"I realised I have always been paying more tax than I should have paid. Thanks to the team they have demystified the entire process to me for me to save more money"<br>
		                                	<p><span>Atishay Jand</span></p>
										<p><span>Associate</span><span class="red"> – TheMathCompany</span></p>
		                                </p>
	                                </div>
	                			</div>
	                			<div role="tabpanel" class="tab-pane fade" id="tab4">
	                				<div class="testimonial-image">
	                					<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/testimonials/4.jpg" alt="t4">
	                				</div>
	                				<div class="testimonial-text" style="height:250px">
		                                <p>
		                                	"DevMantra performed this activity for 4 consecutive years for Nuance Transcription Services.  The planning, services by the staff and the execution was rendered flawlessly and efficiently. All NTS employees are very happy with the timeliness and easy approachability of the DevMantra staff."<br>
		                                	<p><span>Bindu S Magnoor</span></p>
										<p><span>Deputy Manager- Human Resources</span> <span class="red">(Nuance Transcription Services India Pvt Ltd)</span></p>
		                                </p>
	                                </div>
	                			</div>
				<div role="tabpanel" class="tab-pane fade" id="tab5">
	                				<div class="testimonial-image">
	                					<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/testimonials/5.jpg" alt="t4">
	                				</div>
	                				<div class="testimonial-text" style="height:250px">
		                                <p>
		                                	"I and my wife are working professionals with a daughter of 7 years. It is quite difficult for us to keep a tab on how the finance and investment market is doing, the funds that are introduced recently, what should be out debt to equity ratio and more over to plan, file and manage our overall income tax and ITR. Dev Mantra has expert professionals who help us manage the end to end needs on our overall personal finance portfolio management including ITR filing. Since Dev Mantra acts as a one stop shop for us with a single point of contact for our personal finance management requirements it enables us to be ready for the future and any eventualities on the course."<br>
		                                	<p><span>Mr. & Mrs. Harsha Shetty & Poonam Jajji</span></p>
										<p><span>Research Director & Project Manager</span><span class="red"> (Gartner Research & Advisory Services Pvt. Ltd. & Tata Consultancy Services)</span></p>
		                                </p>
	                                </div>
	                			</div>
			<div role="tabpanel" class="tab-pane fade" id="tab6">
	                				<div class="testimonial-image">
	                					<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/testimonials/6.jpg" alt="t4">
	                				</div>
	                				<div class="testimonial-text" style="height:250px">
		                                <p>
		                                	"I always want to do investment and increase my wealth but don't know the right options that I should start to achieve my goals. Team Dev Mantra put me into the habit of doing investment and guided me to choose the right investment options. They took time to listen me, getting to know me, understand my priorities and keeping that in mind they designed my portfolio that will work smartly to achieve my goals."<br>
		                                	<p><span>Padma Lochana Patra</span></p>
										<p><span>Senior research associate</span> <span class="red">(Medgenome Labs Pvt. Ltd.)</span></p>
		                                </p>
	                                </div>
	                			</div>
		<div role="tabpanel" class="tab-pane fade" id="tab7">
	                				<div class="testimonial-image">
	                					<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/testimonials/7.jpg" alt="t4">
	                				</div>
	                				<div class="testimonial-text" style="height:250px">
		                                <p>
		                                	"DevMantra has been helping me in filing taxes for the last 4 years. They are very through in their job and reasonable in terms of fees. They have dedicated personnel who interact with clients to provide timely services. "<br>
		                                	<p><span>Satish Sankaran</span></p>
										<p><span>VP Clinical Operations and Lab Director</span> <span class="red">(Strand LifeSciences Pvt. Ltd.)</span></p>
		                                </p>
	                                </div>
	                			</div>
	<div role="tabpanel" class="tab-pane fade" id="tab8">
	                				<div class="testimonial-image">
	                					<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/testimonials/8.jpg" alt="t4">
	                				</div>
	                				<div class="testimonial-text" style="height:250px">
		                                <p>
		                                	"Understanding and evaluating the requirement, proposing a well defined plan to suit individual needs, frequent updates on the portfolio with meaningful suggestions and personal attention to detail is the hall mark of Dev Mantra "<br>
		                                	<p><span>Kannan Rangamani</span></p>
										<p><span>Finance Director - India and Philippines</span><span class="red"> (Hibu India Pvt. Ltd.)</span></p>
		                                </p>
	                                </div>
	                			</div>
<div role="tabpanel" class="tab-pane fade" id="tab9">
	                				<div class="testimonial-image">
	                					<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/testimonials/9.jpg" alt="t4">
	                				</div>
	                				<div class="testimonial-text" style="height:250px">
		                                <p>"1. Detailed Analysis done on the Income and Expenditure
2. Plan was provided based on the Income and savings plan.
3. Good Knowledge on the market and recommendation provided on the investment plan.
"<br>
		                                	<p><span>Subramanian S</span></p>
										<p><span>Senior Manager</span> - <span class="red">(Vodafone Global Sercvices Pvt.Ltd.)</span></p>
		                                </p>
	                                </div>
	                			</div>
<div role="tabpanel" class="tab-pane fade" id="tab10">
	                				<div class="testimonial-image">
	                					<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/testimonials/10.jpg" alt="t4">
	                				</div>
	                				<div class="testimonial-text" style="height:250px">
		                                <p>
		                                	" I have been working with Dev Mantra since 2015.  On our first meeting, he listened to my requirements and families long-term goals and he provided us with a comprehensive plan of action. Now we are in constant touch throughout the year, and keep updating our financial plan. I am really satisfied with personalised financial services rendered by team DevMantra."<br>
		                                	<p><span>Anil Betageri</span></p>
										<p><span>Consultant Pathologist -</span><span class="red"> (Medstar Speciality Hospital)</span></p>
		                                </p>
	                                </div>
	                			</div>
<div role="tabpanel" class="tab-pane fade" id="tab11">
	                				<div class="testimonial-image">
	                					<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/testimonials/11.jpg" alt="t4">
	                				</div>
	                				<div class="testimonial-text" style="height:250px">
		                                <p>
		                                	"Dev Mantra has not only been our investment consultant but also a teacher who helps one understand the pros and cons of investing in a particular product and guides one through the journey, helping us learn in the process. We have been availing his esteemed services of investment planning and tax returns for two years now and are glad that our investments are in his responsible hands."<br>
		                                	<p><span>Subramanian SAmritanshu Verma & Hena Anand</span></p>
										<p><span>Software Engineer & Associate Director</span> - <span class="red">(Microsoft & Value 360)<span></p>
		                                </p>
	                                </div>
	                			</div>
<div role="tabpanel" class="tab-pane fade" id="tab12">
	                				<div class="testimonial-image">
	                					<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/testimonials/12.jpg" alt="t4">
	                				</div>
	                				<div class="testimonial-text" style="height:250px">
		                                <p>
		                                	"Coming from a middle class educated background, we used to read, talk and meet people to find out more about the right financial investment and thus used to believe that we were making the right decisions. This was until we met Mr. Gireesh from Devmantra through one of our close friend. He started making us understand the fundamental of investment and money management rather than just getting us to put our investment in a financial instrument. We had detail discussion with him where he sent considerable amount of time making us aware of various asset class and what could be best investment going with our financial position and the goals. He came out with complete financial plan for the family and various such goals for future."<br>
		                                	<p><span>Navin Kumar & Shalini Singh</span></p>
										<p><span>Program Manager</span> - <span class="red">(Sasken Technologies)</span></p>
		                                </p>
	                                </div>
	                			</div>
	                		</div>
	                		<!-- Nav tabs -->
	                		<ul class="nav nav-tabs" role="tablist" id="tabs">
	                			<li role="presentation" >
	                				<a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab" onclick="currentSlide(1)"></a>
	                			</li>
	                			<li role="presentation" class="active">
	                				<a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab" onclick="currentSlide(2)"></a>
	                			</li>
	                			<li role="presentation">
	                				<a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab" onclick="currentSlide(3)"></a>
	                			</li>
	                			<li role="presentation">
	                				<a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab" onclick="currentSlide(4)"></a>
	                			</li>
								<li role="presentation">
	                				<a href="#tab5" aria-controls="tab4" role="tab" data-toggle="tab" onclick="currentSlide(5)"></a>
	                			</li>
								<li role="presentation">
	                				<a href="#tab6" aria-controls="tab4" role="tab" data-toggle="tab" onclick="currentSlide(6)"></a>
	                			</li>
								<li role="presentation">
	                				<a href="#tab7" aria-controls="tab4" role="tab" data-toggle="tab" onclick="currentSlide(7)"></a>
	                			</li>
								<li role="presentation">
	                				<a href="#tab8" aria-controls="tab4" role="tab" data-toggle="tab" onclick="currentSlide(8)"></a>
	                			</li>
								<li role="presentation">
	                				<a href="#tab9" aria-controls="tab4" role="tab" data-toggle="tab" onclick="currentSlide(9)"></a>
	                			</li>
								<li role="presentation">
	                				<a href="#tab10" aria-controls="tab4" role="tab" data-toggle="tab" onclick="currentSlide(10)"></a>
	                			</li>
								<li role="presentation">
	                				<a href="#tab11" aria-controls="tab4" role="tab" data-toggle="tab" onclick="currentSlide(11)"></a>
	                			</li>
								<li role="presentation">
	                				<a href="#tab12" aria-controls="tab4" role="tab" data-toggle="tab" onclick="currentSlide(12)"></a>
	                			</li>
	                		</ul>
	                	</div>
	                </div>
		
	</div>
</div>
	

</section>	
	
  
</div><!-- Page Wrapper -->  

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">

$(function() {

    //cache a reference to the tabs
    var tabs = $('#tabs li');
    var tabscontent = $('#testimonialtabscontent div');

    //on click to tab, turn it on, and turn previously-on tab off
    tabs.click(function() { $(this).addClass('active').siblings('.active').removeClass('active'); });

    //auto-rotate every 5 seconds
    setInterval(function() {

            //get currently-on tab
        var onTab = tabs.filter('.active');
            //click either next tab, if exists, else first one
        var nextTab = onTab.index() < tabs.length-1 ? onTab.next() : tabs.first();
        nextTab.click();

        plusSlides(1);
        
    }, 5000);
});

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("tab-pane fade");
  var dots = document.getElementsByClassName("nav nav-tabs");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}

  for (i = 0; i < slides.length; i++) {
      slides[i].className = slides[i].className.replace(" in active", "");
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  //slides[slideIndex-1].style.display = "block";
  slides[slideIndex-1].className += " in active";  
  dots[slideIndex-1].className += " active";
}
</script>