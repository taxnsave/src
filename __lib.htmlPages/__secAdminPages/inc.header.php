<div id="navbar" class="navbar h-navbar navbar-default ace-save-state navbar-fixed-top">
        <div class="navbar-container ace-save-state" id="navbar-container">
            <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                <span class="sr-only">Toggle sidebar</span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>
            </button>
            <div class="navbar-header pull-left">
                <a href="<?php echo $CONFIG->siteurl;?>mySaveTax" class="navbar-brand orange2">               
              <img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/tax_save_logo.png" alt="Administration Suits" style="height:30px;"/>
               </a>
            </div>
            <div class="navbar-buttons navbar-header pull-right" role="navigation" style="margin-top:0px;">
                <ul class="nav ace-nav">
                    <li class="green dropdown-modal">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="ace-icon fa fa-envelope-o  icon-animated-vertical"></i>
                            <span class="badge badge-success"><?php echo 1;?></span>
                        </a>
                        <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                            <li class="dropdown-header">
                                <i class="ace-icon fa fa-envelope-o"></i>
									Messages
                            </li>
                            <li class="dropdown-content">
                                <ul class="dropdown-menu dropdown-navbar"> 
                                    
                                </ul>
                            </li>
                            <li class="dropdown-footer">
                                <a href="?module_interface=<?php echo $commonFunction->setPage('msg');?>">
                                    Read Message(s)
                                    <i class="ace-icon fa fa-arrow-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- #section:basics/navbar.user_menu -->
                    <li class="light-blue dropdown-modal">
                        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                            <img class="nav-user-photo" id="profile_header_img" src="<?php echo $loggedUserImage;?>" alt="<?php echo $CONFIG->loggedUserName;?>" />
                            <span class="user-info">
                                <small>Welcome,</small>
                                <?php echo $_SESSION[$CONFIG->sessionPrefix.'a_user_name'];?>
                            </span>

                        <i class="ace-icon fa fa-caret-down"></i>	
                        </a>

                        <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                            <li>
                                <a href="?module_interface=<?php echo $commonFunction->setPage('profile');?>#settings">
                                    <i class="ace-icon fa fa-cog"></i>
                                    Settings
                                </a>
                            </li>

                            <li>
                                <a href="?module_interface=<?php echo $commonFunction->setPage('profile');?>">
                                    <i class="ace-icon fa fa-user"></i>
                                    Profile
                                </a>
                            </li>

                            <li class="divider"></li>

                            <li>
                                <a href="<?php echo $CONFIG->siteurl ; ?>logout.php">
                                    <i class="ace-icon fa fa-power-off"></i>
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </li>

                    <!-- /section:basics/navbar.user_menu -->
                </ul>
            </div>
            <!-- /section:basics/navbar.dropdown -->
        </div><!-- /.navbar-container -->
    </div>

    <!-- /section:basics/navbar.layout -->