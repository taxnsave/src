<?php
	//echo "<pre>";print_r($_POST);
	if($_REQUEST[user_action] == 'add' && $_REQUEST[offer_name] !='')
	{
		$mutualFund->saveOffer($_POST);
		$commonFunction->jsRedirect("?module_interface=".$commonFunction->setPage('nav_offer'));
		exit;
	}
	if($_REQUEST[user_action] == 'update' && $_REQUEST[offer_id] !='')
	{		
		$mutualFund->saveOffer($_POST);
		$commonFunction->jsRedirect("?module_interface=".$commonFunction->setPage('nav_offer'));
		exit;
	}
	if($_REQUEST[ae_offer_id] != '')
	{
		$offerDetails = $mutualFund->getNavOffer($_REQUEST[ae_offer_id]);		//print_r($userDetail);		
		$action = "update";		
		while(list($offerKey,$offerVal) = each($offerDetails))
		{	
			$nav_ids = $offerVal[offer_nav];
			$offer_id = $offerVal[pk_offer_id]; 
			$offer_name = $offerVal[offer_name]; 
			$offer_status = $offerVal[offer_status]; 
		}	
	}
	else
	{
		$action = "add";
		$nav_ids = '';
		$offer_id ='';		
		$rowDBCount = 0;	
		$nav_ids = 0;	
	}
?>
<div class="main-content">
    <div class="main-content-inner">
        <!-- #section:basics/content.breadcrumbs -->
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="<?php echo $CONFIG->siteurl;?>mySaveTax/">Home</a>
                </li>
                <li class="active">Mutual Fund</li><li class="active">Nav Offers</li>
            </ul>
            <?php include("form.search.php");?>           
        </div>
        <div class="page-content">						
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">
                                    <h4 class="widget-title orange">
                                        <i class="ace-icon fa fa-file-o green"></i>
                                        Add/Edit Offer
                                    </h4>                                                
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main no-padding">
                                        <div class="space-8"></div>
                                       
 <form name="_rule_ae_" id="_rule_ae_" method="post" action="">          
 <input type="hidden" name="module_interface" id="module_interface" value="<?php echo $commonFunction->setPage('nav_ae_offer');?>" />
<input type="hidden" name="user_action" id="user_action" value="<?php echo $action;?>" />
<input type="hidden" name="offer_id" id="offer_id" value="<?php echo $offer_id;?>" />
<input type="hidden" name="nav_ids" id="nav_ids" value="<?php echo $nav_ids; ?>" />    
                                     
                                    <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-top" for="aadd">Offer Name</label>
                                    <div class="col-sm-8">
                                    <input type="text" name="offer_name" id="offer_name" value="<?php echo $offer_name; ?>" />
                                    </div>
                                    </div>
                                    </div>
                                    <div class="space-8"></div>                                                   
                                    <div class="row">
                                    <div class="form-group">
										<label class="col-sm-3 control-label no-padding-top" for="duallist"> Select NAV(s)</label>
										<div class="col-sm-8">											
											<select multiple="multiple" size="10" name="duallistbox_demo1[]" id="duallist">
                                            <?php  $getAllNav = $mutualFund->getAllNAV(); //array('1','2','33','e','r');
													$offerArr = explode(",",$nav_ids);
												while(list($tagKey,$tagVal) = each($getAllNav))
												{
													$schemePrice = '&#8377; '.$tagVal[net_asset_value];
													$schemeDetails = $tagVal[Scheme_Name]." - ".$schemePrice;

													if($offer_name !='' && in_array($tagVal[pk_nav_id],$offerArr))
														$total_nav .= '<option value="'.$tagVal[pk_nav_id].'" SELECTED>'.$schemeDetails.'</Option>';
													else
														$total_nav .= '<option value="'.$tagVal[pk_nav_id].'" title="'.$schemePrice.'">'.$schemeDetails.'</Option>';
												}
												echo $total_nav;
											?>												
											</select>
											<div class="hr hr-16 hr-dotted"></div>
										</div>
									</div>
                                   </div>
                                   <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-top" for="aadd">Offer Status</label>
                                    <div class="col-sm-8">
                                    <select name="offer_status" id="offer_status">
                                    <option value="Active">Active</option>
                                    <option value="Inactive">Inactive</option>
                                    </select>
                                    </div>
                                    </div>
                                    </div>
                                    <div class="space-8"></div>   
                                   <div class="space-8"></div>
                                   <div class="row">
                                    <button class="btn btn-sm btn-success" type="submit">
                                            <span class="ace-icon ga glyphicon-plus  icon-on-right"></span>
                                            <?php echo ucfirst($action); ?> Offer
                                        </button><?php if($_SESSION["msg"]) { ?><small>
                                        <i class="ace-icon fa fa-times red2"></i>
                                        <span class="red"><?php echo $_SESSION["msg"];$_SESSION["msg"]='';?></span>
                                    </small><?php } ?>
                                   </div>
               </form>                                     
                                    </div><!-- /.widget-main -->
                                </div><!-- /.widget-body -->
                            </div><!-- /.widget-box -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->                  
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div>