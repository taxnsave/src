<?php
	//echo "<pre>";print_r($_SESSION);
	//$bankInfo = $customerProfile->getCustomerBankInfo();
	//print_r($bankInfo);
	//$mutualFund->updatePrice();
?>
<div class="main-content">
    <div class="main-content-inner">
        <!-- #section:basics/content.breadcrumbs -->
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="<?php echo $CONFIG->siteurl;?>mySaveTax/">Home</a>
                </li>
                <li class="active">ITR Forms</li><li class="active">List Forms</li>
            </ul>
            <?php include("form.search.php");?>           
        </div>
        <div class="page-content">						
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
<table class="table table-bordered table-striped">
    <thead class="thin-border-bottom">
        <tr>																
            <th>
                <i class="ace-icon fa fa-caret-right blue"></i>Customer Name 
            </th>
            <th>
                <i class="ace-icon fa fa-caret-right blue"></i>Assessment Year
            </th>
            <th class="hidden-480">
                <i class="ace-icon fa fa-caret-right blue"></i>ITR Status
            </th>
            <th class="hidden-480">
                <i class="ace-icon fa fa-caret-right blue"></i>XML
            </th>
            <th class="hidden-480">
                <i class="ace-icon fa fa-caret-right blue"></i>Upload Date
            </th>
        </tr>
    </thead>
    <tbody>
<?php
	$totalFiles = $itrFill->getAllEfillingCount('admin');
	$fileList = $itrFill->getAllEfilling('admin');
	if($totalFiles == 0)
		$fileHTML = '<tr><td class="center red"> No File(s) Found.</td></tr>';
	else
	{
		while(list($fileKey,$fileVal) = each($fileList))
		{	
			$arr = explode("-",$fileVal[asses_year]);		
?>														
    <tr>       
        <td>
            <b class=""><?php echo $customerProfile->getCustomerName($fileVal[fr_user_id]);?></b>
        </td>
        <td>
            <b class=""><?php echo $fileVal[asses_year];?></b>
        </td>
         <td>
            <b class="green">Pending</b>
        </td>
         <td>
            <span class="label label-warning"><strong><a href="<?php echo $CONFIG->siteurl;?>tempXMLAdmin.php?fr_itr_id=<?php echo $fileVal[pk_itr_id];?>&fr_user_id=<?php echo $fileVal[fr_user_id];?>" target="_blank" class="white pull-left"> Export To XML</a></strong></span>
        </td>
        <td class="hidden-480">
            <span class="label label-info arrowed-right arrowed-in"><?php echo $commonFunction->dateFormatWithTime($fileVal[form_created_date ]); ?></span>
        </td>        
    </tr>
<?php
		}
	}
?>

            </tbody>
        </table>                                                
     </div> 
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
			</div>