<?php
	//print_r($_REQUEST);
?>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="?module_interface=<?php echo $commonFunction->setPage('home');?>">Home</a>
                </li>
                <li class="active"><a href="?module_interface=<?php echo $commonFunction->setPage('user_list');?>">User Manager</a></li>
                <li class="active">Admin User Manager</li>
            </ul>
            <div class="nav-search" id="nav-search">
                <form class="form-search">
                    <span class="input-icon">
                        <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                        <i class="ace-icon fa fa-search nav-search-icon"></i>
                    </span>
                </form>
            </div>
        </div>
        <div class="page-content">						
            <div class="page-header">
                <h1>
                    Admin User Manager								
                </h1>
            </div>
            <div class="row">
                <div class="col-xs-12">								
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">                                    
                                       <div class="clearfix">
  											<div class="pull-right tableTools-container">
    											<div class="dt-buttons btn-overlap btn-group">
    												<a class="buttons-colvis btn btn-white btn-primary btn-bold" title="Add Admin Users">
                                                    	<span><i class="fa fa-users bigger-110 blue"></i></span>
                                                    </a>
                                                    <a class="buttons-colvis btn btn-white btn-primary btn-bold" title="Export To Excel">
                                                    	<span><i class="fa fa-database  bigger-110 pink"></i></span>
                                                    </a>                                                    
												</div>
											 </div>
                                        </div>                                   								
                                </div>
                                <div class="">
                                    <div class="">
                                        <table class="table table-bordered table-striped">
                                            <thead class="thin-border-bottom">
                                                <tr>
                                                    <th>
                                                        <i class="ace-icon fa fa-caret-right blue"></i>name
                                                    </th>

                                                    <th>
                                                        <i class="ace-icon fa fa-caret-right blue"></i>price
                                                    </th>

                                                    <th class="hidden-480">
                                                        <i class="ace-icon fa fa-caret-right blue"></i>status
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>MF1</td>
                                                    <td>
                                                        <small>
                                                            <s class="red">&#8377 29.99</s>
                                                        </small>
                                                        <b class="green">&#8377 19.99</b>
                                                    </td>

                                                    <td class="hidden-480">
                                                        <span class="label label-info arrowed-right arrowed-in">on sale</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>MF2</td>
                                                    <td>
                                                        <b class="blue">&#8377 16.45</b>
                                                    </td>
                                                    <td class="hidden-480">
                                                        <span class="label label-success arrowed-in arrowed-in-right">approved</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>MF3</td>
                                                    <td>
                                                        <b class="blue">$15.00</b>
                                                    </td>
                                                    <td class="hidden-480">
                                                        <span class="label label-danger arrowed">pending</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>MF4</td>
                                                    <td>
                                                        <small>
                                                            <s class="red"> &#8377 24.99</s>
                                                        </small>
                                                        <b class="green">&#8377 19.95</b>
                                                    </td>
                                                    <td class="hidden-480">
                                                        <span class="label arrowed">
                                                            <s>out of stock</s>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>MF5</td>
                                                    <td>
                                                        <b class="blue">&#8377 12.00</b>
                                                    </td>
                                                    <td class="hidden-480">
                                                        <span class="label label-warning arrowed arrowed-right">SOLD</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div><!-- /.widget-main -->
                                </div><!-- /.widget-body -->
                            </div><!-- /.widget-box -->
                        </div><!-- /.col -->

                        <!-- /.col -->
                    </div><!-- /.row -->
                    <div class="hr hr32 hr-dotted"></div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
			</div>