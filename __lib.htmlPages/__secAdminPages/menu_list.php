<?php
	//echo "<pre>";print_r($_SESSION);
	//$bankInfo = $customerProfile->getCustomerBankInfo();
	//print_r($bankInfo);
	//$mutualFund->updateInvestorFolio();
?>
<div class="main-content">
    <div class="main-content-inner">
        <!-- #section:basics/content.breadcrumbs -->
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="<?php echo $CONFIG->siteurl;?>mySaveTax/">Home</a>
                </li>
                 <li class="active">Website</li><li class="active">Menu List</li>
            </ul>            
        </div>
        <div class="page-content">						
            <div class="row">
                <div class="col-xs-12">                                                                              
                     <div class="space-8"></div>
                    <div class="row">
                        <div class="col-xs-12">		
                            <div class="clearfix">
                                <div class="pull-right">
                                     <button class="btn btn-app btn-inverse btn-xs" onclick="window.location.href='?module_interface=<?php echo $commonFunction->setPage('menu_website');?>';">
											<i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
											Add
									 </button>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
     <table class="table table-bordered table-striped table-hover dataTable">
         <thead class="thin-border-bottom">                                                 
            <tr>
                <th>Menu Name</th>																
                <th>Title</th>
                <th>URL</th>
                <th>Keyword</th>	
                <th>Description</th>
                <th>Position</th>
                <th>Status</th>
                 <th>Action</th>
            </tr>
        </thead>
    <tbody>
<?php

	if($_REQUEST[whichPage] == '')
		$page = 1;
	else
		$page = $_REQUEST[whichPage];
	
	$currentPage  	= $page;
	$totalItems		= $websiteContent->menuCount();
	$itemsPerPage	= $CONFIG->paginationPageItem;
	$getMFList		= $websiteContent->menuList('',($currentPage*$itemsPerPage)-$itemsPerPage);
	
	if(in_array('MF_NONE',$getMFList))
		echo $fileHTML = '<tr><td class="center red" colspan="8"> No Row(s) Found.</td></tr>';
	else
	{				
		$urlPattern = '?whichPage=(:num)&module_interface='.$_REQUEST['module_interface'];
		$paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
		
		while(list($logKey,$logVal) = each($getMFList))
		{		
?>
        <tr>
            <td class="blue"><b class="blue"><?php echo str_replace('"','',$logVal[link_name]); ?></b></td>
            <td>
                <?php echo str_replace('"','',$logVal[link_title]); ?>
            </td>
            <td>
                <b class="green"><?php echo $CONFIG->siteurl.str_replace('"','',$logVal[link_url]); ?></b>
            </td>
            <td><?php echo str_replace('"','',$logVal[link_keyword]); ?></td>
            <td><?php echo str_replace('"','',$logVal[link_description]); ?></td>
            <td><?php echo str_replace('"','',$logVal[link_position]); ?></td>
            <td><?php echo str_replace('"','',$logVal[link_status]); ?></td> 
            <td><a href="?module_interface=<?php echo $commonFunction->setPage('menu_website');?>&mid=<?php echo str_replace('"','',$logVal[pk_website_id]); ?>"><i class="ace-icon fa fa-pencil-square-o bigger-160"></i></a></td>              
        </tr>        
<?php
		}
		echo '<tr><td class="center red" colspan="8">'.$paginator.'</td></tr>'; 
	}
?>

														</tbody>
													</table>
                                                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
			</div>