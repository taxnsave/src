<?php
	header("Access-Control-Allow-Origin: *");	
	include("../../__lib.includes/config.inc.php");			//$pagename = $_GET[page] || $_GET[module_interface];	
	$_SESSION['oPageAccess'] = 3;
	

if($CONFIG->loggedAdminId)
{
	$pagename = $_REQUEST["module_interface"];
	if ($pagename == "") 
	{
		$pagename = "home";
	}
	else
	{
		$pagename = $commonFunction->getPage($pagename);	
	}
	//print_r($_REQUEST);//exit;
	$CONFIG->pageName = $pagename;
	$page = $pagename.".php";
	$_SESSION[$CONFIG->sessionPrefix.'page_name'] = $pagename;
	//$profileInfo = $customerProfile->getCustomerInfo($CONFIG->loggedUserId); 
	if($profileInfo['profile_image'] == '')
		$loggedUserImage = $CONFIG->siteurl."__UI.assets/postloginAssets/avatars/avatar2.png";
	else
	 	$loggedUserImage = $CONFIG->customerProfileImgURL.$profileInfo['profile_image'];
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />		
        <?php include_once('inc.header_includes.php')?>	
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <title>TaxSave | Administration Suit</title>
	</head>
	<body class="skin-2">
<?php
	
	include("inc.header.php");
	
	$page = $pagename.".php";	
?>	
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
				<?php 
				include("inc.left.php"); 
				
				include($page);

				include("inc.footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<?php include("inc.footer.js.php"); ?>
	</body>
</html>
<?php
}
else
{
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Secure Administration - saveTax Admin</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="../__UI.assets/postloginAssets/css/bootstrap.css" />
		<link rel="stylesheet" href="../__UI.assets/postloginAssets/css/font-awesome.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="../__UI.assets/postloginAssets/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="../__UI.assets/postloginAssets/css/ace.css" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="../__UI.assets/postloginAssets/css/ace-part2.css" />
		<![endif]-->
		<link rel="stylesheet" href="../__UI.assets/postloginAssets/css/ace-rtl.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="../__UI.assets/postloginAssets/css/ace-ie.css" />
		<![endif]-->

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="../__UI.assets/postloginAssets/js/html5shiv.js"></script>
		<script src="../__UI.assets/postloginAssets/js/respond.js"></script>
		<![endif]-->
	</head>

	<body class="login-layout blur-login">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center"><div class="space-6"></div><div class="space-6"></div>
								<h1>
									<i class="ace-icon fa fa-leaf green"></i>
									<span class="red">Admin</span>
									<span class="white" id="id-text2">Application</span>
								</h1>
								<h4 class="blue" id="id-company-text">&copy; TaxSave</h4>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-coffee green"></i>
												Please Enter Your Information
											</h4>

											<div class="space-6"></div>
<form class="adminLoginForm" role="form" method="post" action="../ajax-request/ajax_response.php?action=doAdminLogin&subaction=adminloginSubmit" onSubmit="adminLoginJS(this);return false;" name="login_frm" accept-charset="UTF-8" id="login-nav">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input name="username" id="username" type="email" class="form-control" placeholder="Username" required />
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input name="password" id="password" type="password" class="form-control" placeholder="Password" required/>
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">
														<div style="height:20px;"><div style="display:none;" id="login_loader"><img src="../static/img/formsubmitpreloader.gif"></div>
                          <div style="display:none;" id="invalid_email" class="orange"><strong>Invalid email or password!</strong></div></div>  

														<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
															<i class="ace-icon fa fa-key"></i>
															<span class="bigger-110">Login</span>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>											
											
										</div><!-- /.widget-main -->
										
									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->

								<!-- /.forgot-box -->

								<!-- /.signup-box -->
							</div><!-- /.position-relative -->

							
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="../__UI.assets/postloginAssets/components/jquery/dist/jquery.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="../assets/js/jquery1x.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='../__UI.assets/postloginAssets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
<script src="../__UI.assets/postloginAssets/components/jquery-validation/dist/jquery.validate.min.js"></script>
		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {			 
			});
			function adminLoginJS(form)
			{		
				$("#login_loader").css("display", "block");	
				$("#invalid_email").css("display", "none");
				$.ajax({
					url: form.action,
					type: form.method,
					data: $(form).serialize(),
					success: function(response) {
						if(response.indexOf('PASS')==0)
							location.href='<?php echo $CONFIG->siteurl;?>secureAdmin/';
						else
						{
							$("#login_loader").css("display", "none");
							$("#invalid_email").css("display", "block");
							$("#email").focus();
						}
					}            
				});
				return false;
			}		
		</script>
	</body>
</html>
<?php
}
?>