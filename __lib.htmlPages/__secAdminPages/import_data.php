<?php
	//echo "<pre>";print_r($_SESSION);
	//$bankInfo = $customerProfile->getCustomerBankInfo();
	//print_r($bankInfo);
	//echo $_SESSION[$CONFIG->sessionPrefix.'page_name'];
?>
<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo $CONFIG->siteurl;?>mySaveTax/">Home</a>
							</li>
							<li class="active">Mutual Fund Manager</li><li class="active">Import Data</li>
						</ul>
						<?php include("form.search.php");?>
					</div>
					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">						
						<div class="row">
							<div class="col-xs-12">                            
                            	<div class="row">
                                	<div class="widget-box">
                                        <div class="widget-header">
                                            <h4 class="widget-title">Import <strong class="orange">CAM</strong> DB</h4>
                                            <div class="pull-right">
                                            	<a class="btn btn-minier btn-primary" href="?data_type=cam&module_interface=<?php echo $commonFunction->setPage('mf_list_data'); ?>">
                                                <i class="ace-icon fa fa-database bigger-120"></i><strong>Available CAM Data</strong></a>
                                            </div>
                                        </div>
										<div class="widget-body">
                                            <div class="widget-main">
                                            	<div class="hide pull-right" id="after_upload_cam">                                	
                                    				<input type="hidden" name="form_data_cam" id="form_data_cam" value="" />
                                    <div id="fetchProgressbarInner_cam" class="ui-progressbar ui-widget ui-widget-content ui-corner-all progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="87"><div id="fetchProgressbarInner_cam" class="ui-progressbar-value ui-widget-header ui-corner-left progress-bar progress-bar-success" style="width: 77%;"><strong>Fetching all the data from uploaded files.....</strong></div></div>
                                  				</div>   
                                                <form class="form-inline">
                                                	<label class="inline" style="padding:4px;">
                                                        <div id="queue_cam"></div>
                                                        <input id="file_upload_cam" name="file_upload_cam" type="file" multiple="true">
                                                     </label> 
                                                </form>
                                            </div>                                                										
										</div>
									</div>
								</div>
                                <div class="space-8"></div>
                                <div class="row">
                                	<div class="widget-box">
                                        <div class="widget-header">
                                            <h4 class="widget-title">Import <strong class="orange">FRANKLIN</strong> DB</h4>
                                            <div class="pull-right">
                                            	<a class="btn btn-minier btn-primary" href="?data_type=franklin&module_interface=<?php echo $commonFunction->setPage('mf_list_data'); ?>">
                                                <i class="ace-icon fa fa-database bigger-120"></i><strong>Available FRANKLIN Data</strong></a>
                                            </div>
                                        </div>
										<div class="widget-body">
                                            <div class="widget-main">
                                            	<div class="hide pull-right" id="after_upload_franklin">                                	
                                    				<input type="hidden" name="form_data_franklin" id="form_data_franklin" value="" />
                                    <div id="fetchProgressbar_franklin" class="ui-progressbar ui-widget ui-widget-content ui-corner-all progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="87"><div id="fetchProgressbarInner_franklin" class="ui-progressbar-value ui-widget-header ui-corner-left progress-bar progress-bar-success" style="width: 77%;"><strong>Fetching all the data from uploaded files.....</strong></div></div>
                                  				</div>  
                                                <form class="form-inline">
                                                	<label class="inline" style="padding:4px;">
                                                        <div id="queue_franklin"></div>
                                                        <input id="file_upload_franklin" name="file_upload_franklin" type="file" multiple="true">
                                                     </label> 
                                                </form>
                                            </div>                                                										
										</div>
									</div>
								</div>
                                <div class="space-8"></div>
                                <div class="row">
                                	<div class="widget-box">
                                        <div class="widget-header">
                                            <h4 class="widget-title">Import <strong class="orange">KARVY</strong > DB</h4>
                                            <div class="pull-right">
                                            	<a class="btn btn-minier btn-primary" href="?data_type=karvy&module_interface=<?php echo $commonFunction->setPage('mf_list_data'); ?>">
                                                <i class="ace-icon fa fa-database bigger-120"></i><strong>Available KARVY Data</strong></a>
                                            </div>
                                        </div>
										<div class="widget-body">
                                            <div class="widget-main">
                                            	<div class="hide pull-right" id="after_upload_karvy">                                	
                                    				<input type="hidden" name="form_data_karvy" id="form_data_karvy" value="" />
                                    <div id="fetchProgressbar_karvy" class="ui-progressbar ui-widget ui-widget-content ui-corner-all progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="87"><div id="fetchProgressbarInner_karvy" class="ui-progressbar-value ui-widget-header ui-corner-left progress-bar progress-bar-success" style="width: 77%;"><strong>Fetching all the data from uploaded files.....</strong></div></div>
                                  				</div> 
                                                <form class="form-inline">
                                                	<label class="inline" style="padding:4px;">
                                                        <div id="queue_karvy"></div>
                                                        <input id="file_upload_karvy" name="file_upload_karvy" type="file" multiple="true">
                                                     </label>   
                                                </form>
                                            </div>                                                										
										</div>
									</div>
								</div>
                                <div class="space-8"></div>
                                <div class="row">
                                	<div class="widget-box">
                                        <div class="widget-header">
                                            <h4 class="widget-title">Import <strong class="orange">SUNDARAM</strong> DB</h4>
                                            <div class="pull-right">
                                            	<a class="btn btn-minier btn-primary" href="?data_type=sundram&module_interface=<?php echo $commonFunction->setPage('mf_list_data'); ?>">
                                                <i class="ace-icon fa fa-database bigger-120"></i><strong>Available SUNDARAM Data</strong></a>
                                            </div>
                                        </div>
										<div class="widget-body">
                                            <div class="widget-main">
                                            	<div class="hide pull-right" id="after_upload_sundram">                                	
                                    				<input type="hidden" name="form_data_sundram" id="form_data_sundram" value="" />
                                    <div id="fetchProgressbar_sundram" class="ui-progressbar ui-widget ui-widget-content ui-corner-all progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="87"><div id="fetchProgressbarInner_sundram" class="ui-progressbar-value ui-widget-header ui-corner-left progress-bar progress-bar-success" style="width: 77%;"><strong>Fetching all the data from uploaded files.....</strong></div></div>
                                  				</div> 
                                                <form class="form-inline">
                                                	<label class="inline" style="padding:4px;">
                                                        <div id="queue_sundram"></div>
                                                        <input id="file_upload_sundram" name="file_upload_sundram" type="file" multiple="true">
                                                     </label> 
                                                </form>
                                            </div>                                                										
										</div>
									</div>
								</div>
                                <div class="space-8"></div>
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div>