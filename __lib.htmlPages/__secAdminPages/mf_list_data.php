<?php
	
	//$CONFIG->customerId
	//if($_REQUEST['scheme'] != '' || $_REQUEST['[inv_name'] != '' || $_REQUEST['brok_code'] != '' || $_REQUEST['start'] != '')
	//{
	//	print_r($_REQUEST);
	//}
?>
<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo $CONFIG->siteurl;?>mySaveTax">Home</a>
							</li>
							<li class="active">Mutual Fund Manager</li>
                            <li class="active">RTA Data</li>
						</ul><!-- /.breadcrumb -->
						<?php //include("form.search.php");?>
					</div>
					<div class="page-content">						
						<div class="row">
							<div class="col-xs-12">
								<div class="row">
									<div class="col-xs-12">
										<div class="widget-box transparent">
											<div class="widget-header widget-header-flat">
												<h4 class="widget-title orange">
													<i class="ace-icon fa fa-database green"></i>
													<?php echo strtoupper($_REQUEST[data_type]); ?> Data
													<div class="pull-right">
                                                    <div class="btn-group">
														<button class="btn btn-sm btn-yellow"><b><?php echo strtoupper($_REQUEST[data_type]);?></b></button>
                                                        <button class="btn btn-sm btn-yellow dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="ace-icon fa fa-angle-down icon-only"></i>
                                                        </button>
                                                            <ul class="dropdown-menu dropdown-yellow">
                                                            <?php
																
																while(list($key,$val)=each($CONFIG->RTANames))
																{
																	$link = '?data_type='.$val.'&module_interface='.$commonFunction->setPage('mf_list_data');
																	if($_REQUEST[data_type] != $val)
																		echo '<li><a href="'.$link.'">'.strtoupper($val).'</a></li>';
																}
															?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </h4>                                                                                       
											</div>
											<div class="widget-body">
												<div class="widget-main no-padding">													 
<?php

	if($_REQUEST[whichPage] == '')
		$page = 1;
	else
		$page = $_REQUEST[whichPage];
			
	$totalItems = $mutualFund->totalCountRTA($_REQUEST[data_type]);
	$itemsPerPage = $CONFIG->paginationPageItem;
	$currentPage  = $page;
	$urlPattern = '?whichPage=(:num)&data_type='.$_REQUEST['data_type'].'&module_interface='.$_REQUEST['module_interface'];
	$paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
	
	$getMFList = $mutualFund->listRTA($_REQUEST[data_type],($currentPage*$itemsPerPage)-$itemsPerPage);
	
	if(!in_array('MF_NONE',$getMFList))
	{
?>
<div class="row search-area well  center">
    <form class="form-inline" action="?data_type=Search Result&module_interface=<?php echo $commonFunction->setPage('mf_search_data');?>" method="POST">
        <label class="inline">    
            <input type="text" name="scheme" id="scheme" value="" placeholder="Scheme Name">&nbsp;       
            <input type="text" name="inv_name" id="inv_name" value="" placeholder="Inv Name">&nbsp;          
            <input type="text" name="brok_code" id="brok_code" value="" placeholder="Brok Code">&nbsp;   
           <div class="col-xs-8 col-sm-4">
                <div class="input-daterange input-group">
                    <input type="text" name="start" class="input-sm form-control" placeholder="Start Trade Date">
                    <span class="input-group-addon">
                        <i class="fa fa-exchange"></i>
                    </span>
                    <input type="text" name="end" class="input-sm form-control" placeholder="End Trade Date">
                </div>
            </div>
         </label>                                                   
        <button class="btn btn-info btn-sm" type="submit">
            <i class="ace-icon fa fa-search bigger-110"></i><strong>Search</strong>
        </button>
   </form>    
</div>
<?php
	}
?>      
	<div class="row">  
	<table class="table table-bordered table-striped">
         <thead class="thin-border-bottom">                                                 
            <tr>
                <th>Folio No</th>																
                <th>Scheme</th>
                <th>Inv Name</th>	
                <th>Usercode</th>																
                <th>Purprice</th>																
                <th>Units</th>																
                <th>Amount</th>																
                <th>Trad Date</th>
                <th class="hidden-480">Brok Code</th>
            </tr>
        </thead>
    <tbody>
<?php
	
	if(in_array('MF_NONE',$getMFList))
		echo $fileHTML = '<tr><td class="center red" colspan="9"> No Row(s) Found.</td></tr>';
	else
	{		
		while(list($logKey,$logVal) = each($getMFList))
		{
			
?>
    
        <tr>
            <td><?php echo str_replace('"','',$logVal[0]); ?></td>
            <td>
                <b class="green"><?php echo str_replace('"','',$logVal[1]); ?></b>
            </td>
            <td>
                <b class="green"><?php echo str_replace('"','',$logVal[2]); ?></b>
            </td>
            <td><?php echo str_replace('"','',$logVal[3]); ?></td>
            <td><?php echo str_replace('"','',$logVal[4]); ?></td>
            <td><?php echo str_replace('"','',$logVal[5]); ?></td>
            <td><?php echo str_replace('"','',$logVal[6]); ?></td>
            <td><?php echo $commonFunction->date_format1(str_replace('"','',$logVal[7])); ?></td>
            <td><?php echo str_replace('"','',$logVal[8]); ?></td> 
        </tr>
<?php
		}
		echo '<tr><td class="center red" colspan="9">'.$paginator.'</td></tr>'; 
	}
?>

														</tbody>
													</table>
                                                    </div>
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
									</div><!-- /.col -->

									<!-- /.col -->
								</div><!-- /.row -->

								<div class="hr hr32 hr-dotted"></div>

								<!-- /.row -->

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div>