<!-- #section:basics/sidebar -->
			<div id="sidebar" class="sidebar h-sidebar no-gap lower-highlight responsive-min ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>
				<ul class="nav nav-list">
					<li class="<?php if($CONFIG->pageName == "home") echo 'active'; ?>" title="Dashboard">
						<a href="?module_interface=<?php echo $commonFunction->setPage('home');?>">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>
						<b class="arrow"></b>
					</li>
					<li class=" <?php if($CONFIG->pageName == "user_list" || $CONFIG->pageName == "user_manager" || $CONFIG->pageName == "subscription"
					) echo 'active'; ?>" title="User Manager">
						<a href="?module_interface=<?php echo $commonFunction->setPage('user_manager');?>" >
							<i class="menu-icon fa fa-users"></i>
								
                            <b class="arrow fa fa-angle-down"></b>					
						  </a>		                       
                        <b class="arrow"></b>	
                        <ul class="submenu">								
                            <li class="<?php if($CONFIG->pageName == "user_manager") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('user_manager');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									User Manager
								</a>
								<b class="arrow"></b>
							</li>	
                            <!--<li class="<?php if($CONFIG->pageName == "subscription") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('subscription');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Subscription Manager
								</a>
								<b class="arrow"></b>
							</li>	
                            <li class="" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('admin_role');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Admin Role Manager
								</a>
								<b class="arrow"></b>
							</li>	-->						
						</ul>                      				
					</li>
					<li class="<?php if($CONFIG->pageName == "user_itr") echo 'active'; ?>" title="Forms/ITR Manager">
						<a href="?module_interface=<?php echo $commonFunction->setPage('user_itr');?>">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text white"> Forms/ITR Manager</span>							
						</a>
						<b class="arrow"></b>
					</li>                     
					<li class="<?php if($CONFIG->pageName == "menu_website" || $CONFIG->pageName == "menu_list" || 
					$CONFIG->pageName == "banner") echo 'active'; ?>" title="Website Manager">
						<a href="#">
							<i class="menu-icon fa fa-pencil-square-o"></i>
							<span class="menu-text white"> Web Page Manager </span>
						</a>
						<b class="arrow"></b>	
                        <ul class="submenu">
							<li class="<?php if($CONFIG->pageName == "menu_list") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('menu_list');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Menu Manager
								</a>
								<b class="arrow"></b>
							</li>								
							<!--<li class="<?php if($CONFIG->pageName == "banner") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('banner');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Banner Manager
								</a>
								<b class="arrow"></b>
							</li>	-->						
						</ul>					
					</li>
					<li class="<?php if($CONFIG->pageName == "risk_portfolio" || $CONFIG->pageName == "import_data" || $CONFIG->pageName == "nav_master"
						|| $CONFIG->pageName == "nav_offer"	|| $CONFIG->pageName == "mf_list_data") echo 'active'; ?>" title="MFs Manager">
						<a class="dropdown-toggle" href="#">
							<i class="menu-icon fa fa-flask"></i>
							<span class="menu-text white">
								MFs Manager								
							</span>
                            <b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>
                        <ul class="submenu">
                        	<li class="<?php if($CONFIG->pageName == "nav_master") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('nav_master');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									 Mutual Fund Master
								</a>
								<b class="arrow"></b>
							</li>	
                            <li class="<?php if($CONFIG->pageName == "nav_offer") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('nav_offer');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									 Mutual Fund Offer
								</a>
								<b class="arrow"></b>
							</li>	
                        	<li class="<?php if($CONFIG->pageName == "folio_query") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('folio_query');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									 Folio Query
								</a>
								<b class="arrow"></b>
							</li>	
                            <li class="<?php if($CONFIG->pageName == "mf_list_data") echo 'active'; ?>" >
								<a href="?data_type=cam&module_interface=<?php echo $commonFunction->setPage('mf_list_data');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Data List Manager
								</a>
								<b class="arrow"></b>
							</li>	
							<!--<li class="<?php if($CONFIG->pageName == "risk_portfolio") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('risk_portfolio');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Portfolio Manager
								</a>
								<b class="arrow"></b>
							</li>	-->
                            <li class="<?php if($CONFIG->pageName == "import_data") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('import_data'); ?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Import MF Manager
								</a>
								<b class="arrow"></b>
							</li>							
						</ul>	
					</li>                    
					<li class="<?php if($CONFIG->pageName == "order_manager") echo 'active'; ?>" title="Order Manager">
						<a href="?module_interface=<?php echo $commonFunction->setPage('order_manager');?>">
							<i class="menu-icon fa fa-calendar"></i>
							<span class="menu-text white">
								Order Manager								
							</span>
						</a>
						<b class="arrow"></b>
					</li>     
                             
                    <?php	
					/*		
						
                    echo $runtimeHTML->isPermissionFound('Add_user',$CONFIG->permissionTable['Add_user'][0],'
					<li class="'.$subUserPage.'" title="Payment Manager">
						<a href="?module_interface='.$commonFunction->setPage('sub_users').'">
							<i class="menu-icon fa fa-briefcase"></i>
							<span class="menu-text white"> Payment Manager </span>
						</a>
						<b class="arrow"></b>
						<ul class="submenu">
							<li class="" >
								<a href="?module_interface='.$commonFunction->setPage('ae_rule').'">
									<i class="menu-icon fa fa-caret-right"></i>
									Revenue Manager
								</a>
								<b class="arrow"></b>
							</li>
							<li class="" >
								<a href="?module_interface='.$commonFunction->setPage('ae_rule').'">
									<i class="menu-icon fa fa-caret-right"></i>
									Discount Manager
								</a>
								<b class="arrow"></b>
							</li>							
						</ul>
					</li>');*/
                  ?>
                   <!--<li class="'.rulePage.'" title="Tools">
						<a href="?module_interface='.$commonFunction->setPage('report_tools').'">
							<i class="menu-icon fa fa-cogs"></i>
							<span class="menu-text white"> Tools </span>
						</a>
						<b class="arrow"></b>
						<ul class="submenu">
							<li class="" >
								<a href="?module_interface='.$commonFunction->setPage('ae_rule').'">
									<i class="menu-icon fa fa-caret-right"></i>
									Report
								</a>
								<b class="arrow"></b>
							</li>							
							<li class="" >
								<a href="?module_interface='.$commonFunction->setPage('ae_rule').'">
									<i class="menu-icon fa fa-caret-right"></i>
									Calculator
								</a>
								<b class="arrow"></b>
							</li>	
                            <li class="" >
								<a href="?module_interface='.$commonFunction->setPage('ae_rule').'">
									<i class="menu-icon fa fa-caret-right"></i>
									IT Slab Manager
								</a>
								<b class="arrow"></b>
							</li>							
						</ul>
					</li>-->
					<!--<li class="<?php if($CONFIG->pageName == "msg_list") echo 'active'; ?>" title="Mail/Message Manager">
						<a href="?module_interface=<?php echo $commonFunction->setPage('msg_list');?>">
							<i class="menu-icon fa fa-envelope-o"></i>
							<span class="menu-text white"> Mail/Message Manager </span>
						</a>
						<b class="arrow"></b>
                        <ul class="submenu">
							<li class="" >
								<a href="?module_interface='.$commonFunction->setPage('ae_rule').'">
									<i class="menu-icon fa fa-caret-right"></i>
									Message Manager
								</a>
								<b class="arrow"></b>
							</li>
                            <li class="" >
								<a href="?module_interface='.$commonFunction->setPage('ae_rule').'">
									<i class="menu-icon fa fa-caret-right"></i>
									News Letter Manager
								</a>
								<b class="arrow"></b>
							</li>	
                        </ul>
					</li>-->
                     <li class="<?php if($CONFIG->pageName == "adm_profile") echo 'active'; ?>" title="Profile">
						<a href="?module_interface=<?php echo $commonFunction->setPage('adm_profile');?>">
							<i class="menu-icon fa fa-user"></i>
							<span class="menu-text white"> Profile </span>
						</a>
						<b class="arrow"></b>
					</li>	
					<li class="<?php if($CONFIG->pageName == "account_log") echo 'active'; ?>" title="Account Logs">
						<a href="?module_interface=<?php echo $commonFunction->setPage('account_log');?>">
							<i class="menu-icon fa fa-industry"></i>
							<span class="menu-text white"> Activity Log </span>
						</a>
						<b class="arrow"></b>
					</li>					
				</ul><!-- /.nav-list -->
				<!-- #section:basics/sidebar.layout.minimize -->
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-save-state ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
				<!-- /section:basics/sidebar.layout.minimize -->
			</div>
			<!-- /section:basics/sidebar -->