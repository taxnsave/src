<?php
	//print_r($_REQUEST);
	if($_REQUEST['action'] == "syncBSE")
	{
		$getAllParam = $bseSync->createUserBSEString($_REQUEST['syncUserId']);
		//print_r($getAllParam);
		$getSoapMsg = $bseSync->userUpdateBSE($getAllParam['USER_PARAM'],$getAllParam['FATCA_PARAM']);
		$_SESSION['msg'] = $getSoapMsg; //print_r($getSoapMsg);
		$commonFunction->jsRedirect("?module_interface=".$commonFunction->setPage('user_manager'));
		exit;		
	}
?>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="?module_interface=<?php echo $commonFunction->setPage('home');?>">Home</a>
                </li>
                <li class="active"><a href="?module_interface=<?php echo $commonFunction->setPage('user_manager');?>">User Manager</a></li>               
            </ul>
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">								
                    <div class="row">
                    <?php if($_SESSION['msg'] !='') echo $_SESSION['msg']; $_SESSION['msg']=''; ?>
                        <div class="col-sm-12">
                            <div class="widget-box transparent">
                                <div class="widget-header widget-header-flat">                                    
                                       <div class="clearfix">
                                       		<h4 class="widget-title orange">
													<i class="ace-icon fa fa-users green"></i>
													Resgistered Users                                           
  											<div class="pull-right tableTools-container">
    											<div class="dt-buttons btn-overlap btn-group">
    												<!--<a class="buttons-colvis btn btn-white btn-primary btn-bold" title="Add Admin Users">
                                                    	<span><i class="fa fa-users bigger-110 blue"></i></span>
                                                    </a>
                                                    <a class="buttons-colvis btn btn-white btn-primary btn-bold" title="Export To Excel">
                                                    	<span><i class="fa fa-database  bigger-110 pink"></i></span>
                                                    </a>       -->                                             
												</div>
											 </div>
                                           </h4>
                                        </div>                                   								
                                </div>
                                <div class="">
                                    <div class="">
                                        <table class="table table-bordered table-striped">
                                            <thead class="thin-border-bottom">
                                                <tr>
                                                    <th>
                                                        <i class="ace-icon fa fa-caret-right blue"></i>Name
                                                    </th>
                                                    <th>
                                                        <i class="ace-icon fa fa-caret-right blue"></i>Login Id
                                                    </th>
                                                    <th class="hidden-480">
                                                        <i class="ace-icon fa fa-caret-right blue"></i>BSE Id
                                                    </th>
                                                     <th class="hidden-480">
                                                        <i class="ace-icon fa fa-caret-right blue"></i>Customer Id
                                                    </th>
                                                    <th class="hidden-480">
                                                        <i class="ace-icon fa fa-caret-right blue"></i>Join Date
                                                    </th>
                                                    <th class="hidden-480">
                                                        <i class="ace-icon fa fa-caret-right blue"></i>Status
                                                    </th>
                                                    <th class="hidden-480">
                                                        <i class="ace-icon fa fa-caret-right blue"></i>Sync BSE
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
<?php

	if($_REQUEST[whichPage] == '')
		$page = 1;
	else
		$page = $_REQUEST[whichPage];
			
	$totalItems = $customerProfile->getAllUsersCount($_REQUEST[data_type]);
	$itemsPerPage = $CONFIG->paginationPageItem;
	$currentPage  = $page;
	$urlPattern = '?whichPage=(:num)&data_type='.$_REQUEST['data_type'].'&module_interface='.$_REQUEST['module_interface'];
	$paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
	
	$getUserList = $customerProfile->getAllUsers($_REQUEST[data_type],($currentPage*$itemsPerPage)-$itemsPerPage);
	
	if(in_array('MF_NONE',$getUserList))
		echo $fileHTML = '<tr><td class="center red" colspan="9"> No Row(s) Found.</td></tr>';
	else
	{		
		while(list($logKey,$logVal) = each($getUserList))
		{
			//print_r($logVal);
?>                             
    <tr>
        <td><?php echo $logVal[cust_name]; ?></td>
        <td>
            <b class="green"><?php echo $logVal[login_id]; ?></b>
        </td>
        <td><?php echo $logVal[bse_id]; ?></td>
        <td><?php echo $logVal[fr_customer_id]; ?></td>
        <td><?php echo $logVal[created_date]; ?></td>
        <td><?php echo $logVal[user_status]; ?></td>
        <td><span class="label label-warning">
        <a class="white" href="?module_interface=<?php echo $_REQUEST['module_interface'];?>&action=syncBSE&syncUserId=<?php echo $logVal[pk_user_id]; ?>">Sync BSE</a>
        </span></td>
    </tr>
<?php
		}
		echo '<tr><td class="center red" colspan="9">'.$paginator.'</td></tr>'; 
	}
?>
                                                
                                            </tbody>
                                        </table>
                                    </div><!-- /.widget-main -->
                                </div><!-- /.widget-body -->
                            </div><!-- /.widget-box -->
                        </div><!-- /.col -->

                        <!-- /.col -->
                    </div><!-- /.row -->
                    <div class="hr hr32 hr-dotted"></div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
			</div>