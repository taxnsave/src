<?php
	//echo "<pre>";print_r($_REQUEST);
	//$bankInfo = $customerProfile->getCustomerBankInfo();
	//print_r($bankInfo);
	//$websiteContent->addMenu();
	if($_POST)
	{
		if($_POST[pk_website_id] =='')
			$websiteContent->addMenu($_REQUEST);
		else
			$websiteContent->updateMenu($_REQUEST);	
		
		$commonFunction->jsRedirect("?module_interface=".$commonFunction->setPage('menu_list'));
		exit;	
	}
	if($_REQUEST[mid] != '')
	{
		$pageRows = $websiteContent->getMenuById($_REQUEST);
		//print_r($pageRows);
	}
?>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="<?php echo $CONFIG->siteurl;?>mySaveTax/">Home</a>
                </li>
                <li class="active">Website</li><li class="active">Keyword/Menu</li>
            </ul>
        </div>
        <div class="page-content">						
            <div class="row">
                <div class="col-xs-12">                                                  
                     <div class="space-8"></div>                    
				     <form class="form-horizontal" action="" method="post">
                     	<div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Page Title </label>
							<div class="col-sm-9">
								<input type="text" id="link_title" name="link_title" placeholder="Page Title" class="col-xs-10 col-sm-5" value="<?php echo $pageRows[0]['link_title']; ?>" required /><input type="hidden" id="pk_website_id" name="pk_website_id"  value="<?php echo $_REQUEST['mid']; ?>" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Page Name </label>
							<div class="col-sm-9">
								<input type="text" id="link_name" name="link_name" placeholder="Page Link Name" class="col-xs-10 col-sm-5" value="<?php echo $pageRows[0]['link_name']; ?>" required />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Page URL </label>
							<div class="col-sm-9">
								<input type="text" id="link_url" name="link_url" placeholder="Page Link Name" class="col-xs-10 col-sm-5" value="<?php echo $pageRows[0]['link_url']; ?>" readonly="readonly" />
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Page Keyword </label>
							<div class="col-sm-4">
								<textarea id="link_keyword" name="link_keyword" class="autosize-transition form-control"><?php echo $pageRows[0]['link_keyword']; ?></textarea>
							</div>
						</div>
                         <div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Page Description </label>
							<div class="col-sm-4">
								<textarea id="link_description" name="link_description" class="autosize-transition form-control"><?php echo $pageRows[0]['link_description']; ?></textarea>
							</div>
						</div>
                         <!-- <div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Page Content </label>
							<div class="col-sm-9">
								<input type="text" id="link_content" name="link_content" placeholder="Page Link description" class="col-xs-10 col-sm-5" value="<?php echo $pageRows[0]['link_content']; ?>" />
							</div>
						</div>-->
                         <div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Page Link Position </label>
							<div class="col-sm-9">
								<select id="link_position" name="link_position">
                                	<option value="Header" <?php if($pageRows[0]['link_position'] == "Header") echo 'SELECTED';?>>Top/Header</option>
                                	<option value="Footer" <?php if($pageRows[0]['link_position'] == "Footer") echo 'SELECTED';?>>Bottom/Footer</option>
                                	<option value="Custom" <?php if($pageRows[0]['link_position'] == "Custom") echo 'SELECTED';?>>Custom</option>
                                </select>
							</div>
						</div>
                        <div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Page Link Status </label>
							<div class="col-sm-9">
								<select id="link_status" name="link_status">
                                	<option value="Active" <?php if($pageRows[0]['link_status'] == "Active") echo 'SELECTED';?>>Active</option>
                                	<option value="Inactive" <?php if($pageRows[0]['link_status'] == "Inactive") echo 'SELECTED';?>>Inactive</option>
                                </select>
							</div>
						</div>   
                         <div class="form-group">
							<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> &nbsp; </label>
							<div class="col-sm-9">
								<button class="btn btn-info btn-sm" type="submit">
                                            <i class="ace-icon fa fa-arrow bigger-110"></i><strong>Submit</strong>
                                </button>
							</div>
						</div>                      
                 </form>  
                </div>
            </div>
        </div>
    </div>
</div>