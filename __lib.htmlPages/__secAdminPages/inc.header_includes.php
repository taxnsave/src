<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/font-awesome/css/font-awesome.css" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/css/ace-fonts.css" />
		<link rel="stylesheet" href="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/css/daterangepicker.css" />
        <link rel="stylesheet" href="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/css/bootstrap-datepicker3.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->
        <?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "nav_ae_offer") { ?>   
        <link rel="stylesheet" href="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/bootstrap-duallistbox/dist/bootstrap-duallistbox.css" />
		<link rel="stylesheet" href="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/select2/dist/css/select2.css" /> 
         <?php } ?> 
		<link rel="stylesheet" href="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/css/ace-skins.css" />
		<link rel="stylesheet" href="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/css/ace-rtl.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/html5shiv/dist/html5shiv.min.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/respond/dest/respond.min.js"></script>
		<![endif]-->
		 <link rel="shortcut icon" href="<?php echo $CONFIG->siteurl; ?>images/favicon.ico">
 <?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "import_data") { ?>        
        <link rel="stylesheet" type="text/css" href="<?php echo $CONFIG->siteurl;?>__UI.assets/postloginAssets/uploadify/uploadify.css">
 <?php } ?>            