<?php
	//echo "<pre>";print_r($_SESSION);
	//$bankInfo = $customerProfile->getCustomerBankInfo();
	//print_r($bankInfo);
	//$mutualFund->updatePrice();
?>
<div class="main-content">
    <div class="main-content-inner">
        <!-- #section:basics/content.breadcrumbs -->
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="<?php echo $CONFIG->siteurl;?>mySaveTax/">Home</a>
                </li>
                <li class="active">Mutual Fund</li><li class="active">Nav Offers</li>
            </ul>
            <?php include("form.search.php");?>           
        </div>
        <div class="page-content">						
            <div class="row">
                <div class="col-xs-12">                            
                                               
                     <div class="space-4"></div>                   
                    <div class="row">
                        <div class="col-xs-12">		
                            <div class="pull-right">
                            	<span class="label label-warning">
                            	 <a href="?module_interface=<?php echo $commonFunction->setPage('nav_ae_offer');?>" class="white"><strong>Create Offer</strong></a>  
                                </span> 
                            </div>
                        </div>
                    </div>
                     <div class="space-4"></div>
                    <div class="row">
     <table id="simple-table" class="table table-bordered table-striped table-hover dataTable">
         <thead class="thin-border-bottom">                                                 
            <tr>            	
                <th>Sl. No.</th>																
                <th>Offer Name</th>
                <th>Scheme Name(s)</th>
                <th>Status</th>	
                <th>Action</th>	
            </tr>
        </thead>
    <tbody>
<?php

	if($_REQUEST[whichPage] == '')
		$page = 1;
	else
		$page = $_REQUEST[whichPage];
	
	$currentPage  	= $page;
	$totalItems		= $mutualFund->offerListCount($_REQUEST);
	$itemsPerPage	= $CONFIG->paginationPageItem;
	$getMFList		= $mutualFund->offerList($_REQUEST,($currentPage*$itemsPerPage)-$itemsPerPage);
	$count=1;
	if(in_array('MF_NONE',$getMFList))
		echo $fileHTML = '<tr><td class="center red" colspan="11"> No Row(s) Found.</td></tr>';
	else
	{				
		$urlPattern = '?whichPage=(:num)&search_str='.$_REQUEST['search_str'].'&module_interface='.$_REQUEST['module_interface'];
		$paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
		
		while(list($offerKey,$offerVal) = each($getMFList))
		{
			$arr = explode(",",$offerVal[offer_nav]);
			$schmeDetails = '';
			$navCount=1;
			while(list($logKey,$logVal) = each($arr))
			{
				$mfPrice = $buySell->getNAVPriceWithName($logVal,'*');	//print_r($mfPrice);					
				$schmeDetails .= '<div style="margin-bottom:5px;"><span class="badge badge-success">'.$navCount.'.</span> '.$mfPrice[0][fr_scheme_name].
								 ' - <span class="label label-danger">&#8377;'.$mfPrice[0][net_asset_value].'</span></div>';
				$navCount++;
			}
?>
        <tr>
        	<td>
               <?php echo $count; ?>.
            </td>
            <td class="blue"><b class="green"><?php echo str_replace('"','',$offerVal[offer_name]); ?></b></td>
            <td><?php echo $schmeDetails;?></td>
             <td class="show-details-btn"><b class="orange"><?php echo str_replace('"','',$offerVal[offer_status]); ?></b></td>
            <td><div class="action-buttons">
            <a class="blue" href="?ae_offer_id=<?php echo $offerVal[pk_offer_id];?>&module_interface=<?php echo $commonFunction->setPage('nav_ae_offer');?>" title="Edit">
                <i class="ace-icon fa fa-pencil bigger-130"></i>
            </a>            
            <a class="red" href="#" title="Delete" onClick="deleteFnf('<?php echo $offerVal[pk_offer_id];?>','module_interface=<?php echo $commonFunction->setPage('nav_ae_offer');?>','<?php echo str_replace('"','',$offerVal[offer_name]); ?>','mf_nav_offer');">
                <i class="ace-icon fa fa-trash-o bigger-130"></i>
            </a>
        </div></td>
        </tr>        
<?php
			$count++;
		}
		echo '<tr><td class="center red" colspan="11">'.$paginator.'</td></tr>'; 
	}
?>

            </tbody>
        </table>                                                
     </div> 
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
			</div>