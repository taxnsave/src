<div class="footer">
    <div class="footer-inner">
        <!-- #section:basics/footer -->
        <div class="footer-content">
            <span class="bigger-120">
                <span class="blue bolder">TaxSave</span>&nbsp;
                 &copy; 2017
            </span>

            &nbsp; &nbsp;            
        </div>
        <!-- /section:basics/footer -->
    </div>
</div>