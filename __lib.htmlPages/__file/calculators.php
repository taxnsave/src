<!-- Navbar -->
  <nav class="navbar navbar-inverse my-nav">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="<?php echo $CONFIG->siteurl;?>home.html"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png" alt="Logo" width="160"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li> 
        <li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
		<li><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
      </ul>
      <ul class="nav navbar-nav brdr">
        <li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
		<li><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create Will</a></li>
      </ul>
    </div>
  </div>
</nav>
  <!-- Navbar -->

<!-- Banner -->
<div class="banner_will">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
				<h1><center>We will help you manage your finance better</center><br> <center><span class="span-end">Demistify finance</span></center></h1>
				<center><span class="span-endline"></span></center>
			</div>
			
		</div>
	</div>
</div>
<!-- Banner -->


<section id="content" class="welth-content">
<div class="container-fluid">
    <div class="site-panel">
        <div class="container-fluid">
            <div class="row" style="height:10px"></div>
            <div class="row">
                <div class="col-md-12">
                <div class="content-box well learnPlanAndInvHide calCulatorsShow">
                    <h2><center>TaxSave calculators - Click on a calculator below to save more</center></h2>
                    <div class="list-group">
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#sipCal">SIP calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#lumpsumCal">Lumpsum calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#taxCal">TAX calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#fixdepCal">Fixed deposit calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#recdepCal">Recurring deposit calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#emiHomeCal">EMI Home Loan calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#emiCarCal">EMI Car Loan calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#emiPerCal">EMI Personal Loan calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#ppfCal">PPF calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#epfCal">EPF calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#npsCal">NPS calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#ssyCal">Sukanya Samriddhi Yojana(SSJ)</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#swpCal">SWP calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#hraCal">HRA calculator</a>
                        <a href="#" class="list-group-item" data-toggle="modal" data-target="#sipInsCal">SIP Installment Calculator</a>
                    </div>
                </div>
            </div>
            </div>
            
            <div class="row" style="height:50px"></div>
                    </div>
                </div>
    
    
<!-- All calculators modal start here -->
<!-- SIP calculator -->
<div class="modal fade" id="sipCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">SIP calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">SIP amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="sipInvAmt" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-3" for="">Monthly invested</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Invested for</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="sipNumYears" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">Year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Expected rate of return</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="sipIntRate" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Adjust for inflation</label>
                        <div class="col-sm-3">
                            <select name="" id="sipInfRateTaken" class="form-control">
                                <option value="5">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Inflation rate</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="sipInfRate" name="" placeholder="" value="5">
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="btn btn-primary" value="Check SIP" id="checkSIPBtn" name="">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="sipTotalInvAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Future value investment of</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="sipFVAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                If you invest Rs.***** per month for *** years @ **% P.A expected rate of return, you will accumulate Rs. ***** at the end of the **<sup>th</sup> year.
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <p>If you invest Rs.***** per month for *** years @ **% P.A expected rate of return, you will accumulate Rs. ***** at the end of the **<sup>th</sup> year</p>
                    </div> -->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#sipSTPShow';" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- Lumpsum calculator -->
<div class="modal fade" id="lumpsumCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Lumpsum calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Lumpsum amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="lumpsumInvAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Expected rate of return</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="lumpsumIntRate" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Time period</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="lumpsumNumYears" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">Year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Adjust for inflation</label>
                        <div class="col-sm-3">
                            <select name="" id="lumpsumAdjInfRate" class="form-control">
                                <option value="5">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Inflation rate</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="lumpsumInfRate" name="" placeholder="" value="5">
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="btn btn-primary" name="" value="Check Lumpsum" id="checkLumpsumBtn">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="lumpsumTotalInvAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Future value of investment</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="lumpsumFVAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                If you invest Rs.***** per month for *** years @ **% P.A expected rate of return, you will accumulate Rs. ***** at the end of the **<sup>th</sup> year.
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#lumpSumShow';" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- TAX calculator -->
<div class="modal fade" id="taxCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">TAX calculator</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-9 col-md-offset-2">
                    <p><strong>Investment limit under Sec 80C Rs.1,50,000</strong></p>
                <p><strong>how much are you investing annually in the following</strong></p>
                </div>
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Age</label>
                        <div class="col-sm-3">
                            <!-- <input type="text" class="form-control" id="taxAgeNum" name="" placeholder="" required> -->
                            <select class="form-control" id="taxAgeNum">
                                <option value="normal">0-59</option>
                                <option value="old">60-79</option>
                                <option value="seniorCitizen">>=80</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Equity linked saving scheme(ELSS)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxElssSchemeAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">life insurance premimum paid for self, spouse and children</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxLICSchemeAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Sukanya Samriddhi Yojana(SSY)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxSSYSchemeAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">5 years fixed deposit</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxFDAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">PPF investment</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxPPFAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">unit linked insurance plan</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxInsAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Any other 80C</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxOtherAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Your annual salary</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxAnnSal" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7">Check</label>
                        <div class="col-sm-3">
                            <input type="button" name="" id="taxCheckBtn" value="Check Tax" class="btn btn-primary">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Your current investment are</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxTotalInvAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Further investment opportunity</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxRemInvAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-7" for="">Tax saved through further investment</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="taxSaveAmt123" name="" placeholder="" readonly>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#saveTaxShow';" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- Fixed deposit calculator -->
<div class="modal fade" id="fixdepCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Fixed deposit calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="fixdepAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Invested for number of</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="fixdepNum" name="" placeholder="" required>
                        </div>
                        <div class="col-sm-3">
                            <select name="" id="fixdepNumType" class="form-control">
                                <option value="1">Years</option>
                                <option value="12">Months</option>
                                <option value="365">Days</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest rate</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="fixdepIntRate" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Frequency</label>
                        <div class="col-sm-3">
                            <select name="" id="fixdepIntType" class="form-control">
                                <option value="simpleInterest">Simple interest</option>
                                <option value="12">Monthly</option>
                                <option value="4">Quarterly</option>
                                <option value="2">Half yearly</option>
                                <option value="1">Annually</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" name="" class="btn btn-primary" value="Check Fixed Deposit" id="fdCheckBtn">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Maturity value of investment at the end of <span class="fixdepYearShow">**</span> Rs. </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="fixdepMatAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest earned </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="fixdepTotalInt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                If you invest Rs.<span id="fixdepInvestAmt">*****</span> per <span id="fixdepTimeShow">month</span> for <span class="fixdepYearShow">***</span> @ <span id="fixdepIntShow"></span>% P.A expected rate of return, you will accumulate Rs. <span id="fixdepTotalAmt">****</span> at the end of the <span class="fixdepYearShow">**</span>.
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Earn More then FD's by Investing MF's ".
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#lumpSumShow';" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- Recurring deposit calculator -->
<div class="modal fade" id="recdepCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Recurring deposit calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount invested monthly</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="rdAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Invested for no. of</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="rdNumYears" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">Year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest rate</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="rdIntRate" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="btn btn-primary" id="rdCheckBtn" name="" value="Check RD" placeholder="" >
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Maturity value of investment at the end of <span id="rdYearShow">**</span><sup>th</sup> year is Rs. </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="rdTotalAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Earn More then RD's by Investing MF's ".
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#sipSTPShow';" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- EMI calculator for home loan-->
<div class="modal fade" id="emiHomeCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">EMI calculator for home loan</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Housing Loan Amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="homeLoanAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest rate(P.A.)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="homeLoanIntRate" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Tenure (Yrs)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="homeLoanNumYears" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="homeLoanNumYears">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="btn btn-primary" id="homeLoanCheck" name="" placeholder="" value="Check Home Loan" required>
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">EMI amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="homeLoanEmiAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total amount payable</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="homeLoanTotalPayAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest component</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="homeLoanTotalInt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                <strong>Suggestion:</strong>
                                <br> Keep your loan tenure as low as possible, as the tenure increases interest component increases significantly.
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Invest in SIP's to reach your own house goal ".
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#ownHouseShow';" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- EMI calculator for car loan-->
<div class="modal fade" id="emiCarCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">EMI calculator for car loan</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Car Loan Amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="carLoanAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest rate(P.A.)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="carLoanIntRate" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Tenure (Yrs)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="carLoanNumYears" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="btn btn-primary" id="carLoanCheck" name="" placeholder="" value="Check Car Loan" required>
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">EMI amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="carLoanEmiAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total payble amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="carLoanPayAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="carLoanTotalInt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                <strong>Suggestion:</strong>
                                <br> Keep your loan tenure as low as possible, as the tenure increases interest component increases significantly.
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Invest in SIP's to reach your car goal ".
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#buyCarShow';" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- EMI calculator for personal loan-->
<div class="modal fade" id="emiPerCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">EMI calculator for personal loan</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Loan amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="perLoanAmt" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest rate(P.A.)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="perLoanIntRate" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Tenure (Yrs)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="perLoanNumYears" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="btn btn-primary" id="perLoanCheck" name="" placeholder=""
                            value="Check Personal Loan">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">EMI amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="perLoanEmiAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total amount payable</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="perLoanPayAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest component</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="perLoanTotalInt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                <strong>Suggestion:</strong>
                                <br> Keep your loan tenure as low as possible, as the tenure increases interest component increases significantly.
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Invest in SIP's for emergency fund with instant withdraw option".
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#sipSTPShow';" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- PPF(Public Provident Fund) calculator-->
<div class="modal fade" id="ppfCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">PPF(Public Provident Fund) calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">PPF Interest Rate</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ppfIntRate" name="" placeholder="" required readonly>
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ppfAmt" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">Per year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">No. of years</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ppfNumYear" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Investment made at</label>
                        <div class="col-sm-3">
                            <select name="" id="ppfRateType" class="form-control">
                                <option value="7.6">End of the period</option>
                                <option value="7.8">Begining of the period</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="form-control btn btn-primary" id="ppfCheck" name="" value="Check PPF">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total investment</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ppfTotalInvest" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total interest earned</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ppfTotalIntEarn" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total maturity amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ppfTotalMatAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-info">
                                <strong>TAX saving:</strong>
                                <br> Under Sec 80C.
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " ELSS(Tax Saving MF) is also exempted like PPF and can generate better return then PPF "
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#sipSTPShow';" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- EPF(Employee Provident Fund) calculator-->
<div class="modal fade" id="epfCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">EPF(Employee Provident Fund) calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <legend>Calculate maturity value</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Basic salary per month</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Your age</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Your retirement age</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Assuming</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-3" for="">% contribution</label>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Your contribution</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Employer's contribution (EPF+EPS)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total interest earned</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total maturity amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#sipSTPShow';" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- NPS(National Pension Scheme) calculator-->
<div class="modal fade" id="npsCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">NPS(National Pension Scheme) calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Current age</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="npsCurrAge" name="" placeholder="" required>
                        </div>
                        <label class="control-label col-sm-1" for="">year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Retirement age</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="npsRetAge" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-1" for="">year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total investing period</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="npsNumYears" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-1" for="">year</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">monthly contribution to be done</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="npsMonInvAmt" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Expected rate of return</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="npsIntRate" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5">Check</label>
                        <div class="col-sm-3">
                            <input type="button" name="" class="btn btn-primary" id="npsCheckBtn" value="Check NPS">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Principal amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="npsTotalInvAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest earned on investment</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="npsTotalIntAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Pension wealth generated</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="npsPensAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#retireRichShow';" class="btn btn-default">Start investing</button>
                <button type="button" class="btn btn-default">Email report</button>
            </div>
        </div>
    </div>
</div>
<!-- SSY(Sukanya Samriddhi Yojana) Calculator-->
<div class="modal fade" id="ssyCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">SSY(Sukanya Samriddhi Yojana) Calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount invested</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ssyAmt" name="" placeholder="" required>
                        </div>
                        <div class="col-sm-3">
                            <select name="" id="ssyTimePeriod" class="form-control">
                                <option value="1">Year</option>
                                <option value="12">Month</option>
                                <!-- <option value="">Month</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Interest rate</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ssyIntRate" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Investment started at year of</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ssyStartyear" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="form-control btn btn-primary" id="ssyCheck" name="" value="Check SSY">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Maturity year</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ssyMatYear" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total maturity amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="ssyTotalAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Invest in top MF's for child education, child marriage and for bright future of child . "
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#higherEduShow';" class="btn btn-default">Browse MF'S</button>
                <button type="button" class="btn btn-default">Email</button>
            </div>
        </div>
    </div>
</div>
<!-- SWP(Systematic Withdrawal Plans) Calculator-->
<div class="modal fade" id="swpCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">SWP(Systematic Withdrawal Plans) Calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Total Investment Amount</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Withdrawal per month</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Expected return</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                        <label class="control-label col-sm-1" for="">%</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Tenure (Yrs)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Value of the end of ** <sup>th</sup> year</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="" name="" placeholder="" readonly>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#sipSTPShow';" class="btn btn-default">Invest Now</button>
                <button type="button" class="btn btn-default">Email</button>
            </div>
        </div>
    </div>
</div>
<!-- HRA calculator-->
<div class="modal fade" id="hraCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">HRA calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Basic salary received</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="hraBscSal" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Dearness Allowance(DA) Received</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="hraDA" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">HRA received</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="hraRec" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Actual Rent Paid</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="hraActRentPad" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Select city</label>
                        <div class="col-sm-3">
                            <select name="" id="hraCity" class="form-control">
                                <option value="dl">Delhi</option>
                                <option value="mu">Mumbai</option>
                                <option value="ko">Kolkata</option>
                                <option value="ch">Chennai</option>
                                <option value="oth">Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="form-control btn btn-primary" id="HRA_Check" name="" value="Check">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">HRA exemption</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="hraExempt" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">HRA taxable</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="hrataxable" name="" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9 col-md-offset-2">
                            <div class="alert alert-warning">
                                " Invest in Tax saving mutual funds for saving TAX . "
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#saveTaxShow';" class="btn btn-default">Invest Now</button>
                <button type="button" class="btn btn-default">Email</button>
            </div>
        </div>
    </div>
</div>
<!-- SIP installment calculator -->
<div class="modal fade" id="sipInsCal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">SIP installment calculator</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/action_page.php">
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Amount you want to achieve</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="sipInsAmt" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">With in Number of Years</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="sipInsNumYears" name="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Risk undertaken</label>
                        <div class="col-sm-3">
                            <select class="form-control" id="sipInsRiskTaken" >
                                <option value="7">Low - 7%</option>
                                <option value="12">Moderate - 12%</option>
                                <option value="15">High - 15%</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5" for="">Rate of return (%)</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="sipInsIntRate" name="" placeholder="" value="7" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5"> Check</label>
                        <div class="col-sm-3">
                            <input type="button" class="btn btn-primary" id="checksipInsBtn" value="Check SIP Installment" name="">
                        </div>
                    </div>
                    <legend>Output</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-9" for="">Monthly SIP Amount to be invested to reach your goal</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="sipInsMonInsAmt" name="" placeholder="" readonly>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default">Recompute</button>
                <button type="button" onClick="javascript:window.location='planagoal.html#sipSTPShow';" class="btn btn-default">Invest Now</button>
                <button type="button" class="btn btn-default">Email</button>
            </div>
        </div>
    </div>
</div>
<!-- All calculators modal end here -->
            </div>
        </section>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="<?php echo $CONFIG->siteurl;?>__UI.assets/js/MyWealth.js"></script>
	<script type="text/javascript"
		src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
			google.charts.load('current', { 'packages': ['corechart'] });
			google.charts.setOnLoadCallback(drawChart);
			
			function drawChart() {
			
				var data = google.visualization.arrayToDataTable([
					['Amount', 'Toatal'],
					['Equity', 33],
					['Debt', 77]
				]);
			
				var options = {
					title: 'Investment allocation',
					left:20,
					top:0
				};
		
				var chart = new google.visualization.PieChart(document.getElementById('piechart'));
			
				chart.draw(data, options);
			}
		</script>

<script type="text/javascript">
    $(document).ready(function() {
    //alert("ready");
        openneededcalc(window.location.href);
});
    
    function openneededcalc(urlstring) {
    //alert(urlstring);
    if(urlstring.indexOf("#")>=0) {
        //var val = window.location.href;
        urlstring=urlstring.substring(urlstring.indexOf("#")+1);        
        //alert(urlstring);
        //var x = document.getElementById(urlstring);
        //x.style.display = "block";
        switch(urlstring) {
            case "hraCal" :
                $('#hraCal').modal('show');
                break;
            case "emiCal" :
                $('#emiHomeCal').modal('show');
                break;
            case "taxsavingCal" :
                $('#taxCal').modal('show');
                break;
            case "sipCal" :
                $('#sipCal').modal('show');
                break;
            case "itCal" :
                $('#taxCal').modal('show');
                break;
            case "lumpCal" :
                $('#lumpsumCal').modal('show');
                break;
        }
    }
}
</script>

