 <!-- Navbar -->
  <nav class="navbar navbar-inverse my-nav">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="<?php echo $CONFIG->siteurl;?>home.html"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png" alt="Logo" width="160"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li> 
        <li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
		<li><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
      </ul>
      <ul class="nav navbar-nav brdr">
        <li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
		<li><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create Will</a></li>
      </ul>
    </div>
  </div>
</nav>
  <!-- Navbar -->

  
<!-- Banner -->
<div class="banner">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-2">
				<h1>Money FIRST<br> <span class="span-start">SIMPLIFY,</span> <span class="span-end">You Need Only One Partner</span></h1>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="up-div-one">
					<h3>YOUR ITR</h3>
					<h4>Be Compliant, Be Happy</h4>
					<p>Upload Documents</p>	
					<p>Enter Additional Infomation</p>	
					<p>Reconcile Taxes</p>	
					<p>File Return</p>
					<button class="btn btn-plan center-block" onclick="location.href='filetax.html'">Our Plan <i class="fa fa-angle-double-right"></i></button>
				</div>
				<div class="up-div-two">
					<h3>YOUR WEALTH</h3>
					<h4>Reach Your Goals</h4>
					<p>Plan a Goal</p>	
					<p>Save Tax</p>	
					<p>Invest & Grow</p>	
					<p>Portfolio Management</p>
					<button class="btn btn-plan center-block" onclick="location.href='savetax.html'">Our Plan <i class="fa fa-angle-double-right"></i></button>
				</div>
				<div class="up-div-three">
					<h3>YOUR WILL</h3>
					<h4>Save for Families Future</h4>
					<p>Identify Assets</p>	
					<p>Select Beneficiary and Executor</p>	
					<p>Allocate Assets</p>	
					<p>Sign With Witness</p>
					<button class="btn btn-plan center-block" onclick="location.href='createwill.html'">Our Plan <i class="fa fa-angle-double-right"></i></button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Banner -->



<div class="page-wrapper">

<section class="support">
<div class="container">
<div class="row">
	<div class="col-xs-12 col-sm-5 col-md-3 col-md-offset-2">
		<div class="icon-bg">
			<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/icon-man.png" width="90">
			<button type="button" class="btn btn-post pull-right" onclick="location.href='contact.html'">Post Your Requirement</button>
		</div>
	</div>
	<div class="col-xs-12 col-sm-7 col-md-6">
		<h3 class="sec-h3">NEED EXPERT SUPPORT</h3>
		<p class="sec-p">Consult a CA, Financial Advisor & Lawyer</p>

	</div>
</div>
</div>
</section><!-- Support -->
<div class="clearfix"></div>


<section class="save-tax">
<div class="container">
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<h2 class="title-h2">WHY <span>TAX SAVE</span></h2>
		<p class="title-p">An integrated platform for individual financial management problems – problems related to tax compliances, 
		calculating expenses, ensuring savings, investing at the right time in right options, managing investments and ensuring your 
		legacies are preserved. All this is complex assuming the hassles of multiple accounts & forgotten passwords, different processes 
		for different activities and different contact points for each work. TaxSave automates each activity with an option to avail 
		advanced personalised services in a secured and simplified manner. Complex options in a financial world needs to be simplified 
		and to be addressed in more innovative ways. TaxSave and its team of experts brings to you tools and information to help you 
		achieve your financial goals and making compliances, savings and investing profitable fun and stress free.</p>
	</div>
</div>

<div class="timeline-sec">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 brdr-right">

<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-2">
		<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/icon-filing.png">
	</div>
	<div class="col-xs-8 col-sm-8 col-md-10">
		<h3>ITR Filing</h3>
	</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
		<ul class="time-url">
			<li>Identifying reliable CA’s with good track records.</li>
			<li>Real time advisory on complex matters.</li>
			<li>Correct computation of income.</li>
			<li>Multiple sources of incomes / multiple form 16 / risk of incorrect computation.<li>
			<li>Optimizing tax deductions.</li>
		</ul>
		<p><a href="filetax.html">Explore</a></p>
</div>
</div>
<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-2">
		<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/icon-wealth.png">
	</div>
	<div class="col-xs-8 col-sm-8 col-md-10">
		<h3>Wealth Management</h3>
	</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
		<ul class="time-url">
			<li>Finding trust worthy investments advisors.</li>
			<li>Too much paper work.</li>
			<li>Monitoring growth & risk in investments.</li>
		</ul>
		<p><a href="savetax.html">Explore</a></p>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
<div class="less-left">
<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-2">
		<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/icon-management.png">
	</div>
	<div class="col-xs-8 col-sm-8 col-md-10">
		<h3>Document Management</h3>
	</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
		<ul class="time-url">
			<li>Scattered information leading to perpetual chaos.</li>
			<li>Confusion leading to financial indiscipline.</li>
			<li>Failure to comply with legal and regulatory compliances.</li>
			<li>Personal financial management is very time consuming.</li>
		</ul>
		<p><a href="">Explore</a></p>
</div>
</div>
<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-2">
		<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/icon-creation.png">
	</div>
	<div class="col-xs-8 col-sm-8 col-md-10">
		<h3>Will Creation</h3>
	</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
		<ul class="time-url">
			<li>Getting appointment with lawyers.</li>
			<li>Appropriate guidance while creating a will.</li>
			<li>Confusion in legal terminology – nominee, beneficiary, executors ?</li>

		</ul>
		<p><a href="createwill.html">Explore</a></p>
</div>
</div>
</div>

</div>
</div>
</div><!-- Timeline Sec -->

</div>
</section><!-- Save Tax -->



<section class="calculator">
<div class="container">
<h2 class="title-h2">CALCULATOR</h2>
<div class="below-sec">
	<div class="row">
    
	<div class="col-xs-12 col-sm-4 col-md-3 col-md-offset-1">
		<button type="submit" onclick="window.location='calculators.html#taxsavingCal'" class="btn btn-calcu">Tax Saving Calculator</button>
		<button type="submit" onclick="window.location='calculators.html#itCal'" class="btn btn-calcu">Income Tax Calculator</button>
		<button type="submit" onclick="window.location='calculators.html#hraCal'" class="btn btn-calcu">HRA Calculator</button>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3">
		<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/calculator-img.png" alt="" width="300">
        
	</div>
	<div class="col-xs-12 col-sm-4 col-md-3 col-md-offset-1">
        <button type="submit" onclick="window.location='calculators.html#sipCal'" class="btn btn-calcu">SIP Calculator</button>
        <button type="submit" onclick="window.location='calculators.html#lumpCal'" class="btn btn-calcu">Lumpsum Calculator</button>
		<button type="submit" onclick="window.location='calculators.html#emiCal'" class="btn btn-calcu">EMI Calculator</button>
            
	</div>

	</div>
    <dic class="row">
    <center>    <button type="submit" onclick="window.location='calculators.html'" class="btn btn-calcu">Show more</button></center>
    </dic>
</div><!-- Below Sec -->
</div>
</section><!-- Calculator -->



<section class="financial-info">
<div class="container">
<h2 class="title-h2">CONSOLIDATE YOUR <span>FINANCIAL</span> INFORMATION</h2>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<p class="title-p">Access to financial information is difficult and clutters our life. Make choices best for you and your family. 
But does your family know about all your bank accounts,all your investments, all your properties, your taxation issues and your insurance policies.
Solve your problem, bring all on a single platform with one click.</p>
<button type="button" class="btn btn-register center-block" onclick="location.href='login.html'">REGISTER NOW</button>
	</div>
</div>
</div>
</section><!-- Financial Info -->

<!-- Request Call -->
<div class="bg-pink-call">
	<div class="container">
    	<div class="row">
        	<div class="col-md-6 col-sm-5 col-xs-12">
            	<div class="request-text">
                	<p class="request-one">Request a<br/><span>
                    Call</span></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-7 col-xs-12">
            	<div class="contact-form">
                  <div class="form-box">
                  	<form
							action="ajax-request/ajax_response.php?action=contactus&subaction=submit"
							method="post" name="contact-form" id="contact-form"
							onSubmit="contactusJSHome(this);return false;">
                        <input type="text" name="formname" id="formname" class="form-control input-contact" placeholder="Name" required>
                        <input type="email" name="formemail" id="formemail" class="form-control input-contact" placeholder="e mail" required>
                        <input type="text" name="formnumber" id="formnumber" class="form-control input-contact" placeholder="Mobile No." required>
                        <input type="hidden" name="formmessage" id="formmessage" class="form-control input-contact" placeholder="Email" value="Contact from request a call back">
                       
						<button type="submit" class="btn btn-warning btn-submit">Submit</button></a>						
                    </form>
                     <div style="display: none;" id="contactresponsehome" class="orange">
													<strong>No responce received.</strong>
												</div>
                  </div>
                  
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Request Call -->


<!-- News & Blog -->

<section class="blog">
	<div class="container">
       <div class="row">
       		<div class="col-md-12">
            	<div class="head-title-news">
                	<p class="new-title">News <span>&amp;</span> Blog</p>
                    <p class="new-para">Read the news and blogs ..tax FAQs, investment tips, Government updates, notifications, money mantras to become rich and compliant. Don’t just save, invest and don’t just invest, track and don’t just file, track the same. </p>
                </div>
            </div>
       </div>
	   <div class="blog-sec">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="box">
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/news1.jpg" alt="" width="280"></center>
					<p>A balanced approach to Money Management</p>
					<ul class="blog-ul">
						<li>Save Tax</li>
						<li>Save for Growth</li>
						<li>Save for Family’s Future</li>
					</ul>
				<p><a href="">Read More</a></p>
				</div><!-- Box -->
			</div>
			
			<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="box">
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/news2.jpg" alt="" width="235"></center>
					<p>Fly with your dreams be it any place</p>
					<ul class="blog-ul">
						<li>what does it need – money, plan and right compliances.</li> 
						<li>VISA applications requires 3 years of your ITR</li>
					</ul>
				<p><a href="">Read More</a></p>
				<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/news4.jpg" alt="" width="200"></center>
				</div><!-- Box -->
			</div>
	   
	   		<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="box">
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/news3.jpg" alt="" width="235"></center>
					<p>Small savings today leads to big gains</p>
					<ul class="blog-ul">
						<li>Easy ways to invest your household savings</li>
						<li>Your birthday blessings can secure your education, find out how</li>  
					</ul>
				<p><a href="">Read More</a></p><br>
				<p>It's a Sunny day today</p>
					<ul class="blog-ul">
						<li>Be secure, today and always </li> 
					</ul>
				<p><a href="">Read More</a></p>
				
				</div><!-- Box -->
			</div>
	   
		</div>
	   </div><!-- Blog Sec -->
    </div>
</section>
<div class="clearfix"></div>
<!-- News & Blog -->




  
</div><!-- Page Wrapper -->  



<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
    
	function looksLikeMail(str) {
	    var lastAtPos = str.lastIndexOf('@');
	    var lastDotPos = str.lastIndexOf('.');
	    return (lastAtPos < lastDotPos && lastAtPos > 0 && str.indexOf('@@') == -1 && lastDotPos > 2 && (str.length - lastDotPos) > 2);
	}
	
	function contactusJSHome(form)
	{       
        alert("hi");
		if(!(looksLikeMail($("#formemail").val()))) 
		{
			alert("test");
			 $("#contactresponsehome").html('Please enter valid email address.');
		        $("#contactresponsehome").css("display", "block");
		        return false;
		}  
	            
// 	    $("#contactresponsehome").html('');		
// 	    $("#contactresponsehome").css("display", "none");
	    alert("hi");
	    $.ajax({
	        cache:false,
	        url: form.action,
	        type: form.method,
	        data: $(form).serialize(),
	        success: function(response) {
	        alert("success");
	            if(response.indexOf("CONTACT_SENT")>=0)
	            {
                    
	                $("#contactresponsehome").html("Thank you for contacting us. We will get back to you at the earliest, usually withing 24 hours. If it is urgent, please reach us on the phone numbers provided.");
	                $("#contactresponsehome").css("display", "block");
                    
                    $("#formemail").val("");
                    $("#formnumber").val("");
                    $("#formname").val("");
                    $("#formmessage").val("");
	            }
	        }            
	    });
	    return false;
	}

	</script>