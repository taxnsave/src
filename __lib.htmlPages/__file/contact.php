<!-- Navbar -->
  <nav class="navbar navbar-inverse my-nav">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="<?php echo $CONFIG->siteurl;?>home.html"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png" alt="Logo" width="160"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
        <li  class="active"><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li> 
        <li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
		<li><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
      </ul>
      <ul class="nav navbar-nav brdr">
        <li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
		<li ><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create Will</a></li>
      </ul>
    </div>
  </div>
</nav>
  <!-- Navbar -->

<!-- Banner -->
<div class="banner_will">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
				<h1><center>Reach us</center><br> <center><span class="span-end">Demistify finance</span></center></h1>
				<center><span class="span-endline"></span></center>
			</div>
			
		</div>
	</div>
</div>
<!-- Banner -->

<div class="page-contact-us-area section-spacing">
          <div class="container">
            <!--<div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="google-map-area">
                  <div id="googleMap" style="width:100%; height:424px;"></div>
                </div>
              </div>
            </div>-->
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="page-contact-us-left">
                  <h3 class="title">Feel Free To Drop Us A Line To Contact Us</h3>
                  <!--<p>Curabitur maximus feugiat velit, sed dapibus sem auctor quis. Maecenas turpis purus, tincidunt eget mattis ac, placerat sit amet dolor. Aenean vel porttitor libero, nec tempor magna. Mauris sed ex at tellus elementum tempus dignissim ac est. Curabitur maximus feugiat velit, sed dapibus sem auctor quis.</p>-->
                <div class="row">
                  <div class="contact-form"> 
                    <form
							action="ajax-request/ajax_response.php?action=contactus&subaction=submit"
							method="post" name="contact-form" id="contact-form"
							onSubmit="contactusJS(this);return false;">

                      <fieldset>
                          <div class="row">
                        <div class="col-sm-10">
                          <div class="form-group">
                            <input type="text" placeholder="Name*" class="form-control" name="formname" id="formname" data-error="Name is required" required>
                            <div class="help-block with-errors"></div>
                          </div>
                        </div>
                              </div>
                          <div class="row">
                        <div class="col-sm-10">
                          <div class="form-group">
                            <input type="email" placeholder="Email*" class="form-control" name="formemail" id="formemail" data-error="Email is required" required>
                            <div class="help-block with-errors"></div>
                          </div>
                        </div>
                              </div>
                              <div class="row">
                        <div class="col-sm-10">
                          <div class="form-group">
                            <input type="email" placeholder="Mobile number*" class="form-control" name="formnumber" id="formnumber" data-error="Mobile number is required" required>
                            <div class="help-block with-errors"></div>
                          </div>
                        </div>
                              </div>
                          <div class="row">
                        <div class="col-sm-10">
                          <div class="form-group">
                            <textarea placeholder="Message*" class="textarea form-control" name="formmessage" id="formmessage" rows="8" cols="20" data-error="Message is required" required></textarea>
                            <div class="help-block with-errors"></div>
                          </div>
                        </div>
                              </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                            <button type="submit" class="btn btn-sm btn-primary">Send Message</button>
                          </div>
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div style="display: none;" id="contactresponse" class="orange">
													<strong>No responce received.</strong>
												</div>
                  </div>
                </div>
                </div>
              </div>
              <!--<div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <iframe  src="https://mapsengine.google.com/map/embed?mid=z5Tj3tuDEdsY.kej7D3aHx0_M"></iframe>
                  </div>
                </div>-->
          
              <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="page-contact-us-right">
                  <h3 class="title">Reach us at :</h3>
                  <ul>
                     <li><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Dev Mantra Online Services Private Limited</li>
                     <li><i class="fa fa-paper-plane-o" aria-hidden="true"></i>CIN : U72900KA2018PTC111791</li>
                      <li><i class="fa fa-paper-plane-o" aria-hidden="true"></i>No. 85/1, CBI Main Road,</li>
                      <li><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Bangalore, Karnataka, 560 024</li>
                      <li><i class="fa fa-envelope-o" aria-hidden="true"></i> support@taxsave.in</li>
                  </ul>
                </div>
                <div class="page-contact-us-right">
                  <h3 class="title">Mumbai Office:</h3>
                  <ul>
                     <li><i class="fa fa-paper-plane-o" aria-hidden="true"></i>S12-92, Haware's Centurion Mall,</li>
                      <li><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Sector-19A, Sea Woods(E),</li>
                      <li><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Navi Mumbai- 400706</li>
	                  <li><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Maharashtra, India</li>
                  </ul>
                </div>
              </div>
                
            </div>
              <div class="row" style="height:60px">   </div>
          </div>
        </div>
  
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
    
	function looksLikeMail(str) {
	    var lastAtPos = str.lastIndexOf('@');
	    var lastDotPos = str.lastIndexOf('.');
	    return (lastAtPos < lastDotPos && lastAtPos > 0 && str.indexOf('@@') == -1 && lastDotPos > 2 && (str.length - lastDotPos) > 2);
	}
	
	function contactusJS(form)
	{       
        //alert("hi");
		if(!(looksLikeMail($("#formemail").val()))) 
		{
			//alert("test");
			 $("#contactresponse").html('Please enter valid email address.');
		        $("#contactresponse").css("display", "block");
		        return false;
		}  
	            
	    $("#contactresponse").html('');		
	    $("#contactresponse").css("display", "none");
	    alert("hi");
	    $.ajax({
	        cache:false,
	        url: form.action,
	        type: form.method,
	        data: $(form).serialize(),
	        success: function(response) {
	        //alert("success");
	            if(response.indexOf("CONTACT_SENT")>=0)
	            {
                    
	                $("#contactresponse").html("Thank you for contacting us. We will get back to you at the earliest, usually withing 24 hours. If it is urgent, please reach us on the phone numbers provided.");
	                $("#contactresponse").css("display", "block");
                    
                    $("#formemail").val("");
                    $("#formname").val("");
                    $("#formnumber").val("");
                    $("#formmessage").val("");
	            }
	        }            
	    });
	    return false;
	}

	</script>