 <!-- Navbar -->
  <nav class="navbar navbar-inverse my-nav">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="<?php echo $CONFIG->siteurl;?>home.html"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png" alt="Logo" width="160"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li> 
        <li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
		<li><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
      </ul>
      <ul class="nav navbar-nav brdr">
        <li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
		<li class="active"><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create Will</a></li>
      </ul>
    </div>
  </div>
</nav>
  <!-- Navbar -->

<!-- Banner -->
<div class="banner_will">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
				<h1><center>SAVE for family's future</center><br> <center><span class="span-end">SECURE your family's future</span></center></h1>
				<center><span class="span-endline"><a href="<?php echo $CONFIG->siteurl;?>login.html">Login to explore application</a></span></center>
			</div>
			
		</div>
	</div>
</div>
<!-- Banner -->



<div class="page-wrapper">

<section class="prepare-will">
<div class="container">


<div class="timeline-prepare">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 brdr-right">

<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-2">
		<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/Prepare_will_chac.png">
	</div>	
</div></div>
<div class="col-xs-12 col-sm-6 col-md-6">
<div class="less-left">
<div class="row">
		<div class="col-xs-8 col-sm-8 col-md-10">
		<h3 class="title-h3"><span>PREPARE</span> WILL YOURSELF</h3>
	</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
		<p class="prewill-text">
			A lawyers's assistance is not required when preparing a will. Most people can create a simple will by using intelligent software and updating their financial data in a diligent manner
		</p>		
</div>
</div>
</div>
</div>
</div>
</div><!-- Timeline Sec -->
</div>
</section><!-- prepare will -->

<section class="stamp-paper">
<div class="container">

<div class="timeline-stamp">
<div class="row">

<div class="col-xs-12 col-sm-6 col-md-6">
<div class="less-left">
<div class="row">
		<div class="col-xs-8 col-sm-8 col-md-10">
		<h3 class="title-h3"><span>STAMP PAPER</span> </h3>
	</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
		<p class="stamp-text">
			It is not compulsory that will need to be executed on stamp paper for registering your Will.
		</p>		
</div>
</div>
</div>
</div>
<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-2">
		<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/stamp_paper.png">
	</div>	
</div>
</div>
</div><!-- Timeline Sec -->
</div>
</section><!-- stamp Sec -->

<section class="property-claim">
<div class="container">


<div class="timeline-prepare">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 brdr-right">

<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-2">
		<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/property_icon.png">
	</div>	
</div></div>
<div class="col-xs-12 col-sm-6 col-md-6">
<div class="less-left">
<div class="row">
		<div class="col-xs-8 col-sm-8 col-md-10">
		<h3 class="title-h3"><span>PROPERTY</span> CLAIM</h3>
	</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
		<p class="prewill-text">
			Legally a nomination is just a facility to claim property by a nominee in the event of death of owner and nominee will only act as Trustee for temporary period till legal heir is established as per the Will or as per the Succession Act, thereafter nominee has to handover those properties to their legal heirs.
		</p>		
</div>
</div>
</div>
</div>
</div>
</div><!-- Timeline Sec -->
</div>
</section><!-- Property Claim -->

<section class="change-will">
<div class="container">

<div class="timeline-stamp">
<div class="row">

<div class="col-xs-12 col-sm-6 col-md-6">
<div class="less-left">
<div class="row">
		<div class="col-xs-8 col-sm-8 col-md-10">
		<h3 class="title-h3"><span>CHANGE WILL</span> </h3>
	</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
		<p class="stamp-text">
			A Will can always be revised or revoked by creating a new will or by adding an addition called a codicil.
		</p>		
</div>
</div>
</div>
</div>
<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-2">
		<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/changewill_icon.png">
	</div>	
</div>
</div>
</div><!-- Timeline Sec -->
</div>
</section>

<section class="gift-will">
<div class="container">


<div class="timeline-prepare">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 brdr-right">

<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-2">
		<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/giftwill_icon.png">
	</div>	
</div></div>
<div class="col-xs-12 col-sm-6 col-md-6">
<div class="less-left">
<div class="row">
		<div class="col-xs-8 col-sm-8 col-md-10">
		<h3 class="title-h3"><span>GIFT AND</span> WILL</h3>
	</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
		<p class="prewill-text">
			A Will and Gift Deed are not same. These two documents are entirely different. If a will is made by a testator, the property doesn’t immediately pass on to the hands of the beneficiary. It will devolve only at the time of the death of the testator. In a gift deed, the donee can receive it in the donor’s lifetime.
		</p>		
</div>
</div>
</div>
</div>
</div>
</div><!-- Timeline Sec -->
</div>
</section>

<section class="regis-will">
<div class="container">

<div class="timeline-stamp">
<div class="row">

<div class="col-xs-12 col-sm-6 col-md-6">
<div class="less-left">
<div class="row">
		<div class="col-xs-8 col-sm-8 col-md-10">
		<h3 class="title-h3"><span>REGISTRATION OF WILL</span> </h3>
	</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
		<p class="stamp-text">
			In India, the registration of Wills is not compulsory even if it relates to immoveable property. The registration of a document provides evidence that the proper parties had appeared before the registering officers and the latter had attested the same after ascertaining their identity. Notarisation or Registration of Will is Not Mandatory
		</p>		
</div>
</div>
</div>
</div>
<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-2">
		<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/registration_icon.png">
	</div>	
</div>
</div>
</div><!-- Timeline Sec -->
</div>
</section>

<section class="joint-pro">
<div class="container">


<div class="timeline-prepare">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 brdr-right">

<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-2">
		<img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/joint_icon.png">
	</div>	
</div></div>
<div class="col-xs-12 col-sm-6 col-md-6">
<div class="less-left">
<div class="row">
		<div class="col-xs-8 col-sm-8 col-md-10">
		<h3 class="title-h3"><span>JOINT </span>PROPERTIES</h3>
	</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
		<p class="prewill-text">
			Will needs to be executed for Joint Properties as well for the Share in the joint property. It does not naturally pass to the joint owner.
		</p>		
</div>
</div>
</div>
</div>
</div>
</div><!-- Timeline Sec -->
</div>
</section>



<div class="clearfix"></div>





  
</div><!-- Page Wrapper -->  