<!-- Navbar -->
  <nav class="navbar navbar-inverse my-nav">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="<?php echo $CONFIG->siteurl;?>home.html"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png" alt="Logo" width="160"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li> 
        <li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
		<li><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
      </ul>
      <ul class="nav navbar-nav brdr">
        <li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
		<li ><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create Will</a></li>
      </ul>
    </div>
  </div>
</nav>
  <!-- Navbar -->

<!-- Banner -->
<div class="banner_will">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
				<h1><center>Privacy Policy</center></h1>
			</div>
			
		</div>
	</div>
</div>
<!-- Banner -->


<div class="about-area-one section-spacing">
      <div class="inner-about">
        <div class="container">
            <div class="row" style="height:20px"></div>

         <div class="row" align="justify">
            
            <p>TaxSave recognizes that your privacy is very important and we take it seriously. This Privacy Policy (&ldquo;Privacy Policy&rdquo;) describes our policies and procedures on the collection, use, disclosure, and sharing of your personal information or personal data when you use the TaxSave Platform. This privacy policy is incorporated into and is subject to our Terms of Use. By using the TaxSave Platform, you accept our Terms and Conditions, Privacy Policy and Disclaimers of this website.</p>
<p>We collect personal information about our users in order provide our products, services, and customer support. Our products, services, and customer support are provided through many platforms including but not limited to: websites, phone apps, email, and telephone. The specific platform and product, service, or support you interact with may affect the personal data we collect.</p>
<p><strong><u>What information we collect.</u></strong></p>
<p>When you create an account and profile on the TaxSave Platform, we collect your name, contact information (including your telephone number and email) and verify the same. We collect further information while you use our services, such as for filing a Tax Return we collect details such as PAN, personal information, Date of Birth, Bank account details, income details and others as required by the Income Tax Authorities from time to time. We also advise you on investments in various financial products based on your goals. You provide data specific to your goals and the same gets captured to create your financial plan and advise on investments. The KYC requirements mandates collection of additional documents such as Address, Aadhar, etc apart from additional declaration.</p>
<p><strong><u>How we use personal information</u></strong><strong>.</strong></p>
<p>The information we request, collect, and process is primarily used to provide users with the product or service they have requested. More specifically, we may use your personal information for the following purposes:</p>
<ul>
<li>to provide the service or product you have requested;</li>
<li>to facilitate the creation of a User Contract (see Terms of Service for more information);</li>
<li>to provide technical or other support to you;</li>
<li>to answer enquiries about our services, or to respond to a complaint;</li>
<li>to promote our other programs, products or services which may be of interest to you (unless you have opted out from such communications);</li>
<li>to allow for debugging, testing and otherwise operate our platforms;</li>
<li>to conduct data analysis, research and otherwise build and improve our platforms;</li>
<li>to comply with legal and regulatory obligations;</li>
<li>if otherwise permitted or required by law; or</li>
</ul>
<p>For other purposes with your consent, unless you withdraw your consent for these purposes.</p>
<p><strong><u>When we disclose personal information.</u></strong></p>
<ol>
<li><strong>Our third party service providers</strong></li>
</ol>
<p>The personal information of users may be held or processed on our behalf including 'in the cloud', by our third party service providers. Our third party service providers are bound by contract to only use your personal information on our behalf, under our instructions. Our third party service providers include:</p>
<ul>
<li>Cloud hosting, storage, networking and related providers</li>
<li>SMS providers</li>
<li>Payment and banking providers</li>
<li>Marketing and analytics providers</li>
<li>Security providers</li>
</ul>
<ol start="2">
<li><strong>Third party applications</strong></li>
</ol>
<p>For various services to be provided, we need to create API integrations like BSE Star, ERI Integration, etc. Depending on the permissions that are granted, these applications may be able to access some personal information or do actions on the users' behalf. These third party applications are not controlled by us and will be governed by their own privacy policy.</p>
<ol start="3">
<li><strong>Statutory requirements</strong></li>
</ol>
<p>TaxSave shall disclose any information as necessary or appropriate to satisfy any law, regulation or other governmental request, to operate the site properly, or to protect itself or its customers.</p>
<ol start="4">
<li><strong>Web Statistics</strong></li>
</ol>
<p>We use log files generated by our web servers to analyze site usage and statistics but the said files do not identify any personal information. Log file analysis helps us to understand usage patterns on our website and to make improvements to our service</p>
<p>&nbsp;We are fully compliant with the Information Technology Act, 2000 of India and adhere to all its provisions and regulations.</p>
<p><strong><u>Accessing, correcting, or downloading your personal information</u></strong></p>
<p>Most personal information can be accessed by logging into your account. If you wish to access information that is not accessible through the platform, or wish to download all personal information we hold on you in a portable data format, please contact our Privacy Officer.</p>
<p>You also have the right to request the correction of the personal information we hold about you. All your personal information can be updated through the user settings pages. If you require assistance please contact our customer support.</p>
<u><p><strong>Changes in Privacy Policy.</strong></p></u>
<p>We may need to add or make changes in the Privacy Policy. We post details of any changes to our Privacy Policy on the website and you review the same on periodical basis.</p>
<p><strong><u>Security</u></strong></p>
<p>We take reasonable precautions to protect our users' information. Your account information is located on a secured server. Although we allow you to set privacy setting that limit access to your information, please be aware that no security measures are perfect or 100% effective. We cannot control the actions of other users with whom you may choose to share your pages and information. Even after removal, copies of Materials may remain viewable in cached and archived pages or if other users have copied or stored your Materials.</p>
<p>If you have an enquiry or a complaint about the way we handle your personal information, or to seek to exercise your privacy rights in relation to the personal information we hold about you, you may contact our Privacy Officer as follows:</p>
<p>By Email:<strong>support@taxsave.in</strong></p>
<p>For the purposes of the GDPR, our Privacy Officer is also our Data Protection Officer (DPO).</p>
<p>While we endeavour to resolve complaints quickly and informally, if you wish to proceed to a formal privacy complaint, we request that you make your complaint in writing to our Privacy Officer, by mail or email as above. We will acknowledge your formal complaint within 10 working days of receipt.</p>
            </div>
        </div>
      </div>
      
    </div>
    <div style="height:50px"></div>