    <!-- Navbar -->
  <nav class="navbar navbar-inverse my-nav">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="<?php echo $CONFIG->siteurl;?>home.html"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png" alt="Logo" width="160"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li> 
        <li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
		<li><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
      </ul>
      <ul class="nav navbar-nav brdr">
        <li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
        <li class="active"><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
		<li><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create Will</a></li>
      </ul>
    </div>
  </div>
</nav>
  <!-- Navbar -->
  
<!-- Banner -->
<div class="banner_inner">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
				<h1><center>Save Tax</center><br></h1>
				<center><span class="span-endline">Pour your taxes into your investments </span></center>
			</div>
			
		</div>
	</div>
</div>
<!-- Banner -->

<style>
<!--
.pointer {cursor: pointer;}
-->
</style>


<div class="page-wrapper">

<section class="easy-step">
	<div class="container">
       <div class="row">
       		<div class="col-md-12">
            	<div class="head-title-news">
                	<p class="savetax-title">Single Window <span>To Manage</span> Your Wealth</p>
                    
                </div>
            </div>
       </div>
	   <div class="savetax-sec">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="box">
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/risk_icon.png" alt="" width="68"></center>
					<p>RISK PROFILE</p>
					<p><span>Your risk profile should decide your<br/> investment strategy. Higher risk will<br/>yield higher return.</span></p>
					<button type="button" class="btn btn-step center-block">Check it out now</button>
				</div><!-- Box -->
			</div>
			
			<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="box">
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/Calc_icon.png" alt="" width="54"></center>
					<p>CALCULATORS</p>
					<p><span>Calculators to help you make<br/> better financial decisions<br/>&nbsp;</span></p>
					<button onClick="javascript:window.location='calculators.html'" type="button" class="btn btn-step center-block">Use them for free</button>
				
				</div><!-- Box -->
			</div>
	   
	   		<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="box">
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/asset_icon.png" alt="" width="62"></center>
					<p>ASSET ALLOCATION</p>
					<p><span>An Ideal portfolio should balance<br/> your liquidity returns. It is important<br/> to check your portfolio allocation.            </span></p>
					<button type="button" class="btn btn-step center-block">Check it out now</button>
				
				
				</div><!-- Box -->
			</div>

	   
		</div>
		   
		 <div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="box">
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/sip_icon.png" alt="" width="68"></center>
					<p>SIP / STP</p>
					<p><span>Turn market swings into<br/> opportunities by investing <br/>through SIP</span></p>
					<button type="button" onClick="javascript:window.location='planagoal.html#sipSTPShow';" class="btn btn-step center-block">Invest now</button>
				</div><!-- Box -->
			</div>
			
			<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="box">
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/lumsum_icon.png" alt="" width="62"></center>
					<p>LUMPSUM</p>
					<p><span>Top best Mutual funds for <br/> Lumpsum investment <br/>&nbsp;</span></p>
					<button type="button"  onClick="javascript:window.location='planagoal.html#lumpSumShow';" class="btn btn-step center-block">Invest now</button>
				
				</div><!-- Box -->
			</div>
	   
	   		<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="box">
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/taxsave_icon.png" alt="" width="50"></center>
					<p>TaxSave</p>
					<p><span>Save your taxes by investing  <br/> in top ELSS funds <br/>&nbsp;</span></p>
					<button type="button"  onClick="javascript:window.location='planagoal.html#saveTaxShow';" class="btn btn-step center-block">Invest now</button>
				
				
				</div><!-- Box -->
			</div>

	   
		</div>  
	   </div><!-- manage wealth-->
    </div>
</section>
	
	

<section class="plan-goal">
	<div class="container">
       
	   <div class="goal-text">
		<div class="row">
			
			<div class="col-xs-12 col-sm-8 col-md-8">
				<h2 class="title-h2">PLAN A <span>GOAL</span></h2>
                
				<div class="plans"><ul>
					<li class="pointer" onClick="javascript:window.location='planagoal.html#retireRichShow';">Retire Rich</li>
					<li class="pointer" onClick="javascript:window.location='planagoal.html#grandWeddingShow';">Grand Wedding</li>
					<li class="pointer" onClick="javascript:window.location='planagoal.html#higherEduShow';">Higher Education</li>
					<li class="pointer" onClick="javascript:window.location='planagoal.html#ownHouseShow';">Own A House</li></ul>
				</div>
				<div class="plans"><ul>
					<li class="pointer" onClick="javascript:window.location='planagoal.html#buyCarShow';">Buy A Car</li>
					<li class="pointer" onClick="javascript:window.location='planagoal.html#vacPlanShow';">Vacation Plan</li>
					<li class="pointer" onClick="javascript:window.location='planagoal.html#emerFundShow';">Emergency Fund</li>
					<li class="pointer" onClick="javascript:window.location='planagoal.html#uniGoalShow';">Unique Goal</li></ul>
				</div>
              
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4">
				
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/target_icon.png" alt=""></center></div>
			
			
				   
		</div>
	   </div><!-- Blog Sec -->
		
    </div>
</section>

<section class="invest-goal">
	<div class="container">
       
	   <div class="goal-text">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				
					<center><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>img/invest_option.png" alt=""></center></div>
			<div class="col-xs-12 col-sm-8 col-md-8">
				<h2 class="title-h2">BEST <span>INVESTMENT</span>OPTIONS</h2>
				<div class="plans"><ul>
					<li>Best performing Mutual funds</li>
					<li>Best Liquid funds</li>
					<li>Top 10 SIP mutual funds</li>
					<li>Best ELSS funds</li></ul>
				</div>
				<div class="plans"><ul>
					<li>Best Equity Mutual funds</li>
					<li>Best large cap funds</li>
					<li>Top Lumpsum investment funds</li>
					<li>Explore all the funds</li></ul>
				</div>
			</div>
			
			
			
				   
		</div>
	   </div><!-- Blog Sec -->
		
    </div>
</section>
	
	
  
</div><!-- Page Wrapper -->  