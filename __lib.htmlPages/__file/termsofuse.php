<!-- Navbar -->
  <nav class="navbar navbar-inverse my-nav">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="<?php echo $CONFIG->siteurl;?>home.html"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png" alt="Logo" width="160"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li ><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li> 
        <li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
		<li><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
      </ul>
      <ul class="nav navbar-nav brdr">
        <li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
        <li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
		<li ><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create Will</a></li>
      </ul>
    </div>
  </div>
</nav>
  <!-- Navbar -->

<!-- Banner -->
<div class="banner_will">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
				<h1><center>Terms of Use</center></h1>
			</div>
        </div>
        
       
        
		</div>
	</div>

<!-- Banner -->


<div class="about-area-one section-spacing">
      <div class="inner-about">
        <div class="container">
           <div class="row" style="height:20px"></div>
        
        <div class="row" align="justify" >
             <p><strong>Welcome to TaxSave!!</strong></p>
<p>TaxSave is an integrated platform to resolve individual financial management problems &ndash; problems related to tax compliances, calculating expenses, ensuring savings, investing at the right time in right options, managing investments and ensuring your legacies are preserved. All this is complex assuming the hassles of multiple accounts &amp; forgotten passwords, different processes for different activities and different contact points for each work.</p>
<p>Our Mission : To automate individual finance management with an option to avail advanced personalised services in a secured and simplified manner. TaxSave and its team of experts brings to you tools and information to help you achieve your financial goals and making compliances, savings and investing profitable fun and stress free.</p>
<p>TaxSave terms of service are to be read in conjunction with the Caveats mentioned below. Please note that we accept no responsibility or liability to any third parties for use of information, tools, applications, software and content. TaxSave will not be responsible for any damage or loss of action to any one, of any kind, in any manner, direct or indirect. Users and readers should seek appropriate professional advice before acting on the basis of any information contained herein or using any Software provided herein.</p>
<p>We&nbsp;reserve the right to periodically revise/up-date/modify the Terms of Use without prior notice. It is in your own interest that you review this document periodically for any updates and ensure your familiarity with the most current version. By continuing use of the website you agree to the modified Terms of Use.</p>
<p>By using the TaxSave Platform, you consent to these terms of service and it governs the use of the products and services we offer through our web and applications and constitutes the Agreement between TaxSave and You. Continued use of the website would be deemed as your acceptance to the Terms and Conditions, Privacy Policy and Disclaimers of this website.</p>
<p><strong><u>1. INTRODUCTION</u></strong></p>
<p>This document provides the Terms of Use under which a Visitor or a Registered User or Customer (&ldquo;you&rdquo;) may use the website&nbsp;- <a href="http://www.taxsave.in">www.taxsave.in</a> (&ldquo;Website&rdquo;), a portal of&nbsp;<strong>M/s&nbsp;Dev Mantra Online Services Private Limited, </strong>a Company duly incorporated under the Companies Act, 2013 and having CIN No U72900KA2018PTC111791 and its Registered office at No. 85/1, CBI Main Road, Bangalore &ndash; 560024 India.</p>
<p>TaxSave is an integrated financial services platform for individuals to provide the following services &ndash;</p>
<p><strong>1. File Tax Return and works as below &ndash; </strong></p>
<ul>
<li>Upload Documents</li>
<li>Enter Additional Information</li>
<li>Reconcile Taxes</li>
<li>File Return</li>
<li>Support notice assistance and Refund</li>
</ul>
<p><strong>2. Manage Wealth and includes as below - </strong></p>
<ul>
<li>Plan a Goal</li>
<li>Save Tax</li>
<li>Invest &amp; Grow</li>
<li>Portfolio Management</li>
<li>KYC Compliance</li>
</ul>
<p><strong>3. Will Creation and works as below - </strong></p>
<ul>
<li>Identify Assets</li>
<li>Select beneficiary and Executor</li>
<li>Allocate Assets</li>
<li>Print the Will</li>
<li>Sign with Witness</li>
</ul>
<p><strong>4. Manage Documents, also called as e Locker for the App - </strong></p>
<ul>
<li>Track refunds</li>
<li>Retrieve ITR</li>
<li>Track Investments</li>
<li>Preserve will</li>
<li>Preserve Masters</li>
</ul>
<p>A visitor needs to register/sign up on the website to use the services. The website enables him to manage all his finance related problems at one platform. The website provides automated on-line software/application, advice &amp; assistance, and also recommends personalised assistance of Chartered Accountants, Lawyers and other experts to manage his tax compliance, investment decisions and will creation. The website also facilitates user to store all these information at one central location, thereby enabling ease of data retrieval, called as the e Locker. Most of the features are designed in a manner to enable you to &ldquo;Do It Yourself&rdquo; (DIY) post you register for the website and you make the payments. The information, assistance and documentation provided is to guide a visitor or a registered user or a customer but does not constitute legal, financial or tax advice.&nbsp;</p>
<p><strong><u>2. USING THE TAXSAVE PLATFORM</u></strong></p>
<ol>
<li><strong>Who Can Use It. </strong>Use of the TaxSave Platform is permitted to anyone who are at least 18 years of age or otherwise have received parental consent and supervision when using the website. Your parent or legal guardian must consent to this Terms of Service and affirm that they accept this Agreement on your behalf and bear responsibility for your use.</li>
<li><strong>Registration. </strong>When you set up a profile on the TaxSave Platform, you will be asked to provide certain information about yourself. You agree to provide us accurate information, including your real name, when you create your account on the TaxSave Platform. We will treat information you provide as part of registration in accordance with our <a href="https://www.quora.com/about/privacy"><em>Privacy Policy</em></a>. This information can be shared with the Regulatory bodies like SEBI/RBI/IRDA/AMFI/Income Tax if and when sought from TaxSave by these regulators. It would be your responsibility to ensure that your details with us are most current and correct. Please note that to access this website as a registered user you would be required to have a &ldquo;Login ID&rdquo; and &ldquo;Password&rdquo;. TaxSave retains the right to allocate the Login ID to you as per their own discretion. You must ensure that you keep this Login ID and Password secure with yourself and do not share your password with anyone to prevent unauthorized access. TaxSave shall have no liability in case of unauthorized access that results with your sharing of password with others.</li>
<li><strong>Prohibited Use. </strong>You will not use the Website for any purpose that is unlawful or prohibited by these Terms. Users are responsible for complying with all local rules, laws, and regulations including, without limitation, rules about intellectual property rights, the Internet, technology, data, email, or privacy.</li>
<li><strong>Privacy Policy. </strong>Our privacy practices are set forth in our <a href="https://www.quora.com/about/privacy"><em>Privacy Policy</em></a>. By use of the TaxSave Platform, you agree to accept our <a href="https://www.quora.com/about/privacy"><em>Privacy Policy</em></a>, regardless of whether you are a registered user.</li>
<li><strong>Acceptable Use Policy.</strong> In your interaction with others on the TaxSave Platform, you agree to follow the <a href="https://www.quora.com/about/acceptable_use"><em>Acceptable Norms </em></a>&nbsp;at all times. You agree that you would not infect the website with viruses/ Trojans or any other malicious softwares that would impact the functioning of the website or any of its features adversely.</li>
<li><strong>Termination. </strong>You may close your account at any time by going to account settings and disabling your account. We may terminate or suspend your TaxSave account if you violate any TaxSave policy or for any other reason.</li>
<li><strong>Changes to the TaxSave Platform. </strong>We are always trying to improve your experience on the TaxSave Platform. We may need to add or change features and may do so without notice to you. TaxSave reserves the right at any time to modify, alter, or update these terms of use and Disclaimer without prior notice and you agree that you shall be bound by such modifications, alterations, or updates.</li>
<li>We welcome your feedback and suggestions about how to improve the TaxSave Platform. Feel free to submit feedback at <a href="https://www.quora.com/contact">TaxSave.in/contact</a>. By submitting feedback, you agree to grant us the right, at our discretion, to use, disclose and otherwise exploit the feedback, in whole or part, freely and without compensation to you.</li>
</ol>
<p><strong>3.&nbsp;<u>WEB RESOURCES AND THIRD-PARTY SERVICES</u></strong></p>
<p>The TaxSave Platform may also offer you the opportunity to visit links to other websites or to engage with third-party products or services. You assume all risk arising out of your use of such websites or resources. We cannot guarantee that these links will work at all times and have no control over the availability of the linked pages. In particular TaxSave is not responsible for any privacy policies on external websites. TaxSave does not endorse or review these websites and in no way warrant or guarantee the products, services, information, content, views, opinions, data offered on these websites and would be not responsible in any way for the same.</p>
<p>You may enable another online service provider, such as a social networking service, to be directly integrated into your account on the TaxSave Platform.</p>
<p>If you e-file your return at TaxSave.in, your return will be uploaded to the income tax department server through TaxSave's registered e-return intermediary certificate. By registering for TaxSave services, user(s) will be added as a TaxSave client at the Income Tax Department's website. You authorize TaxSave to periodically check your income tax return history and send alert in case of any requirement.</p>
<p>TaxSave has tied up with Bombay Stock Exchange (BSE) Star MF platform to process all your Mutual Fund Buy / Purchase / Switch / Systematic Investment Plan (SIP) / Systematic Transfer Plans (STP) / Systematic Withdrawal Plan (SWP) etc. and pass them to the relevant AMC and RTA for processing. The Payment Gateway available on the TaxSave website is also provided by BSE Star MF platform. TaxSave does not offer all mutual Fund schemes for investing for its registered users and is not obliged to do that. TaxSave would only not be able to offer schemes that are not available on the BSE Star MF platform. TaxSave only distributes AMC products and does not warranty or guaranty the AMC or any mutual fund schemes that they offer. Your details like PAN, Address Details, FATCA declaration, Bank Details and Documents, Signature, etc would be shared with BSE Star MF platform to facilitate your Mutual Fund order and Payment processing to be passed onto relevant AMCs and RTAs. Such records may be used by the TaxSave/BSE/AMC/RTA, in physical/offline/Online mode, for authorizing the transactions that have been submitted by you. TaxSave and BSE would be sending your messages on the Mobile Number and email ID provided by you to inform you status of your orders/ Investments/Portfolio/New schemes etc and this consent will override any registration for Do Not Call ("DNC") / National Do Not Call ("NDNC") registry. For investment in Systematic Investment Plans, your NACH/ECS mandate would need to be sent to BSE and further to your Bank for registration. Only post this successful registration and subsequent successful money transfer, your requests for subscriptions will be sent to the relevant AMCs. Subscription to the Fund Schemes is subject to realization of funds. The instructions would be processed for the same day if they are received before the cut off times prescribed by TaxSave and BSE. BSE and TaxSave reserve the right, at their sole discretion, to keep these cut offs ahead of the cut offs prescribed by AMCs and RTAs. The TaxSave and BSE cut offs are kept ahead of the AMC/RTA cut off keeping in mind the processing time required at their end for checking / validating and reconciling the order and the payments before passing them onto the AMCs/RTAs.</p>
<p>By enabling an integrated service, you are allowing us to pass to, and receive from, the Integrated Service Provider your log-in information and other user data. Note that your use of any Integrated Service Provider and its own handling of your data and information is governed solely by their terms of use, privacy policies, and other policies.</p>
<p>The website may hold advertising and other material from other third parties and TaxSave under no circumstances would accept any liability on the same. TaxSave is not responsible for any errors of omission or commission on account of these third party advertisements and content.</p>
<p><strong><u>4. ACCESS TO DATA AND INFORMATION</u></strong></p>
<p>TaxSave has a legal duty to protect the confidentiality of taxpayer information. We take all reasonable steps to protect any information you submit via our website, both online and offline, in accordance with relevant legislation in India. We take all appropriate steps to protect your personally identifiable information as you transmit your information from your computer to our site and to protect such information for loss, misuse, and unauthorised access, disclosure, alteration, or destruction. We use technologies to safeguard your data, and operate strict security standards to prevent any unauthorised access to it.</p>
<p>However, you acknowledge and agree that TaxSave has the right to monitor the Site electronically from time to time and to disclose any information as necessary or appropriate to satisfy any law, regulation or other governmental request, to operate the Site properly, or to protect itself or its customers. TaxSave will not intentionally monitor or disclose any private electronic-mail message unless required by law. TaxSave reserves the right to refuse to post or to remove any information or materials, in whole or in part, that, in its sole discretion, are unacceptable, undesirable, inappropriate or in violation of these Terms of Service</p>

<strong><u>5. SERVICES THAT REQUIRE SEPARATE AGREEMENT</u></strong>
<p>Certain features or services may require that you enter into a separate and supplemental written agreement prior to use. Services provided vide separate agreements shall be governed by respective Letter of Engagement.</p>
<strong><u>6. COPYRIGHT POLICY AND TRADEMARK POLICY</u></strong>
<p>The use of TaxSave website does not grant you any ownership of the Intellectual Property Rights, logo, brand, designs, algorithms, Trademarks owned by TaxSave. You are not allowed to use this without explicit approvals from TaxSave.</p>
<p><strong><u>7. PAYMENTS, CANCELLATION AND REFUND</u></strong></p>
<p>You can make payment for our online services in the manner set-out on the website including payment gateway and for offline services you can make payment by NEFT or cheque. We shall be liable to charge and recover the charges in respect to payment gateway from you in event you choose to make payment through payment gate way. GST would be added to the value of services or any other applicable taxes shall be levied. TaxSave has tied up with Bombay Stock Exchange (BSE) Star MF platform to process all your Mutual Fund Buy / Purchase / Switch / Systematic Investment Plan (SIP) / Systematic Transfer Plans (STP) / Systematic Withdrawal Plan (SWP) etc. and pass them to the relevant AMC and RTA for processing. The Payment Gateway available on the TaxSave website is also provided by BSE Star MF platform.</p>
<p>You make seek refund in case of any excess payment for reasons such system error, incorrect estimate or any other operational or technical issue subject to sole discretion to determine whether a particular case is appropriate for full refund or not. Any refunds made shall be subject to deduction of payment gateway charges by us, if applicable.</p>
<p><strong>8. DISCLAIMERS AND LIMITATION OF LIABILITY </strong></p>
<p><strong> PLEASE READ THIS SECTION CAREFULLY SINCE IT LIMITS THE <u>LIABILITY OF TAXSAVE ENTITIES TO YOU.</u></strong><u><br /></u></p>
<p>The material contained on this site and on the associated web pages is general information and is not intended to be advice on any particular matter. Users and readers should seek appropriate professional advice before acting on the basis of any information contained herein or using any Software provided herein. TaxSave, its directors, employees, agents, representatives and the authors expressly disclaim any and all liability to any person, whether a subscriber or not, in respect of anything and of the consequences of anything done or omitted to be done by any such person in reliance upon the contents of this site or use of software provided in this site and associated web pages.</p>
<p>The selection of recommended funds in TaxSave is not influenced by the commission earned by us. You also agree that TaxSave has not offered any Cash backs, Gifts or rebates from the commissions to influence you directly or indirectly to invest in the mutual fund schemes.</p>
<p>In no event shall TaxSave be liable for any direct, indirect, punitive, incidental, special, consequential damages or any damages whatsoever including, without limitation, damages for loss of use, data or profits, arising out of the use or performance or inability to use the TaxSave website or related services. We diligently work to ensure the accuracy of the calculations on every form prepared using Taxsave.in tax preparation software. The services related to Will Creation are completely professional in nature and we are not a Law Firm.</p>
<p>The site is provided "as is," without warranty of any kind, either express or implied, including without limitation, any warranty for information, data, services, uninterrupted access, or products provided through or in connection with the site. Specifically, TaxSave disclaims any and all warranties, including, but not limited to: (1) any warranties concerning the security, availability, accuracy, usefulness, or content of information, products or services and (2) any warranties of title, warranty of non-infringement, warranties of merchantability or fitness for a particular purpose. This disclaimer of liability applies to any damages or injury caused by any failure of performance, error, omission, interruption, deletion, defect, delay in operation or transmission, computer virus, communication line failure, theft or destruction or unauthorized access to, alteration of, or use of record, whether for breach of contract, tortuous behaviour, negligence, or under any other cause of action.</p>
<p>Users of the website must read the respective offer documents of the Mutual Funds carefully before investing. The estimates based on historical performance of the funds and are done to facilitate informed decision making for the investors. These do not constitute a promise or a guarantee a certainty of returns.</p>
<p>SEBI DISCLAIMER - Mutual fund investments are subject to market risks. Please read the scheme information and other related documents carefully before investing. Past performance is not indicative of future returns. Portfolio returns and allocation between equity and debt are estimated based on a number of factors including the user's risk profile, goal horizon and disclosed financial position. Performance of any investment portfolio can neither be predicted nor guaranteed.</p>
<p>TaxSave has taken great care and effort in doing research in putting together the information, data and analysis to facilitate and help you in taking investment decisions that you could be best for you to meet your investment goals. The information available on the TaxSave website, the recommendations and other data available does not amount to Investment Advice. You must take independent financial planning, legal, accounting, tax or other professional advice before investing or withdrawing from Mutual Funds schemes being offered by TaxSave on our website or through our Offline channels.</p>
<p>TaxSave would not be liable to any loss of interest and/ or opportunity loss and /or any loss arising due to movement of Net Asset Value (NAV), notional or otherwise, due to any subscription/redemption/SIP/STP/SWP etc order not being processed, rejected or being delayed at AMC, RTA, Bank, BSE or TaxSave due to manual or auto processing, breakdown of technology systems and connectivity, website out of service etc. Any order placed on a holiday or after cut off time on the day just before the holiday would be processed on the next business day and the NAV would be applicable as per the respective scheme related documents. All directions given by you for SIP/STP/SWP would be considered as the bona fide orders placed by you and would be sent for processing to BSE and subsequently to AMCs and RTAs. You agree to these terms and conditions by registering with TaxSave.</p>
<p>TaxSave has tied up with Bombay Stock Exchange (BSE) Star MF platform to process all your Mutual Fund Buy / Purchase / Switch / Systematic Investment Plan (SIP) / Systematic Transfer Plans (STP) / Systematic Withdrawal Plan (SWP) etc. and pass them to the relevant AMC and RTA for processing. The Payment Gateway available on the TaxSave website is also provided by BSE Star MF platform. TaxSave advises its customers to refer to the latest Prices/Rates/market values etc from time to time. Under no circumstances can TaxSave be held liable for any losses/capital losses etc. In no case shall TaxSave or its directors, employees, or authorized representatives be held liable for any direct, indirect, punitive, consequential ,special, or exemplary damages of any nature whatsoever, for any decision made or action taken in reliance upon the Services and Information.</p>
<p>TaxSave takes utmost care in the website data and its content, registered user and Visitor information is kept secure and safeguarded. However, the registered users and Visitors of the website fully understand and agree that TaxSave would not be held liable for any breaches on account of unscrupulous, illegal and unlawful access of this data through hacking or viruses. The Registered users and visitors waive their right to make any claims on TaxSave and its management, employees, authorized personnel and Directors. TaxSave makes no claims, warranties, guarantees, representations (either implicit or express) that the Information, Services, content etc. of the website are not infected by any malicious computer programmes or code, viruses etc. which has any destructive or contaminating properties. TaxSave shall have no liability in respect thereof. Technical reasons can be responsible for incorrectness, delays, shortcomings or defects of the information, content and services.</p>

<p><strong><u>9. INDEMNIFICATION.</u></strong></p>
<p>You agree to Indemnify, protect, fully compensate, defend and hold harmless, TaxSave and its Directors, Employees, Affiliates, Group Companies, Authorized Personnel, Sub Brokers, Agents, Representatives, contractors, from any losses imposed or claims asserted and incurred due to you accessing this website, your violation and infringement of the terms and conditions or any violations of intellectual property.</p>

<p><strong><u>10. GOVERNING LAW AND JURISDICTION.</u></strong></p>
<p>These terms and conditions shall be governed by and construed in accordance with the laws of India. Any dispute arising under these terms and conditions shall be subject to the exclusive jurisdiction of the courts of Bangalore, India.</p>

<p><strong><u>11. APPLICATIONS AND MOBILE DEVICES. </u></strong></p>
<p>If you access the TaxSave Platform through a TaxSave application, you acknowledge that this Agreement is between you and TaxSave only, and not with another application service provider or application platform provider (such as Apple Inc. or Google Inc.), which may provide you the application subject to its own terms. To the extent you access the TaxSave Platform through a mobile device, your wireless carrier&rsquo;s standard charges, data rates, and other fees may apply.</p>
<p><strong><u>12. ELECTRONIC COMMUNICATIONS. </u></strong></p>
<p>You consent to receive communications from us by email in accordance with this Agreement and applicable law. You acknowledge and agree that all agreements, notices, disclosures and other communications that we provide to you electronically will satisfy any legal requirement that such communications be in writing. TaxSave may use the contact information (phone number, email ids, etc.) shared by you to send you important communication related to latest tax updates, return filing process, reminders, product features, new releases, etc. via email, sms, whatsapp or any other such medium. If you do not wish to receive such emails, please <a href="https://www.taxspanner.com/unsubscribe-newsletter/">click here</a> to unsubscribe.</p>
<p>TaxSave shall use the Mobile number / Email Address / Address Details provided by you to communicate with you. You may opt out of some of these communications, if you deem fit. TaxSave would also generate alerts and news flash for you in your account. These could also be sent as SMS on your Mobile number or as emails on your email address provided to TaxSave. You hereby agree to receive all these communications and alerts from TaxSave.</p>
<p><strong><u>13. LEGAL JURISDICTION &amp; PERSONAL LAWS.</u></strong></p>
<ol>
<li>Our File Tax services are suitable for Tax Return Filing as per the provisions contained in the Income Tax Act, 1961.</li>
<li>Our Save Tax, including Investment in Mutual Fund services are available for both residents (RI) and non-residents Indians (NRI). However, NRIs need to comply with applicable provisions of FEMA, 1999.</li>
<li>Our Create Will services are designed to be suitable for the application of the law of India and can are designed for Indian citizens and NRIs with respect to assets / properties in India.</li>
<li>As per the Muslim Personal Law as prevailing in India there are certain restrictions in respect to a person practicing and following Islam from making a Will and hence same are specifically excluded from our services.</li>
<li>All communication that we send to you will assume that you have read this notice.</li>
</ol>
<p><strong><u>14. ENTIRE AGREEMENT/ SEVERABILITY</u></strong><strong>. </strong></p>
<p>This Agreement supersedes all prior terms, agreements, discussions and writings regarding the TaxSave Platform and constitutes the entire agreement between you and us regarding the TaxSave Platform (except as to services that require separate written agreement with us, in addition to this Agreement). If any provision in this Agreement is found to be unenforceable, then that provision will not affect the enforceability of the remaining provisions of the Agreement, which will remain in full force and effect.</p>
<p>All notices permitted or required under this Agreement, unless specified otherwise in this Agreement, must be sent in writing as follows in order to be valid: (i) if to you, by us via email to the address associated with your account, and (ii) if to us, by you via support<a href="mailto:Legal@TaxSave.com">@taxsave.</a>in. Notices will be deemed given (a) if to you, when emailed, and (b) if to us, on receipt by us.</p>
<p><strong><u>15.&nbsp;NOTICES.</u></strong></p>
<p>All notices permitted or required under this Agreement, unless specified otherwise in this Agreement, must be sent in writing as follows in order to be valid: (i) if to you, by us via email to the address associated with your account, and (ii) if to us, by you via support<a href="mailto:Legal@TaxSave.com">@taxsave.</a>in. Notices will be deemed given (a) if to you, when emailed, and (b) if to us, on receipt by us.</p>
<p><strong><u>16. RELATIONSHIP</u></strong><u>. </u></p>
<p>This Agreement does not create a joint venture, agency, partnership, or other form of joint enterprise between you and us. Except as expressly provided herein, neither party has the right, power, or authority to create any obligation or duty, express or implied, on behalf of the other.</p>
<p><strong><u>17. WAIVER</u></strong><u>. </u></p>
<p>No wavier of any terms will be deemed a further or continuing waiver of such term or any other term. Our failure to assert a right or provision under this Agreement will not constitute a waiver of such right or provision.</p>
<p><strong><u>18. FURTHER ASSURANCES.</u></strong></p>
<p>You agree to execute a hard copy of this Agreement and any other documents, and to take any actions at your expense that we may request to confirm and effect the intent of this Agreement and any of your rights or obligations under this Agreement.</p>
<p><strong><u>19. TERMINATION.</u></strong></p>
<p>At any stage if you are dissatisfied with any products, services, software and related material purchased or any portion of the TaxSave website, or with any of these terms of use, your sole and exclusive remedy is to discontinue the use of TaxSave website.</p>
<p><strong><u>20.&nbsp;CONTACT.</u></strong></p>
<p>Feel free to contact us through <a href="https://www.quora.com/contact">TaxSave.in/contact</a> with any questions about these terms. TaxSave is an India based entity located at No. 85/1, CBI Main Road, Bangalore &ndash; 560024, India.</p>
<p>Use the free <a href="https://html-cleaner.com/js/">JavaScript formatter</a> browser program to format your client-side scripts to optimize your code!</p>
            </div>
        </div>
      </div>
      
    </div>
    <div style="height:50px"></div>