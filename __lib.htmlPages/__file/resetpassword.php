<!-- Navbar -->
<nav class="navbar navbar-inverse my-nav">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand"
				href="<?php echo $CONFIG->siteurl;?>home.html"><img
				src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png"
				alt="Logo" width="160"></a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help Centre</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>login.html" class="nav-color">Login / Register</a></li>
			</ul>
			<ul class="nav navbar-nav brdr">
				<li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create
						Will</a></li>
			</ul>
		</div>
	</div>
</nav>
<!-- Navbar -->

<!-- Banner -->
<div class="banner_will">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
				<h1>
					<center>Sign In for a better future</center>
					<br>
					<center>
						<span class="span-end">SECURE your family's future</span>
					</center>
				</h1>
				<center>
					<span class="span-endline">Login to explore application</span>
				</center>
			</div>

		</div>
	</div>
</div>
<!-- Banner -->


<section id="page_content" class="">
	<div class="container">
			<div class="row">
			<center>
				<div class="col-xs-12">
					<div id="login-box" class="login-box visible widget-box no-border">
						<div class="widget-body">
							<div class="widget-main">
								<h4 class="header blue lighter bigger">
									<i class="ace-icon fa fa-sign-in green"></i> Reset password
								</h4>
								<div class="space-6"></div>
								<form class="loginForm" role="form" method="post"
									action="ajax-request/ajax_response.php?action=doChangePassword&subaction=loginSubmit"
									onSubmit="resetpasswordJS(this);return false;" name="login_frm"
									accept-charset="UTF-8" id="login-nav">
									<fieldset>
										<label class="block clearfix">
											<div class="input-group input-group-unstyled">
												<input type="email" name="resetemail" class="form-control"
													id="resetemail" placeholder="Email address"
													required> 
													<span class="input-group-addon"> <i
													class="ace-icon fa fa-user"></i>
												</span>
											</div>
										</label>
										<div class="space-6"></div>
										<label class="block clearfix">
									<div class="input-group input-group-unstyled">
										<input type="password" id="reset_passwd" name="reset_passwd"
											class="form-control form-control-lg required"
											placeholder="New Password" required> <span
											class="input-group-addon"> <i class="ace-icon fa fa-lock"></i>
										</span>
									</div>
								</label>
								<div class="space-6"></div>
								<label class="block clearfix"><div
										class="input-group input-group-unstyled">
										<input type="password" id="resrepasswd" name="resrepasswd"
											class="form-control form-control-lg  required"
											placeholder="Confirm Password" required> <span
											class="input-group-addon"> <i class="ace-icon fa fa-lock"></i>
										</span>
									</div> 
                                        </label>
                                        <input type="hidden" id="resetcode" name="resetcode"
											class="form-control form-control-lg "
											placeholder="Code" required>
										<div class="space-22"></div>

										<div class="clearfix">
											<button id="loginFrm" name="loginFrm" type="submit"
												class="width-35 pull-center btn btn-sm btn-primary">
												<i class="ace-icon fa fa-key"></i> <span class="bigger-110">Reset Password</span>
											</button>
											<div style="height: 30px;">
												<div style="display: none;" id="signup_loader">
													<img src="static/img/formsubmitpreloader.gif">
												</div>
												<div style="display: none;" id="resetfailed" class="orange">
													<strong>Could not reset password. Please ensure you use the link from the received e-mail.</strong>
												</div>
											</div>

										</div>


									</fieldset>
								</form>
								<div class="space-8"></div>
							</div>
						</div>
					</div>

					
					
					
			

					<div class="space-4"></div>
					<div class="space-22" style="height:50px"></div>
					<div class="space-24"></div>
				</div>
				</center>
			</div>
	</div>
</section>


<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">

//    alert("hi");
		//alert("test");
        var url_string = window.location.href;
        var url = new URL(url_string);
        var email = url.searchParams.get("a");
        var seccode = url.searchParams.get("forget_password");
//        alert(email + seccode);
        $("#resetemail").val(email);
        $("#resetcode").val(seccode);

    
	function looksLikeMail(str) {
	    var lastAtPos = str.lastIndexOf('@');
	    var lastDotPos = str.lastIndexOf('.');
	    return (lastAtPos < lastDotPos && lastAtPos > 0 && str.indexOf('@@') == -1 && lastDotPos > 2 && (str.length - lastDotPos) > 2);
	}
	
	function resetpasswordJS(form)
	{       
		if(!(looksLikeMail($("#resetemail").val()))) 
		{
			//alert("test");
			 $("#resetfailed").html('Please enter valid email address.');
		        $("#resetfailed").css("display", "block");
		        return false;
		}  
        if($("#resetcode").val()=="") {
            $("#resetfailed").html('Please reset the password from forgot password in login.');
		        $("#resetfailed").css("display", "block");
		        return false;
        }
	    if($("#reset_passwd").val().length < 6)
	    {
	        $("#resetfailed").html('Password must be 6 character long.');
	        $("#resetfailed").css("display", "block");
	        return false;
	    }		
	    if($("#reset_passwd").val() != $("#resrepasswd").val())
	    {
	        $("#resetfailed").html('Password Mismatch.');
	        $("#resetfailed").css("display", "block");
	        return false;
	    }
	            
	    $("#resetfailed").html('');		
	    $("#signup_loader").css("display", "block");	
	    $("#resetfailed").css("display", "none");
	    //alert("inside js");
	    $.ajax({
	        cache:false,
	        url: form.action,
	        type: form.method,
	        data: $(form).serialize(),
	        success: function(response) {
	        //alert("success");
	            if(response.indexOf("CODE_MISMATCH")>=0)
	            {
                    $("#signup_loader").css("display", "none");
	                $("#resetfailed").html('Please reset the password from forgot password in login.');
	                $("#resetfailed").css("display", "block");
	                $("#email").focus();
	            }
	            else if(response.indexOf("PASSWORD_CHANGED")>=0)
	            {									
	                alert("Password is reset. Please login with the new password.");
	                location.href='<?php echo $CONFIG->siteurl;?>login.html';				
	            }
	        }            
	    });
	    return false;
	}

	</script>