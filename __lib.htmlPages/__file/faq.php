
<!-- Navbar -->
<nav class="navbar navbar-inverse my-nav">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand"
				href="<?php echo $CONFIG->siteurl;?>home.html"><img
				src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png"
				alt="Logo" width="160"></a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="<?php echo $CONFIG->siteurl;?>home.html">Home</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>contact.html">Contact</a></li>
				<li class="active"><a href="<?php echo $CONFIG->siteurl;?>faq.html">Faq</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>helpcentre.html">Help
						Centre</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>login.html"
					class="nav-color">Login / Register</a></li>
			</ul>
			<ul class="nav navbar-nav brdr">
				<li><a href="<?php echo $CONFIG->siteurl;?>filetax.html">File Tax</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>savetax.html">Save Tax</a></li>
				<li><a href="<?php echo $CONFIG->siteurl;?>createwill.html">Create
						Will</a></li>
			</ul>
		</div>
	</div>
</nav>
<!-- Navbar -->

<!-- Banner -->
<div class="banner_will">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-1">
				<h1>
					<center></center>
				</h1>
				<span class="span-endline"><h2>
						<center>Get you finance questions answered in simple English</center>
					</h2></span>
			</div>

		</div>
	</div>
</div>
<!-- Banner -->

<style>
.accordion {
	background-color: #053002;
	color: #fff;
	cursor: pointer;
	padding: 5px;
	width: 100%;
	border: none;
	text-align: left;
	outline: none;
	font-size: 12px;
	transition: 0.4s;
    border-radius: 4px;
}

.accordion:active, .accordion:hover {
	background-color: #5b9b63;
}

.panel {
	padding: 0 18px;
	display: none;
	background-color: #eee;
	overflow: hidden;
    border-radius: 4px;
}

#myBtn {
	display: none;
	position: fixed;
	bottom: 20px;
	right: 30px;
	z-index: 99;
	font-size: 18px;
	border: none;
	outline: none;
	background-color: #777;
	color: white;
	cursor: pointer;
	padding: 15px;
	border-radius: 4px;
}

#myBtn:hover {
	background-color: #222;
}
    
.sidebar1 {

}
</style>


<section id="content" class="welth-content">
	<div class="container-fluid">
		<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>

		<div class="site-panel">
			<div class="container-fluid">
				<div class="row"></div>
				<div class="row">
					<div class="col-md-2">
						<div class="sidebar1">
							<h4 id="faqhome">
								<center>FAQ categories</center>
							</h4>
							<div class="list-group">
								<a href="#general" class="list-group-item">General FAQ</a> <a
									href="#salary" class="list-group-item">Salary FAQ</a> <a
									href="#tds" class="list-group-item">TDS</a> <a
									href="#houseproperty" class="list-group-item">House Property</a>
								<a href="#deductions" class="list-group-item">Deductions</a> <a
									href="#capitalgains" class="list-group-item">Capital Gains</a>
								<a href="#cashtransactions" class="list-group-item">Cash
									Transactions</a> <a href="#otherincome" class="list-group-item">Other
									Income</a> <a href="#bussandprof" class="list-group-item">Business
									and Profession</a> <a href="#setoffandcarryforward"
									class="list-group-item">Set off and carry forward</a> <a
									href="#assessment" class="list-group-item">Assessment</a>
							</div>
						</div>
					</div>


					<div class="col-md-8">
						<h2 id="general">General FAQ</h2>
						<button class="accordion">When is an assessee required to file
							return of Income u/s 139 even if his income is below exemption
							limit?</button>
						<div class="panel">
							<p>
								As per the provisions of Section 139, every assessee whose gross
								total income does not exceeds the basic exemption limit for any
								financial year, he will be relieved from filing the return for
								the said assessment year. <br>However, as per the amended
								provisions of section 139(1), where the gross total income of
								the assessee after including the exempted income u/s 10(38) i.e.
								Long term capital gain on sale of listed securities, exceeds the
								basic exemption limit, he shall be compulsorily required to file
								the Income Tax Return for the said financial year even if his
								net taxable income is less than the basic exemption limit. <br>This
								means that even if your taxable income is less than the basic
								exemption limit of Rs. 2,50,000 but because of exempt LTCG
								income, the total amount exceeds Rs 2,50,000, then filing of
								return of income is mandatory. <br>The above provisions are
								applicable with effect from 1.04.2017.
							</p>
						</div>

						<button class="accordion">Rebate u/s 87A for Small Income Tax
							Payers (Income < 5,00,000)</button>
						<div class="panel">
							<p>
								An Income Tax benefit for lower income bracket is provided u/s
								87A to an extent of <strong>INR 5000 [FY 2016-17]</strong> which
								has now reduced to <strong>INR 2500 [FY 2017-18]</strong> and
								onwards by the honorable FM.
							</p>
							<p>
								Section 87A rebate provides for rebate from Income Tax to <strong>RESIDENT
									INDIVIDUALS </strong>earning income below specified limit.
							</p>
							<p>
								<strong><u>For FY 2017-18</u></strong>
							</p>
							<p>The rebate u/s 87A can be claimed upon fulfillment of the
								following two conditions:</p>
							<ol>
								<li>The person claiming the rebate should be a <strong>RESIDENT
										INDIVIDUAL.</strong></li>
								<li>The person&rsquo;s total Income Less Deductions (under
									Section 80) is equal to or less than Rs 3,50,000</li>
							</ol>
							<p>If both the above conditions are satisfied, rebate of Rs 2,500
								will be available under Section 87A. The rebate is limited to Rs
								2,500. Which means if the total tax payable is lower than Rs
								2,500, such lower amount of tax will be the rebate under section
								87A. This rebate is applied on total tax before adding Education
								Cess(3%). This rebate is also available to Senior Citizens who
								are 60 years old but less than 80 years old.</p>
							<p>
								<strong><u>For FY 2016-17</u></strong>
							</p>
							<p>The rebate u/s 87A can be claimed upon fulfillment of the
								following two conditions:</p>
							<ol start="3">
								<li>The person claiming the rebate should be a <strong>RESIDENT
										INDIVIDUAL.</strong></li>
								<li>The person&rsquo;s total Income Less Deductions (under
									Section 80) is equal to or less than <strong>Rs 5,00,000.</strong>
								</li>
							</ol>
							<p>
								If both the above conditions are satisfied, <strong>rebate of
									lower of Rs.5000 or Total tax payable</strong> will be
								available under Section 87A. This rebate is applied on total tax
								before adding Education Cess(3%). This rebate is also available
								to Senior Citizens who are 60 years old but less than 80 years
								old.
							</p>
							<p>
								<strong><em><u>Note:</u></em></strong><em> NRI and assessee
									other than Resident Individual can&rsquo;t claim benefit under
									this section.</em>
							</p>
						</div>

						<button class="accordion">Distribution of surplus cash funds from
							companies (Private limited company)</button>
						<div class="panel">
							<p>The probable ways of withdrawal of funds / distribution of
								funds of a private limited company under different case and its
								Tax implication is discussed as hereunder:</p>
							<p>
								<strong><em>Case 1: In case of voluntary winding up of company</em></strong>
							</p>
							<p>
								In case a company has voluntarily passed the resolution for
								winding up / liquidation and the company has no other debts/
								loan / creditors outstanding at the time of liquidation, in such
								case the company can distribute the funds available with the
								company to the shareholders which could be in the nature of <strong>Income
									or Capital</strong>.
							</p>
							<ul>
								<li><strong><u>Income-</u></strong>if a distribution to members
									is from income generated by the company through trading
									activities or operation activities, then it will be <strong>deemed
										to be a dividend paid</strong> to members out of profits
									derived from the company i.e. distribution to the extent of
									accumulated Reserve &amp; Surplus (Retained Earnings) of the
									company would be termed as Deemed Dividend u/s 2(22)(d) in the
									hands of the Shareholders. On this dividend the company would
									be required to pay <strong>Dividend Distribution Tax (DDT) </strong>of
									15% + surcharge + Cess (16.995% effective Rate).</li>
							</ul>
							<p></p>
							<ul>
								<li><strong><u>Capital -</u></strong> Because the liquidation of
									a company gives rise to the cancellation of the shares in the
									company, a capital gain or loss could arise. In simple terms, a
									capital gain would arise for the shareholders if the capital
									proceeds from the cancellation of the shares exceed the cost
									base of the shares. The time the capital gain arises is when
									the shares are cancelled (i.e. at the time the company is
									dissolved). As such Capital Gain Tax (CGT) has to be calculated
									<strong>for the members</strong> in accordance to <strong>section
										46(2) of Income Tax Act</strong>.</li>
							</ul>
							<p>
								<strong><em>Case 2: In case of buy back of shares of company</em></strong>
							</p>
							<p>An obvious question that comes to one’s mind is whether
								buyback, as a means of surplus cash distribution, is more tax
								efficient over dividend pay-outs. The answer is not
								straightforward since it depends on several variable factors
								such as nature of shares (listed/unlisted), residential status
								of the investor, period of holding, etc.</p>
							<ul>
								<li><strong>Buyback of shares listed on recognised stock
										exchange in India</strong></li>
							</ul>
							<p>There are generally no tax implications in the hands of
								company on buyback of its shares under the modes prescribed
								under Securities and Exchange Board of India (Buy back of
								Securities) Regulations, 1998, since Dividend Distribution Tax
								(DDT) and buyback tax (BBT) are not triggered.</p>
							<p>The buyback of shares listed on a stock exchange can be
								considered as a tax-efficient mode of surplus cash distribution
								from the company’s standpoint.</p>
							<p>
								<strong>In the hands of the shareholders</strong> (irrespective
								of residential status), a buyback triggers capital gain tax
								since exemption under Section 10(34A) is available only in cases
								where BBT is paid by the company. The buyback of listed shares
								held for over a year, qualifies as long term capital gain (LTCG)
								and the same is tax exempt under Section 10(38) of the Act if
								shares are bought back before March 31, 2018.
							</p>
							<p>Else, the same may trigger capital gain tax implications in
								the hands of shareholders. However, MAT implications would get
								trigged in the hands of resident corporate shareholders.</p>
							<p>In case the shares are held for a period less than a year, the
								gains would qualify as short term capital gain (STCG) and would
								generally be taxable at an effective tax rate of 16.22%
								(inclusive of surcharge &amp; cess).</p>
							<p>It will be worthwhile to note that a non-resident investor
								possessing a valid Tax Residency Certificate can avail the
								beneficial provisions of the relevant Double Tax Avoidance
								Agreement entered into by the Indian government with its tax
								residence country.</p>
							<p>In case the shares are held as stock-in trade, then any gain
								arising out of buyback would be taxable as business income as
								per commercial principles.</p>
							<p>Apart from tax implications in the hands of investor, one also
								needs to understand the withholding implications in the hands of
								the company. No withholding is required on payments made to a
								resident investor on account of buyback of shares.</p>
							<p>However, if gains are arising to non-resident shareholders
								pursuant to buyback, the company is required to withhold tax at
								the applicable rates. Absence of adequate details of
								non-resident investors in case of buybacks executed over a stock
								exchange, could impose certain practical challenges for the
								company for discharging its withholding tax obligations.</p>
							<ul>
								<li><strong>Buyback of unlisted shares</strong></li>
							</ul>
							<p>A domestic company is required to pay BBT at the rate of
								23.072% (inclusive of surcharge and cess) on buyback of its
								unlisted shares implemented under any mode prescribed under the
								Companies Act, 1956. BBT is an additional tax in the hands of
								the domestic company over and above its income tax liability.</p>
							<p>In terms of Section 10(34A), a shareholder participating in
								such a buyback scheme enjoys tax exemption, irrespective of
								whether shares are held as capital assets or stock-in-trade and
								irrespective of availability of treaty benefit. Further, no MAT
								liability is triggered in the hands of a corporate shareholder.</p>
							<p>These provisions, compared with the provisions of taxability
								of dividend distribution, make the buyback of shares a tax
								efficient manner of distribution of surplus cash, especially in
								case of large shareholders subject to tax on dividend receipt
								(Individuals, HUFs, certain trusts, etc).</p>
						</div>


						<button class="accordion">Who are required to file Return?/ Is it
							mandatory to file return of income after getting PAN?</button>
						<div class="panel">
							<p>
							
							
							<li>It is not mandatory to file return of income after getting
								PAN.</li>
							<li>Return is to be filed only if you are liable to file return
								of income as under :</li>
							<li>If your Gross total Income for financial Year 2017-18 (Income
								before allowing Sec80 Deductions) is more than Rs.2,50,000/-.
								(The limit is Rs.3, 00,000/- for the persons of age between
								60-80years and Rs.5, 00,000/- for the persons above 80 years
								age).</li>
							<li>If you wish to claim Refund of taxes deducted/ paid.</li>
							<li>If you wish to carry forward losses to be set off in the next
								years return, then you have to declare losses for the current
								year by filing return.</li>
							<li>If you are resident and having any asset outside India
								orhaving financial interest in any entity outside India then you
								have to file return of income even though you are not having any
								other incomes.</li>
							<li>Return filing is compulsory even if you being a resident have
								signing authority in a foreign account.</li>
							<li>In the above two points resident means only a resident and
								ordinary resident (ROR); it does not include resident but not
								ordinary resident (RNOR) or Non resident (NRI).</li>
							<li>If you are having any exempt long term capital gains of more
								than Rs.2, 50,000/- in a financial year then you have to file
								the return compulsorily even though such gains are exempt from
								tax.(Here exempted long term capital gains include sale of
								equity shares, sale of units in equity oriented mutual funds,
								sale of units of a business trust)- effective form FY 2016-17.</li>
							<li>If you are in receipt of any income derived from property
								held under a charitable or religious trust or a political party,
								educational institution, hospital, trade unions, any non profit
								organization, any authority, body or a trust etc., you have to
								file the income tax return compulsorily.</li>
							<li>If you are planning to get any loan from bank or any other
								financial institution you may be asked copy of Income Tax
								Return.</li>
							<li>In respective of a firm or a company, Return of Income needs
								to be filed irrespective of the income/loss of the firm or
								company during the financial year.</li>
							</p>
						</div>

						<button class="accordion">If my income is below the taxable
							limits, can I still file my Tax Returns ?</button>
						<div class="panel">
							<p>Yes you can still file your income tax return but it is not
								mandatory</p>
						</div>

						<button class="accordion">Can you still file your Income Tax
							Return if there is any tax liability ?</button>
						<div class="panel">
							<p>If you file the return with Tax Liability, you will receive a
								notice from the department under sectin 139(9) stating the
								return filed as defective. You have to respond to the notice
								with in 15 days from the date of receipt of notice other wise
								the return will be treated as invalid that means the return is
								treated as if it is not filed at all</p>
						</div>

						<button class="accordion">What happens if we don&rsquo;t file the
							return within the due date?</button>
						<div class="panel">
							<p>A new section 234F has been inserted in Income Tax Act, 1961
								with effect from Assessment Year 2018-19 (Financial Year
								2017-18).</p>
							<p>&nbsp;</p>
							<p>Under this section, fee/penalty is levied if the Income-tax
								return is filed after the due date specified by the department.
								Earlier this penalty was levied by Assessing Officer at his
								discretion. But now, the same is payable before filing of
								Income-tax return.</p>
							<p>&nbsp;</p>
							<table width="0">
								<tbody>
									<tr>
										<td width="175">
											<p>
												<strong>Total Income</strong>
											</p>
										</td>
										<td width="366">
											<p>
												<strong>Return Filed</strong>
											</p>
										</td>
										<td width="172">
											<p>
												<strong>Fee/Penalty</strong>
											</p>
										</td>
									</tr>
									<tr>
										<td width="175">
											<p>
												<strong>Less than or</strong>
											</p>
											<p>
												<strong>equal to Rs. 5,00,000</strong>
											</p>
										</td>
										<td width="366">
											<p>Return filed any time after due date</p>
										</td>
										<td width="172">
											<p>Rs. 1,000</p>
										</td>
									</tr>
									<tr>
										<td rowspan="2" width="175">
											<p>
												<strong>More than</strong>
											</p>
											<p>
												<strong>Rs.5,00,000</strong>
											</p>
										</td>
										<td width="366">
											<p>
												Return filed on or before 31<sup>st </sup>December of
												Assessment Year
											</p>
										</td>
										<td width="172">
											<p>Rs. 5,000</p>
										</td>
									</tr>
									<tr>
										<td width="366">
											<p>In any other case</p>
										</td>
										<td width="172">
											<p>Rs. 10,000</p>
										</td>
									</tr>
								</tbody>
							</table>
							<p>&nbsp;</p>
							<p>&nbsp;</p>
							</li>
							</ul>
							</p>
						</div>

						<a href="#faqhome">Back to categories</a><br>
						<h2 id="salary">Salary FAQ</h2>
						<button class="accordion">What is the difference between PAN and
							TAN</button>
						<div class="panel">
							<p>
								PAN stands for Permanent Account Number and TAN stands for Tax
								Deduction Account Number. TAN is to be obtained by the person
								responsible to deduct tax, i.e., the deductor. In all the
								documents relating to TDS and all the correspondence with the
								Income-tax Department relating to TDS one has to quote his TAN.
								<br> PAN cannot be used for TAN, hence, the deductor has to
								obtain TAN, even if he holds PAN. <br> However, in case of TDS
								on purchase of land and building (as per section 194-IA) as
								discussed in previous FAQ, the deductor is not required to
								obtain TAN and can use PAN for remitting the TDS. <br>
							</p>
						</div>

						<button class="accordion">Is leave encashment taxable as salary?</button>
						<div class="panel">
							<p>
								It is taxable if received while in service. Leave encashment
								received at the time of retirement is exempt in the hands of the
								Government employee. In the hands of non-Government employee
								leave encashment will be exempt subject to the limit prescribed
								in this behalf under the Income-tax Law<br>
							</p>
						</div>

						<button class="accordion">Are retirement benefits like PF and
							Gratuity taxable?</button>
						<div class="panel">
							<p>
								In the hands of a Government employee Gratuity and PF receipts
								on retirement are exempt from tax. In the hands of
								non-Government employee, gratuity is exempt subject to the
								limits prescribed in this regard and PF receipts are exempt from
								tax, if the same are received from a recognised PF after
								rendering continuous service of not less than 5 years. <br>
							</p>
						</div>

						<button class="accordion">Is pension income taxed as salary
							income?</button>
						<div class="panel">
							<p>
								Yes. However, pension received from the United Nations
								Organisation is exempt. <br>
							</p>
						</div>

						<a href="#faqhome">Back to categories</a><br>
						<h2 id="tds">TDS FAQ</h2>
						<button class="accordion">How to apply for a Lower Tax Deduction
							Certificate?</button>
						<div class="panel">
							<p>
								Section 197 facilitates a tax payer whose TDS is being deducted
								to apply for a Lower Tax Deduction Certificate by filing <strong>Form
									13 </strong>to the Income Tax Officer in case his tax liability
								is less than the amount of TDS that is being deducted.&lt;br&gt;
							</p>
							<p>Once such a certificate is granted by the officer on him being
								satisfied that lower deduction of TDS is justified, the TDS will
								be deducted as per the TDS rate stated therein. &lt;br&gt;</p>
							<p>This certificate is required to be submitted to the person who
								is deducting the TDS in all the cases except where payment is
								being made as Interest on securities or Interest on fixed
								deposits U/S 197A where Form 15 G/ Form 15 H has to be
								submitted. &lt;br&gt;</p>
							<p>
								<em><u>Procedure for filing Form 13 to the Income Tax Officer:</u></em>
								&lt;br&gt;
							</p>
							<p>Various details that are required to be furnished by the tax
								payer while making an application in Form 13 are as below:</p>
							<ul>
								<li>Name and PAN no.</li>
								<li>Details regarding the purpose for which the payment is being
									received</li>
								<li>Details of Income for the last 3 years and the projected
									current year’s income.</li>
								<li>Details of payment of tax of the last 3 years.</li>
								<li>Details of tax deducted / paid for the current year.</li>
							</ul>
							<p>As per section 28AA, the certificate so issued by the income
								tax officer under section 197 is valid only for the assessment
								year mentioned in the certificate unless cancelled before the
								expiry of the date mentioned in the certificate.</p>
						</div>

						<button class="accordion">Payment to non residents – Section 195
							read with DTAA</button>
						<div class="panel">
							<p>A General Rule “ Expenses for Payer is Income for the Payee”.
								As such whenever a payment is made to a Non-Resident, an element
								of income is generated which brings it within the purview of TAX
								in India.</p>
							<p>
								TDS is the first way of collecting taxes. As such section 195
								has been introduced in the IT Act for collection of taxes on
								Non-Resident payments which bears the character of Income. Under
								section 195, the act prescribes <strong>the rates</strong> (to
								be increased by applicable surcharge &amp; education cess) that
								would be applicable to the Non-Resident payments depending on
								the nature of Income. The rates are as follows:
							</p>
							<table>
								<tbody>
									<tr>
										<td width="534">
											<p>
												<strong>Particulars</strong>
											</p>
										</td>
										<td width="72">
											<p>
												<strong>TDS Rates</strong>
											</p>
										</td>
									</tr>
									<tr>
										<td width="534">
											<p>Income in respect of investment made by a NRI</p>
										</td>
										<td width="72">
											<p>20%</p>
										</td>
									</tr>
									<tr>
										<td width="534">
											<p>Income by the way of long term capital gains in Section
												115E in case of a NRI</p>
										</td>
										<td width="72">
											<p>10%</p>
										</td>
									</tr>
									<tr>
										<td width="534">
											<p>Income by way of Long term capital gains</p>
										</td>
										<td width="72">
											<p>10%</p>
										</td>
									</tr>
									<tr>
										<td width="534">
											<p>Short Term Capital gains under section 111A</p>
										</td>
										<td width="72">
											<p>15%</p>
										</td>
									</tr>
									<tr>
										<td width="534">
											<p>Any other income by way of long-term capital gains</p>
										</td>
										<td width="72">
											<p>20%</p>
										</td>
									</tr>
									<tr>
										<td width="534">
											<p>Interest payable on money borrowed in Foreign Currency</p>
										</td>
										<td width="72">
											<p>20%</p>
										</td>
									</tr>
									<tr>
										<td width="534">
											<p>Income by way of royalty payable by Government or an
												Indian concern</p>
										</td>
										<td width="72">
											<p>10%</p>
										</td>
									</tr>
									<tr>
										<td width="534">
											<p>Income by way of royalty, not being royalty of the nature
												referred to be payable by Government or an Indian concern</p>
										</td>
										<td width="72">
											<p>10%</p>
										</td>
									</tr>
									<tr>
										<td width="534">
											<p>Income by way of fees for technical services payable by
												Government or an Indian concern</p>
										</td>
										<td width="72">
											<p>10%</p>
										</td>
									</tr>
									<tr>
										<td width="534">
											<p>Any other income</p>
										</td>
										<td width="72">
											<p>30%</p>
										</td>
									</tr>
								</tbody>
							</table>
							<p></p>
							<p>Besides, wherever applicable, in course of making payments to
								non-resident, DTAA provisions has to be read in order to
								determine the effective rate of TDS to be applicable for that
								payment. For this a CA certificate in Form 15CB is required
								(Form 15CA to be filed online) which certifies the details of
								the payment, TDS rate and TDS deduction as per section 195 of
								the Income Tax Act, if any DTAA (Double Tax Avoidance Agreement)
								is applicable, and other details of nature &amp; purpose of the
								remittance.</p>
						</div>

						<button class="accordion">Inadmissible Expenses – Non Deduction of
							TDS on Foreign Payments [Section 40a(i)]</button>
						<div class="panel">
							<p>TDS is the first source of collection of Taxes in India which
								if not deducted as per the IT Act will result in the increment
								of Tax payable.</p>
							<p>
								<strong>Section 40a(i)</strong> provides the list of expenses
								which are specifically disallowed while computing income
								chargeable to tax under the head “Profits and gains of business
								or profession” for <strong>non deduction of TDS</strong> on such
								payments made to a non resident outside India / in India.
							</p>
							<p>
								Any interest, royalty, fees for technical services or other sum
								(which is chargeable to tax under the Act) which is <strong>payable
									outside India to any person or in India to a non-resident</strong>,
								is not deductible, if the assessee has not deducted tax at
								source from such payments, or after deducting tax, he has not
								deposited such tax with the Government before the end of
								previous year [or before the due date of deposit specified under
								section 200(1) in case due date of deposit falls in next year].
								However, if tax is deposited in next year(s) after the due date
								of deposit, then such amount is deductible in the subsequent
								previous year in which the said tax is deposited by the assessee
								with Government.
							</p>
							<p>
								<u>Whether TDS to be deducted u/s 40a(i) read with section 195
									on payments made to Foreign Citizens for non taxable expenses
									incurred outside India?</u>
							</p>
							<p>
								According to the amendment in section 195 of Income Tax Act in
								budget 2015, where <strong>any payment</strong> is made to a non
								resident, irrespective of its taxability under the income tax
								act provisions, the payer is <strong>obliged to give information</strong>
								of the same in the manner prescribed to the concerned officer
								and <strong>obtain a certificate for no deduction of TDS</strong>
								where the payee is not liable to be taxed in India.
							</p>
							<p>The relevant extracts of the amendment is as below:</p>
							<p>
								<em>In section 195 of the Income-tax Act, for sub-section (6),
									the following sub-section shall be substituted with effect from
									the <strong><u>1st day of June, 2015</u></strong>, namely:—
								</em>
							</p>
							<p>
								<em>"(6) The person responsible for paying to a non-resident,
									(not being a company), or to a foreign company, any sum,
									whether or not chargeable under the provisions of this Act,
									shall furnish the information relating to payment of such sum,
									in such form and manner, as may be prescribed."</em>
							</p>
						</div>

						<button class="accordion">Is TDS to be deducted on the amount of
							GST ?</button>
						<div class="panel">
							<p>
								TDS is the act of collecting taxes on <strong>payments for
									services</strong> by a resident to any person where such
								payments bears a <strong>character of Income</strong> in the
								hands of recipient. GST is an indirect tax that is levied on the
								supply of goods &amp; services. According to the Indian Tax
								Laws, the receiver of service is required to do two things at
								the time of making any payment:
							</p>
							<ol>
								<li>Pay GST on the services received.</li>
								<li>Calculate TDS on Income and deduct it from the payment made.</li>
							</ol>
							<p>
								GST being an Indirect tax doesn’t bear the character of income
								and hence is <strong><u>not liable to be part of TDS deduction</u></strong>.
								GST which is collected has to be deposited to the government. So
								logically, TDS is not required to be deducted from the amount
								that is inclusive of Income Tax and should be deducted from the
								amount that is exclusive of the GST.
							</p>
							<p>But if GST amount has not been disclosed separately in the
								invoice, then TDS in such case would be deducted from the total
								amount i.e the amount that is inclusive of GST.</p>
						</div>

						<a href="#faqhome">Back to categories</a><br>
						<h2 id="houseproperty">House Property</h2>
						<button class="accordion">Whether the rental income could be
							charged to tax in the hands of the person who is not registered
							owner of the property?</button>
						<div class="panel">
							<p>
								Rental income from property is charged to tax under the head
								"Income from house property in the hands of the owner of the
								property". If a person receiving the rent is not the owner of
								the property, then rental income is not charged to tax under the
								head "Income from house property" (<em>E.g.</em> Rent received
								by tenant from sub-letting). In the following cases a person may
								not be the registered owner of the property, but he will be
								treated as the owner (<em>i.e.,</em> deemed owner) of the
								property and rental income from property will be charged to tax
								in his hands:
							</p>
							<p>(1) If an individual transfers his or her house property to
								his/her spouse (not being a transfer in connection with an
								agreement to live apart) or to his/her minor child (not being
								married daughter) without adequate consideration, then the
								transferor will be deemed as owner of the property.</p>
							<p></p>
							<p>(2) Holder of impartible estate is deemed as the owner of the
								property comprised in the estate</p>
							<p></p>
							<p>(3) A member of co-operative society, company or other
								association of persons to whom a building (or part of it) is
								allotted or leased under house building scheme of the society,
								company or association, as the case may be, is treated as deemed
								owner of the property.</p>
							<p></p>
							<p>(4) A person acquiring property by satisfying the conditions
								of section 53A of the Transfer of Property Act, will be treated
								as deemed owner (although he may not be the registered owner).
								Section 53A of said Act prescribes following conditions:</p>
							<p></p>
							<p>(a) There must be an agreement in writing.</p>
							<p></p>
							<p>(b) The purchase consideration is paid or the purchaser is
								willing to pay it.</p>
							<p></p>
							<p>(c) Purchaser has taken the possession of the property in
								pursuance of the agreement.</p>
							<p></p>
							<p>(5) In case of lease of a property for a period exceeding 12
								years (whether originally fixed or provision for extension
								exists), lessee is deemed to be the owner of the property.
								However, any right by way of lease from month-to-month or for a
								period not exceeding one year is not covered by this provision.</p>
						</div>

						<button class="accordion">Is Rental Income from sub-letting
							chargeable to tax under the head “Income from House Property”?</button>
						<div class="panel">
							<p>Rental income in the hands of owner is charged to tax under
								the head “Income from house property”. Rental income of a person
								other than the owner cannot be charged to tax under the head
								“Income from house property”. Hence, rental income received by a
								tenant from sub-letting cannot be charged to tax under the head
								“Income from house property”. Such income is taxable under the
								head “Income from other sources” or profits and gains from
								business or profession, as the case may be.</p>
						</div>

						<button class="accordion">Under which head the rental income from
							the shop charged to tax?</button>
						<div class="panel">
							<p>Rental income from a property, being building or land
								appurtenant thereto, of which the taxpayer is the owner is
								charged to tax under the head “Income from house property”. To
								tax the rental income under the head “Income from house
								property”, the rented property should be building or land
								appurtenant thereto. Shop being a building, rental income will
								be charged to tax under the head “Income from house property”.</p>
						</div>

						<button class="accordion">What is the Tax treatment of composite
							rent when the composite rent pertains to letting of building
							along with other assets?</button>
						<div class="panel">
							<p>Composite rent includes rent of building and rent towards
								other assets or facilities. The tax treatment of composite rent
								is as follows:-</p>
							<p>(a) In a case where letting out of building and letting out of
								other assets are inseparable (i.e., both the lettings are
								composite and not separable, e.g., letting of equipped theatre),
								entire rent (i.e. composite rent) will be charged to tax under
								the head “Profits and gains of business and profession” or
								“Income from other sources”, as the case may be. Nothing is
								charged to tax under the head “Income from house property”..</p>
							<p>(b) In a case where, letting out of building and letting out
								of other assets are separable (i.e., both the lettings are
								separable, e.g., letting out of refrigerator along with
								residential bungalow), rent of building will be charged to tax
								under the head “Income from house property” and rent of other
								assets will be charged to tax under the head “Profits and gains
								of business and profession” or “Income from other sources”, as
								the case may be. This rule is applicable, even if the owner
								receives composite rent for both the lettings. In other words,
								in such a case, the composite rent is to be allocated for
								letting out of building and for letting of other assets.</p>
						</div>

						<button class="accordion">What is the Tax treatment of composite
							rent when the composite rent pertains to letting out of building
							along with charges of provision of services></button>
						<div class="panel">
							<p>In such a case, composite rent includes rent of building and
								charges for different services (like lift, watchman, water
								supply, etc.): In this situation, the composite rent is to be
								bifurcated and the sum attributable to the use of property will
								be charged to tax under the head “Income from house property”
								and charges for various services will be charged to tax under
								the head “Profits and gains of business and profession” or
								“Income from other sources” (as the case may be).</p>
						</div>

						<button class="accordion">What is pre-construction period?</button>
						<div class="panel">
							<p>
								While computing income chargeable to tax under the head "Income
								from house property" in case of a let-out property, the taxpayer
								can claim deduction under <a
									href="http://www.incometaxindia.gov.in/Pages/faqs.aspx">section
									24(b)</a> on account of interest on loan taken for the purpose
								of purchase, construction, repair, renewal or reconstruction of
								the property.
							</p>
							<p>Deduction on account of interest is classified in two forms,
								viz., interest pertaining to pre-construction period and
								interest pertaining to post-construction period.</p>
							<p>Post-construction period interest is the interest pertaining
								to the relevant year (i.e., the year for which income is being
								computed).</p>
							<p>Pre-construction period is the period commencing from the date
								of borrowing of loan and ends on earlier of the following:</p>
							<p>➣ Date of repayment of loan; or</p>
							<p>➣ 31st March immediately prior to the date of completion of
								the construction/acquisition of the property.</p>
							<p>Interest pertaining to pre-construction period is allowed as
								deduction in five equal annual instalments, commencing from the
								year in which the house property is acquired or constructed.</p>
							<p>
								Thus, total deduction available to the taxpayer under <a
									href="http://www.incometaxindia.gov.in/Pages/faqs.aspx">section
									24(b)</a> on account of interest will be 1/5th of interest
								pertaining to pre-construction period (if any) + Interest
								pertaining to post construction period (if any).
							</p>
						</div>

						<button class="accordion">What is the Self-Occupied property, how
							to compute the income from self occupied property?</button>
						<div class="panel">
							<p>A self-occupied property means a property which is occupied
								throughout the year by the taxpayer for his residence. Income
								chargeable to tax under the head "Income from house property" in
								case of a self-occupied property is computed in following manner
								:</p>
							<table width="100%">
								<tbody>
									<tr>
										<td width="410">
											<p>Particulars</p>
										</td>
										<td width="297">
											<p>Amount</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>Gross annual value</p>
										</td>
										<td>
											<p>Nil</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>Less:- Municipal taxes paid during the year</p>
										</td>
										<td>
											<p>Nil</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>Net Annual Value (NAV)</p>
										</td>
										<td>
											<p>Nil</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>
												Less:- Deduction under <a
													href="http://www.incometaxindia.gov.in/Pages/faqs.aspx">section
													24</a>
											</p>
										</td>
										<td>
											<p></p>
										</td>
									</tr>
									<tr>
										<td>
											<p>
												➣Deduction under <a
													href="http://www.incometaxindia.gov.in/Pages/faqs.aspx">section
													24(a)</a> @ 30% of NAV
											</p>
											<p>
												➣Deduction under <a
													href="http://www.incometaxindia.gov.in/Pages/faqs.aspx">section
													24(b)</a> on account of interest on borrowed capital
											</p>
										</td>
										<td>
											<p>Nil</p>
											<p></p>
											<p>(XXXX)</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>Income from house property</p>
										</td>
										<td>
											<p>XXXX</p>
										</td>
									</tr>
								</tbody>
							</table>
							<p></p>
							<p>From the above computation it can be observed that "Income
								from house property" in the case of a self occupied property
								will be either Nil (if there is no interest on housing loan) or
								negative (i.e., loss) to the extent of interest on housing loan.
								Deduction in respect of interest on housing loan in case of a
								self-occupied property cannot exceed Rs. 2,00,000 or Rs. 30,000,
								as the case may be (discussed later).</p>
						</div>

						<button class="accordion">In case of Self-Occupied Property, how
							much of interest on housing loan can be claimed as deduction?</button>
						<div class="panel">
							<p>
								The provisions relating to deduction under <a
									href="http://www.incometaxindia.gov.in/Pages/faqs.aspx">section
									24(b)</a> on account of interest on housing loan in case of
								self-occupied property are same as applicable in case of let-out
								property. In other words, deduction available to taxpayer under
								<a href="http://www.incometaxindia.gov.in/Pages/faqs.aspx">section
									24(b)</a> in respect of self-occupied property will be 1/5th of
								interest pertaining to pre-construction period (if any) +
								Interest pertaining to post-construction period (if any).
							</p>
							<p>
								However, in the case of self-occupied property, deduction under
								<a href="http://www.incometaxindia.gov.in/Pages/faqs.aspx">section
									24(b)</a> cannot exceed Rs.2,00,000 or Rs. 30,000 (as the case
								may be). If all the following conditions are satisfied, then the
								limit in respect of interest on borrowed capital will be
								Rs.2,00,000:
							</p>
							<p>➣ Capital is borrowed on or after 1-4-1999.</p>
							<p>➣ Capital is borrowed for the purpose of acquisition or
								construction (i.e., not for repair, renewal, reconstruction).</p>
							<p>➣ Acquisition or construction is completed within 5 years from
								the end of the financial year in which the capital was borrowed.</p>
							<p>➣ The person extending the loan certifies that such interest
								is payable in respect of the amount advanced for acquisition or
								construction of the house or as re-finance of the principal
								amount outstanding under an earlier loan taken for acquisition
								or construction of the property.</p>
							<p>If any of the above condition is not satisfied, then the limit
								of Rs. 2,00,000 will be reduced to Rs. 30,000.</p>
						</div>


						<button class="accordion">What is the tax treatment of unrealized
							rent which is subsequently realized?</button>
						<div class="panel">
							<p>Any subsequent recovery of unrealized rent shall be deemed to
								be the income of taxpayer under the head “Income from house
								property” in the year in which such rent is realized (whether or
								not the assesse is the owner of that property in that year). It
								will be charged to tax after deducting a sum equal to 30% of
								unrealized rent.</p>
						</div>

						<button class="accordion">What is the tax treatment of Arrears of
							rent?</button>
						<div class="panel">
							<p>The amount received on account of arrears of rent (not charged
								to tax earlier) will be charged to tax after deducting a sum
								equal to 30% of such arrears. It is charged to tax in the year
								in which it is received. Such amount is charged to tax whether
								or not the taxpayer owns the property in the year of receipt.</p>
						</div>

						<button class="accordion">Does subletting of house property
							taxable as rental income under the head “Income from house
							Property”?</button>
						<div class="panel">
							<p>No, the income earned from subletting a house property is
								chargeable to tax under the head “income from other sources”
								rather than house property Income. Since, the assessee is not
								the owner of the property the said rental income cannot be held
								as income from house property. Since, the assessee was not
								engaged in the business of subletting, the same cannot be
								construed as business income of the assessee. Accordingly, such
								income shall be taxable as last resort income head i.e. income
								from other sources.</p>
						</div>

						<a href="#faqhome">Back to categories</a><br>

						<h2 id="deductions">Deductions</h2>
						<button class="accordion">Additional deduction above 1.5 lakhs U/s
							80C with respect to section 80CCD</button>
						<div class="panel">
							<p>
								Deduction u/s 80CCD for deposit under NEW PENSION SCHEME has
								been introduced for all tax payers whether employed by the
								Government, any other employers or Self employed individuals in
								addition to Section 80C with a combined limit to <strong><u>Rs.2
										Lakhs</u></strong>.
							</p>
							<p>
								<em><u>NPS Tax Benefits under Sec.80CCD (1)</u></em>
							</p>
							<ul>
								<li>The maximum benefit available is Rs.1.5 lakh (including
									Sec.80C limit).</li>
								<li>An individual&rsquo;s maximum 10% of annual income or an
									employee&rsquo;s (10% of Basic + DA) contribution will be
									eligible for deduction.</li>
								<li>As I said above, this section will form the part of Sec.80C
									limit.</li>
							</ul>
							<p>
								<em><u>NPS Tax Benefits under Sec.80CCD (1B)</u></em>
							</p>
							<ul>
								<li>This is the additional tax benefit of up to Rs.50,000
									eligible for income tax deduction and was introduced in the
									Budget 2015 for voluntary contribution to NPS.</li>
								<li>Introduced in Budget 2015. One can avail the&nbsp;benefit of
									this Sect.80CCD (1B) from FY 2015-16.</li>
								<li>Both self-employed and employees are eligible for availing
									this deduction.</li>
								<li>This is over and above Sec.80CCD (1) i.e. If the taxpayer
									contributes more than Rs.1.5 lakh to the NPS in a year, the
									amount in excess of Rs 1.5 lakh can be treated as voluntary
									investment and claimed as a deduction under the new Section
									80CCD(1b).</li>
							</ul>
							<p>
								<em><u>NPS Tax Benefits under Sec.80CCD (2)</u></em>
							</p>
							<ul>
								<li>There is a misconception among many that there is no upper
									limit for this section. However, the limit is <strong>least</strong>
									of 3 conditions.
								</li>
							</ul>
							<p>&nbsp;1) Amount contributed by an employer,</p>
							<p>&nbsp;2) 10% of Basic Salary (Basic + DA) and</p>
							<p>&nbsp;3) Gross Total Income.</p>
							<ul>
								<li>This is <strong>additional deduction</strong> which will not
									form the part of Sec.80C limit.
								</li>
								<li>The deduction under this section will <strong>not</strong>
									be eligible for <strong>self-employed</strong>.
								</li>
							</ul>
							</p>
						</div>

						<button class="accordion">Additional deduction above 1.5 lakhs U/s
							80C with respect to section 80CCG</button>
						<div class="panel">
							<p>
								A new saving scheme introduced vide Finance Act 2012 and amended
								vide Finance Act 2013 for the <strong>Resident Individuals (NO
									HUF) </strong>with a Gross total income of Rs.12 Lakhs or Less
								to claim deduction u/s 80CCG to an extent of <strong>Rs.50,000 </strong>in
								addition to 1.5 lakhs u/s 80C for investment in specified equity
								shares and mutual funds.
							</p>
							<p>
								The scheme was introduced by the government as <strong>RGESS</strong>
								(Rajiv Gandhi Equity Saving Scheme) under which an individual
								investing would be eligible to claim <strong>50% of the invested
									amount</strong> as deduction from his total income to a maximum
								limit of INR 50,000.
							</p>
							<p>
								<em><u>Key features of the above deduction scheme:</u></em>
							</p>
							<ul>
								<li>The allowed tax deduction u/s 80CCG will be over and above
									the Rs. 1.5 Lakhs limit permitted under Section 80C of the
									Income Tax (IT) Act, making it thus attractive for the middle
									class investors</li>
								<li>Further, the Dividend income is tax free, if the company is
									liable to dividend distribution tax</li>
								<li>The benefits can be availed for three consecutive years</li>
								<li>Investor is free to trade / churn the portfolio after the
									fixed lock-in period i.e 1 year, subject to certain conditions</li>
								<li>Gains arising out of higher market valuation of RGESS
									eligible securities can be realized after a year viz: fixed
									lock-in period. Provisions exist to protect the investor from
									general declines in the market to a certain extent. This is in
									contrast to all other tax saving instruments.</li>
								<li>Facility for pledging stocks after the fixed lock-in period</li>
								<li>For investments upto Rs.50,000 in your sole RGESS demat
									account, if you opt for Basic Service Demat Account, annual
									maintenance charges for the demat account is zero and for
									investments upto Rs. 2 lakh, it is stipulated at Rs 100</li>
								<li>The investments can be made in installments during the
									financial year in which tax deduction is claimed</li>
							</ul>
							<p>
								The <a
									href="https://ruwix.com/online-rubiks-cube-solver-program/"
									rel="nofollow noopener">online Rubik's Cube solver</a> helps
								you to find the solution for your unsolved puzzle.
							</p>
						</div>

						<button class="accordion">Can 80C Deduction adjusted / Claimed
							against Short term Capital Gain u/s 111A</button>
						<div class="panel">
							<p>
								<strong>No,</strong> deduction under sections 80C to 80U is not
								allowed on short-term capital gains referred to in section 111A.
								However, such deductions can be claimed from STCG other than
								covered under section 111A. The above statement is applicable
								for both the Residents &amp; Non residents.
							</p>
						</div>

						<button class="accordion">Can 80C Deduction adjusted / Claimed
							against Long term Capital Gain</button>
						<div class="panel">
							<p>
								<strong>No,</strong> deduction under sections 80C to 80U is not
								allowed to be adjusted against Long-term capital gains for any
								tax payer. However LTCG can be adjusted against Basic exemption
								limit in case of Resident Individuals.
							</p>
						</div>

						<button class="accordion">Deduction of Interest on House loan u/s
							80EE in addition to 2 Lakh u/s 24</button>
						<div class="panel">
							<p>Section 80EE was re-introduced by budget 2016 to provide for
								additional deduction of Rs.50,000 for payment of interest on
								home loan over and above that is allowed u/s 24.</p>
							<p>This Deduction of Section 80EE would be applicable only in the
								following cases:-</p>
							<ol>
								<li>This deduction would be allowed only if the <strong>value of
										the property</strong> purchased is less than <strong>Rs. 50
										Lakhs</strong> and the <strong>value of loan</strong> taken is
									less than <strong>Rs. 35 Lakhs</strong>.
								</li>
								<li>The loan should be sanctioned between 1st April 2016 and
									31st March 2017.</li>
								<li>The benefit of this deduction would be available till the
									time the repayment of the loan continues.</li>
								<li>This Deduction would be available from <strong>Financial
										Year 2016-17 onwards</strong>.
								</li>
								<li>The above tax deductions are per person and not per
									Property. So in case you&rsquo;ve purchased a property jointly
									and have taken a joint home loan, each person repaying the
									amount would be eligible to claim whole deduction separately.</li>
								<li>If you are living in a rented premise and are taking&nbsp;<a
									href="http://www.charteredclub.com/house-rent-allowance/">Tax
										Benefit of HRA Allowance</a>, even then you can claim&nbsp;<em>Tax
										benefit on home loan</em>&nbsp;under Section 24, Section 80EE
									&amp; Section 80C.
								</li>
							</ol>
							</p>
						</div>

						<button class="accordion">Deduction u/s 80D &ndash; Medical /
							Health Insurance</button>
						<div class="panel">
							<p>
								Section 80D provides for the deduction from the gross total
								taxable income of any <strong>Individual or HUF (resident &amp;
									non-resident)</strong> with respect to the <strong>premium
									amount</strong> paid by any mode <strong>other than cash</strong>
								towards the medical / health insurance policy taken for Self,
								spouse, dependent children &amp; parents.
							</p>
							<p>
								<em><u>Limit of Deduction</u></em>
							</p>
							<p>
								<strong>For Self and Family:</strong>
							</p>
							<ul>
								<li>Maximum deduction of Rs.25,000 per year on health insurance
									premium for self, spouse &amp; kids.</li>
								<li>Maximum deduction of Rs.30,000 per year if you are a senior
									citizen.</li>
							</ul>
							<p>
								<strong>For Parents:</strong>
							</p>
							<ul>
								<li>Maximum deduction of Rs.25,000 per year on health insurance
									premium paid on behalf of parents.</li>
								<li>Maximum deduction of Rs.30,000 per year on premium payments
									for senior citizen parents.</li>
							</ul>
							<p>
								<strong>Additional Deduction (Preventive Health checkups) :</strong>
							</p>
							<ul>
								<li>A deduction of Rs.5,000 can be claimed every year on
									expenses related to <strong>health check-ups</strong>. This
									limit includes the check-up expenses of all members in a
									family, including spouse, kids and parents.
								</li>
							</ul>
							</p>
						</div>

						<button class="accordion">Deduction for Rent Paid u/s 80GG where
							no HRA from employer</button>
						<div class="panel">
							<p>
								House Rent Allowance is a deduction from gross taxable salary
								that can be claimed by a <strong>Salaried Individual </strong>who
								lives in a rented house. HRA as provided by the employer is
								calculated and respective deduction is given in Form 16 and
								accordingly the TDS u/s 192 is calculated for every employee.
							</p>
							<p>
								However, if the employer doesn&rsquo;t provide HRA to any
								employee who is paying rent towards any furnished / unfurnished
								accommodation, such employee can resort to <strong>Section 80GG</strong>
								for deduction from Gross Taxable Income.
							</p>
							<p>
								The <strong>lowest</strong> of these will be considered as the
								deduction under section 80GG &ndash;
							</p>
							<ul>
								<li>Rs 5,000 per month / Rs.60000 per year</li>
								<li>25% of adjusted total income*</li>
								<li>Actual Rent less 10% of adjusted total&nbsp;Income* [Actual
									Rent &ndash; 10% of Adj.Total Income]</li>
							</ul>
							<p>
								<br />
								<em>Adjusted Total Income* means Total Income Less long term
									capital gain, short term capital gain under section 111A and
									Income under section 115A or 115D and deductions 80C to 80U
									(except deduction under section 80GG)</em>
							</p>
						</div>

						<button class="accordion">Difference between medical reimbursement
							and medical allowance</button>
						<div class="panel">
							<p>
								The two terms often used interchangeably is &ldquo;Medical
								Reimbursement&rdquo; and &ldquo;Medical Allowance&rdquo; of
								which <u>Medical Re-imbursement by employer is <strong>Not
										Taxable</strong></u> while <u>Medical Allowance is <strong>taxable</strong></u>
								at the Marginal Income Tax Rate (Income Rate Slab) of the
								employee.
							</p>
							<p>
								Both the employer and the employee try to structure salary to
								maximize in-hand income of the employee.&nbsp;There are certain
								allowances or expense reimbursements that are <strong>not
									taxable</strong> in the hands of the employee. One such payment
								from employer is <strong>reimbursement of the medical expenses</strong>
								of the employee or his family.
							</p>
							<p>On the other hand, Medical allowance is a fixed part of an
								employees&rsquo; salary which is taxable in the hands of the
								employee under the head &ldquo;Salary&rdquo;.</p>
							<p>
								<strong>The reimbursement is only on actual basis</strong>&nbsp;(employer
								can&rsquo;t reimburse more than you have incurred). Contrast
								this with fixed medical allowance, which is guaranteed
								regardless of whether the employee (or his family member)
								undergo medical treatment or not.
							</p>
							<p>
								Reimbursement of medical expenses by the employer is exempt to
								the extent of <strong>Rs.15000</strong> per financial year. Any
								additional reimbursement would be taxable as Salary in the hands
								of the employee.
							</p>
							<p>
								<strong>In addition to above, there are specific scenarios where
									the entire amount reimbursed/cost incurred by the employer is
									exempt from tax i.e. there is no cap on tax exemption.</strong>
							</p>
							<ol>
								<li>The value of the treatment provided to the employee or his
									family member at a hospital maintained by the employer.</li>
								<li>Amount paid by the employer in respect of the medical
									treatment availed by employee or his family member in a
									Government hospital or any hospital approved by Government for
									such employees.</li>
								<li>Cost incurred for treatment of specific illness in any
									hospital approved by the Chief Commissioner. (You must file a
									certificate from the hospital specifying the disease/treatment
									and payment receipts).</li>
							</ol>
							<p>
								Become a master of the Rubik's Cube with <a
									href="http://rubiks-cu.be/#tutorial" rel="nofollow noopener">this
									online tutorial</a>. Learn how to solve the cube with the
								beginner's method!
							</p>
						</div>

						<button class="accordion">Changes in deductions under Chapter VI-A
							&ndash; Finance Bill 2018</button>
						<div class="panel">
							<p>On February 1, 2018, the Finance Minister, Mr. Arun Jaitley,
								presented the Union Budget for the year 2018.&nbsp;List of all
								the proposals related to deductions under chapter VI-A is
								highlighted as under:</p>
							<ol>
								<li><u>Hike in deduction limit for health insurance premium and/
										or medical expenditure from Rs. 30,000 to Rs. 50,000 under <strong>section
											80D</strong>.
								</u></li>
							</ol>
							<p>
								The current deduction u/s 80D of <strong>INR 30,000</strong>
								allowed to an Individual or HUF with respect to premium amount
								paid for health insurance (medical policy) for dependent parent
								/ parents being Senior Citizen (age above 60 Yrs) has been
								enhanced vide Finance Bill 2018 to <strong>INR 50,000</strong>.
								The above increment of deduction is also applicable to an
								individual for premium amount of his medical policy if he has
								attained the age of 60 and above.
							</p>
							<ol start="2">
								<li><u>Deduction limit under section 80DDB is enhanced</u></li>
							</ol>
							<p>
								The deduction limit for medical expenditure for certain critical
								illness has been increased from Rs. 60,000 (in case of senior
								citizens) and from Rs. 80,000 (in case of very senior citizens)
								to <strong>Rs. 1 lakh</strong> for all senior citizens, under
								section 80DDB.
							</p>
							<p>The differentiation between senior and super senior citizen is
								removed</p>
							<ol start="3">
								<li><u>Deductions under Section 80JJAA is extended to footwear
										and leather industry</u></li>
							</ol>
							<p>
								Section 80JJAA allows deductions to the manufacturers who employ
								new employees for a minimum period of <strong>240 days</strong>
								during the year. This deduction is calculated at the rate of 30%
								of the additional employee cost incurred by the assessee during
								the year.
							</p>
							<p>
								The eligibility of a manufacturer to claim this deduction is
								determined only if he gives employment for a minimum period of
								240 days during the year. However, for apparel industry the
								minimum period of employment is relaxed to 150 days. <strong>The
									concession of minimum employment period for 150 days has been
									extended to footwear and leather industry.</strong>
							</p>
							<p>Manufacturers are often denied the deduction if an employee is
								employed in year 1 for a period of less than 240 days or 150
								days, but continues to remain employed for more than 240 days or
								150 days in year 2. To overcome some difficulties, the
								employment conditions has been proposed to be relaxed. Now in
								this situation the deduction shall be allowed to the
								manufacturer if an employee hired in last year continues to
								remain in employment in current year for more than 240 or 150
								days, as the case may be.</p>
							<ol start="4">
								<li><u>New deduction introduced for Farm Producer Companies</u></li>
							</ol>
							<ul>
								<li>To promote agricultural activities a <strong>new section
										80PA</strong> is proposed to be inserted. This new provision
									proposes 100% deductions of profits for a period of 5 years to
									farm producer companies.
								</li>
								<li>This deduction is allowed to farm producer companies who
									have <strong>total turnover of less than Rs. 100 crores</strong>
									during the financial year. For claiming this deduction,
									companies' gross total income should include income from:
								</li>
							</ul>
							<ol>
								<li>Marketing of agricultural produce grown by its members</li>
								<li>Purchase of agricultural implements, seeds, livestock or
									other articles intended for agriculture for the purpose of
									supplying them to its members</li>
								<li>Processing of agricultural produce of its members</li>
							</ol>
							<ol start="5">
								<li><u>Exemption of interest income on deposits with banks and
										post offices to be increased from Rs. 10,000 to Rs. 50,000
										only for senior citizens.</u></li>
							</ol>
							<p>
								The current deduction u/s 80 TTA for Individuals / HUF with
								respect to interest on savings bank account of Rs.10,000 has
								been enhanced for Senior Citizens by inserting a new <strong>section
									80 TTB </strong>to Rs.50,000.
							</p>
							<p>
								That is to say, this provision allows deduction of up to Rs.
								50,000 to the senior citizen who has earned interest income from
								deposits <strong>(both Fixed &amp; Savings)</strong> with banks
								or post office or co-operative banks.
							</p>
							<p>After introducing this new deduction, the existing deduction
								of up to Rs. 10,000 under Section 80TTA shall not be allowed to
								the senior citizens.</p>
							<ol start="6">
								<li><u>Certain Deductions other than those covered under<strong>
											Heading A (Section 80A to 80C), Heading B (Section 80CC to
											80GGC) &amp; Heading D (Section 80U to 80VV) </strong>not to
										be allowed if return is not filed on time
								</u></li>
							</ol>
							<p>
								As per existing provisions of <strong>Section 80AC</strong> of
								the Act, no deduction would be admissible under section 80-IA or
								section 80-IAB or section 80-IB or section 80-IC or section
								80-ID or section 80-IE, unless the return of income by the
								assessee is furnished on or before the due date specified under
								Section 139(1).
							</p>
							<p>This burden of filing of return on time is not casted on other
								assesses who are claiming deductions under other similar
								provisions.</p>
							<p>
								Therefore, to bring uniformity in all income-based deduction, it
								is now proposed that the scope of section 80AC shall be extended
								to all similar deductions which are covered in <strong>heading
									"C.&mdash;Deductions in respect of certain incomes" in Chapter
									VIA (sections 80 H to 80RRB</strong>). The impact of such
								amendment shall be that no deduction would be allowed to a
								taxpayer under <strong>Heading C</strong> of <u>Deductions
									Chapter</u> if income-tax return is not filled on or before the
								due date.
							</p>
							<p>
								This amendment will take effect, from 1<sup>st&nbsp;</sup>April,
								2018 and will accordingly apply in relation to the assessment
								year 2018-19 and subsequent assessment years.
							</p>
							<ol start="7">
								<li><strong>Amendment in section 80-IAC to promote new start-ups</strong></li>
							</ol>
							<p>Deductions under Section 80-IAC is available to an eligible
								start-up for 3 consecutive assessment years out of 7 years at
								the option of such start-up.</p>
							<p>These deductions are allowed subject to certain conditions as
								given below:</p>
							<ol>
								<li>It is incorporated between 01/04/2016 and 31/03/2019</li>
								<li>The total turnover of its business does not exceed Rs. 25
									crores in any of the previous year 2016-17 to 2020-21; and</li>
								<li>It is engaged in the eligible business which involves
									innovation, development, deployment or commercialization of new
									products, processes or services driven by technology or
									intellectual property.</li>
							</ol>
							<p>In order to improve the effectiveness of the scheme for
								promoting start-ups in India, it is proposed to make following
								changes in the taxation regime for the start -ups:</p>
							<ol>
								<li>The benefit would also be available to start up cos
									incorporated between 01/04/2019 and 31/03/2021;</li>
								<li>The requirement of turnover not exceeding Rs. 25 Crore would
									apply to 7 previous years commencing from the date of
									incorporation;</li>
								<li>The definition of eligible business has been expanded to
									provide that the benefit would be available if it is engaged in
									innovation, development or improvement of products or processes
									or services, or a scalable business model with a high potential
									of employment generation or wealth creation.</li>
							</ol>
							</p>
						</div>

						<button class="accordion">What is the Investment Limit Under
							section 80C?</button>
						<div class="panel">
							<p>Section 80C of the Income Tax Act provides provisions for tax
								deductions on a number of payments, with both individuals and
								Hindu Undivided Families eligible for these deductions. Eligible
								taxpayers can claim deductions to the tune of Rs 1.5 lakh per
								year under Section 80C, with this amount being a combination of
								deductions available under Sections 80 C, 80 CCC and 80 CCD.</p>
						</div>

						<button class="accordion">What are the Major investments that can
							be made under section 80C?</button>
						<div class="panel">
							<p>Some of the popular investments which are eligible for this
								tax deduction are mentioned below.</p>
							<ul>
								<li>Payment made towards life insurance policies (for self,
									spouse or children)</li>
								<li>Payment made towards a superannuation/provident fund</li>
								<li>Tuition fees paid to educate a maximum of two children</li>
								<li>Payments made towards construction or purchase of a
									residential property</li>
								<li>Payments issued towards a fixed deposit with a minimum
									tenure of 5 years</li>
							</ul>
							<p>This section provides for a number of additional deductions
								like investment in mutual funds, senior citizens saving schemes,
								purchase of NABARD bonds, etc..</p>
						</div>
						<a href="#faqhome">Back to categories</a><br>

						<h2 id="capitalgains">Capital Gains</h2>

						<button class="accordion">Special rate of Long term capital gain
							tax @ 10%</button>
						<div class="panel">
							<p>Generally, long-term capital gains are charged to tax @ 20%
								(plus surcharge and cess as applicable), but in certain special
								cases, at the option of tax payer the capital gain can be
								calculated @ 10% (plus surcharge and cess as applicable).</p>
							<p>The benefit of charging long-term capital gain @ 10% is
								available only in respect of long-term capital gains arising on
								transfer of any of the following asset:</p>
							<ol>
								<li>Any security (*) which is listed in a recognized stock
									exchange in India.</li>
								<li><em>Any unit of UTI or mutual fund (whether listed or not)
										(it is not available now because this option is available only
										in respect of units sold on or before 10-7-2014.)</em></li>
								<li>Zero coupon bonds.</li>
							</ol>
							<p>(*) The definition of security generally includes shares,
								scrips, stocks, bonds, debentures, debenture stocks or other
								marketable securities.</p>
							<p>In case of long term capital gain arising on account of above
								assets, the taxpayer has following two options:</p>
							<ol>
								<li>Avail the benefit of indexation; the capital gains so
									computed will be charged to tax at normal rate of 20%</li>
								<li>Do not avail of the benefit of indexation; the capital gain
									so computed is charged to tax @ 10%.</li>
							</ol>
							<p>The selection of the option is to be done by computing the tax
								liability under both the options, and the option with lower tax
								liability is to be selected.</p>
							<p>
								<strong><em>Example: </em></strong>
							</p>
							<p>Mr. X (a non resident) purchased equity shares (listed) of
								Shyamal Ltd. in December 1995 for Rs. 28,100. These shares are
								sold (outside recognized stock exchange) in April, 2016 for Rs.
								5,00,000. He does not have any other taxable income in India.
								What will be his tax liability?</p>
							<p>Mr. X has purchased the listed equity shares so he is eligible
								to take the benefit of 10% taxation.</p>
							<p>
								<strong>Mr. X has following two options:</strong>
							</p>
							<table>
								<tbody>
									<tr>
										<td width="265">
											<p>
												<strong>Particulars</strong>
											</p>
										</td>
										<td width="198">
											<p>
												<strong>Option 1 (Avail</strong>
											</p>
											<p>
												<strong>indexation)</strong>
											</p>
										</td>
										<td width="175">
											<p>
												<strong>Option 2 (Do not</strong>
											</p>
											<p>
												<strong>avail indexation)</strong>
											</p>
										</td>
									</tr>
									<tr>
										<td width="265">
											<p>Full value of consideration</p>
										</td>
										<td width="198">
											<p>5,00,000</p>
										</td>
										<td width="175">
											<p>5,00,000</p>
										</td>
									</tr>
									<tr>
										<td width="265">
											<p>Less: Indexed cost of acquisition (Rs.</p>
											<p>28,100 &times; 1125/281)</p>
										</td>
										<td width="198">
											<p>(1,12,500)</p>
										</td>
										<td width="175">
											<p>--------</p>
										</td>
									</tr>
									<tr>
										<td width="265">
											<p>Less: Cost of acquisition</p>
										</td>
										<td width="198">
											<p>--------</p>
										</td>
										<td width="175">
											<p>(28,100)</p>
										</td>
									</tr>
									<tr>
										<td width="265">
											<p>
												<strong>Taxable Gain </strong>
											</p>
										</td>
										<td width="198">
											<p>
												<strong>3,87,500 </strong>
											</p>
										</td>
										<td width="175">
											<p>
												<strong>4,71,900</strong>
											</p>
										</td>
									</tr>
									<tr>
										<td width="265">
											<p>Tax @ 20% on Rs. 3,87,500</p>
										</td>
										<td width="198">
											<p>77, 500</p>
										</td>
										<td width="175">
											<p>--------</p>
										</td>
									</tr>
									<tr>
										<td width="265">
											<p>Tax @ 10% on Rs. 4,71,900</p>
										</td>
										<td width="198">
											<p>--------</p>
										</td>
										<td width="175">
											<p>47,190</p>
										</td>
									</tr>
								</tbody>
							</table>
							<p>From the above example it is clear that Mr. X should opt for
								option 2, because since in this situation the tax liability
								(excluding cess as applicable) comes to Rs. 47,190 which is less
								than tax liability under option 1 i.e. Rs. 77,500. Tax liability
								after EC @ 2% and SHEC @ 1% will come to Rs. 48,606.</p>
							</li>
							</ol>
							</p>
						</div>

						<button class="accordion">Adjustment of Long term capital gain
							(LTCG) with Basic exemption limit</button>
						<div class="panel">
							<p>Basic exemption limit means the level of income up to which a
								person is not required to pay any tax. This Basic exemption
								limit is available only for Individual, HUF, AOP, BOI and
								artificial judicial persons</p>
							<p>We all know that if the income is below the basic exemption
								limit, then there will be no tax liability. But always a
								question arises that &lsquo;can an individual adjust the basic
								exemption limit against long-term capital gain?&rsquo;</p>
							<p>The answer will depend on the residential status of the
								individual that is whether the individual or HUF is resident or
								Non-resident.</p>
							<p>Only a resident individual/HUF can adjust the exemption limit
								against LTCG. Thus, a non-resident individual and non-resident
								HUF cannot adjust the basic exemption limit against LTCG.</p>
							<p>A resident individual can adjust the LTCG with Basic exemption
								limit, but only after making adjustment of other income. That
								means, first income under other heads (other than LTCG) is to be
								adjusted against the basic exemption limit and then the
								remaining limit (if any) can be adjusted against LTCG.</p>
						</div>

						<button class="accordion">Recent Ammendments to Section 54 &ndash;
							LTCG Exemption</button>
						<div class="panel">
							<p>
								Section 54 provides exemption from payment of Tax on Long term
								capital gain earned by virtue of sale of a Long term immovable
								property, being a House property to the extent of <strong>Lower
									of the following</strong>:
							</p>
							<ol>
								<li>Amount of <strong>capital gains</strong> arising on transfer
									of residential house; or
								</li>
								<li>Amount <strong>invested in purchase/construction</strong> of
									new residential house property [including the amount deposited
									in Capital Gains Deposit Account Scheme]&nbsp;
								</li>
							</ol>
							<p>
								An <strong>individual or HUF</strong> only can claim benefit u/s
								54 upon fulfillment of following basic conditions:
							</p>
							<ol>
								<li>The asset transferred should be a long-term capital asset,
									being a residential house property.</li>
								<li>Within a period of one year before or two years after the
									date of transfer of old house, the taxpayer should acquire
									another residential house or should construct a residential
									house within a period of three years from the date of transfer
									of the old house. In case of compulsory acquisition the period
									of acquisition or construction will be determined from the date
									of receipt of compensation (whether original or additional).</li>
							</ol>
							<p>
								However, with effect from <strong>assessment year 2015-16 </strong>exemption
								can be claimed only in respect of <strong>one residential house
									property purchased/constructed in India</strong>. If more than
								one house is purchased or constructed, then exemption under
								section 54 will be available in respect of one house only. No
								exemption can be claimed in respect of house purchased outside
								India.
							</p>
							<p>Text of Amendments in for the above Sections is given as
								under:</p>
							<p>
								<em>&ldquo;In section 54 of the Income-tax Act, in sub-section
									(1), for the words &ldquo;constructed,&nbsp;<strong>a&nbsp;</strong>residential
									house&rdquo;, the words &ldquo;constructed,&nbsp;<strong>one&nbsp;</strong>residential
									house in India&rdquo; shall be substituted with effect from the
									1st day of April, 2015.&rdquo;
								</em>
							</p>
							<p>Besides, to keep a check on the misutilization of this
								exemption, a restriction is imposed upon the assessee availing
								this exemption. The provisions of Section 54 reads as:</p>
							<ul>
								<li>The restriction will be attracted, if after claiming
									exemption under section 54, the new house is sold before a
									period of 3 years from the date of its purchase/completion of
									construction.</li>
								<li>If the new house is sold before a period of 3 years from the
									date of its purchase/completion of construction, then at the
									time of computation of capital gain arising on transfer of the
									new house, the amount of capital gain claimed as exempt under
									section 54 will be deducted from the cost of acquisition of the
									new house.</li>
							</ul>
							</li>
							</ol>
							<p>
								Use the <a href="http://rubiks-cu.be/#cubesolver">Rubik Cube
									solver</a> program to calculate the solution for your unsolved
								Rubik's Cube.
							</p>
						</div>

						<button class="accordion">What incomes are charged to tax under
							the head &ldquo;Capital Gains&rdquo;?</button>
						<div class="panel">
							<p>
								Any profit or gain arising from transfer of a <u>capital asset</u>
								[as defined u/s 2(14) of IT Act,1961] during the year is charged
								to tax under the head &ldquo;Capital Gains&rdquo;.
							</p>
						</div>

						<button class="accordion">What is the meaning of capital asset?</button>
						<div class="panel">
							<p>Capital asset is defined to include:</p>
							<ol>
								<li>a) Any kind of property held by an assesse, whether or not
									connected with business or profession of the assesse.</li>
								<li>b) Any securities held by a FII which has invested in such
									securities in accordance with the regulations made under the
									SEBI Act, 1992.</li>
							</ol>
							<p>
								However, the following items are <strong><em>excluded</em></strong>
								from the definition of "capital asset":
							</p>
							<ul>
								<li>Any stock-in-trade, consumable stores, or raw materials held
									by a person for the purpose of his business or profession.</li>
							</ul>
							<p>E.g., Motor car for a motor car dealer or gold for a jewellery
								merchant, are their stock-in-trade and, hence, they are not
								capital assets for them.</p>
							<ul>
								<li>Personal effects of a person, that is to say, movable
									property including wearing apparels (*) and furniture held for
									use, by a person or for use by any member of his family
									dependent on him.</li>
							</ul>
							<p>(*) However, jewellery, archeological collections, drawings,
								paintings, sculptures, or any work of art are not treated as
								personal effects and, hence, are included in the definition of
								capital assets.</p>
							<p>The term jewellery has been given a wider meaning and includes
								ornaments made up of gold, silver, platinum or any other
								precious metal or any alloy containing one or more of such
								precious metals, whether or not containing any precious or
								semi-precious stones, and whether or not worked or sewn into any
								wearing apparel. It also includes precious or semi-precious
								stones, whether or not set in any furniture, utensil, or other
								article or worked or sewn into any wearing apparel.</p>
							<ul>
								<li>Agricultural Land in India, not being a land situated:</li>
							</ul>
							<ol>
								<li>Within jurisdiction of municipality, notified area
									committee, town area committee, cantonment board and which has
									a population of not less than 10,000;</li>
								<li>Within range of following distance measured aerially from
									the local limits of any municipality or cantonment board:</li>
							</ol>
							<ul>
								<li>not being more than 2 KMs, if population of such area is
									more than 10,000 but not exceeding 1 lakh;</li>
							</ul>
							<ol>
								<li>not being more than 6 KMs , if population of such area is
									more than 1 lakh but not exceeding 10 lakhs; or</li>
								<li>not being more than 8 KMs , if population of such area is
									more than 10 lakhs.</li>
							</ol>
							<p>Population is to be considered according to the figures of
								last preceding census of which relevant figures have been
								published before the first day of the year.</p>
							<ul>
								<li>6&frac12;% Gold Bonds,1977 or 7% Gold Bonds, 1980 or
									National Defense Gold Bonds, 1980 issued by the Central
									Government.</li>
								<li>Special Bearer Bonds, 1991, issued by the Central Government</li>
								<li>Gold Deposit Bonds issued under Gold Deposit Scheme, 1999.</li>
								<li>Deposit certificates issued under the Gold Monetization
									Scheme, 2015.</li>
							</ul>
							<p>
								<em><u>Following points should be kept in mind :</u></em>
							</p>
							<p>The property being capital asset may or may not be connected
								with the business or profession of the taxpayer. E.g. Bus used
								to carry passenger by a person engaged in the business of
								passenger transport will be his Capital asset.</p>
							<p>Any securities held by a Foreign Institutional Investor which
								has invested in such securities in accordance with the
								regulations made under the Securities and Exchange Board of
								India Act, 1992 will always be treated as capital asset, hence,
								such securities cannot be treated as stock-in-trade.</p>
						</div>

						<button class="accordion">What is the meaning of the term
							&lsquo;long-term capital asset&rsquo;?</button>
						<div class="panel">
							<p>Any capital asset held by a person for a period of more than
								36 months immediately preceding the date of its transfer will be
								treated as long-term capital asset.</p>
							<p>
								<strong>&nbsp;&nbsp;&nbsp;&nbsp; </strong>Any capital asset held
								by a person for a period of not more than 36 months immediately
								preceding the date of its transfer will be a short-term capital
								asset.
							</p>
							<p>However, in respect of certain assets like shares (equity or
								preference) which are listed in a recognised stock exchange in
								India, units of equity oriented mutual funds, listed securities
								like debentures and Government securities, Units of UTI and Zero
								Coupon Bonds, the period of holding to be considered is 12
								months instead of 36 months.</p>
							<p>In case of unlisted shares in a company, the period of holding
								to be considered is 24 months instead of 36 months.</p>
							<p>
								<strong><em>Illustration</em></strong>
							</p>
							<p>Mr. Raj is a salaried employee. On 8th April, 2014, he
								purchased a piece of land and sold the same on 29th June, 2016.
								In this case, land is a capital asset for Mr. Raj. He purchased
								the land on 8th April, 2014 and sold it on 29th June, 2016,
								i.e., after holding it for a period of less than 36 months.
								Hence, land will be a short-term capital asset.</p>
							<p>
								<strong><em>Illustration</em></strong>
							</p>
							<p>Mr. Kumar is a salaried employee. On 8th July, 2015, he
								purchased shares of SBI Ltd. (listed in BSE) and sold the same
								on 29th June, 2016. In this case, shares are capital assets for
								Mr. Kumar. He purchased shares on 8th July, 2015 and sold them
								on 29th June, 2016 i.e., after holding them for a period of less
								than 12 months. Hence, shares are short-term capital assets.</p>
						</div>

						<button class="accordion">What is long-term capital gain and
							short-term capital gain?</button>
						<div class="panel">
							<p>Gain arising on transfer of long-term capital asset is termed
								as long-term capital gain and gain arising on transfer of
								short-term capital asset is termed as short-term capital gain.
								However, there are a few exceptions to this rule, like gain on
								depreciable asset is always taxed as short-term capital gain.</p>
						</div>

						<button class="accordion">Why capital gains are classified as
							short-term and long-term?</button>
						<div class="panel">
							<p>The taxability of capital gain depends on the nature of gain,
								i.e. whether short-term or long-term. Hence to determine the
								taxability, capital gains are classified into short-term capital
								gain and long-term capital gain. In other words, the tax rates
								for long-term capital gain and short-term capital gain are
								different. Similarly, computation provisions are different for
								long-term capital gains and short-term capital gains.</p>
						</div>

						<button class="accordion">Is the benefit of indexation available
							while computing capital gain arising on transfer of short-term
							capital asset?</button>
						<div class="panel">
							<p>
								<u><strong>No</strong></u>, the benefit of indexation is
								available only in case of long-term capital assets and is not
								available in case of short-term capital assets. Indexation is a
								process by which the cost of acquisition/improvement of a
								capital asset is adjusted against inflationary rise in the value
								of asset. Hence it is applicable only in the case of long term
								capital assets.
							</p>
						</div>

						<button class="accordion">In respect of capital asset acquired
							before 1st April, 1981 is there any special method to compute
							cost of acquisition?</button>
						<div class="panel">
							<p>
								<strong><u>Yes</u></strong>, the method of computation of cost
								of acquisition for capital asset acquired before 1<sup>st</sup>
								April, 1981 differs. Generally, cost of acquisition of a capital
								asset is the cost incurred in acquiring the capital asset. It
								includes the purchase consideration plus any expenditure
								incurred exclusively for acquiring the capital asset.
							</p>
							<p>&nbsp;</p>
							<p>
								However, in respect of capital asset acquired before 1st April,
								1981, the cost of acquisition will be <strong><u>higher</u></strong>
								of:
							</p>
							<ul>
								<li>the actual cost of acquisition of the asset; or</li>
								<li>fair market value of the asset as on 1st April, 1981.</li>
							</ul>
							<p>This option is not available in the case of a depreciable
								asset.</p>
						</div>

						<button class="accordion">As per the Income-tax Law, gain arising
							on transfer of capital asset is charged to tax under the head
							&ldquo;Capital gains&rdquo;. What constitutes
							&lsquo;transfer&rsquo; as per Income-tax Law?</button>
						<div class="panel">
							<p>&nbsp;</p>
							<p>Generally, transfer means sale, however, for the purpose of
								Income-tax Law "Transfer&rdquo;, in relation to a capital asset,
								includes:</p>
							<ol>
								<li>Sale, exchange or relinquishment of the asset;</li>
								<li>Extinguishment of any rights in relation to a capital asset;</li>
							</ol>
							<p>iii. Compulsory acquisition of an asset;</p>
							<ol>
								<li>Conversion of capital asset into stock-in-trade;</li>
								<li>Maturity or redemption of a zero coupon bond;</li>
								<li>Allowing possession of immovable properties to the buyer in
									part performance of the contract;</li>
							</ol>
							<p>vii. Any transaction which has the effect of transferring an
								(or enabling the enjoyment of) immovable property; or</p>
							<p>viii. Disposing of or parting with an asset or any interest
								therein or creating any interest in any asset in any manner
								whatsoever.</p>
						</div>

						<button class="accordion">What are the provisions relating to
							computation of capital gain in case of transfer of asset by way
							of gift, will, etc.?</button>
						<div class="panel">
							<p>Capital gain arises if a person transfers a capital asset.
								section 47 excludes various transactions from the definition of
								'transfer'. Thus, transactions covered under section 47 are not
								deemed as 'transfer' and, hence, these transactions will not
								give rise to any capital gain.&nbsp; Transfer of capital asset
								by way of gift, will, etc., are few major transactions covered
								in section 47. Thus, if a person gifts his capital asset to any
								other person, then no capital gain will arise in the hands of
								the person making the gift (*).</p>
							<p>If the person receiving the capital asset by way of gift,
								will, etc. subsequently transfers such asset, capital gain will
								arise in his hands. Special provisions are designed to compute
								capital gains in the hands of the person receiving the asset by
								way of gift, will, etc. In such a case, the cost of acquisition
								of the capital asset will be the cost of acquisition to the
								previous owner and the period of holding of the capital asset
								will be computed from the date of acquisition of the capital
								asset by the previous owner.</p>
							<p>(*) As regards the taxability of gift in the hands of person
								receiving the gift, separate provisions are designed under
								section 56.</p>
						</div>

						<button class="accordion">I have sold a house which had been
							purchased by me 5 years ago. Am I required to pay any tax on the
							profit earned by me on account of such sale?</button>
						<div class="panel">
							<p>House sold by you is a long-term capital asset. Any gain
								arising on transfer of capital asset is charged to tax under the
								head &ldquo;Capital Gains&rdquo;. Income-tax Law has prescribed
								the method of computing capital gain arising on account of sale
								of capital assets. Thus, to check the taxability in your case,
								you have to compute capital gain by following the rules laid
								down in this regard, and if the result is gain, then the same
								will be liable to tax.</p>
						</div>

						<button class="accordion">Are any capital gains exempt under
							section 10?</button>
						<div class="panel">
							<p>
								<strong><em>Section 10:</em></strong> provides list of incomes
								which are exempt from tax Amongst these the major exemptions
								relating to capital gains are listed below:
							</p>
							<p>
								<strong><em>Section 10(33)</em></strong>: Long-term or
								short-term capital gain arising on transfer of units of Unit
								Scheme, 1964 (US 64) (transferred on or after 1-4-2002).
							</p>
							<p>
								<strong><em>Section 10(37)</em></strong>: An individual or Hindu
								Undivided Family (HUF) can claim exemption in respect of capital
								gain arising on transfer of agricultural land situated in an
								urban&nbsp; area by way of compulsory acquisition. This
								exemption is available if the land was used by the taxpayer (or
								by his parents in the case of an individual) for agricultural
								purpose for a period of 2 years immediately preceding the date
								of its transfer. .
							</p>
							<p>
								<strong><em>Section 10(38</em></strong><strong><em>)</em></strong>:
								Long-term capital gain arising on transfer of equity shares or
								units of equity oriented mutual fund (*) or a unit of a business
								trust other than a unit allotted by the trust in exchange of
								shares of a special purpose vehicle as referred to in section
								47(xvii), will be exempt from tax, if the following conditions
								are satisfied:
							</p>
							<p>The asset transferred should be equity shares of a company or
								units of an equity oriented mutual fund or a unit of a business
								trust other than a unit allotted by the trust in exchange of
								shares of a special purpose vehicle as referred to in section
								47.</p>
							<p>The transaction should be liable to securities transaction tax
								at the time of transfer.</p>
							<p>Such asset should be a long-term capital asset.</p>
							<p>Transfer should take place on or after October 1, 2004.</p>
							<p>Note: Any long-term capital gain arising from a transaction
								undertaken in recognized stock exchange located in an
								International Financial Service Center shall be exempt from tax.
								Such exemption is available if such transaction is undertaken in
								foreign current and even if no STT is paid on such transaction.
								[Inserted by the Finance Act w.e.f. 1-4-2017]</p>
							<p>(*) Equity oriented mutual fund means a mutual fund specified
								under section 10(23D) and 65% of its investible funds, out of
								total proceeds of such fund are invested in equity shares of
								domestic companies.</p>
						</div>

						<button class="accordion">Is there any benefit available in
							respect of re-investment of capital gain in any other capital
							asset?</button>
						<div class="panel">
							<p>A taxpayer can claim exemption from certain capital gains by
								re-investing the capital gain into specified asset. The
								following table highlights the assets in respect of which the
								benefit of re-investment is available:</p>
							<p>
								<strong><em>&nbsp;</em></strong>
							</p>
							<table width="100%">
								<tbody>
									<tr>
										<td width="113">
											<p>
												<strong><em>Section under</em></strong>
											</p>
											<p>
												<strong><em>which benefit</em></strong>
											</p>
											<p>
												<strong><em>is available</em></strong>
											</p>
										</td>
										<td width="333">
											<p>
												<strong><em>Gain eligible for claiming exemption</em></strong>
											</p>
										</td>
										<td width="261">
											<p>
												<strong><em>Asset in which the capital gain is to be
														re-invested to claim exemption</em></strong>
											</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>
												<a
													href="http://www.incometaxindia.gov.in/_layouts/15/dit/mobile/faqs/faq-questions.aspx?key=FAQs+on+Capital+Gains&amp;k="><strong><em>section
															54</em></strong></a><strong><em></em></strong>
											</p>
										</td>
										<td>
											<p>Long-term capital gain arising on transfer of residential
												house property.</p>
										</td>
										<td>
											<p>Gain to be re-invested in purchase or construction of one
												residential house property in India.</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>
												<a
													href="http://www.incometaxindia.gov.in/_layouts/15/dit/mobile/faqs/faq-questions.aspx?key=FAQs+on+Capital+Gains&amp;k="><strong><em>section
															54B</em></strong></a>
											</p>
										</td>
										<td>
											<p>Long-term or short-term capital gain arising on transfer
												of agricultural land.</p>
										</td>
										<td>
											<p>Gain to be re-invested in purchase of agricultural land.</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>
												<a
													href="http://www.incometaxindia.gov.in/_layouts/15/dit/mobile/faqs/faq-questions.aspx?key=FAQs+on+Capital+Gains&amp;k="><strong><em>section
															54EC</em></strong></a>
											</p>
										</td>
										<td>
											<p>Long-term capital gain arising on transfer of any capital
												asset.</p>
										</td>
										<td>
											<p>Gain to be re-invested in bonds issued by National Highway
												Authority of India or by the Rural Electrification
												Corporation Limited.</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>
												<strong><em>&nbsp;</em></strong><a
													href="http://www.incometaxindia.gov.in/_layouts/15/dit/mobile/faqs/faq-questions.aspx?key=FAQs+on+Capital+Gains&amp;k="><strong><em>Section
															54EE</em></strong></a>
											</p>
										</td>
										<td>
											<p>Long-term capital gain arising on transfer of any capital
												asset.</p>
										</td>
										<td>
											<p>Gain to be re-invested in units of specified fund, as may
												be notified by Govt. to finance start-ups.</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>
												<a
													href="http://www.incometaxindia.gov.in/_layouts/15/dit/mobile/faqs/faq-questions.aspx?key=FAQs+on+Capital+Gains&amp;k="><strong><em>section
															54F</em></strong></a>
											</p>
										</td>
										<td>
											<p>Long-term capital gain arising on transfer of any capital
												asset other than residential house property.</p>
										</td>
										<td>
											<p>Net sale consideration to be re-invested in purchase or
												construction of one residential house property in India.</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>
												<a
													href="http://www.incometaxindia.gov.in/_layouts/15/dit/mobile/faqs/faq-questions.aspx?key=FAQs+on+Capital+Gains&amp;k="><strong><em>section
															54D</em></strong></a>
											</p>
										</td>
										<td>
											<p>Gain arising on transfer of land or building forming part
												of industrial undertaking which is compulsorily acquired by
												Government and was used for industrial purpose for a period
												of 2 years prior to its acquisition.</p>
										</td>
										<td>
											<p>Gain to be re-invested to acquire land or building for
												industrial purpose.</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>
												<a
													href="http://www.incometaxindia.gov.in/_layouts/15/dit/mobile/faqs/faq-questions.aspx?key=FAQs+on+Capital+Gains&amp;k="><strong><em>section
															54G</em></strong></a>
											</p>
										</td>
										<td>
											<p>Gain arising on transfer of land, building, plant or
												machinery in order to shift an industrial undertaking from
												urban area to rural area</p>
										</td>
										<td>
											<p>Gain to be re-invested to acquire land, building, plant or
												machinery in order to shift the industrial undertaking from
												an urban area to a rural area</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>
												<a
													href="http://www.incometaxindia.gov.in/_layouts/15/dit/mobile/faqs/faq-questions.aspx?key=FAQs+on+Capital+Gains&amp;k="><strong><em>section
															54GA</em></strong></a>
											</p>
										</td>
										<td>
											<p>Gain arising on transfer of land, building, plant or
												machinery in order to shift an industrial undertaking from
												urban area to any Special Economic Zone</p>
										</td>
										<td>
											<p>Gain to be re-invested to acquire land, building, plant or
												machinery in order to shift the industrial undertaking from
												urban area to any Special Economic Zone.</p>
										</td>
									</tr>
									<tr>
										<td>
											<p>
												<a
													href="http://www.incometaxindia.gov.in/_layouts/15/dit/mobile/faqs/faq-questions.aspx?key=FAQs+on+Capital+Gains&amp;k="><strong><em>section
															54GB</em></strong></a><strong><em></em></strong>
											</p>
										</td>
										<td>
											<p>
												Long-term capital gain arising on transfer of residential
												property (a house or a plot of land). The transfer should
												take place during 1<sup>st</sup>&nbsp;April, 2012 and 31<sup>st</sup>&nbsp;March
												2017. However, in case of investment in &ldquo;eligible
												start-up&rdquo;, sunset limit of 31st march 2017 is extended
												to 31st march 2019.
											</p>
										</td>
										<td>
											<p>The net sale consideration should be utilised for
												subscription in equity shares of an "eligible company".
												&nbsp;W.e.f. April 1, 2017, eligible start-up is also
												included in definition of eligible company</p>
										</td>
									</tr>
								</tbody>
							</table>
							</p>
						</div>

						<button class="accordion">Are there any bonds in which I can
							invest my capital gains to claim tax relief?</button>
						<div class="panel">
							<p>
								<strong><u>Yes</u></strong>, as per section 54EC you can claim
								tax relief by investing the long-term capital gains in the bonds
								issued by the National Highway Authority of India or by the
								Rural Electrification Corporation Limited. The investment should
								be made within a period of 6 months from the date of transfer of
								capital asset and bonds should not be redeemed before 3 years.
								This benefit cannot be availed in respect of short-term capital
								gain. Maximum amount which qualifies for investment will be Rs.
								50,00,000. Thus, deduction under section 54EC cannot be claimed
								for more than Rs. 50,00,000.
							</p>
						</div>

						<button class="accordion">What is the meaning of stamp duty value
							and what is its relevance while computing capital gain in case of
							transfer of capital asset, being land or building or both?</button>
						<div class="panel">
							<p>Stamp duty value means the value adopted or assessed or
								assessable by any authority of a State Government for the
								purpose of payment of stamp duty.</p>
							<p>As per section 50C, while computing capital gain arising on
								transfer of land or building or both, if the actual sale
								consideration of such land and/or building is less than the
								stamp duty value, then the stamp duty value will be taken as
								full value of consideration, i.e., as deemed selling price and
								capital gain will be computed accordingly.</p>
							<p>
								<strong><em>Illustration</em></strong>
							</p>
							<p>Mr. Raja sold his bungalow for Rs. 80,00,000. The value
								adopted by the Stamp Valuation Authority of the bungalow for the
								purpose of payment of stamp duty is Rs. 84,00,000. In this
								situation, while computing taxable capital gain arising on
								transfer of bungalow, Rs. 84,00,000 will be taken as full value
								of consideration (i.e., sale value of the bungalow). Thus,
								actual selling price of Rs. 80,00,000 (being less than stamp
								duty value) will not be taken into account while computing
								taxable capital gain.</p>
							<p>
								<strong><em>Illustration</em></strong>
							</p>
							<p>Mr. Karan sold his land for Rs. 25,20,000. The value adopted
								by the Stamp Valuation Authority of the bungalow for the purpose
								of payment of stamp duty is Rs. 20,00,000. In this situation,
								while computing taxable capital gain arising on transfer of
								land, Rs. 25,20,000 (being actual sale value) will be taken as
								full value of consideration. Thus, stamp duty value (being less
								than actual selling price) will not be taken into account while
								computing taxable capital gain.</p>
							<ul>
								<li>What is the tax treatment of Advance money forfeited under a
									un-materialized contract for transfer of capital Asset?</li>
							</ul>
							<p>Any advance received on transfer of capital asset shall be
								chargeable to tax under the head 'Income from other sources', if
								such sum is forfeited and the negotiations do not result in
								transfer of capital Asset.</p>
						</div>

						<button class="accordion">Does booking of flat with the builder be
							eligible for claiming exemption u/s 54 / 54 F in case of long
							term capital gain from sale of property?</button>
						<div class="panel">
							<p>Yes, in order to claim exemption u/s 54 / 54F against long
								term capital gain from sale of house property, one can book the
								flat with the builder.</p>
							<p>As per the recent decision by ITAT in the case of ACIT Vs. Sh.
								Vineet Kumar Kapila (ITAT Delhi), ITAT held that booking of flat
								with the builder has to be treated as construction of flat by
								the assessee and hence period of three years would apply for
								construction of new house from the date of transfer of long term
								capital asset. Therefore, the Ld. CIT(A) has rightly allowed the
								exemption u/s. 54 of the Act, because in the present case also
								the flat booked with the builder by the assesse has to be
								considered as a case of construction of flat and the deduction
								claimed by the assessee u/s. 54 of the Act was rightly allowed,</p>
						</div>

						<a href="#faqhome">Back to categories</a><br>

						<h2 id="cashtransactions">Cash Transactions</h2>

						<button class="accordion">Cash Transaction Limit &ndash; Section
							269ST read with Section 271DA</button>
						<div class="panel">
							<p>
								In addition to section 40A(3), a new section 269ST has been
								inserted in the Income tax Act vide Finance Act 2017 with effect
								from <strong>01.04.2017</strong>, that <strong>prohibits receipt
									of an amount of INR 2 Lakhs or more</strong> by a person, in
								the circumstances specified therein, through modes other than by
								way of an account payee cheque or an account payee bank draft or
								use of electronic clearing system through a bank account. In
								other words, <u>No person</u>&nbsp;shall receive an amount of
								two lakh rupees or more&mdash;
							</p>
							<p>(a) in aggregate from a person in a day; or</p>
							<p>(b) in respect of a single transaction; or</p>
							<p>(c) in respect of transactions relating to one event or
								occasion from a person,</p>
							<p>otherwise than by an account payee cheque or an account payee
								bank draft or use of electronic clearing system through a bank
								account.</p>
							<p>However, in the case of repayment of loan by NBFCs or HFCs,
								the receipt of one instalment of loan repayment in respect of a
								loan shall constitute a &lsquo;single transaction&rsquo; as
								specified in clause (b) of section 269ST of the Act and all the
								instalments paid for a loan shall not be aggregated for the
								purposes of determining applicability of the provisions section
								269ST.</p>
							<p>
								<strong>Penal provisions</strong> have also been introduced by
								way of a new <strong>section 271DA</strong>, which provides that
								if a person receives any amount in contravention to the
								provisions of section 269ST, it shall be liable to pay penalty
								of a sum equal to the amount of such receipt.
							</p>
							<p>The above section is applicable to all the persons-
								individuals, HUF, Firm, LLP, company, Trust etc, transacting in
								cash irrespective of the nature of transactions i.e Capital or
								Revenue.</p>
						</div>

						<button class="accordion">Cash Transaction Limit &ndash; Section
							40A(3)</button>
						<div class="panel">
							<p>
								The amendment brought about under the existing section 40A(3)
								vide Finance Bill 2017 with effect from <strong>01.04.2018 </strong>is
								basically to promote the concept of Digital India and there by
								discourage cash transaction. As per the amendment, the existing
								threshold of cash payment of <u>Rs.20,000 has been reduced to
									Rs.10,000 per person in a single day</u>, i.e any payment in
								cash above ten thousand rupees to a person in a day, shall not
								be allowed as deduction in computation of Income from
								&ldquo;Profits and gains of business or profession&ldquo;.
							</p>
							<p>
								<em>Extract of relevant clause from&nbsp;Finance Bill, 2017</em>
							</p>
							<p>
								<strong>Amendment of section 40A.</strong>
							</p>
							<p>In section 40A of the Income-tax Act,&mdash;</p>
							<p>
								(a) in sub-section (<em>2</em>), in clause (<em>a</em>), in the
								proviso, after the words &ldquo;Provided that&rdquo;, the words,
								figures and letters &ldquo;for an assessment year commencing on
								or before the 1st day of April, 2016&rdquo; shall be inserted;
							</p>
							<p>(b) with effect from the 1st day of April, 2018,&mdash;</p>
							<p>
								(A) in sub-section (<em>3</em>), for the words &ldquo;exceeds
								twenty thousand rupees&rdquo;, the words &ldquo;or use of
								electronic clearing system through a bank account, exceeds ten
								thousand rupees,&rdquo; shall be substituted;
							</p>
							<p>
								<strong>in sub-section (<em>3A</em>),&mdash;
								</strong>
							</p>
							<p>(i) after the words &ldquo;account payee bank draft,&rdquo;,
								the words &ldquo;or use of electronic clearing system through a
								bank account&rdquo; shall be inserted;</p>
							<p>(ii) for the words &ldquo;twenty thousand rupees&rdquo;, the
								words &ldquo;ten thousand rupees&rdquo; shall be substituted;</p>
							<p>(iii) in the first proviso, for the words &ldquo;exceeds
								twenty thousand rupees&rdquo;, the words &ldquo;or use of
								electronic clearing system through a bank account, exceeds ten
								thousand rupees,&rdquo; shall be substituted;</p>
							<p>(iv) in the second proviso, for the words &ldquo;twenty
								thousand rupees&rdquo;, the words &ldquo;ten thousand
								rupees&rdquo; shall be substituted;</p>
						</div>

						<a href="#faqhome">Back to categories</a><br>

						<h2 id="otherincome">Other Income</h2>

						<button class="accordion">Taxation of Gifts in the hands of
							Individual / HUF &ndash; Section 56(2)(vii)</button>
						<div class="panel">
							<p>
								The term &ldquo;<strong>Gift</strong>&rdquo; denotes anything
								received by anyone without consideration or inadequate
								consideration.
							</p>
							<p>According to the provisions of Income Tax Law, in case of
								Gifts, in all situation the receiver of Gift shall be under the
								purview of Taxation. Section 56 of the Income Tax Act contains
								the relevant provisions that guide the Taxation of Gifts.</p>
							<p>
								Not all the gifts received are taxable in the hands of the
								recipient. As per section 56(2)(vii) following Gifts received by
								an <strong>Individual or HUF </strong>are taxable under the head
								&ldquo;Income from other Sources&rdquo;:
							</p>
							<ol>
								<li>Aggregate of all the <strong>Cash Gifts </strong>received in
									a Financial year in excess of Rs.50,000 is taxable
								</li>
								<li>Value of Gift of immovable property having a <strong>Stamp
										value in excess of INR 50,000</strong> after taking into
									consideration any inadequate consideration involved.
								</li>
								<li>Aggregate value of the Gift of movable property having a <strong>Fair
										Market Value in excess of INR 50,000 </strong>after taking
									into consideration the inadequate consideration involved
									therein (if any).
								</li>
							</ol>
							<p>
								<strong><u>Exceptions:</u></strong>
							</p>
							<p>
								In certain situations / conditions, gift received in excess of
								the above mentioned limit of Rs.50,000 shall <strong>not be
									taxable</strong>:
							</p>
							<ol>
								<li>Gifts from <strong>Relatives</strong>*
								</li>
								<li>Gift received on occasion of marriage <em>(Whether received
										from friends or relatives)</em></li>
								<li>Under will / Inheritance</li>
								<li>In contemplation of death (No will has been prepared but a
									person is about to die and he gives some amount to any person)</li>
								<li>Any trust (registered under section 10A)</li>
								<li>Any local authority</li>
								<li>Any university / Fund / Foundation / Educational institute /
									hospital / medical institution etc. (section 10 (23C)</li>
								<li><strong><em>transaction not regarded as transfer under
											clause (</em></strong><strong>vicb<em>) or clause (</em>vid<em>)
											or clause (</em>vii<em>) of Section 47 </em></strong><em>(These
										include transfer of shares in case of Amalgamation /Demerger
										of Company/Business Reorganization of a Cooperative Bank) <strong>(NEW
											AMENDMENT)</strong>
								</em></li>
							</ol>
							<p>
								* <u>Definition of Relative</u>
							</p>
							<p>
								<strong>In case of an Individual:</strong>
							</p>
							<ul>
								<li>Spouse</li>
								<li>Brother &ndash; Sister</li>
								<li>Brother &ndash; Sister, of spouse</li>
								<li>Brother &ndash; Sister, of parents.</li>
								<li>Lineal Ascendant Or Descendant of individual or Spouse</li>
								<li>Spouse, of above</li>
							</ul>
							<p>
								<strong>In case of HUF:</strong>
							</p>
							<p>All the members of HUF.</p>
							<p>
								Rubik's Cubes make a perfect gift for any occasion. Learn more
								about this amazing puzzle on <a href="https://ruwix.com/"
									rel="nofollow noopener">this link</a>.
							</p>
						</div>

						<button class="accordion">Can remuneration received by spouse of
							an individual be clubbed with his/her income?</button>
						<div class="panel">
							<p>Under certain circumstances as given in section 64(1)(ii) ,
								remuneration ( i.e., salary) received by the spouse of an
								individual from a concern in which the individual is having
								substantial interest is clubbed with the income of the
								individual. Provisions in this regard are as follows:</p>
							<ul>
								<li>The individual is having substantial interest in a concern
									(*).</li>
								<li>Spouse of the individual is employed in the concern in which
									the individual is having substantial interest.</li>
								<li>The spouse of the individual is employed without any
									technical or professional knowledge or experience ( i.e.,
									remuneration is not justifiable).</li>
							</ul>
							<p>(*) An individual shall be deemed to have substantial interest
								in any concern, if such individual alone or along with his
								relatives beneficially holds at any time during the previous
								year 20% or more of the equity shares (in case of a company) or
								is entitled to 20% of profit (in case of concern other than a
								company).</p>
							<p>Relative for this purpose includes husband, wife, brother or
								sister or lineal ascendantor descendent of that individual [
								section 2( 41 ) ].</p>
						</div>

						<button class="accordion">Is minor child&rsquo;s income clubbed
							with the income of parent?</button>
						<div class="panel">
							<p>As per section 64(1A) , income of minor child is clubbed with
								the income of his/her parent (*). Income of minor child earned
								on account of manual work or any activity involving application
								of his/her skill, knowledge, talent, experience, etc. will not
								be clubbed with the income of his/her parent. However, accretion
								from such income will be clubbed with the income of parent of
								such minor.</p>
							<p>Income of minor will be clubbed along with the income of that
								parent whose income (excluding minor's income) is higher.</p>
							<p>If the marriage of parents does not sustain, then minor's
								income will be clubbed with the income of parent who maintains
								the minor.</p>
							<p>In case the income of individual includes income of his/her
								minor child, such individual can claim an exemption under
								section 10(32) of Rs. 1,500 or income of minor so clubbed,
								whichever is less.</p>
							<p>(*) Provisions of section 64(1A) will not apply to any income
								of a minor child suffering from disability specified under
								section 80U . In other words income of a minor suffering from
								disability specified under section 80U will not be clubbed with
								the income of his/her parent.</p>
						</div>

						<button class="accordion">Will any clubbing provision apply in
							case of transfer of asset to Hindu Undivided Family (HUF) by its
							member?</button>
						<div class="panel">
							<p>As per section 64(2) , when an individual, being a member of
								HUF, transfers his property to the HUF otherwise than for
								adequate consideration or converts his property into the
								property belonging to the HUF (it is done by impressing such
								property with the character of joint family property or throwing
								such property into the common stock of the family), then
								clubbing provisions will apply as follows:</p>
							<ul>
								<li>Before partition of the HUF, entire income from such
									property will be clubbed with the income of transferor.</li>
								<li>After partition of the HUF, such property is distributed
									amongst the members of the family. In such a case income
									derived from such property by the spouse of the transferor will
									be clubbed with the income of the individual and will be
									charged to tax in his hands.</li>
							</ul>
							</p>
						</div>

						<a href="#faqhome">Back to categories</a><br>

						<h2 id="bussandprof">Business and Profession</h2>

						<button class="accordion">
							Insertion of Section 44ADA for Chargeability on Professionals
							income under head &ldquo;<em>Profits &amp; Gains from
								Business/Profession</em>&rdquo;:
						</button>
						<div class="panel">
							<p>
								&ldquo;<strong>Section 44ADA</strong>&rdquo; as inserted by the
								Finance Act 2016 with effect from 01.04.2017 (FY 2017-18) is
								proposed in line with the recommendation of Justice Easwar
								Committee for simplification of taxation of professionals.
							</p>
							<p>
								Certain <strong>resident professionals</strong> referred to in
								section 44AA(1)* of the Income Tax Act whose&nbsp;<strong><u>total
										gross receipts from profession does not exceed Rs. 50 lakhs</u></strong>&nbsp;in
								a financial year may offer a sum equal to <strong><u>higher of</u></strong>
								the below as income chargeable to Tax under the head &ldquo;<em>profits
									&amp; gains from business / profession&rdquo;:</em>
							</p>
							<ol>
								<li>50% of the gross receipts from profession&nbsp;<strong><u>(OR)</u></strong></li>
								<li>Income from profession offered by the assessee</li>
							</ol>
							<p>All deductions from sections 30 to 38 (including depreciation
								and un-absorbed depreciation / allowances) shall be deemed as
								allowed and Written down value (WDV) of depreciable assets shall
								be recomputed deducting depreciation which is deemed as allowed.</p>
							<p>Although an assessee opting to pay taxes u/s 44ADA are not
								required to maintain books of accounts, however to avoid legal
								consequences, necessary documents and evidences should be kept
								and preserved as substantial proof to verify the stand of the
								assessee.</p>
							<p>
								*Professionals with a <strong><u>technical certificate</u></strong>
								of their profession as specified u/s 44AA(1) who are eligible to
								opt for taxation u/s 44ADA are listed as under:
							</p>
							<ul>
								<li>Legal</li>
								<li>Medical</li>
								<li>Engineering</li>
								<li>Architecture</li>
								<li>Accountancy</li>
								<li>Technical consultancy</li>
								<li>Interior decoration</li>
								<li>Other notified professionals</li>
								<li>Authorized representatives</li>
								<li>Film Artists</li>
								<li>Certain sports related persons</li>
								<li>Company Secretaries and</li>
								<li>Information technology</li>
							</ul>
							<p>
								The online <a href="http://html-css-js.com/js/compressor/"
									target="_blank" rel="nofollow noopener">JavaScript compressor</a>
								will definitely help you optimize your scripts for a faster page
								loading.
							</p>
						</div>

						<button class="accordion">Can a person whose total turnover or
							gross receipts for the year exceed Rs. 2,00,00,000 adopt the
							presumptive taxation scheme of section 44ADA?</button>
						<div class="panel">
							<p>The presumptive taxation scheme of section 44ADA can be opted
								by the eligible persons if the total turnover or gross receipts
								from the business do not exceed the limit prescribed under
								section 44AB (i.e., Rs. 2,00,00,000). In other words, if the
								total turnover or gross receipt of the business exceeds Rs.
								2,00,00,000 then the scheme of section 44ADA cannot be adopted.
							</p>
						</div>

						<button class="accordion">Can an insurance agent adopt the
							presumptive taxation scheme of section 44AD?</button>
						<div class="panel">
							<p>A person who is earning income in the nature of commission or
								brokerage cannot adopt the presumptive taxation scheme of
								section 44AD. Insurance agents earn income by way of commission
								and, hence, they cannot adopt the presumptive taxation scheme of
								section 44AD</p>
						</div>

						<button class="accordion">For whom the presumptive taxation scheme
							of section 44AE is designed?</button>
						<div class="panel">
							<p>The scheme of sections 44AE is available to the person who
								owns not more than ten goods carriages at any time during the
								previous year and who is engaged in the business of plying,
								hiring or leasing such goods carriages.</p>
						</div>

						<button class="accordion">If a person adopts the presumptive
							taxation scheme of section 44AE, then is he liable to pay advance
							tax in respect of income from business covered under section
							44AE?</button>
						<div class="panel">
							<p>There is no concession as regards payment of advance tax in
								case of a person who is adopting the presumptive taxation scheme
								of section 44AE and, hence, he will be liable to pay advance tax
								even if he adopts the presumptive taxation scheme of section
								44AE</p>
						</div>

						<button class="accordion">What are Form Nos. 3CA/3CB and 3CD?</button>
						<div class="panel">
							<p>The report of the tax audit conducted by the chartered
								accountant is to be furnished in the prescribed form. The form
								prescribed for audit report in respect of audit conducted under
								section 44AB is Form No. 3CB and the prescribed particulars are
								to be reported in Form No. 3CD.</p>
							<p>In case of persons covered under those who are required to get
								their accounts audited by or under any other law, the form
								prescribed for audit report is Form No. 3CA/3CB and the
								prescribed particulars are to be reported in Form No. 3CD.</p>
						</div>

						<button class="accordion">What is the penalty for not getting the
							accounts audited as required by section 44AB?</button>
						<div class="panel">
							<p>According to section 271B, if any person who is required to
								comply with section 44AB fails to get his accounts audited in
								respect of any year or years as required under section 44AB, the
								Assessing Officer may impose a penalty. The penalty shall be
								lower of the following amounts:</p>
							<p>(a) 0.5% of the total sales, turnover or gross receipts, as
								the case may be, in business, or of the gross receipts in
								profession, in such year or years.</p>
							<p>(b) Rs. 1,50,000.</p>
							<p>However, according to section 273B, no penalty shall be
								imposed if reasonable cause for such failure is proved.</p>
						</div>

						<button class="accordion">Can a person engaged in a profession as
							prescribed under section 44AA(1) adopt the presumptive taxation
							scheme of section 44AD?</button>
						<div class="panel">
							<p>A person who is engaged in any profession as prescribed under
								section 44AA(1) cannot adopt the presumptive taxation scheme of
								section 44AD.</p>
							<p>However, he can opt for presumptive taxation scheme under
								section 44ADA and declare 50% of gross receipts of profession as
								his presumptive income. Presumptive Scheme under section 44ADA
								is applicable only for resident assessee whose total gross
								receipts of profession do not exceed fifty lakh rupees.</p>
						</div>

						<button class="accordion">What is the manner of computation of
							taxable business income under the normal provisions of the
							Income-tax Law, i.e., in case of a person not adopting the
							presumptive taxation scheme of section 44AD?</button>
						<div class="panel">
							<p>Generally, as per the Income-tax Law, the taxable business
								income of every person is computed as follows :</p>
							<p>Particulars Amount</p>
							<p>Turnover or gross receipts from the business XXXXX</p>
							<p>Less : Expenses incurred in relation to earning of the income
								(XXXXX)</p>
							<p>Taxable Business Income XXXXX</p>
							<p>For the purpose of computing taxable business income in the
								above manner, the taxpayers have to maintain books of account of
								the business and income will be computed on the basis of the
								information revealed in the books of accounts.</p>
						</div>

						<button class="accordion">If a person adopts the presumptive
							taxation scheme of section 44AD, then is he required to maintain
							books of account as per section 44AA?</button>
						<div class="panel">
							<p>Section 44AA deals with provisions relating to maintenance of
								books of account by a person engaged in business/profession.
								Thus, a person engaged in business/profession has to maintain
								books of account of his business/profession according to the
								provisions of section 44AA.</p>
							<p>In case of a person engaged in a business and opting for the
								presumptive taxation scheme of section 44AD, the provisions of
								section 44AA relating to maintenance of books of account will
								not apply. In other words, if a person adopts the provisions of
								section 44AD and declares income @ 8%/6% of the turnover, then
								he is not required to maintain the books of account as provided
								under section 44AA in respect of business covered under the
								presumptive taxation scheme of section 44AD.</p>
						</div>

						<a href="#faqhome">Back to categories</a><br>

						<h2 id="setoffandcarryforward">Set off and carry forward</h2>

						<button class="accordion">Inter head and Intra head set-off</button>
						<div class="panel">
							<p>
								<strong>Concept of set-off:</strong>
							</p>
							<p>
								<strong>&nbsp;</strong>
							</p>
							<p>Specific provisions have been made in the Income-tax Act, 1961
								for the set-off and carry forward of losses. In simple words,
								&ldquo;Set-off&rdquo; means adjustment of losses against the
								profits from another source/head of income in the same
								assessment year. If losses cannot be set-off in the same year
								due to inadequacy of eligible profits, then such losses are
								carried forward to the next assessment year for adjustment
								against the eligible profits of that year. The maximum period
								for which different losses can be carried forward for set-off
								has been provided in the Act.</p>
							<p>&nbsp;</p>
							<p>
								<strong>Intra head set off/Inter source adjustment [Section 70]</strong>
							</p>
							<p>&nbsp;</p>
							<p>Under this section, the losses incurred by the Assessee in
								respect of one source shall be set-off against income from any
								other source under the same head of income, since the income
								under each head is to be computed by grouping together the net
								result of the activities of all the sources covered by that
								head. In simpler terms, loss from one source of income can be
								adjusted against income from another source, both the sources
								being under the same head.</p>
							<p>&nbsp;</p>
							<p>
								<em>Example 1:</em> Loss from one house property can be set off
								against the income from another house property.
							</p>
							<p>
								<em>Example 2</em><strong>: </strong>Loss from one business, say
								textiles, can be set off against income from any other business,
								say printing, in the same year as both these sources of income
								fall under one head of income. Therefore, the loss in one
								business may be set-off against the profits from another
								business in the same year.
							</p>
							<p>&nbsp;</p>
							<p>
								<strong>Exemptions for the above:</strong>
							</p>
							<p>
								<strong>&nbsp;</strong>
							</p>
							<ol>
								<li>Short-term capital loss is allowed to be set off against
									both short-term capital gain and long-term capital gain.
									However, long-term capital loss can be set-off only against
									long-term capital gain and not short-term capital gain.</li>
								<li>A loss in speculation business can be set-off only against
									the profits of any other speculation business and not against
									any other business or professional income. However, losses from
									other business can be adjusted against profits from speculation
									business.</li>
								<li>The losses incurred by an Assessee from the activity of
									owning and maintaining race horses cannot be set-off against
									the income from any other source other than the activity of
									owning and maintaining race horses.</li>
							</ol>
							<p>&nbsp;</p>
							<p>
								<strong>Inter head adjustment [Section 71]</strong>
							</p>
							<p>
								<strong>&nbsp;</strong>
							</p>
							<p>Loss under one head of income can be adjusted or set off
								against income under another head. However, the following points
								should be considered:</p>
							<p>&nbsp;</p>
							<ol>
								<li>Loss under head Capital Gains cannot be set-off with any
									other head of income.</li>
								<li>Loss under the head Profits and gains from business or
									profession cannot be set off with income from salary</li>
								<li>Speculation loss and loss from the activity of owning and
									maintaining race horses cannot be set off against income under
									any other head.</li>
							</ol>
							<p>&nbsp;</p>
							<p>
								<strong>Loss from exempted source of income cannot be adjusted
									against taxable income:</strong>
							</p>
							<p>
								<strong>&nbsp;</strong>
							</p>
							<p>If income from a particular source is exempt from tax, then
								loss from such exempted source cannot be set off against any
								other income which is chargeable to tax.</p>
							<p>&nbsp;</p>
							<p>
								<strong>E.g.,</strong> Agricultural income is exempt from tax,
								if the taxpayer incurs loss from agricultural activity, then
								such loss cannot be adjusted against any other taxable income.
							</p>
						</div>

						<button class="accordion">Intra &amp; Inter Head set-off in brief</button>
						<div class="panel">
							<p>Let us understand the inter source and inter head setoff
								process in brief in a tabular format:</p>
							<table width="0">
								<tbody>
									<tr>
										<td colspan="2" rowspan="2" width="170">
											<p>
												<strong><u>Particulars of loss</u></strong>
											</p>
										</td>
										<td rowspan="2" width="64">
											<p>
												<strong><u>Income from Salary</u></strong>
											</p>
										</td>
										<td rowspan="2" width="75">
											<p>
												<strong><u>Income from House property</u></strong>
											</p>
										</td>
										<td width="63">
											<p>
												<strong><u>Long Term Capital Gain</u></strong>
											</p>
										</td>
										<td width="65">
											<p>
												<strong><u>Short Term capital gain</u></strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong><u>Non Speculative Business profit</u></strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong><u>Speculative Business Profit</u></strong>
											</p>
										</td>
										<td rowspan="2" width="69">
											<p>
												<strong><u>Income from other Sources</u></strong>
											</p>
										</td>
										<td rowspan="2" width="96">
											<p>
												<strong><u>Gains from Activity of owning and maintaining
														race horses</u></strong>
											</p>
										</td>
									</tr>
									<tr>
										<td colspan="2" width="127">
											<p>
												<strong><u>Capital Gains</u></strong>
											</p>
										</td>
										<td colspan="2" width="187">
											<p>
												<strong><u>Business/ Professional Income</u></strong>
											</p>
										</td>
									</tr>
									<tr>
										<td colspan="2" width="170">
											<p>Loss from House property</p>
										</td>
										<td width="64">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="75">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="63">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="65">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="69">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="96">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
									</tr>
									<tr>
										<td width="97">
											<p>Short Term Capital loss</p>
										</td>
										<td rowspan="2" width="73">
											<p>
												<strong><u>Capital Gains</u></strong>
											</p>
											<p>&nbsp;</p>
											<p>&nbsp;</p>
										</td>
										<td width="64">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="75">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="63">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="65">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="69">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="96">
											<p>
												<strong>O</strong>
											</p>
										</td>
									</tr>
									<tr>
										<td width="97">
											<p>Long term capital loss</p>
										</td>
										<td width="64">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="75">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="63">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="65">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="69">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="96">
											<p>
												<strong>O</strong>
											</p>
										</td>
									</tr>
									<tr>
										<td width="97">
											<p>Non Speculative Business loss</p>
										</td>
										<td rowspan="3" width="73">
											<p>
												<strong><u>Business Income</u></strong>
											</p>
										</td>
										<td width="64">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="75">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="63">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="65">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="69">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="96">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
									</tr>
									<tr>
										<td width="97">
											<p>Speculative business loss</p>
										</td>
										<td width="64">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="75">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="63">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="65">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
										<td width="69">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="96">
											<p>
												<strong>O</strong>
											</p>
										</td>
									</tr>
									<tr>
										<td width="97">
											<p>Loss from owning and maintaining race horses</p>
										</td>
										<td width="64">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="75">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="63">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="65">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="93">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="69">
											<p>
												<strong>O</strong>
											</p>
										</td>
										<td width="96">
											<p>
												<strong>&uuml;</strong>
											</p>
										</td>
									</tr>
								</tbody>
							</table>
							</p>
						</div>

						<button class="accordion">Losses</button>
						<div class="panel">
							<p>
								<strong>Carry forward of Losses:</strong>
							</p>
							<p>&nbsp;</p>
							<p>Many times it may happen that after making intra-head and
								inter-head adjustments, still the loss remains unadjusted due to
								inadequacy of eligible profits, such losses are carried forward
								to the next assessment year for adjustment against the eligible
								profits of that year. The maximum period for which different
								losses can be carried forward for set-off has been provided in
								the Income Tax Act.</p>
							<p>&nbsp;</p>
							<p>
								<strong>Carry forward and set off of business loss:</strong>
							</p>
							<p>&nbsp;</p>
							<ul>
								<li>If loss of any business/profession cannot be fully adjusted
									in the year in which it is actually incurred, then the
									unadjusted loss can be carried forward to the next year for
									making adjustment. In the next years such loss can be adjusted
									only with the income under the head &ldquo;Profits and gains of
									business or profession&rdquo;</li>
							</ul>
							<p>&nbsp;</p>
							<ul>
								<li>Loss under the head &ldquo;Profits and gains of business or
									profession&rdquo; can be carried forward only if the Income tax
									return of the year in which loss is incurred is filed on or
									before the due date of filing the return.</li>
							</ul>
							<p>
								<strong>&nbsp;</strong>
							</p>
							<p>
								<strong>Non speculative Business loss:</strong>
							</p>
							<p>&nbsp;</p>
							<ul>
								<li>Loss from non speculative business can be carried forward
									for eight years immediately next to the year in which the loss
									is incurred.</li>
							</ul>
							<p>
								<strong>&nbsp;</strong>
							</p>
							<p>
								<strong>Loss from owning and maintaining race horses:</strong>
							</p>
							<p>
								<strong>&nbsp;</strong>
							</p>
							<ul>
								<li>Loss from the business of owning and maintaining race horses
									cannot be set off against any income other than income from the
									business of owning and maintaining race horses. Such loss can
									be carried forward only for a period of 4 years</li>
							</ul>
							<p>&nbsp;</p>
							<p>
								<strong>Speculative Business loss:</strong>
							</p>
							<p>&nbsp;</p>
							<ul>
								<li>If loss of any speculative business cannot be fully adjusted
									in the year in which it is incurred, then the unadjusted
									speculative loss can be carried forward for making adjustment
									in the next year. In the subsequent years such loss can be
									adjusted only against income from speculative business (may be
									same or any other speculative business). Such loss can be
									carried forward for 4 years immediately succeeding the year in
									which the loss is incurred.</li>
							</ul>
							<p>
								<strong>&nbsp;</strong>
							</p>
							<p>
								<strong>Specified Business loss:</strong>
							</p>
							<p>
								<strong>&nbsp;</strong>
							</p>
							<ul>
								<li>Loss from business specified under section 35AD cannot be
									set off against any other income except income from specified
									business. Such loss can be carried forward for adjustment
									against income from specified business for any number of years</li>
							</ul>
							<p>&nbsp;</p>
							<ul>
								<li>Section 35AD is applicable in respect of certain specified
									businesses like setting up a cold chain facility, setting up
									and operating warehousing facility for storage of agricultural
									produce, developing and building a housing projects, etc.</li>
							</ul>
							</p>
						</div>

						<button class="accordion">Provisions under the Income-tax Law in
							relation to carry forward and set off of house property loss</button>
						<div class="panel">
							<p>
							
							
							<ul>
								<li>If loss under the head &ldquo;Income from house
									property&rdquo; cannot be fully adjusted in the year in which
									such loss is incurred, then unadjusted loss can be carried
									forward to next year.</li>
							</ul>
							<p>&nbsp;</p>
							<ul>
								<li>In the subsequent years(s) such loss can be adjusted only
									against income chargeable to tax under the head &ldquo;Income
									from house property&rdquo;</li>
							</ul>
							<p>&nbsp;</p>
							<ul>
								<li>Such loss can be carried forward for eight years immediately
									succeeding the year in which the loss is incurred.</li>
							</ul>
							Loss under the head &ldquo;Income from house property&rdquo; can
							be carried forward even if the Income Tax Return of the year in
							which loss is incurred is not filed on or before the due date of
							filing the return.
							</p>
						</div>

						<button class="accordion">Carry forward and set off of capital
							loss</button>
						<div class="panel">
							<p>
							
							
							<li>In case if loss under the head &ldquo;Capital gains&rdquo;
								incurred during a year cannot be adjusted in the same year, then
								unadjusted capital loss can be carried forward to next eight
								immediate years.</li>
							</ul>
							<p>&nbsp;</p>
							<ul>
								<li>Such loss can be carried forward only if the Income Tax
									Return is filed on or before the due date of filing the return.</li>
							</ul>
							<p>&nbsp;</p>
							<ul>
								<li>Such loss can only be set off under the head capital gain.
									Long-term capital loss can be adjusted only with long-term
									capital gains. Short-term capital loss can be adjusted with
									long-term capital gains as well as short-term capital gains.</li>
							</ul>
							</p>
						</div>

						<button class="accordion">Set off of unabsorbed depreciation,
							unabsorbed capital expenditure on scientific research and
							unabsorbed capital expenditure on promoting family planning
							amongst the employees</button>
						<div class="panel">
							<p>Apart from several other deductions, in computation of income
								chargeable to tax under the head &ldquo;Profits and gains of
								business or profession&rdquo; a person is allowed to claim
								deduction on account for depreciation, capital expenditure
								incurred by him on scientific research and capital expenditure
								incurred by a company for promoting family planning amongst its
								employees.</p>
							<p>&nbsp;</p>
							<p>If the income of the year in which these expenses are incurred
								falls short of these expenses, then the unabsorbed expenses can
								be carried forward to next year in the form of unabsorbed
								depreciation or unabsorbed capital expenditure on scientific
								research or unabsorbed capital expenditure on promoting family
								planning amongst the employees.</p>
							<p>&nbsp;</p>
							<p>The unabsorbed portion shall be added to the amount of
								depreciation for the following year and shall be deemed to be
								the part of depreciation for that year (similar treatment would
								be given to other allowances as mentioned above).</p>
							<p>&nbsp;</p>
							<p>However, while doing set off, following order needs to be
								followed:</p>
							<p>&nbsp;</p>
							<ol>
								<li>First adjustments are to be made for current scientific
									research expenditure, family planning expenditure and current
									depreciation.</li>
							</ol>
							<p>&nbsp;</p>
							<ol start="2">
								<li>Second adjustment is to be made for brought forward business
									loss.</li>
							</ol>
							<p>&nbsp;</p>
							<ol start="3">
								<li>Third adjustments are to be made for unabsorbed
									depreciation, unabsorbed capital expenditure on scientific
									research or on family planning.</li>
							</ol>
							</p>
						</div>

						<button class="accordion">Limitation on setoff of House property
							loss against other heads of Income (Rs.2, 00,000/-) (Section 71,
							71A and 71B)</button>
						<div class="panel">
							<p>As per the amendment in the finance budget 2017 with effect
								from 01-04-2018:</p>
							<p>&nbsp;</p>
							<p>
								Till the financial year 2016-17(AY 2017-18) there is no limit on
								the amount of house property loss set off with other heads of
								income but from financial year 2017-18 (AY 2018-19) that is from
								01<sup>st</sup> of April 2017, set off for the house property
								loss with other heads of income is limited to Rs. 2 Lakhs.
							</p>
							<p>&nbsp;</p>
							<p>The loss if any in excess to 2 Lakhs can be carried forward
								and set off with the income from house property in the next 8
								Assessment years (Immediate).</p>
							<p>&nbsp;</p>
							<p>One should be clear about the amendment; it is not limiting
								the housing loan interest u/s 24b for a let out property but is
								limiting the amount of loss that can be set off with other heads
								of income</p>
							<p>&nbsp;</p>
							<p>This amendment is not impacting those only having a single
								self occupied property because for self occupied property the
								housing loan interest that can be claimed as deduction under sec
								24b is limited to Rs. 2 Lakhs and the loss of house property
								which actually arises due to housing loan interest will also be
								less than or equal to Rs. 2 lakhs.</p>
							<p>&nbsp;</p>
							<p>When we see the let out/rented out property the housing loan
								interest that can be claimed as deduction under sec 24b is
								unlimited and loss arising from the house property may be more
								than 2 lakhs. In this way the new amendment impacts the let out/
								rented out property.</p>
						</div>

						<a href="#faqhome">Back to categories</a><br>

						<h2 id="assessment">Assessment</h2>

						<button class="accordion">Intimation u/s 143(1) can be considered
							as Assessment</button>
						<div class="panel">
							<p>Although an intimation received by an assessee u/s 143(1)
								within a period of 1 Year from the end of Financial Year in
								which the Return of Income was filed for the year under
								assessment can be considered as Assessment by the authorized
								Assessing Officer, however it is a matter of judgment where
								143(1) is taken as a mere Intimation rather than Assessment
								order.</p>
							<p>
								<em><u>Case Law: TATA AIG GENERAL INSURANCE PVT LTD Vs DCIT ITAT
										(Mumbai)- ITA No.968 (MUM)2015.</u></em>
							</p>
							<p>
								<strong><em>Assessment under section 143(1)</em></strong>
							</p>
							<p>This is intimation from the CPC and is a computer generated
								statement. All the data available in the Income Tax Return is
								validated by CPC with reference to the data available in the
								records of the income tax department.</p>
							<p>
								<strong><em>Scope of assessment under section 143(1)</em></strong>
							</p>
							<p>Intimation under section 143(1) is like primary verification
								of the return of income filed. At this stage of verification
								detailed examination of the return of income is not carried out.
								At this stage, the total income or loss is computed after making
								the adjustments (if any). Some of the adjustments made can be
								for the following matters:-</p>
							<ul>
								<li>The arithmetical errors in the return of income filed; or</li>
								<li>Any incorrect claims made in the return of income;</li>
								<li>Any loss claimed will be disallowed, if return of the
									previous year for which set-off of loss is claimed was
									furnished after the due date specified for filing the return of
									income;</li>
								<li>Any expenditure mentioned in the audit report will be
									disallowed if it is not taken into consideration for the
									computation of total income in the return filed;</li>
								<li>Any deduction claimed u/s 10AA, 80IA to 80-IE will also be
									disallowed, if the return is furnished after the due date
									specified for filing the return of income; or</li>
								<li>Any additional income appearing in Form 26AS which was not
									included in computing the total income in the return of income.</li>
							</ul>
							<p>However, no adjustment shall be made by CPC unless intimation
								is given to the assessee of such adjustment either in writing or
								in electronic mode. So, the assessee will get the intimation
								about the adjustment. If the assessee gives any response, such
								response shall be considered before making any adjustment, and
								if no response is received within 30 days from the issue of the
								intimation, adjustments shall be made by the department.</p>
							<p>
								Use the <a href="http://rubiks-cu.be/#cubesolver"
									rel="nofollow noopener">Rubik Cube solver</a> program to
								calculate the solution for your unsolved Rubik's Cube.
							</p>
						</div>

						<button class="accordion">After filing Return of Income u/s 139
							for a Financial year, What notice that can be received in its
							respect?</button>
						<div class="panel">
							<p>
								After successful filing of Return of Income by an assessee u/s
								139, the major notices that can be expected to come within a
								period of 1 year from the end of Financial Year in which the
								said return was filed is <u>Intimation Notice U/s 143(1)</u>, <u>Notice
									for Scrutiny Assessment u/s 143(2) ( within a period of 6
									months from the end of the Financial Year in which the said
									return was filed) </u>and <u>Notice for Best Judgement
									Assessment u/s 142(1).</u>
							</p>
							<p>
								In addition to above, a <u>notice u/s 133C</u> as inserted by
								the Finance (No.2) Act 2014 w.e.f 01/10/2014 can also be issued
								by the prescribed Income Tax authority for Return of Income
								filed for AY 2015-16 and onwards within a time period of 4 years
								or 6 years as prescribed u/s 153.
							</p>
							<p>
								Every taxpayer has to furnish the details of his income to the
								Income-tax Department. These details are to be furnished by
								filing up his return of income. Once the return of income is
								filed up by the taxpayer, the next step is the processing of the
								return of income by the Income Tax Department. The Income Tax
								Department examines the return of income for its correctness.
								The process of examining the return of income by the Income Tax
								department is called as &ldquo;<strong>Assessment</strong>&rdquo;.
								Assessment also includes <strong>re-assessment</strong> and <strong>best
									judgment assessment </strong>under section 144.
							</p>
							<p>Under the Income-tax Law, there are four major assessments
								given below:</p>
							<ul>
								<li>Assessment under section 143(1), i.e., Summary assessment
									without calling the assessee.</li>
								<li>Assessment under section 143(3), i.e., Scrutiny assessment.</li>
								<li>Assessment under section 144, i.e., Best judgment
									assessment.</li>
								<li>Assessment under section 147, i.e., Income escaping
									assessment.</li>
							</ul>
							<p>All the above mentioned notices are issued which leads to the
								given assessment proceedings.</p>
						</div>

						<button class="accordion">How to reply for Notice u/s 139 (9) ?</button>
						<div class="panel">
							<p>
								Notice u/s 139(9) is basically intimation from the Income Tax
								Department for the Income tax return filed for a financial year
								being <strong>defective </strong>due to following common
								reasons:
							</p>
							<p>The assessee having filed in the return the details of taxes
								paid, but failed to provide income details, the Income Tax
								Department deems the return as defective.</p>
							<p>The assessee having claimed the tax deducted at source (TDS)
								as refund, failed to provide income details in the return. In
								such cases the return will be deemed to be defective.</p>
							<p>The assessee having liability to pay taxes fails to pay the
								taxes in full before filing the return.</p>
							<p>The Assessee who is required to maintain balance sheet and
								profit and loss statement as per the provisions of Income tax
								Act, 1961, but fails to provide such statements while preparing
								the income tax return.</p>
							<p>When there is a mismatch of name in the return filed with the
								name as per PAN card.</p>
							<p>
								In order to clear the defects in the original return filed i.e.
								in response to the notice sent by the department u/s 139(9), the
								assessee is required to file response within <strong>15 days</strong>
								of the receipt of intimation or within such other time as
								extended by the AO on a written request filed for the same. For
								filing the response one should mention in the return the section
								under which the return is being filed and the communication
								reference number which is already there on the intimation given
								by the department.
							</p>
						</div>

						<button class="accordion">When can an assessee receive notice for
							scrutiny assessment u/s 147?</button>
						<div class="panel">
							<p>
								Notice for scrutiny assessment u/s 147 is issued u/s 148 if the
								Assessing Officer(AO) has reason to believe that any <u>income
									has escaped assessment which should be chargeable to tax </u>.
								The main objective of
							</p>
							<p>How the scrutiny assessment can be made by the assessing
								officer (AO)?</p>
							<p>
								For making the scrutiny assessment the AO has to issue a notice
								to the assessee (i.e., the tax payer) u/s 148 and assessee
								should be given an opportunity of being heard. The time-limit
								for issuance this notice u/s 148 is a period of <u>4 years</u>
								from the end of the relevant assessment year. If the income
								escaped by the assessee is Rs. 1,00,000 or more and certain
								other conditions are satisfied, then notice can be issued upto <u>6
									years</u> from the end of the relevant assessment year.
							</p>
							<p>
								In case the income which is escaped relates to any asset located
								outside India, notice can be issued upto <u>16 years</u> from
								the end of the relevant assessment year.
							</p>
							<p>Notice under section 148 can be issued by AO only after
								getting prior approval from the prescribed authority.</p>
							<p>The objective of carrying out assessment under section 147 is
								to bring under the tax net any income which has escaped
								assessment in original assessment under sections 143(1), 143(3),
								144 &amp; 147 (as the case may be).</p>
							<p>In the following cases, it will be considered as income having
								escaped assessment:</p>
							<ul>
								<li>Where no return of income has been furnished by the
									taxpayer, although his total income or the total income of any
									other person in respect of which he is assessable during the
									previous year exceeded the maximum amount which is not
									chargeable to income-tax.</li>
								<li>Where a return of income has been furnished by the taxpayer
									but no assessment has been made and it is noticed by the
									Assessing Officer that the taxpayer has understated the income
									or has claimed excessive loss, deduction, allowance or relief
									in the return.</li>
								<li>Where the taxpayer has failed to furnish a report in respect
									of any international transaction which he was required to do
									under section 92E.</li>
								<li>Where an assessment has been made, but:
									<ol>
										<li>income chargeable to tax has been under assessed; or</li>
										<li>income has been assessed at low rate; or</li>
									</ol>
								</li>
							</ul>
							<ul>
								<li>income has been made the subject of excessive relief; or</li>
							</ul>
							<ol>
								<li>excessive loss or depreciation allowance or any other
									allowance has been computed;</li>
							</ol>
							<ul>
								<li>Where a person is found to have any asset (including
									financial interest in any entity) located outside India.</li>
								<li>Where a return of income has not been furnished by the
									assessee and on the basis of information or document received
									from the prescribed income-tax authority under section 133C(2),
									it is noticed by the Assessing Officer that the income of the
									assessee exceeds the maximum amount not chargeable to tax.</li>
								<li>Where a return of income has been furnished by the assessee
									and on the basis of information or document received from the
									prescribed income-tax authority under section 133C(2), it is
									noticed by the Assessing Officer that the assessee has
									understated the income or has claimed excessive loss,
									deduction, allowance or relief in the return.</li>
							</ul>
						</div>


						<div style="height: 60px"></div>

						<!-- 		<button class="accordion">text</button> -->
						<!-- 		<div class="panel"> -->
						<!-- 			<p>text</p> -->
						<!-- 		</div> -->

					</div>
				</div>
			</div>
		</div>

</section>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}
    
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
    if($(window).scrollTop() > $(".banner_will").height()+50){
        //begin to scroll
        $(".sidebar1").css("position","fixed");
        $(".sidebar1").css("top",0);
    }
    else{
        //lock it back into place
        $(".sidebar1").css("position","relative");
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>
