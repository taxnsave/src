<div id="navbar" class="navbar h-navbar navbar-default ace-save-state navbar-fixed-top">
    <div class="navbar-container ace-save-state" id="navbar-container">
       <div class="col-lg-2 col-md-2"> <div class="space-8"></div>
          <div class="logo-area">
            <a href="<?php echo $CONFIG->siteurl;?>"><img src="<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>assets/images/logo.png" alt="" style="height:30px;"/>                          
            </a>
          </div>
        </div>
    <div class="navbar-buttons navbar-header pull-right" role="navigation" style="margin-top:0px;">
    	<div class="space-8"></div>
        <ul class="nav ace-nav">
            <li class="green dropdown-modal">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">                    
					<strong><?php if($_SESSION[$CONFIG->sessionPrefix.'_AY_TEXT'] == '') echo "Please Select - A.Y."; else echo "A.Y. ".$_SESSION[$CONFIG->sessionPrefix.'_AY_TEXT'];?></strong>
                </a>
            </li>
			<li class="light-blue dropdown-modal">
				<a data-toggle="dropdown" href="#" class="dropdown-toggle">
					<img class="nav-user-photo" id="profile_header_img" src="<?php echo $loggedUserImage;?>" alt="<?php echo $CONFIG->loggedUserName;?>" />
					<span class="user-info">
						<small>Welcome,</small>
						<?php echo $CONFIG->loggedUserName;?>
					</span>

				<i class="ace-icon fa fa-caret-down"></i>	
				</a>

				<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
					<li>
						<a href="?module_interface=<?php echo $commonFunction->setPage('profile');?>#settings">
							<i class="ace-icon fa fa-cog"></i>
							Settings
						</a>
					</li>

					<li>
						<a href="?module_interface=<?php echo $commonFunction->setPage('profile');?>">
							<i class="ace-icon fa fa-user"></i>
							Profile
						</a>
					</li>

					<li class="divider"></li>

					<li>
						<a href="<?php echo $CONFIG->siteurl ; ?>logout.php">
							<i class="ace-icon fa fa-power-off"></i>
							Logout
						</a>
					</li>
				</ul>
			</li>
                </ul>
            </div>
        </div>
    </div>