<style>
    #payment-type {background-color: dimgrey;}
    h1   {color: black;}
    p    {color: whitesmoke;}
    label {color: whitesmoke;}
    checkbox {color: whitesmoke;}
    
    .payment-sec {margin-top:20px;}
    .form-check-label {font-size: 16px;font-weight: 200;	color:#333333;	line-height:40px;	padding:2px 10px;	font-family: 'Montserrat', sans-serif; background-color: #cccccc; width: auto; border-radius: 5px;}
    .payment-sec .form-check-label .small {font-size: 11px; color:#ff0000;	line-height:3px;font-family: 'Montserrat', sans-serif; background-color: #cccccc;}
    .form-check-input {background-color: #ccc;
  border: 5px solid #000;
  border-radius: 50%;
  cursor: pointer;
  height: 20px;  
  width: 20px; 
        background-image: url(<?php echo $CONFIG->staticURL;?><?php echo $CONFIG->theme; ?>/img/chk_base.png);}
    .payment-step hr{border-color: #666;}
    .fee{background-color: #cccccc; text-align: right; line-height: 40px; width: auto; font-size: 16px;font-weight: 200;color:#333333;	padding:2px 10px;	font-family: 'Montserrat', sans-serif;}
    .payment-sec input[type=text]{background-color: #cccccc; text-align: right; line-height: 35px; width: auto; font-size: 16px;font-weight: 200;color:#333333;	padding:2px 10px;	font-family: 'Montserrat', sans-serif;}
    .btn-submit  {font-family: 'Montserrat', sans-serif;font-size:18px;border-radius:5px;margin-bottom:30px;width:310px;background-color:#003366;color: #fff; height: 64px;}
    .note {font-family: 'Montserrat', sans-serif;color:#000000;font-size:12px;font-weight:400;text-align: center;}
    
    .payment-title{
    color: #000000;
    font-size: 36px;
    font-weight: 700;
	font-family: 'Montserrat', sans-serif;
	text-transform:uppercase;
}
    
    .payment-text {
	font-size: 18px;
    font-weight: 400;
	color:#333333;
	line-height:26px;
	padding:2px 10px;
	font-family: 'Montserrat', sans-serif;
}
    table {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
        }

        th {
            font-size: 12px;
            background: #54b254;
            color: #FFFFFF;
            font-weight: bold;
            height: 30px;
        }

        td {
            font-size: 12px;
            background: #dff3e0
        }

        .error {
            color: #FF0000;
            font-weight: bold;
        }
</style>

<?php 
if(isset($_POST)){
	$actual_link = $_POST['return_page'];
    $product_desc = $_POST['proddesc'];
}


?>

<div id="paymentpage">
<section class="payment-step" id="payment-type">
	<div class="container">
       <div class="row">
       		<div class="col-md-12">
            	<div class="head-title-news">
                	<h1 class="payment-title">Payment Option</h1>
                    <hr/>
					<p class="payment-text">Select your income for AY 2018-19 (FY 2017-18)</p>
                </div>
            </div>

    <div class="payment-sec">
		<form class="paymentForm" role="form" method="post"
									action="./pay/paymentdetails.php"
                                    onSubmit="changetopaymentdetails(this);return false;"
                                    name="payment_frm"
									accept-charset="UTF-8" id="pay-select">
		 <div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<input class="form-check-input" type="checkbox" id="singleform16" value="SingleForm-16">
  <label class="form-check-label" for="singleform16">Salary Income - Single Form-16</label>
			</div>
			
			<div class="col-xs-12 col-sm-4 col-md-4">
				<input class="form-check-input" type="checkbox" id="multipleform16" value="MultipleForm -16">
  <label class="form-check-label" for="multipleform16">Salary Income - Multiple Form -16</label>
			</div>
	   
	   		<div class="col-xs-12 col-sm-4 col-md-4">
				<input class="form-check-input" type="checkbox" id="interest" value="Interestincome">
  <label class="form-check-label" for="interest">Interest income</label>
			</div>
			 
				 <div class="col-md-12"><hr/>
				 <input class="form-check-input" type="checkbox" id="oneproperty" value="OneProperty">
  <label class="form-check-label" for="oneproperty">Income form House Property - One Property</label><hr/>
				 </div>
			   <div class="col-md-12">
				 <input class="form-check-input" type="checkbox" id="morethanoneproperty" value="MorethanoneProperty">
  <label class="form-check-label" for="morethanoneproperty">Income form House Property - More than one Property</label><hr/>
				 </div>
			 <div class="col-md-12">
				 <input class="form-check-input" type="checkbox" id="capitalgain" value="CapitalGains">
  <label class="form-check-label" for="capitalgain">Capital Gains - 1 to 5 transactions<p class="small"> If more than 5 transactions, please consult a CA</p></label><hr/>
				 </div>
			 <div class="col-md-12">
				 <input class="form-check-input" type="checkbox" id="incomefrombusiness" value="IncomefromBusiness">
  <label class="form-check-label" for="incomefrombusiness">Income form  Business - Turnover or Gross receipts less than Rs. 2 Crores or Income from Profession - Gross receipts less than 25 Lakhs<p class="small"> For any other cases, please consult a CA</p></label><hr/>
				 </div>
			 <div class="col-md-12">
				 <input class="form-check-input" type="checkbox" id="anyotherincome" value="Anyotherincome">
  <label class="form-check-label" for="anyotherincome">Any other income</label><hr/>
				 </div>
	   <div class="col-xs-12 col-sm-10 col-md-10">
				 <p class="fee">Your applicable fee Rs.</p>
				 </div>
			 <div class="col-xs-12 col-sm-2 col-md-2">
				 <input type="text" id="amount" name="amount" value="0"/>
				 </div>
			 <div class="col-md-12">
				 <center><button type="submit" class="btn-submit">Proceed to payment</button></center><hr/>
				 </div>
		</div>  
        
            <?php
                if($actual_link == "") {
                $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                }
            
            if($product_desc == "") {$product_desc = "Product info missing";}
            ?>
            <input type="hidden" name="return_page"    value="<?php echo $actual_link; ?>" /> 
            <input type="hidden" name="description"    value="<?php echo $product_desc; ?>" /> 
        </form>
		   <div class="row">
			    <div class="col-md-12">
				 <p class="note">Note - If there is any differential payment due to wrong selection of income or due to any other reason like complication in the case etc, the differential payment needs to be paid.  </p>
				 </div>
		   </div>
	   </div><!-- manage wealth-->
    </div>
    
    </div>
</section>
</div>	

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">

    function calculate_total() {
        var totalcost = 0;
        if(document.getElementById("singleform16").checked) {
            totalcost += 9;
        }
        if(document.getElementById("multipleform16").checked) {
            totalcost += 9;
        }
        if(document.getElementById("interest").checked) {
            totalcost += 9;
        }
        if(document.getElementById("oneproperty").checked) {
            totalcost += 9;
        }
        if(document.getElementById("morethanoneproperty").checked) {
            totalcost += 9;
        }
        if(document.getElementById("capitalgain").checked) {
            totalcost += 9;
        }
        if(document.getElementById("incomefrombusiness").checked) {
            totalcost += 9;
        }
        if(document.getElementById("anyotherincome").checked) {
            totalcost += 9;
        }
        
        document.getElementById("amount").value = totalcost;     
    }
    
    
    $(document).ready(function(){
    
            $("#singleform16").click(function(){
            calculate_total();
    });
            $("#multipleform16").click(function(){
            calculate_total();
    });
            $("#interest").click(function(){
            calculate_total();
    });
            $("#oneproperty").click(function(){
            calculate_total();
    });
            $("#morethanoneproperty").click(function(){
            calculate_total();
    });
            $("#capitalgain").click(function(){
            calculate_total();
    });
            $("#incomefrombusiness").click(function(){
            calculate_total();
    });
            $("#anyotherincome").click(function(){
            calculate_total();
    });
});
    
    function changetopaymentdetails(form)     {
        calculate_total();
        if(document.getElementById("amount").value == "0") {
            alert("Please select any one option");
            return false;
        }
        
        //alert("prep");
        $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize(),
        success: function(response) {
            //alert("resp");
            $("#paymentpage").html(response);
        }            
    });
    return false;
    }
</script>
  
	
