	<!-- #section:basics/sidebar -->
			<div id="sidebar" class="sidebar h-sidebar no-gap lower-highlight responsive-min menu-min ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>
				
				<ul class="nav nav-list">
					<li class="<?php if($CONFIG->pageName == "home") echo 'active'; ?>" title="Dashboard">
						<a href="?module_interface=<?php echo $commonFunction->setPage('home');?>">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>
						<b class="arrow"></b>
					</li>										                   					              
					<li class="<?php if($CONFIG->pageName == "profile") echo 'active'; ?>" title="Profile">
						<a href="?module_interface=<?php echo $commonFunction->setPage('profile');?>">
							<i class="menu-icon fa fa-user"></i>
							<span class="menu-text white"> Profile </span>
						</a>
						<b class="arrow"></b>
					</li>	
                    <li class="<?php if($CONFIG->pageName == "mf_dashboard" || $CONFIG->pageName == "folio_summary" ||
										$CONFIG->pageName == "ac_stmt" || $CONFIG->pageName == "holdings"
										|| $CONFIG->pageName == "returns" || $CONFIG->pageName == "map_users"
										 || $CONFIG->pageName == "mf_buy_sell" || $CONFIG->pageName == "order_manager") echo 'active'; ?>" title="MF">
						<a class="dropdown-toggle" href="#">
							<i class="menu-icon fa fa-flask" aria-hidden="true"></i>                   
							<span class="menu-text white"> Mutual Fund </span>
                            <b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>
                        <ul class="submenu">
							<li class="<?php if($CONFIG->pageName == "folio_summary") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('folio_summary');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Portfolio Summary
								</a>
								<b class="arrow"></b>
							</li>	
                            <li class="<?php if($CONFIG->pageName == "ac_stmt") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('ac_stmt');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									A/c Statement
								</a>
								<b class="arrow"></b>
							</li>	
                            <li class="<?php if($CONFIG->pageName == "folio_query") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('folio_query');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Folio Query
								</a>
								<b class="arrow"></b>
							</li>	
                    	   <li class="<?php if($CONFIG->pageName == "mf_offer_buy") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('mf_offer_buy');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Recomended Plans
								</a>
								<b class="arrow"></b>
							</li>
                            <li class="<?php if($CONFIG->pageName == "mf_buy_sell") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('mf_buy_sell');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Buy/Sell
								</a>
								<b class="arrow"></b>
							</li>
                            <li class="<?php if($CONFIG->pageName == "order_manager") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('order_manager');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Order Manager
								</a>
								<b class="arrow"></b>
							</li>
                            <li class="<?php if($CONFIG->pageName == "map_users") echo 'active'; ?>" >
								<a href="?module_interface=<?php echo $commonFunction->setPage('map_users');?>">
									<i class="menu-icon fa fa-caret-right"></i>
									Map Other User
								</a>
								<b class="arrow"></b>
							</li>							
						</ul>	
					</li>	
                    <li class="<?php if($CONFIG->pageName == "forms_list" || $CONFIG->pageName == "itr_forms" || $CONFIG->pageName == "fill_itr") echo 'active'; ?>" title="Forms ITR">
						<a href="?module_interface=<?php echo $commonFunction->setPage('fill_itr');?>">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text white"> Forms/ITR </span>
						</a>
						<b class="arrow"></b>
					</li>	
                    <li class="<?php if($CONFIG->pageName == "create_will") echo 'active'; ?>" title="Will">
						<a href="?module_interface=<?php echo $commonFunction->setPage('create_will');?>">
							<i class="menu-icon fa fa-gift"></i>
							<span class="menu-text white"> Will </span>
						</a>
						<b class="arrow"></b>
					</li>   
					<li class="<?php if($CONFIG->pageName == "payment") echo 'active'; ?>" title="Payment">
						<a href="?module_interface=<?php echo $commonFunction->setPage('payment');?>">
							<i class="menu-icon fa fa-credit-card"></i>
							<span class="menu-text white"> Payment </span>
						</a>
						<b class="arrow"></b>
					</li>
                     <li class="<?php if($CONFIG->pageName == "payment") echo 'active'; ?>" title="Payment">
                         <a href="?module_interface=<?php echo $commonFunction->setPage('payment');?>">
                             <i class="menu-icon fa fa-credit-card"></i>
                             <span class="menu-text white"> Payment </span>
                         </a>
                         <b class="arrow"></b>
                     </li>   
					<li class="<?php if($CONFIG->pageName == "account_log") echo 'active'; ?>" title="Account Logs">
						<a href="?module_interface=<?php echo $commonFunction->setPage('account_log');?>">
							<i class="menu-icon fa fa-industry"></i>
							<span class="menu-text white"> Account Log </span>
						</a>
						<b class="arrow"></b>
					</li>                                    										
				</ul><!-- /.nav-list -->
				<!-- #section:basics/sidebar.layout.minimize -->
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-save-state ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
				<!-- /section:basics/sidebar.layout.minimize -->
			</div>
			<!-- /section:basics/sidebar -->