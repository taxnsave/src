<?php
	//print_r($_REQUEST);
?>
<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo $CONFIG->siteurl;?>mySaveTax/">Home</a>
							</li>
							<li class="active">Dashboard</li>
						</ul><!-- /.breadcrumb -->
						<?php //include("mdocs.lib.htmlPages/form.search.php");?>

						<!-- /section:basics/content.searchbox -->
					</div>

					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">						
						<div class="row">
							<div class="col-xs-12">
              <?php 
			  		if($_SESSION['msg_strip'] != '')
					{
				?>              
                        <div class="alert alert-danger">
                            <button data-dismiss="alert" class="close" type="button">
                                <i class="ace-icon fa fa-times"></i>
                            </button>                        
                            <strong>
                                <i class="ace-icon fa fa-times"></i>
                                Oh!
                            </strong>                        
                           <?php echo $_SESSION['msg_strip']; ?>
                            <br>
                        </div>
             <?php
			 		$_SESSION['msg_strip'] = '';
			 	}
			?>                   
								<!-- PAGE CONTENT BEGINS -->	<?php /* ?>							
								<div class="row">
									<div class="space-6"></div>
									<div class="col-sm-12 infobox-container">
										<div class="widget-box">
											<div class="widget-header widget-header-flat widget-header-small">
												<h5 class="widget-title pull-left orange">
													<i class="ace-icon fa fa-search green"></i>
													Search
												</h5>												
											</div>
											<div class="widget-body">
												<div class="widget-main">
													<!-- #section:plugins/charts.flotchart -->
                                                   <!-- <form class="form-search" method="get" action="?module_interface=<?php echo $commonFunction->setPage('mf_buy_sell');?>" id="searchForm" name="searchForm">
                                                   <input type="hidden" name="module_interface" id="module_interface" value="<?php echo $commonFunction->setPage('mf_buy_sell');?>" />
													<div class="input-group input-group-lg">
                                                        <span class="input-group-addon">
                                                            <i class="ace-icon fa fa-check"></i>
                                                        </span>
														
															<input type="text" placeholder="Type your query" class="form-control search-query" id="search_input" name="search_input"> 
																	<span class="input-group-btn">
																		<button class="btn btn-success btn-lg" type="submit">
																			<span class="ace-icon fa fa-search icon-on-right bigger-110"></span>
																			Search
																		</button>
																	</span>
																</div>		<div id="email-error" class="help-block"></div>										
												</form>--></div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
									</div> 
									<div class="vspace-12-sm"></div>
									<div class="col-sm-5 hide">
										<div class="widget-box">
											<div class="widget-header widget-header-flat widget-header-small">
												<h5 class="widget-title orange">
													<i class="ace-icon fa fa-signal green"></i>
													Usages Details
												</h5>												
											</div>
											<div class="widget-body">
												<div class="widget-main">
													<!-- #section:plugins/charts.flotchart -->
													<div id="piechart-placeholder"></div>													
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
									</div><!-- /.col -->
								</div><!-- /.row -->

								<!-- #section:custom/extra.hr -->
								<div class="hr hr32 hr-dotted"></div><?php */ ?>

								<!-- /section:custom/extra.hr -->
								<div class="row">
									<div class="col-sm-12">
										<div class="widget-box transparent">
											<div class="widget-header widget-header-flat">
												<h4 class="widget-title orange">
													<i class="ace-icon fa fa-file-o green"></i>
													Latest Uploaded Forms
												</h4>

												<div class="widget-toolbar">
													<a href="#" data-action="collapse">
														<i class="ace-icon fa fa-chevron-up"></i>
													</a>
												</div>
											</div>

											<div class="widget-body">
												<div class="widget-main no-padding">
													<table class="table table-bordered table-striped">
														<thead class="thin-border-bottom">
															<tr>		
																<th>
																	<i class="ace-icon fa fa-caret-right blue"></i>PAN Number
																</th>															
																<th>
																	<i class="ace-icon fa fa-caret-right blue"></i>Assessment Year
																</th>
																<th class="hidden-480">
																	<i class="ace-icon fa fa-caret-right blue"></i>ITR Status
																</th>
                                                                <th class="hidden-480">
																	<i class="ace-icon fa fa-caret-right blue"></i>Action
																</th>                                                                
																<th class="hidden-480">
																	<i class="ace-icon fa fa-caret-right blue"></i>Upload Date
																</th>
															</tr>
														</thead>
                                                        <tbody>
<?php
	$totalFiles = $itrFill->getAllEfillingCount();
	$fileList = $itrFill->getAllEfilling();
	
	if($totalFiles == 0)
		echo $fileHTML = '<tr><td class="center red" colspan="5"> No File(s) Found.</td></tr>';
	else
	{
		while(list($fileKey,$fileVal) = each($fileList))
		{	
			$arr = explode("-",$fileVal[asses_year]);		
?>														
    <tr>
        <td>
            <b class="green"><?php echo $fileVal['itr_pd_pan_number'];?></b>
        </td>	
        <td>
            <b class="green"><?php echo $fileVal[asses_year];?></b>
        </td>
         <td>
            <b class="green"><a href="?ay=<?php echo $arr[0];?>&formsDataID=<?php echo $fileVal[pk_itr_id];?>&module_interface=<?php echo $commonFunction->setPage('middle_layer_redirection');?>">Pending &nbsp;(Click here to continue)</a></b>
        </td>
         <td><span class="label label-warning">
            <a class="white" href="?action=remove&itr_id=<?php echo $fileVal[pk_itr_id];?>&user_id=<?php echo $fileVal[fr_user_id];?>&ay=<?php echo $arr[0];?>&module_interface=<?php echo $commonFunction->setPage('middle_layer_redirection');?>">Reset Data & Fresh Start</a></span>
        </td>
        <td class="hidden-480">
            <span class="label label-info arrowed-right arrowed-in"><?php echo $commonFunction->dateFormatWithTime($fileVal[form_created_date ]); ?></span>
        </td>        
    </tr>
<?php
		}
	}
?>

														</tbody>
													</table>
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
									</div><!-- /.col -->

									<!-- /.col -->
								</div><!-- /.row -->

								<div class="hr hr32 hr-dotted"></div>

								<!-- /.row -->

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div>