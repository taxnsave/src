<form action="data_submit/frmForm.php" name="frmBankAccount" id="frmBankAccount" method="post" class="frmCurrent has-validation-callback">
<div class="agreement-row">
  <input type="hidden" name="form_id" value="5.1">
            <div class="container">

                <div id="mutual_fund">
                        <div class="row">
                            <input name="stocksbondsdetails_process" value="1" type="hidden">
                            <input name="investment_type" id="investment_type" value="stock" type="hidden">
                            <input name="sbm_id" id="sbm_id" value="0" type="hidden">
                            <input name="joint_account_count" id="joint_account_count" value="1" type="hidden">
                            <input name="total_count" id="total_count" value="1" type="hidden">
                            <input id="security_type_hidden" value="physical" type="hidden">
                            <input name="auth_token" id="auth_token_name" value="TVRBekxqVTFMakUyT0M0eE5UWnRiek15WWpkbmNHODJNamhsWkdZeWFtcG1hbWMxWlhZeE13PT0=" type="hidden">
                            <input name="btn_clicked" id="btn_clicked" value="next" type="hidden">
                            <div class="col-xs-12">
                                <div id="bnkacAddWrap">
                                    <div><!-- do not delte this div -->
                                        <h3>Add  Securities<span>&nbsp;</span></h3>
                                        <h4>
                                            <strong class="mandatory_label">Do you hold Securities in Physical or Dematerialised Form? &nbsp;<span class="mandatory_star">*</span></strong>
                                            <span>
                                                <input value="physical" name="securities_format" checked="" title="Select if you hold securities in physical form" alt="Select if you hold securities in physical form" type="radio"> Physical
                                                <input value="demat" name="securities_format" title="Select if you hold securities in Demat form" alt="Select if you hold securities in Demat form" type="radio"> Demat
                                                <!-- <input type="radio" value="both" name="securities_format"  title="Select if you hold Securities in Physical or Dematerialised Form as Both" alt="Select if you hold Securities in Physical or Dematerialised Form as Both"> Both -->
                                            </span>
                                        </h4>
                                        <div class="panel panel-body sub-panel" id="main_demart_physical_div">
                                            <div id="physical_form" class="hide"></div>

                                            <div class="divider hide" id="main_demart_physical_div_divider">&nbsp;</div>

                                            <div id="demat_form" class="">            <div class="row">
				 <div class="col-lg-5">
                    <label class="mandatory">Name of the Depository</label><br>
                    <select class="input-select form-control" id="name_of_depository" name="name_of_depository" data-validation="" data-validation-length="1-16" data-validation-error-msg="Please select valid Name of the Depository" title="Please Select the Name of the depository" alt="Please Select the Name of the depository">
                        <option value="">Please Select</option>
                        <option value="NSDL">National Securities Depository Limited</option>
                        <option value="CDSL">Central Depository Services (India) Limited</option>
                    </select>
                </div>
                <div class="col-lg-4">
                    <label class="mandatory mandatory_label">Name of the Depository Participant(DP) <span class="mandatory_star">*</span></label><br>
                    <input placeholder="Name of the DP" class="form-control" name="participants_name" value="" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Name of the Depository Participant(DP)" title="Please enter Name of the Depository Participant(DP)" alt="Please enter Name of the Depository Participant(DP)" type="text">
                </div>

                <div class="col-lg-3">
                    <label class="mandatory ">DP ID and Client ID / BO ID <!-- <span class="mandatory_star">*</span> --></label><br>
                    <input placeholder="ID" class="form-control" name="dp_id" value="" maxlength="16" data-validation="" data-validation-length="1-16" data-validation-error-msg="Please enter valid DP ID and Client ID / BO ID" title="If NSDL, please enter DP ID followed by Client ID. IF CDSL, please enter BO ID." alt="If NSDL, please enter DP ID followed by Client ID. IF CDSL, please enter BO ID." type="text">
                </div>

            </div>
</div>
                                        </div><!-- panel-body END -->

                                        <h4>
                                            <strong class="mandatory_label">Ownership Type? &nbsp;<span class="mandatory_star">*</span></strong>
                                            <span>
                                                                                                <input class="sel_ownership_type" name="ownership_type" value="single" title="Select if Ownership Type is Single" alt="Select if Ownership Type is Single" type="radio"> Single
                                                <input class="sel_ownership_type" name="ownership_type" value="joint" checked="" title="Select if Ownership Type is Joint" alt="Select if Ownership Type is Joint" type="radio"> Joint
                                            </span>
                                        </h4>

                                        <div id="div_joint_account_holders">
                                                            <div id="account_1">
                                        <div id="jointholderAddWrap"><!-- *** If value 'Joint' display this div *** -->
                                            <input name="ejd_hld_id[]" value="0" type="hidden">
                                            <div><!-- Keep this div as it required for copying -->
                                            <h4>
                                                <div class="jah_account_numbering"><strong>Joint Account Holder #1 Details</strong></div>
                                                <span id="span_account_1">
                                                    <a href="javascript:void(0);" class="btn btn-primary btn-sm btn-add-large a_add_account" title="Click to Add Joint Account Holder" alt="Click to Add Joint Account Holder">+ Add Joint Account Holder</a>                                                </span>
                                            </h4>
                                            <div class="panel panel-body sub-panel">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <label class="mandatory mandatory_label">Details of the Joint Holder(s) <span class="mandatory_star">*</span></label>
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <select name="ejd_title[]" id="ejd_title_1" class="input-select form-control" data-validation="length" data-validation-length="1-2" data-validation-error-msg="Please select a valid Title for joint account holder" title="Please select a Title for joint account holder" alt="Please select a Title for joint account holder "><option value="">Title</option><option value="1">Mr.</option><option value="2">Mrs.</option><option value="3">Ms.</option><option value="10">Master</option><option value="23">Kumar</option><option value="24">Kumari</option></select>                                                            </div>
                                                            <div class="col-xs-3">
                                                                <input class="form-control" name="ejd_lastname[]" id="ejd_lastname_1" value="" placeholder="Last Name / Surname" maxlength="33" title="Please enter Last Name / Surname for joint account holder " alt="Please enter Last Name / Surname for joint account holder " data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please enter Last Name / Surname for joint account holder " type="text">
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <input class="form-control" name="ejd_firstname[]" id="ejd_firstname_1" value="" placeholder="First Name" maxlength="33" title="Please enter First Name for joint account holder " alt="Please enter First Name for joint account holder " type="text">
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <input class="form-control" name="ejd_middlename[]" id="ejd_middlename_1" value="" placeholder="Middle Name" maxlength="33" title="Please enter Middle Name for joint account holder " alt="Please enter Middle Name for joint account holder " type="text">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="divider"></div>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            &nbsp;&nbsp;&nbsp;<input class="same_per_cor_address" name="is_same_as_permanent[]" value="P" id="is_same_as_permanent_1" account_count="1" for_address="permanent" title="Select if is this Address same as your Permanent Address?" alt="Select if is this Address same as your Permanent Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
                                                            <label class="mandatory">Is Address same as your Permanent Address?</label>

                                                        </div>
                                                        <div class="col-xs-6">
                                                            &nbsp;&nbsp;&nbsp;<input class="same_per_cor_address" name="is_same_as_correspondence[]" value="C" id="is_same_as_correspondence_1" account_count="1" for_address="permanent" title="Select if is this Address same as your Correspondence Address?" alt="Select if is this Address same as your Correspondence Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
                                                            <label class="mandatory">Is Address same as your Correspondence Address?</label>
                                                        </div>
                                                     </div>
                                                      <div class="row" style="display:none;">
                                                        <div class="col-xs-6">
                                                            &nbsp;&nbsp;&nbsp;<input name="is_same_as_permanent_or_correspondence[]" id="is_same_as_permanent_or_correspondence_1" value="" type="hidden">
                                                        </div>
                                                     </div>
													                                                     <div class="divider"></div>
                                                    <div id="populate_permanent_address_1">
                                                        <div class="row">
                                                            <div class="col-xs-8">
                                                                &nbsp;&nbsp;&nbsp;<input name="ejd_is_foreign_address[]" id="ejd_is_foreign_address_1" value="1" title="Please select if its a foreign address" alt="Please select if its a foreign address" class="is_foreign_address" changezip="ejd_zipcode_1" country="ejd_country_1" state="ejd_state_1" span_country="span_ejd_country_1" span_state="span_ejd_state_1" other_state="span_ejd_state_other_1" other_state_txt="ejd_state_other_1" type="checkbox">&nbsp;&nbsp;&nbsp;<label class="mandatory">Is this Address a Foreign Address? </label>
                                                            </div>
                                                        </div>
                                                        <div class="divider"></div>
                                                        <div class="row">
                                                            <div class="col-xs-4">
                                                                <label class="mandatory mandatory_label">Address Line1 <span class="mandatory_star">*</span></label>
                                                                <input class="form-control" name="ejd_address_line1[]" id="ejd_address_line1_1" value="" placeholder="Address Line1" maxlength="60" title="Please enter Address Line1 for joint account holder address" alt="Please enter Address Line1 for joint account holder address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1 for joint account holder address" type="text">
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <label class="mandatory mandatory_label">Address Line2 <span class="mandatory_star">*</span></label>
                                                                <input class="form-control" placeholder="Address Line2" name="ejd_address_line2[]" id="ejd_address_line2_1" value="" maxlength="60" title="Please enter Address Line2 for joint account holder address" alt="Please enter Address Line2 for joint account holder address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2 for joint account holder address" type="text">
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <label>Address Line3</label>
                                                                <input class="form-control" placeholder="Address Line3" name="ejd_address_line3[]" id="ejd_address_line3_1" value="" maxlength="60" title="Please enter Address Line3 for joint account holder address" alt="Please enter Address Line3 for joint account holder address" type="text">
                                                            </div>
                                                        </div>
                                                        <div class="divider"></div>
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <label class="mandatory_label">City / Village / Town <span class="mandatory_star">*</span></label>
                                                                <input class="form-control" placeholder="City / Village / Town" name="ejd_city_village_town[]" id="ejd_city_village_town_1" value="" maxlength="60" title="Please enter City / Village / Town for joint account holder address" alt="Please enter City / Village / Town for joint account holder address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town for joint account holder address" type="text">
                                                            </div>
                                                            <div class="col-xs-2">
                                                                <label class="mandatory mandatory_label">Pincode / Zipcode <span class="mandatory_star">*</span></label>
                                                                <input class="form-control numeric" placeholder="Pincode" name="ejd_zipcode[]" id="ejd_zipcode_1" value="" title="Please enter Pincode / Zipcode for joint account holder address" alt="Please enter Pincode / Zipcode for joint account holder address" maxlength="6" data-validation="length" data-validation-length="6-6" data-validation-error-msg="Please enter valid Pincode for joint account holder address" type="text">
                                                            </div>
                                                            <div class="col-xs-2">
                                                                <label class="mandatory mandatory_label">Country<span class="mandatory_star">*</span></label>
                                                                <span id="span_ejd_country_1">
                                                                <select name="ejd_country[]" id="ejd_country_1" class="input-select form-control select-country-list" title="Please select a Country for joint account holder address" alt="Please select a Country for joint account holder address" data-validation="length" data-validation-length="1-4" data-validation-error-msg="Please select valid Country" other_state_txt="ejd_state_other_1" onchange="javascript: populate_states(this.value,'ejd_state_1','span_ejd_state_1', '','toggle_other_state(ejd_state_1,ejd_state_other_1)'); toggle_other_state_country(this.value, 'span_ejd_state_other_1');;"><option value="102" selected="selected">India</option></select>                                                                </span>
                                                            </div>
                                                            <div class="col-xs-2">
                                                                <label class="mandatory mandatory_label">State <span class="mandatory_star">*</span></label>
                                                                <span id="span_ejd_state_1">
                                                                    <select name="ejd_state[]" id="ejd_state_1" class="input-select form-control" title="Please select a State for joint account holder address" alt="Please select a State for joint account holder address" onchange="javascript: toggle_other_state('ejd_state_1', 'ejd_state_other_1');"><option value="35" selected="selected">Andaman and Nicobar Islands</option><option value="28">Andhra Pradesh</option><option value="12">Arunachal Pradesh</option><option value="18">Assam</option><option value="10">Bihar</option><option value="4">Chandigarh</option><option value="22">Chhattisgarh</option><option value="26">Dadra and Nagar Haveli</option><option value="25">Daman and Diu</option><option value="7">Delhi</option><option value="30">Goa</option><option value="24">Gujarat</option><option value="6">Haryana</option><option value="2">Himachal Pradesh</option><option value="1">Jammu and Kashmir</option><option value="20">Jharkhand</option><option value="29">Karnataka</option><option value="32">Kerala</option><option value="31">Lakshadweep</option><option value="23">Madhya Pradesh</option><option value="27">Maharashtra</option><option value="14">Manipur</option><option value="17">Meghalaya</option><option value="15">Mizoram</option><option value="13">Nagaland</option><option value="21">Odisha</option><option value="34">Puducherry</option><option value="3">Punjab</option><option value="8">Rajasthan</option><option value="11">Sikkim</option><option value="33">Tamil Nadu</option><option value="36">Telangana</option><option value="16">Tripura</option><option value="5">Uttarakhand</option><option value="9">Uttar Pradesh</option><option value="19">West Bengal</option></select>                                                                </span>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <span id="span_ejd_state_other_1" style="display:none;" validatemsg="Please enter Other State for joint account holder">
                                                                    <label class="cnd-mandatory mandatory_label">Other State(If not listed) <span class="mandatory_star">*</span></label>
                                                                    <input name="ejd_state_other[]" id="ejd_state_other_1" class="form-control" value="" placeholder="Mention State Name" maxlength="60" type="text">
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="divider"></div>
                                                <div class="row">
                                                     <div class="col-xs-6">
                                                            <label class="mandatory">Telephone Number </label>
                                                            <div id="TelephoneAddWrap_1">
                                                                                                                                            <div class="row" id="phone_1_1">
                                                                                                                                                                <div class="col-xs-plus">
                                                                                    <span class="plus_sign">+</span>
                                                                                </div>
                                                                                <div class="col-xs-2"><input class="form-control numeric" name="ejd_phone_isd[]" id="ejd_phone_isd_1" value="91" placeholder="ISD" maxlength="3" title="Please enter Telephone ISD for Joint account holder" alt="Please enter Telephone ISD for Joint account holder" type="text"></div>
                                                                                <div class="col-xs-3"><input class="form-control numeric" name="ejd_phone_std[]" id="ejd_phone_std_1" value="" placeholder="STD" maxlength="5" title="Please enter Telephone STD for Joint account holder" alt="Please enter Telephone STD for Joint account holder" type="text"></div>
                                                                                <div class="col-xs-5 last"><input class="form-control numeric" name="ejd_phone_number[]" id="ejd_phone_number_1" value="" placeholder="Phone Number" maxlength="10" title="Please enter Telephone Number for Joint account holder" alt="Please enter Telephone Number for Joint account holder" type="text"></div>
                                                                                                                                                                        <a id="AddMoreFileBox" href="javascript:void(0);" rec_id="1" class=" btn-add a_phone" title="Click to Add Phone" alt="Click to Add Phone">+</a>
                                                                                                                                                            </div>
                                                                                                                                <input id="actual_phone_count_1" value="1" type="hidden">
                                                            </div>
                                                        </div>
                                                    <div class="col-xs-6">
                                                            <label class="mandatory">Office Number</label>
                                                            <div id="InputsWrapperOffice_1">
                                                                                                                                            <div class="row" id="office_1_1">
                                                                                                                                                                <div class="col-xs-plus">
                                                                                    <span class="plus_sign">+</span>
                                                                                </div>
                                                                                <div class="col-xs-2"><input class="form-control numeric" name="ejd_office_phone_isd[]" id="ejd_office_phone_isd_1" value="91" placeholder="ISD" maxlength="3" title="Please enter Office Phone ISD for Joint account holder" alt="Please enter Office Phone ISD for Joint account holder" type="text"></div>
                                                                                <div class="col-xs-3"><input class="form-control numeric" name="ejd_office_phone_std[]" id="ejd_office_phone_std_1" value="" placeholder="STD" maxlength="5" title="Please enter Office Phone STD for Joint account holder" alt="Please enter Office Phone STD for Joint account holder" type="text"></div>
                                                                                <div class="col-xs-5 last"><input class="form-control numeric" name="ejd_office_phone_number[]" id="ejd_office_phone_number_1" value="" placeholder="Office Number" maxlength="10" title="Please enter Office Phone Number for Joint account holder" alt="Please enter Office Phone Number for Joint account holder" type="text"></div>
                                                                                <a class=" btn-add a_office_phone" id="AddMoreFileBoxOffice" href="javascript:void(0);" rec_id="1" title="Click to Add Office Phone" alt="Click to Add Office Phone">+</a>
                                                                            </div>
                                                                                                                                            <input id="actual_office_count_1" value="1" type="hidden">
                                                            </div>
                                                        </div>
                                                </div>

                                                <div class="divider"></div>
                                                 <div class="row">
                                                    <div class="col-xs-6">
                                                        <label class="mandatory">Mobile Number </label>
                                                        <div id="MobileAddWrap_1">
                                                            																					<div class="row" id="mobile_1_1">
                                                                                                                                                                <div class="col-xs-plus">
                                                                                    <span class="plus_sign">+</span>
                                                                                </div>
                                                                                <div class="col-xs-2"><input class="form-control numeric" name="ejd_mobile_isd[]" id="ejd_mobile_isd_1" value="91" placeholder="ISD" maxlength="3" data-validation="" data-validation-length="2-3" data-validation-error-msg="Please enter valid Mobile ISD" title="Please enter Mobile ISD for Joint account holder" alt="Please enter Mobile ISD for Joint account holder" type="text"></div>
                                                                                <div class="col-xs-8 last"><input class="form-control numeric" name="ejd_mobile_number[]" id="ejd_mobile_number_1" value="" placeholder="Mobile" maxlength="10" data-validation="" data-validation-length="8-10" data-validation-error-msg="Please enter mobile no." title="Please enter Mobile Number for Joint account holder" alt="Please enter Mobile Number for Joint account holder" type="text"></div>
                                                                                                                                                                        <a class=" btn-add a_mobile" id="AddMoreFileBoxMobile" href="javascript:void(0);" rec_id="1" title="Click to Add Mobile" alt="Click to Add Mobile">+</a>
                                                                                                                                                            </div>
																	                                                            <input id="actual_mobile_count_1" value="1" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                            <label class="mandatory">Email</label>
                                                            <div id="JahEmailAddWrap_1">
                                                                																		 <div class="row" id="jah_email_1_1">
                                                                                                                                                                <div class="col-xs-8 last"><input class="form-control" name="ejd_email[]" id="ejd_email_1" value="" placeholder="Email" maxlength="254" data-validation="" data-validation-length="1-254" data-validation-error-msg="Please enter valid Email for Joint account holder" title="Please enter Email for Joint account holder" alt="Please enter Email for Joint account holder" type="text"></div>
                                                                                                                                                                        <a class=" btn-add a_jah_email" id="AddMoreFileBoxJahEmail" href="javascript:void(0);" rec_id="1" title="Click to Add Email" alt="Click to Add Email">+</a>
                                                                                                                                                            </div>
																		                                                                <input id="actual_email_count_1" value="1" type="hidden">
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="divider"></div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-4">
                                                                <label class="cnd-mandatory mandatory_label">Date of Birth <span class="mandatory_star">*</span></label>
                                                                <input id="show_ejd_birth_date_1" class="form-control hasDatepicker" value="" placeholder="Birth date" readonly="" title="Please select a Birth date" alt="Please select a Birth date" ac_id="1" type="text">
                                                                <input name="ejd_birth_date[]" id="ejd_birth_date_1" class="form-control" value="0000-00-00" placeholder="Birth date" type="hidden">
                                                            </div>
                                                            <div class="col-xs-1">
                                                                <br><label class="cnd-mandatory">OR</label>

                                                            </div>
                                                            <div class="col-xs-2">
                                                                <label class="cnd-mandatory mandatory_label">Adult / Minor <span class="mandatory_star">*</span></label>
                                                                <select class="input-select form-control jah_is_adult" id="ejd_is_adult_1" name="ejd_is_adult[]" title="Please select if you are Adult/Minor" alt="Please select if you are Adult/Minor" ac_id="1">
                                                                    <option value="1" selected="">Adult</option>
                                                                    <option value="0">Minor</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- panel-body END -->
                                        </div>

                                <div class="grdn-details" id="ejd_guardian_1">
                            </div><!-- grdn-details END -->
                                        <a remove_id="1" href="javascript:void(0);" id="jointholderAddBtn" class="btn btn-primary btn-sm btn-add-large-red jointholderRemove a_removeaccount hide">- Remove Joint Account Holder</a>
                                    </div>
                                </div><!-- JointHolder END -->
                                            </div>
                                    </div>

                                </div>
                            </div><!-- col-xs-12 END -->

							<div class="row">&nbsp;</div>
						 <div id="div_beneficiary">
							<div id="div_beneficiary_1">
								<h4>
									<strong>Beneficiaries</strong>
								</h4>
								<div class="panel panel-body sub-panel">
									<div class="row">
										<div class="col-xs-4">
											<label class="mandatory mandatory_label">Beneficiary <span class="mandatory_star">*</span></label>
											<select name="sel_beneficiary" id="sel_beneficiary" class="input-select form-control" title="Please select a Beneficiary" alt="Please select a Beneficiary"><option value="">Please select</option></select>										</div>
										<div class="col-xs-4">
																						<label class="mandatory mandatory_label">Percentage Share to be alloted <span class="mandatory_star">*</span></label>
											<input class="form-control numeric_share" name="txt_beneficiary_share" id="txt_beneficiary_share" placeholder="Percentage Share to be alloted" maxlength="5" title="Please enter Percentage Share to be alloted" alt="Please enter Percentage Share to be alloted" value="" type="text">
										</div>
										<div class="col-xs-4">
																						<label class="mandatory">Any Other Information</label>
											<input class="form-control" name="txt_other_info" id="txt_other_info" maxlength="200" placeholder="Any Other Information" title="Please enter Any Other Information" alt="Please enter Any Other Information" value="" type="text">
										</div>
									</div>
									<div><br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add Beneficiary" title="Add Beneficiary" id="beneficiaryAddBtnNew" class="btn btn-primary btn-sm btn-add-large a_beneficiarynew">+ Add Beneficiary</a></span></div>
									<br>
									<table class="table table-striped">
											  <thead>
												<tr>
												  <th>#</th>
												  <th>Name</th>
												  <th>Share</th>
												  <th>Other Information</th>
												  <th>Status</th>
												</tr>
											  </thead>
											  <tbody id="tb_con_ben"><tr id="no_beneficiary"><td colspan="5"></td></tr>
																										<input name="tot_ben" id="tot_ben" value="1" type="hidden">
													<input id="total_share" value="0" type="hidden">

											  </tbody>
											</table>


								</div>
							</div>
						</div>

                        </div><!-- row END -->

                            <div class="row">&nbsp;</div>
							<div class="bottom_buttons row">
								<div class="div_prev_button">
									<input name="previous" id="btn-previous" value="« Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="public-provident-fund" type="button">
								</div>
								<div class="div_center_buttons">
									<input name="add_sercurity" id="btn-add" value="Add / Save" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " type="submit">

									<input name="update" id="btn-update" value="Update" title="Click here to Update the changed data" alt="Click here to Update the changed data" class="btn-submit btn-submit-disabled" disabled="" type="submit">
								</div>
								<div class="div_next_button" style="margin-right: -30px; text-align: right;">
									<input name="next" id="btn-next" value="Next »" title="Click here to go to Next page" alt="Click here to go to Next page" class="btn-submit btn-move" next_page="mutual-funds-details" type="button">
								</div><br><br><br>
							</div>
                            <div class="row">
							<input name="txt_other_submit" id="txt_other_submit" value="0" type="hidden">
<input name="txt_redirect_page" id="txt_redirect_page" value="" type="hidden">
<input name="sumbit" value="Submit" class="btn-submit btn-submit-disabled" title="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" alt="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" disabled="" type="submit">                            </div>

                    </form>
                    <div class="row">&nbsp;</div>
                    <div id="physical_form_hidden" class="hide">
                        <div class="row">
                            <div class="col-xs-4">
                                <label class="mandatory mandatory_label">Name of the Company <span class="mandatory_star">*</span></label><br>
                                <input placeholder="Name of the Company" class="form-control" name="company_name" value="" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Name of the Company" title="Please enter Name of the Company" alt="Please enter Name of the Company" type="text">
                            </div>
                            <div class="col-xs-4">
                                <label class="mandatory">Folio Number <!-- <span class="mandatory_star">*</span> --></label><br>
                                <input placeholder="Folio Number" class="form-control " name="dmat_folio_number" value="" maxlength="20" title="Please enter Folio Number" alt="Please enter Folio Number" data-validation="" data-validation-length="1-20" data-validation-error-msg="Please enter valid Folio Number" type="text">
                            </div>
                            <!-- <div class="col-xs-4">
                                <label class="mandatory"><blink>Number of Shares <span class="mandatory_star">*</span></blink></label><br>
                                <input type="text" placeholder="Number of Shares" class="form-control numeric_dobule" maxlength="18" name="no_of_shares" value="0.0" title="Please enter Number of Shares" alt="Please enter Number of Shares" data-validation="length" data-validation-length="1-20" data-validation-error-msg="Please enter valid Number of Shares">
                            </div> -->
                        </div>
                        <div class="divider"></div>
                        <div class="row">
                            <div class="col-xs-12">
                                <input name="all_securities" id="all_securities" value="1" title="Please select if All Physical Shares held by the user is to be distributed as per the Beneficiary details provided below." alt="Please select if All Physical Shares held by the user is to be distributed as per the Beneficiary details provided below." type="checkbox">&nbsp;&nbsp;&nbsp;<label>All Physical Shares held by the user is to be distributed as per the Beneficiary details provided below.</label>
                            </div>
                        </div>
                    </div>


            </div>
        </div><!-- gen-row END -->

		<div id="guardiant_main_hidden" class="hide">
					<div id="fdJoinGuardiantAddWrap">
                        <h3>Add Guardian</h3>
                        <p class="note">
                            Note: Only requires to fill below information under these one conditions:<br>
                            1. If the Nominee is less than 18 years of age, the details of the Guardian have to be captured
                        </p>
                        <div class="panel panel-body sub-panel">
                            <div class="row">
                                <div class="col-xs-9">
                                    <label class="mandatory mandatory_label">Guardian Details <span class="mandatory_star">*</span></label>
                                    <div class="row">
                                        <div class="col-xs-3">
                                        	<select name="end_guardian_title" id="end_guardian_title" class="input-select form-control" title="Please select guardian Title" alt="Please select guardian Title" data-validation="length" data-validation-length="1-3" data-validation-error-msg="Please select valid guardian Title"><option value="">Title</option><option value="1">Mr. </option><option value="2">Mrs. </option><option value="3">Ms. </option></select>                                        </div>
										<div class="col-xs-3"><input class="form-control" placeholder="Last Name / Surname" name="end_guardian_lastname" value="" title="Please enter Last Name / Surname" alt="Please enter Last Name / Surname" maxlength="33" data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please enter valid guardian Last Name / Surname" type="text"></div>
                                        <div class="col-xs-3"><input class="form-control" placeholder="First Name" name="end_guardian_firstname" value="" title="Please enter guardian First Name" alt="Please enter guardian First Name" maxlength="33" type="text"></div>
                                        <div class="col-xs-3"><input class="form-control" placeholder="Middle Name" name="end_guardian_middlename" value="" title="Please enter guardian Middle Name" alt="Please enter guardian Middle Name" maxlength="33" type="text"></div>

                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <label class="mandatory">Date of Birth</label>
                                    <input class="form-control" id="show_guardian_birth_date" value="" placeholder="Birth date" readonly="" title="Please select guardian Birth date" alt="Please select guardian Birth date" maxlength="10" type="text">

                                    <input class="form-control" id="guardian_birth_date" placeholder="Date" name="end_guardian_birth_date" value="0000-00-00" type="hidden">
                                </div>
                                <!--<div class="col-xs-4">
                                    <label class="mandatory">Is this Address a Foreign Address?</label><br>
                                    <input type="checkbox" class="is_foreign_address" name="end_guardian_is_foreign_address" id="end_guardian_is_foreign_address" value="1" title='Please select if its a foreign address'  alt='Please select if its a foreign address' changezip="end_guardian_zipcode" country="end_guardian_country" state="end_guardian_state" span_country="span_end_guardian_country" span_state="span_end_guardian_state" other_state="span_end_guardian_state_other" title="Please select if it is a foreign address" alt="Please select if it is a foreign address"/>
                                </div> -->
                            </div>
                            <div class="divider"></div>
							<div class="row">
								<div class="col-xs-6">
									&nbsp;&nbsp;&nbsp;<input class="nominee_guardian_same_per_address" name="guardian_nominee_is_same_as_permanent" value="P" id="guardian_nominee_is_same_as_permanent" account_count="" for_address="nominee_guardian" title="Select if manent Address?" alt="Select if is this Address same as your Permanent Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
									<label class="mandatory">Is this Address same as your Permanent Address?</label>
								</div>
								<div class="col-xs-6">
									&nbsp;&nbsp;&nbsp;<input class="nominee_guardian_same_per_address" name="guardian_nominee_is_same_as_correspondence" value="C" id="guardian_nominee_is_same_as_correspondence" account_count="" for_address="nominee_guardian" title="Select if is this Address same as your Correspondence Address?" alt="Select if is this Address same as your Correspondence Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
									<label class="mandatory">Is this Address same as your Correspondence Address?</label>
								</div>
							</div>
							<div class="row" style="display:none;">
								<div class="col-xs-6">
								&nbsp;&nbsp;&nbsp;<input name="end_guardian_addr_copied_from" id="end_guardian_addr_copied_from" value="" type="hidden">
								</div>
							</div>
							<div class="divider"></div>
														<div id="populate_nominee_guardian_address">
								<div class="row">
									<div class="col-xs-4">
									&nbsp;&nbsp;&nbsp;<input class="is_foreign_address" name="end_guardian_is_foreign_address" id="end_guardian_is_foreign_address" value="1" title="Please select if its a foreign address" alt="Please select if its a foreign address" changezip="end_guardian_zipcode" country="end_guardian_country" state="end_guardian_state" span_country="span_end_guardian_country" span_state="span_end_guardian_state" other_state="span_end_guardian_state_other" type="checkbox">&nbsp;&nbsp;&nbsp;<label class="mandatory">Is this Address a Foreign Address?</label>
									</div>
								</div>
								<div class="divider"></div>
								<div class="row">
									<div class="col-xs-4">
										<label class="mandatory mandatory_label">Address Line1 <span class="mandatory_star">*</span></label>
										<input class="form-control" placeholder="Address Line1" name="end_guardian_address_line1" id="end_guardian_address_line1" value="" title="Please enter Address Line1 for guardian address" alt="Please enter Address Line1 for guardian address" maxlength="60" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1 for guardian address" type="text">
									</div>
									<div class="col-xs-4">
										<label class="mandatory mandatory_label">Address Line2 <span class="mandatory_star">*</span></label>
										<input class="form-control" placeholder="Address Line2" name="end_guardian_address_line2" id="end_guardian_address_line2" value="" title="Please enter Address Line2 for guardian address" alt="Please enter Address Line2 for guardian address" maxlength="60" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2 for guardian address" type="text">
									</div>
									<div class="col-xs-4">
										<label>Address Line3</label>
										<input class="form-control" placeholder="Address Line3" name="end_guardian_address_line3" id="end_guardian_address_line3" value="" title="Please enter Address Line3 for guardian address" alt="Please enter Address Line3 for guardian address" maxlength="60" type="text">
									</div>
								</div>
								<div class="divider"></div>
								<div class="row">
									<div class="col-xs-3">
										<label class="mandatory_label">City / Village / Town <span class="mandatory_star">*</span></label>
										<input class="form-control" placeholder="City / Village / Town" name="end_guardian_city_village_town" id="end_guardian_city_village_town" value="" title="Please enter City / Village / Town for guardian address" alt="Please enter City / Village / Town for guardian address" maxlength="60" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town for guardian address" type="text">
									</div>
									<div class="col-xs-2">
										<label class="mandatory mandatory_label">Pincode / ZipCode <span class="mandatory_star">*</span></label>
										<input class="form-control numeric" id="end_guardian_zipcode" placeholder="Pincode" name="end_guardian_zipcode" value="" maxlength="6" data-validation="length" data-validation-length="6-6" data-validation-error-msg="Please enter valid Pincode / Zipcode for guardian address" title="Please enter Pincode / Zipcode for guardian address" alt="Please enter Pincode / Zipcode for guardian address" type="text">
									</div>
									<div class="col-xs-2">
										<label class="mandatory mandatory_label">Country <span class="mandatory_star">*</span></label>
										<span id="span_end_guardian_country">
										<select name="end_guardian_country" id="end_guardian_country" class="input-select form-control" title="Please select a Country for guardian address" alt="Please select a Country for guardian address" data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please select a valid Country for guardian address" onchange="javascript: populate_states(this.value,'end_guardian_state', 'span_guardian_permanent_state', '0', 'toggle_other_state(\'end_guardian_state\', \'guardian_permanent_span_other_state\')'); toggle_other_state_country(this.value, 'guardian_permanent_span_other_state');;"><option value="102" selected="selected">India</option></select>									</span></div>
									<div class="col-xs-2">
										<label class="mandatory mandatory_label">State <span class="mandatory_star">*</span></label>
										<span id="span_end_guardian_state">
																								<select name="end_guardian_state" id="end_guardian_state" class="input-select form-control" title="Please select a State for guardian address" alt="Please select a State for guardian address" data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please select a valid State for guardian address" onchange="javascript: toggle_other_state('end_guardian_state', 'span_end_guardian_state_other');"><option value="">Please Select</option><option value="35">Andaman and Nicobar Islands</option><option value="28">Andhra Pradesh</option><option value="12">Arunachal Pradesh</option><option value="18">Assam</option><option value="10">Bihar</option><option value="4">Chandigarh</option><option value="22">Chhattisgarh</option><option value="26">Dadra and Nagar Haveli</option><option value="25">Daman and Diu</option><option value="7">Delhi</option><option value="30">Goa</option><option value="24">Gujarat</option><option value="6">Haryana</option><option value="2">Himachal Pradesh</option><option value="1">Jammu and Kashmir</option><option value="20">Jharkhand</option><option value="29">Karnataka</option><option value="32">Kerala</option><option value="31">Lakshadweep</option><option value="23">Madhya Pradesh</option><option value="27">Maharashtra</option><option value="14">Manipur</option><option value="17">Meghalaya</option><option value="15">Mizoram</option><option value="13">Nagaland</option><option value="21">Odisha</option><option value="34">Puducherry</option><option value="3">Punjab</option><option value="8">Rajasthan</option><option value="11">Sikkim</option><option value="33">Tamil Nadu</option><option value="36">Telangana</option><option value="16">Tripura</option><option value="5">Uttarakhand</option><option value="9">Uttar Pradesh</option><option value="19">West Bengal</option></select>																						</span>
									</div>
									<div class="col-xs-3">
																				<span id="span_end_guardian_state_other" style="display:none;" validatemsg="Please enter Other State for guardian address">
											<label class="cnd-mandatory mandatory_label">Other state <span class="mandatory_star">*</span></label>
											<input name="end_guardian_state_other" id="end_guardian_state_other" class="form-control" value="" placeholder="Other state" maxlength="60" title="Please enter Other State for guardian address" alt="Please enter Other State for guardian address" type="text">
										</span>
									</div>
								</div>
							</div>
							<div class="divider"></div>
							<div class="row">
								<div class="col-xs-6">
									<label class="mandatory">Telephone Number </label>
									<div id="TelephoneAddWrap2">
																				<div class="row">
											<div class="col-xs-plus">
												<span class="plus_sign">+</span>
											</div>
											<div class="col-xs-2">
												<input class="form-control numeric" name="end_guardian_phone_isd[]" placeholder="ISD" maxlength="3" title="Please enter guardian Telephone ISD" alt="Please enter guardian Telephone ISD" value="" type="text">
											</div>
											<div class="col-xs-3">
												<input class="form-control numeric" name="end_guardian_phone_std[]" placeholder="STD" maxlength="5" title="Please enter guardian Telephone STD" alt="Please enter guardian Telephone STD" value="" type="text">
											</div>
											<div class="col-xs-5 last">
												<input class="form-control numeric" name="end_guardian_phone_number[]" placeholder="Phone Number" maxlength="10" title="Please enter guardian Telephone number" alt="Please enter guardian Telephone number" value="" type="text">
											</div>
											<a href="javascript:void(0);" class="btn-add" id="TelephoneAddBtn2" title="Please click to enter additional Telephone Numbers" alt="Please click to enter additional Telephone Numbers">+</a>
											<input id="guardian_num_phone_numbers" value="1" type="hidden">
										</div>
																		</div>
								</div>
								<div class="col-xs-6">
									<label class="mandatory">Mobile Number </label>
									<div id="MobileAddWrap2">
																				<div class="row">
											<div class="col-xs-plus">
												<span class="plus_sign">+</span>
											</div>
											<div class="col-xs-2">
												<input class="form-control numeric" name="end_guardian_mobile_isd[]" placeholder="ISD" maxlength="3" title="Please enter guardian Mobile ISD" alt="Please enter guardian Mobile ISD" data-validation="" data-validation-length="2-3" data-validation-error-msg="Please enter valid guardian Mobile ISD" type="text">
											</div>
											<div class="col-xs-8 last">
												<input class="form-control numeric" name="end_guardian_mobile_number[]" placeholder="Mobile Number" maxlength="10" title="Please enter guardian Mobile number" alt="Please enter guardian Mobile number" data-validation="" data-validation-length="8-10" data-validation-error-msg="Please enter valid guardian Mobile number" type="text">
											</div>
											<a href="javascript:void(0);" class="btn-add" id="MobileAddBtn2" title="Please click to enter additional Mobile Numbers" alt="Please click to enter additional Mobile Numbers">+</a>
											<input id="guardian_num_mobile_numbers" value="1" type="hidden">
										</div>
																			</div>
								</div>
							</div>
							<div class="divider"></div>
							<div class="row">
								<div class="col-xs-6">
									<label class="mandatory">Email </label>
									<div id="EmailAddWrap2">
                                        										<div class="row">
											<div class="col-xs-11 last">
												<input class="form-control" name="end_guardian_email[]" placeholder="Email" title="Please enter guardian Email" alt="Please enter guardian Email" maxlength="254" data-validation="" data-validation-length="1-254" data-validation-error-msg="Please enter valid guardian Email" type="text">
											</div>
											<a href="javascript:void(0);" class="btn-add" id="EmailAddBtn2" title="Please click to enter additional email IDs" alt="Please click to enter additional email IDs">+</a>
											<input id="guardian_num_emails" value="1" type="hidden">
										</div>
										                                    </div>
								</div>
							</div>
                        </div><!-- panel-body END -->
                    </div>
            	</div>
        <!-- <script type="text/javascript" src="/js/stocks-bonds-details.js" lang="javascript"></script>
        <script src="/js/form-validator/jquery.form-validator.js"></script>
        <script src="/js/jquery.numeric.js"></script>
        <link media="all" type="text/css" rel="stylesheet" href="/css/jquery-ui.css">
        <script type="text/javascript" src="/js/jquery-ui.js" lang="javascript"></script> -->
        <!-- <script>
            account_count = 1;
            actual_account_count = 1;
            beneficiary_count = 1;
            actual_beneficiary_count = 1;

            $(window).load(function(){
                for (var i = 1; i <= account_count; i++) {
                    var temp = $('#actual_phone_count_'+i).val();
                    actual_phone_count[i] = $('#actual_phone_count_'+i).val();
                    actual_office_phone_count[i] = $('#actual_office_count_'+i).val();
                    actual_mobile_count[i] = $('#actual_mobile_count_'+i).val();
                    actual_email_count[i] = $('#actual_email_count_'+i).val();
                    actual_fax_count[i] = $('#actual_fax_count_'+i).val();
                }
            });

        </script> -->
		<script>
		$(document).ready(function(){
			$.validate({form:"#frmBankAccount", onSuccess:validate_stocksbonds});
		});
		$('#btn-update').click(function(){
			$.validate({form:"#frmBankAccount", onSuccess:validate_stocksbonds});
		});
		$('#btn-add').click(function(){
			$.validate({form:"#frmBankAccount", onSuccess:validate_stocksbonds});
		});

		</script>
    </div>
