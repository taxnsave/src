<form action="data_submit/frmForm.php" name="frmNonBeneficiary" id="frmNonBeneficiary" method="post" class="frmCurrent">
<input type="hidden" name="form_id" value="13">
<div class="agreement-row">
			<div class="container">

				<div class="row">
										<div class="col-xs-12">
						<h4><strong>Non-Beneficiaries</strong>&nbsp; &nbsp; &nbsp;<a href="#" class="blue-italic-cities" data-toggle="modal" data-target="#non-beneficiary">Definition?</a></h4>
						<div class="panel panel-body">
						   <br>
						</div>
					</div>
				</div><!-- Listing END -->
			</div>
        	<div class="container">
            	<div class="row">
					<div class="col-xs-12">
                    	<div id="mfAddWrap">
                        <div><!-- do not delte this div -->

									<div id="num1">
										<h3>
                      	<span class="non_beneficiary_numbering" style="float:left;">Add Non-Beneficiaries </span>
                  			<span><a href="javascript:void(0);" id="mfAddBtn" class="btn btn-primary btn-sm btn-add-large" title='Click to Add more Non-beneficiaries' alt='Click to Add more Non-beneficiaries'>+ Add Non Beneficiary</a></span>
                    </h3>
										<div class="panel panel-body">
											<div class="row">
												<div class="col-xs-12">
													<label class="mandatory mandatory_label">Name of Non-Beneficiaries <span class="mandatory_star">*</span></label>
													<div class="row">

														<div class="col-xs-3">
														   <select name="title[]" id="title[]" class="input-select form-control" title='Please select a Title' alt='Please select a Title' data-validation="length" data-validation-length="1-20" data-validation-error-msg="Please select a Title" onchange="javascript: title_change();"  ><option value="">Title</option><option value="1">Mr.</option><option value="2">Mrs.</option><option value="3">Ms.</option><option value="10">Master</option><option value="23">Kumar</option><option value="24">Kumari</option></select>														</div>
														<div class="col-xs-3">
															<input type="text" class="form-control" name="benificiary_lastname[]" placeholder="Last Name / Surname" value="" title='Please enter Last Name / Surname' alt='Please enter Last Name / Surname' maxlength="33" data-validation="length" data-validation-length="2-33" data-validation-error-msg="Please enter valid Last Name / Surname">
														</div>
														<div class="col-xs-3">
															<input type="text" class="form-control" name="benificiary_firstname[]" placeholder="First Name" value="" title='Please enter First Name' alt='Please enter First Name' maxlength="33" >
														</div>
														<div class="col-xs-3">
															<input type="text" class="form-control" name="benificiary_middlename[]" placeholder="Middle Name" value="" title='Please enter Middle Name' alt='Please enter Middle Name' maxlength="33" >
														</div>

													</div>

													<div class="divider"></div>

													 <div class="row">

														<div class="col-xs-3">
															<label class="mandatory mandatory_label">Relationship with the Testator <span class="mandatory_star">*</span></label><br>
															<select name="relationship_with_testator[]" id="relationship_with_testator[]" class="input-select form-control" title='Please select a Relationship with you' alt='Please select a Relationship with you' data-validation='length' data-validation-length='1-3' data-validation-error-msg='Please select a Relationship with you' onchange="javascript: toggle_other_relation_value(this.value, 'span_beneficary_relationship');;"  ><option value="">Please select</option><option value="2">Spouse</option><option value="3">Son</option><option value="4">Daughter</option><option value="5">Mother</option><option value="6">Father</option><option value="7">Brother</option><option value="8">Sister</option><option value="19">Grand Daughter</option><option value="20">Grandson</option><option value="21">Grand Father</option><option value="22">Grand Mother</option><option value="23">Son-in-Law</option><option value="24">Daughter-in-law</option><option value="99">Other</option></select>														</div>
																													<span id="span_beneficary_relationship"  style='display:none;'>
																<div class="col-xs-3">

																		<label class="cnd-mandatory mandatory_label">Other Relation (If not listed) <span class="mandatory_star">*</span></label>
																		<input type="text" class="form-control" name="other_relationship[]" value="" placeholder="Other Relationship"  maxlength="33" />

																</div>
														</span>

														<div class="col-xs-6">
															<label class="mandatory">Any Other Information</label><br>
															<input type="text" class="form-control" name="other_info[]" placeholder="Any Other Information" value="" maxlength="200" />
														</div>
													</div>
												</div>
											</div>
	                            		</div><!-- panel-body END -->
	                            		                            		</div>

                        </div><!-- mfAddWrap END -->

                    </div><!-- col-xs-12 END -->
					 <p>&nbsp;</p>
					<div class="bottom_buttons">
						<div class="div_prev_button">
							<input type="button" name="previous" id="btn-previous" value="&laquo; Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="liability-details"/>
						</div>
						<div class="div_center_buttons">
							<input type="submit" name="add_non_beneficiary" id="btn-add" value="Add / Save" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " />
							<input type="submit" name="update" id="btn-update" value="Update" title="Click here to Update the changed data" alt="Click here to Update the changed data" class="btn-submit btn-submit-disabled" disabled/>
						</div>
						<div class="div_next_button" style="text-align: right;">
							<input type="button" name="next" id="btn-next" value="Next &raquo;" title="Click here to go to Next page" alt="Click here to go to Next page" class="btn-submit btn-move" next_page="contingency-special-clause"/>
						</div><br><br><br>
					</div>
					<input type="hidden" name="txt_other_submit"  id="txt_other_submit" value="0">
<input type="hidden" name="txt_redirect_page"  id="txt_redirect_page" value="">
<input type="submit" name="sumbit" value="Submit" class="btn-submit btn-submit-disabled" title="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" alt="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" disabled/>                </div><!-- row END -->
				<p>&nbsp;</p>
			</div>
        </div><!-- gen-row END -->

    </div><!-- main END -->
	</form>
