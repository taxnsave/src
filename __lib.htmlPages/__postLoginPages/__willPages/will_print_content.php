<?php 
	
	include_once("__lib.includes/config.inc.php");
	
	$pi_data = $willProfile->getWillDetails('will_personal_information');

    $bene_data = $willProfile->getWillDetails('will_beneficiary');

	$exe_data = $willProfile->getWillDetails('will_executor');

    $wip_data = $willProfile->getWillDetails('will_immovable_properties');

    $wba_data = $willProfile->getWillDetails('will_bank_accounts');

    $wli_data = $willProfile->getWillDetails('will_locker_info');

    $wfd_data = $willProfile->getWillDetails('will_fixed_deposit');

    $wmf_data = $willProfile->getWillDetails('will_mutual_fund');

    $wgi_data = $willProfile->getWillDetails('will_general_insurance');

    $wshare_data = $willProfile->getWillDetails('will_share');

    $wbd_data = $willProfile->getWillDetails('will_bond_details');

    $wjewel_data = $willProfile->getWillDetails('will_jewellery');

    $wrf_data = $willProfile->getWillDetails('will_retirement_funds');

    $wlic_data = $willProfile->getWillDetails('will_lic');

    $wvech_data =$willProfile->getWillDetails('will_vechile');

    $pa_data = $willProfile->getWillDetails('will_pet_animal');

    $woasset_data = $willProfile->getWillDetails('will_other_assets');

    $wbact_data = $willProfile->getWillDetails('will_business');

    $wda_data = $willProfile->getWillDetails('will_digital_assets');
	
    $wbo_data = $willProfile->getWillDetails('will_body_organ');

    $wls_data = $willProfile->getWillDetails('will_liabilities');

    $wal_data = $willProfile->getWillDetails('will_add_liabilities');

    $wcust_data = $willProfile->getWillDetails('will_custodian');

    $wwit_data = $willProfile->getWillDetails('will_witness');

	// print_r($bene_data);
?>

<div class="container">
	
	<table>
        <tr><td><br> </td></tr>
        <tr><td><br> </td></tr>
        <tr><td><br> </td></tr>
        <tr><td><br> </td></tr>
        <tr><td><p><center><strong>LAST WILL<br>AND<br>TESTAMENT<br>of<br> 
            <?php 
                echo $pi_data->pi_f_name;
            ?>
            </strong></center></p></td></tr>
        
        
        <tr><td><strong>A. PERSONAL PROFILE</strong></td></tr>
	
		<tr><td>&nbsp;</td></tr>

		<tr class="fix-width">
			<td>
				I, <strong> <?php echo $pi_data->pi_f_name." "; echo $pi_data->pi_m_name." "; echo $pi_data->pi_l_name;?> </strong> son/daughter/wife of  <strong> <?php echo $pi_data->pi_gd_f_name." "; echo $pi_data->pi_gd_mid_name." "; echo $pi_data->pi_gd_l_name; ?> </strong>, currently residing at <strong> <?php echo $pi_data->pi_per_addr_line1." "; echo $pi_data->pi_per_addr_line2." "; echo $pi_data->pi_per_addr_line3." "; echo $pi_data->pi_per_city." "; echo $pi_data->pi_per_state." "; echo $pi_data->pi_per_country." "; echo $pi_data->pi_per_zip; ?></strong>, by religion <strong> <?php echo $pi_data->pi_religion; ?> </strong>, <strong> <?php echo $pi_data->nationality; ?> </strong> by nationality, aged about <strong> <?php echo $pi_data->pi_per_info_age; ?> </strong>, having PAN No./Aadhar No <strong> <?php echo $pi_data->pi_panaadharnum." / "; echo $pi_data->pi_panaadharnumper; ?> </strong> do hereby revoke all my previous Wills  and declare that this is my last Will, which I make on this <strong> <?php echo $pi_data->pi_f_name; ?> </strong>
			</td>
		</tr>


		<tr><td>&nbsp;</td></tr>

		<tr><td class="fix-width">I declare that I am writing this Will out of my free volition and am solely making this independent decision without any coercion or  under any undue influence whatsoever and I maintain good health & possess a sound mind.</td></tr>

		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>B.	FAMILY DESCRIPTION/BENEFICIARIES</strong></td></tr>

		<tr><td>&nbsp;</td></tr>

		<tr>
			<td class="fix-width">
				<ol>
					<li>
						<strong><?php echo $bene_data->f_name." "; echo $bene_data->m_name." "; echo $bene_data->l_name;?></strong> my <strong> <?php echo $bene_data->rel_with_testator." "; ?> </strong>, aged <strong> <?php echo $bene_data->ben_age." "; ?> </strong>years residing at <strong> <?php echo $bene_data->per_addr_line1." "; echo $bene_data->per_addr_line2." "; echo $bene_data->per_addr_line3." "; echo $bene_data->per_city." "; echo $bene_data->per_state." "; echo $bene_data->per_country." "; echo $bene_data->per_zip; ?> </strong>
					</li>
					<!-- <li>.................... my ..............., aged............years residing at..........</li>
					<li>.................... my ..............., aged............years residing at..........</li>
					<li>.................... my ..............., aged............years residing at..........</li>
					<li>.................... my ..............., aged............years residing at..........</li> -->
				</ol>
			</td>
		</tr>

		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>C.	APPOINTMENT OF EXECUTOR</strong></td></tr>

		<tr><td>&nbsp;</td></tr>

		<tr>
			<td class="fix-width">
				I hereby appoint <strong> <strong> <?php echo $exe_data->exe_f_name." "; echo $exe_data->exe_m_name." "; echo $exe_data->exe_l_name;?> </strong> </strong>, Son/Daughter of <strong> <?php echo $exe_data->exe_fat_f_name." "; echo $exe_data->exe_fat_m_name." "; echo $exe_data->exe_fat_l_name." "; echo $exe_data->exe_religious;?> </strong>, by religion, <strong> <?php echo $exe_data->exe_nationality." ";?> </strong> by nationality, aged about <strong> <?php echo $exe_data->exe_age." ";?> </strong>, having occupation <strong> <?php echo $exe_data->exe_occupation." ";?> </strong> currently residing at <strong> <?php echo $exe_data->exe_per_addr_line1." "; echo $exe_data->exe_per_addr_line2." "; echo $exe_data->exe_per_addr_ine3." "; echo $exe_data->exe_per_city." "; echo $exe_data->exe_per_state." "; echo $exe_data->exe_per_country." "; echo $exe_data->exe_per_zip; ?> </strong>to be the Sole Executor of this Will
				<br><br>
				<strong> <?php echo $exe_data->exe_f_name." "; echo $exe_data->exe_m_name." "; echo $exe_data->exe_l_name;?> </strong> is <strong> <?php echo $exe_data->rel_with_tes." ";?> </strong> of <strong> <?php echo $pi_data->pi_f_name." "; echo $pi_data->pi_m_name." "; echo $pi_data->pi_l_name;?> </strong>
				<br><br>
				In the event of death of <strong> <?php echo $exe_data->exe_f_name." "; echo $exe_data->exe_m_name." "; echo $exe_data->exe_l_name;?> </strong> before me, then in such case ……………., Son/ Daughter of ..............., by religion………….., ................... by nationality, aged about ………., having occupation ............ currently residing at ....................will be the executor of this Will.
				<br><br>
				........................ is ................. of <strong> <?php echo $pi_data->pi_f_name." "; echo $pi_data->pi_m_name." "; echo $pi_data->pi_l_name;?> </strong>
			</td>
		</tr>

		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>D. GUARDIAN DETAILS</strong></td></tr>

		<tr><td>&nbsp;</td></tr>

		<tr>
			<td class="fix-width">
				I hereby appoint <strong> <?php echo $bene_data->gd_f_name." "; echo $bene_data->gd_m_name." "; echo $bene_data->gd_l_name;?> </strong> Son/daughter of <strong> <?php echo $bene_data->gd_fat_f_name." "; echo $bene_data->gd_fat_m_name." "; echo $bene_data->gd_fat_l_name;?> </strong>, by religion <strong> <?php echo $bene_data->gd_religious." , "; echo $bene_data->gd_nationality." ";?> </strong>,  by nationality, aged about <strong> <?php echo $bene_data->gd_age." , ";?> </strong>,  having occupation <strong> <?php echo $bene_data->gd_occupation." , ";?> </strong> currently residing at <strong> <?php echo $bene_data->gd_per_addr_line1." "; echo $bene_data->gd_per_addr_line2." "; echo $bene_data->gd_per_addr_line3." "; echo $bene_data->gd_per_city." "; echo $bene_data->gd_per_state." "; echo $bene_data->gd_per_country." "; echo $bene_data->gd_per_zip; ?> </strong> as Guardian of my minor son/daughter <strong> <?php echo $bene_data->f_name." "; echo $bene_data->m_name." "; echo $bene_data->l_name;?> </strong> for the personal care and his/her property till he/she attains age of 18 years. 
				Relationship of the Guardian with the Minor <strong> <?php echo $bene_data->rel_with_testator." "; ?> </strong>
			</td>
		</tr>

		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>E.	DISTRIBUTION OF ASSETS</strong></td></tr>

		<tr><td>&nbsp;</td></tr>

		<tr><td class="fix-width">Presently, I have the following estates consisting of both immovable and movable properties ,  and I hereby give, leave and bequeath the following legacies to be effected out of my estates after the event of my death, viz;
		</td></tr>
		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>&rArr; IMMOVABLE PROPERTY</strong></td></tr>
		<tr><td>&nbsp;</td></tr>

		<tr>
			<td>
				<ol>
					<li>
						a. My <strong> <?php echo $wip_data->prop_type." "; ?> </strong> land  measuring <strong> <?php echo $wip_data->prop_mes." "; ?> </strong> located at <strong> <?php echo $wip_data->prop_addr_line1." "; echo $wip_data->prop_addr_line2." "; echo $wip_data->prop_addr_line3." "; echo $wip_data->prop_city." "; echo $wip_data->prop_state." "; echo $wip_data->prop_country." "; echo $wip_data->prop_zipcode; ?> </strong> having <strong> <?php echo $wip_data->prop_type." "; ?> </strong> ownership to be given in favour of my <strong>............</strong>, named <strong>............</strong> 
						<br>OR<br>
						b. <strong>...............</strong> of my agriculture land measuring <strong>..........................</strong>  located at .................. having .............. ownership to be given in favour of my............, named.............. 
					</li>
					<li>
						a. My <strong>non-agriculture</strong> land measuring .......................... located at .................. having .............. ownership to be given in favour of my............, named.............. 
						<br>OR<br>
						b. ............... of my <strong>non- agriculture</strong> land measuring .......................... located at .................. having .............. ownership to be given in favour of my............, named.............. 
					</li>
					<li>
						a. My <strong>commercial building</strong> of ............ located at .................. having .............. ownership to be given in favour of my............, named.............. 
						<br>OR<br>
						b. ............... of my <strong>commercial building</strong>  of .............located at .................. having .............. ownership to be given in favour of my............, named.............. 
					</li>
					<li>
						a. My <strong>residential building</strong>  of ............located at .................. having .............. ownership to be given in favour of my............, named..............
						<br>OR<br>
						b. ............... of my <strong>residential building</strong> of .......... located at .................. having .............. ownership to be given in favour of my............, named.............. 
					</li>
					<li>
						a. I, holding ....................... <strong>Factory including/or Plant and Machinery</strong> located at .................. having .............. ownership to be given in favour of my............, named..............
						<br>OR<br>
						b. ............... of my <strong>Factory including/or Plant and Machinery located</strong> at ..................  having ..............  ownership  to be given in favour of my............, named.............. 
					</li>
				</ol>
			</td>
		</tr>

		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>&rArr; ANY OTHER IMMOVABLE PROPERTIES NOT MENTIONED ABOVE:</strong></td></tr>
		<tr><td>&nbsp;</td></tr>

		<tr>
			<td class="fix-width">
				<ol>
					<li>
						I holding <strong> <?php echo $wip_data->prop_type." "; ?> </strong> having <strong> <?php echo $wip_data->prop_own_status." "; ?> </strong>ownership located at <strong> <?php echo $wip_data->prop_addr_line1." "; echo $wip_data->prop_addr_line2." "; echo $wip_data->prop_addr_line3." "; echo $wip_data->prop_city." "; echo $wip_data->prop_state." "; echo $wip_data->prop_country." "; echo $wip_data->prop_zipcode; ?> </strong> to be given in favour of
						my................  named................
					</li>
					<br>OR<br>
					<li>
						........... of my  ................... having .............ownership located at ..................  held under my name to be given in favour of my............, named.............. 
					</li>
				</ol>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>


		<tr><td><strong>&rArr; MOVABLE PROPERTY</strong></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td class="fix-width">
				<ol>
					<li>
						a. My <strong><?php echo $wba_data->bnk_acc_type." "; ?></strong> bank account  having account number <strong><?php echo $wba_data->bnk_acc_no." "; ?></strong> maintained with <strong> <?php echo $wba_data->bnk_bank_name." , "; echo $wba_data->bnk_branch_name." ";?> </strong>branch  located at <strong> <?php echo $wba_data->bnk_acc_addr_line1." "; echo $wba_data->bnk_acc_addr_line2." "; echo $wba_data->bnk_acc_addr_line3." "; echo $wba_data->bnk_acc_city." "; echo $wba_data->bnk_acc_state." "; echo $wba_data->bnk_acc_country." "; echo $wba_data->bnk_acc_zip; ?> </strong> to be given in favour of my............, named..............
						<br>OR<br>
						b. <strong>...Share...</strong> of my <strong> <?php echo $wba_data->bnk_acc_type." "; ?> </strong> bank account  having account number <strong> <?php echo $wba_data->bnk_acc_no." "; ?> </strong> maintained with <strong> <?php echo $wba_data->bnk_bank_name." , "; echo $wba_data->bnk_branch_name." ";?> </strong>, branch located at <strong> <?php echo $wba_data->bnk_acc_addr_line1." "; echo $wba_data->bnk_acc_addr_line2." "; echo $wba_data->bnk_acc_addr_line3." "; echo $wba_data->bnk_acc_city." "; echo $wba_data->bnk_acc_state." "; echo $wba_data->bnk_acc_country." "; echo $wba_data->bnk_acc_zip; ?> </strong> to be given in favour of my............, named.............. 
					</li>
					<li>
						a. The contents of Bank Locker no <strong> <?php echo $wli_data->locker_number." "; ?> </strong>,  maintained with <strong> <?php echo $wli_data->locker_bank_name." "; ?> </strong>bank <strong> <?php echo $wli_data->locker_branch_name." "; ?> </strong> branch, located at <strong> <?php echo $wli_data->locker_addr_line1." "; echo $wli_data->locker_addr_line2." "; echo $wli_data->locker_addr_line3." "; echo $wli_data->locker_city." "; echo $wli_data->locker_state." "; echo $wli_data->locker_country." "; echo $wli_data->locker_zipcode; ?> </strong> to be given in favour of my <strong> ............ </strong>, named <strong>............</strong>
						<br>OR<br>
						b. <strong>...share...</strong> of the contents of Bank Locker no <strong> <?php echo $wli_data->locker_number." "; ?> </strong>, maintained with <strong> <?php echo $wli_data->locker_bank_name." "; ?> </strong> bank <strong> <?php echo $wli_data->locker_addr_line1." "; echo $wli_data->locker_addr_line2." "; echo $wli_data->locker_addr_line3." "; echo $wli_data->locker_city." "; echo $wli_data->locker_state." "; echo $wli_data->locker_country." "; echo $wli_data->locker_zipcode; ?> </strong>branch, to be given in favour of my <strong> ............ </strong>, named <strong>............</strong>
					</li>
					<li>
						a. My Fixed Deposits maintained with <strong> <?php echo $wfd_data->fd_bank_name." , "; echo $wfd_data->fd_branch_name." , "; ?> </strong> branch, situated at <strong> <?php echo $wfd_data->fd_addr_line1." "; echo $wfd_data->fd_addr_line2." "; echo $wfd_data->fd_addr_line3." "; echo $wfd_data->fd_city." "; echo $wfd_data->fd_state." "; echo $wfd_data->fd_country." "; echo $wfd_data->fd_zipcode; ?> </strong> bearing FD Account number <strong> <?php echo $wfd_data->fd_account_number." , "; ?> </strong> to be given in favour of my............, named.............. 
						<br>OR<br>
						b. <strong>...Share...</strong> of my Fixed Deposits maintained with <strong> <?php echo $wfd_data->fd_bank_name." , "; echo $wfd_data->fd_branch_name." , "; ?> </strong> branch, situated at <strong> <?php echo $wfd_data->fd_addr_line1." "; echo $wfd_data->fd_addr_line2." "; echo $wfd_data->fd_addr_line3." "; echo $wfd_data->fd_city." "; echo $wfd_data->fd_state." "; echo $wfd_data->fd_country." "; echo $wfd_data->fd_zipcode; ?> </strong> bearing FD Account number <strong> <?php echo $wfd_data->fd_account_number." , "; ?> </strong> to be given in favour of my <strong> ............</strong>, named <strong> ............</strong> 
					</li>
					<li>
						a. My Mutual Fund investments amounting to Rs. <strong> <?php echo $wmf_data->mutl_amount." "; ?> </strong>(Rupees ............) of various fund houses <strong> <?php echo $wmf_data->mutl_comp_name." "; ?> </strong> to be given in favour of my<strong> ............</strong>, named <strong> ............</strong> 
						<br>OR<br>
						b. <strong>...Share...</strong> of my Mutual Fund investments amounting to Rs. <strong> <?php echo $wmf_data->mutl_amount." "; ?> </strong>(Rupees ............) of various fund houses <strong> <?php echo $wmf_data->mutl_comp_name." "; ?> </strong> to be given in favour of my <strong> ............</strong>, named <strong> ............</strong> 
					</li>
					<li>
						a. The proceeds of my <strong><?php echo $wgi_data->gi_pol_type." "; ?></strong> insurance policy holding policy number. <strong> <?php echo $wgi_data->gi_com_name." "; ?> </strong> from <strong> <?php echo $wgi_data->gi_start_date." "; ?> </strong> to <strong> <?php echo $wgi_data->gi_mat_date." "; ?> </strong> of <strong> <?php echo $wgi_data->gi_amount." "; ?> </strong>  to be given in favour of my <strong>..............</strong>, named <strong>..............</strong>
						<br>OR<br>
						b. <strong>...Share...</strong> of the proceeds of my <strong> <?php echo $wgi_data->gi_pol_type." "; ?> </strong> insurance policy holding policy number <strong> <?php echo $wgi_data->gi_com_name." "; ?> </strong>from <strong> <?php echo $wgi_data->gi_start_date." "; ?> </strong> to <strong> <?php echo $wgi_data->gi_mat_date." "; ?> </strong> of <strong> <?php echo $wgi_data->gi_amount." "; ?> </strong>  to be given in favour of my <strong>..............</strong>, named <strong>..............</strong>
					</li>
					<li>
						a. My Investments in securities amounting to Rs. <strong> <?php echo $wshare_data->share_amount." "; ?> </strong>(Rupees ............) of various entities held in Demat form having Account number <strong> <?php echo $wshare_data->share_demat_num." "; ?> </strong> with <strong> <?php echo $wshare_data->share_bank_name." "; ?> </strong> to be given in favour of my <strong>..............</strong>, named <strong>..............</strong>
						<br>OR<br>
						b. <strong>...Share...</strong> of  my Investments in securities  amounting to Rs. <strong> <?php echo $wshare_data->share_amount." "; ?> </strong>(Rupees ............) of various entities held under my name in Demat form having Account number <strong> <?php echo $wshare_data->share_demat_num." "; ?> </strong> with <strong> <?php echo $wshare_data->share_bank_name." "; ?> </strong>  to be given in favour of my <strong>..............</strong>, named <strong>..............</strong> 
					</li>
					<li>
						a. My Investments in bonds amounting to Rs. <strong> <?php echo $wbd_data->bond_amount." "; ?> </strong>(Rupees ............) of various entities held in  Demat form Account number <strong> <?php echo $wbd_data->bond_demat_no." "; ?> </strong> under <strong> <?php echo $wbd_data->bond_scheme_name." "; ?> </strong> scheme with <strong> <?php echo $wbd_data->bond_bank_name." "; ?> </strong> to be given in favour of my <strong>..............</strong>, named <strong>..............</strong> 
						<br>OR<br>
						b. <strong>...Share...</strong> of my Investments in bonds amounting to Rs. <strong> <?php echo $wbd_data->bond_amount." "; ?> </strong>(Rupees ............) of various entities held under my name in  Demat form Account number <strong> <?php echo $wbd_data->bond_demat_no." "; ?> </strong> under <strong> <?php echo $wbd_data->bond_scheme_name." "; ?> </strong> scheme with <strong> <?php echo $wbd_data->bond_bank_name." "; ?> </strong> to be given in favour of my <strong>..............</strong>, named <strong>..............</strong>  
					</li>					
					<li>
						a. I own gold/ silver/diamond/other items <strong> <?php echo $wjewel_data->jwl_type." ,"; echo $wjewel_data->jwl_name." ,"; echo $wjewel_data->jwl_desc." ";?> </strong> of Rs. <strong> <?php echo $wjewel_data->jwl_amount." "; ?> </strong>(Rupees ............) which I wish to give in favour of my <strong>..............</strong>, named <strong>..............</strong>  
						<br>OR<br>
						b. <strong>...Share...</strong> of  gold/ silver/diamond/other items <strong> <?php echo $wjewel_data->jwl_type." ,"; echo $wjewel_data->jwl_name." ,"; echo $wjewel_data->jwl_desc." ";?> </strong>of Rs. <strong> <?php echo $wjewel_data->jwl_amount." "; ?> </strong>(Rupees ............) owned by me which I wish to give in favour of my <strong>..............</strong>, named <strong>..............</strong> 
						<br>OR<br>
						c. Gold/ silver/diamond/other item <strong> <?php echo $wjewel_data->jwl_type." ,"; echo $wjewel_data->jwl_name." ,"; echo $wjewel_data->jwl_desc." ";?> </strong> owned by me which I wish to give in favour of my <strong>..............</strong>, named <strong>..............</strong>  
					</li>
					<li>a. I holding Provident Fund under account number <strong><?php echo $wrf_data->pf_account_number." "; ?></strong> with <strong><?php echo $wrf_data->pf_company_name." "; ?></strong> having registered office at <strong><?php echo $wrf_data->pf_addr_line1." "; echo $wrf_data->pf_addr_line2." "; echo $wrf_data->pf_addr_line3." "; echo $wrf_data->pf_city." "; echo $wrf_data->pf_state." "; echo $wrf_data->pf_country." "; echo $wrf_data->pf_zipcode; ?></strong> to be given in favour of my............, named.............. and/or my .............., named.................. 
					<br>OR<br>
					b. <strong>...share...</strong> of my Provident Fund under account number <strong><?php echo $wrf_data->pf_account_number." "; ?></strong> with <strong><?php echo $wrf_data->pf_company_name." "; ?></strong> having registered office at <strong><?php echo $wrf_data->pf_addr_line1." "; echo $wrf_data->pf_addr_line2." "; echo $wrf_data->pf_addr_line3." "; echo $wrf_data->pf_city." "; echo $wrf_data->pf_state." "; echo $wrf_data->pf_country." "; echo $wrf_data->pf_zipcode; ?></strong> to be given in favour of my............, named.............. and/or my .............., named..................  
					</li>
					<li>
						a. I holding Pension fund under account number <strong><?php echo $wpenf_data->pen_acc_num." "; ?></strong> with <strong><?php echo $wpenf_data->pen_com_name." "; ?></strong> having registered office at <strong><?php echo $wpenf_data->pen_addr_line1." "; echo $wpenf_data->pen_addr_line2." "; echo $wpenf_data->pen_addr_line3." "; echo $wpenf_data->pen_city." "; echo $wpenf_data->pen_state." "; echo $wpenf_data->pen_country." "; echo $wpenf_data->pen_zipcode; ?></strong> to be given in favour of my............, named.............. and/or my .............., named.................. 
						<br>OR<br>
						b. <strong>...share...</strong> of my Pension fund under account number <strong><?php echo $wpenf_data->pen_acc_num." "; ?></strong> with <strong><?php echo $wpenf_data->pen_com_name." "; ?></strong> having registered office at <strong><?php echo $wpenf_data->pen_addr_line1." "; echo $wpenf_data->pen_addr_line2." "; echo $wpenf_data->pen_addr_line3." "; echo $wpenf_data->pen_city." "; echo $wpenf_data->pen_state." "; echo $wpenf_data->pen_country." "; echo $wpenf_data->pen_zipcode; ?></strong> to be given in favour of my............, named.............. and/or my .............., named.................. 
					</li>
					<li>
						a. I holding Gratuity under account number <strong> <?php echo $wgrat_data->gra_acc_num." "; ?> </strong> with <strong> <?php echo $wgrat_data->gra_com_name." "; ?> </strong> having registered office at <strong> <?php echo $wgrat_data->gra_addr_line1." "; echo $wgrat_data->gra_addr_line2." "; echo $wgrat_data->gra_addr_line3." "; echo $wgrat_data->gra_city." "; echo $wgrat_data->gra_state." "; echo $wgrat_data->gra_country." "; echo $wgrat_data->gra_zipcode; ?> </strong> to be  given in favour of my............, named.............. and/or my .............., named.................. 
						<br>OR<br>
						b. <strong>...Share...</strong> of my Gratuity held under my name under account number <strong> <?php echo $wgrat_data->gra_acc_num." "; ?> </strong> with <strong> <?php echo $wgrat_data->gra_com_name." "; ?> </strong> having registered office at <strong> <?php echo $wgrat_data->gra_addr_line1." "; echo $wgrat_data->gra_addr_line2." "; echo $wgrat_data->gra_addr_line3." "; echo $wgrat_data->gra_city." "; echo $wgrat_data->gra_state." "; echo $wgrat_data->gra_country." "; echo $wgrat_data->gra_zipcode; ?> </strong>  to be given in favour of my <strong>..............</strong>, named <strong>..............</strong>  
					</li>
					<li>
						a. I holding Life Insurance Policy having policy number <strong> <?php echo $wlic_data->policy_number." "; ?> </strong> of Rs. <strong> <?php echo $wlic_data->sum_assured." "; ?> </strong>(Rupees ................) issued by <strong> <?php echo $wlic_data->lic_company_name." ,"; echo $wlic_data->lic_branch_name." ";?> </strong> branch, to be given in favour of my <strong>..............</strong>, named <strong>..............</strong>  
						<br>OR<br>
						b. <strong>...Share...</strong> of my Life Insurance Policy having policy number <strong> <?php echo $wlic_data->policy_number." "; ?> </strong> Rs. <strong> <?php echo $wlic_data->sum_assured." "; ?> </strong>(Rupees ................) held under my name issued by <strong> <?php echo $wlic_data->lic_company_name." ,"; echo $wlic_data->lic_branch_name." ";?> </strong> branch, in favour of my <strong>..............</strong>, named <strong>..............</strong> 
					</li>
					<li>
						<strong> <?php echo $wvech_data->vechile_name." "; ?> </strong> having registration no <strong> <?php echo $wvech_data->vechile_reg_num." "; ?> </strong> in <strong> <?php echo $wvech_data->vechile_color." "; ?> </strong> colour  registered under my name to be given in favour of my <strong>..............</strong>, named <strong>..............</strong> 
					</li>
				</ol>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>&rArr; ANY OTHER MOVABLE PROPERTIES NOT MENTIONED ABOVE:</strong></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td class="fix-width">
				<ol>
					<li>
						I holding <strong> <?php echo $woasset_data->oasset_name." "; ?> </strong> having <strong> <?php echo $woasset_data->oasset_own_type." "; ?> </strong> ownership located at <strong> <?php echo $woasset_data->oasset_addr_line1." "; echo $woasset_data->oasset_addr_line2." "; echo $woasset_data->oasset_addr_line3." "; echo $woasset_data->oasset_city." "; echo $woasset_data->oasset_state." "; echo $woasset_data->oasset_country." "; echo $woasset_data->oasset_zipcode; ?> </strong>in favour of my................  named................
					</li>
					<li>
						<strong>...Share...</strong> of <strong> <?php echo $woasset_data->oasset_name." "; ?> </strong> having <strong> <?php echo $woasset_data->oasset_own_type." "; ?> </strong> ownership under my name located at <strong> <?php echo $woasset_data->oasset_addr_line1." "; echo $woasset_data->oasset_addr_line2." "; echo $woasset_data->oasset_addr_line3." "; echo $woasset_data->oasset_city." "; echo $woasset_data->oasset_state." "; echo $woasset_data->oasset_country." "; echo $woasset_data->oasset_zipcode; ?> </strong> to be given in favour of my............, named.............. 
						<br>OR<br>
					</li>
					<li>
						I carrying on business of <strong> <?php echo $wbact_data->bus_type." "; ?> </strong> having <strong> <?php echo $wbact_data->bus_own_type." "; ?> </strong> ownership under the name and style of <strong> <?php echo $wbact_data->bus_com_name." "; ?> </strong> located at <strong> <?php echo $wbact_data->bus_addr_line1." "; echo $wbact_data->bus_addr_line2." "; echo $wbact_data->bus_addr_line3." "; echo $wbact_data->bus_city." "; echo $wbact_data->bus_state." "; echo $wbact_data->bus_country." "; echo $wbact_data->bus_zipcode; ?> </strong> to be given in favour of my............, named.............. 
						<br>OR<br>
					</li>
					<li>
						<strong>...Share...</strong>of business of <strong> <?php echo $wbact_data->bus_type." "; ?> </strong> carried on by me having <strong> <?php echo $wbact_data->bus_own_type." "; ?> </strong> ownership under the same and style of <strong> <?php echo $wbact_data->bus_com_name." "; ?> </strong> located at <strong> <?php echo $wbact_data->bus_addr_line1." "; echo $wbact_data->bus_addr_line2." "; echo $wbact_data->bus_addr_line3." "; echo $wbact_data->bus_city." "; echo $wbact_data->bus_state." "; echo $wbact_data->bus_country." "; echo $wbact_data->bus_zipcode; ?> </strong> to be given in favour of my............, named............
					</li>
				</ol>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td><strong>&rArr; PET ANIMALS</strong></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td class="fix-width">
				<ol>
					<li>
						I, having <strong> <?php echo $pa_data->pa_type." "; ?> </strong> named <strong> <?php echo $pa_data->pa_name." "; ?> </strong> currently residing with me which I wish to give to my .............. named ................ for taking its proper care after the event of my death.
					</li>
				</ol>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>F.	DISTRIBUTION OF ESTATE (REST AND RESIDUAL PROPERTIES)</strong></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td class="fix-width">
				a.  .......................... owned by me after the event of making this last Will on.......... to be given   in favour of my............, named.............. after the event of my death.
				<br>OR<br>
				b.   .......... of .......................... owned by me after the event of making this last Will on .................. to be given in favour of my............, named.............. and/or my .............., named................... after the event of my death.
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>G.	INTELLECTUAL PROPERTY RIGHTS</strong></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td class="fix-width">
				a. The proceeds of <strong> <?php echo $wda_data->dig_asset_type." "; ?> </strong> of <strong> <?php echo $wda_data->dig_asset_amount." "; ?> </strong> which is held under my name, I wish to give in favour of my............, named..............
				<br>OR<br>
				b. <strong>...Share...</strong> of the proceeds of <strong> <?php echo $wda_data->dig_asset_type." "; ?> </strong> of <strong> <?php echo $wda_data->dig_asset_amount." "; ?> </strong> which is held under my name, I wish to give in favour of my............, named.............. and/or my .............., named...................
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>H.	DONATION OF BODY ORGAN</strong></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td class="fix-width">
				I <strong> <?php echo $pi_data->pi_f_name." "; echo $pi_data->pi_m_name." "; echo $pi_data->pi_l_name;?> </strong> out of my free volition and without any coercion or under any undue influence whatsoever wish to donate my body organ <strong> <?php echo $wbo_data->borgan_name; ?>  </strong> to <strong> <?php echo $wbo_data->borgan_hos_name; ?> </strong> located at <strong> <?php echo $wbo_data->borgan_addr_line1." "; echo $wbo_data->borgan_addr_line2." "; echo $wbo_data->borgan_addr_line3." "; echo $wbo_data->borgan_city." "; echo $wbo_data->borgan_state." "; echo $wbo_data->borgan_country." "; echo $wbo_data->borgan_zipcode; ?> </strong>
				<br>
				I hereby declare that this is solely my independent decision.
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>I.	DISTRIBUTION OF LIABILITIES</strong></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td class="fix-width">
				a.	Loan amount of Rs. <strong> <?php echo $wls_data->amount; ?> </strong> (Rupees.....) outstanding as on date against my Immovable/ Movable property <strong> <?php echo $wls_data->liabi_type; ?> </strong> measuring/of  <strong> <?php echo $wls_data->liabi_prop_mes; ?> </strong> located at <strong> <?php echo $wls_data->liabi_addr_line1." "; echo $wls_data->liabi_addr_line2." "; echo $wls_data->liabi_addr_line3." "; echo $wls_data->liabi_city." "; echo $wls_data->liabi_state." "; echo $wls_data->liabi_country." "; echo $wls_data->liabi_zipcode; ?> </strong> availed from <strong> <?php echo $wls_data->liabi_inst_name; ?> </strong>  situated at <strong> <?php echo $wls_data->ind_addr_line1." "; echo $wls_data->ind_addr_line2." "; echo $wls_data->ind_addr_line3." "; echo $wls_data->ind_city." "; echo $wls_data->ind_state." "; echo $wls_data->ind_country." "; echo $wls_data->ind_zipcode; ?> </strong> from date <strong> <?php echo $wls_data->liabi_start_date; ?> </strong> to <strong> <?php echo $wls_data->liabi_closing_date; ?> </strong> maturity date at the rate of interest <strong> <?php echo $wls_data->liabi_int_rate; ?> </strong> to be paid by my............., named....................
				<br>OR<br>
				b.	<strong>...Share...</strong> of my Loan amount of Rs. <strong> <?php echo $wls_data->amount; ?> </strong> (Rupees.....) outstanding as on date against my Immovable/ Movable property <strong> <?php echo $wls_data->liabi_type; ?> </strong>measuring/of  <strong> <?php echo $wls_data->liabi_prop_mes; ?> </strong> located at <strong> <?php echo $wls_data->liabi_addr_line1." "; echo $wls_data->liabi_addr_line2." "; echo $wls_data->liabi_addr_line3." "; echo $wls_data->liabi_city." "; echo $wls_data->liabi_state." "; echo $wls_data->liabi_country." "; echo $wls_data->liabi_zipcode; ?> </strong> availed from <strong> <?php echo $wls_data->liabi_inst_name; ?> </strong>  situated at  <strong> <?php echo $wls_data->ind_addr_line1." "; echo $wls_data->ind_addr_line2." "; echo $wls_data->ind_addr_line3." "; echo $wls_data->ind_city." "; echo $wls_data->ind_state." "; echo $wls_data->ind_country." "; echo $wls_data->ind_zipcode; ?> </strong> from date <strong> <?php echo $wls_data->liabi_start_date; ?> </strong>to <strong> <?php echo $wls_data->liabi_closing_date; ?> </strong> maturity date at the rate of interest <strong> <?php echo $wls_data->liabi_int_rate; ?> </strong> to be paid by my............., named.................... and/or  my .............., named.................. 
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>J.	ADDITIONAL LIABILITIES:</strong></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td class="fix-width">
				a. I bearing liability/s of outstanding amount of Rs. <strong> <?php echo $wal_data->addlib_amount; ?> </strong>  (Rupees........)  in respect of loan/guarantee taken/given as on date <strong> <?php echo $pi_data->creation_date; ?>  </strong> against <strong> <?php echo $wal_data->addlib_pro_name; ?> </strong> from <strong> <?php echo $wal_data->addlib_loan_amount; ?> </strong> situated at  <strong> <?php echo $wal_data->addlib_addr_line1." "; echo $wal_data->addlib_addr_line2." "; echo $wal_data->addlib_addr_line3." "; echo $wal_data->addlib_city." "; echo $wal_data->addlib_state." "; echo $wal_data->addlib_country." "; echo $wal_data->addlib_zipcode; ?> </strong> having Loan account number <strong> <?php echo $wal_data->addlib_loan_acc_no; ?> </strong> to be paid by my............, named.............. and/or my .............., named.................. 
				<br>
				b.	I bearing liability/s of outstanding amount of Rs. <strong> <?php echo $wal_data->addlib_amount; ?> </strong>  (Rupees........)  in respect of loan/guarantee taken/given as on date against <strong> <?php echo $wal_data->addlib_pro_name; ?> </strong> from <strong> <?php echo $pi_data->creation_date; ?> </strong> situated at  <strong> <?php echo $wal_data->addlib_addr_line1." "; echo $wal_data->addlib_addr_line2." "; echo $wal_data->addlib_addr_line3." "; echo $wal_data->addlib_city." "; echo $wal_data->addlib_state." "; echo $wal_data->addlib_country." "; echo $wal_data->addlib_zipcode; ?> </strong> having Loan account number <strong> <?php echo $wal_data->addlib_loan_acc_no; ?> </strong> out of which <strong> <?php echo $wal_data->addlib_loan_amount; ?> </strong> of the total amount to be paid by my............, named.............. and/or  my .............., named.................. 
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>

		<tr><td class="fix-width">
			<strong>
				However, if any part of any estate is spent during my life time, the same will be deemed to be reduction in the respective estate and only the residue of that estate will be accepted by the respective legatee.
			</strong> 
		</td></tr>
		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>K.	NO CONTEST CLAUSE</strong></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td class="fix-width">
			It is my wish that my beneficiaries shall respect this Will in letter and spirit. Wherever there is a disproportionate distribution of any property, it has been done thoughtfully and considering the circumstances prevailing during my lifetime. Hence, no part of the Will can be challenged in any proceeding in any court on such grounds. I further declare that I have full knowledge and understanding of all the contents in this WILL and that the contents are true and correct as per my knowledge, belief and information.
		</td></tr>
		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>L.	CUSTODIAN </strong></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td class="fix-width">
				I, hereby appoint <strong> <?php echo $wcust_data->custodian_name; ?> </strong>, son/daughter/wife of Shri <strong> <?php echo $wcust_data->custodian_father_name; ?> </strong>, currently residing at <strong> <?php echo $wcust_data->custodian_addr_line1." "; echo $wcust_data->custodian_addr_line2." "; echo $wcust_data->custodian_addr_line3." "; echo $wcust_data->custodian_city." "; echo $wcust_data->custodian_state." "; echo $wcust_data->custodian_country." "; echo $wcust_data->custodian_zipcode; ?> </strong>, by religion <strong> <?php echo $wcust_data->custodian_religion.", "; echo $wcust_data->custodian_nationality; ?> </strong>, by nationality, aged about <strong> <?php echo $wcust_data->custodian_age; ?> </strong>, as my Custodian and will hold my Will until my death and thereafter shall handover the Will in proper condition to my executor after my death.
			</td>
		</tr>
		<tr>
			<td class="fix-width">
				Signature of Testator
				<br>
				...............
				<br>
				<strong> <?php echo $pi_data->pi_f_name." "; echo $pi_data->pi_m_name." "; echo $pi_data->pi_l_name;?> </strong>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>

		<tr><td><strong>M.	WITNESSES</strong></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td class="fix-width">
				We hereby attest that this Will has been signed by <strong> <?php echo $pi_data->pi_f_name." "; echo $pi_data->pi_m_name." "; echo $pi_data->pi_l_name;?> </strong> as his/her last Will at <strong> <?php echo $pi_data->pi_place." ,"; echo $pi_data->pi_date; ?> </strong> in the joint presence of himself and us. The testator is of sound mind and has made this Will out of his/her free will without any coercion or under any undue influence.
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>

		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>
				<p class="col-sm-6">
					Signature of Witness (1)<br>
					Name- <strong><?php echo $wwit_data->witness1_name; ?> </strong><br>
					Address- <strong> <?php echo $wwit_data->witness1_addr_line1." "; echo $wwit_data->witness1_addr_line2." "; echo $wwit_data->witness1_addr_line3." "; echo $wwit_data->witness1_city." "; echo $wwit_data->witness1_state." "; echo $wwit_data->witness1_country." "; echo $wwit_data->witness1_zipcode; ?></strong><br>
					Phone no- <strong><?php echo $wwit_data->witness1_phone; ?> </strong>
				</p>
				<p class="col-sm-6">
					Signature of Witness (2)<br>
					Name- <strong><?php echo $wwit_data->witness2_name; ?> </strong><br>
					Address- <strong> <?php echo $wwit_data->witness2_addr_line1." "; echo $wwit_data->witness2_addr_line2." "; echo $wwit_data->witness2_addr_line3." "; echo $wwit_data->witness2_city." "; echo $wwit_data->witness2_state." "; echo $wwit_data->witness2_country." "; echo $wwit_data->witness2_zipcode; ?></strong><br>
					Phone no- <strong><?php echo $wwit_data->witness2_phone; ?> </strong>
				</p>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>

		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>
				<p class="text-center"><strong>DRAFTED BY: WWW.TAXSAVE.IN</strong></p>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>

	</table>
	<!-- <p>
		 currently residing at ..................…………………., by religion………….., ................... by nationality, aged about ………., having PAN No./Aadhar No. ..................... do hereby revoke all my previous Wills  and declare that this is my last Will, which I make on this ……., ………………
		I declare that I am writing this Will out of my free volition and am solely making this independent decision without any coercion or  under any undue influence whatsoever and I maintain good health & possess a sound mind.
	</p> -->
</div>