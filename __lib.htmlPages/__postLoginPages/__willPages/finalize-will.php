<!DOCTYPE html>
<html lang="en">
  <head>
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="01 Oct 2014 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />    <!--[if !IE]>
	<![endif]-->
	<title>EzeeWill-finalize-will</title>


	<!--[if lt IE 9]>
	<script src="/js/compatibility/html5shiv.min.js"></script>
	<script src="/js/compatibility/respond.min.js"></script>
    <![endif]-->

    <!-- <link media="all" type="text/css" rel="stylesheet" href="css/bootstrap.min.css"> -->
    <!-- <link href="css/style.css" rel="stylesheet"> -->
    <!--<link href="/css/non-responsive.css" rel="stylesheet">-->
    <!-- <link media="all" type="text/css" rel="stylesheet" href="css/jquery-ui.css"> -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="https://www.ezeewill.com/js/jquery.min.js"></script>
	<script lang="javascript" type="text/javascript" src="js/jquery.numeric.js"></script>
	<script src="https://www.ezeewill.com/js/common.js?r=835"></script>
    <script src="https://www.ezeewill.com/js/post_login.js"></script>
	<script>
		var WS_PATH = "https://www.ezeewill.com/";
	</script>
    <link href="css/fonts.css" rel="stylesheet">
    <script src="https://www.ezeewill.com/js/jquery.cookie.js"></script>
    <script type='text/javascript' src='https://www.ezeewill.com/js/jquery.simplemodal.js'></script>
    <script src="https://www.ezeewill.com/js/browser.js"></script> -->
  </head>
  <body>
      <header>
        <div class="hdr-top">
            <div class="container">
                <div class="xrow">
                    <a>Welcome Arpit Basnet</a> | <a class="a_link" next_page="dashboard-main">Dashboard</a> | <a href="https://www.ezeewill.com/logout.php" class="a_link" next_page="logout.php">Logout</a></div>
	        </div>
        </div>
        <div class="hdr">
            <div class="container">
                <div class="row">
                	<div class="col-lg-5 col-xs-12 sponsorer">
                    	<div class="mlogo"><a href="https://www.ezeewill.com/"><img src="https://www.ezeewill.com/images/logo-ezeewill.png" title="EzeeWill" alt="EzeeWill" /></a></div>
                    	<div class="logoProj"><br>
                        Project by:<br>
                        <a target="_blank" href="https://egov-nsdl.co.in/"><img src="https://www.ezeewill.com/images/logo-nsdl.png" alt="" class="nsdl" /></a>
                        <a target="_blank" href="http://www.warmond.co.in/"><img src="https://www.ezeewill.com/images/logo-warmond.png" alt="" class="warmond" /></a>
                        </div>
                    </div>
                
                    <!--<div class="col-xs-2 logo"><img src="images/logo-ezeewill.png" alt="" /></div>
                    <div class="col-xs-3 sponsorer">
                        Project by:<br>
                        <a target="_blank" href="http://www.egov-nsdl.co.in"><img src="images/logo-nsdl.png" alt="" /></a>
                        <a target="_blank" href="http://www.warmond.co.in"><img src="images/logo-warmond.png" alt="" /></a>
                    </div>-->
                    <div class="col-xs-4"></div>
                    <div class="col-xs-3"></div>
                </div>
            </div>
        </div><!-- hdr END -->
    </header>
    <div class="main">
    <script src="https://www.ezeewill.com/js/progressbar.js"></script>
        		  
        <div class="gen-row">
        	<div class="container">            	<div class="row">
					<div class="col-xs-12 border-grey">
                    	<p class="note"><span class="txt-black"><b>Notes:</b></span><br>
						1. Block marked with <strong class="txt-orange">orange</strong> border are compulsory.<br>
                        2. Block marked in <strong class="txt-orange">orange</strong> border will change to <strong style="color:#5CB85C;">green</strong> once all the mandatory fields are updated.<br>
						3. All the fields label in <strong class="mandatory_star">red</strong> color and marked with symbol <span class="mandatory_star">*</span> are mandatory.<br/>
												</p>
                    </div>
				</div>
                <p class="sb-hdg" style="float:none;margin-bottom:0px;"><label style="font-weight:900; color: #542918;">Create Will</label></p>
            	<div class="row status-bar">
					<div class="col-xs-5">
                    	<div class="sb-hdg "><span class="number1">1</span> <label>Submit Details</label></div>
                        <div class="sb-dots">
                        	<span></span>
                        	<span></span>
                        	<span></span>
                        	<span></span>
                        	<span></span>
                        	<span></span>
                        	<span></span>

							<!-- <span ></span>
                        	<span ></span>
                        	<span ></span>
                        	<span ></span>
                        	<span ></span>
                        	<span ></span>
                        	<span ></span>
                        	<span ></span>
                        	<span ></span>
                        	<span ></span> -->
                        </div>
                    </div>
					<div class="col-xs-3">
                    	<div class="sb-hdg active"><span class="number2">2</span> <label style="font-size: 18px;">Confirm Details</label></div>
                        <div class="sb-dots">
                        	<span></span>
                        	<span></span>
                        </div>
                    </div>
					<div class="col-xs-4">
                    	<div class="sb-hdg "><span class="number3">3</span> <label style="font-size: 18px;">Proceed for Payment</label></div>
                        <div class="sb-dots">
                        	<span></span>
                        </div>
                    </div>
				</div><!-- status-bar END --><div class="row dashbaord-tabs">
                    <a href="https://www.ezeewill.com/dashboard" class="mandatory a_dashboard_tab  single-word " next_page="dashboard"><span></span>Personal Information</a>
                    
					<a href="https://www.ezeewill.com/beneficiary-details" class="mandatory a_dashboard_tab  single-word letter-spacing" next_page="beneficiary-details">Beneficiaries</a>
                    
					<a href="https://www.ezeewill.com/executor-details" class="mandatory a_dashboard_tab  single-word letter_spacing" next_page="executor-details">Executors</a>

                    <a href="https://www.ezeewill.com/bank-account-details" class="a_dashboard_tab  single-word letter_spacing " next_page="bank-account-details" canadd="1" tabname="bankaccount">Bank Accounts</a>
                    
					<a href="https://www.ezeewill.com/stocks-bonds-details" class="a_dashboard_tab add_width  double-word letter_spacing " next_page="stocks-bonds-details" canadd="1" tabname="stockbonddetails">Securities /<br>Mutual Funds / Bonds </a>
                    
					<a href="https://www.ezeewill.com/immovable-properties-details" class="a_dashboard_tab add_width1 remove_padding  double-word letter_spacing " next_page="immovable-properties-details" canadd="1" tabname="immovableproperties">Immovable<br>Properties</a>
                    
					<a href="https://www.ezeewill.com/retirement-plan-details" class="a_dashboard_tab  single-word letter_spacing " next_page="retirement-plan-details" canadd="1" tabname="retirementplan">Retirement Funds</a>
                    
					<a href="https://www.ezeewill.com/life-insurance-policies" class="a_dashboard_tab  double-word letter_spacing " next_page="life-insurance-policies" canadd="1" tabname="lifeinsurancepolicy">Life Insurance<br>Policies</a>
                    
					<a href="https://www.ezeewill.com/other-assets-details" class="a_dashboard_tab  single-word " next_page="other-assets-details" canadd="1" tabname="otherassets">Other Assets</a>
                    
					<a href="https://www.ezeewill.com/liability-details" class="a_dashboard_tab  single-word letter_spacing " next_page="liability-details" canadd="1" tabname="liability">Liabilities</a>
                    
					<a href="https://www.ezeewill.com/non-beneficiaries" class="a_dashboard_tab add_width  single-word " next_page="non-beneficiaries" canadd="1" tabname="nonbeneficiary">Non-Beneficiaries</a>
                                        <a href="https://www.ezeewill.com/contingency-special-clause" class=" a_dashboard_tab  double-word1 add_width1 remove_padding letter_spacing " next_page="contingency-special-clause" canadd="1" tabname="contigency">Contingent Beneficiary / Special Instructions</a>
                </div></div>
        </div><!-- gen-row END -->
    
        <style type="text/css">
            label {font-weight:400; color:#777 !important;}
        </style>
        <div class="agreement-row">
            <div class="container">
                                <div class="row">
					                    <div class="col-xs-12">
                        <div id="accordion" class="panel-group dashboard-accordion"><div class="panel panel-default">
                                                                                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#PersonalInformation" data-parent="#One" id="one" data-toggle="collapse" class="collapsed"><strong>Personal Information</strong></a>
                                        <div align="right"><a href="dashboard.php">Edit</a></div></span>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse in" id="PersonalInformation" style="height: auto;">
                                    <div class="panel-body">
                                        <div class="">
                                            <div class="col-xs-12">
                                                <div class="" id="div_personal_info">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label class="mandatory">Full Name of the Testator</label>
                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    Mr. Arpit Kumar Basnet                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6">
                                                                                                                <label class="mandatory">Father's Name</label>
                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    Mr. Name  Father                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- row END -->
                                                    <div class="divider"></div>
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <label class="cnd-mandatory">Date of Birth</label>
                                                            26/03/1997                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label>Marital Status</label>
                                                            Single                                                        </div>
                                                        <div class="col-xs-3">
                                                                                                                </div>
                                                    </div><br>
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <label>Gender</label>
                                                                Male                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label class="cnd-mandatory">Adult</label>
                                                            Adult                                                        </div>
                                                    </div>
                                                </div><!-- panel-body END -->
                                                <div class="divider">&nbsp;</div>
                                                <p><strong>Permanent Address</strong></p>
                                                <div class="" id="div_permanent_address">
                                                    <center><p class="error" id="error2"></p></center>  
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <label class="mandatory">Is this Address a Foreign Address?</label>
                                                            No                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label class="mandatory">Address Line1</label>
                                                            House no 431/1                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label class="mandatory">Address Line2</label>
                                                            Adarsh nagar                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label>Address Line3</label>
                                                            <center>-</center>                                                        </div>
                                                    </div>
                                                    <div class="divider"></div>
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <label>City / Village / Town</label>
                                                            Gurgaon                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label class="mandatory">Pincode / Zipcode</label>
                                                            122001                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label class="mandatory">Country</label>
                                                            India                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label class="mandatory">State</label>
                                                            <span id="span_permanent_state">
                                                              Haryana                                                            </span>
                                                        </div>
                                                                                                            </div>
                                                </div><!-- Permanent Address END -->

                                                <div class="divider"></div>
                                                <p><strong>Correspondence  Address</strong></p>
                                                <div class="">
                                                <center><p class="error" id="error3"></p></center>  
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <label>Is this Address a Foreign Address?</label>
                                                            No                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label class="mandatory">Address Line1</label>
                                                            House no 431/1                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label class="mandatory">Address Line2</label>
                                                            Adarsh nagar                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label>Address Line3</label>
                                                            <center>-</center>                                                        </div>
                                                    </div>
                                                    <div class="divider"></div>
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <label>City / Village / Town</label>
                                                            Gurgaon                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label class="mandatory">Pincode / Zipcode</label>
                                                            122001                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label class="mandatory">Country</label>
                                                            India                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label class="mandatory">State</label>
                                                            <span id="span_correspondence_state">
                                                               Haryana                                                            </span>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <span id="span_correspondence_state_other" style="display:none;" >
                                                                <label class="cnd-mandatory">Other State</label>
                                                                                                                            </span>
                                                            
                                                        </div>
                                                    </div>
                                                </div><!-- Permanent Address END -->
                                                <div class="divider">&nbsp;</div>
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-xs-4">
                                                             <label class="mandatory">Telephone Number</label>
                                                            <div id="TelephoneAddWrap">
                                                                                                                                <div class="row">
                                                                                                                                                <div class="col-xs-9">
                                                                            +91--1234567890                                                                        </div>
                                                                    </div>
                                                                                                                        </div>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <label>Office Number</label>
                                                            <div id="OfficeAddWrap">
                                                                                                                                        <div class="row">
                                                                                                                                                        <div class="col-xs-9">
                                                                                +91--222222                                                                            </div>
                                                                        </div>
                                                                                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <label class="mandatory">Mobile Number</label>
                                                            <div id="MobileAddWrap">
                                                                                                                                        <div class="row">
                                                                                                                                                        <div class="col-xs-9">
                                                                                +91-9986233758                                                                            </div>
                                                                        </div>
                                                                                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="divider"></div>
                                                    <div class="row">
                                                        <div class="col-xs-8">
                                                            <label class="mandatory">Email</label>
                                                            <div id="EmailAddWrap">
                                                                <div class="row">
                                                                    <div class="col-xs-6 last">
                                                                        basnetarpit25@gmail.com                                                                    </div>
                                                                </div>

                                                                                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="divider"></div>
                                                    <div class="row" id="div_other_details">
                                                    <center><p class="error" id="error4"></p></center>  
                                                        <div class="col-xs-2">
                                                            <label>Passport No.</label>
                                                            <center>-</center>                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label>PAN</label>
                                                            <center>-</center>                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label>Aadhaar No.</label>
                                                            <center>-</center>                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label class="mandatory">Nationality</label>
                                                            Indian                                                        </div>
                                                                                                            </div>
                                                    <div class="divider"></div>
                                                    <div class="row">
                                                         <div class="col-xs-2">
                                                            <label>Religion</label>
                                                            Hindu                                                        </div>
                                                                                                                <div class="col-xs-2">
                                                            <label>Caste</label>
                                                            <center>-</center>                                                        </div>
                                                    </div>
                                                    <div class="divider"></div>
                                                    <div class="row">
                                                        
                                                        <div class="col-xs-4">
                                                            <label class="mandatory">Date of making Will</label>
                                                            13/02/2018                                                        </div>
                                                    </div>
                                                </div><!-- panel-body END -->
                                            </div><!-- col-xs-12 END -->
                                        </div><!-- row END -->
                                    </div>
                                </div>
                            </div><!-- PersonalInformation END -->
                          
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#BeneficiaryDetails" data-parent="#Two" data-toggle="collapse"  id="two" class="collapsed"><strong>Beneficiaries</strong></a>
                                        <span><a href="beneficiary-details">Edit</a></span>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="BeneficiaryDetails" style="height: 0px;">
                                    <div class="panel-body">
                                                                            </div>
                                </div>
                            </div><!-- BeneficiaryDetails END -->
                          
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#ExecutorDetails" data-parent="#three" data-toggle="collapse" id="three" class="collapsed"><strong>Executors</strong></a>
                                        <span><a href="executor-details">Edit</a></span>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse " id="ExecutorDetails" style="height: 0px;">
                                    <div class="panel-body">
                                        Testator has not appointed any Executor.
                                    </div>
                                </div>
                            </div><!-- ExecutorDetails END -->

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#bankaccount" data-parent="#Four" data-toggle="collapse" id="four" class="collapsed"><strong>Bank Accounts</strong></a>
                                        <span><a href="bank-account-details">Edit</a></span>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="bankaccount" style="height: auto;">
                                    <div class="panel-body">
                                        <div class="rowx">
                                            <div class="col-xs-12x tab-btn">
                                                <a class="active l_bank" rec_id="bank_listing">Bank Accounts</a>
                                                <a class="l_bank" rec_id="locker_listing">Lockers</a>
                                                <a class="l_bank" rec_id="fixed_deposits_listing">FD / RD A/c</a>
                                                <a class="l_bank" rec_id="public_provident_fund_listing">Public Provident Fund (PPF)</a>
                                            </div>
                                        </div>
                                        <div id="bank_listing" >
                                                                                    </div>
                                        <div id="locker_listing" class="hide">
                                                                                    </div>
                                        <div id="fixed_deposits_listing" class="hide">
                                                                                    </div>

                                        <div id="public_provident_fund_listing" class="hide">
                                            
                                                                                    </div>
                                    </div>
                                </div>
                            </div><!-- BankInfo END -->

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a href="#stockBondInfo" data-parent="#five" data-toggle="collapse" id="five" class="collapsed"><strong>Securities / Mutual Funds / Bonds</strong></a>
                                    <span><a href="stocks-bonds-details">Edit</a></span>
                                  </h4>
                                </div>
                                <div class="panel-collapse collapse " id="stockBondInfo" style="height: auto;">
                                    <div class="panel-body">
                                        <div class="rowx">
                                            <div class="col-xs-12x tab-btn">
                                                <a class="active l_stock" rec_id="stock_bond_listing">Securities</a>
                                                <a class="l_stock" rec_id="mutual_fund_listing">Mutual Fund</a>
                                                <a class="l_stock" rec_id="bond_listing">Bond</a>
                                            </div>
                                        </div>
                                        <div id="stock_bond_listing">
                                                                                    </div>

                                        <div id="mutual_fund_listing" class="hide">
                                                                                    </div>

                                        <div id="bond_listing" class="hide">
                                                                                    </div>
                                    
                                    </div>
                                </div>
                            </div><!-- stockBondInfo END -->

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#immovablePropertyInfo" data-parent="#six" data-toggle="collapse" id="six" class="collapsed"><strong>Immovable Properties</strong></a>
                                        <span><a href="immovable-properties-details">Edit</a></span>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="immovablePropertyInfo" style="height: auto;">
                                    <div class="panel-body">
                                                                            </div>
                                </div>
                            </div><!-- immovablePropertyInfo END -->

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#retirementPlanInfo" data-parent="#seven" data-toggle="collapse" id="seven" class="collapsed"><strong>Retirement Funds</strong></a>
                                        <span><a href="retirement-plan-details">Edit</a></span>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse " id="retirementPlanInfo" style="height: auto;">
                                    <div class="panel-body">
                                        <div class="rowx">
                                            <div class="col-xs-12x tab-btn">
                                                <a class="active l_retirement" rec_id="provident_fund_listing">Provident Fund(PF)</a>
                                                <a class="l_retirement" rec_id="pension_fund_listing">Pension Fund</a>
                                            </div>
                                        </div>
                                        <div id="provident_fund_listing" >
                                                                                    </div>

                                        <div id="pension_fund_listing" class="hide">
                                                                                    </div>
                                        

                                    </div>
                                </div>
                            </div><!-- retirementPlanInfo END -->

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#LICInfo" data-parent="#eight" data-toggle="collapse"id="eight"  class="collapsed"><strong>Life Insurance Policies</strong></a>
                                        <span><a href="life-insurance-policies">Edit</a></span>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse " id="LICInfo" style="height: auto;">
                                    <div class="panel-body">
                                                                            </div>
                                </div>
                            </div><!-- LICInfo END -->





                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#otherAssetsInfo" data-parent="#nine" data-toggle="collapse" id="nine"  class="collapsed"><strong>Other Assets</strong></a>
                                        <span><a href="other-assets-details">Edit</a></span>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse " id="otherAssetsInfo" style="height: auto;">
                                    <div class="panel-body">
                                        <div class="rowx">
                                            <div class="col-xs-12x tab-btn">
                                                <a class="active l_assets" rec_id="other_assets_listing">Other Assets</a>
                                                <a class="l_assets" rec_id="digital_assets_listing">Digital Assets</a>
                                            </div>
                                        </div>
                                        <div id="other_assets_listing">
                                                                                    </div>

                                        <div id="vehicle_assets_listing" class="hide">
                                                                                    </div>

                                        <div id="digital_assets_listing" class="hide">
                                                                                    </div>

                                    </div>
                                </div>
                            </div><!-- otherAssetsInfo END -->




                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#liabilityInfo" data-parent="#ten" data-toggle="collapse" id="ten" class="collapsed"><strong>Liabilities</strong></a>
                                        <span><a href="liability-details">Edit</a></span>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="liabilityInfo" style="height: auto;">
                                    <div class="panel-body">
                                          
                                    </div>
                                </div>
                            </div><!-- liabilityInfo END -->

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#nonBeneficiariesInfo" data-parent="#eleven" data-toggle="collapse" id="eleven" class="collapsed"><strong>Non-Beneficiaries</strong></a>
                                        <span><a href="non-beneficiaries">Edit</a></span>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse" id="nonBeneficiariesInfo" style="height: auto;">
                                    <div class="panel-body">
                                                                            </div>
                                </div>
                            </div><!-- nonBeneficiariesInfo END -->

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#contingencyInfo" data-parent="#Twevle" data-toggle="collapse" id="twevle" class="collapsed"><strong>Contingent Beneficiary / Special Instructions</strong></a>
                                        <span><a href="contingency-special-clause">Edit</a></span>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse " id="contingencyInfo" style="height: auto;">
                                    <div class="panel-body">
                                        <div class="rowx">
                                            <div class="col-xs-12x tab-btn">
                                                <a class="active l_clause" rec_id="contingent_beneficiary_listing">Contingent Beneficiary</a>
                                                <a class="l_clause" rec_id="special_clause_listing">Special Instructions</a>
                                            </div>
                                        </div>

                                        <div id="contingent_beneficiary_listing">
                                                                                </div>

                                        <div id="special_clause_listing" class="hide">
                                            <div class="col-xs-12">
                                                                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- contingencyInfo END -->

                        </div><!-- accordion END -->
                        <p>&nbsp;</p><p><span class="error">Data is not ready for submission, please fill data for all mandatory fields.</span></p></div><div align="center"></div><!-- col-xs-12 END -->
                </div><!-- row END -->
                
            </div>
        </div><!-- gen-row END -->
        <form name="frmDashboardMain" method="post" class="frmCurrent">
                        <input type="hidden" name="auth_token" value="TVRBMkxqSXlNeTQwT0M0eU1UUTBOSEJ4YjJwcFltSnNOMnc0TVhKMGMzQnRjemcwYVdWeE53PT0=" >
            <input type="hidden" id="finalize_will_check" name="finalize_will" value="1">
        </form>
        <script lang="javascript" type="text/javascript" src="js/willreviews.js" ></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.tab-btn a').unbind();
                $('.l_bank').click(function(){
                    if($(this).attr('rec_id') == "bank_listing"){
                        $('#bank_listing').removeClass('hide');
                        $('#locker_listing').addClass('hide');
                        $('#fixed_deposits_listing').addClass('hide');
                        $('#public_provident_fund_listing').addClass('hide');
                        $('.l_bank').removeClass('active');
                        $(this).addClass('active');
                    }else if($(this).attr('rec_id') == "locker_listing"){
                        $('#bank_listing').addClass('hide');
                        $('#fixed_deposits_listing').addClass('hide');
                        $('#locker_listing').removeClass('hide');
                        $('#public_provident_fund_listing').addClass('hide');
                        $('.l_bank').removeClass('active');
                        $(this).addClass('active');
                    }else if($(this).attr('rec_id') == "fixed_deposits_listing"){
                        $('#bank_listing').addClass('hide');
                        $('#locker_listing').addClass('hide');
                        $('#fixed_deposits_listing').removeClass('hide');
                        $('#public_provident_fund_listing').addClass('hide');
                        $('.l_bank').removeClass('active');
                        $(this).addClass('active');
                    }else if($(this).attr('rec_id') == "public_provident_fund_listing"){
                        $('#bank_listing').addClass('hide');
                        $('#locker_listing').addClass('hide');
                        $('#fixed_deposits_listing').addClass('hide');
                        $('#public_provident_fund_listing').removeClass('hide');
                        $('.l_bank').removeClass('active');
                        $(this).addClass('active');
                    }
                });

                $('.l_stock').click(function(){
                    if($(this).attr('rec_id') == "stock_bond_listing"){
                        $('#stock_bond_listing').removeClass('hide');
                        $('#bond_listing').addClass('hide');
                        $('#mutual_fund_listing').addClass('hide');
                        $('.l_stock').removeClass('active');
                        $(this).addClass('active');
                    }else if($(this).attr('rec_id') == "mutual_fund_listing"){
                        $('#stock_bond_listing').addClass('hide');
                        $('#bond_listing').addClass('hide');
                        $('#mutual_fund_listing').removeClass('hide');
                        $('.l_stock').removeClass('active');
                        $(this).addClass('active');
                    }else if($(this).attr('rec_id') == "bond_listing"){
                        $('#mutual_fund_listing').addClass('hide');
                        $('#stock_bond_listing').addClass('hide');
                        $('#bond_listing').removeClass('hide');
                        $('.l_stock').removeClass('active');
                        $(this).addClass('active');
                    }
                });

                $('.l_retirement').click(function(){
                    if($(this).attr('rec_id') == "provident_fund_listing"){
                        $('#provident_fund_listing').removeClass('hide');
                        $('#pension_fund_listing').addClass('hide');
                        $('#public_provident_fund_listing').addClass('hide');
                        $('.l_retirement').removeClass('active');
                        $(this).addClass('active');
                    }else if($(this).attr('rec_id') == "pension_fund_listing"){
                        $('#provident_fund_listing').addClass('hide');
                        $('#public_provident_fund_listing').addClass('hide');
                        $('#pension_fund_listing').removeClass('hide');
                        $('.l_retirement').removeClass('active');
                        $(this).addClass('active');
                    }else if($(this).attr('rec_id') == "public_provident_fund_listing"){
                        $('#provident_fund_listing').addClass('hide');
                        $('#pension_fund_listing').addClass('hide');
                        $('#public_provident_fund_listing').removeClass('hide');
                        $('.l_retirement').removeClass('active');
                        $(this).addClass('active');
                    }
                });

                $('.l_assets').click(function(){
                    if($(this).attr('rec_id') == "other_assets_listing"){
                        $('#other_assets_listing').removeClass('hide');
                        $('#vehicle_assets_listing').addClass('hide');
                        $('#digital_assets_listing').addClass('hide');
                        $('.l_assets').removeClass('active');
                        $(this).addClass('active');
                    }else if($(this).attr('rec_id') == "vehicle_assets_listing"){
                        $('#other_assets_listing ').addClass('hide');
                        $('#digital_assets_listing').addClass('hide');
                        $('#vehicle_assets_listing').removeClass('hide');
                        $('.l_assets').removeClass('active');
                        $(this).addClass('active');
                    }else if($(this).attr('rec_id') == "digital_assets_listing"){
                        $('#other_assets_listing ').addClass('hide');
                        $('#vehicle_assets_listing').addClass('hide');
                        $('#digital_assets_listing').removeClass('hide');
                        $('.l_assets').removeClass('active');
                        $(this).addClass('active');
                    }
                });

                $('.l_clause').click(function(){
                    if($(this).attr('rec_id') == "contingent_beneficiary_listing"){
                        $('#contingent_beneficiary_listing').removeClass('hide');
                        $('#special_clause_listing').addClass('hide');
                        $('.l_clause').removeClass('active');
                        $(this).addClass('active');
                    }else if($(this).attr('rec_id') == "special_clause_listing"){
                        $('#contingent_beneficiary_listing ').addClass('hide');
                        $('#special_clause_listing').removeClass('hide');
                        $('.l_clause').removeClass('active');
                        $(this).addClass('active');
                    }
                });
            });


        </script>
    </div><!-- main END -->
	<footer style="padding-top:0px;">
        <div class="btm-row">
	        <div class="container">
            	<div class="row">
					<input type="hidden" id="footer_will_status" value="" />
                    <div class="col-xs-9" style="text-align:center;">
                        Copyright © 2014 EzeeWill <!--| <a next_page="https://www.ezeewill.com/privacy-policy.html" class="a_other_links" target="_parent">Privacy Policy</a> | <a next_page="https://www.ezeewill.com/terms-and-conditions.html" class="a_other_links" target="_parent">Terms of Use</a> | <a next_page="https://www.ezeewill.com/disclaimer.html" class="a_other_links" target="_parent">Disclaimer</a> | <a next_page="https://www.ezeewill.com/contact-us.html" class="a_other_links" target="_parent">Contact Us</a>-->
                    </div>
                </div>
            </div> 
        </div><!-- btm-row END -->
    </footer>    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://www.ezeewill.com/js/bootstrap.min.js"></script>
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/973014196/?guid=ON&amp;script=0"/>
  </body>
</html>
