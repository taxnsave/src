<?php
require('fpdf/fpdf.php');
include_once("../../../__lib.includes/config.inc.php");

$name = $_POST['pi_name'];
$place = $_POST['pi_place'];
$date = $_POST['pi_date'];

    $pi_data = $willProfile->getWillDetails('will_personal_information');
    $bene_data = $willProfile->getWillDetails('will_beneficiary');
    $bene_data_all = $willProfile->getWillBenDetails('will_beneficiary');
    $bene_data_forguardion = $willProfile->getWillBenDetails('will_beneficiary');
	$exe_data = $willProfile->getWillDetails('will_executor');
    $wip_data = $willProfile->getWillDetails('will_immovable_properties');
    $wba_data = $willProfile->getWillDetails('will_bank_accounts');
    $wli_data = $willProfile->getWillDetails('will_locker_info');
    $wfd_data = $willProfile->getWillDetails('will_fixed_deposit');
    $wmf_data = $willProfile->getWillDetails('will_mutual_fund');
    $wgi_data = $willProfile->getWillDetails('will_general_insurance');
    $wshare_data = $willProfile->getWillDetails('will_share');
    $wbd_data = $willProfile->getWillDetails('will_bond_details');
    $wjewel_data = $willProfile->getWillDetails('will_jewellery');
    $wrf_data = $willProfile->getWillDetails('will_retirement_funds');
    $wlic_data = $willProfile->getWillDetails('will_lic');
    $wvech_data =$willProfile->getWillDetails('will_vechile');
    $pa_data = $willProfile->getWillDetails('will_pet_animal');
    $woasset_data = $willProfile->getWillDetails('will_other_assets');
    $wbact_data = $willProfile->getWillDetails('will_business');
    $wda_data = $willProfile->getWillDetails('will_digital_assets');
    $wbo_data = $willProfile->getWillDetails('will_body_organ');
    $wls_data = $willProfile->getWillDetails('will_liabilities');
    $wal_data = $willProfile->getWillDetails('will_add_liabilities');
    $wcust_data = $willProfile->getWillDetails('will_custodian');
    $wwit_data = $willProfile->getWillDetails('will_witness');

//encryption functions
if(function_exists('openssl_encrypt'))
{
    function RC4($key, $data)
    {
        return openssl_encrypt($data, 'RC4-40', $key, OPENSSL_RAW_DATA);
    }
}
elseif(function_exists('mcrypt_encrypt'))
{
    function RC4($key, $data)
    {
        return @mcrypt_encrypt(MCRYPT_ARCFOUR, $key, $data, MCRYPT_MODE_STREAM, '');
    }
}
else
{
    function RC4($key, $data)
    {
        static $last_key, $last_state;

        if($key != $last_key)
        {
            $k = str_repeat($key, 256/strlen($key)+1);
            $state = range(0, 255);
            $j = 0;
            for ($i=0; $i<256; $i++){
                $t = $state[$i];
                $j = ($j + $t + ord($k[$i])) % 256;
                $state[$i] = $state[$j];
                $state[$j] = $t;
            }
            $last_key = $key;
            $last_state = $state;
        }
        else
            $state = $last_state;

        $len = strlen($data);
        $a = 0;
        $b = 0;
        $out = '';
        for ($i=0; $i<$len; $i++){
            $a = ($a+1) % 256;
            $t = $state[$a];
            $b = ($b+$t) % 256;
            $state[$a] = $state[$b];
            $state[$b] = $t;
            $k = $state[($state[$a]+$state[$b]) % 256];
            $out .= chr(ord($data[$i]) ^ $k);
        }
        return $out;
    }
}

    
class PDF extends FPDF
{
    protected $encrypted = false;  //whether document is protected
    protected $Uvalue;             //U entry in pdf document
    protected $Ovalue;             //O entry in pdf document
    protected $Pvalue;             //P entry in pdf document
    protected $enc_obj_id;         //encryption object id
    
    function SetProtection($permissions=array(), $user_pass='', $owner_pass=null)
    {
        $options = array('print' => 4, 'modify' => 8, 'copy' => 16, 'annot-forms' => 32 );
        $protection = 192;
        foreach($permissions as $permission)
        {
            if (!isset($options[$permission]))
                $this->Error('Incorrect permission: '.$permission);
            $protection += $options[$permission];
        }
        if ($owner_pass === null)
            $owner_pass = uniqid(rand());
        $this->encrypted = true;
        $this->padding = "\x28\xBF\x4E\x5E\x4E\x75\x8A\x41\x64\x00\x4E\x56\xFF\xFA\x01\x08".
                        "\x2E\x2E\x00\xB6\xD0\x68\x3E\x80\x2F\x0C\xA9\xFE\x64\x53\x69\x7A";
        $this->_generateencryptionkey($user_pass, $owner_pass, $protection);
    }
    
    function _putstream($s)
    {
        if ($this->encrypted)
            $s = RC4($this->_objectkey($this->n), $s);
        parent::_putstream($s);
    }

    function _textstring($s)
    {
        if (!$this->_isascii($s))
            $s = $this->_UTF8toUTF16($s);
        if ($this->encrypted)
            $s = RC4($this->_objectkey($this->n), $s);
        return '('.$this->_escape($s).')';
    }

    /**
    * Compute key depending on object number where the encrypted data is stored
    */
    function _objectkey($n)
    {
        return substr($this->_md5_16($this->encryption_key.pack('VXxx',$n)),0,10);
    }

    function _putresources()
    {
        parent::_putresources();
        if ($this->encrypted) {
            $this->_newobj();
            $this->enc_obj_id = $this->n;
            $this->_put('<<');
            $this->_putencryption();
            $this->_put('>>');
            $this->_put('endobj');
        }
    }

    function _putencryption()
    {
        $this->_put('/Filter /Standard');
        $this->_put('/V 1');
        $this->_put('/R 2');
        $this->_put('/O ('.$this->_escape($this->Ovalue).')');
        $this->_put('/U ('.$this->_escape($this->Uvalue).')');
        $this->_put('/P '.$this->Pvalue);
    }

    function _puttrailer()
    {
        parent::_puttrailer();
        if ($this->encrypted) {
            $this->_put('/Encrypt '.$this->enc_obj_id.' 0 R');
            $this->_put('/ID [()()]');
        }
    }

    /**
    * Get MD5 as binary string
    */
    function _md5_16($string)
    {
        return md5($string, true);
    }

    /**
    * Compute O value
    */
    function _Ovalue($user_pass, $owner_pass)
    {
        $tmp = $this->_md5_16($owner_pass);
        $owner_RC4_key = substr($tmp,0,5);
        return RC4($owner_RC4_key, $user_pass);
    }

    /**
    * Compute U value
    */
    function _Uvalue()
    {
        return RC4($this->encryption_key, $this->padding);
    }

    /**
    * Compute encryption key
    */
    function _generateencryptionkey($user_pass, $owner_pass, $protection)
    {
        // Pad passwords
        $user_pass = substr($user_pass.$this->padding,0,32);
        $owner_pass = substr($owner_pass.$this->padding,0,32);
        // Compute O value
        $this->Ovalue = $this->_Ovalue($user_pass,$owner_pass);
        // Compute encyption key
        $tmp = $this->_md5_16($user_pass.$this->Ovalue.chr($protection)."\xFF\xFF\xFF");
        $this->encryption_key = substr($tmp,0,5);
        // Compute U value
        $this->Uvalue = $this->_Uvalue();
        // Compute P value
        $this->Pvalue = -(($protection^255)+1);
    }
    // Page header
    function Header()
    {
        $name = $GLOBALS['name'];
        $place = $GLOBALS['place'];
        $date = $GLOBALS['date'];

        $this->SetFont('Arial','B',8);
        // Move to the right
        $this->Cell(80);
        $headertext = "This last will of ".$name." executed at ".$place." on ".$date;
        $this->Cell(30,10,$headertext,0,0,'C');
        // Line break
        $this->Ln(20);
    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-10);
        // Arial italic 8
        $this->SetFont('Arial','B',8);

        $footertext = "[Full sign of Testator]                              [Full sign of Witness]                                    [Full sign of Witness]                 Page ".$this->PageNo()."/{nb}";

        $this->SetY(-10);
        $this->Cell(0,10,$footertext,0,0,'C');
    }
}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->SetProtection(array('print'));
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);
$pdf->SetAutoPageBreak(true, 40);

print_contents($pdf);

$pdf->Output('WillPDF.pdf','I');


function print_contents($pdf) {
    global $pi_data,$bene_data,$bene_data_all,$exe_data,$wip_data,$wba_data,$wli_data,$wfd_data,$wmf_data,$wgi_data,$wshare_data,$wbd_data,$wjewel_data,$wrf_data,$wlic_data,$wvech_data,$pa_data,$woasset_data,$wda_data,$wbo_data,$wls_data,$wal_data,$wcust_data,$wwit_data;     

    $pdf->Cell(0,5,' ',0,1);

    $pdf->SetFont('Times','B',16);
    $content = "LAST WILL AND TESTAMENT of ".$pi_data->pi_f_name."";
        $pdf->Cell(0,50,$content,0,5,'C');
        
    $pdf->SetFont('Times','',12);
        
    $content="A. PERSONAL PROFILE";
    $pdf->Cell(0,10,$content,0,1);  
    
    $relaltion = "son/daughter/wife";
    if(($pi_data->pi_gd_rel == "F")||($pi_data->pi_gd_rel == "M")) {
        if($pi_data->pi_gender == "1") {
            $relaltion = "son";    
        } else if($pi_data->pi_gender == "2") {
            $relaltion = "daughter";
        } 
    } else if(($pi_data->pi_gd_rel == "S")) {
        if($pi_data->pi_gender == "1") {
            $relaltion = "husband";    
        } else if($pi_data->pi_gender == "2"){
            $relaltion = "wife";
        }
    }
    
    $content = "I, ".$pi_data->pi_f_name." ".$pi_data->pi_m_name." ".$pi_data->pi_l_name. " " . $relaltion . " of ".$pi_data->pi_gd_f_name." ".$pi_data->pi_gd_mid_name." ".$pi_data->pi_gd_l_name." , currently residing at ".$pi_data->pi_per_addr_line1." ".$pi_data->pi_per_addr_line2." ".$pi_data->pi_per_addr_line3." ".$pi_data->pi_per_city." ".getState($pi_data->pi_per_state)." ".getCountry($pi_data->pi_per_country)." ".$pi_data->pi_per_zip.", by religion ". getReligion($pi_data->pi_religion).",".$pi_data->nationality." by nationality, aged about ".$pi_data->pi_per_info_age.", having PAN No./Aadhar No ".$pi_data->pi_panaadharnum." / ".$pi_data->pi_panaadharnumper." do hereby revoke all my previous Wills and declare that this is my last Will, which I make on this".$pi_data->pi_date."";    
    //$pdf->Write(5,$content); 
    $pdf->MultiCell(0,5,$content); 
    
    
    $content = "\n\nI declare that I am writing this Will out of my free volition and am solely making this independent decision without any coercion or  under any undue influence whatsoever and I maintain good health & possess a sound mind.";
    $pdf->MultiCell(0,5,$content); 

    $content="\n\n\nB. FAMILY DESCRIPTION/BENEFICIARIES\n\n";
    $pdf->MultiCell(0,5,$content); 
    
    $num = 1;
    while($row = mysql_fetch_array($bene_data_all)) {
        $content = $num . ". " . generateBenDatatoPrint($row);
        $pdf->MultiCell(0,5,$content);
        $num +=1;
    }
       
    $content="\n\n\nC.	APPOINTMENT OF EXECUTOR\n\n";
    $pdf->MultiCell(0,5,$content); 
    $content = "I hereby appoint ".$exe_data->exe_f_name." ".$exe_data->exe_m_name." ".$exe_data->exe_l_name.", " . getRelationship($exe_data->exe_rel_with_tes) . " of ".$exe_data->exe_fat_f_name." ".$exe_data->exe_fat_m_name." ".$exe_data->exe_fat_l_name." ".getReligion($exe_data->exe_religious)." by religion, ".$exe_data->exe_nationality." by nationality, aged about ".$exe_data->exe_age." , having occupation ".$exe_data->exe_occupation." currently residing at ".$exe_data->exe_per_addr_line1." ".$exe_data->exe_per_addr_line2." ".$exe_data->exe_per_addr_ine3." ".$exe_data->exe_per_city." ".getState($exe_data->exe_per_state)." ".getCountry($exe_data->exe_per_country)." ".$exe_data->exe_per_zip." to be the Sole Executor of this Will\n\n";
    $pdf->MultiCell(0,5,$content); 
    
    $content = $exe_data->exe_f_name." ".$exe_data->exe_m_name." ".$exe_data->exe_l_name." is ".getRelationship($exe_data->exe_rel_with_tes)."   of ".$pi_data->pi_f_name." ".$pi_data->pi_m_name." ".$pi_data->pi_l_name."\n\n";
    $pdf->MultiCell(0,5,$content); 
    
    //$content = "In the event of death of ".$exe_data->exe_f_name." ".$exe_data->exe_m_name." ".$exe_data->exe_l_name." before me, then in such case ., Son/ Daughter of ..............., by religion.., ................... by nationality, aged about ., having occupation ............ currently residing at ....................will be the executor of this Will.\n\n";
    //$pdf->MultiCell(0,5,$content); 
    //$content = "........................ is ................. of ".$pi_data->pi_f_name." ".$pi_data->pi_m_name." ".$pi_data->pi_l_name."";
    //$pdf->MultiCell(0,5,$content); 

    $content="\n\n\nD. GUARDIAN DETAILS\n\n";
    $pdf->MultiCell(0,5,$content);
    
    $gaurdianheading = false;
    $num = 1;
    while($row = mysql_fetch_array($bene_data_forguardion)) {
        if($row['fname']!="") {            
            $content = $num . ". " . generateBenGuardianDatatoPrint($row);
            $pdf->MultiCell(0,5,$content);
            $gaurdianheading = true;
            }
        $num +=1;
    }
    
    if($gaurdianheading == false) {
        $content="Not Applicable";
        $pdf->MultiCell(0,5,$content);
    }
    
    $content="\n\n\nE. DISTRIBUTION OF ASSETS\n\n";
    $pdf->MultiCell(0,5,$content); 
    $content = "Presently, I have the following assets consisting of both immovable and movable properties ,  and I hereby give, leave and bequeath the following legacies to be effected out of my assets after the event of my death, viz;";
    $pdf->MultiCell(0,5,$content); 
    $content="\n\n\nIMMOVABLE PROPERTY\n\n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="a. My ".$wip_data->prop_type." land  measuring ".$wip_data->prop_mes." located at " .$wip_data->prop_addr_line1." ". $wip_data->prop_addr_line2." ".$wip_data->prop_addr_line3." ".$wip_data->prop_city." ".getState($wip_data->prop_state)." ".getCountry($wip_data->prop_country)." ".$wip_data->prop_zipcode." having ". $wip_data->prop_type." ownership to be given in favour of my ............, named ............ \nOR\n";
    $pdf->MultiCell(0,5,$content); 
        
    $content="b. ............... of my agriculture land measuring ..........................  located at .................. having .............. ownership to be given in favour of my............, named..............";
    $pdf->MultiCell(0,5,$content); 
    $content="a. My non-agriculture land measuring .......................... located at .................. having .............. ownership to be given in favour of my............, named..............\nOR\n";
    $pdf->MultiCell(0,5,$content); 
    $content="b. ............... of my non- agriculture land measuring .......................... located at .................. having .............. ownership to be given in favour of my............, named..............";
    $pdf->MultiCell(0,5,$content); 
    $content="a. My commercial building of ............ located at .................. having .............. ownership to be given in favour of my............, named.............. \nOR\n";
    $pdf->MultiCell(0,5,$content); 
    $content="b. ............... of my commercial building  of .............located at .................. having .............. ownership to be given in favour of my............, named..............";
    $pdf->MultiCell(0,5,$content); 
    $content="a. My residential building  of ............located at .................. having .............. ownership to be given in favour of my............, named..............\nOR\n";
    $pdf->MultiCell(0,5,$content); 
    $content="b. ............... of my residential building of .......... located at .................. having .............. ownership to be given in favour of my............, named..............";    
    $pdf->MultiCell(0,5,$content); 
    $content="a. I, holding ....................... Factory including/or Plant and Machinery located at .................. having .............. ownership to be given in favour of my............, named..............\nOR\n";
    $pdf->MultiCell(0,5,$content); 
    $content="b. ............... of my Factory including/or Plant and Machinery located at ..................  having ..............  ownership  to be given in favour of my............, named..............";
    $pdf->MultiCell(0,5,$content); 

    $content="\n\nANY OTHER IMMOVABLE PROPERTIES NOT MENTIONED ABOVE:\n";
    $pdf->MultiCell(0,5,$content); 

    $content="I holding ". $wip_data->prop_type." having ". $wip_data->prop_own_status." ownership located at ".$wip_data->prop_addr_line1." ".$wip_data->prop_addr_line2." ".$wip_data->prop_addr_line3." ".$wip_data->prop_city." ".getState($wip_data->prop_state)." ".getCountry($wip_data->prop_country)." ".$wip_data->prop_zipcode." to be given in favour of my................  named................\nOR\n";
    $pdf->MultiCell(0,5,$content); 
    $content="........... of my  ................... having .............ownership located at ..................  held under my name to be given in favour of my............, named..............";
    $pdf->MultiCell(0,5,$content); 
    
    $content="\n\n\nMOVABLE PROPERTY\n\n";
    $pdf->MultiCell(0,5,$content); 
    
    $content = "a. My " . $wba_data->bnk_acc_type." bank account  having account number " . $wba_data->bnk_acc_no." maintained with  " . $wba_data->bnk_bank_name." , ". $wba_data->bnk_branch_name." branch located at  " . $wba_data->bnk_acc_addr_line1." ". $wba_data->bnk_acc_addr_line2." ". $wba_data->bnk_acc_addr_line3." ". $wba_data->bnk_acc_city." ". getState($wba_data->bnk_acc_state)." ". getCountry($wba_data->bnk_acc_country)." ". $wba_data->bnk_acc_zip." to be given in favour of my............, named..............\nOR\n";
    $pdf->MultiCell(0,5,$content); 
    
    $content = "b. ...Share... of my  " . $wba_data->bnk_acc_type."  bank account  having account number  " .$wba_data->bnk_acc_no."  maintained with  " . $wba_data->bnk_bank_name." , ". $wba_data->bnk_branch_name." branch located at  " . $wba_data->bnk_acc_addr_line1." ". $wba_data->bnk_acc_addr_line2." ". $wba_data->bnk_acc_addr_line3." ". $wba_data->bnk_acc_city." ". getState($wba_data->bnk_acc_state)." ". getCountry($wba_data->bnk_acc_country)." ". $wba_data->bnk_acc_zip . " to be given in favour of my............, named..............\n";
    $pdf->MultiCell(0,5,$content); 
    
    $content = "a. The contents of Bank Locker no  " . $wli_data->locker_number." ,  maintained with  " .$wli_data->locker_bank_name." bank  " . $wli_data->locker_branch_name."  branch, located at  " . $wli_data->locker_addr_line1." ". $wli_data->locker_addr_line2." ". $wli_data->locker_addr_line3." ". $wli_data->locker_city." ". getState($wli_data->locker_state)." ". getCountry($wli_data->locker_country)." ". $wli_data->locker_zipcode." to be given in favour of my  ............ , named ............\nOR\n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="b. ...share... of the contents of Bank Locker no  " . $wli_data->locker_number." , maintained with  " . $wli_data->locker_bank_name."  bank  " . $wli_data->locker_addr_line1." ". $wli_data->locker_addr_line2." ". $wli_data->locker_addr_line3." ". $wli_data->locker_city." ". getState($wli_data->locker_state)." ". getCountry($wli_data->locker_country)." ". $wli_data->locker_zipcode." branch, to be given in favour of my  ............ , named ............ \n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="a. My Fixed Deposits maintained with  " . $wfd_data->fd_bank_name." , ". $wfd_data->fd_branch_name." ,  branch, situated at  " . $wfd_data->fd_addr_line1." ". $wfd_data->fd_addr_line2." ". $wfd_data->fd_addr_line3." ". $wfd_data->fd_city." ". getState($wfd_data->fd_state)." ". getCountry($wfd_data->fd_country)." ". $wfd_data->fd_zipcode."  bearing FD Account number  " . $wfd_data->fd_account_number." ,  to be given in favour of my............, named.............\nOR\n ";
    $pdf->MultiCell(0,5,$content); 
    
    $content="b. ...Share... of my Fixed Deposits maintained with  " . $wfd_data->fd_bank_name." , ". $wfd_data->fd_branch_name." ,  branch, situated at  " . $wfd_data->fd_addr_line1." ". $wfd_data->fd_addr_line2." ". $wfd_data->fd_addr_line3." ". $wfd_data->fd_city." ". getState($wfd_data->fd_state)." ". getCountry($wfd_data->fd_country)." ". $wfd_data->fd_zipcode."  bearing FD Account number  " . $wfd_data->fd_account_number." ,  to be given in favour of my  ............, named  ............  \n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="a. My Mutual Fund investments amounting to Rs.  " . $wmf_data->mutl_amount." (Rupees ............) of various fund houses  " . $wmf_data->mutl_comp_name."  to be given in favour of my ............, named  ............ \nOR\n ";
    $pdf->MultiCell(0,5,$content); 
    
    $content="b. ...Share... of my Mutual Fund investments amounting to Rs.  " . $wmf_data->mutl_amount." (Rupees ............) of various fund houses  " . $wmf_data->mutl_comp_name."  to be given in favour of my  ............, named  ............  \n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="a. The proceeds of my " . $wgi_data->gi_pol_type." insurance policy holding policy number.  " . $wgi_data->gi_com_name."  from  " . $wgi_data->gi_start_date."  to  " . $wgi_data->gi_mat_date."  of  " . $wgi_data->gi_amount."   to be given in favour of my .............., named ..............\nOR\n ";
    $pdf->MultiCell(0,5,$content); 
    
    $content="b. ...Share... of the proceeds of my  " . $wgi_data->gi_pol_type."  insurance policy holding policy number  " . $wgi_data->gi_com_name." from  " . $wgi_data->gi_start_date."  to  " . $wgi_data->gi_mat_date."  of  " . $wgi_data->gi_amount."   to be given in favour of my .............., named .............. \n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="a. My Investments in securities amounting to Rs.  " . $wshare_data->share_amount." (Rupees ............) of various entities held in Demat form having Account number  " . $wshare_data->share_demat_num."  with  " . $wshare_data->share_bank_name."  to be given in favour of my .............., named ..............\nOR\n ";
    $pdf->MultiCell(0,5,$content); 
    
    $content="b. ...Share... of  my Investments in securities  amounting to Rs.  " . $wshare_data->share_amount." (Rupees ............) of various entities held under my name in Demat form having Account number  " . $wshare_data->share_demat_num."  with  " . $wshare_data->share_bank_name."   to be given in favour of my .............., named ..............  \n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="a. My Investments in bonds amounting to Rs.  " . $wbd_data->bond_amount." (Rupees ............) of various entities held in  Demat form Account number  " . $wbd_data->bond_demat_no."  under  " . $wbd_data->bond_scheme_name."  scheme with  " . $wbd_data->bond_bank_name."  to be given in favour of my .............., named .............. \nOR\n ";
    $pdf->MultiCell(0,5,$content); 
    
    $content="b. ...Share... of my Investments in bonds amounting to Rs.  " . $wbd_data->bond_amount." (Rupees ............) of various entities held under my name in  Demat form Account number  " . $wbd_data->bond_demat_no."  under  " . $wbd_data->bond_scheme_name."  scheme with  " . $wbd_data->bond_bank_name."  to be given in favour of my .............., named ..............  \n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="a. I own gold/ silver/diamond/other items " . $wjewel_data->jwl_type." ,". $wjewel_data->jwl_name." ,". $wjewel_data->jwl_desc." of Rs. " . $wjewel_data->jwl_amount." (Rupees ............) which I wish to give in favour of my .............., named ..............  \nOR\n";
    $pdf->MultiCell(0,5,$content); 
    
	$content="b. ...Share... of  gold/ silver/diamond/other items  " . $wjewel_data->jwl_type." ,". $wjewel_data->jwl_name." ,". $wjewel_data->jwl_desc." of Rs.  " . $wjewel_data->jwl_amount." (Rupees ............) owned by me which I wish to give in favour of my .............., named .............. \nOR\n ";
	$pdf->MultiCell(0,5,$content); 
	
	$content="c. Gold/ silver/diamond/other item  " . $wjewel_data->jwl_type." ,". $wjewel_data->jwl_name." ,". $wjewel_data->jwl_desc."  owned by me which I wish to give in favour of my .............., named .............. \n"; 
	$pdf->MultiCell(0,5,$content); 
	
	$content="a. I holding Provident Fund under account number " . $wrf_data->pf_account_number." with " . $wrf_data->pf_company_name." having registered office at " . $wrf_data->pf_addr_line1." ". $wrf_data->pf_addr_line2." ". $wrf_data->pf_addr_line3." ". $wrf_data->pf_city." ". getState($wrf_data->pf_state)." ". getCountry($wrf_data->pf_country)." ". $wrf_data->pf_zipcode." to be given in favour of my............, named.............. and/or my .............., named..................\nOR\n ";
	$pdf->MultiCell(0,5,$content); 
	
	$content="b. ...share... of my Provident Fund under account number " . $wrf_data->pf_account_number." with " . $wrf_data->pf_company_name." having registered office at " . $wrf_data->pf_addr_line1." ". $wrf_data->pf_addr_line2." ". $wrf_data->pf_addr_line3." ". $wrf_data->pf_city." ". getState($wrf_data->pf_state)." ". getCountry($wrf_data->pf_country)." ". $wrf_data->pf_zipcode." to be given in favour of my............, named.............. and/or my .............., named.................. \n"; 
	$pdf->MultiCell(0,5,$content); 
	
	$content="a. I holding Pension fund under account number " . $wpenf_data->pen_acc_num." with " . $wpenf_data->pen_com_name." having registered office at " . $wpenf_data->pen_addr_line1." ". $wpenf_data->pen_addr_line2." ". $wpenf_data->pen_addr_line3." ". $wpenf_data->pen_city." ". getState($wpenf_data->pen_state)." ". getCountry($wpenf_data->pen_country)." ". $wpenf_data->pen_zipcode." to be given in favour of my............, named.............. and/or my .............., named.................. \nOR\n ";
	$pdf->MultiCell(0,5,$content); 
	
	$content="b. ...share... of my Pension fund under account number " . $wpenf_data->pen_acc_num." with " . $wpenf_data->pen_com_name." having registered office at " . $wpenf_data->pen_addr_line1." ". $wpenf_data->pen_addr_line2." ". $wpenf_data->pen_addr_line3." ". $wpenf_data->pen_city." ". getState($wpenf_data->pen_state)." ". getCountry($wpenf_data->pen_country)." ". $wpenf_data->pen_zipcode." to be given in favour of my............, named.............. and/or my .............., named..................\n"; 
	$pdf->MultiCell(0,5,$content); 
	
	$content="a. I holding Gratuity under account number  " . $wgrat_data->gra_acc_num."  with  " . $wgrat_data->gra_com_name."  having registered office at  " . $wgrat_data->gra_addr_line1." ". $wgrat_data->gra_addr_line2." ". $wgrat_data->gra_addr_line3." ". $wgrat_data->gra_city." ". getState($wgrat_data->gra_state)." ". getCountry($wgrat_data->gra_country)." ". $wgrat_data->gra_zipcode."  to be  given in favour of my............, named.............. and/or my .............., named.................. \nOR\n ";
	$pdf->MultiCell(0,5,$content); 
	
	$content="b. ...Share... of my Gratuity held under my name under account number  " . $wgrat_data->gra_acc_num."  with  " . $wgrat_data->gra_com_name."  having registered office at  " . $wgrat_data->gra_addr_line1." ". $wgrat_data->gra_addr_line2." ". $wgrat_data->gra_addr_line3." ". $wgrat_data->gra_city." ". getState($wgrat_data->gra_state)." ". getCountry($wgrat_data->gra_country)." ". $wgrat_data->gra_zipcode."   to be given in favour of my .............., named ..............\n";  
	$pdf->MultiCell(0,5,$content); 
	
	$content="a. I holding Life Insurance Policy having policy number  " . $wlic_data->policy_number."  of Rs.  " . $wlic_data->sum_assured." (Rupees ................) issued by  " . $wlic_data->lic_company_name." ,". $wlic_data->lic_branch_name."  branch, to be given in favour of my .............., named ..............  \nOR\n ";
	$pdf->MultiCell(0,5,$content); 
	
	$content="b. ...Share... of my Life Insurance Policy having policy number  " . $wlic_data->policy_number."  Rs.  " . $wlic_data->sum_assured." (Rupees ................) held under my name issued by  " . $wlic_data->lic_company_name." ,". $wlic_data->lic_branch_name."  branch, in favour of my .............., named .............. \n";
	$pdf->MultiCell(0,5,$content); 
	
	$content= $wvech_data->vechile_name."  having registration no  " . $wvech_data->vechile_reg_num."  in  " . $wvech_data->vechile_color."  colour  registered under my name to be given in favour of my .............., named .............. \n";
	$pdf->MultiCell(0,5,$content); 
    
	$content="\n\n\nANY OTHER MOVABLE PROPERTIES NOT MENTIONED ABOVE:\n\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="I holding  ".$woasset_data->oasset_name."   having  ".$woasset_data->oasset_own_type."   ownership located at  ".$woasset_data->oasset_addr_line1." ".$woasset_data->oasset_addr_line2." ".$woasset_data->oasset_addr_line3." ".$woasset_data->oasset_city." ".getState($woasset_data->oasset_state)." ".getCountry($woasset_data->oasset_country)." ".$woasset_data->oasset_zipcode."in favour of my................  named................\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="...Share... of  ".$woasset_data->oasset_name."   having  ".$woasset_data->oasset_own_type."   ownership under my name located at  ".$woasset_data->oasset_addr_line1." ".$woasset_data->oasset_addr_line2." ".$woasset_data->oasset_addr_line3." ".$woasset_data->oasset_city." ".getState($woasset_data->oasset_state)." ".getCountry($woasset_data->oasset_country)." ".$woasset_data->oasset_zipcode." to be given in favour of my............, named.............. \nOR\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="I carrying on business of  ".$wbact_data->bus_type."   having  ".$wbact_data->bus_own_type."   ownership under the name and style of  ".$wbact_data->bus_com_name."   located at  ".$wbact_data->bus_addr_line1." ".$wbact_data->bus_addr_line2." ".$wbact_data->bus_addr_line3." ".$wbact_data->bus_city." ".getState($wbact_data->bus_state)." ".getCountry($wbact_data->bus_country)." ".$wbact_data->bus_zipcode." to be given in favour of my............, named.............. \nOR\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="...Share...of business of  ".$wbact_data->bus_type."   carried on by me having  ".$wbact_data->bus_own_type."   ownership under the same and style of  ".$wbact_data->bus_com_name."   located at  ".$wbact_data->bus_addr_line1." ".$wbact_data->bus_addr_line2." ".$wbact_data->bus_addr_line3." ".$wbact_data->bus_city." ".getState($wbact_data->bus_state)." ".getCountry($wbact_data->bus_country)." ".$wbact_data->bus_zipcode." to be given in favour of my............, named............\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="PET ANIMALS\n";
	$pdf->MultiCell(0,5,$content); 
	
    $content="I, having  ".$pa_data->pa_type."   named  ".$pa_data->pa_name."   currently residing with me which I wish to give to my .............. named ................ for taking its proper care after the event of my death.\n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="F. DISTRIBUTION OF ESTATE (REST AND RESIDUAL PROPERTIES)\n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="a.  .......................... owned by me after the event of making this last Will on.......... to be given   in favour of my............, named.............. after the event of my death.\nOR\n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="b.   .......... of .......................... owned by me after the event of making this last Will on .................. to be given in favour of my............, named.............. and/or my .............., named................... after the event of my death.\n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="G. INTELLECTUAL PROPERTY RIGHTS\n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="a. The proceeds of  ".$wda_data->dig_asset_type."   of  ".$wda_data->dig_asset_amount."   which is held under my name, I wish to give in favour of my............, named..............\nOR\n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="b. ...Share... of the proceeds of  ".$wda_data->dig_asset_type."   of  ".$wda_data->dig_asset_amount."   which is held under my name, I wish to give in favour of my............, named.............. and/or my .............., named...................\n";
    $pdf->MultiCell(0,5,$content); 
    
    $content="H.	DONATION OF BODY ORGAN\n";
    $pdf->MultiCell(0,5,$content); 
    
	$content="I  ".$pi_data->pi_f_name." ".$pi_data->pi_m_name." ".$pi_data->pi_l_name." out of my free volition and without any coercion or under any undue influence whatsoever wish to donate my body organ  ".$wbo_data->borgan_name."  to  ".$wbo_data->borgan_hos_name." located at  ".$wbo_data->borgan_addr_line1." ".$wbo_data->borgan_addr_line2." ".$wbo_data->borgan_addr_line3." ".$wbo_data->borgan_city." ".getState($wbo_data->borgan_state)." ".getCountry($wbo_data->borgan_country)." ".$wbo_data->borgan_zipcode."\nI hereby declare that this is solely my independent decision.\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="I.	DISTRIBUTION OF LIABILITIES\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="a.	Loan amount of Rs.  ".$wls_data->amount." (Rupees.....) outstanding as on date against my Immovable/ Movable property  ".$wls_data->liabi_type." measuring/of   ".$wls_data->liabi_prop_mes." located at  ".$wls_data->liabi_addr_line1." ".$wls_data->liabi_addr_line2." ".$wls_data->liabi_addr_line3." ".$wls_data->liabi_city." ".getState($wls_data->liabi_state)." ".getCountry($wls_data->liabi_country)." ".$wls_data->liabi_zipcode." availed from  ".$wls_data->liabi_inst_name."  situated at  ".$wls_data->ind_addr_line1." ".$wls_data->ind_addr_line2." ".$wls_data->ind_addr_line3." ".$wls_data->ind_city." ".getState($wls_data->ind_state)." ".getCountry($wls_data->ind_country)." ".$wls_data->ind_zipcode." from date  ".$wls_data->liabi_start_date." to  ".$wls_data->liabi_closing_date." maturity date at the rate of interest  ".$wls_data->liabi_int_rate." to be paid by my............., named....................\nOR\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="b.	...Share... of my Loan amount of Rs.  ".$wls_data->amount." (Rupees.....) outstanding as on date against my Immovable/ Movable property  ".$wls_data->liabi_type."measuring/of   ".$wls_data->liabi_prop_mes." located at  ".$wls_data->liabi_addr_line1." ".$wls_data->liabi_addr_line2." ".$wls_data->liabi_addr_line3." ".$wls_data->liabi_city." ".getState($wls_data->liabi_state)." ".getCountry($wls_data->liabi_country)." ".$wls_data->liabi_zipcode." availed from  ".$wls_data->liabi_inst_name."  situated at   ".$wls_data->ind_addr_line1." ".$wls_data->ind_addr_line2." ".$wls_data->ind_addr_line3." ".$wls_data->ind_city." ".getState($wls_data->ind_state)." ".getCountry($wls_data->ind_country)." ".$wls_data->ind_zipcode." from date  ".$wls_data->liabi_start_date."to  ".$wls_data->liabi_closing_date." maturity date at the rate of interest  ".$wls_data->liabi_int_rate." to be paid by my............., named.................... and/or  my .............., named..................\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="J. ADDITIONAL LIABILITIES:\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="a. I bearing liability/s of outstanding amount of Rs.  ".$wal_data->addlib_amount."  (Rupees........)  in respect of loan/guarantee taken/given as on date  ".$pi_data->creation_date."  against  ".$wal_data->addlib_pro_name." from  ".$wal_data->addlib_loan_amount." situated at   ".$wal_data->addlib_addr_line1." ".$wal_data->addlib_addr_line2." ".$wal_data->addlib_addr_line3." ".$wal_data->addlib_city." ".getState($wal_data->addlib_state)." ".getCountry($wal_data->addlib_country)." ".$wal_data->addlib_zipcode." having Loan account number  ".$wal_data->addlib_loan_acc_no." to be paid by my............, named.............. and/or my .............., named.................. \n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="b.	I bearing liability/s of outstanding amount of Rs.  ".$wal_data->addlib_amount."  (Rupees........)  in respect of loan/guarantee taken/given as on date against  ".$wal_data->addlib_pro_name." from  ".$pi_data->creation_date." situated at   ".$wal_data->addlib_addr_line1." ".$wal_data->addlib_addr_line2." ".$wal_data->addlib_addr_line3." ".$wal_data->addlib_city." ".getState($wal_data->addlib_state)." ".getCountry($wal_data->addlib_country)." ".$wal_data->addlib_zipcode." having Loan account number  ".$wal_data->addlib_loan_acc_no." out of which  ".$wal_data->addlib_loan_amount." of the total amount to be paid by my............, named.............. and/or  my .............., named..................";
	$pdf->MultiCell(0,5,$content); 
	
	$content="However, if any part of any estate is spent during my life time, the same will be deemed to be reduction in the respective estate and only the residue of that estate will be accepted by the respective legatee.\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="K. NO CONTEST CLAUSE\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="It is my wish that my beneficiaries shall respect this Will in letter and spirit. Wherever there is a disproportionate distribution of any property, it has been done thoughtfully and considering the circumstances prevailing during my lifetime. Hence, no part of the Will can be challenged in any proceeding in any court on such grounds. I further declare that I have full knowledge and understanding of all the contents in this WILL and that the contents are true and correct as per my knowledge, belief and information.\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="L. CUSTODIAN\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="I, hereby appoint  ".$wcust_data->custodian_name.", son/daughter/wife of Shri  ".$wcust_data->custodian_father_name.", currently residing at  ".$wcust_data->custodian_addr_line1." ".$wcust_data->custodian_addr_line2." ".$wcust_data->custodian_addr_line3." ".$wcust_data->custodian_city." ".getState($wcust_data->custodian_state)." ".getCountry($wcust_data->custodian_country)." ".$wcust_data->custodian_zipcode.", by religion  ".getReligion($wcust_data->custodian_religion).", ".$wcust_data->custodian_nationality.", by nationality, aged about  ".$wcust_data->custodian_age.", as my Custodian and will hold my Will until my death and thereafter shall handover the Will in proper condition to my executor after my death.\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="\n\n\nSignature of Testator\n..................................\n
 ".$pi_data->pi_f_name." ".$pi_data->pi_m_name." ".$pi_data->pi_l_name ."\n\n";
	$pdf->MultiCell(0,5,$content); 

	$content="M.	WITNESSES\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="We hereby attest that this Will has been signed by  ".$pi_data->pi_f_name." ".$pi_data->pi_m_name." ".$pi_data->pi_l_name." as his/her last Will at  ".$pi_data->pi_place." ,".$pi_data->pi_date." in the joint presence of himself and us. The testator is of sound mind and has made this Will out of his/her free will without any coercion or under any undue influence.\n";
	$pdf->MultiCell(0,5,$content); 

	$content="\n\nSignature of Witness (1)\n 
Name- ". $wwit_data->witness1_name . "\n
Address-  ".$wwit_data->witness1_addr_line1." ".$wwit_data->witness1_addr_line2." ".$wwit_data->witness1_addr_line3." ".$wwit_data->witness1_city." ".getState($wwit_data->witness1_state)." ".getCountry($wwit_data->witness1_country)." ".$wwit_data->witness1_zipcode."\n
Phone no- ".$wwit_data->witness1_phone."\n\n";
	$pdf->MultiCell(0,5,$content); 
	
	$content="\n\nSignature of Witness (2)\n
Name- ".$wwit_data->witness2_name."\n
Address-  ".$wwit_data->witness2_addr_line1." ".$wwit_data->witness2_addr_line2." ".$wwit_data->witness2_addr_line3." ".$wwit_data->witness2_city." ".getState($wwit_data->witness2_state)." ".getCountry($wwit_data->witness2_country)." ".$wwit_data->witness2_zipcode." \n
Phone no- ".$wwit_data->witness2_phone."\n\n";
	$pdf->MultiCell(0,5,$content); 

$content="DRAFTED BY: WWW.TAXSAVE.IN";
	$pdf->MultiCell(0,5,$content); 

    // $content = $pdf->MultiCell(0,5,$content); 
} 

function generateBenGuardianDatatoPrint($beneoneitem) {
    $retval = "I hereby appoint ".$beneoneitem['gd_f_name']." ".$beneoneitem['gd_m_name']." ".$beneoneitem['gd_l_name']." Son/daughter of ".$beneoneitem['gd_fat_f_name']." ".$beneoneitem['gd_fat_m_name']." ".$beneoneitem['gd_fat_l_name'].", by religion ".getReligion($beneoneitem['gd_religious'])." , ".$beneoneitem['gd_nationality']." by nationality, aged about ".$beneoneitem['gd_age'].", having occupation ".$beneoneitem['gd_occupation']." , currently residing at ".$beneoneitem['gd_per_addr_line1']." ".$beneoneitem['gd_per_addr_line2']." ".$beneoneitem['gd_per_addr_line3']." ".$beneoneitem['gd_per_city']." ".getState($beneoneitem['gd_per_state'])." ".getCountry($beneoneitem['gd_per_country'])." ".$beneoneitem['gd_per_zip']. " as Guardian of my minor son/daughter ".$beneoneitem['f_name']." ".$beneoneitem['m_name']." ".$beneoneitem['l_name']." for the personal care and his/her property till he/she attains age of 18 years.\n\nRelationship of the Guardian with the Minor ".$beneoneitem['rel_with_testator']." " ;
    return $retval;
}

function generateBenDatatoPrint($benoneitem) {
    $retval = $benoneitem['f_name'] . " ".$benoneitem['m_name']." ".$benoneitem['l_name']." my ".getRelationship($benoneitem['rel_with_testator']).", aged ".$benoneitem['ben_age']." years residing at ".$benoneitem['per_addr_line1'] . " ".$benoneitem['per_addr_line2'] . " ".$benoneitem['per_addr_line3']." ".$benoneitem['per_city']." ".getState($benoneitem['per_state'])." ".getCountry($benoneitem['per_country)'])." ".$benoneitem['per_zip'] . " ";
    return $retval;
}

function getCountry($country) {
    return "India";
}

function getState($state)
{
    switch ($state) {
        case "35":
            $retval = "Andaman and Nicobar Islands";
            break;
        case "25":
            $retval = "Daman and Diu";
            break;
        case "28":
            $retval = "Andhra Pradesh";
            break;
        case "12":
            $retval = "Arunachal Pradesh";
            break;
        case "18":
            $retval = "Assam";
            break;
        case "10":
            $retval = "Bihar";
            break;
        case "4":
            $retval = "Chandigarh";
            break;
        case "22":
            $retval = "Chhattisgarh";
            break;
        case "26":
            $retval = "Dadra and Nagar Haveli";
            break;
        case "7":
            $retval = "Delhi";
            break;
        case "30":
            $retval = "Goa";
            break;
        case "24":
            $retval = "Gujarat";
            break;
        case "6":
            $retval = "Haryana";
            break;
        case "2":
            $retval = "Himachal Pradesh";
            break;
        case "1":
            $retval = "Jammu and Kashmir";
            break;
        case "20":
            $retval = "Jharkhand";
            break;
        case "29":
            $retval = "Karnataka";
            break;
        case "32":
            $retval = "Kerala";
            break;
        case "31":
            $retval = "Lakshadweep";
            break;
        case "23":
            $retval = "Madhya Pradesh";
            break;
        case "27":
            $retval = "Maharashtra";
            break;
        case "14":
            $retval = "Manipur";
            break;
        case "17":
            $retval = "Meghalaya";
            break;
        case "15":
            $retval = "Mizoram";
            break;
        case "13":
            $retval = "Nagaland";
            break;
        case "21":
            $retval = "Odisha";
            break;
        case "34":
            $retval = "Puducherry";
            break;
        case "3":
            $retval = "Punjab";
            break;
        case "8":
            $retval = "Rajasthan";
            break;
        case "11":
            $retval = "Sikkim";
            break;
        case "33":
            $retval = "Tamil Nadu";
            break;
        case "36":
            $retval = "Telangana";
            break;
        case "16":
            $retval = "Tripura";
            break;
        case "5":
            $retval = "Uttarakhand";
            break;
        case "9":
            $retval = "Uttar Pradesh";
            break;
        case "19":
            $retval = "West Bengal";
            break;
        default:
            $retval = "Other State";
    }
    return $retval;
}

function getRelationship($relationval)
{
    switch ($relationval) {
        case "2":
            $retval = "Spouse";
            break;
        case "2":
            $retval = "Spouse";
            break;
        case "3":
            $retval = "Son";
            break;
        case "4":
            $retval = "Daughter";
            break;
        case "5":
            $retval = "Mother";
            break;
        case "6":
            $retval = "Father";
            break;
        case "7":
            $retval = "Brother";
            break;
        case "8":
            $retval = "Sister";
            break;
        case "19":
            $retval = "Grand Daughter";
            break;
        case "20":
            $retval = "Grandson";
            break;
        case "21":
            $retval = "Grand Father";
            break;
        case "22":
            $retval = "Grand Mother";
            break;
        case "23":
            $retval = "Son-in-Law";
            break;
        case "24":
            $retval = "Daughter-in-law";
            break;
        case "99":
            $retval = "Other";
            break;
        default:
            $retval = "Other";
    }
    return $retval;
}

function getReligion($religionnum) {
    switch($religionnum) {
            case "1" : $retval = "Buddhist"; break;
case "2" : $retval = "Christian"; break;
case "3" : $retval = "Hindu"; break;
case "4" : $retval = "Islam"; break;
case "5" : $retval = "Jain"; break;
case "6" : $retval = "Judaism"; break;
case "7" : $retval = "Parsi"; break;
case "8" : $retval = "Sikh"; break;
        default:
        case "10" : 
            $retval = "Other"; break;
    }
    return $retval;
}
?>
