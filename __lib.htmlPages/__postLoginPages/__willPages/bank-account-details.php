<?php

	$wba_data		= $willProfile->getWillDetails('will_bank_accounts');
	$ben_name		= $willProfile->getWillDetails('will_beneficiary');
	$ben_per		= $willProfile->getWillDetails('will_beneficiary_percentage');
	
	$bene_data_all	= $willProfile->getWillBenDetails('will_beneficiary');

?>
<form action="data_submit/frmForm.php" method="post">
    <input type="hidden" name="form_id" value="1">
    <div class="agreement-row">
        <div class="container">
            <div class="row">
                <input name="bankdetails_process" value="1" type="hidden">
                <input name="acc_id" id="acc_id" value="0" type="hidden">
                <input name="joint_account_count" id="joint_account_count" value="1" type="hidden">
                <input name="total_count" id="total_count" value="1" type="hidden">
                <input name="auth_token" id="auth_token_name" type="hidden">
                <input name="btn_clicked" id="btn_clicked" value="next" type="hidden">
                <div class="col-xs-12">
                    <div id="bnkacAddWrap">
                        <div class="panel panel-body">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label class="mandatory">Account Number
                                        <span class="mandatory_star">*</span></label>
                                    <input class="form-control numeric" name="account_number" value="<?= $wba_data->bnk_acc_no; ?>" placeholder="Account No." maxlength="18" data-validation="" data-validation-length="1-18" data-validation-error-msg="Please enter valid Account No" title="Please enter Account No" alt="Please enter Account No" type="text" required>
                                </div>
                                <div class="col-xs-4">
                                    <label class="mandatory mandatory_label">Name of Bank <span class="mandatory_star">*</span></label>
                                    <input class="form-control" name="bank_name" value="<?= $wba_data->bnk_bank_name; ?>" placeholder="Name of the bank" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Bank Name" title="Please enter Bank Name" alt="Please enter Bank Name" type="text" required>
                                </div>
                            </div>
                            <!-- row END -->
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <label class="mandatory  mandatory_label">Branch Name <span class="mandatory_star">*</span></label>
                                    <input class="form-control" name="branch_name" value="<?= $wba_data->bnk_branch_name; ?>" placeholder="Branch name" maxlength="30" data-validation="length" data-validation-length="1-30" data-validation-error-msg="Please enter valid Branch Name" title="Please enter Branch Name" alt="Please enter Branch Name" type="text" required>
                                </div>
                                <div class="col-xs-4">
                                    <label class="mandatory ">Type of Account
                                        <span class="mandatory_star">*</span></label>
                                    <select name="account_type" id="account_type" class="input-select form-control" title="Please select Type of Account" alt="Please select Type of Account" data-validation="" data-validation-length="1-20" data-validation-error-msg="Please select Type of Account" required>
                                        <?php $selected=$wba_data->bnk_acc_type; ?>
                                        <option value="" <?php echo ($selected=="" ) ? "selected" : "" ; ?> >Please select</option>
                                        <option value="1" <?php echo ($selected==1 ) ? "selected" : "" ; ?>>Current</option>
                                        <option value="2" <?php echo ($selected==2 ) ? "selected" : "" ; ?> >Savings</option>
                                    </select>
                                </div>
                            </div>
                            <!-- row END -->
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <label class="mandatory ">Address Line1
                                        <span class="mandatory_star">*</span></label>
                                    <input class="form-control" name="address_line1" value="<?= $wba_data->bnk_acc_addr_line1; ?>" placeholder="Address Line1" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1" title="Please enter Address Line1" alt="Please enter Address Line1" type="text" required>
                                </div>
                                <div class="col-xs-4">
                                    <label class="mandatory ">Address Line2
                                        <span class="mandatory_star">*</span></label>
                                    <input class="form-control" name="address_line2" value="<?= $wba_data->bnk_acc_addr_line2; ?>" placeholder="Address Line2" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2" title="Please enter Address Line2" alt="Please enter Address Line2" type="text" required>
                                </div>
                                <div class="col-xs-4">
                                    <label class="mandatory">Address Line3</label>
                                    <input class="form-control" name="address_line3" value="<?= $wba_data->bnk_acc_addr_line3; ?>" placeholder="Address Line3" maxlength="60" title="Please enter Address Line3" alt="Please enter Address Line3" type="text">
                                </div>
                            </div>
                            <!-- row END -->
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <label class="mandatory">City / Village / Town
                                        <!-- <span class="mandatory_star">*</span> --></label>
                                    <input class="form-control" name="city_village_town" value="<?= $wba_data->bnk_acc_city; ?>" placeholder="City / Village / Town" maxlength="60" title="Please enter City / Village / Town" alt="Please enter City / Village / Town" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town" type="text">
                                </div>
                                <div class="col-xs-2">
                                    <label class="mandatory ">State
                                        <!-- <span class="mandatory_star">*</span> --></label>
                                    <span id="span_state">
                                        <?php $corre_state=$wba_data->bnk_acc_state; ?>
                                        <select name="state" id="state" class="input-select form-control" title="Please select appropriate State" alt="Please select appropriate State" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please select a valid State">
                                        <option value="35" <?php echo ($corre_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($corre_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($corre_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($corre_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($corre_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($corre_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($corre_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($corre_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($corre_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($corre_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($corre_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($corre_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($corre_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($corre_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($corre_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($corre_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($corre_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($corre_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($corre_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($corre_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($corre_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($corre_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($corre_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($corre_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($corre_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($corre_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($corre_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($corre_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($corre_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($corre_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($corre_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($corre_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($corre_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($corre_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($corre_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($corre_state == 19)?"selected":"" ?>>West Bengal</option>
                                        </select>
                                    </span>
                                </div>
                                <div class="col-xs-3">
                                    <span id="span_state_other" style="display:none;" validatemsg="Please enter a Other State">
                      <label class="cnd-mandatory">Other State (If not listed) 
                        <!-- <span class="mandatory_star">*</span> -->
                    </label>
                                    <input class="form-control" name="state_other" value="" placeholder="Mention State Name" maxlength="60" title="Please enter a Other State" alt="Please enter a Other State" type="text">
                                    </span>
                                </div>
                                <div class="col-xs-2">
                                    <label class="mandatory ">Pincode
                                        <!-- <span class="mandatory_star">*</span> --></label>
                                    <input class="form-control numeric" name="zipcode" value="<?= $wba_data->bnk_acc_zip; ?>" placeholder="Pincode" maxlength="6" data-validation="" data-validation-length="6-6" data-validation-error-msg="Please enter valid Pincode." title="Please enter Pincode" alt="Please enter Pincode" type="text">
                                </div>
                                <div class="col-xs-2">
                                    <label class="mandatory ">Country
                                        <!-- <span class="mandatory_star">*</span> --></label>
                                    <select name="country" id="country" class="input-select form-control" title="Please select a Country" alt="Please select a Country" data-validation="" data-validation-length="1-33" data-validation-error-msg="Please select a valid Country" onchange="javascript: populate_states(this.value,'state', 'span_state', '0','toggle_other_state(\'state\',\'state_other\')'); toggle_other_state_country(this.value, 'span_state_other');;">
                                        <option value="102" selected="selected">India</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <label class="mandatory  mandatory_label">% of ownership<span class="mandatory_star">*</span></label>
                                    <input class="form-control" name="ownership_perc" value="<?= $wba_data->bnk_ownership_perc; ?>" placeholder="% of ownership" maxlength="30" data-validation="length" data-validation-length="1-30" data-validation-error-msg="Please enter valid percentage of ownership" title="Please enter percentage of ownership" alt="Please enter percentage of ownership" type="text" required>
                                </div>
                            </div>
                        </div>
                        <!-- panel-body END -->
                        <div class="row">&nbsp;</div>
                        <div id="div_beneficiary">
                            <div id="div_beneficiary_1">
                                <h4>
          <strong>Beneficiaries</strong>
        </h4>
                                <div class="panel panel-body sub-panel">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label class="mandatory mandatory_label">Beneficiary <span class="mandatory_star">*</span></label>
                                            <select name="sel_beneficiary" id="sel_beneficiary" class="input-select form-control" title="Please select a Beneficiary" alt="Please select a Beneficiary" required>
                                                <option value="">Please select</option>
                                                <?php 
                                                $cnt=1;
                                                while($row = mysql_fetch_array($bene_data_all)) {
                                                    $html = "<option value='" . $cnt . "'>" .$row['f_name'] . "</option>";
                                                    
                                                    $cnt+= 1;
                                                    echo $html;
                                                } 
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-xs-4">
                                            <label class="mandatory mandatory_label">Percentage Share to be allotted <span class="mandatory_star">*</span></label>
                                            <input class="form-control numeric_share" name="txt_beneficiary_share" id="txt_beneficiary_share" placeholder="Percentage Share to be allotted" maxlength="5" title="Please enter Percentage Share to be allotted" alt="Please enter Percentage Share to be allotted" value="" type="text" required>
                                        </div>
                                    </div>
                                    <div>
                                        <br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add Beneficiary" title="Add Beneficiary" id="beneficiaryAddBtnNew" class="btn btn-primary btn-sm btn-add-large a_beneficiarynew">+ Add Beneficiary</a></span></div>
                                    <br>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Relation</th>
                                                <th>Share</th>
                                                <th><a href="" class="btn btn-danger">Reset all</a></th>
                                            </tr>
                                        </thead>
                                        <tbody id="tb_con_ben">
                                            <tr id="no_beneficiary">
                                                <td><?= $ben_name->fk_user_id ; ?></td>
                                                <td>
                                                    <?= $ben_name->f_name; ?>
                                                    <?= $ben_name->m_name; ?>
                                                    <?= $ben_name->l_name; ?>
                                                </td>
                                                <td></td>
                                                <td><?= $ben_per->bene_percentage; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div>
<!--                                     <br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add More Beneficiary" title="Add More Beneficiary" id="" class="btn btn-success btn-sm">+ Add More</a></span> -->
                                </div>
                            </div>
                        </div>

                    </div>

                                            <div class="bottom_buttons row">
            <div class="div_prev_button">
                <input name="previous" id="btn-previous" value="« Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="executor-details" type="button">
            </div>
            <div class="div_center_buttons">
                <!-- <input name="bank-btn" id="btn-add" value="Add / Save" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " type="submit"> -->
                <!-- <input name="update" id="btn-update" value="Update" title="Click here to Update the changed data" alt="Click here to Update the changed data" class="btn-submit btn-submit-disabled" disabled="" type="submit"> -->
            </div>
            <div class="div_next_button" style="margin-right: -30px; text-align: right;">
                <input name="bank-btn" id="btn-add" value="Save & Next »" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit" type="submit">
                <!-- <input name="next" id="btn-next" value="Next »" title="Click here to go to Next page" alt="Click here to go to Next page" class="btn-submit btn-move" next_page="locker-details" type="button"> -->
            </div><br><br><br>
        </div>
                        </div>
            </div>
        </div>
        <!-- row END -->
    </div>
</form>