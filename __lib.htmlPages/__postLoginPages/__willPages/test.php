<?php
    include 'data_submit/toDatabase.php';
    $obj = new update();
    $tbname ='will_immovable_properties ';
    $user_id = 1;
    $result = $obj->selectAllDbRecord($tbname, $user_id);
    $wip_data = mysqli_fetch_object($result);
    // print_r($wip_data);
?>
<form action="data_submit/frmForm.php" name="" id="" method="post" class="frmCurrent has-validation-callback">
    <div class="agreement-row">
        <div class="container">
            <input type="hidden" name="form_id" value="6">
            <h3>Property</h3>
            <div class="row">
                <input name="immovableproperties_process" value="1" type="hidden">
                <input name="prp_id" id="prp_id" value="0" type="hidden">
                <input name="joint_account_count" id="joint_account_count" value="1" type="hidden">
                <input name="total_count" id="total_count" value="1" type="hidden">
                <input name="auth_token" id="auth_token_name" value="TVRBMkxqSXlNeTR4T1RrdU1UYzNkVGh5YjJoa016QTNOR2wyYWpWa2EyWTJZM1ZpTkd4dmJEVT0=" type="hidden">
                <input name="btn_clicked" id="btn_clicked" value="next" type="hidden">
                <div class="col-xs-12">
                    <div id="bnkacAddWrap">
                        <div>
                            <!-- do not delte this div -->
                            <!--<h3>Property Details<span>&nbsp;</span></h3> -->
                            <div class="panel panel-body">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <label class="mandatory">Type of the Property </label>
                                        <select class="form-control" name="" id="">
                                            <option value="">Select</option>
                                            <option value="">Ag land</option>
                                            <option value="">Non Ag. Land</option>
                                            <option value="">C. Building</option>
                                            <option value="">R. building</option>
                                            <option value="">Factory</option>
                                            <option value="">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- row END -->
                                <div class="divider"></div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        &nbsp;&nbsp;&nbsp;
                                        <input name="is_same_as_permanent" value="P" id="is_same_as_permanent" title="Please select if Property Address is same as your Permanent Address" alt="Please select if Property Address is same as your Permanent Address" class="is_property_address_same" type="checkbox">&nbsp;&nbsp;&nbsp;
                                        <label class="mandatory">Is Property Address same as your Permanent Address 1?</label>
                                    </div>

                                </div>
                                <div class="row" style="display:none;">
                                    <div class="col-xs-6">
                                        &nbsp;&nbsp;&nbsp;
                                        <input name="is_same_as_permanent_or_correspondence" id="is_same_as_permanent_or_correspondence" value="" type="hidden">
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div id="populate_address">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label class="mandatory mandatory_label">Address Line1 <span class="mandatory_star">*</span></label>
                                            <input class="form-control" name="address_line1" id="address_line1" value="<?= $wip_data->prop_addr_line1; ?>" placeholder="Address Line1" maxlength="60" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter Address Line1" title="Please enter Address Line1" alt="Please enter Address Line1" type="text" required>
                                        </div>
                                        <div class="col-xs-4">
                                            <label class="mandatory mandatory_label">Address Line2 <span class="mandatory_star">*</span></label>
                                            <input class="form-control" name="address_line2" id="address_line2" value="<?= $wip_data->prop_addr_line2; ?>" placeholder="Address Line2" maxlength="60" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter Address Line2" title="Please enter Address Line2" alt="Please enter Address Line2" type="text" required>
                                        </div>
                                        <div class="col-xs-4">
                                            <label>Address Line3</label>
                                            <input class="form-control" name="address_line3" id="address_line3" value="<?= $wip_data->prop_addr_line3; ?>" placeholder="Address Line3" maxlength="60" title="Please enter Address Line3" alt="Please enter Address Line3" type="text">
                                        </div>
                                    </div>
                                    <!-- row END -->
                                    <div class="divider"></div>
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label class="mandatory_label">City / Village / Town <span class="mandatory_star">*</span></label>
                                            <input class="form-control" name="city_village_town" id="city_village_town" value="<?= $wip_data->prop_zipcode; ?>" placeholder="City / Village / Town" maxlength="60" title="Please enter City / Village / Town" alt="Please enter City / Village / Town" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter City / Village / Town" type="text" required>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="mandatory mandatory_label">Pincode <span class="mandatory_star">*</span></label>
                                            <input class="form-control numeric" name="zipcode" id="zipcode" value="<?= $wip_data->prop_zipcode; ?>" placeholder="Pincode" maxlength="6" data-validation="length" data-validation-length="6-6" data-validation-error-msg="Please enter valid Pincode" title="Please enter Pincode" alt="Please enter Pincode" type="text" required>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="mandatory mandatory_label">Country <span class="mandatory_star">*</span></label>
                                            <select name="country" id="country" class="input-select form-control" title="Please select a Country" alt="Please select a Country" data-validation="length" data-validation-length="1-3" data-validation-error-msg="Please select Country" onchange="javascript: populate_states_disable(this.value,'state', 'span_state', '0','toggle_other_state(\'state\',\'state_other\')'); toggle_other_state_country(this.value, 'state_other');" required>
                                                <option value="102" selected="selected">India</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="mandatory mandatory_label">State <span class="mandatory_star">*</span></label>
                                            <span id="span_state">
                                            <?php $curr_state=$wip_data->prop_state ; ?>
                                                <select name="state" id="state" class="input-select form-control" title="Please select appropriate State" alt="Please select appropriate State" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please select a valid State" required>
                                                <option value="35" selected <?php echo ($curr_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($curr_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($curr_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($curr_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($curr_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($curr_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($curr_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($curr_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($curr_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($curr_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($curr_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($curr_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($curr_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($curr_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($curr_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($curr_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($curr_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($curr_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($curr_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($curr_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($curr_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($curr_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($curr_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($curr_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($curr_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($curr_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($curr_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($curr_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($curr_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($curr_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($curr_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($curr_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($curr_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($curr_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($curr_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($curr_state == 19)?"selected":"" ?>>West Bengal</option>
                                            </select>
                                        </span>
                                        </div>
                                        <div class="col-xs-3">
                                            <span id="span_state_other" style="display:none" validatemsg="Please enter Other State">
                            <label class="cnd-mandatory mandatory_label">Other State (If not listed)<span class="mandatory_star">*</span></label>
                                            <input class="form-control" name="state_other" value="" placeholder="Mention State Name" maxlength="60" title="Please enter Other State" alt="Please enter Other State" type="text" required>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="mandatory mandatory_label">Measurement of the property <span class="mandatory_star">*</span></label>
                                        <input class="form-control" name="prop_measurement" id="prop_measurement"  type="text">
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Owner Status <span class="mandatory_star">*</span></label>
                                        <select name="ownership_status" id="ownership_status" class="form-control">
                                            <option value="">Select one</option>
                                            <option value="single">Single</option>
                                            <option value="joint">Joint</option>
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="divider"></div>

                            </div>
                            <!-- panel-body END -->
                            <div class="row">&nbsp;</div>
                            <div id="div_beneficiary">
                                <div id="div_beneficiary_1">
                                    <h4>
              <strong>Beneficiaries</strong>
            </h4>
                                    <div class="panel panel-body sub-panel">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <label class="mandatory mandatory_label">Beneficiary 
                                                <!-- <span class="mandatory_star">*</span> -->
                                                </label>
                                                <select name="sel_beneficiary" id="sel_beneficiary" class="input-select form-control" title="Please select a Beneficiary" alt="Please select a Beneficiary">
                                                    <option value="">Please select</option>
                                                </select>
                                            </div>
                                            <div class="col-xs-4">
                                                <label class="mandatory mandatory_label">Percentage Share to be alloted 
                                                <span class="mandatory_star">*</span>
                                                </label>
                                                <input class="form-control numeric_share" name="txt_beneficiary_share" id="txt_beneficiary_share" placeholder="Percentage Share to be alloted" maxlength="5" title="Please enter Percentage Share to be alloted" alt="Please enter Percentage Share to be alloted" value="" type="text">
                                            </div>
                                        </div>
                                        <div>
                                            <br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add Beneficiary" title="Add Beneficiary" id="beneficiaryAddBtnNew" class="btn btn-primary btn-sm btn-add-large a_beneficiarynew">+ Add Beneficiary</a></span>
                                        </div>
                                        <br>
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Share</th>
                                                    <th>Relation</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tb_con_ben">
                                                <tr id="no_beneficiary">
                                                    <td colspan="5"></td>
                                                </tr>
                                                <input name="tot_ben" id="tot_ben" value="1" type="hidden">
                                                <input id="total_share" value="0" type="hidden">
                                            </tbody>
                                        </table>                   
                                    </div>
                                    <div>
                                        <br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add More Beneficiary" title="Add More Beneficiary" id="" class="btn btn-success btn-sm">+ Add More</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- row END -->
                        <div class="row">&nbsp;</div>
                        <div class="bottom_buttons row">
                            <div class="div_prev_button">
                                <input name="previous" id="btn-previous" value="« Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="public-provident-fund" type="button">
                            </div>
                            <div class="div_center_buttons">
                            </div>
                            <div class="div_next_button" style="margin-right: -30px; text-align: right;">
                                <input name="add_immovable" id="btn-add" value="Save & Next »" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " type="submit">
                            </div><br><br><br>
                        </div>
                        <div class="row">&nbsp;</div>
                        <div class="divider"></div>
                    </div>
                </div>
            </div>
            <!-- gen-row END -->
        </div>
    </div>
</form>