<?php
	include_once("../../../__lib.includes/config.inc.php");
	$bene_data_all	= $willProfile->getWillBenDetails('will_beneficiary');
	$wfd_data		= $willProfile->getWillDetails('will_fixed_deposit');

?>

<form name="frmBankAccount" action="data_submit/frmForm.php" id="frmBankAccount" method="post" class="frmCurrent has-validation-callback">
<input type="hidden" name="form_id" value="3">
<div class="agreement-row">
            <div class="container">

                    <div class="row">
                        <input name="fixeddeposits_process" value="1" type="hidden">
                        <input name="fxd_id" id="fxd_id" value="0" type="hidden">
                        <input name="joint_account_count" id="joint_account_count" value="1" type="hidden">
                        <input name="total_count" id="total_count" value="1" type="hidden">
                        <input name="end_account_id" id="end_account_id" value="" type="hidden">
                        <input name="is_adult" id="is_adult" value="0" type="hidden">
                        <input name="auth_token" id="auth_token_name" value="TVRBekxqVTFMakUyT0M0eE5UWnRiek15WWpkbmNHODJNamhsWkdZeWFtcG1hbWMxWlhZeE13PT0=" type="hidden">
                        <input name="btn_clicked" id="btn_clicked" value="next" type="hidden">
                        <div class="col-xs-12">
                            <div id="bnkacAddWrap">
                                <div><!-- do not delte this div -->

                                    <div class="panel panel-body">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label class="mandatory">Account Number <!-- <span class="mandatory_star">*</span> --></label>
                                                <input class="form-control " name="fd_account_number" value="<?= $wfd_data->fd_account_number; ?>" placeholder="Account No." maxlength="18" data-validation="" data-validation-length="1-18" data-validation-error-msg="Please enter valid Account Number" title="Please enter valid Account Number" alt="Please enter valid Account Number" type="text">
                                            </div>
                                            <div class="col-xs-6" style="display: none">
                                                <label class="mandatory">IFSC </label>
                                                <input class="form-control" name="ifsc_code" value="" placeholder="IFSC Code" maxlength="11" title="Please enter IFSC code" alt="Please enter IFSC code" type="text">
                                            </div>
                                        </div><!-- row END -->
                                        <div class="divider"></div>
                                        <div class="row">
                                           <div class="col-xs-6">
                                                <label class="mandatory mandatory_label">Name of Bank <span class="mandatory_star">*</span></label>
                                                <input class="form-control" name="fd_bank_name" value="<?= $wfd_data->fd_bank_name; ?>" placeholder="Name of the bank" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Bank Name." title="Please enter valid Bank Name" alt="Please enter valid Bank Name" type="text">
                                            </div>
                                            <div class="col-xs-6">
                                                <label class="mandatory mandatory_label">Branch Name <span class="mandatory_star">*</span></label>
                                                <input class="form-control" name="fd_branch_name" value="<?= $wfd_data->fd_branch_name; ?>" placeholder="Branch name" maxlength="30" data-validation="length" data-validation-length="1-30" data-validation-error-msg="Please enter valid Branch Name." title="Please enter Branch Name" alt="Please enter Branch Name" type="text">
                                            </div>
                                        </div><!-- row END -->
                                        <div class="divider"></div>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <label class="mandatory">Address Line1 <!-- <span class="mandatory_star">*</span> --></label>
                                                <input class="form-control" name="fd_address_line1" value="<?= $wfd_data->fd_addr_line1; ?>" placeholder="Address Line1" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1" title="Please enter Address Line1" alt="Please enter Address Line1" type="text">
                                            </div>
                                            <div class="col-xs-4">
                                                <label class="mandatory ">Address Line2 <!-- <span class="mandatory_star">*</span> --></label>
                                                <input class="form-control" name="fd_address_line2" value="<?= $wfd_data->fd_addr_line2; ?>" placeholder="Address Line2" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2" title="Please enter Address Line2" alt="Please enter Address Line2" type="text">
                                            </div>
                                            <div class="col-xs-4">
                                                <label class="mandatory">Address Line3</label>
                                                <input class="form-control" name="fd_address_line3" value="<?= $wfd_data->fd_addr_line3; ?>" placeholder="Address Line3" maxlength="60" title="Please enter Address Line3" alt="Please enter Address Line3" type="text">
                                            </div>
                                        </div><!-- row END -->
                                        <div class="divider"></div>
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label class="mandatory">City / Village / Town <!-- <span class="mandatory_star">*</span> --></label>
                                                <input class="form-control" name="fd_city_village_town" value="<?= $wfd_data->fd_city; ?>" placeholder="City / Village / Town" maxlength="60" title="Please enter City / Village / Town" alt="Please enter City / Village / Town" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town" type="text">
                                            </div>
                                            <div class="col-xs-2">
                                                <label class="mandatory">Pincode <!-- <span class="mandatory_star">*</span> --></label>
                                                <input class="form-control numeric" name="fd_zipcode" value="<?= $wfd_data->fd_zipcode; ?>" placeholder="Pincode" maxlength="6" data-validation="" data-validation-length="6-6" data-validation-error-msg="Please enter valid Pincode." title="Please enter Pincode" alt="Please enter Pincode" type="text">
                                            </div>
                                            <div class="col-xs-2">
                                                <label class="mandatory">Country <!-- <span class="mandatory_star">*</span> --></label>
                                                <select name="fd_country" id="fd_country" class="input-select form-control" title="Please select a Country" alt="Please select a Country" data-validation="" data-validation-length="1-33" data-validation-error-msg="Please select a valid Country" onchange="javascript: populate_states(this.value,'state', 'span_state', '0','toggle_other_state(\'state\',\'state_other\')'); toggle_other_state_country(this.value, 'span_state_other');"><option value="102" selected="selected">India</option></select>                                            </div>
                                            <div class="col-xs-2">
                                                <label class="mandatory ">State <!-- <span class="mandatory_star">*</span> --></label> 
                                                <span id="span_state">
                                                <?php $corre_state=$wfd_data->fd_state; ?>
                                                <select
											name="fd_state" id="state" class="input-select form-control"
											title="Please select appropriate State"
											alt="Please select appropriate State" data-validation=""
											data-validation-length="1-60"
											data-validation-error-msg="Please select a valid State">
											
											<option value="35" <?php echo ($corre_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($corre_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($corre_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($corre_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($corre_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($corre_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($corre_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($corre_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($corre_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($corre_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($corre_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($corre_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($corre_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($corre_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($corre_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($corre_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($corre_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($corre_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($corre_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($corre_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($corre_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($corre_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($corre_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($corre_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($corre_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($corre_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($corre_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($corre_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($corre_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($corre_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($corre_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($corre_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($corre_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($corre_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($corre_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($corre_state == 19)?"selected":"" ?>>West Bengal</option>
                                        </select></span>
									</div>
                                            <div class="col-xs-3">
                                                                                                <span id="span_state_other" style="display:none;">
                                                    <label class="cnd-mandatory ">Other State (If not listed) <!-- <span class="mandatory_star">*</span> --></label>
                                                    <input class="form-control" name="state_other" value="" placeholder="Other state" maxlength="60" title="Please enter a Other State" alt="Please enter a Other State" type="text">
                                                </span>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="row">
                                            <div class="col-xs-12" style="display: none">
                                                <input name="all_fds" id="all_fds" value="1" title="Please select if All Fixed Deposits / Recurring Deposits held with this bank is to be distributed as per the Beneficiary details provided below." alt="Please select if All Fixed Deposits / Recurring Deposits held with this bank is to be distributed as per the Beneficiary details provided below." type="checkbox">&nbsp;&nbsp;&nbsp;<label>All Fixed Deposits / Recurring Deposits held with this bank is to be distributed as per the Beneficiary details provided below.</label>
                                            </div>
                                        </div>
                                    </div><!-- panel-body END -->

                                                                    </div>

                                                                </div>
                                                            </div><!-- col-xs-12 END -->

                                    						<div class="row">&nbsp;</div>
                                    						 <div id="div_beneficiary">
                                    							<div id="div_beneficiary_1">
                                    								<h4>
                                    									<strong>Beneficiaries</strong>
                                    								</h4>
                                    								<div class="panel panel-body sub-panel">
                                    									<div class="row">
                                    										<div class="col-xs-4">
                                    											<label class="mandatory mandatory_label">Beneficiary <span class="mandatory_star">*</span></label> <select
										name="sel_beneficiary" id="sel_beneficiary"
										class="input-select form-control"
										title="Please select a Beneficiary"
										alt="Please select a Beneficiary">
										<option value="">Please select</option>
                                    											 <?php
                                                $cnt = 1;
                                                while ($row = mysql_fetch_array($bene_data_all)) {
                                                    $html = "<option value='" . $cnt . "'>" . $row['f_name'] . "</option>";
                                                    
                                                    $cnt += 1;
                                                    echo $html;
                                                }
                                                ?>
                                    											
                                    											</select>
								</div>
                                    										<div class="col-xs-4">
                                    																						<label class="mandatory mandatory_label">Percentage Share to be allotted <span class="mandatory_star">*</span></label>
                                    											<input class="form-control numeric_share" name="txt_beneficiary_share" id="txt_beneficiary_share" placeholder="Percentage Share to be allotted" maxlength="5" title="Please enter Percentage Share to be allotted" alt="Please enter Percentage Share to be allotted" value="" type="text">
                                    										</div>
                                    										<div class="col-xs-4">
                                    																						<label class="mandatory">Any Other Information</label>
                                    											<input class="form-control" name="txt_other_info" id="txt_other_info" maxlength="200" placeholder="Any Other Information" title="Please enter Any Other Information" alt="Please enter Any Other Information" value="" type="text">
                                    										</div>
                                    									</div>
                                    									<div><br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add Beneficiary" title="Add Beneficiary" id="beneficiaryAddBtnNew" class="btn btn-primary btn-sm btn-add-large a_beneficiarynew">+ Add Beneficiary</a></span></div>
                                    									<br>
                                    									<table class="table table-striped">
                                    											  <thead>
                                    												<tr>
                                    												  <th>#</th>
                                    												  <th>Name</th>
                                    												  <th>Share</th>
                                    												  <th>Other Information</th>
                                    												  <th>Status</th>
                                    												</tr>
                                    											  </thead>
                                    											  <tbody id="tb_con_ben"><tr id="no_beneficiary"><td colspan="5"></td></tr>
                                    																										<input name="tot_ben" id="tot_ben" value="1" type="hidden">
                                    													<input id="total_share" value="0" type="hidden">

                                    											  </tbody>
                                    											</table>


                                    								</div>
                                    							</div>
                                    						</div>


                                                        </div><!-- row END -->

                                    							<div class="row">&nbsp;</div>
                                    							<div class="bottom_buttons row">
                                    								<div class="div_prev_button">
                                    									<input name="previous" id="btn-previous" value="« Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="bank-account-details" type="button">
                                    								</div>
                                    								
                                    								<div class="div_next_button" style="margin-right: -30px; text-align: right;">
                                    									<input name="fd_btn" id="fd_btn" value="Save & Next »" title="Click here to go to Next page" alt="Click here to go to Next page" class="btn-submit btn-move" next_page="fixed-deposit-details" class="btn-submit" type="submit"><br><br>
                                    								</div><br><br><br>
                                    							</div>
                                                                

                                                    <div class="row">&nbsp;</div>
                                                    <div id="div_joint_account_holders_hidden" class="hide">
                                                                                        <div id="account_1">
                                                                        <div id="jointholderAddWrap"><!-- *** If value 'Joint' display this div *** -->
                                                                            <input name="ejd_hld_id_1" value="0" type="hidden">
                                                                            <div><!-- Keep this div as it required for copying -->
                                                                                <h4>
                                                                                    <div class="jah_account_numbering"><strong>Joint Account Holder #2 Details</strong></div>
                                                                                    <span id="span_account_1">
                                                                                        <a href="javascript:void(0);" class="btn btn-primary btn-sm btn-add-large a_add_account" title="Click to Add Joint Account Holder" alt="Click to Add Joint Account Holder">+ Add Joint Account Holder</a>                                                </span>
                                                                                </h4>
                                                                                <div class="panel panel-body sub-panel">
                                                                                    <div class="row">
                                                                                        <div class="col-xs-12">
                                                                                            <label class="mandatory mandatory_label">Details of the Joint Holder(s) <span class="mandatory_star">*</span></label>
                                                                                            <div class="row">
                                                                                                <div class="col-xs-3">
                                                                                                    <select name="ejd_title_1" id="ejd_title_1" class="input-select form-control" data-validation="length" data-validation-length="1-2" data-validation-error-msg="Please select a valid Title for joint account holder" title="Please select a Title for joint account holder" alt="Please select a Title for joint account holder "><option value="">Title</option><option value="1">Mr.</option><option value="2">Mrs.</option><option value="3">Ms.</option><option value="10">Master</option><option value="23">Kumar</option><option value="24">Kumari</option></select>                                                            </div>
                                                                                                <div class="col-xs-3">
                                    																<input class="form-control" name="ejd_lastname_1" id="ejd_lastname_1" value="" placeholder="Last Name / Surname" maxlength="33" title="Please enter Last Name / Surname for joint account holder " alt="Please enter Last Name / Surname for joint account holder " data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please enter Last Name / Surname for joint account holder " type="text">
                                    															</div>
                                                                                                <div class="col-xs-3">
                                    																<input class="form-control" name="ejd_firstname_1" id="ejd_firstname_1" value="" placeholder="First Name" maxlength="33" title="Please enter First Name for joint account holder " alt="Please enter First Name for joint account holder " type="text">
                                    															</div>
                                                                                                <div class="col-xs-3">
                                    																<input class="form-control" name="ejd_middlename_1" id="ejd_middlename_1" value="" placeholder="Middle Name" maxlength="33" title="Please enter Middle Name for joint account holder " alt="Please enter Middle Name for joint account holder " type="text">
                                    															</div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="divider"></div>
                                    													 <div class="row">
                                    														<div class="col-xs-6">
                                    															&nbsp;&nbsp;&nbsp;<input class="same_per_cor_address" name="is_same_as_permanent_1" value="P" id="is_same_as_permanent_1" account_count="1" for_address="permanent" title="Select if is this Address same as your Permanent Address?" alt="Select if is this Address same as your Permanent Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
                                    															<label class="mandatory">Is Address same as your Permanent Address?</label>

                                    														</div>
                                    														<div class="col-xs-6">
                                    															&nbsp;&nbsp;&nbsp;<input class="same_per_cor_address" name="is_same_as_correspondence_1" value="C" id="is_same_as_correspondence_1" account_count="1" for_address="permanent" title="Select if is this Address same as your Correspondence Address?" alt="Select if is this Address same as your Correspondence Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
                                    															<label class="mandatory">Is Address same as your Correspondence Address?</label>
                                    														</div>
                                    													 </div>
                                    													  <div class="row" style="display:none;">
                                    														<div class="col-xs-6">
                                    															&nbsp;&nbsp;&nbsp;<input name="is_same_as_permanent_or_correspondence_1" id="is_same_as_permanent_or_correspondence_1" value="" type="hidden">
                                    														</div>
                                    													 </div>
                                    													  													<div class="divider"></div>
                                    													<div id="populate_permanent_address_1">
                                                                                            <div class="row">
                                                                                                <div class="col-xs-8">
                                                                                                    &nbsp;&nbsp;&nbsp;<input name="ejd_is_foreign_address_1" id="ejd_is_foreign_address_1" value="1" title="Please select if its a foreign address" alt="Please select if its a foreign address" class="is_foreign_address" changezip="ejd_zipcode_1" country="ejd_country_1" state="ejd_state_1" span_country="span_ejd_country_1" span_state="span_ejd_state_1" other_state="span_ejd_state_other_1" type="checkbox">&nbsp;&nbsp;&nbsp;<label class="mandatory">Is this Address a Foreign Address?</label>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="divider"></div>
                                                                                            <div class="row">
                                                                                                <div class="col-xs-4">
                                                                                                    <label class="mandatory mandatory_label">Address Line1 <span class="mandatory_star">*</span></label>
                                                                                                    <input class="form-control" name="ejd_address_line1_1" id="ejd_address_line1_1" value="" placeholder="Address Line1" maxlength="60" title="Please enter Address Line1 for joint account holder address" alt="Please enter Address Line1 for joint account holder address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1 for joint account holder address" type="text">
                                                                                                </div>
                                                                                                <div class="col-xs-4">
                                                                                                    <label class="mandatory mandatory_label">Address Line2 <span class="mandatory_star">*</span></label>
                                                                                                    <input class="form-control" placeholder="Address Line2" name="ejd_address_line2_1" id="ejd_address_line2_1" value="" maxlength="60" title="Please enter Address Line2 for joint account holder address" alt="Please enter Address Line2 for joint account holder address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2 for joint account holder address" type="text">
                                                                                                </div>
                                                                                                <div class="col-xs-4">
                                                                                                    <label>Address Line3</label>
                                                                                                    <input class="form-control" placeholder="Address Line3" name="ejd_address_line3_1" id="ejd_address_line3_1" value="" maxlength="60" title="Please enter Address Line3 for joint account holder address" alt="Please enter Address Line3 for joint account holder address" type="text">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="divider"></div>
                                                                                            <div class="row">
                                                                                                <div class="col-xs-3">
                                                                                                    <label class="mandatory_label">City / Village / Town <span class="mandatory_star">*</span></label>
                                                                                                    <input class="form-control" placeholder="City / Village / Town" name="ejd_city_village_town_1" id="ejd_city_village_town_1" value="" maxlength="60" title="Please enter City / Village / Town for joint account holder address" alt="Please enter City / Village / Town for joint account holder address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town for joint account holder address" type="text">
                                                                                                </div>
                                                                                                <div class="col-xs-2">
                                                                                                    <label class="mandatory mandatory_label">Pincode / Zipcode <span class="mandatory_star">*</span></label>
                                                                                                    <input class="form-control numeric" placeholder="Pincode" name="ejd_zipcode_1" id="ejd_zipcode_1" value="" title="Please enter Pincode / Zipcode for joint account holder address" alt="Please enter Pincode / Zipcode for joint account holder address" maxlength="6" data-validation="length" data-validation-length="6-6" data-validation-error-msg="Please enter valid Pincode for joint account holder address" type="text">
                                                                                                </div>
                                                                                                <div class="col-xs-2">
                                                                                                    <label class="mandatory mandatory_label">Country<span class="mandatory_star">*</span></label>
                                                                                                    <span id="span_ejd_country_1">
                                                                                                    <select name="ejd_country_1" id="ejd_country_1" class="input-select form-control" title="Please select a Country for joint account holder address" alt="Please select a Country for joint account holder address" data-validation="length" data-validation-length="1-4" data-validation-error-msg="Please select valid Country" onchange="javascript: populate_states(this.value,'ejd_state_1','span_ejd_state_1', '','toggle_other_state(ejd_state_1,ejd_state_other_1)'); toggle_other_state_country(this.value, 'span_ejd_state_other_1');;"><option value="102" selected="selected">India</option></select>                                                                </span>
                                                                                                </div>
                                                                                                <div class="col-xs-2">
                                                                                                    <label class="mandatory mandatory_label">State <span class="mandatory_star">*</span></label>
                                                                                                    <span id="span_ejd_state_1">
                                                                                                        <select name="ejd_state_1" id="ejd_state_1" class="input-select form-control" title="Please select a State for joint account holder address" alt="Please select a State for joint account holder address" onchange="javascript: toggle_other_state('ejd_state_1', 'ejd_state_other_1');"><option value="35" selected="selected">Andaman and Nicobar Islands</option><option value="28">Andhra Pradesh</option><option value="12">Arunachal Pradesh</option><option value="18">Assam</option><option value="10">Bihar</option><option value="4">Chandigarh</option><option value="22">Chhattisgarh</option><option value="26">Dadra and Nagar Haveli</option><option value="25">Daman and Diu</option><option value="7">Delhi</option><option value="30">Goa</option><option value="24">Gujarat</option><option value="6">Haryana</option><option value="2">Himachal Pradesh</option><option value="1">Jammu and Kashmir</option><option value="20">Jharkhand</option><option value="29">Karnataka</option><option value="32">Kerala</option><option value="31">Lakshadweep</option><option value="23">Madhya Pradesh</option><option value="27">Maharashtra</option><option value="14">Manipur</option><option value="17">Meghalaya</option><option value="15">Mizoram</option><option value="13">Nagaland</option><option value="21">Odisha</option><option value="34">Puducherry</option><option value="3">Punjab</option><option value="8">Rajasthan</option><option value="11">Sikkim</option><option value="33">Tamil Nadu</option><option value="36">Telangana</option><option value="16">Tripura</option><option value="5">Uttarakhand</option><option value="9">Uttar Pradesh</option><option value="19">West Bengal</option></select>                                                                </span>
                                                                                                </div>
                                                                                                <div class="col-xs-3">
                                                                                                    <span id="span_ejd_state_other_1" style="display:none;" validatemsg="Please enter Other State for joint account holder"></span>
<!--                                                                                                         <label class="cnd-mandatory mandatory_label">Other State(If not listed) <span class="mandatory_star">*</span></label> -->
<!--                                                                                                         <input name="ejd_state_other_1" id="ejd_state_other_1" class="form-control" value="" placeholder="Mention State Name" maxlength="60" type="text"> -->
<!--                                                                                                     </span> -->
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    <div class="divider"></div>
                                                                                    <div class="row">
                                                                                        <div class="col-xs-6">
                                                                                            <label class="mandatory">Telephone Number </label>
                                                                                            <div id="TelephoneAddWrap_1">
                                                                                                                                                                        <div class="row" id="phone_1_1">
                                                                                                                                                                                            <div class="col-xs-plus">
                                                                                                                    <span class="plus_sign">+</span>
                                                                                                                </div>
                                                                                                                <div class="col-xs-2"><input class="form-control numeric" name="ejd_phone_isd_1[]" id="ejd_phone_isd_1" value="91" placeholder="ISD" maxlength="3" title="Please enter Telephone ISD for Joint account holder" alt="Please enter Telephone ISD for Joint account holder" type="text"></div>
                                                                                                                <div class="col-xs-3"><input class="form-control numeric" name="ejd_phone_std_1[]" id="ejd_phone_std_1" value="" placeholder="STD" maxlength="5" title="Please enter Telephone STD for Joint account holder" alt="Please enter Telephone STD for Joint account holder" type="text"></div>
                                                                                                                <div class="col-xs-5 last"><input class="form-control numeric" name="ejd_phone_number_1[]" id="ejd_phone_number_1" value="" placeholder="Phone Number" maxlength="10" title="Please enter Telephone Number for Joint account holder" alt="Please enter Telephone Number for Joint account holder" type="text"></div>
                                                                                                                                                                                                    <a id="AddMoreFileBox" href="javascript:void(0);" rec_id="1" class=" btn-add a_phone" title="Click to Add Phone" alt="Click to Add Phone">+</a>
                                                                                                                                                                                        </div>
                                                                                                                                                            <input id="actual_phone_count_1" value="1" type="hidden">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-6">
                                                                                            <label class="mandatory">Office Number</label>
                                                                                            <div id="InputsWrapperOffice_1">
                                                                                                                                                                        <div class="row" id="office_1_1">
                                                                                                                                                                                            <div class="col-xs-plus">
                                                                                                                    <span class="plus_sign">+</span>
                                                                                                                </div>
                                                                                                                <div class="col-xs-2"><input class="form-control numeric" name="ejd_office_phone_isd_1[]" id="ejd_office_phone_isd_1" value="91" placeholder="ISD" maxlength="3" title="Please enter Office Phone ISD for Joint account holder" alt="Please enter Office Phone ISD for Joint account holder" type="text"></div>
                                                                                                                <div class="col-xs-3"><input class="form-control numeric" name="ejd_office_phone_std_1[]" id="ejd_office_phone_std_1" value="" placeholder="STD" maxlength="5" title="Please enter Office Phone STD for Joint account holder" alt="Please enter Office Phone STD for Joint account holder" type="text"></div>
                                                                                                                <div class="col-xs-5 last"><input class="form-control numeric" name="ejd_office_phone_number_1[]" id="ejd_office_phone_number_1" value="" placeholder="Office Number" maxlength="10" title="Please enter Office Phone Number for Joint account holder" alt="Please enter Office Phone Number for Joint account holder" type="text"></div>
                                                                                                                <a class=" btn-add a_office_phone" id="AddMoreFileBoxOffice" href="javascript:void(0);" rec_id="1" title="Click to Add Office Phone" alt="Click to Add Office Phone">+</a>
                                                                                                            </div>
                                                                                                                                                                        <input id="actual_office_count_1" value="1" type="hidden">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="divider"></div>
                                                                                    <div class="row">
                                                                                        <div class="col-xs-6">
                                                                                            <label class="mandatory ">Mobile Number </label>
                                                                                            <div id="MobileAddWrap_1">
                                                                                                                                                                                    <div class="row" id="mobile_1_1">
                                                                                                                                                                                                    <div class="col-xs-plus">
                                                                                                                        <span class="plus_sign">+</span>
                                                                                                                    </div>
                                                                                                                    <div class="col-xs-2"><input class="form-control numeric" name="ejd_mobile_isd_1[]" id="ejd_mobile_isd_1" value="91" placeholder="ISD" maxlength="3" data-validation="" data-validation-length="2-3" data-validation-error-msg="Please enter valid Mobile ISD" title="Please enter Mobile ISD for Joint account holder" alt="Please enter Mobile ISD for Joint account holder" type="text"></div>
                                                                                                                    <div class="col-xs-8 last"><input class="form-control numeric" name="ejd_mobile_number_1[]" id="ejd_mobile_number_1" value="" placeholder="Mobile" maxlength="10" data-validation="" data-validation-length="8-10" data-validation-error-msg="Please enter mobile no." title="Please enter Mobile Number for Joint account holder" alt="Please enter Mobile Number for Joint account holder" type="text"></div>
                                                                                                                                                                                                            <a class=" btn-add a_mobile" id="AddMoreFileBoxMobile" href="javascript:void(0);" rec_id="1" title="Click to Add Mobile" alt="Click to Add Mobile">+</a>
                                                                                                                                                                                                </div>
                                                                                                                                                                    <input id="actual_mobile_count_1" value="1" type="hidden">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-xs-6">
                                                                                                <label class="mandatory">Email</label>
                                                                                                <div id="JahEmailAddWrap_1">
                                                                                                    																		 <div class="row" id="jah_email_1_1">
                                                                                                                                                                                                    <div class="col-xs-8 last"><input class="form-control" name="ejd_email_1[]" id="ejd_email_1" value="" placeholder="Email" maxlength="254" data-validation="" data-validation-length="1-254" data-validation-error-msg="Please enter valid Email for Joint account holder" title="Please enter Email for Joint account holder" alt="Please enter Email for Joint account holder" type="text"></div>
                                                                                                                                                                                                            <a class=" btn-add a_jah_email" id="AddMoreFileBoxJahEmail" href="javascript:void(0);" rec_id="1" title="Click to Add Email" alt="Click to Add Email">+</a>
                                                                                                                                                                                                </div>
                                    																		                                                                <input id="actual_email_count_1" value="1" type="hidden">
                                                                                                </div>
                                                                                            </div>
                                                                                    </div>
                                                                                    <div class="divider"></div>
                                    												<div class="row">
                                    													<div class="col-xs-12">
                                    														<div class="row">
                                    															<div class="col-xs-4">
                                    																<label class="cnd-mandatory mandatory_label">Date of Birth <span class="mandatory_star">*</span></label>
                                    																<input id="show_ejd_birth_date_1" class="form-control" value="" placeholder="Birth date" readonly="" title="Please select a Birth date" alt="Please select a Birth date" ac_id="1" type="text">
                                    																<input name="ejd_birth_date_1" id="ejd_birth_date_1" class="form-control" value="0000-00-00" placeholder="Birth date" type="hidden">
                                    															</div>
                                    															<div class="col-xs-1">
                                    																<br><label class="cnd-mandatory">OR</label>

                                    															</div>
                                    															<div class="col-xs-2">
                                    																<label class="cnd-mandatory mandatory_label">Adult / Minor <span class="mandatory_star">*</span></label>
                                    																<select class="input-select form-control jah_is_adult" id="ejd_is_adult_1" name="ejd_is_adult_1" title="Please select if you are Adult/Minor" alt="Please select if you are Adult/Minor" ac_id="1">
                                    																	<option value="1" selected="">Adult</option>
                                    																	<option value="0">Minor</option>
                                    																</select>
                                    															</div>
                                    														</div>
                                    													</div>
                                    												</div>
                                                                                </div><!-- panel-body END -->
                                                                            </div>
                                                                            <a remove_id="1" href="javascript:void(0);" id="jointholderAddBtn" class="btn btn-primary btn-sm btn-add-large-red jointholderRemove a_removeaccount hide" title="Click to Remove Joint Account Holder" alt="Click to Remove Joint Account Holder">- Remove Joint Account Holder</a>
                                                                    </div>
                                                                </div><!-- JointHolder END -->

                                    							<div class="grdn-details" id="ejd_guardian_1">
                                    							</div><!-- grdn-details END -->
                                                    </div>
                                                </div>
                                            </div>
                                      </form>
