<form action="data_submit/frmForm.php" name="frmContingencySplCause" id="frmContingencySplCause" method="post" class="frmCurrent">
<input type="hidden" name="form_id" value="13">
        <div class="agreement-row">
			<div class="container">
								<div class="row">
										<div class="col-xs-12">
                    	<h4><strong>Contingent Beneficiary</strong>&nbsp; &nbsp; &nbsp;<a href="#" class="blue-italic-cities" data-toggle="modal" data-target="#myContingencyModal">Definition?</a></h4>
                        <div class="panel panel-body">
						  </div>
                    </div>
				</div><!-- Listing END -->
			</div>
        		<input type="hidden" name="auth_token" id="auth_token_name" value="TVRBMkxqSXlNeTQwT0M0eU1UUTBOSEJ4YjJwcFltSnNOMnc0TVhKMGMzQnRjemcwYVdWeE53PT0=" >
        		<input type="hidden" name="btn_clicked" id="btn_clicked" value="next" >
            	<div class="container">
					<div class="row">

					</div>
				<div class="row">

					<div class="col-xs-12">
                    	<div id="mfAddWrap">
						<input type="hidden" class="" name="contingency_spl_clause_process" id="contingency_spl_clause_process" value="1"  />						<input type="hidden" name="clause_id" id="clause_id" value="0"/>
                                    <div><!-- do not delte this div -->
                                        <!-- <h3>Contingent Beneficiary / Special Clause <span><a href="javascript:void(0);" id="mfAddBtn" class="btn btn-primary btn-sm btn-add-large">+ Add Contingent Beneficiary / Special Clause</a></span></h3> -->
										<h3>Add Contingency Clause</h3>
                                        <div class="panel panel-body">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <label class="mandatory mandatory_label">Contingency Clause (upto 200 characters)<span class="mandatory_star">*</span></label>
                                                    <textarea class="input-select form-control" name="contigency_clause[]" title='Please enter Contingency Clause' alt='Please enter Contingency Clause' maxlength="200" data-validation="length" data-validation-length="2-200" data-validation-error-msg="Please enter valid Contingency Clause"></textarea>
                                                </div>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label class="mandatory mandatory_label">Primary Beneficiary <span class="mandatory_star">*</span></label><br>
            										<select name="primary_beneficiary[]" id="primary_beneficiary[]" class="input-select form-control error" title='Please select a Primary Beneficiary' alt='Please select a Primary Beneficiary' data-validation='length' data-validation-length='1-7' data-validation-error-msg='Please select a valid Primary Beneficiary' onchange="javascript: changeHiddenPraimry(this.value);;"  ><option value="">Please Select</option></select>                                                </div>
                                            </div>
											<input type="hidden" id="sel_primary_beneficiary" value="" />
                                            <div class="divider"></div>
                                        </div><!-- panel-body END -->
                                    </div>

                                    <div id="div_beneficiary">
                                        <div id="div_beneficiary_1">
                                            <h4>
                                                <strong>Contingent Beneficiary</strong>
                                            </h4>
                                            <div class="panel panel-body sub-panel">
                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <label class="mandatory mandatory_label">Contingent Beneficiary <span class="mandatory_star">*</span></label>
                                                        <select name="sel_beneficiary" id="sel_beneficiary" class="input-select form-control" title='Please select a Contingent Beneficiary' alt='Please select a Contingent Beneficiary' ><option value="">Please select</option></select>                                                    </div>
                                                    <div class="col-xs-4">
													                                                        <label class="mandatory mandatory_label">Percentage Share to be allotted <span class="mandatory_star">*</span></label>
                                                        <input type="text" class="form-control numeric_share" name="txt_beneficiary_share" id="txt_beneficiary_share" placeholder="Percentage Share to be allotted" maxlength="5" title="Please enter Percentage Share to be allotted" alt="Please enter Percentage Share to be allotted" value=""/>


                                                    </div>
                                                    <div class="col-xs-4">
													                                                        <label class="mandatory">Any Other Information</label>
                                                        <input type="text" class="form-control" name="txt_other_info" id="txt_other_info" maxlength="200" placeholder="Any Other Information" title="Please enter Any Other Information" alt="Please enter Any Other Information" value=""/>
                                                    </div>
                                                </div>
												<div><br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add Contingent Beneficiary" title="Add Contingent Beneficiary" id="beneficiaryAddBtnNew" class="btn btn-primary btn-sm btn-add-large a_beneficiarynew">+ Add Contingent Beneficiary</a></span></div>
												<br/>
												<table class="table table-striped">
														  <thead>
															<tr>
															  <th>#</th>
															  <th>Name</th>
															  <th>Share</th>
															  <th>Other Information</th>
															  <th>Status</th>
															</tr>
														  </thead>
														  <tbody id="tb_con_ben"><tr id="no_beneficiary"><td colspan="5"></td></tr>
																																<input type="hidden" name="tot_ben" id="tot_ben" value="1" />
																<input type="hidden" id="total_share" value="0">

														  </tbody>
														</table>


                                            </div>
                                        </div>
                                    </div>
								</div>
                            </div>
							<div class="row">&nbsp;</div>
                        <p>&nbsp;</p>
						<div class="bottom_buttons">
							<div class="div_prev_button">
								<input type="button" name="previous" id="btn-previous" value="&laquo; Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="non-beneficiaries"/>
							</div>
							<div class="div_center_buttons">
								<input type="submit" name="add_contingent_bene" id="btn-add" value="Add / Save" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " />
								<input type="submit" name="update" id="btn-update" value="Update" title="Click here to Update the changed data" alt="Click here to Update the changed data" class="btn-submit btn-submit-disabled" disabled/>
							</div>
							<div class="div_next_button" style="text-align: right;">
								<input type="button" name="next" id="btn-next" value="Next &raquo;" title="Click here to go to Next page" alt="Click here to go to Next page" class="btn-submit btn-move" next_page="special-clause">
								<!--<input type="button" name="next" id="btn-next" value="Preview &raquo;" title="Click here to go to Preview page" alt="Click here to go to Preview page" class="btn-submit btn-move" next_page="special-clause"/> -->
							</div><br><br><br>
						</div>
						<input type="hidden" name="txt_other_submit"  id="txt_other_submit" value="0">
<input type="hidden" name="txt_redirect_page"  id="txt_redirect_page" value="">
<input type="submit" name="sumbit" value="Submit" class="btn-submit btn-submit-disabled" title="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" alt="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" disabled/>                    </div><!-- col-xs-12 END -->
                </div><!-- row END -->
			</div>
        </div><!-- gen-row END -->



    </div><!-- main END -->

  </form>
