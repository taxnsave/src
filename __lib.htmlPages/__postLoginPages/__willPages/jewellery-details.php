<?php
	include_once("../../../__lib.includes/config.inc.php");
	$wjewel_data		= $willProfile->getWillDetails('will_jewellery');
	$bene_data_all	= $willProfile->getWillBenDetails('will_beneficiary');
?>
<form action="data_submit/frmForm.php" name="frmJwl" id="frmJwl" method="post" class="frmCurrent">
    <input type="hidden" name="form_id" value="9">
    <div class="agreement-row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="bnkacAddWrap">
                        <div>
                            <!-- do not delte this div -->
                            <h3>Jewellery</h3>
                            <div class="panel panel-body sub-panel">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="mandatory mandatory_label">Type of jewellery <span class="mandatory_star">*</span></label>
                                        <br>
                                        <?php $selted_jewl=$wjewel_data->jwl_type; ?>
                                        <select class="form-control" name="jwl_type" id="jwl_type">
                                            <option value="" <?= ($selted_jewl == "")?"selected":"" ?> >Select</option>
                                            <option value="gold" <?= ($selted_jewl == "gold")?"selected":"" ?> >Gold</option>
                                            <option value="silver" <?= ($selted_jewl == "silver")?"selected":"" ?> >Silver</option>
                                            <option value="dimond" <?= ($selted_jewl == "dimond")?"selected":"" ?> >Diamond</option>
                                            <option value="other" <?= ($selted_jewl == "other")?"selected":"" ?> >Other</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="mandatory mandatory_label">Name <span class="mandatory_star">*</span></label><br>
                                        <input type="text" class="form-control" name="jwl_name" value="<?= $wjewel_data->jwl_name; ?>" placeholder="Policy Number" maxlength="20" title="Please enter Policy number" alt="Please enter Policy number" data-validation="" data-validation-length="1-20" data-validation-error-msg="Please enter valid Policy number" required/>
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="mandatory mandatory_label">Amount <span class="mandatory_star">*</span></label><br>
                                        <input type="text" class="form-control" name="jwl_amount" value="<?= $wjewel_data->jwl_amount; ?>" placeholder="Policy Number" maxlength="20" title="Please enter Policy number" alt="Please enter Policy number" data-validation="" data-validation-length="1-20" data-validation-error-msg="Please enter valid Policy number" required/>
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div class="row">
                                    <div class="col-xs-7">
                                        <label class="mandatory mandatory_label">Description <span class="mandatory_star">*</span></label><br>
                                        <textarea name="jwl_description" id="jwl_description" cols="30" rows="3" class="form-control"><?= $wjewel_data->jwl_desc; ?></textarea>
                                        <!-- <input type="date" id="policy_start_date" name="policy_start_date" class="form-control" value="<?= $wjewel_data->policy_start_date; ?>" placeholder="Policy Start Date" title="Please select Policy Start Date" alt="Please select Policy Start Date" data-validation="" data-validation-length="1-20" data-validation-error-msg="Please select valid Policy Start Date" required/> -->
                                    </div>
                                </div>
                                <!-- row END -->
                                <div class="divider"></div>
                            </div>
                            <!-- panel-body END -->
                        </div>
                    </div>
                </div>
                <!-- col-xs-12 END -->
            </div>
            <!-- row END -->
            <input type="hidden" name="has_nominee" value="no">
            <div class="row">
                <div class="col-xs-12">
                    <div id="guardiant_main">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="div_beneficiary">
                        <div id="div_beneficiary_1">
                            <h4>
										<strong>Beneficiaries</strong>
									</h4>
                            <div class="panel panel-body sub-panel">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Beneficiary <span class="mandatory_star">*</span></label>
                                        <select name="sel_beneficiary" id="sel_beneficiary" class="input-select form-control" title='Please select a Beneficiary' alt='Please select a Beneficiary'>
                                            <option value="">Please select</option>
                                            <?php 
                                                $cnt=1;
                                                while($row = mysql_fetch_array($bene_data_all)) {
                                                    $html = "<option value='" . $cnt . "'>" .$row['f_name'] . "</option>";
                                                    
                                                    $cnt+= 1;
                                                    echo $html;
                                                } 
                                                ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Percentage Share to be allotted <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control numeric_share" name="txt_beneficiary_share" id="txt_beneficiary_share" placeholder="Percentage Share to be allotted" maxlength="5" title="Please enter Percentage Share to be allotted" alt="Please enter Percentage Share to be allotted" value="" />
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Asset wise<span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control numeric_share" name="txt_asset_wise" id="txt_asset_wise" placeholder="Assetwise Share to be allotted" title="Please enter Asset wise Share to be allotted" alt="Please enter Asset wise share to be allotted" value="" />
                                    </div>
                                </div>
                                <div>
                                    <br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add Beneficiary" title="Add Beneficiary" id="beneficiaryAddBtnNew" class="btn btn-primary btn-sm btn-add-large a_beneficiarynew">+ Add Beneficiary</a></span></div>
                                <br/>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Relation</th>
                                            <th>Share</th>
                                            <th><a href="" class="btn btn-danger">Reset all</a></th>
                                        </tr>
                                    </thead>
                                    <tbody id="tb_con_ben">
                                        <tr id="no_beneficiary">
                                            <td colspan="5"></td>
                                        </tr>
                                        <input type="hidden" name="tot_ben" id="tot_ben" value="1" />
                                        <input type="hidden" id="total_share" value="0">
                                    </tbody>
                                </table>
                            </div>
                            <div>
<!--                                 <br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add More Beneficiary" title="Add More Beneficiary" id="" class="btn btn-success btn-sm">+ Add More</a></span> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">&nbsp;</div>
            <div class="bottom_buttons row">
                <div class="div_prev_button">
                    <input type="button" name="previous" id="btn-previous" value="&laquo; Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="pension-fund" />
                </div>
                <div class="div_next_button" style="margin-right: -30px; text-align: right;">
                    <input type="submit" name="add_jewel" id="btn-add" value="Save & Next »" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " />
						</div><br><br><br>
            </div>
            <div class="row">
            </div>
        </div>
    </div>
</form>