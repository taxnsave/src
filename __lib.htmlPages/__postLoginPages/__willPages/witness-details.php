<?php
	include_once("../../../__lib.includes/config.inc.php");
	$wwit_data		= $willProfile->getWillDetails('will_witness');
	
?>
<form action="data_submit/frmForm.php" name="frmLiability" id="frmLiability" method="post" class="frmCurrent">
    <input type="hidden" name="form_id" value="12">
    <div class="agreement-row">
        <div class="container">
            <h3>Witness 1</h3>
            <div class="row">
                <div class="col-xs-12">
                    <div id="bnkacAddWrap">
                        <div>
                            <!-- do not delte this div -->
                            <div class="panel panel-body">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <label class="mandatory mandatory_label">Name <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="witness1_name" value="<?= $wwit_data->witness1_name; ?>" placeholder="Name of the Individual / Institution" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Name of the Individual / Institution" title="Please enter Name of the Individual / Institution" alt="Please enter Name of the Individual / Institution" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Phone <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="witness1_phone" value="<?= $wwit_data->witness1_phone; ?>" placeholder="Name of the Individual / Institution" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Name of the Individual / Institution" title="Please enter Name of the Individual / Institution" alt="Please enter Name of the Individual / Institution" required/>
                                    </div>
                                
                                </div>
                                <!-- row END -->
                                <div class="divider"></div>
                                <div class="row">
                                    <h4><strong>Address of the witness 1</strong></h4>
                                    <div class="col-xs-4">
                                        <label class="mandatory ">Address Line1
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="witness1_addr_line1" value="<?= $wwit_data->witness1_addr_line1; ?>" placeholder="Address Line1" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1" title="Please enter Address Line1" alt="Please enter Address Line1" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory">Address Line2
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="witness1_addr_line2" value="<?= $wwit_data->witness1_addr_line2; ?>" placeholder="Address Line2" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2" title="Please enter Address Line2" alt="Please enter Address Line2" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        <label>Address Line3</label>
                                        <input type="text" class="form-control" name="witness1_addr_line3" value="<?= $wwit_data->witness1_addr_line3; ?>" placeholder="Address Line3" maxlength="60" title="Please enter Address Line3" alt="Please enter Address Line3" />
                                    </div>
                                </div>
                                <!-- row END -->
                                <div class="divider"></div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="mandatory">City / Village / Town
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="witness1_city_village_town" value="<?= $wwit_data->witness1_city; ?>" placeholder="City/Town/Village" maxlength="60" title="Please enter City / Village / Town" alt="Please enter City / Village / Town" alt="Please enter City / Village / Town" alt="Please enter City / Village / Town" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town" required>
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="mandatory ">Pincode / Zipcode
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control numeric" name="witness1_zipcode" id="witness1_zipcode" value="<?= $wwit_data->witness1_zipcode; ?>" placeholder="Pincode" maxlength="6" data-validation="" data-validation-length="1-6" data-validation-error-msg="Please enter Pincode" title="Please enter Pincode" alt="Please enter Pincode" required>
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="mandatory">Country
                                            <span class="mandatory_star">*</span></label>
                                        <select name="witness1_country" id="witness1_country" class="input-select form-control" title='Please select a Country' alt='Please select a Country' onchange="javascript: populate_states(this.value,'state', 'span_state', '0','toggle_other_state(\'state\',\'state_other\')'); toggle_other_state_country(this.value, 'state_other');" required>
                                            <option value="102" selected="selected">India</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-2">
                                    <?php $corre_state=$wwit_data->witness1_state; ?>
                                        <label class="mandatory ">State
                                            <!-- <span class="mandatory_star">*</span> --></label> <span id="span_state">
											<select name="witness1_state" id="witness1_state"
											class="input-select form-control">
											<option value="35" <?php echo ($corre_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($corre_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($corre_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($corre_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($corre_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($corre_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($corre_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($corre_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($corre_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($corre_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($corre_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($corre_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($corre_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($corre_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($corre_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($corre_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($corre_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($corre_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($corre_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($corre_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($corre_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($corre_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($corre_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($corre_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($corre_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($corre_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($corre_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($corre_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($corre_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($corre_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($corre_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($corre_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($corre_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($corre_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($corre_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($corre_state == 19)?"selected":"" ?>>West Bengal</option>
                                        
												</select>
										</span>
									</div>
                                    <div class="col-xs-3">
                                        <span id="witness1_state_other" style='display:none;'>
                                                    <label class="cnd-mandatory ">Other State (If not listed) <!-- <span class="mandatory_star">*</span> --></label>
                                        <input type="text" class="form-control" name="witness1_state_other" value="" placeholder="Mention State Name" maxlength="60" title="Please enter Other State" alt="Please enter Other State" />
                                        </span>
                                    </div>
                                </div>
                                <!-- row END witness 1-->
                                <div class="row">
                                    <h3>Witness 2</h3>
                                    <div class="col-xs-2">
                                        <label class="mandatory mandatory_label">Name <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="witness2_name" value="<?= $wwit_data->witness2_name; ?>" placeholder="Name of the Individual / Institution" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Name of the Individual / Institution" title="Please enter Name of the Individual / Institution" alt="Please enter Name of the Individual / Institution" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Phone <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="witness2_phone" value="<?= $wwit_data->witness2_phone; ?>" placeholder="Name of the Individual / Institution" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Name of the Individual / Institution" title="Please enter Name of the Individual / Institution" alt="Please enter Name of the Individual / Institution" required/>
                                    </div>
                                    
                                </div>
                                <!-- row END -->
                                <div class="divider"></div>
                                <div class="row">
                                    <h4><strong>Address of the witness 2</strong></h4>
                                    <div class="col-xs-4">
                                        <label class="mandatory ">Address Line1
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="witness2_addr_line1" value="<?= $wwit_data->witness2_addr_line1; ?>" placeholder="Address Line1" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1" title="Please enter Address Line1" alt="Please enter Address Line1" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory">Address Line2
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="witness2_addr_line2" value="<?= $wwit_data->witness2_addr_line2; ?>" placeholder="Address Line2" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2" title="Please enter Address Line2" alt="Please enter Address Line2" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        <label>Address Line3</label>
                                        <input type="text" class="form-control" name="witness2_addr_line3" value="<?= $wwit_data->witness2_addr_line3; ?>" placeholder="Address Line3" maxlength="60" title="Please enter Address Line3" alt="Please enter Address Line3" />
                                    </div>
                                </div>
                                <!-- row END -->
                                <div class="divider"></div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="mandatory">City / Village / Town
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control" name="witness2_city_village_town" value="<?= $wwit_data->witness2_city; ?>" placeholder="City/Town/Village" maxlength="60" title="Please enter City / Village / Town" alt="Please enter City / Village / Town" alt="Please enter City / Village / Town" alt="Please enter City / Village / Town" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town" required>
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="mandatory ">Pincode / Zipcode
                                            <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control numeric" name="witness2_zipcode" id="witness2_zipcode" value="<?= $wwit_data->witness2_zipcode; ?>" placeholder="Pincode" maxlength="6" data-validation="" data-validation-length="1-6" data-validation-error-msg="Please enter Pincode" title="Please enter Pincode" alt="Please enter Pincode" required>
                                    </div>
                                    <div class="col-xs-2">
                                        <label class="mandatory">Country
                                            <span class="mandatory_star">*</span></label>
                                        <select name="witness2_country" id="witness2_country" class="input-select form-control" title='Please select a Country' alt='Please select a Country' onchange="javascript: populate_states(this.value,'state', 'span_state', '0','toggle_other_state(\'state\',\'state_other\')'); toggle_other_state_country(this.value, 'state_other');" required>
                                            <option value="102" selected="selected">India</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-2">
                                    <?php $corre_state=$wwit_data->witness2_state; ?>
                                        <label class="mandatory ">State
                                            <!-- <span class="mandatory_star">*</span> --></label> <span id="span_state">
											<select name="witness2_state" id="witness2_state"
											class="input-select form-control">
											<option value="35" <?php echo ($corre_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($corre_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($corre_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($corre_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($corre_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($corre_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($corre_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($corre_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($corre_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($corre_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($corre_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($corre_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($corre_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($corre_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($corre_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($corre_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($corre_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($corre_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($corre_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($corre_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($corre_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($corre_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($corre_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($corre_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($corre_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($corre_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($corre_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($corre_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($corre_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($corre_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($corre_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($corre_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($corre_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($corre_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($corre_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($corre_state == 19)?"selected":"" ?>>West Bengal</option>
                                        
												</select>
										</span>
									</div>
                                    <div class="col-xs-3">
                                        <span id="witness2_state_other" style='display:none;'>
                                                    <label class="cnd-mandatory ">Other State (If not listed) <!-- <span class="mandatory_star">*</span> --></label>
                                        <input type="text" class="form-control" name="witness2_state_other" value="" placeholder="Mention State Name" maxlength="60" title="Please enter Other State" alt="Please enter Other State" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!-- panel-body END -->
                        </div>
                    </div>
                </div>
                <!-- col-xs-12 END -->
            </div>
            <div class="row">&nbsp;</div>
            <div class="bottom_buttons row">
                <div class="div_prev_button">
                    <input type="button" name="previous" id="btn-previous" value="&laquo; Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="digital-assets-details" />
                </div>
                <div class="div_next_button" style="margin-right: -30px; text-align: right;">
                    <input type="submit" name="add_witness" id="btn-add" value="Submit" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " />
                </div>
                <br>
                <br>
                <br>
            </div>
        </div>
    </div>
    <!-- gen-row END -->
    <!-- main END -->
</form>