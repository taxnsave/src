<ul class="nav nav-tabs bank4">
<li class="active"><a data-toggle="tab" data="../ajax-request/will_request.php?action=sub_retirement-plan-details">Provident Fund (PF)</a></li>
<li><a data-toggle="tab" data="../ajax-request/will_request.php?action=sub_pension-fund">Pension Fund</a></li>
<li><a data-toggle="tab" data="../ajax-request/will_request.php?action=sub_gratuity-fund">Gratuity</a></li>
</ul>

<div class="panel-body-small4">

</div>

<!-- <script src="js/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7js/bootstrap.min.js"></script> -->
<script type="text/javascript">

function showsubsubtab(urltoload) {
    $.ajax({
        url: urltoload,
        type: 'get',
        success: function(data) {
            $('.panel-body-small4').html(data);
        },
        error: function(error) {

        }
    });
}

  $(document).ready(function(){
    // alert("hello");
    $('.bank4 li a').click(function(){
      var list = $(this).attr("data");
      $.ajax({
        url: list,
        type: 'get',
        success: function(data){
          $('.panel-body-small4').html(data);
        },
        error: function(error){

        }
      });
    });

  });


  var redirparam = url.searchParams.get("subpage2");
  $(".bank4 li").removeClass("active"); //Remove any "active" class
  /* if(redirparam == "mf") {
  	
  } else */ 
  if(redirparam == "pen") {
  	$(".bank4 li:eq(1)").addClass("active").show();
  	showsubsubtab("../ajax-request/will_request.php?action=sub_pension-fund");
  } else if(redirparam == "gra") {
  	$(".bank4 li:last").addClass("active").show();
  	showsubsubtab("../ajax-request/will_request.php?action=sub_gratuity-fund");
  } else {
	  $(".bank4 li:first").addClass("active").show();
	  	showsubsubtab("../ajax-request/will_request.php?action=sub_retirement-plan-details");
  }
  
</script>
