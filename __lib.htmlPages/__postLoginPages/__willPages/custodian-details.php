<?php
    include_once ("../../../__lib.includes/config.inc.php");
    $wcust_data = $willProfile->getWillDetails('will_custodian');
?>
<form name="frmCustodian" action="data_submit/frmform.php"
	id="frmCustodian" method="post" class="frmCurrent" autocomplete="off">
	<input type="hidden" name="form_id" value="12">
	<div class="agreement-row">
		<div class="container">
			<h3>Custodian</h3>
			<div class="panel panel-body sub-panel">
				<div class="row">
					<div class="col-xs-4">
						<label class="mandatory mandatory_label">Custodian name <span
							class="mandatory_star">*</span></label> <br> <input type="text"
							class="form-control" name="cust_name"
							value="<?php echo $wcust_data->custodian_name; ?>"
							placeholder="custodian name" maxlength="20"
							title="please enter custodian name"
							alt="please enter custodian name" data-validation=""
							data-validation-error-msg="please enter valid custodian name"
							required />
					</div>
					<div class="col-xs-4">
						<label class="mandatory mandatory_label">Custodian father name<span
							class="mandatory_star">*</span>
						</label><br> <input type="text" class="form-control"
							name="cust_fat_name"
							value="<?php echo $wcust_data->custodian_father_name; ?>"
							placeholder="custodian father name" maxlength="20"
							title="please enter custodian father name"
							alt="please enter custodian father name" data-validation=""
							data-validation-error-msg="please enter valid custodian father name"
							required />
					</div>
				</div>
				<div class="divider"></div>
				<div class="row">
					<div class="col-xs-4">
						<label class="mandatory ">Address line1 <span
							class="mandatory_star">*</span></label> <input
							class="form-control" name="cust_address_line1"
							value="<?php echo $wcust_data->custodian_addr_line1; ?>"
							placeholder="address line1" maxlength="60" data-validation=""
							data-validation-length="1-60"
							data-validation-error-msg="please enter valid address line1"
							title="please enter address line1"
							alt="please enter address line1" type="text" required>
					</div>
					<div class="col-xs-4">
						<label class="mandatory ">Address line2 <span
							class="mandatory_star">*</span></label> <input
							class="form-control" name="cust_address_line2"
							value="<?php echo $wcust_data->custodian_addr_line2; ?>"
							placeholder="address line2" maxlength="60" data-validation=""
							data-validation-length="1-60"
							data-validation-error-msg="please enter valid address line2"
							title="please enter address line2"
							alt="please enter address line2" type="text" required>
					</div>
					<div class="col-xs-4">
						<label class="mandatory">Address line3</label> <input
							class="form-control" name="cust_address_line3"
							value="<?php echo $wcust_data->custodian_addr_line3; ?>"
							placeholder="address line3" maxlength="60"
							title="please enter address line3"
							alt="please enter address line3" type="text">
					</div>
				</div>
				<!-- row end -->
				<div class="divider"></div>
				<div class="row">
					<div class="col-xs-3">
						<label class="mandatory">City / Village / Town </label> <input
							class="form-control" name="cust_city_village_town"
							value="<?php echo $wcust_data->custodian_city; ?>"
							placeholder="city / village / town" maxlength="30"
							title="please enter city / village / town"
							alt="please enter city / village / town" data-validation=""
							data-validation-length="1-60"
							data-validation-error-msg="please enter valid city / village / town"
							type="text">
					</div>
					<div class="col-xs-2">
					<?php $corre_state=$wcust_data->custodian_state; ?>
						<label class="mandatory ">State </label> <span id="span_state"> <select
							name="cust_state" id="cust_state"
							class="input-select form-control"
							title="please select appropriate state" data-validation=""
							data-validation-length="1-60"
							data-validation-error-msg="please select a valid state">
								<option value="35" <?php echo ($corre_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($corre_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($corre_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($corre_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($corre_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($corre_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($corre_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($corre_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($corre_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($corre_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($corre_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($corre_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($corre_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($corre_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($corre_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($corre_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($corre_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($corre_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($corre_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($corre_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($corre_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($corre_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($corre_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($corre_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($corre_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($corre_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($corre_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($corre_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($corre_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($corre_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($corre_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($corre_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($corre_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($corre_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($corre_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($corre_state == 19)?"selected":"" ?>>West Bengal</option>
                                        
						</select>
						</span>
					</div>
					<div class="col-xs-3">
						<span id="span_state_other" style="display: none"> <label
							class="cnd-mandatory">other state (if not listed) </label> <input
							class="form-control" name="cust_state_other" value=""
							placeholder="mention state name" maxlength="60"
							title="please enter a other state"
							alt="please enter a other state" type="text">
						</span>
					</div>
					<div class="col-xs-2">
						<label class="mandatory ">Pincode <!-- <span class="mandatory_star">*</span> --></label>
						<input class="form-control numeric" name="cust_zipcode"
							value="<?php echo $wcust_data->custodian_zipcode; ?>"
							placeholder="pincode" maxlength="6" data-validation=""
							data-validation-length="6-6"
							data-validation-error-msg="please enter valid pincode."
							title="please enter pincode" alt="please enter pincode"
							type="text">
					</div>
					<div class="col-xs-2">
						<label class="mandatory ">Country <!-- <span class="mandatory_star">*</span> --></label>
						<select name="cust_country" id="cust_country"
							class="input-select form-control" title="please select a country"
							data-validation="" data-validation-length="1-33"
							data-validation-error-msg="please select a valid country"
							onchange="javascript: populate_states(this.value,'state', 'span_state', '0','toggle_other_state(\'state\',\'state_other\')'); toggle_other_state_country(this.value, 'span_state_other');;">
							<option value="102" selected="selected">India</option>
						</select>
					</div>
				</div>
				<div class="devider"></div>
				<div class="row">
					<div class="col-xs-3">
						<label class="cnd-mandatory mandatory_label">Age <span
							class="mandatory_star">*</span>
						</label> <input type="text" id="cust_age" name="cust_age"
							class="form-control"
							value="<?php echo $wcust_data->custodian_age; ?>"
							placeholder="age" title="age" alt="age" required />
					</div>
					<div class="col-xs-2">
						<label class="mandatory_label">Religion <span
							class="mandatory_star">*</span>
						</label> <select name="cust_religion" id="cust_religion"
							class="input-select form-control"
							title="please select appropriate religion from the dropdown list"
							data-validation="length" data-validation-length="1-20"
							data-validation-error-msg="please select a religion"
							onchange="javascript: toggle_other_religion(this.value, 'span_religion_other');"
							required>
							<option value=""
								<?php echo ($wcust_data->custodian_religion == "")?"selected":"" ?>>Please
								Select</option>
							<option value="1"
								<?php echo ($wcust_data->custodian_religion == 1)?"selected":"" ?>>Buddhist</option>
							<option value="2"
								<?php echo ($wcust_data->custodian_religion == 2)?"selected":"" ?>>Christian</option>
							<option value="3"
								<?php echo ($wcust_data->custodian_religion == 3)?"selected":"" ?>>Hindu</option>
							<option value="4"
								<?php echo ($wcust_data->custodian_religion == 4)?"selected":"" ?>>Islam</option>
							<option value="5"
								<?php echo ($wcust_data->custodian_religion == 5)?"selected":"" ?>>Jain</option>
							<option value="6"
								<?php echo ($wcust_data->custodian_religion == 6)?"selected":"" ?>>Judaism</option>
							<option value="7"
								<?php echo ($wcust_data->custodian_religion == 7)?"selected":"" ?>>Parsi</option>
							<option value="8"
								<?php echo ($wcust_data->custodian_religion == 8)?"selected":"" ?>>Sikh</option>
							<option value="10"
								<?php echo ($wcust_data->custodian_religion == 10)?"selected":"" ?>>Other</option>
						</select>
					</div>
					<div class="col-xs-3">
						<label class="mandatory mandatory_label">Nationality <span
							class="mandatory_star">*</span>
						</label> <select class="input-select form-control"
							id="nationality" name="nationality" data-validation="length"
							data-validation-length="1-33"
							data-validation-error-msg="please enter a valid nationality"
							title="please select appropriate nationality from the dropdown list"
							required>
							<option value=""
								<?php echo ($wcust_data->custodian_nationality == "")?"selected":"" ?>>Please
								Select</option>
							<option value="Indian"
								<?php echo ($wcust_data->custodian_nationality == "Indian")?"selected":"" ?>>Indian</option>
							<option value="Other"
								<?php echo ($wcust_data->custodian_nationality == "Other")?"selected":"" ?>>Other</option>
						</select>
					</div>

					<!-- panel-body end -->
				</div>
			</div>


			<div class="bottom_buttons row">
				<div class="div_prev_button">
					<input type="button" name="previous" id="btn-previous"
						value="&laquo; Prev" title="click here to go on previous page"
						alt="click here to go on previous page"
						class="btn-submit btn-move" />
				</div>
				<div class="div_next_button"
					style="margin-right: -30px; text-align: right;">
					<input type="submit" name="add_custodian" id="btn-add"
						value="Save & Next »"
						title="click here to add / save the entered data"
						alt="click here to add / save the entered data"
						class="btn-submit " />
				</div>
				<br> <br> <br>
			</div>
			<div class="row"></div>
			<!-- row end -->
		</div>
	</div>

</form>