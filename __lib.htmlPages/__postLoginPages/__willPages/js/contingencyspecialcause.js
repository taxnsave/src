var clause_count = 1;
var actual_clause_count = 1;

var beneficiary_count = 1;
var actual_beneficiary_count = 1;
var total_count = 1;
$(document).ready(function(){
    $.validate();
    on_ready_functions();
	add_functions();
	$('.numeric').numeric();
	$('.numeric_share').numeric({decimal:"."});
	$(".ben_edit").click(function(){
		var clause_id = $(this).attr("rec_id");
		$("#hid_clause_id").val(clause_id);
		document.frmEditContingencySplCause.submit();
    });

	$(".ben_delete").click(function(){
		
		if( !confirm("Are you sure you want to delete this Contingency clause?") )
            return;
        var filtervars = {
            do_what:'del_cont_special_clause',
            delete_ids: $(this).attr("rec_id"),
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/contingency-special-clause_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                alert("Contingency Clause details deleted successfully");
                window.location.reload('non-beneficiaries.php');
            }
        });
    });

    //$(".mfRemove").unbind("click");
    $(".mfRemove").click(function(){
        var clause_id = $(this).attr('remove_id');
        alert(clause_id);
        $('#num'+clause_id).remove();
        add_functions();
    });
	


	$('#sel_beneficiary').change(function(){
	if($('#sel_primary_beneficiary').val() == this.value)
		{
			alert('Please select different Primary and Contingent Beneficiary');
			$('#sel_beneficiary').val('');
			$('#txt_beneficiary_share').val('');
			$('#txt_other_info').val('');
			$('#beneficiaryAddBtnNew').show();
			$('#sel_beneficiary').attr('style', ' ');
			$('#sel_beneficiary_error').remove();
			$('#txt_beneficiary_share').attr('style', ' ');
			$('#txt_beneficiary_share_error').remove();
		}
	});

});

function on_ready_functions(){
	$('.numeric').numeric();
	$('.numeric_share').numeric({decimal:"."});
    $("#mfAddBtn").unbind("click");

    $("#mfAddBtn").click(function(){
        //alert(clause_count);
        clause_count++;
        actual_clause_count++;

        var filtervars = {
            do_what:'add_ont_special_clause',
            clause_count:clause_count,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/contingency-special-clause_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
               $('#mfAddWrap').append(html);
               $.validate();
               add_functions();
               $('.numeric').numeric();
               $('.numeric_share').numeric({decimal:"."});
            }
        });
    });
	

	
    /* Beneficiary */
    $(".a_beneficiary").unbind("click");
    $(".a_beneficiary").click(function(){
		var current_beneficiary = $(this).attr("rec_id");
		beneficiary_count++;
        actual_beneficiary_count++;
        var filtervars = {
            do_what:'add_beneficiary',
            beneficiary_count:beneficiary_count,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/contingency-special-clause_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $(".a_beneficiary").remove();
                $("#div_beneficiary").append(html);
                add_functions();
            }
        });
    });
	 /* Beneficiary */
    $(".a_removebeneficiary").unbind("click");
    $(".a_removebeneficiary").click(function(){
        //alert(actual_beneficiary_count);
        if(actual_beneficiary_count==1){
            //actual_beneficiary_count++;
            alert("You can not delete this Contingenct Beneficiary, Please add at-least one Contingenct Beneficiary.");
            return;
        }
        var remove_id = $(this).attr("remove_id");
        actual_beneficiary_count--;
        $("#div_beneficiary_"+remove_id).remove();

        $("#account_"+remove_id).remove();
        var temp = $('.a_removebeneficiary').length;
        temp --;
        
        $('#span_beneficiary_'+$('.a_removebeneficiary').eq(temp).attr('remove_id')).html('<a class="btn btn-primary btn-sm btn-add-large a_beneficiary" id="beneficiaryAddBtn" title="Add Contingenct Beneficiary" alt="Add Contingenct Beneficiary" href="javascript:void(0);">+ Add Contingenct Beneficiary</a>');
        add_functions();
    });

    /* Beneficiary */
    add_functions();
}

function add_functions(){
	$.validate();
    $('.numeric').numeric();
    $('.numeric_share').numeric({decimal:"."});
    $(".mfRemove").unbind("click");
    $(".mfRemove").click(function(){
        var clause_id = $(this).attr('remove_id');
        $('#num'+clause_id).remove();
        add_functions();
    });

	$(".a_beneficiary").unbind("click");
    $(".a_beneficiary").click(function(){
		var current_beneficiary = $(this).attr("rec_id");
		beneficiary_count++;
        actual_beneficiary_count++;
        var filtervars = {
            do_what:'add_beneficiary',
            beneficiary_count:beneficiary_count,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/contingency-special-clause_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $(".a_beneficiary").remove();
                $("#div_beneficiary").append(html);
                add_functions();
				$.validate();
            }
        });
    });
	
	/* Beneficiary */
    $(".a_removebeneficiary").unbind("click");
    $(".a_removebeneficiary").click(function(){
        //alert(actual_beneficiary_count);
        if(actual_beneficiary_count==1){
            //actual_beneficiary_count++;
            alert("You can not delete this Contingenct Beneficiary, Please add at-least one Contingenct Beneficiary.");
            return;
        }
        var remove_id = $(this).attr("remove_id");
        actual_beneficiary_count--;
        $("#div_beneficiary_"+remove_id).remove();

        $("#account_"+remove_id).remove();
        var temp = $('.a_removebeneficiary').length;
        temp --;
        
        $('#span_beneficiary_'+$('.a_removebeneficiary').eq(temp).attr('remove_id')).html('<a class="btn btn-primary btn-sm btn-add-large a_beneficiary" id="beneficiaryAddBtn" title="Add Contingenct Beneficiary" alt="Add Contingenct Beneficiary" href="javascript:void(0);">+ Add Contingenct Beneficiary</a>');
        add_functions();
		$.validate();
    });

	

	$(".a_beneficiarynew").unbind("click");
    $(".a_beneficiarynew").click(function(){
		var ben_id, share, other_info, ben_name;
		var ben_exist = false;
		var bcount = $("#tot_ben").val();
		var total_share = $('#total_share').val();
		ben_name = $("#sel_beneficiary option:selected").text();
		ben_id = $("#sel_beneficiary").val();
		share = $("#txt_beneficiary_share").val();
		other_info = $("#txt_other_info").val();
		total_share = parseInt(total_share) + parseInt(share);
		//if($('#total_share').val() != 100){
			if($('#primary_beneficiary').val() == ben_id)
			{
				alert('Please select different Primary and Contingent Beneficiary');
				clear_add_beneficiary();
			}
			if(ben_id<=0 && (parseInt(share)<=0 || share=="" || share>100)){
				$('#sel_beneficiary').attr('style', ' ');
				$('#sel_beneficiary_error').remove();
				$('#sel_beneficiary').attr('style', 'border-top-color: red; border-right-color: red; border-bottom-color: red; border-left-color: red; ');
				($('#sel_beneficiary').parent()).append('<span class="help-block form-error error_ben" id="sel_beneficiary_error">Please select a Beneficiary</span>');
				$('#txt_beneficiary_share').attr('style', ' ');
				$('#txt_beneficiary_share_error').remove();
				$('#txt_beneficiary_share').attr('style', 'border-top-color: red; border-right-color: red; border-bottom-color: red; border-left-color: red; ');
				($('#txt_beneficiary_share').parent()).append('<span class="help-block form-error error_ben" id="txt_beneficiary_share_error">Please enter valid Percentage Share to be allotted</span>');
				return false;
			}
			if(ben_id<=0){
				$('#sel_beneficiary').attr('style', ' ');
				$('#sel_beneficiary_error').remove();
				$('#sel_beneficiary').attr('style', 'border-top-color: red; border-right-color: red; border-bottom-color: red; border-left-color: red; ');
				($('#sel_beneficiary').parent()).append('<span class="help-block form-error error_ben" id="sel_beneficiary_error">Please select a Beneficiary</span>');
				return false;
			}
			else{
				$('#sel_beneficiary').attr('style', ' ');
				$('#sel_beneficiary_error').remove();
			}
			if(parseInt(share)<=0 || share=="" || share>100){
				$('#txt_beneficiary_share').attr('style', ' ');
				$('#txt_beneficiary_share_error').remove();
				$('#txt_beneficiary_share').attr('style', 'border-top-color: red; border-right-color: red; border-bottom-color: red; border-left-color: red; ');
				($('#txt_beneficiary_share').parent()).append('<span class="help-block form-error error_ben" id="txt_beneficiary_share_error">Please enter valid Percentage Share to be allotted</span>');
				return false;
			}
			else{
				$('#txt_beneficiary_share').attr('style', ' ');
				$('#txt_beneficiary_share_error').remove();
			}
			if(total_share > 100)
			{
				alert("Contigent Beneficiary total Percentage Share exceeds 100%");
				return false;
			}
			if(total_share == 100)
			{
				$('#beneficiaryAddBtnNew').hide();
			}
			$('.cls_ben').each(function( index ) {  
				if(ben_id == this.value){
					$('#sel_beneficiary').attr('style', 'border-top-color: red; border-right-color: red; border-bottom-color: red; border-left-color: red; ');
					($('#sel_beneficiary').parent()).append('<span class="help-block form-error error_ben" id="sel_beneficiary_error">Beneficiary already exists, Please select different beneficiary</span>');
					ben_exist = true;
				}
			}); 
			if(ben_exist == true)
			{
				$('#beneficiaryAddBtnNew').show();
				return false;
			}

			if(bcount == 1){
				$('#no_beneficiary').remove();
			}
			
			other_info = other_info.replace(new RegExp('"','g'), '&#34;');
			
			$("#tb_con_ben").append('<tr id="tr_con_ben_'+bcount+'"><td class="ben_new_account_numbering">'+bcount+'</td><td>'+ben_name+'<input type="hidden" class="w120px" name="ben_id[]" value="0"/><input type="hidden" class="cls_ben" name="beneficiary[]" value="'+ben_id+'"></td><td>'+share+'%<input type="hidden" name="beneficiary_share[]" value="'+share+'"></td><td>'+other_info+'<input type="hidden" name="other_info[]" value="'+other_info+'"></td><td><a href="javascript:void(0);" class="a_removebeneficiarynew" trid="tr_con_ben_'+bcount+'" trshare="'+share+'">Delete</a></td></tr>');
			
			$("#tot_ben").val(parseInt(bcount)+1);
			$('#total_share').val(total_share);
			add_functions();
			clear_add_beneficiary();
			change_jah_ben_numbering();
		/*}
		else{
			$('.frmCurrent')[0].submit();
		}*/
    });

	 /* Beneficiary */


	function clear_add_beneficiary()
	{
		$('#sel_beneficiary').val('');
		$('#txt_beneficiary_share').val('');
		$('#txt_other_info').val('');
		$('#beneficiaryAddBtnNew').show();
		$('#sel_beneficiary').attr('style', ' ');
		$('#sel_beneficiary_error').remove();
		$('#txt_beneficiary_share').attr('style', ' ');
		$('#txt_beneficiary_share_error').remove();
	}


}

	function changeHiddenPraimry(beneficiary_value){
		if(beneficiary_value !="" || beneficiary_value !="0")
			$('#sel_primary_beneficiary').val(beneficiary_value);
	}

