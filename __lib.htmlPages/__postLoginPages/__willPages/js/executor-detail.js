
$(document).ready(function(){
    $.validate({form:"#frmexecutordetails", onSuccess:validate_executor});
    //is_foreign_address();

    if($('#exc_id').val() == 0 || $('#exc_id').val() == undefined){
        $('#opt_status').html('Add Executor');
    }else{
        $('#opt_status').html('Update Executor');
    }
    //$('input[type="checkbox"]').click(function(){if($(this).attr("value")=="correspondenceAdd"){$(".correspondenceAdd").toggle();}});
    
    $('input[type="radio"]').click(function(){if($(this).attr("value")=="1"){$(".MentallyYes").removeClass('hide');}});
    $('input[type="radio"]').click(function(){if($(this).attr("value")=="0"){$(".MentallyYes").addClass('hide');}});

    
    if($("input[name=executor_quest]:checked").val() == 1){
        $('#select_executor_dropdown').html($('#select_executor_dropdown_hidden').html());
        if($('#pro_exc_id').val() > 0){
            $('#professional_executor').html($('#professional_executor_hidden').html());
        }
        if($('#exc_id').val() > 0)
            $('#executor_option').attr('disabled', true);
    }

    $('.clscorrespondenceAdd').click(function(){
		if($(this).attr("value")=="correspondenceAdd"){
			if(this.checked){
				$('#is_correspondence_same_as_permanent').val('1'); // setting the hidden variabel value to 1;
				$('#correspondence_address_line1').val($('#permanent_address_line1').val());
				$('#correspondence_address_line2').val($('#permanent_address_line2').val());
				$('#correspondence_address_line3').val($('#permanent_address_line3').val());
				$('#correspondence_city_village_town').val($('#permanent_city_village_town').val());
				$('#correspondence_country').val($('#permanent_country').val());
				$('#correspondence_state').val($('#permanent_state').val());
				var checked;
				if($('#permanent_is_foreign_address').prop('checked'))
					checked = 1;
				else
					checked = 0;
				populate_country_disable($('#permanent_country').val(), "correspondence_country", "span_correspondence_country", checked, "");
				$('#correspondence_state').val($('#permanent_state').val());
				if($('#permanent_country').val()!="")
					populate_states_disable($('#permanent_country').val(), "correspondence_state", "span_correspondence_state",$('#permanent_state').val(), "", "correspondence_state");
				if($('#permanent_span_other_state').is(':visible')){
					$('#correspondence_span_other_state').show();
					//$('#permanent_span_other_state').prop('readonly', true);
				}else{
					$('#correspondence_span_other_state').hide();
				}
				if(checked != 1)
					$('#correspondence_span_other_state').attr('style', 'display:none;');

				$('#correspondence_state_other').val($('#permanent_state_other').val());
				$('#correspondence_zipcode').val($('#permanent_zipcode').val());
				$('#correspondence_is_foreign_address').prop('checked',$('#permanent_is_foreign_address').prop('checked'));
				$('input[name=correspondence_state_other]').val($('input[name=permanent_state_other]').val());

				$('#correspondence_address_line1').prop('readonly', true);
				$('#correspondence_address_line2').prop('readonly', true);
				$('#correspondence_address_line3').prop('readonly', true);
				$('#correspondence_city_village_town').prop('readonly', true);
				$('#correspondence_state_other').prop('readonly', true);
				$('#correspondence_zipcode').prop('readonly', true);
				$('input[name=correspondence_state_other]').prop('readonly', true);

				$('#correspondence_country').prop('disabled', true);
				$('#correspondence_is_foreign_address').prop('disabled', true);

				$('#correspondence_is_same_as_permanent').prop('checked', $('#permanent_is_same_as_permanent').prop('checked'));
				$('#correspondence_is_same_as_correspondence').prop('checked', $('#permanent_is_same_as_correspondence').prop('checked'));
				$('#correspondence_is_same_as_permanent').attr('disabled', true);
				$('#correspondence_is_same_as_correspondence').attr('disabled', true);
				$('#correspondence_addr_copied_from').val($('#permanent_addr_copied_from').val());
		
				$.validate({form:"#frmexecutordetails", onSuccess:validate_executor});
				copy_permanent_address();
				copy_correspondence_address();
				//$('#correspondence_state')

				if(checked ==1)
				{
					$('#permanent_zipcode').removeClass( "numeric" );
					$('#permanent_zipcode').attr( "placeholder", "Zipcode" );
					$('#permanent_zipcode').attr( "maxlength", "25" );
					$('#permanent_zipcode').attr( "data-validation-length", "5-25" );
					$('#permanent_zipcode').attr( "data-validation-error-msg", "Please enter valid Zipcode" );
					
					$('#correspondence_zipcode').removeClass( "numeric" );
					$('#correspondence_zipcode').attr( "placeholder", "Zipcode" );
					$('#correspondence_zipcode').attr( "maxlength", "25" );
					$('#correspondence_zipcode').attr( "data-validation-length", "5-25" );
					$('#correspondence_zipcode').attr( "data-validation-error-msg", "Please enter valid Zipcode" );
				}
				else{
					$('#permanent_zipcode').addClass( "numeric" );
					$('#permanent_zipcode').attr( "placeholder", "Pincode" );
					$('#permanent_zipcode').attr( "maxlength", "6" );
					$('#permanent_zipcode').attr( "data-validation-length", "1-6" );
					$('#permanent_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
					
					$('#correspondence_zipcode').addClass( "numeric" );
					$('#correspondence_zipcode').attr( "placeholder", "Pincode" );
					$('#correspondence_zipcode').attr( "maxlength", "6" );
					$('#correspondence_zipcode').attr( "data-validation-length", "1-6" );
					$('#correspondence_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
				}

    		}else{
				$('#is_correspondence_same_as_permanent').val('0'); // setting the hidden variabel value to 0;
				$('#correspondence_address_line1').val('');
				$('#correspondence_address_line1').prop('readonly', false);
				$('#correspondence_address_line2').val('');
				$('#correspondence_address_line2').prop('readonly', false);
				$('#correspondence_address_line3').val('');
				$('#correspondence_address_line3').prop('readonly', false);
				$('#correspondence_city_village_town').val('');
				$('#correspondence_city_village_town').prop('readonly', false);
				$('#correspondence_country').val('');
				$('#correspondence_country').prop('disabled', false);
				//$('#correspondence_state').val('');
				$('#correspondence_state').find('option').remove().end().append('<option value="">Please select</option>').val('');
				
				$('#correspondence_is_foreign_address').prop('checked', false);
				$('#correspondence_is_foreign_address').prop('disabled', false);
				$('#correspondence_is_foreign_address').prop('readonly', false);
				populate_country(102, "correspondence_country", "span_correspondence_country", 0, "");
				populate_states(102, "correspondence_state", "span_correspondence_state","", "", "correspondence_state");
				$('#correspondence_span_other_state').attr('style', 'display:none;');
				$('#correspondence_state').prop('disabled', false);
				$('#correspondence_state_other').val('');
				$('#correspondence_state_other').prop('disabled', false);
				$('#correspondence_state_other').prop('readonly', false);
				$('#correspondence_zipcode').val('');
				$('#correspondence_zipcode').prop('readonly', false);
				//------------------------------------------- changes 8-8-14 -----------------------------------------//
				$('#correspondence_is_same_as_permanent').prop('checked', false);
				$('#correspondence_is_same_as_correspondence').prop('checked', false);
				$('#correspondence_is_same_as_permanent').prop('disabled', false);
				$('#correspondence_is_same_as_correspondence').prop('disabled', false);
				$('#correspondence_addr_copied_from').val('');
				
                $('#correspondence_is_same_as_permanent').prop('disabled', $('#permanent_is_same_as_permanent').prop('disabled'));
                $('#correspondence_is_same_as_correspondence').prop('disabled', $('#permanent_is_same_as_correspondence').prop('disabled'));
				//------------------------------------------- End changes 8-8-14 -----------------------------------------//
				copy_permanent_address();
				copy_correspondence_address();
			}
		}               
	});

    /* ------   Remove Code Start    ------ */
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="Yes"){
            $(".profExecutorYes").removeClass('hide'); 
            $('#relative').addClass('hide'); 
            $('#executor_details').html('');
            $('#professional_executor').html($('#professional_executor_hidden').html());
            $.validate({form:"#frmexecutordetails", onSuccess:validate_executor});
        }
    });
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="No"){
            $(".profExecutorYes").addClass('hide');
            $('#relative').removeClass('hide'); 
            $('#executor_details').html('');
            $('#professional_executor').html('');
            $.validate({form:"#frmexecutordetails", onSuccess:validate_executor});
        }
    });
    /* ------   Remove Code End    ------ */
    

    $('input[name="executor_quest"]').click(function(){
        $('#error1').html('');
        $('#error').html('');
        if($(this).attr("value")=="1"){
            //alert("yes");
            $('#btn-add').attr('disabled', false);
            $('#btn-add').removeClass('btn-submit-disabled');
            $('#select_executor_dropdown').html($('#select_executor_dropdown_hidden').html());

            var do_what = "update_executor";
            var filtervars = {
                    do_what: do_what
            };
            $.ajax({
                type: "POST",
                url: "_ajax_files/executor-detail_ajax.php",
                data: filtervars,
                dataType: "html",
                success: function(html){
                    executor_option_change();
                }
            });
            
            //executor_option_change();
        }else{
            
            $('#btn-add').attr('disabled', true);
            $('#btn-add').addClass('btn-submit-disabled');
            $('#select_executor_dropdown').html('');
            $('#professional_executor').html('');
            $('#executor_details').html('');

            if($('#executor_count').val() == 0){
	            if( !confirm("Are you sure you do not want to appoint any executor?") ){
                    window.location.reload('executor-details');
	                return;
                }

	            var do_what = "release_mandatory_tab_executor";
	            var filtervars = {
	                do_what: do_what,
                    auth_token: $("#auth_token_name").val()
	            };
	            $.ajax({
	                type: "POST",
	                url: "_ajax_files/executor-detail_ajax.php",
	                data: filtervars,
	                dataType: "html",
	                success: function(html){
	                    window.location.reload('executor-details');
	                }
	            });
	        }
        }
    });

    if(!$('input[name="executor_quest"]').is(':checked')){
        $('#btn-add').attr('disabled', true);
        $('#btn-add').addClass('btn-submit-disabled');
    }else if($('input[name="executor_quest"]:checked').val() == 1){
        /*$('#btn-add').attr('disabled', false);
        $('#btn-add').removeClass('btn-submit-disabled');*/
    }
    
    if($('.no_selected').attr('checked') == 'checked'){
        $('#btn-add').attr('disabled', true);
        $('#btn-add').addClass('btn-submit-disabled');
    }

    /*----------------*/
    $('input[name=is_relative_executor]').click(function(){
        //alert($(this).attr("value"));
        if($(this).attr("value") != 0){
            var check_id = $(this).attr("rec_operation");
            var filtervars = {
                do_what:$(this).attr("rec_operation"),
                auth_token: $("#auth_token_name").val()
            };
            $.ajax({
                type: "POST",
                url: "_ajax_files/executor-detail_ajax.php",
                data: filtervars,
                dataType: "html",
                success: function(html){
                    if(check_id=="trustees"){
                        $("#ed2_ajax").hide()
                        $("#ed3_ajax").hide()
                        $("#ed2_ajax").html('');
                        $("#ed3_ajax").html('');
                        $('#ed1_ajax').html(html);
                        $('#ed1_ajax').show();
                    }else if(check_id=="beneficiary"){
                        $('#executor_details').html(html);
                    }else if(check_id=="other"){
                        $('#executor_details').html(html);
                    }
                    $.validate({form:"#frmexecutordetails", onSuccess:validate_executor});

                    $('input[type="checkbox"]').click(function(){
                        if($(this).attr("value")=="correspondenceAdd"){
                            if(this.checked){
                                $('#correspondence_address_line1').val($('#permanent_address_line1').val());
                                $('#correspondence_address_line2').val($('#permanent_address_line2').val());
                                $('#correspondence_address_line3').val($('#permanent_address_line3').val());
                                $('#correspondence_city_village_town').val($('#permanent_city_village_town').val());
                                $('#correspondence_country').val($('#permanent_country').val());
                                $('#correspondence_state').val($('#permanent_state').val());
                                populate_states_disable($('#correspondence_country').val(), "correspondence_state", "span_correspondence_state",$('#permanent_state').val(), "", "correspondence_state");
                                if($('#permanent_span_other_state').is(':visible')){
                                    $('#correspondence_span_other_state').show();

                                }else{
                                    $('#correspondence_span_other_state').hide();
                                }
                                $('#correspondence_state_other').val($('#permanent_state_other').val());
                                $('#correspondence_zipcode').val($('#permanent_zipcode').val());
                                $('#correspondence_is_foreign_address').prop('checked',$('#permanent_is_foreign_address').prop('checked'));
                                $('input[name=correspondence_state_other]').val($('input[name=permanent_state_other]').val());

                                $('#correspondence_address_line1').prop('readonly', true);
                                $('#correspondence_address_line2').prop('readonly', true);
                                $('#correspondence_address_line3').prop('readonly', true);
                                $('#correspondence_city_village_town').prop('readonly', true);
                                $('#correspondence_span_other_state').prop('readonly', true);
                                $('#correspondence_zipcode').prop('readonly', true);
                                $('input[name=correspondence_state_other]').prop('readonly', true);

                                $('#correspondence_country').prop('disabled', true);
                                $('#correspondence_is_foreign_address').prop('disabled', true);

                                $.validate({form:"#frmexecutordetails", onSuccess:validate_executor});
                                copy_permanent_address();
                                copy_correspondence_address();

                                //$('#correspondence_state')

                            }else{
                                $('#correspondence_address_line1').val('');
                                $('#correspondence_address_line2').val('');
                                $('#correspondence_address_line3').val('');
                                $('#correspondence_city_village_town').val('');
                                $('#correspondence_country').val('');
                                $('#correspondence_state').val('');
                                $('#correspondence_state').find('option').remove().end().append('<option value="">Please select</option>').val('');
                                $('#correspondence_zipcode').val('');
                                $('#correspondence_is_foreign_address').prop('checked',false);

                                $('#correspondence_address_line1').prop('readonly', false);
                                $('#correspondence_address_line2').prop('readonly', false);
                                $('#correspondence_address_line3').prop('readonly', false);
                                $('#correspondence_city_village_town').prop('readonly', false);
                                $('#correspondence_span_other_state').prop('readonly', false);
                                $('#correspondence_zipcode').prop('readonly', false);
                                $('input[name=correspondence_state_other]').prop('readonly', false);

                                $('#correspondence_country').prop('disabled', false);
                                $('#correspondence_state').prop('disabled', false);
                                $('#correspondence_is_foreign_address').prop('disabled', false);

                                $.validate({form:"#frmexecutordetails", onSuccess:validate_executor});
                                copy_permanent_address();
                                copy_correspondence_address();
                            }
                        }               
                    });

                    
                    $('#show_birth_date').datepicker({
                        dateFormat: 'dd/mm/yy',
                        numberOfMonths: 1,
                        minDate: "-150Y",
                        //maxDate: "-18Y",
                        yearRange: "-150:+0",
                        altField: "#birth_date",
                        altFormat: "yy-mm-dd",
                        changeMonth: true,
                        changeYear: true,
                        onSelect: function (dateText, inst) {
                        	$('#is_adult').prop('checked', true);
                            $('#show_birth_date').removeClass( "error" );
                            $('#show_birth_date').attr("style", "");
                            $('#show_birth_date').parent().removeClass('has-error');
                            $('.help-block').remove();
                            $('#show_birth_date').addClass( "valid" );
                            $('#show_birth_date').parent().addClass('has-success');
                            var startDate = new Date(dateText);
                            var selectedYear = startDate.getFullYear();
                            var today = new Date();
                            var todayYear = today.getFullYear();
                            var diff=  todayYear-selectedYear;
                            if(diff > 100)
                                alert('You have selected '+selectedYear+' as birth_date year');
                        }   
                    });

                    addPhoneNumber();
                    addPhoneMobile();
                    addOfficeNumber();
                    addFaxNumber();
                    addEmail();
                    $('.numeric').numeric();
                    copy_permanent_address();
                    copy_correspondence_address();

                    return false;
                }
            });
        }
    });
    /*---------------*/

    $(".delete_ex").click(function(){
        $('#error1').html('');
        $('#error').html('');
        
        if($(this).attr("rec_id") == $('#exc_id').val()){
            alert('Record can not be deleted in edit mode.');
            return;
        }

        if($(this).attr("ex_type_name") == 'primary' && $('#executor_count').val() > 1){
        	/*if($('#executor_count').val() == 1){
        		$('.make_primary').removeClass('hide');
        	}else{*/
        		if( confirm("Please select another Executor as Primary Executor before deleting.") )
                	$('.make_primary').removeClass('hide');
        	/*}*/
        }else{
            if( !confirm("Are you sure you want to delete this Executor?") )
                return;
                
            $("#loading").show();
            
            var filtervars = {
                do_what:'del_executor',
                delete_ids: $(this).attr("rec_id"),
                auth_token: $("#auth_token_name").val()
            };

            $.ajax({
                type: "POST",
                url: "_ajax_files/executor-detail_ajax.php",
                data: filtervars,
                dataType: "html",
                success: function(html){
                alert("Executor details deleted successfully");
                window.location.href='executor-details';
                }
            });
        }
    });

    $(".alt_ex").click(function(){
        if( !confirm("Are you sure you want make this Executor as primary?") )
            return;

            var ben_id = $(this).attr("rec_id");
            $("#hid_ben_id").val(ben_id);
            document.frmaltPrimaryExecutor.submit(); 
    });

    $(".edit_ex").click(function(){
        $('#opt_status').html('Update Executor');
        $('#error1').html('');
        $('#error').html('');
        $('#executor_option').attr('disabled', true);
        $('#appoint_executor').addClass('hide');
        $('#btn-add').addClass("btn-submit-disabled");
        $('#btn-add').attr('disabled', true);

        $('#btn-update').removeClass("btn-submit-disabled");
        $('#btn-update').attr('disabled', false);

        var text = $(this).attr('ex_type_name');
        var pro_id = $(this).attr("ex_pro_id");
        var type_id = $(this).attr("ex_type_id");
        var filtervars = {
            do_what:$(this).attr("ex_option"),
            edit_id: $(this).attr("rec_id"),
            auth_token: $("#auth_token_name").val()
        };
        var check_id = $(this).attr("ex_option");
        $.ajax({
            type: "POST",
            url: "_ajax_files/executor-detail_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                if(check_id=="trustees"){
                    $('#executor_details').html(html);
                    $('#professional_executor').html($('#professional_executor_hidden').html());
                    $('#relative').addClass('hide');
                    $('.profExecutorYes').removeClass('hide');
                    $('.yes_selected').prop('checked', true);
                    $('#professional_executor').show();
                    $("#pro_exc_id option[value='"+pro_id+"']").attr('selected','true');
                    //pro_id
                    //$('#pro_exc_id').attr('disabled', true);

                    $("#pro_exc_id option[value='"+pro_id+"']").prop('selected', true);
                    $('#select_executor_dropdown').html($('#select_executor_dropdown_hidden').html());
                    $("#select_executor_dropdown option[value='"+check_id+"']").prop('selected', true);
                    $("#executor_type option[value='"+type_id+"']").prop('selected', true);
                    $('#executor_option').attr('disabled', true);
                    if(text == 'primary'){
                        $('#primary_text').html('Primary ');
                        $('#executor_type').val('1');
                    }else{
                        $('#executor_type').val('2');
                        $('#primary_text').html('preferred ');
                    }
                    //$('#select_executor_dropdown').html($('#select_executor_dropdown_hidden').html());
                    $("#select_executor_dropdown option[value='"+check_id+"']").prop('selected', true);

                    $('#text_executor').html();
                    //$("#professional_executor #pro_exc_id option[value='"+pro_id+"']").attr('selected','true');
                    $('#profExecutorYes').prop('checked', true);
                    $('#beneficiary').prop('checked', false);
                    $('#other').prop('checked', false);
                }else if(check_id=="beneficiary"){
                    $('#executor_details').html(html);
                    $('#professional_executor').html('');
                    $('#relative').removeClass('hide');
                    $('.profExecutorYes').addClass('hide');
                    $("#executor_type option[value='"+type_id+"']").attr('selected','selected');
                    $('#select_executor_dropdown').html($('#select_executor_dropdown_hidden').html());
                    $("#select_executor_dropdown option[value='"+check_id+"']").prop('selected', true);
                    $('#'+check_id).prop('checked', true);
                    $('#executor_option').attr('disabled', true);
                    $('#profExecutorYes').prop('checked', false);
                    $('#profExecutorNo').prop('checked', true);
                }else if(check_id=="other"){
                    $('#executor_details').html(html);
                    $('#professional_executor').html('');
                    $('#relative').removeClass('hide');
                    $('.profExecutorYes').addClass('hide');
                    $('.yes_selected').prop('checked', true);
                    $("#executor_type option[value='"+type_id+"']").attr('selected','selected');
                    $('#select_executor_dropdown').html($('#select_executor_dropdown_hidden').html());
                    $('#executor_option').attr('disabled', true);
                    if(text == 'primary'){
                        $('#primary_text').html('Primary ');
                        $('#executor_type').val('1');
                    }else{
                        $('#executor_type').val('2');
                        $('#primary_text').html('preferred ');
                    }
                    $("#select_executor_dropdown option[value='"+check_id+"']").prop('selected', true);
                    $('#'+check_id).prop('checked', true);
                    $('#profExecutorYes').prop('checked', false);
                    $('#profExecutorNo').prop('checked', true);

                }
                $.validate({form:"#frmexecutordetails", onSuccess:validate_executor});

                 $('.clscorrespondenceAdd').click(function(){
						if($(this).attr("value")=="correspondenceAdd"){
							if(this.checked){
								$('#is_correspondence_same_as_permanent').val('1'); // setting the hidden variabel value to 1;
								$('#correspondence_address_line1').val($('#permanent_address_line1').val());
								$('#correspondence_address_line2').val($('#permanent_address_line2').val());
								$('#correspondence_address_line3').val($('#permanent_address_line3').val());
								$('#correspondence_city_village_town').val($('#permanent_city_village_town').val());
								$('#correspondence_country').val($('#permanent_country').val());
								$('#correspondence_state').val($('#permanent_state').val());
								var checked;
								if($('#permanent_is_foreign_address').prop('checked'))
									checked = 1;
								else
									checked = 0;
								populate_country_disable($('#permanent_country').val(), "correspondence_country", "span_correspondence_country", checked, "");
								$('#correspondence_state').val($('#permanent_state').val());
								if($('#permanent_country').val()!="")
									populate_states_disable($('#permanent_country').val(), "correspondence_state", "span_correspondence_state",$('#permanent_state').val(), "", "correspondence_state");
								if($('#permanent_span_other_state').is(':visible')){
									$('#correspondence_span_other_state').show();
									//$('#permanent_span_other_state').prop('readonly', true);
								}else{
									$('#correspondence_span_other_state').hide();
								}
								if(checked != 1)
									$('#correspondence_span_other_state').attr('style', 'display:none;');

								$('#correspondence_state_other').val($('#permanent_state_other').val());
								$('#correspondence_zipcode').val($('#permanent_zipcode').val());
								$('#correspondence_is_foreign_address').prop('checked',$('#permanent_is_foreign_address').prop('checked'));
								$('input[name=correspondence_state_other]').val($('input[name=permanent_state_other]').val());

								$('#correspondence_address_line1').prop('readonly', true);
								$('#correspondence_address_line2').prop('readonly', true);
								$('#correspondence_address_line3').prop('readonly', true);
								$('#correspondence_city_village_town').prop('readonly', true);
								$('#correspondence_state_other').prop('readonly', true);
								$('#correspondence_zipcode').prop('readonly', true);
								$('input[name=correspondence_state_other]').prop('readonly', true);

								$('#correspondence_country').prop('disabled', true);
								$('#correspondence_is_foreign_address').prop('disabled', true);

								$('#correspondence_is_same_as_permanent').prop('checked', $('#permanent_is_same_as_permanent').prop('checked'));
								$('#correspondence_is_same_as_correspondence').prop('checked', $('#permanent_is_same_as_correspondence').prop('checked'));
								$('#correspondence_is_same_as_permanent').attr('disabled', true);
								$('#correspondence_is_same_as_correspondence').attr('disabled', true);
								$('#correspondence_addr_copied_from').val($('#permanent_addr_copied_from').val());
						
								$.validate({form:"#frmexecutordetails", onSuccess:validate_executor});
								copy_permanent_address();
								copy_correspondence_address();
								//$('#correspondence_state')

								if(checked ==1)
								{
									$('#permanent_zipcode').removeClass( "numeric" );
									$('#permanent_zipcode').attr( "placeholder", "Zipcode" );
									$('#permanent_zipcode').attr( "maxlength", "25" );
									$('#permanent_zipcode').attr( "data-validation-length", "5-25" );
									$('#permanent_zipcode').attr( "data-validation-error-msg", "Please enter valid Zipcode" );
									
									$('#correspondence_zipcode').removeClass( "numeric" );
									$('#correspondence_zipcode').attr( "placeholder", "Zipcode" );
									$('#correspondence_zipcode').attr( "maxlength", "25" );
									$('#correspondence_zipcode').attr( "data-validation-length", "5-25" );
									$('#correspondence_zipcode').attr( "data-validation-error-msg", "Please enter valid Zipcode" );
								}
								else{
									$('#permanent_zipcode').addClass( "numeric" );
									$('#permanent_zipcode').attr( "placeholder", "Pincode" );
									$('#permanent_zipcode').attr( "maxlength", "6" );
									$('#permanent_zipcode').attr( "data-validation-length", "1-6" );
									$('#permanent_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
									
									$('#correspondence_zipcode').addClass( "numeric" );
									$('#correspondence_zipcode').attr( "placeholder", "Pincode" );
									$('#correspondence_zipcode').attr( "maxlength", "6" );
									$('#correspondence_zipcode').attr( "data-validation-length", "1-6" );
									$('#correspondence_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
								}

							}else{
							
								$('#is_correspondence_same_as_permanent').val('0'); // setting the hidden variabel value to 0;
								$('#correspondence_address_line1').val('');
								$('#correspondence_address_line1').prop('readonly', false);
								$('#correspondence_address_line2').val('');
								$('#correspondence_address_line2').prop('readonly', false);
								$('#correspondence_address_line3').val('');
								$('#correspondence_address_line3').prop('readonly', false);
								$('#correspondence_city_village_town').val('');
								$('#correspondence_city_village_town').prop('readonly', false);
								$('#correspondence_country').val('');
								$('#correspondence_country').prop('disabled', false);
								//$('#correspondence_state').val('');
								$('#correspondence_state').find('option').remove().end().append('<option value="">Please select</option>').val('');
								
								$('#correspondence_is_foreign_address').prop('checked', false);
								$('#correspondence_is_foreign_address').prop('disabled', false);
								$('#correspondence_is_foreign_address').prop('readonly', false);
								populate_country(102, "correspondence_country", "span_correspondence_country", 0, "");
								populate_states(102, "correspondence_state", "span_correspondence_state","", "", "correspondence_state");
								$('#correspondence_span_other_state').attr('style', 'display:none;');
								$('#correspondence_state').prop('disabled', false);
								$('#correspondence_state_other').val('');
								$('#correspondence_state_other').prop('disabled', false);
								$('#correspondence_state_other').prop('readonly', false);
								$('#correspondence_zipcode').val('');
								$('#correspondence_zipcode').prop('readonly', false);
								//------------------------------------------- changes 8-8-14 -----------------------------------------//
								$('#correspondence_is_same_as_permanent').prop('checked', false);
								$('#correspondence_is_same_as_correspondence').prop('checked', false);
								$('#correspondence_is_same_as_permanent').prop('disabled', false);
								$('#correspondence_is_same_as_correspondence').prop('disabled', false);
								$('#correspondence_addr_copied_from').val('');
								
								$('#permanent_zipcode').addClass( "numeric" );
								$('#permanent_zipcode').attr( "placeholder", "Pincode" );
								$('#permanent_zipcode').attr( "maxlength", "6" );
								$('#permanent_zipcode').attr( "data-validation-length", "1-6" );
								$('#permanent_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
								
								$('#correspondence_zipcode').addClass( "numeric" );
								$('#correspondence_zipcode').attr( "placeholder", "Pincode" );
								$('#correspondence_zipcode').attr( "maxlength", "6" );
								$('#correspondence_zipcode').attr( "data-validation-length", "1-6" );
								$('#correspondence_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
								//------------------------------------------- End changes 8-8-14 -----------------------------------------//
                                $('#correspondence_is_same_as_permanent').prop('disabled', $('#permanent_is_same_as_permanent').prop('disabled'));
                                $('#correspondence_is_same_as_correspondence').prop('disabled', $('#permanent_is_same_as_correspondence').prop('disabled'));

								copy_permanent_address();
								copy_correspondence_address();
							}
						}               
					});

                
                $('#show_birth_date').datepicker({
                    dateFormat: 'dd/mm/yy',
                    numberOfMonths: 1,
                    minDate: "-150",
                    //maxDate: "-18Y",
                    yearRange: "-150:+0",
                    altField: "#birth_date",
                    altFormat: "yy-mm-dd",
                    changeMonth: true,
                    changeYear: true,
                    onSelect: function (dateText, inst) {
                    	$('#is_adult').prop('checked', true);
                        $('#show_birth_date').removeClass( "error" );
                        $('#show_birth_date').attr("style", "");
                        $('#show_birth_date').parent().removeClass('has-error');
                        $('.help-block').remove();
                        $('#show_birth_date').addClass( "valid" );
                        $('#show_birth_date').parent().addClass('has-success');

                        var startDate = new Date(dateText);
                        var selectedYear = startDate.getFullYear();
                        var today = new Date();
                        var todayYear = today.getFullYear();
                        var diff=  todayYear-selectedYear;
                        if(diff > 100)
                            alert('You have selected '+selectedYear+' as birth year');
                    }   
                });
                addPhoneNumber();
                addOfficeNumber();
                addPhoneMobile();
                addFaxNumber();
                addEmail();
                $('.numeric').numeric();
                is_foreign_address();
                validate_firstname();
                copy_permanent_address();
                copy_correspondence_address();
            }
        });
    });

    $('#show_birth_date').datepicker({
        dateFormat: 'dd/mm/yy',
        numberOfMonths: 1,
        minDate: "-150Y",
        //maxDate: "-18Y",
        yearRange: "-150:+0",
        altField: "#birth_date",
        altFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        onSelect: function (dateText, inst) {
        	$('#is_adult').prop('checked', true);
            $('#show_birth_date').removeClass( "error" );
            $('#show_birth_date').attr("style", "");
            $('#show_birth_date').parent().removeClass('has-error');
            $('.help-block').remove();
            $('#show_birth_date').addClass( "valid" );
            $('#show_birth_date').parent().addClass('has-success');

            var startDate = new Date(dateText);
            var selectedYear = startDate.getFullYear();
            var today = new Date();
            var todayYear = today.getFullYear();
            var diff=  todayYear-selectedYear;
            if(diff > 100)
                alert('You have selected '+selectedYear+' as birth year');
        }   
    });


	
    

    addPhoneNumber();
    addOfficeNumber();
    addPhoneMobile();
    addFaxNumber();
    addEmail();
    $('.numeric').numeric();
    //is_foreign_address();
    copy_permanent_address();
    copy_correspondence_address();
    executor_option_change();
});

function executor_option_change(){
    $('#executor_option').change(function(){
                if($(this).prop("value") == "trustees"){
                    var v = confirm("You have selected Warmond Trustees & Executors Pvt. Ltd. (Warmond) as a Professional Executor for your WILL. \nWarmond's fees to act as an executor will be as follows:\nExecutorship Acceptance fees shall be â‚¹ 1 lac.\nDuring the time of execution it will be 0.5 â€“ 1 % of the market value of assets, with minimum fees of â‚¹ 5 lacs. Exact fees shall be discussed and determined with the client based on the size, nature and jurisdiction of the assets\n\nPlease click on 'OK' to re-confirm your selection of Warmond as professional executor, else click on 'Cancel'. If you choose to cancel, you would be required to appoint your friend or relative as Executor.");
                    if(v == true) {
                        $('#pro_exc_id [value="1"]').attr('selected', true);
                        $(".profExecutorYes").removeClass('hide'); 
                        $('#relative').addClass('hide'); 
                        $('#executor_details').html('');
                        $('#professional_executor').html($('#professional_executor_hidden').html());
                        get_Trustee_Detatils();
                        $('#pro_exc_id').attr('disabled', true);
                        $.validate({form:"#frmexecutordetails", onSuccess:validate_executor});
                    } else {
                        //alert('No selected');
                        $(this).prop("value", '');
                    }
                    
                    
                } else if($(this).prop("value") == "other"){
                    $(".profExecutorYes").addClass('hide');
                    $('#relative').removeClass('hide'); 
                    $('#executor_details').html('');
                    $('#professional_executor').html('');
                    $.validate({form:"#frmexecutordetails", onSuccess:validate_executor});

                    var check_id = "other";
                    
                    var filtervars = {
                        do_what: check_id,
                        auth_token: $("#auth_token_name").val()
                    };
                    $.ajax({
                        type: "POST",
                        url: "_ajax_files/executor-detail_ajax.php",
                        data: filtervars,
                        dataType: "html",
                        success: function(html){

                            if(check_id=="trustees"){
                                $("#ed2_ajax").hide()
                                $("#ed3_ajax").hide()
                                $("#ed2_ajax").html('');
                                $("#ed3_ajax").html('');
                                $('#ed1_ajax').html(html);
                                $('#ed1_ajax').show();
                            }else if(check_id=="beneficiary"){
                                $('#executor_details').html(html);
                            }else if(check_id=="other"){
                                $('#executor_details').html(html);
                            }
                            $.validate({form:"#frmexecutordetails", onSuccess:validate_executor});

                            $('input[type="checkbox"]').click(function(){

                                if($(this).attr("value")=="correspondenceAdd"){
                                    if(this.checked){
                                        $('#correspondence_address_line1').val($('#permanent_address_line1').val());
                                        $('#correspondence_address_line2').val($('#permanent_address_line2').val());
                                        $('#correspondence_address_line3').val($('#permanent_address_line3').val());
                                        $('#correspondence_city_village_town').val($('#permanent_city_village_town').val());
                                        $('#correspondence_country').val($('#permanent_country').val());
                                        $('#correspondence_state').val($('#permanent_state').val());
                                        var checked;
                                        if($('#permanent_is_foreign_address').prop('checked'))
                                            checked = 1;
                                        else
                                            checked = 0;
                                        populate_country_disable($('#permanent_country').val(), "correspondence_country", "span_correspondence_country", checked, "");
                                        populate_states_disable($('#correspondence_country').val(), "correspondence_state", "span_correspondence_state",$('#permanent_state').val(), "", "correspondence_state");
                                        if($('#permanent_span_other_state').is(':visible')){
                                            $('#correspondence_span_other_state').show();

                                        }else{
                                            $('#correspondence_span_other_state').hide();
                                        }
                                        $('#correspondence_state_other').val($('#permanent_state_other').val());
                                        $('#correspondence_zipcode').val($('#permanent_zipcode').val());
                                        $('#correspondence_is_foreign_address').prop('checked',$('#permanent_is_foreign_address').prop('checked'));

                                        $('#correspondence_is_same_as_permanent').prop('checked',$('#permanent_is_same_as_permanent').prop('checked'));
                                        $('#correspondence_is_same_as_correspondence').prop('checked',$('#permanent_is_same_as_correspondence').prop('checked'));
                                        $('#correspondence_is_same_as_permanent').prop('disabled', true);
                                        $('#correspondence_is_same_as_correspondence').prop('disabled', true);

                                        $('input[name=correspondence_state_other]').val($('input[name=permanent_state_other]').val());

                                        $('#correspondence_address_line1').prop('readonly', true);
                                        $('#correspondence_address_line2').prop('readonly', true);
                                        $('#correspondence_address_line3').prop('readonly', true);
                                        $('#correspondence_city_village_town').prop('readonly', true);
                                        $('#correspondence_span_other_state').prop('readonly', true);
                                        $('#correspondence_zipcode').prop('readonly', true);
                                        $('input[name=correspondence_state_other]').prop('readonly', true);

                                        $('#correspondence_country').prop('disabled', true);
                                        $('#correspondence_is_foreign_address').prop('disabled', true);

                                        $.validate({form:"#frmexecutordetails", onSuccess:validate_executor});
                                        copy_permanent_address();
                                        copy_correspondence_address();
                                        
                                        //$('#correspondence_state')

                                    }else{
                                        $('#correspondence_address_line1').val('');
                                        $('#correspondence_address_line2').val('');
                                        $('#correspondence_address_line3').val('');
                                        $('#correspondence_city_village_town').val('');
                                        $('#correspondence_country').val('');
                                        $('#correspondence_state').val('');
                                        $('#correspondence_state').find('option').remove().end().append('<option value="">Please select</option>').val('');
                                        $('#correspondence_zipcode').val('');
                                        $('#correspondence_is_foreign_address').prop('checked',false);

                                        $('#correspondence_address_line1').prop('readonly', false);
                                        $('#correspondence_address_line2').prop('readonly', false);
                                        $('#correspondence_address_line3').prop('readonly', false);
                                        $('#correspondence_city_village_town').prop('readonly', false);
                                        $('#correspondence_span_other_state').prop('readonly', false);
                                        $('#correspondence_zipcode').prop('readonly', false);
                                        $('input[name=correspondence_state_other]').prop('readonly', false);

                                        $('#correspondence_country').prop('disabled', false);
                                        $('#correspondence_state').prop('disabled', false);
                                        $('#correspondence_is_foreign_address').prop('disabled', false);


                                         $('#correspondence_is_same_as_permanent').prop('checked', false);
                                        $('#correspondence_is_same_as_correspondence').prop('checked',false);

                                        $('#correspondence_is_same_as_permanent').prop('disabled', false);
                                        $('#correspondence_is_same_as_correspondence').prop('disabled', false);

                                        $('#correspondence_is_same_as_permanent').prop('disabled', $('#permanent_is_same_as_permanent').prop('disabled'));
                                        $('#correspondence_is_same_as_correspondence').prop('disabled', $('#permanent_is_same_as_correspondence').prop('disabled'));

                                        $.validate({form:"#frmexecutordetails", onSuccess:validate_executor});
                                        copy_permanent_address();
                                        copy_correspondence_address();
                                    }
                                }               
                            });

                            
                            $('#show_birth_date').datepicker({
                                dateFormat: 'dd/mm/yy',
                                numberOfMonths: 1,
                                minDate: "-150Y",
                                //maxDate: "-18Y",
                                yearRange: "-150:+0",
                                altField: "#birth_date",
                                altFormat: "yy-mm-dd",
                                changeMonth: true,
                                changeYear: true,
                                onSelect: function (dateText, inst) {
                                    $('#is_adult').prop('checked', true);
                                    $('#show_birth_date').removeClass( "error" );
                                    $('#show_birth_date').attr("style", "");
                                    $('#show_birth_date').parent().removeClass('has-error');
                                    $('.help-block').remove();
                                    $('#show_birth_date').addClass( "valid" );
                                    $('#show_birth_date').parent().addClass('has-success');

                                    var startDate = new Date(dateText);
                                    var selectedYear = startDate.getFullYear();
                                    var today = new Date();
                                    var todayYear = today.getFullYear();
                                    var diff=  todayYear-selectedYear;
                                    if(diff > 100)
                                        alert('You have selected '+selectedYear+' as birth year');
                                }   
                            });

                            addPhoneNumber();
                            addPhoneMobile();
                            addOfficeNumber();
                            addFaxNumber();
                            addEmail();
                            $('.numeric').numeric();
                            is_foreign_address();
                            copy_permanent_address();
                            copy_correspondence_address();
                            return false;
                        }
                    });
                }else{
                    $('#executor_details').html('');
                    $('#professional_executor').html('');
                }
                is_foreign_address();
            });
}

function title_change()
{
    $("#fullname").val($("#title option:selected").text()+" "+$("#firstname").val()+" "+$("#middlename").val()+" "+$("#lastname").val());
} 

function addPhoneNumber()
{
    var MaxInputs1       = 5; //maximum input boxes allowed
    var TelephoneAddWrap   = $("#TelephoneAddWrap"); //Input boxes wrapper ID
    var AddButton       = $("#TelephoneAddBtn"); //Add button ID
   // var x = TelephoneAddWrap.length; //initlal text box count
    var num_phone_numbers = $('#num_phone_numbers').val(); //initlal text box count
    var FieldCount=1; //to keep track of text box added
    $(AddButton).click(function (e)  //on add input button click
    {
            if(num_phone_numbers < MaxInputs1) //max input box allowed
            {
                FieldCount++; //text box added increment
                //add input box
                $(TelephoneAddWrap).append('<div class="row"><div class="divider"></div><div class="col-xs-plus"><span class="plus_sign">+</span></div><div class="col-lg-2"><input type="text" class="form-control numeric" name="phone_isd[]" placeholder="ISD" maxlength="3" title="Please enter country code for Telephone Number" alt="Please enter country code for Telephone Number" value="91"/></div><div class="col-lg-3"><input type="text" class="form-control numeric" name="phone_std[]" placeholder="STD" maxlength="5" title="Please enter STD Code for Telephone Number" alt="Please enter STD Code for Telephone Number"/></div><div class="col-lg-5 last"><input type="text" class="form-control numeric" name="phone_number[]" placeholder="Phone Number" maxlength="10" title="Please enter Telephone Number" alt="Please enter Telephone Number"/></div><a href="#" class="btn-delete TelephoneRemove" title="Please click to remove the entered Telephone Number" alt="Please click to remove the entered Telephone Number">&ndash;</a></div>');
                num_phone_numbers++; //text box increment
                 $('#num_phone_numbers').val(num_phone_numbers); 
            }
            else if(num_phone_numbers == MaxInputs1)
                alert('You can not add more than '+MaxInputs1+' telephone phone numbers');
        $('.numeric').numeric();
    return false;
    });
    $("body").on("click",".TelephoneRemove", function(e){ //user click on remove text
           if( num_phone_numbers > 1 ) {
                
                    $(this).parent('div').remove(); //remove text box
                    num_phone_numbers--; //decrement textbox
            }

    return false;
    });
}

function addEmail(){
    var MaxInputs5       = 3; //maximum input boxes allowed
    var EmailAddWrap    = $("#EmailAddWrap"); //Input boxes wrapper ID
    var EmailAddButton  = $("#EmailAddBtn"); //Add button ID
    var x = EmailAddWrap.length; //initlal text box count
    var num_emails = $('#num_emails').val();
    var FieldCount=1; //to keep track of text box added
    $(EmailAddButton).click(function (e)  //on add input button click
    {
      if(num_emails < MaxInputs5) //max input box allowed
        {
            FieldCount++; //text box added increment
            //add input box

            $(EmailAddWrap).append('<div class="row"><div class="divider"></div><div class="col-lg-8 last"><input type="text" class="form-control" name="email[]" placeholder="Email" maxlength="254"/></div><a href="#" class="btn-delete EmailRemove">&ndash;</a></div>');
            num_emails++; //text box increment
            $('#num_emails').val(num_emails);
        }
        else if(num_emails == MaxInputs5)
            alert('You can not add more than '+MaxInputs5+' Email id');
    return false;
    });
    $("body").on("click",".EmailRemove", function(e){ //user click on remove text
            if( num_emails > 1 ) {
                    $(this).parent('div').remove(); //remove text box
                    num_emails--; //decrement textbox
            }
    return false;
    });
}

function addPhoneMobile()
{
    var MaxInputs3       = 3; //maximum input boxes allowed
    var MobileAddWrap   = $("#MobileAddWrap"); //Input boxes wrapper ID
    var MobAddButton       = $("#MobileAddBtn"); //Add button ID
    var x = MobileAddWrap.length; //initlal text box count
    var num_mobile_numbers = $('#num_mobile_numbers').val();
    var FieldCount=1; //to keep track of text box added
    $(MobAddButton).click(function (e)  //on add input button click
    {
            if(num_mobile_numbers < MaxInputs3) //max input box allowed
            {
                FieldCount++; //text box added increment
                //add input box
                $(MobileAddWrap).append('<div class="row"><div class="divider"></div><div class="col-xs-plus"><span class="plus_sign">+</span></div><div class="col-lg-2"><input type="text" class="form-control numeric" name="mobile_isd[]" placeholder="ISD" maxlength="3" title="Please enter country code for Mobile Number" alt="Please enter country code for Mobile Number" value="91"/></div><div class="col-lg-8 last"><input type="text" class="form-control numeric" name="mobile_number[]" placeholder="Mobile Number" maxlength="10" title="Please enter Mobile Number" alt="Please enter Mobile Number"/></div><a href="#" class="btn-delete MobileRemove" title="Please click to remove the entered Mobile Number" alt="Please click to remove the entered Mobile Number">&ndash;</a></div>');
                num_mobile_numbers++; //text box increment
            }
            else if(num_mobile_numbers == MaxInputs3)
                alert('You can not add more than '+MaxInputs3+' mobile numbers');
        $('.numeric').numeric();
    return false;
    });
    $("body").on("click",".MobileRemove", function(e){ //user click on remove text
            if( num_mobile_numbers > 1 ) {
                    $(this).parent('div').remove(); //remove text box
                    num_mobile_numbers--; //decrement textbox
            }
    return false;
    });
}

function addOfficeNumber()
{
    var MaxInputs2       = 5; //maximum input boxes allowed
    var OfficeAddWrap   = $("#OfficeAddWrap"); //Input boxes wrapper ID
    var OfficeAddButton       = $("#OfficeAddBtn"); //Add button ID
    var x = OfficeAddWrap.length; //initlal text box count
    var num_office_numbers = $('#num_office_numbers').val();
    var FieldCount=1; //to keep track of text box added
    $(OfficeAddButton).click(function (e)  //on add input button click
    {
            if(num_office_numbers < MaxInputs2) //max input box allowed
            {
                FieldCount++; //text box added increment
                //add input box
                $(OfficeAddWrap).append('<div class="row"><div class="divider"></div><div class="col-xs-plus"><span class="plus_sign">+</span></div><div class="col-lg-2"><input type="text" class="form-control numeric" name="office_phone_isd[]" placeholder="ISD" maxlength="3" title="Please enter country code for Office Phone Number" alt="Please enter country code for Office Phone Number" value="91" /></div><div class="col-lg-3"><input type="text" class="form-control numeric" name="office_phone_std[]" placeholder="STD" maxlength="5" title="Please enter STD Code for Office Phone Number" alt="Please enter STD Code for Office Phone Number"/></div><div class="col-lg-5 last"><input type="text" class="form-control numeric" name="office_phone_number[]" placeholder="Office Number" maxlength="10" title="Please enter Office Phone Number" alt="Please enter Office Phone Number"/></div><a href="#" class="btn-delete OfficeRemove" title="Please click to remove the entered Office Phone Numbers" alt="Please click to remove the entered Office Phone Numbers">&ndash;</a></div>');
                num_office_numbers++; //text box increment
            }
            else if(num_office_numbers == MaxInputs2)
                alert('You can not add more than '+MaxInputs2+' office phone numbers');
        $('.numeric').numeric();
    return false;
    });
    $("body").on("click",".OfficeRemove", function(e){ //user click on remove text
            if( num_office_numbers > 1 ) {
                    $(this).parent('div').remove(); //remove text box
                    num_office_numbers--; //decrement textbox
            }
    return false;
    });
}

function addFaxNumber()
{
   var MaxInputs4       = 5; //maximum input boxes allowed
    var FaxAddWrap   = $("#FaxAddWrap"); //Input boxes wrapper ID
    var FaxAddButton   = $("#FaxAddBtn"); //Add button ID
    var x = FaxAddWrap.length; //initlal text box count
    var num_fax_numbers = $('#num_fax_numbers').val();
    var FieldCount=1; //to keep track of text box added
    $(FaxAddButton).click(function (e)  //on add input button click
    {
            if(num_fax_numbers < MaxInputs4) //max input box allowed
            {
                FieldCount++; //text box added increment
                //add input box
                $(FaxAddWrap).append('<div class="row"><div class="divider"></div><div class="col-lg-3"><input type="text" class="form-control" name="fax_isd[]" placeholder="ISD" maxlength="4"/></div><div class="col-lg-3"><input type="text" class="form-control numeric" name="fax_std[]" placeholder="STD" maxlength="4"/></div><div class="col-lg-5 last"><input type="text" class="form-control numeric" name="fax_number[]" placeholder="Phone" maxlength="10"/></div><a href="#" class="btn-delete FaxRemove">&ndash;</a></div>');
                num_fax_numbers++; //text box increment
                $('#num_fax_numbers').val(num_fax_numbers);
            }
            else if(num_fax_numbers == MaxInputs4)
                alert('You can not add more than '+MaxInputs4+' fax numbers');

        $('.numeric').numeric();
    return false;
    });
    $("body").on("click",".FaxRemove", function(e){ //user click on remove text
            if( num_fax_numbers > 1 ) {
                    $(this).parent('div').remove(); //remove text box
                    num_fax_numbers--; //decrement textbox
            }
    return false;
    });
}

function get_Beneficiary_Detatils(){
    var ben_id = $('#ben_id').val();
    if(ben_id != 0){
        var filtervars = {
                do_what:"beneficiary",
                ben_id: ben_id,
                auth_token: $("#auth_token_name").val()
            };
        var check_id = "beneficiary";
        $.ajax({
            type: "POST",
            url: "_ajax_files/executor-detail_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                /*$('form[name="frmexecutordetails"]').find("input:radio:checked").removeAttr('checked')
                var $radios = $('input[type=radio]');
                if($radios.is(':checked') === false) {
                    $radios.filter('[value='+check_id+']').prop('checked', true);
                }*/
                if(check_id=="trustees"){
                    $("#ed2_ajax").hide()
                    $("#ed3_ajax").hide()
                    $("#ed2_ajax").html('');
                    $("#ed3_ajax").html('');
                    $('#ed1_ajax').html(html);
                    $('#ed1_ajax').show();
                }else if(check_id=="beneficiary"){
                    $('#executor_details').html(html);
                }else if(check_id=="other"){
                    $("#ed1_ajax").hide()
                    $("#ed2_ajax").hide()
                    $("#ed1_ajax").html('');
                    $("#ed2_ajax").html('');
                    $('#ed3_ajax').html(html);
                    $('#ed3_ajax').show();
                }
                copy_permanent_address();
                copy_correspondence_address();
            }
        });
    }
}

function get_Trustee_Detatils(){
    var pro_exc_id = $('#pro_exc_id').val();
    if(pro_exc_id != 0){
        var filtervars = {
                do_what:"trustees",
                pro_exc_id: pro_exc_id,
                auth_token: $("#auth_token_name").val()
            };
        var check_id = "beneficiary";
        $.ajax({
            type: "POST",
            url: "_ajax_files/executor-detail_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                //alert(html);
                /*$('form[name="frmexecutordetails"]').find("input:radio:checked").removeAttr('checked')
                var $radios = $('input[type=radio]');
                if($radios.is(':checked') === false) {
                    $radios.filter('[value='+check_id+']').prop('checked', true);
                }*/
                if(check_id=="trustees"){
                    $("#ed2_ajax").hide()
                    $("#ed3_ajax").hide()
                    $("#ed2_ajax").html('');
                    $("#ed3_ajax").html('');
                    $('#ed1_ajax').html(html);
                    $('#ed1_ajax').show();
                }else if(check_id=="beneficiary"){
                    $('#executor_details').html(html);
                }else if(check_id=="other"){
                    $("#ed1_ajax").hide()
                    $("#ed2_ajax").hide()
                    $("#ed1_ajax").html('');
                    $("#ed2_ajax").html('');
                    $('#ed3_ajax').html(html);
                    $('#ed3_ajax').show();
                }
                copy_permanent_address();
                copy_correspondence_address();
            }
        });
    }
}

function validate_executor(){
    $("select").prop("disabled", false); 
	$("input").prop("disabled", false); 
	$("input").prop("readonly", false); 

    $('#correspondence_address_line1').prop('readonly', false);
    $('#correspondence_address_line2').prop('readonly', false);
    $('#correspondence_address_line3').prop('readonly', false);
    $('#correspondence_city_village_town').prop('readonly', false);
    $('#correspondence_span_other_state').prop('readonly', false);
    $('#correspondence_zipcode').prop('readonly', false);
    $('input[name=correspondence_state_other]').prop('readonly', false);

    $('#correspondence_country').prop('disabled', false);
    $('#correspondence_is_foreign_address').prop('disabled', false);
}



function copy_permanent_address()
{
    $('.same_per_address').click(function(){
        var click_account_id = $(this).attr("account_count");
        var for_address = $(this).attr("for_address");
        if($(this).val()=="P"){
			clear_is_same_as_permanent();
            if($('#permanent_is_same_as_permanent').is(":checked")){
                $('#permanent_is_same_as_correspondence').prop('checked', false);
                //$('#permanent_is_same_as_correspondence').prop('disabled', false);
                //$('#permanent_is_same_as_permanent').prop('disabled', false);
                $('#permanent_addr_copied_from').val('P');
                var filtervars = {
                    do_what:'populate_address',
                    address_value:"P",
                    address_for:"normalpermanent",
                    account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
                };
                $.ajax({
                    type: "POST",
                    url: "_ajax_files/get_address_ajax.php",
                    data: filtervars,
                    dataType: "html",
                    success: function(html){
                        $('#populate_'+for_address+'_address').html(html);
                        //add_functions();

                        $('.clscorrespondenceAdd').click(function(){
						if($(this).attr("value")=="correspondenceAdd"){
							if(this.checked){
								$('#is_correspondence_same_as_permanent').val('1'); // setting the hidden variabel value to 1;
								$('#correspondence_address_line1').val($('#permanent_address_line1').val());
								$('#correspondence_address_line2').val($('#permanent_address_line2').val());
								$('#correspondence_address_line3').val($('#permanent_address_line3').val());
								$('#correspondence_city_village_town').val($('#permanent_city_village_town').val());
								$('#correspondence_country').val($('#permanent_country').val());
								$('#correspondence_state').val($('#permanent_state').val());
								var checked;
								if($('#permanent_is_foreign_address').prop('checked'))
									checked = 1;
								else
									checked = 0;
								populate_country_disable($('#permanent_country').val(), "correspondence_country", "span_correspondence_country", checked, "");
								$('#correspondence_state').val($('#permanent_state').val());
								if($('#permanent_country').val()!="")
									populate_states_disable($('#permanent_country').val(), "correspondence_state", "span_correspondence_state",$('#permanent_state').val(), "", "correspondence_state");
								if($('#permanent_span_other_state').is(':visible')){
									$('#correspondence_span_other_state').show();
									//$('#permanent_span_other_state').prop('readonly', true);
								}else{
									$('#correspondence_span_other_state').hide();
								}
								if(checked != 1)
									$('#correspondence_span_other_state').attr('style', 'display:none;');

								$('#correspondence_state_other').val($('#permanent_state_other').val());
								$('#correspondence_zipcode').val($('#permanent_zipcode').val());
								$('#correspondence_is_foreign_address').prop('checked',$('#permanent_is_foreign_address').prop('checked'));
								$('input[name=correspondence_state_other]').val($('input[name=permanent_state_other]').val());

								$('#correspondence_address_line1').prop('readonly', true);
								$('#correspondence_address_line2').prop('readonly', true);
								$('#correspondence_address_line3').prop('readonly', true);
								$('#correspondence_city_village_town').prop('readonly', true);
								$('#correspondence_state_other').prop('readonly', true);
								$('#correspondence_zipcode').prop('readonly', true);
								$('input[name=correspondence_state_other]').prop('readonly', true);

								$('#correspondence_country').prop('disabled', true);
								$('#correspondence_is_foreign_address').prop('disabled', true);

								$('#correspondence_is_same_as_permanent').prop('checked', $('#permanent_is_same_as_permanent').prop('checked'));
								$('#correspondence_is_same_as_correspondence').prop('checked', $('#permanent_is_same_as_correspondence').prop('checked'));
								$('#correspondence_is_same_as_permanent').attr('disabled', true);
								$('#correspondence_is_same_as_correspondence').attr('disabled', true);
								$('#correspondence_addr_copied_from').val($('#permanent_addr_copied_from').val());
						
								$.validate({form:"#frmexecutordetails", onSuccess:validate_executor});
								copy_permanent_address();
								copy_correspondence_address();
								is_foreign_address();
								//$('#correspondence_state')

								if(checked ==1)
								{
									$('#permanent_zipcode').removeClass( "numeric" );
									$('#permanent_zipcode').attr( "placeholder", "Zipcode" );
									$('#permanent_zipcode').attr( "maxlength", "25" );
									$('#permanent_zipcode').attr( "data-validation-length", "5-25" );
									$('#permanent_zipcode').attr( "data-validation-error-msg", "Please enter valid Zipcode" );
									
									$('#correspondence_zipcode').removeClass( "numeric" );
									$('#correspondence_zipcode').attr( "placeholder", "Zipcode" );
									$('#correspondence_zipcode').attr( "maxlength", "25" );
									$('#correspondence_zipcode').attr( "data-validation-length", "5-25" );
									$('#correspondence_zipcode').attr( "data-validation-error-msg", "Please enter valid Zipcode" );
								}
								else{
									$('#permanent_zipcode').addClass( "numeric" );
									$('#permanent_zipcode').attr( "placeholder", "Pincode" );
									$('#permanent_zipcode').attr( "maxlength", "6" );
									$('#permanent_zipcode').attr( "data-validation-length", "1-6" );
									$('#permanent_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
									
									$('#correspondence_zipcode').addClass( "numeric" );
									$('#correspondence_zipcode').attr( "placeholder", "Pincode" );
									$('#correspondence_zipcode').attr( "maxlength", "6" );
									$('#correspondence_zipcode').attr( "data-validation-length", "1-6" );
									$('#correspondence_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
								}

							}else{
							
								$('#is_correspondence_same_as_permanent').val('0'); // setting the hidden variabel value to 0;
								$('#correspondence_address_line1').val('');
								$('#correspondence_address_line1').prop('readonly', false);
								$('#correspondence_address_line2').val('');
								$('#correspondence_address_line2').prop('readonly', false);
								$('#correspondence_address_line3').val('');
								$('#correspondence_address_line3').prop('readonly', false);
								$('#correspondence_city_village_town').val('');
								$('#correspondence_city_village_town').prop('readonly', false);
								$('#correspondence_country').val('');
								$('#correspondence_country').prop('disabled', false);
								//$('#correspondence_state').val('');
								$('#correspondence_state').find('option').remove().end().append('<option value="">Please select</option>').val('');
								
								$('#correspondence_is_foreign_address').prop('checked', false);
								$('#correspondence_is_foreign_address').prop('disabled', false);
								$('#correspondence_is_foreign_address').prop('readonly', false);
								populate_country(102, "correspondence_country", "span_correspondence_country", 0, "");
								populate_states(102, "correspondence_state", "span_correspondence_state","", "", "correspondence_state");
								$('#correspondence_span_other_state').attr('style', 'display:none;');
								$('#correspondence_state').prop('disabled', false);
								$('#correspondence_state_other').val('');
								$('#correspondence_state_other').prop('disabled', false);
								$('#correspondence_state_other').prop('readonly', false);
								$('#correspondence_zipcode').val('');
								$('#correspondence_zipcode').prop('readonly', false);
								//------------------------------------------- changes 8-8-14 -----------------------------------------//
								$('#correspondence_is_same_as_permanent').prop('checked', false);
								$('#correspondence_is_same_as_correspondence').prop('checked', false);
								$('#correspondence_is_same_as_permanent').prop('disabled', false);
								$('#correspondence_is_same_as_correspondence').prop('disabled', false);
								$('#correspondence_addr_copied_from').val('');
								
								$('#permanent_zipcode').addClass( "numeric" );
								$('#permanent_zipcode').attr( "placeholder", "Pincode" );
								$('#permanent_zipcode').attr( "maxlength", "6" );
								$('#permanent_zipcode').attr( "data-validation-length", "1-6" );
								$('#permanent_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
								
								$('#correspondence_zipcode').addClass( "numeric" );
								$('#correspondence_zipcode').attr( "placeholder", "Pincode" );
								$('#correspondence_zipcode').attr( "maxlength", "6" );
								$('#correspondence_zipcode').attr( "data-validation-length", "1-6" );
								$('#correspondence_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
								//------------------------------------------- End changes 8-8-14 -----------------------------------------//
                                $('#correspondence_is_same_as_permanent').prop('disabled', $('#permanent_is_same_as_permanent').prop('disabled'));
                                $('#correspondence_is_same_as_correspondence').prop('disabled', $('#permanent_is_same_as_correspondence').prop('disabled'));
								copy_permanent_address();
								copy_correspondence_address();
								is_foreign_address();
							}
						}               
					});
                    }
                });
            }
            else {
				//$('#permanent_is_same_as_correspondence').prop('disabled', false);  
				//$('#permanent_is_same_as_permanent').prop('disabled', false); 
				$('#permanent_is_same_as_permanent').prop('checked', false);
				$('#permanent_addr_copied_from').val('');
				//----------------------- changes 8-8-14 ------------------//
				$('#permanent_is_foreign_address').prop('disabled', false);
				$('#permanent_is_foreign_address').prop('checked', false);
				$('#permanent_is_foreign_address').prop('readonly', false);
				$('#permanent_address_line1').prop('readonly', false);
				$('#permanent_address_line2').prop('readonly', false);
				$('#permanent_address_line3').prop('readonly', false);
				$('#permanent_city_village_town').prop('readonly', false);
				$('#permanent_zipcode').prop('readonly', false);
				$('#permanent_country').prop('disabled', false);
				$('#permanent_state').prop('disabled', false);
				$('#permanent_state_other').prop('readonly', false);

				$('#permanent_address_line1').val('');
				$('#permanent_address_line2').val('');
				$('#permanent_address_line3').val('');
				$('#permanent_city_village_town').val('');
				$('#permanent_zipcode').val('');
				populate_country(102, "permanent_country", "span_permanent_country", 0, "");
				populate_states(102, "permanent_state", "span_permanent_state", "", "permanent_state");
				$('#permanent_state_other').val('');
				$('#permanent_span_other_state').attr('style', 'display:none;');
				$('#permanent_span_other_state').attr('style', 'display:none;');
				$('#permanent_zipcode').addClass('numeric');
				$('#permanent_zipcode').attr('maxlength', '6');
				$('#permanent_zipcode').attr('data-validation', 'length');
				$('#permanent_zipcode').attr('data-validation-length', '6-6');
				$('#permanent_zipcode').attr('data-validation-error-msg', 'Please enter valid Pincode');
				$('.numeric').numeric();
				is_foreign_address();
				//----------------------- End changes 8-8-14 ---------------------//
            }
        }else if($(this).val()=="C")
        {
			clear_is_same_as_permanent();
            if($('#permanent_is_same_as_correspondence').is(":checked")){
                $('#permanent_is_same_as_permanent').prop('checked', false);
                //$('#permanent_is_same_as_permanent').prop('disabled', false);
                //$('#permanent_is_same_as_correspondence').prop('disabled', false);
                $('#permanent_addr_copied_from').val('C');
                var filtervars = {
                    do_what:'populate_address',
                    address_value:"C",
                    address_for:"normalpermanent",
                    account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
                };
                $.ajax({
                    type: "POST",
                    url: "_ajax_files/get_address_ajax.php",
                    data: filtervars,
                    dataType: "html",
                    success: function(html){
                        $('#populate_'+for_address+'_address').html(html);
                        $.validate();
                        //add_functions();

                       $('.clscorrespondenceAdd').click(function(){
						if($(this).attr("value")=="correspondenceAdd"){
							if(this.checked){
								$('#is_correspondence_same_as_permanent').val('1'); // setting the hidden variabel value to 1;
								$('#correspondence_address_line1').val($('#permanent_address_line1').val());
								$('#correspondence_address_line2').val($('#permanent_address_line2').val());
								$('#correspondence_address_line3').val($('#permanent_address_line3').val());
								$('#correspondence_city_village_town').val($('#permanent_city_village_town').val());
								$('#correspondence_country').val($('#permanent_country').val());
								$('#correspondence_state').val($('#permanent_state').val());
								var checked;
								if($('#permanent_is_foreign_address').prop('checked'))
									checked = 1;
								else
									checked = 0;
								populate_country_disable($('#permanent_country').val(), "correspondence_country", "span_correspondence_country", checked, "");
								$('#correspondence_state').val($('#permanent_state').val());
								if($('#permanent_country').val()!="")
									populate_states_disable($('#permanent_country').val(), "correspondence_state", "span_correspondence_state",$('#permanent_state').val(), "", "correspondence_state");
								if($('#permanent_span_other_state').is(':visible')){
									$('#correspondence_span_other_state').show();
									//$('#permanent_span_other_state').prop('readonly', true);
								}else{
									$('#correspondence_span_other_state').hide();
								}
								if(checked != 1)
									$('#correspondence_span_other_state').attr('style', 'display:none;');

								$('#correspondence_state_other').val($('#permanent_state_other').val());
								$('#correspondence_zipcode').val($('#permanent_zipcode').val());
								$('#correspondence_is_foreign_address').prop('checked',$('#permanent_is_foreign_address').prop('checked'));
								$('input[name=correspondence_state_other]').val($('input[name=permanent_state_other]').val());

								$('#correspondence_address_line1').prop('readonly', true);
								$('#correspondence_address_line2').prop('readonly', true);
								$('#correspondence_address_line3').prop('readonly', true);
								$('#correspondence_city_village_town').prop('readonly', true);
								$('#correspondence_state_other').prop('readonly', true);
								$('#correspondence_zipcode').prop('readonly', true);
								$('input[name=correspondence_state_other]').prop('readonly', true);

								$('#correspondence_country').prop('disabled', true);
								$('#correspondence_is_foreign_address').prop('disabled', true);

								$('#correspondence_is_same_as_permanent').prop('checked', $('#permanent_is_same_as_permanent').prop('checked'));
								$('#correspondence_is_same_as_correspondence').prop('checked', $('#permanent_is_same_as_correspondence').prop('checked'));
								$('#correspondence_is_same_as_permanent').attr('disabled', true);
								$('#correspondence_is_same_as_correspondence').attr('disabled', true);
								$('#correspondence_addr_copied_from').val($('#permanent_addr_copied_from').val());
						
								$.validate({form:"#frmexecutordetails", onSuccess:validate_executor});
								copy_permanent_address();
								copy_correspondence_address();
								is_foreign_address();
								//$('#correspondence_state')

								if(checked ==1)
								{
									$('#permanent_zipcode').removeClass( "numeric" );
									$('#permanent_zipcode').attr( "placeholder", "Zipcode" );
									$('#permanent_zipcode').attr( "maxlength", "25" );
									$('#permanent_zipcode').attr( "data-validation-length", "5-25" );
									$('#permanent_zipcode').attr( "data-validation-error-msg", "Please enter valid Zipcode" );
									
									$('#correspondence_zipcode').removeClass( "numeric" );
									$('#correspondence_zipcode').attr( "placeholder", "Zipcode" );
									$('#correspondence_zipcode').attr( "maxlength", "25" );
									$('#correspondence_zipcode').attr( "data-validation-length", "5-25" );
									$('#correspondence_zipcode').attr( "data-validation-error-msg", "Please enter valid Zipcode" );
								}
								else{
									$('#permanent_zipcode').addClass( "numeric" );
									$('#permanent_zipcode').attr( "placeholder", "Pincode" );
									$('#permanent_zipcode').attr( "maxlength", "6" );
									$('#permanent_zipcode').attr( "data-validation-length", "1-6" );
									$('#permanent_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
									
									$('#correspondence_zipcode').addClass( "numeric" );
									$('#correspondence_zipcode').attr( "placeholder", "Pincode" );
									$('#correspondence_zipcode').attr( "maxlength", "6" );
									$('#correspondence_zipcode').attr( "data-validation-length", "1-6" );
									$('#correspondence_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
								}

							}else{
							
								$('#is_correspondence_same_as_permanent').val('0'); // setting the hidden variabel value to 0;
								$('#correspondence_address_line1').val('');
								$('#correspondence_address_line1').prop('readonly', false);
								$('#correspondence_address_line2').val('');
								$('#correspondence_address_line2').prop('readonly', false);
								$('#correspondence_address_line3').val('');
								$('#correspondence_address_line3').prop('readonly', false);
								$('#correspondence_city_village_town').val('');
								$('#correspondence_city_village_town').prop('readonly', false);
								$('#correspondence_country').val('');
								$('#correspondence_country').prop('disabled', false);
								//$('#correspondence_state').val('');
								$('#correspondence_state').find('option').remove().end().append('<option value="">Please select</option>').val('');
								
								$('#correspondence_is_foreign_address').prop('checked', false);
								$('#correspondence_is_foreign_address').prop('disabled', false);
								$('#correspondence_is_foreign_address').prop('readonly', false);
								populate_country(102, "correspondence_country", "span_correspondence_country", 0, "");
								populate_states(102, "correspondence_state", "span_correspondence_state","", "", "correspondence_state");
								$('#correspondence_span_other_state').attr('style', 'display:none;');
								$('#correspondence_state').prop('disabled', false);
								$('#correspondence_state_other').val('');
								$('#correspondence_state_other').prop('disabled', false);
								$('#correspondence_state_other').prop('readonly', false);
								$('#correspondence_zipcode').val('');
								$('#correspondence_zipcode').prop('readonly', false);
								//------------------------------------------- changes 8-8-14 -----------------------------------------//
								$('#correspondence_is_same_as_permanent').prop('checked', false);
								$('#correspondence_is_same_as_correspondence').prop('checked', false);
								$('#correspondence_is_same_as_permanent').prop('disabled', false);
								$('#correspondence_is_same_as_correspondence').prop('disabled', false);
								$('#correspondence_addr_copied_from').val('');
								
								$('#permanent_zipcode').addClass( "numeric" );
								$('#permanent_zipcode').attr( "placeholder", "Pincode" );
								$('#permanent_zipcode').attr( "maxlength", "6" );
								$('#permanent_zipcode').attr( "data-validation-length", "1-6" );
								$('#permanent_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
								
								$('#correspondence_zipcode').addClass( "numeric" );
								$('#correspondence_zipcode').attr( "placeholder", "Pincode" );
								$('#correspondence_zipcode').attr( "maxlength", "6" );
								$('#correspondence_zipcode').attr( "data-validation-length", "1-6" );
								$('#correspondence_zipcode').attr( "data-validation-error-msg", "Please enter valid Pincode" );
								//------------------------------------------- End changes 8-8-14 -----------------------------------------//
                                $('#correspondence_is_same_as_permanent').prop('disabled', $('#permanent_is_same_as_permanent').prop('disabled'));
                                $('#correspondence_is_same_as_correspondence').prop('disabled', $('#permanent_is_same_as_correspondence').prop('disabled'));
								copy_permanent_address();
								copy_correspondence_address();
								is_foreign_address();
							}
						}               
					});
                    }
                });
            }
            else {
				
				//$('#permanent_is_same_as_correspondence').prop('disabled', false);       
				//$('#permanent_is_same_as_permanent').prop('disabled', false);
				$('#permanent_is_same_as_permanent').prop('checked', false);
				$('#permanent_addr_copied_from').val('');
				//----------------------- changes 8-8-14 ------------------//
				$('#permanent_is_foreign_address').prop('disabled', false);
				$('#permanent_is_foreign_address').prop('checked', false);
				$('#permanent_is_foreign_address').prop('readonly', false);
				$('#permanent_address_line1').prop('readonly', false);
				$('#permanent_address_line2').prop('readonly', false);
				$('#permanent_address_line3').prop('readonly', false);
				$('#permanent_city_village_town').prop('readonly', false);
				$('#permanent_zipcode').prop('readonly', false);
				$('#permanent_country').prop('disabled', false);
				$('#permanent_state').prop('disabled', false);
				$('#permanent_state_other').prop('readonly', false);

				$('#permanent_address_line1').val('');
				$('#permanent_address_line2').val('');
				$('#permanent_address_line3').val('');
				$('#permanent_city_village_town').val('');
				$('#permanent_zipcode').val('');
				populate_country(102, "permanent_country", "span_permanent_country", 0, "");
				populate_states(102, "permanent_state", "span_permanent_state", "", "permanent_state");
				$('#permanent_state_other').val('');
				$('#permanent_span_other_state').attr('style', 'display:none;');
				$('#permanent_zipcode').addClass('numeric');
				$('#permanent_zipcode').attr('maxlength', '6');
				$('#permanent_zipcode').attr('data-validation', 'length');
				$('#permanent_zipcode').attr('data-validation-length', '6-6');
				$('#permanent_zipcode').attr('data-validation-error-msg', 'Please enter valid Pincode');
				$('.numeric').numeric();
				is_foreign_address();
				//----------------------- End changes 8-8-14 ---------------------//            
			}
        }
    }); 

}


function copy_correspondence_address()
{
    $('.same_cor_address').click(function(){
        var click_account_id = $(this).attr("account_count");
        var for_address = $(this).attr("for_address");
        if($(this).val()=="P"){
            if($('#correspondence_is_same_as_permanent').is(":checked")){
                $('#correspondence_is_same_as_correspondence').prop('checked', false);
                //$('#correspondence_is_same_as_correspondence').prop('disabled', false);
                //$('#correspondence_is_same_as_permanent').prop('disabled', false);
                $('#correspondence_addr_copied_from').val('P');
                var filtervars = {
                    do_what:'populate_address',
                    address_value:"P",
                    address_for:"normalcorrespondence",
                    account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
                };
                $.ajax({
                    type: "POST",
                    url: "_ajax_files/get_address_ajax.php",
                    data: filtervars,
                    dataType: "html",
                    success: function(html){
                        $('#populate_'+for_address+'_address').html(html);
                        $.validate();
                        //add_functions();
                    }
                });
            }
            else {
                //$('#correspondence_is_same_as_permanent').prop('disabled', false);       
				//$('#correspondence_is_same_as_correspondence').prop('disbaled', false);
				$('#correspondence_is_same_as_correspondence').prop('checked', false);
				$('#correspondence_addr_copied_from').val('');
				//----------------------- changes 8-8-14 ------------------//
				$('#correspondence_is_foreign_address').prop('disabled', false);
				$('#correspondence_is_foreign_address').prop('checked', false);
				$('#correspondence_is_foreign_address').prop('readonly', false);
				$('#correspondence_address_line1').prop('readonly', false);
				$('#correspondence_address_line2').prop('readonly', false);
				$('#correspondence_address_line3').prop('readonly', false);
				$('#correspondence_city_village_town').prop('readonly', false);
				$('#correspondence_zipcode').prop('readonly', false);
				$('#correspondence_country').prop('disabled', false);
				$('#correspondence_state').prop('disabled', false);
				$('#correspondence_state_other').prop('readonly', false);

				$('#correspondence_address_line1').val('');
				$('#correspondence_address_line2').val('');
				$('#correspondence_address_line3').val('');
				$('#correspondence_city_village_town').val('');
				$('#correspondence_zipcode').val('');
				populate_country(102, "correspondence_country", "span_correspondence_country", 0, "");
				populate_states(102, "correspondence_state", "span_correspondence_state", "", "correspondence_state");
				$('#correspondence_state_other').val('');
				$('#correspondence_span_other_state').attr('style', 'display:none;');
				$('#correspondence_zipcode').addClass('numeric');
				$('#correspondence_zipcode').attr('maxlength', '6');
				$('#correspondence_zipcode').attr('data-validation', 'length');
				$('#correspondence_zipcode').attr('data-validation-length', '6-6');
				$('#correspondence_zipcode').attr('data-validation-error-msg', 'Please enter valid Pincode');
				$('.numeric').numeric();
				is_foreign_address();
				//----------------------- End changes 8-8-14 ---------------------//
                
            }
        }
        else if($(this).val()=="C")
        {
            if($('#correspondence_is_same_as_correspondence').is(":checked")){
                $('#correspondence_is_same_as_permanent').prop('checked', false);
                //$('#correspondence_is_same_as_permanent').prop('disabled', false);
                //$('#correspondence_is_same_as_correspondence').prop('disabled', false);
                $('#correspondence_addr_copied_from').val('C');
                var filtervars = {
                    do_what:'populate_address',
                    address_value:"C",
                    address_for:"normalcorrespondence",
                    account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
                };
                $.ajax({
                    type: "POST",
                    url: "_ajax_files/get_address_ajax.php",
                    data: filtervars,
                    dataType: "html",
                    success: function(html){
                        $('#populate_'+for_address+'_address').html(html);
                        $.validate();
                        //add_functions();
                    }
                });
            }
            else {
				//$('#correspondence_is_same_as_permanent').prop('disabled', false);       
				$('#correspondence_is_same_as_correspondence').prop('checked', false);
				$('#correspondence_addr_copied_from').val('');
				//----------------------- changes 8-8-14 ------------------//
				$('#correspondence_is_foreign_address').prop('disabled', false);
				$('#correspondence_is_foreign_address').prop('checked', false);
				$('#correspondence_is_foreign_address').prop('readonly', false);
				$('#correspondence_address_line1').prop('readonly', false);
				$('#correspondence_address_line2').prop('readonly', false);
				$('#correspondence_address_line3').prop('readonly', false);
				$('#correspondence_city_village_town').prop('readonly', false);
				$('#correspondence_zipcode').prop('readonly', false);
				$('#correspondence_country').prop('disabled', false);
				$('#correspondence_state').prop('disabled', false);
				$('#correspondence_state_other').prop('readonly', false);

				$('#correspondence_address_line1').val('');
				$('#correspondence_address_line2').val('');
				$('#correspondence_address_line3').val('');
				$('#correspondence_city_village_town').val('');
				$('#correspondence_zipcode').val('');
				populate_country(102, "correspondence_country", "span_correspondence_country", 0, "");
				populate_states(102, "correspondence_state", "span_correspondence_state", "", "correspondence_state");
				$('#correspondence_state_other').val('');
				$('#correspondence_span_other_state').attr('style', 'display:none;');
				$('#correspondence_zipcode').addClass('numeric');
				$('#correspondence_zipcode').attr('maxlength', '6');
				$('#correspondence_zipcode').attr('data-validation', 'length');
				$('#correspondence_zipcode').attr('data-validation-length', '6-6');
				$('#correspondence_zipcode').attr('data-validation-error-msg', 'Please enter valid Pincode');
				$('.numeric').numeric();
				is_foreign_address();
				//----------------------- End changes 8-8-14 ---------------------//
            }
        }
    }); 
}


	function clear_is_same_as_permanent()
	{
		if($('.clscorrespondenceAdd').is(':checked')){
				$('.clscorrespondenceAdd').prop('checked', false);
				
					$('#correspondence_is_same_as_permanent').prop('disabled', false);
					$('#correspondence_is_same_as_correspondence').prop('disabled', false);      
					$('#correspondence_is_same_as_permanent').prop('readonly', false);
					$('#correspondence_is_same_as_correspondence').prop('readonly', false);      
					$('#correspondence_is_same_as_permanent').prop('checked', false);
					$('#correspondence_is_same_as_correspondence').prop('checked', false);
					$('#correspondence_addr_copied_from').val('');
					//----------------------- changes 8-8-14 ------------------//
					$('#correspondence_is_foreign_address').prop('disabled', false);
					$('#correspondence_is_foreign_address').prop('readonly', false);
					$('#correspondence_is_foreign_address').prop('checked', false);
					$('#correspondence_address_line1').prop('readonly', false);
					$('#correspondence_address_line2').prop('readonly', false);
					$('#correspondence_address_line3').prop('readonly', false);
					$('#correspondence_city_village_town').prop('readonly', false);
					$('#correspondence_zipcode').prop('readonly', false);
					$('#correspondence_country').prop('disabled', false);
					$('#correspondence_state').prop('disabled', false);
					$('#correspondence_state_other').prop('readonly', false);

					$('#correspondence_address_line1').val('');
					$('#correspondence_address_line2').val('');
					$('#correspondence_address_line3').val('');
					$('#correspondence_city_village_town').val('');
					$('#correspondence_zipcode').val('');
					populate_country(102, "correspondence_country", "span_correspondence_country", 0, "");
					populate_states(102, "correspondence_state", "span_correspondence_state", "", "correspondence_state");
					$('#correspondence_state_other').val('');
					$('#correspondence_span_other_state').attr('style', 'display:none;');
					//----------------------- End changes 8-8-14 ---------------------//
					$('.numeric').numeric();
					is_foreign_address();
			}
	}
function check_minor()
{
    var startDate = new Date($("#show_birth_date").val());
    if(startDate!=""){
        var date = $('#show_birth_date').datepicker('getDate');
        var selectedYear = startDate.getFullYear();
        var today = new Date();
        var todayYear = today.getFullYear();
        var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));

        /*if((diff < 18 && $('#eum_is_adult').val()==1) || (diff > 18 && $('#eum_is_adult').val()==0)){
            $("#show_birth_date").val("");
            $("#birth_date").val("");
        }*/
        if((diff < 18 && $('#is_adult').is(':checked')) || (diff > 18 && !$('#is_adult').is(':checked'))){
            $("#show_birth_date").val("");
            $("#birth_date").val("");
        }
    }
}

function add_functions(){
    addPhoneNumber();
    addOfficeNumber();
    addPhoneMobile();
    addFaxNumber();
    addEmail();
}
