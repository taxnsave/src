$(document).ready(function(){
    $(".uname").keyup(function(){
		$("#fullname").val($("#title option:selected").text()+" "+$("#firstname").val()+" "+$("#middlename").val()+" "+$("#lastname").val());
    });

	/*$('#permanent_is_foreign_address').click(function(){
		if(this.checked){
			$('#permanent_country option[value="102"]').remove();
		}else{
			$('#permanent_country option[value="102"]').append();
		}
	});
	*/
    
    $("#gender").change(function (){
        var v = $(this).val();
        if(v == 2 || v == 1) {
            $("#radio_spouse").show();
            $("#span_spouse").show();
            $("#radio_spouse").removeAttr("disabled");
        } else {
            $("#radio_spouse").attr("disabled", true);
            $("#radio_spouse").hide();
            $("#span_spouse").hide();
        }
    });
    //alert('Hello');
	$('#show_birth_date').datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		minDate: "-150Y",
		//maxDate: "-18Y",
		yearRange: "-150:+0", 
		altField: "#birth_date",
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect: function (dateText, inst) {
			/*var startDate = new Date(dateText);
			var date = $(this).datepicker('getDate');
			var selectedYear = date.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
			if(diff > 100)
				alert('You have selected '+selectedYear+' as birth year');
			if(diff >= 18)
				$('#is_adult').val("1");
			else
				$('#is_adult').val("0");*/

			var date = $('#show_birth_date').datepicker('getDate');
			var selectedYear = date.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));



			if(diff > 100)
				alert('You have selected '+selectedYear+' as birth year');

			if(diff>=18)
				$("#is_adult").prop("checked", true);
			else
				$("#is_adult").prop("checked", false);
		}
		
	});
	
	/*function toggle_other_religion(religion, span)
	{
	
		if(religion == 10){
			$("#"+span).show();
		}
		else{
			$("#"+span).hide();
		}
	}*/
	
	$('#show_father_mother_husband_birthdate').datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		minDate: "-150Y",
		maxDate: new Date(),
		yearRange: "-150:+0",
		altField: "#father_mother_husband_birthdate",
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect: function (dateText, inst) {
			var startDate = new Date(dateText);
			var date = $(this).datepicker('getDate');
			var selectedYear = startDate.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
			if(diff > 100)
				alert('You have selected '+selectedYear+' as birth year');
			
		}
		
	});

	var check_date = $('#birth_date').val();

	$('#show_anniversary_date').datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		minDate: new Date(check_date),
		//maxDate: new Date(),
		yearRange: "-120:+0",
		altField: "#anniversary_date",
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect: function (dateText, inst) {
			var startDate = new Date(dateText);
			var date = $(this).datepicker('getDate');
			var selectedYear = startDate.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
			
			if(diff > 100)
				alert('You have selected '+selectedYear+' as Anniversary year');
			
		}
		
	});

	/*$('#show_willdate').datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		//maxDate: new Date(),
		yearRange: "-120:+0",
		minDate: new Date(),
		altField: "#willdate",
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true
		
	});*/
	
	$('#chkcorrespondence').click(function(){
		if(this.checked){
			
			$('#correspondence_address_line1').val($('#permanent_address_line1').val());
			$('#correspondence_address_line1').prop('readonly', true);
			$('#correspondence_address_line2').val($('#permanent_address_line2').val());
			$('#correspondence_address_line2').prop('readonly', true);
			$('#correspondence_address_line3').val($('#permanent_address_line3').val());
			$('#correspondence_address_line3').prop('readonly', true);
			$('#correspondence_city_village_town').val($('#permanent_city_village_town').val());
			$('#correspondence_city_village_town').prop('readonly', true);
			$('#correspondence_country').val($('#permanent_country').val());
			$('#correspondence_country').prop('disabled', true);
			$('#correspondence_state').val($('#permanent_state').val());
			

			var checked;
			if($('#permanent_is_foreign_address').prop('checked'))
				checked = 1;
			else
				checked = 0;
			populate_country_disable($('#permanent_country').val(), "correspondence_country", "span_correspondence_country", checked, "");
			if($('#correspondence_country').val()!="")
				populate_states_disable($('#permanent_country').val(), "correspondence_state", "span_correspondence_state",$('#permanent_state').val(), "", "correspondence_state");
			
			if($('#permanent_state_other').is(':visible')){
                $('#span_correspondence_state_other').show();
				//$('#permanent_state_other').prop('readonly', true);
            }else{
                $('#span_correspondence_state_other').hide();
            }

			
			$('#correspondence_state').prop('disabled', true);
			$('#correspondence_state_other').val($('#permanent_state_other').val());
			$('#correspondence_state_other').prop('readonly', true);
			$('#correspondence_zipcode').val($('#permanent_zipcode').val());
			$('#correspondence_zipcode').prop('readonly', true);
			$('#correspondence_is_foreign_address').prop('checked',$('#permanent_is_foreign_address').prop('checked'));
			$('#correspondence_is_foreign_address').prop('disabled', true);

			if(checked == 1){
				$('#correspondence_zipcode').attr('data-validation', 'length');
				$('#correspondence_zipcode').attr('data-validation-length', '5-25');
				$('#correspondence_zipcode').attr('data-validation-error-msg', 'Please enter valid Zipcode for correspondence address');
			}
			else{
				$('#correspondence_zipcode').attr('data-validation', 'length');
				$('#correspondence_zipcode').attr('data-validation-length', '6-6');
				$('#correspondence_zipcode').attr('data-validation-error-msg', 'Please enter valid Pincode for correspondence address');
			}
			$.validate({onSuccess:validate_personal_info});

		}else{
			$('#correspondence_address_line1').val('');
			$('#correspondence_address_line1').prop('readonly', false);
			$('#correspondence_address_line2').val('');
			$('#correspondence_address_line2').prop('readonly', false);
			$('#correspondence_address_line3').val('');
			$('#correspondence_address_line3').prop('readonly', false);
			$('#correspondence_city_village_town').val('');
			$('#correspondence_city_village_town').prop('readonly', false);
			$('#correspondence_country').val('');
			$('#correspondence_country').prop('disabled', false);
			//$('#correspondence_state').val('');
			$('#correspondence_state').find('option').remove().end().append('<option value="">Please select</option>').val('');
			$('#correspondence_state').prop('disabled', false);
			$('#correspondence_state_other').val('');
			$('#correspondence_state_other').prop('readonly', false);
			$('#correspondence_zipcode').val('');
			$('#correspondence_zipcode').prop('readonly', false);
			$('#correspondence_is_foreign_address').attr('checked', false);
			$('#correspondence_is_foreign_address').prop('disabled', false);
			populate_country(102, "correspondence_country", "span_correspondence_country", 0, "");
			populate_states(102, "correspondence_state", "span_correspondence_state","", "", "correspondence_state");
			$('#correspondence_zipcode').attr('maxlength', '6');
			$('#correspondence_zipcode').addClass('numeric');
			$('.numeric').numeric();
			$('#correspondence_zipcode').attr('data-validation', 'length');
			$('#correspondence_zipcode').attr('data-validation-length', '6-6');
			$('#correspondence_zipcode').attr('data-validation-error-msg', 'Please enter valid Pincode for correspondence address');
			$('#span_correspondence_state_other').hide();
			$.validate({onSuccess:validate_personal_info});
			//$('#correspondence_is_foreign_address').val('checked',$('#permanent_is_foreign_address').prop('checked'));
			
		}
	});

	$('.numeric').numeric();
	$("#fullname").val($("#title option:selected").text()+" "+$("#firstname").val()+" "+$("#middlename").val()+" "+$("#lastname").val());

	$("#btn-next").click(function(){
		$("#validate_process").val("0");
		$("#btn_clicked").val("next");
		var form = $('form#frmProfile').get(0);
		$(form).removeData('validate');
		document.frmProfile.submit();
	});
	$("#btn-update").click(function(){
		$("#validate_process").val("1");
	});
	$("#btn-save-next").click(function(){
		$("#validate_process").val("1");
		$("#btn_clicked").val("next");
	});
	/*function marital_status_change(value)
	{
		if(value==1){
			$('#show_anniversary_date').val('');
			$('#anniversary_date').val('');
			$("#span_anniversary_date").hide();
		}
		else{
			$('#show_anniversary_date').val('');
			$('#anniversary_date').val('');
			$("#span_anniversary_date").show();
		}
	}*/

	$('#nationality').change(function(){
		var check = $('#nationality').val();
		if(check == 'indian' || check == '' ){
			$('#other_nationality_div').hide();

            $('#other_nationality').attr('data-validation', '');
            $('#other_nationality').attr('data-validation-length', '');
            $('#other_nationality').attr('data-validation-error-msg', '');

            $.validate({onSuccess:validate_personal_info});
		}else{
			$('#other_nationality_div').show();

			$('#other_nationality').attr('data-validation', 'length');
            $('#other_nationality').attr('data-validation-length', '1-60');
            $('#other_nationality').attr('data-validation-error-msg', 'Please enter valid Other Nationality');

            $.validate({onSuccess:validate_personal_info});
		}
	});

	$('input[name=father_mother_husband_relation]').click(function(){
		if($(this).val() == 'N'){
			$('#father_mother_husband_lastname').val('');
			$('#father_mother_husband_firstname').val('');
			$('#father_mother_husband_middlename').val('');
			$('#father_mother_husband_title').val('');
			
			$('#father_mother_husband_lastname').attr('readonly', true);
			$('#father_mother_husband_firstname').attr('readonly', true);
			$('#father_mother_husband_middlename').attr('readonly', true);
			$('#father_mother_husband_title').attr('disabled', true);
		}else{
			$('#father_mother_husband_lastname').attr('readonly', false);
			$('#father_mother_husband_firstname').attr('readonly', false);
			$('#father_mother_husband_middlename').attr('readonly', false);
			$('#father_mother_husband_title').attr('disabled', false);
		}
	});

});
	

	function title_change()
	{
		$("#fullname").val($("#title option:selected").text()+" "+$("#firstname").val()+" "+$("#middlename").val()+" "+$("#lastname").val());
	} 

	function marital_status_change(value)
	{
		if($("#marital_status option:selected").text().toLowerCase() == 'single' || value==''){		
			$('#show_anniversary_date').val('');
			$('#anniversary_date').val('');
			$("#span_anniversary_date").hide();

			var filtervars = {
                do_what: 'update_anniversary_date',
                auth_token: $("#auth_token_name").val()
            };
            $.ajax({
                type: "POST",
                url: "_ajax_files/personalinfo_ajax.php",
                data: filtervars,
                dataType: "html",
                success: function(html){
               	}
            });
		}
		else{
			$("#span_anniversary_date").show();

		}
	}

	function toggle_other_religion(religion, span)
	{
		if($('#religion option:selected').text().toLowerCase() == 'other'){
			$("#"+span).show();
		}
		else{
			$('#religion_other').val('');
			$("#"+span).hide();
		}
	}

	function check_minor()
	{
		var startDate = new Date($("#show_birth_date").val());
		if(startDate!=""){
			/*var selectedYear = startDate.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff =  todayYear-selectedYear;
			if((diff < 18 && $('#is_adult').val()==1) || (diff > 18 && $('#is_adult').val()==0)){
				$("#show_birth_date").val("");
				$("#birth_date").val("");
			}*/
			var date = $('#show_birth_date').datepicker('getDate');
			var selectedYear = startDate.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));

			/*if((diff < 18 && $('#eum_is_adult').val()==1) || (diff > 18 && $('#eum_is_adult').val()==0)){
				$("#show_birth_date").val("");
				$("#birth_date").val("");
			}*/
			if((diff < 18 && $('#is_adult').is(':checked')) || (diff > 18 && !$('#is_adult').is(':checked'))){
				$("#show_birth_date").val("");
				$("#birth_date").val("");
			}
		}
	}


function validate_personal_info()
{
	$("select").prop("disabled", false); 
	$("input").prop("readonly", false); 
	$("input").prop("disabled", false); 

	$('#correspondence_country').prop('disabled', false);
	$('#correspondence_state').prop('disabled', false);
	$('#correspondence_is_foreign_address').prop('disabled', false);

	if($('#title').val() == ""){
		$('#error1').html('Please select a title');
		$('#error1').addClass('error');
		$('#title').addClass("input_error_border");
		$('html, body').animate({scrollTop: $("#div_personal_info").offset().top}, 100);
		return false;
	}

	/*
	if ($("input[@name='father_mother_husband_relation']:checked").val()) {
		if($("#father_mother_husband_title").val()==""){
			alert("Please select "+$("input[@name='father_mother_husband_relation']:checked").val()+"' Title");
			return false;
		}
		if($("#father_mother_husband_lastname").val()==""){
			alert("Please enter "+$("input[@name='father_mother_husband_relation']:checked").val()+"' Last Name / Surname");
			return false;
		}
	}
	*/

	/*
	if($('#gender').val() == ""){
		$('#error1').html('Please select a gender');
		$('#error1').addClass('error');
		$('#gender').addClass("input_error_border");
		$('html, body').animate({scrollTop: $("#div_personal_info").offset().top}, 100);
		return false;
	}
	if($('#marital_status').val() == ""){
		$('#error1').html('Please select marital status');
		$('#error1').addClass('error');
		$('#marital_status').addClass("input_error_border");
		$('html, body').animate({scrollTop: $("#div_personal_info").offset().top}, 100);
		return false;
	}
	if($('#permanent_country').val() == ""){
		$('#error2').html('Please select a country');
		$('#error2').addClass('error');
		$('#permanent_country').addClass("input_error_border");
		$('html, body').animate({scrollTop: $("#div_permanent_address").offset().top}, 100);
		return false;
	}
	if($('#permanent_state').val() == ""){
		$('#error2').html('Please select a state');
		$('#error2').addClass('error');
		$('#permanent_state').addClass("input_error_border");
		$('html, body').animate({scrollTop: $("#div_permanent_address").offset().top}, 100);
		return false;
	}
	if($('#correspondence_country').val() == ""){
		$('#error3').html('Please select a country');
		$('#error3').addClass('error');
		$('#correspondence_country').addClass("input_error_border");
		$('html, body').animate({scrollTop: $("#div_permanent_address").offset().top}, 100);
		return false;
	}
	if($('#correspondence_state').val() == ""){
		$('#error3').html('Please select a state');
		$('#error3').addClass('error');
		$('#correspondence_state').addClass("input_error_border");
		$('html, body').animate({scrollTop: $("#div_permanent_address").offset().top}, 100);
		return false;
	}
	if($('#religion'.val() == "")){
		$('#error4').html('Please select a religion');
		$('#error4').addClass('error');
		$('#religion').addClass("input_error_border");
		$('html, body').animate({scrollTop: $("#div_other_details").offset().top}, 100);
		return false;
	}
	*/
	return true;
}

 $(function() {
        var MaxInputs1       = 5; //maximum input boxes allowed
        var TelephoneAddWrap   = $("#TelephoneAddWrap"); //Input boxes wrapper ID
        var AddButton       = $("#TelephoneAddBtn"); //Add button ID
       // var x = TelephoneAddWrap.length; //initlal text box count
	    var num_phone_numbers = $('#num_phone_numbers').val(); //initlal text box count
		var FieldCount=1; //to keep track of text box added
        $(AddButton).click(function (e)  //on add input button click
        {
                if(num_phone_numbers < MaxInputs1) //max input box allowed
                {
                    FieldCount++; //text box added increment
                    //add input box
                    $(TelephoneAddWrap).append('<div class="row"><div class="divider"></div><div class="col-xs-plus"><span class="plus_sign" >+</span></div><div class="col-xs-2"><input type="text" class="form-control numeric" name="phone_isd[]" placeholder="ISD" title="Please enter contry code for Telephone Number" alt="Please enter contry code for Telephone Number" maxlength="3" value="91" /></div><div class="col-xs-3"><input type="text" class="form-control numeric" name="phone_std[]" placeholder="STD" maxlength="5"  title="Please enter STD Code for Telephone Number" alt="Please enter STD Code for Telephone Number"  /></div><div class="col-xs-5 last"><input type="text" class="form-control numeric" name="phone_number[]" placeholder="Phone Number"  title="Please enter Telephone Number" alt="Please enter Telephone Number" maxlength="10"/></div><a href="#" class="btn-delete TelephoneRemove" title="Please click to remove the entered Office Phone Numbers" alt="Please click to remove the entered Office Phone Numbers">&ndash;</a></div>');
					$('.numeric').numeric();
                    num_phone_numbers++; //text box increment
					 $('#num_phone_numbers').val(num_phone_numbers); 
                }
				else if(num_phone_numbers == MaxInputs1)
					alert('You can not add more than '+MaxInputs1+' Telephone numbers');
        return false;
        });
        $("body").on("click",".TelephoneRemove", function(e){ //user click on remove text
               if( num_phone_numbers >= 1 ) {
                        $(this).parent('div').remove(); //remove text box
						num_phone_numbers--; //decrement textbox
						$('#num_phone_numbers').val(num_phone_numbers);
                }

        return false;
        });


        var MaxInputs2       = 5; //maximum input boxes allowed
        var OfficeAddWrap   = $("#OfficeAddWrap"); //Input boxes wrapper ID
        var OfficeAddButton       = $("#OfficeAddBtn"); //Add button ID
        var x = OfficeAddWrap.length; //initlal text box count
		var num_office_numbers = $('#num_office_numbers').val();
        var FieldCount=1; //to keep track of text box added
        $(OfficeAddButton).click(function (e)  //on add input button click
        {
                if(num_office_numbers < MaxInputs2) //max input box allowed
                {
                    FieldCount++; //text box added increment
                    //add input box
                    $(OfficeAddWrap).append('<div class="row"><div class="divider"></div><div class="col-xs-plus"><span class="plus_sign" >+</span></div><div class="col-xs-2"><input type="text" class="form-control numeric" name="office_phone_isd[]" value="91" placeholder="ISD"  maxlength="3" title="Please enter contry code for Office Phone Number" alt="Please enter contry code for Office Phone Number"/></div><div class="col-xs-3"><input type="text" class="form-control numeric" name="office_phone_std[]" placeholder="STD" maxlength="5" title="Please enter STD Code for Office Phone Number" alt="Please enter STD Code for Office Phone Number"/></div><div class="col-xs-5 last"><input type="text" class="form-control numeric" name="office_phone_number[]" placeholder="Office Number" maxlength="10" title="Please enter Office Phone Number" alt="Please enter Office Phone Number" /></div><a href="#" class="btn-delete OfficeRemove" title="Please click to remove the entered Office Phone Numbers" alt="Please click to remove the entered Office Phone Numbers">&ndash;</a></div>');
					$('.numeric').numeric();
                    num_office_numbers++; //text box increment
                }
				else if(num_office_numbers == MaxInputs2)
					alert('You can not add more than '+MaxInputs2+' Office phone numbers');
        return false;
        });
        $("body").on("click",".OfficeRemove", function(e){ //user click on remove text
                if( num_office_numbers >= 1 ) {
                        $(this).parent('div').remove(); //remove text box
                        num_office_numbers--; //decrement textbox
                        $('#num_office_numbers').val(num_office_numbers);
                }
        return false;
        });
		
        var MaxInputs3       = 3; //maximum input boxes allowed
        var MobileAddWrap   = $("#MobileAddWrap"); //Input boxes wrapper ID
        var MobAddButton       = $("#MobileAddBtn"); //Add button ID
        var x = MobileAddWrap.length; //initlal text box count
		var num_mobile_numbers = $('#num_mobile_numbers').val();
        var FieldCount=1; //to keep track of text box added
        $(MobAddButton).click(function (e)  //on add input button click
        {
			    if(num_mobile_numbers < MaxInputs3) //max input box allowed
                {
                    FieldCount++; //text box added increment
                    //add input box
                    $(MobileAddWrap).append('<div class="row"><div class="divider"></div><div class="col-xs-plus"><span class="plus_sign" >+</span></div><div class="col-xs-2"><input type="text" class="form-control numeric" name="mobile_isd[]" value="91" laceholder="ISD" maxlength="3" title="Please enter contry code for Mobile Number" alt="Please enter contry code for Mobile Number"/></div><div class="col-xs-8 last"><input type="text" class="form-control numeric" name="mobile_number[]" placeholder="Mobile Number" maxlength="10"  title="Please enter Mobile Number" alt="Please enter Mobile Number"/></div><a href="#" class="btn-delete MobileRemove" title="Remove Mobile number" alt="Remove Mobile number">&ndash;</a></div>');
					$('.numeric').numeric();
                    num_mobile_numbers++; //text box increment
                }
				else if(num_mobile_numbers == MaxInputs3)
					alert('You can not add more than '+MaxInputs3+' Mobile numbers');
        return false;
        });
        $("body").on("click",".MobileRemove", function(e){ //user click on remove text
                if( num_mobile_numbers >= 1 ) {
                        $(this).parent('div').remove(); //remove text box
                        num_mobile_numbers--; //decrement textbox
                        $('#num_mobile_numbers').val(num_mobile_numbers);
                }
        return false;
        });



      
      

		var MaxInputs5       = 3; //maximum input boxes allowed
        var EmailAddWrap    = $("#EmailAddWrap"); //Input boxes wrapper ID
        var EmailAddButton  = $("#EmailAddBtn"); //Add button ID
        var x = EmailAddWrap.length; //initlal text box count
		var num_emails = $('#num_emails').val();
        var FieldCount=1; //to keep track of text box added
        $(EmailAddButton).click(function (e)  //on add input button click
        {
			if(num_emails < MaxInputs5) //max input box allowed
			{
				FieldCount++; //text box added increment
				//add input box
				//$(EmailAddWrap).append('<div class="row"><div class="divider"></div><div class="col-xs-8 last"><input type="text" class="form-control" name="extra_email[]" placeholder="Email" title="Please enter only valid and active email ID" alt="Please enter only valid and active email ID" maxlength="254" data-required="false" data-validation="email" data-validation-length="3-254" data-validation-error-msg="Please enter valid Email" /></div><a href="#" class="btn-delete EmailRemove"  title="Please click to remove the entered email IDs" alt="Please click to remove the entered email IDs">&ndash;</a></div>');
                $(EmailAddWrap).append('<div class="row"><div class="divider"></div><div class="col-xs-8 last"><input type="text" class="form-control" name="extra_email[]" placeholder="Email" title="Please enter only valid and active email ID" alt="Please enter only valid and active email ID" maxlength="254" data-validation-optional="true" data-validation="email" data-validation-length="3-254" data-validation-error-msg="Please enter valid Email" /></div><a href="#" class="btn-delete EmailRemove"  title="Please click to remove the entered email IDs" alt="Please click to remove the entered email IDs">&ndash;</a></div>');
				num_emails++; //text box increment
				$('#num_emails').val(num_emails);
			}
			else if(num_emails == MaxInputs5)
				alert('You can not add more than '+(MaxInputs5)+' Emails');
        return false;
        });
        $("body").on("click",".EmailRemove", function(e){ //user click on remove text
             
			   if( num_emails >= 1 ) {
					
                        $(this).parent('div').remove(); //remove text box
                        num_emails--; //decrement textbox
                }
        return false;
        });
});
