var ben_count = 1;
var actual_ben_count = 1;

$(document).ready(function(){
    on_ready_functions();

	$(".ben_edit").click(function(){
		 
        var ben_id = $(this).attr("rec_id");
		$("#hid_ben_id").val(ben_id);
		document.frmeditNonBeneficiary.submit();
    });

	$(".ben_delete").click(function(){
		
		if( !confirm("Are you sure you want to delete this Non-Beneficiary?") )
            return;
        var filtervars = {
            do_what:'del_nonbeneficiary',
            delete_ids: $(this).attr("rec_id"),
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/non-beneficiaries_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                alert("Non-Beneficiary details deleted successfully");
                window.location.reload('non-beneficiaries.php');
            }
        });
    });

    if($("#ben_id").val() > 0){
        $("#mfAddBtn").addClass("hide");
    }
});

function on_ready_functions(){
    $("#mfAddBtn").unbind("click");

    $("#mfAddBtn").click(function(){
        ben_count++;
        actual_ben_count++;

        var filtervars = {
            do_what:'add_nonbeneficiary',
            ben_count:ben_count,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/non-beneficiaries_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
               $('#mfAddWrap').append(html);
               add_functions();
            }
        });
    });
    add_functions();
}

function add_functions(){
    
    $(".mfRemove").unbind("click");
    $(".mfRemove").click(function(){
        $('#num'+$(this).attr("remove_id")).remove();
        add_functions();
    });
    
    change_jah_ben_numbering();
}