var phone_count = new Array();
var office_phone_count = new Array();
var fax_count = new Array();
var mobile_count = new Array();
var email_count = new Array();

var actual_phone_count = new Array();
var actual_office_phone_count = new Array();
var actual_fax_count = new Array();
var actual_mobile_count = new Array();
var actual_email_count = new Array();

var account_count = 1;
var actual_account_count = 1;

var beneficiary_count = 1;
var actual_beneficiary_count = 1;
var total_count = 1;

$(document).ready(function(){
   if(($("#ast_id").val()>0 && $('input:radio[name=ownership_type]:checked').val()=="joint")  || ($("#joint_account_count").val()>0 && $('input:radio[name=ownership_type]:checked').val()=="joint")){
		$("#div_joint_account_holders").html($("#div_joint_account_holders_hidden").html());
	}
	
    on_ready_functions();
    is_foreign_address();

	$('#asset_type').change(function(){
		$('#others').val('');
	});


	
	/* Edit bank account */
    $(".a_edit").click(function(){
        var ast_id = $(this).attr("rec_id");
        $("#hid_ast_id").val(ast_id);
        document.frmHidden.submit();
    });


    $(".a_delete").click(function(){
        if( !confirm("Are you sure you want to delete this Digital asset details?") )
            return;
            
        $("#loading").show();
        
        var filtervars = {
            do_what:'del_digitalasset',
            delete_ids: $(this).attr("rec_id"),
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/digital-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                alert("Digital asset details deleted successfully");
                window.location.href='digital-assets-details';
            }
        });
    });
    $('.numeric').numeric();
	$('.numeric_share').numeric({decimal:"."});
});

function on_ready_functions()
{
    $.validate({form:"#frmDigitalasset", onSuccess:validate_digitalasset});
    
	$('#loan_against_asset').click(function(){
		if($(this).is(":checked")){
			$('#span_lender_details').show();
		}else{
			$('#span_lender_details').hide();
		}
	});
	
	$('#asset_type').change(function(){
		var selected  = $('#asset_type option:selected').text();
		var selected_lower = selected.toLowerCase(); 
		if(selected_lower == 'others' || selected_lower == 'other')
			$('#span_other_asset').show();
		else
			$('#span_other_asset').hide();
	});

    $(".sel_ownership_type").change(function(){
        var o_type = $(this).val();
        if(o_type=="single" || o_type==""){
            /*
			if($("#joint_account_count").val()>0)
				$("#div_joint_account_holders_hidden").html($("#div_joint_account_holders").html());
            $("#div_joint_account_holders").html("");
            */
            if($("#joint_account_count").val()>0){
                if(!confirm("You are changing the ownership type from 'Joint' to 'Single'.\nThe data entered for Joint Account Holder shall be cleared if you continue.\nPlease click on 'OK' to continue or else click on 'Cancel'.")){
                    $(":radio[value=joint]").prop("checked",true);
                    return false;
                }
            }
            $("#div_joint_account_holders").html("");
            $("#joint_account_count").val("0"); 
            account_count = actual_account_count = total_count = 1;
            return;
        }
        if($("#joint_account_count").val()==0)
        {
            var filtervars = {
                do_what:'get_joint_accounts_html',
                sbm_id:$("#sbm_id").val(),
                auth_token: $("#auth_token_name").val()
            };

            $.ajax({
                type: "POST",
                url: "_ajax_files/digital-assets-details_ajax.php",
                data: filtervars,
                dataType: "html",
                success: function(html){
                    $(".a_add_account").remove();
                    $("#joint_account_count").val(actual_account_count);
                    $("#total_count").val(parseInt(total_count));
                    $("#div_joint_account_holders").html(html);
                    $.validate({form:"#frmDigitalasset", onSuccess:validate_digitalasset});
                    add_functions();
                    $('.numeric').numeric();
					$('.numeric_share').numeric({decimal:"."});
                    is_foreign_address();
                }
            });
        }else{
            $("#div_joint_account_holders").html($("#div_joint_account_holders_hidden").html());
            add_functions();
        }
    });
    
    toggle_other_state('state','state_other');
    $( ".sel_country" ).trigger( "change" );
    add_functions();
}

function add_functions()
{
    /* Beneficiary */
    $(".a_beneficiary").unbind("click");
    $(".a_beneficiary").click(function(){
        var current_beneficiary = $(this).attr("rec_id");
        beneficiary_count++;
        actual_beneficiary_count++;
        var filtervars = {
            do_what:'add_beneficiary',
            beneficiary_count:beneficiary_count,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/digital-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $(".a_beneficiary").remove();
                $("#div_beneficiary").append(html);
                add_functions();
            }
        });
    });

	
	 /* Beneficiary 
	$(".a_beneficiarynew").unbind("click");
    $(".a_beneficiarynew").click(function(){
		var ben_id, share, other_info, ben_name;
		var ben_exist = false;
		var bcount = $("#tot_ben").val();
		var total_share = $('#total_share').val();
		ben_name = $("#sel_beneficiary option:selected").text();
		ben_id = $("#sel_beneficiary").val();
		share = $("#txt_beneficiary_share").val();
		other_info = $("#txt_other_info").val();
		total_share = parseInt(total_share) + parseInt(share);
		if(ben_id<=0){
			alert("Please select a Beneficiary.");
			return false;
		}
		else if(parseInt(share)<=0 || share=="" || share>100){
			alert("Please enter valid Share.");
			return false;
		}
		else if(total_share > 100)
		{
			alert("Beneficiary total Percentage Share exceeds 100%");
			return false;
		}
		else if(total_share == 100)
		{
			$('#beneficiaryAddBtnNew').hide();
		}
		$('.cls_ben').each(function( index ) {  
			if(ben_id == this.value){
				alert('Beneficiary already exists.');
				ben_exist = true;
			}
		}); 
		if(ben_exist == true)
		{
			$('#beneficiaryAddBtnNew').show();
			return false;
		}

		if(bcount == 1){
			$('#no_beneficiary').remove();
		}
		
		$("#tb_con_ben").append('<tr id="tr_con_ben_'+bcount+'"><td class="ben_new_account_numbering">'+bcount+'</td><td>'+ben_name+'<input type="hidden" class="w120px" name="ben_id[]" value="0"/><input type="hidden" class="cls_ben" name="beneficiary[]" value="'+ben_id+'"></td><td>'+share+'%<input type="hidden" name="beneficiary_share[]" value="'+share+'"></td><td>'+other_info+'<input type="hidden" name="other_info[]" value="'+other_info+'"></td><td><a href="javascript:void(0);" class="a_removebeneficiarynew" trid="tr_con_ben_'+bcount+'" trshare="'+share+'">Delete</a></td></tr>');
		
		$("#tot_ben").val(parseInt(bcount)+1);
		$('#total_share').val(total_share);
		add_functions();
		clear_add_beneficiary();
		change_jah_ben_numbering();
    });
*/

    $(".a_removebeneficiarynew").unbind("click");
    $(".a_removebeneficiarynew").click(function(){
		if( !confirm("Are you sure you want to delete this Beneficiary?") )
            return;
		else{
		var bcount		= $("#tot_ben").val();
		var trshare		= $(this).attr("trshare");
		var total_share = $('#total_share').val();
        var trid =  $(this).attr("trid");
		$("#"+$(this).attr("trid")).remove();
		total_share = parseInt(total_share) - parseInt(trshare);
		$('#total_share').val(total_share);
		$("#tot_ben").val(bcount-1);
		$('#beneficiaryAddBtnNew').show();
		
		alert('Beneficiary deleted successfully');
		change_jah_ben_numbering();
		}
    });
	
	function clear_add_beneficiary()
	{
		$('#sel_beneficiary').val('');
		$('#txt_beneficiary_share').val('');
		$('#txt_other_info').val('');
		$('#beneficiaryAddBtnNew').show();
	}

    /* add remove account holder */
    $(".a_add_account").unbind("click");
    $(".a_add_account").click(function(){
        account_count++;
        actual_account_count++;
        var filtervars = {
            do_what:'add_joint_account_holder',
            account_count:account_count,
            auth_token: $("#auth_token_name").val()
        };
        $("#joint_account_count").val(actual_account_count);
        $.ajax({
            type: "POST",
            url: "_ajax_files/digital-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $(".a_add_account").remove();
                $("#div_joint_account_holders").append(html);
                $("#total_count").val(account_count);
                add_functions();
                $.validate({form:"#frmDigitalasset", onSuccess:validate_digitalasset});
                $('.numeric').numeric();
				$('.numeric_share').numeric({decimal:"."});
                is_foreign_address();
                copy_address();
            }
        });
    });

    $(".a_removeaccount").unbind("click");
    $(".a_removeaccount").click(function(){
        actual_account_count--;
        if(actual_account_count==0){
            actual_account_count++;
            alert("You can not delete this account holder, Please add at-least joint account holder.");
            return;
        }

        $("#joint_account_count").val(actual_account_count);
        var remove_id = $(this).attr("remove_id");
        $("#account_"+remove_id).remove();
        var temp = $('#div_joint_account_holders .a_removeaccount').length;
        if(temp==0)
            temp=1;
        temp --;
        $('#span_account_'+$('.a_removeaccount').eq(temp).attr('remove_id')).html('<a href="javascript:void(0);" class="btn btn-primary btn-sm btn-add-large a_add_account">+ Add Joint Account Holder</a>');

        add_functions();
    });
    
    /* add remove account holder ends*/

    /* Email box */
    $(".a_jah_email").unbind("click");
    $(".a_jah_email").click(function(){
        var current_account = $(this).attr("rec_id");
        if(email_count[current_account]===undefined)
            email_count[current_account] = 2;
        else
            email_count[current_account]++;

        if(actual_email_count[current_account]===undefined)
            actual_email_count[current_account] = 2;
        else
            actual_email_count[current_account]++;

        if(actual_email_count[current_account]==4){
            actual_email_count[current_account] = 3;
            alert("You can not add more than 3 emails");
            return;
        }
        var filtervars = {
            do_what:'add_jah_email',
            jah_email_count:email_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/digital-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#JahEmailAddWrap_"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removejahemail").unbind("click");
    $(".a_removejahemail").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_email_count[current_account]===undefined)
            actual_email_count[current_account] = 1;
        else
            actual_email_count[current_account]--;
        $(this).parent('div').remove();
    });
    /* Email box ends */
    
    /* add remove phone */
    $(".a_phone").unbind("click");
    $(".a_phone").click(function(){
        var current_account = $(this).attr("rec_id");
        if(phone_count[current_account]===undefined)
            phone_count[current_account] = 2;
        else
            phone_count[current_account]++;

        if(actual_phone_count[current_account]===undefined)
            actual_phone_count[current_account] = 2;
        else
            actual_phone_count[current_account]++;
        if(actual_phone_count[current_account]==6){
            actual_phone_count[current_account] = 5;
            alert("You can not add more than 5 phone numbers");
            return;
        }
        var filtervars = {
            do_what:'add_phone',
            phone_count:phone_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/digital-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#TelephoneAddWrap_"+current_account).append(html);
                add_functions();
				$('.numeric').numeric();
				$('.numeric_share').numeric({decimal:"."});
            }
        });
    });

    $(".a_removephone").unbind("click");
    $(".a_removephone").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_phone_count[current_account]===undefined)
            actual_phone_count[current_account] = 1;
        else
            actual_phone_count[current_account]--;
        $(this).parent('div').remove();
    });
    /* add remove phone ends */

    /* Office Phone */
    $(".a_office_phone").unbind("click");
    $(".a_office_phone").click(function(){
        var current_account = $(this).attr("rec_id");
        if(office_phone_count[current_account]===undefined)
            office_phone_count[current_account] = 2;
        else
            office_phone_count[current_account]++;

        if(actual_office_phone_count[current_account]===undefined)
            actual_office_phone_count[current_account] = 2;
        else
            actual_office_phone_count[current_account]++;

        if(actual_office_phone_count[current_account]==6){
            actual_office_phone_count[current_account] = 5;
            alert("You can not add more than 5 office numbers");
            return;
        }
        var filtervars = {
            do_what:'add_office_phone',
            phone_count:office_phone_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/digital-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#InputsWrapperOffice_"+current_account).append(html);
                add_functions();
				$('.numeric').numeric();
				$('.numeric_share').numeric({decimal:"."});
            }
        });
    });

    $(".a_removeofficephone").unbind("click");
    $(".a_removeofficephone").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_office_phone_count[current_account]===undefined)
            actual_office_phone_count[current_account] = 1;
        else
            actual_office_phone_count[current_account]--;
        $(this).parent('div').remove();
        //$("#"+remove_id).remove();
    });
    /* Office Phone ends */

    /* Fax */
    $(".a_fax").unbind("click");
    $(".a_fax").click(function(){
        var current_account = $(this).attr("rec_id");
        if(fax_count[current_account]===undefined)
            fax_count[current_account] = 2;
        else
            fax_count[current_account]++;

        if(actual_fax_count[current_account]===undefined)
            actual_fax_count[current_account] = 2;
        else
            actual_fax_count[current_account]++;

        if(actual_fax_count[current_account]==6){
            actual_fax_count[current_account] = 5;
            alert("You can not add more than 5 fax numbers");
            return;
        }
        var filtervars = {
            do_what:'add_fax',
            phone_count:fax_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/digital-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#MobileAddWrap"+current_account).append(html);
                add_functions();
				$('.numeric').numeric();
				$('.numeric_share').numeric({decimal:"."});
            }
        });
    });

    $(".a_removefax").unbind("click");
    $(".a_removefax").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_fax_count[current_account]===undefined)
            actual_fax_count[current_account] = 1;
        else
            actual_fax_count[current_account]--;
        $(this).parent('div').remove();
        //$("#"+remove_id).remove();
    });
    /* Fax ends */

    /* Mobile box */
    $(".a_mobile").unbind("click");
    $(".a_mobile").click(function(){
        var current_account = $(this).attr("rec_id");
        if(mobile_count[current_account]===undefined)
            mobile_count[current_account] = 2;
        else
            mobile_count[current_account]++;

        if(actual_mobile_count[current_account]===undefined)
            actual_mobile_count[current_account] = 2;
        else
            actual_mobile_count[current_account]++;

        if(actual_mobile_count[current_account]==4){
            actual_mobile_count[current_account] = 3;
            alert("You can not add more than 3 mobile numbers");
            return;
        }
        var filtervars = {
            do_what:'add_mobile',
            phone_count:mobile_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/digital-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#MobileAddWrap_"+current_account).append(html);
                add_functions();
				$('.numeric').numeric();
				$('.numeric_share').numeric({decimal:"."});
            }
        });
    });

    $(".a_removemobile").unbind("click");
    $(".a_removemobile").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_mobile_count[current_account]===undefined)
            actual_mobile_count[current_account] = 1;
        else
            actual_mobile_count[current_account]--;
        $(this).parent('div').remove();
        //$("#"+remove_id).remove();
    });
    /* Mobile box ends */
 
    
    /* Beneficiary */
    $(".a_removebeneficiary").unbind("click");
    $(".a_removebeneficiary").click(function(){
        //alert(actual_beneficiary_count);
        if(actual_beneficiary_count==1){
            //actual_beneficiary_count++;
            alert("This is the default beneficary share and You cannot delete else you will need to define atleast one beneficary share.");
            return;
        }
        var remove_id = $(this).attr("remove_id");
        actual_beneficiary_count--;
        $("#div_beneficiary_"+remove_id).remove();

        $("#account_"+remove_id).remove();
        var temp = $('.a_removebeneficiary').length;
        temp --;
        
        $('#span_beneficiary_'+$('.a_removebeneficiary').eq(temp).attr('remove_id')).html('<a class="btn btn-primary btn-sm btn-add-large a_beneficiary" id="beneficiaryAddBtn" title="Add beneficiary" alt="Add beneficiary" href="javascript:void(0);">+ Add Beneficiary</a>');
        add_functions();
    });
    /* Beneficiary */

    copy_address();
    change_jah_ben_numbering();
}

function copy_address()
{
	$('.same_per_cor_address').unbind("click");
	$('.same_per_cor_address').click(function(){
		var click_account_id = $(this).attr("account_count");
		
		var for_address = $(this).attr("for_address");
		if($(this).val()=="P"){
			if($('#is_same_as_permanent_'+click_account_id).is(":checked")){
				$('#is_same_as_correspondence_'+click_account_id).prop('checked', false);
				//$('#is_same_as_permanent_'+click_account_id).prop('disabled', false);
				$('#is_same_as_permanent_or_correspondence_'+click_account_id).val('P');
				var filtervars = {
					do_what:'populate_address',
					address_value:"P",
					address_for:"jointaccountholder",
					account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#populate_'+for_address+'_address_'+click_account_id).html(html);
                        $.validate({form:"#frmDigitalasset", onSuccess:validate_digitalasset});
						add_functions();
					}
				});
			}
			else {
				
				//$('#is_same_as_correspondence_'+click_account_id).prop('disabled', false);		
				$('#is_same_as_permanent_'+click_account_id).prop('checked', false);
				$('#is_same_as_permanent_or_correspondence_'+click_account_id).val('');
				
				
				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#ejd_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#ejd_is_foreign_address_'+click_account_id).prop('checked', false);
				$('#ejd_address_line1_'+click_account_id).val('');
				$('#ejd_address_line1_'+click_account_id).prop('readonly', false);
				$('#ejd_address_line2_'+click_account_id).val('');
				$('#ejd_address_line2_'+click_account_id).prop('readonly', false);
				$('#ejd_address_line3_'+click_account_id).val('');
				$('#ejd_address_line3_'+click_account_id).prop('readonly', false);
				$('#ejd_city_village_town_'+click_account_id).val('');
				$('#ejd_city_village_town_'+click_account_id).prop('readonly', false);
				$('#ejd_zipcode_'+click_account_id).val('');
				$('#ejd_zipcode_'+click_account_id).prop('readonly', false);

				$('#ejd_country_'+click_account_id).prop('disbaled', false);
				
				populate_country(102, "ejd_country_"+click_account_id, "span_ejd_country_"+click_account_id, 0, "");
				populate_states(102, "ejd_state_"+click_account_id, "span_ejd_state_"+click_account_id, "", "");
				$("#ejd_state_"+click_account_id).val('');
				$("#ejd_state_"+click_account_id).prop('selected', '');
				$('#ejd_state_other_'+click_account_id).val('');
				$('#ejd_state_other_'+click_account_id).prop('readonly', false);
				$('#span_ejd_state_other_'+click_account_id).attr('style', 'display:none;');
				$('#ejd_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#ejd_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				$('.numeric_share').numeric({decimal:"."});
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- //
				
			}
		}
		else if($(this).val()=="C")
		{
			if($('#is_same_as_correspondence_'+click_account_id).is(":checked")){
				$('#is_same_as_permanent_'+click_account_id).prop('checked', false);
				//$('#is_same_as_correspondence_'+click_account_id).prop('disabled', false);
				$('#is_same_as_permanent_or_correspondence_'+click_account_id).val('C');
				var filtervars = {
					do_what:'populate_address',
					address_value:"C",
					address_for:"jointaccountholder",
					account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#populate_'+for_address+'_address_'+click_account_id).html(html);
                        $.validate({form:"#frmDigitalasset", onSuccess:validate_digitalasset});
						add_functions();
					}
				});
			}
			else {
				//$('#is_same_as_permanent_'+click_account_id).prop('disabled', false);		
				$('#is_same_as_correspondence_'+click_account_id).prop('checked', false);
				$('#is_same_as_permanent_or_correspondence_'+click_account_id).val('');

				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#ejd_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#ejd_is_foreign_address_'+click_account_id).prop('checked', false);
				$('#ejd_address_line1_'+click_account_id).val('');
				$('#ejd_address_line1_'+click_account_id).prop('readonly', false);
				$('#ejd_address_line2_'+click_account_id).val('');
				$('#ejd_address_line2_'+click_account_id).prop('readonly', false);
				$('#ejd_address_line3_'+click_account_id).val('');
				$('#ejd_address_line3_'+click_account_id).prop('readonly', false);
				$('#ejd_city_village_town_'+click_account_id).val('');
				$('#ejd_city_village_town_'+click_account_id).prop('readonly', false);
				$('#ejd_zipcode_'+click_account_id).val('');
				$('#ejd_zipcode_'+click_account_id).prop('readonly', false);

				$('#ejd_country_'+click_account_id).prop('disbaled', false);
				
				populate_country(102, "ejd_country_"+click_account_id, "span_ejd_country_"+click_account_id, 0, "");
				populate_states(102, "ejd_state_"+click_account_id, "span_ejd_state_"+click_account_id, "", "");
				$("#ejd_state_"+click_account_id).val('');
				$("#ejd_state_"+click_account_id).prop('selected', '');
				$('#ejd_state_other_'+click_account_id).val('');
				$('#span_ejd_state_other_'+click_account_id).attr('style', 'display:none;');
				$('#ejd_state_other_'+click_account_id).prop('readonly', false);
				$('#ejd_state_other_'+click_account_id).prop('disabled', false);
				$('#ejd_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#ejd_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				$('.numeric_share').numeric({decimal:"."});
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- //
			}
		}
	});	
}

function validate_digitalasset()
{
	$("select").prop("disabled", false); 
	$("input").prop("readonly", false); 
	$("input").prop("disabled", false); 
	
}

/* Note:- Othere Two functions [ copy_guardian_address() , copy_guardian_correspondence_address() ] are not copied from bank-account-details.js   */