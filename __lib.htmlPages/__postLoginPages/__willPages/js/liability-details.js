var beneficiary_count = 1;
var actual_beneficiary_count = 1;
var total_count = 1;

$(document).ready(function(){
    on_ready_functions();
    /* Edit bank account */
    $(".a_edit").click(function(){
        var lbt_id = $(this).attr("rec_id");
        $("#hid_lbt_id").val(lbt_id);
        document.frmHidden.submit();
    });
	var user_birth_date = $('#user_birth_date').val();
	
    $(".a_delete").click(function(){
        if( !confirm("Are you sure you want to delete this Liability details?") )
            return;
            
        $("#loading").show();
        
        var filtervars = {
            do_what:'del_liability',
            delete_ids: $(this).attr("rec_id"),
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/liability-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                alert("Liablity details deleted successfully.");
                window.location.href='liability-details';
            }
        });
    });

    $('#liability_type').change(function(){
    	$('input[name=institution_name]').val('');
    	$('input[name=institution_as_guarantor]').val('');
        var text = $('#liability_type option:selected').text();
        text = text.toLowerCase();
        if(text != 'guarantor'){
            $('#account_num_txt').html('Loan Account Number');
            $('input[name="account_number"]').attr('placeholder', 'Loan Account Number');
            $('input[name="account_number"]').attr('data-validation-error-msg', 'Please enter valid Loan Account Number');
            $('input[name="account_number"]').attr('title', 'Please enter Loan Account Number');
            $('input[name="account_number"]').attr('alt', 'Please enter Loan Account Number');

            $('#institution_name_txt').html('Name of the Individual / Institution');
            $('input[name="institution_name"]').attr('placeholder', 'Name of the Individual / Institution');
            $('input[name="institution_name"]').attr('data-validation-error-msg', 'Please enter valid Name of the Individual / Institution');
            $('input[name="institution_name"]').attr('title', 'Name of the Individual / Institution');
            $('input[name="institution_name"]').attr('alt', 'Name of the Individual / Institution');
            $('#institution_as_guarantor_div').hide();

            $('input[name="institution_as_guarantor"]').attr('placeholder', 'Name of the Individual / Institution');
            $('input[name="institution_as_guarantor"]').attr('data-validation', '');
            $('input[name="institution_as_guarantor"]').attr('data-validation-length', '');
            $('input[name="institution_as_guarantor"]').attr('data-validation-error-msg', '');

            $('input[name=account_number]').unbind();
            $('input[name=account_number]').removeClass('numeric');

            $('.numeric').numeric();
            $('.numeric_share').numeric({decimal:"."});
            $.validate({onSuccess:validate_liability});

        }else{
            $('#account_num_txt').html('Account Number');
            $('input[name="account_number"]').attr('placeholder', 'Account Number');
            $('input[name="account_number"]').attr('data-validation-error-msg', 'Please enter valid Account number');
            $('input[name="account_number"]').attr('title', 'Please enter Account Number');
            $('input[name="account_number"]').attr('alt', 'Please enter Account Number');
            

            $('#institution_name_txt').html('Name of the individual to whom you stand guarantor');
            $('input[name="institution_name"]').attr('placeholder', 'Name of the individual to whom you stand guarantor');
            $('input[name="institution_name"]').attr('data-validation-error-msg', 'Please enter valid Name of the individual to whom you stand guarantor');
            $('input[name="institution_name"]').attr('title', 'Name of the individual to whom you stand guarantor');
            $('input[name="institution_name"]').attr('alt', 'Name of the individual to whom you stand guarantor');
            $('#institution_as_guarantor_div').show();

            $('input[name="institution_as_guarantor"]').attr('data-validation', 'length');
            $('input[name="institution_as_guarantor"]').attr('data-validation-length', '1-99');
            $('input[name="institution_as_guarantor"]').attr('data-validation-error-msg', 'Please enter valid Name of the Institution where you stand guarantor');
            
            $('input[name=account_number]').addClass('numeric');
            $('.numeric').numeric();
            $('.numeric_share').numeric({decimal:"."});
            $.validate({onSuccess:validate_liability});
        }
    });
	
	$(".pay_from_estate").click(function (){
		var myval = $(this).val();
		if(myval=="1")
			$("#div_add_beneficiary").addClass("hide");
		else
			$("#div_add_beneficiary").removeClass("hide");
	});

    $('.numeric').numeric();
    $('.numeric_share').numeric({decimal:"."});
    $.validate({onSuccess:validate_liability});
});

function on_ready_functions()
{
    $.validate({onSuccess:validate_liability});
    toggle_other_state('state','state_other');
    $( ".sel_country" ).trigger( "change" );
    add_functions();
}

function add_functions()
{
	$.validate({onSuccess:validate_liability});
    /* Beneficiary */
    $(".a_beneficiary").unbind("click");
    $(".a_beneficiary").click(function(){
        var current_beneficiary = $(this).attr("rec_id");
        beneficiary_count++;
        actual_beneficiary_count++;
        var filtervars = {
            do_what:'add_beneficiary',
            beneficiary_count:beneficiary_count,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/liability-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $(".a_beneficiary").remove();
                $("#div_beneficiary").append(html);
                add_functions();
            }
        });
    });

    /* Beneficiary */
$(".a_beneficiarynew").unbind("click");
    $(".a_beneficiarynew").click(function(){
		var ben_id, share, other_info, ben_name;
		var ben_exist = false;
		var bcount = $("#tot_ben").val();
		var total_share = $('#total_share').val();
		ben_name = $("#sel_beneficiary option:selected").text();
		ben_id = $("#sel_beneficiary").val();
		share = $("#txt_beneficiary_share").val();
		other_info = $("#txt_other_info").val();
		total_share = parseInt(total_share) + parseInt(share);
		if(ben_id<=0 && (parseInt(share)<=0 || share=="" || share>100)){
			$('#sel_beneficiary').attr('style', ' ');
			$('#sel_beneficiary_error').remove();
			$('#sel_beneficiary').attr('style', 'border-top-color: red; border-right-color: red; border-bottom-color: red; border-left-color: red; ');
			($('#sel_beneficiary').parent()).append('<span class="help-block form-error error_ben" id="sel_beneficiary_error">Please select a to be Paid By</span>');
			$('#txt_beneficiary_share').attr('style', ' ');
			$('#txt_beneficiary_share_error').remove();
			$('#txt_beneficiary_share').attr('style', 'border-top-color: red; border-right-color: red; border-bottom-color: red; border-left-color: red; ');
			($('#txt_beneficiary_share').parent()).append('<span class="help-block form-error error_ben" id="txt_beneficiary_share_error">Please enter valid Percentage Share to be paid by</span>');
			return false;
		}
		if(ben_id<=0){
				$('#sel_beneficiary').attr('style', ' ');
				$('#sel_beneficiary_error').remove();
				$('#sel_beneficiary').attr('style', 'border-top-color: red; border-right-color: red; border-bottom-color: red; border-left-color: red; ');
				($('#sel_beneficiary').parent()).append('<span class="help-block form-error error_ben" id="sel_beneficiary_error">Please select a to be Paid By</span>');
				return false;
		}
		else{
			$('#sel_beneficiary').attr('style', ' ');
			$('#sel_beneficiary_error').remove();
		}
		if(parseInt(share)<=0 || share=="" || share>100){
				$('#txt_beneficiary_share').attr('style', ' ');
				$('#txt_beneficiary_share_error').remove();
				$('#txt_beneficiary_share').attr('style', 'border-top-color: red; border-right-color: red; border-bottom-color: red; border-left-color: red; ');
				($('#txt_beneficiary_share').parent()).append('<span class="help-block form-error error_ben" id="txt_beneficiary_share_error">Please enter valid Percentage Share to be Paid By</span>');
				return false;
		}
		else{
			$('#txt_beneficiary_share').attr('style', ' ');
			$('#txt_beneficiary_share_error').remove();
		}
		if(total_share > 100)
		{
			alert("Paid By total Percentage Share exceeds 100%");
			return false;
		}
		if(total_share == 100)
		{
			$('#beneficiaryAddBtnNew').hide();
		}
		$('.cls_ben').each(function( index ) {  
			if(ben_id == this.value){
				$('#sel_beneficiary').attr('style', 'border-top-color: red; border-right-color: red; border-bottom-color: red; border-left-color: red; ');
				($('#sel_beneficiary').parent()).append('<span class="help-block form-error error_ben" id="sel_beneficiary_error">To be Paid By already exists, Please select different to be Paid By</span>');
				ben_exist = true;
			}
		});
		if(ben_exist == true)
		{
			$('#beneficiaryAddBtnNew').show();
			return false;
		}

		if(bcount == 1){
			$('#no_beneficiary').remove();
		}
		other_info = other_info.replace(new RegExp('"','g'), '&#34;');		
		$("#tb_con_ben").append('<tr id="tr_con_ben_'+bcount+'"><td class="ben_new_account_numbering">'+bcount+'</td><td>'+ben_name+'<input type="hidden" class="w120px" name="paidby_id[]" value="0"/><input type="hidden" class="cls_ben" name="beneficiary[]" value="'+ben_id+'"></td><td>'+share+'%<input type="hidden" name="beneficiary_share[]" value="'+share+'"></td><td>'+other_info+'<input type="hidden" name="other_info[]" value="'+other_info+'"></td><td><a href="javascript:void(0);" class="a_removebeneficiarynew" trid="tr_con_ben_'+bcount+'" trshare="'+share+'">Delete</a></td></tr>');
		
		$("#tot_ben").val(parseInt(bcount)+1);
		$('#total_share').val(total_share);
		add_functions();
		clear_add_beneficiary();
		change_jah_ben_numbering();
    });

	 /* Beneficiary */
    $(".a_removebeneficiarynew").unbind("click");
    $(".a_removebeneficiarynew").click(function(){
		if( !confirm("Are you sure you want to delete this Paid By?") )
            return;
		else{
			var bcount		= $("#tot_ben").val();
			var trshare		= $(this).attr("trshare");
			var total_share = $('#total_share').val();
			var trid =  $(this).attr("trid");
			$("#"+$(this).attr("trid")).remove();
			total_share = parseInt(total_share) - parseInt(trshare);
			$('#total_share').val(total_share);
			$("#tot_ben").val(bcount-1);
			$('#beneficiaryAddBtnNew').show();
			change_jah_ben_numbering();
		}
    });

	function clear_add_beneficiary()
	{
		$('#sel_beneficiary').val('');
		$('#txt_beneficiary_share').val('');
		$('#txt_other_info').val('');
		$('#beneficiaryAddBtnNew').show();
	}
	

    /* shaow date pickers */    
	var user_birth_date = $('#user_birth_date').val().split("-");
	user_birth_date[0] = parseInt(user_birth_date[0]) + 18;
	var user_birth_date = user_birth_date[0]+"-"+user_birth_date[1]+"-"+user_birth_date[2];
	$('#show_starting_date').datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		minDate: new Date(user_birth_date),	
		//maxDate: new Date(),
		yearRange: "-150:+0",
		//minDate: new Date(),
		altField: "#start_date",
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect: function (dateText, inst) {
			var startDate = new Date(dateText);
			var selectedYear = startDate.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=  todayYear-selectedYear;
		}	
	});
	
	var today = new Date();
	var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

	$('#show_closing_date').datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		minDate: tomorrow,
		maxDate: "+60Y",
		yearRange: "0:+60",
		//minDate: new Date(),
		altField: "#closing_date",
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect: function (dateText, inst) {
			var startDate = new Date(dateText);
			var selectedYear = startDate.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=  todayYear-selectedYear;
		}	
	});

    /* shaow date pickers ends*/

    change_jah_ben_numbering();
}

function validate_liability(){
	$('select').prop('disabled', false);
}