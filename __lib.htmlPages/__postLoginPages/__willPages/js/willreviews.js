
$(document).ready(function(){

	$(".btn-submit").click(function(){
		//if( !confirm("On Clicking 'Submit' button, your data will be submitted for preparation of the Will document.") )
		if( !confirm("On Clicking 'Submit' button, you will not be able to modify the data and your data will be submitted for preparation of the Will document.") )
            return;
        var filtervars = {
            do_what:'save_will_reviews',
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/will-reviews_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
				alert(html);
				window.location = 'finalize-will-pay-options';
            }
        });
    });
});