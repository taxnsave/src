var phone_count = new Array();
var office_phone_count = new Array();
var fax_count = new Array();
var mobile_count = new Array();
var email_count = new Array();

var actual_phone_count = new Array();
var actual_office_phone_count = new Array();
var actual_fax_count = new Array();
var actual_mobile_count = new Array();
var actual_email_count = new Array();

var account_count = 1;
var actual_account_count = 1;

var beneficiary_count = 1;
var actual_beneficiary_count = 1;
var total_count = 1;

$(document).ready(function(){
	add_date_functions();
    if(($("#fxd_id").val()>0  && $('input:radio[name=ownership_type]:checked').val()=="joint")  || ($("#joint_account_count").val()>0 && $('input:radio[name=ownership_type]:checked').val()=="joint")){
        $("#div_joint_account_holders").html($("#div_joint_account_holders_hidden").html());
        add_functions();
        $('.numeric').numeric();
        $('.numeric_share').numeric({decimal:"."});
        is_foreign_address();
        add_date_functions($("#joint_account_count").val());
        add_guardian_birth_date_for_all($("#joint_account_count").val());
        add_guardian_functions($("#joint_account_count").val());
    }

    if($('input:radio[name=has_nominee]:checked').val()=="yes"){
        $("#div_nominee_detail").html($("#div_nominee_detail_hidden").html());
        birth_date_function();
        add_nominee();
        add_nominee_guardian();
        copy_nominee_address();
        copy_nominee_guardian_address();
    }
    
    $('.has_nominee').change(function(){
        if($(this).val() == 'yes'){
            $("#div_nominee_detail").html($("#div_nominee_detail_hidden").html());
            on_ready_functions();
            birth_date_function();
            add_nominee();
            add_nominee_guardian();
            copy_nominee_address();
            copy_nominee_guardian_address();
        }else if($(this).val() == 'no'){
            $("#div_nominee_detail").html('');
            $("#guardiant_main").html('');
        }
    });



    on_ready_functions();
    /* Edit bank account */
    $(".a_edit").click(function(){
        var fxd_id = $(this).attr("rec_id");
        $("#hid_fxd_id").val(fxd_id);
        document.frmHidden.submit();
    });
	

    $(".a_delete").click(function(){
        if( !confirm("Are you sure you want to delete this Fixed Deposit / Recurring Deposit account details?") )
            return;
            
        var filtervars = {
            do_what:'del_fixed_deposit',
            delete_ids: $(this).attr("rec_id"),
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/fixed-deposit-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                //alert(html);
				alert("Fixed Deposit / Recurring Deposit account details deleted successfully.");
                window.location.reload('fixed-deposit-details.php');
            }
        });
    });
	
    add_functions();
    //add_nominee_guardian();
    $.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
    $('.numeric').numeric();
    $('.numeric_share').numeric({decimal:"."});

});

function birth_date_function(){
    $('#show_birth_date').datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		//maxDate: new Date(),
		yearRange: "-150:+0",
		altField: "#birth_date",
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect: function (dateText, inst) {
			$('#show_birth_date').removeClass( "error" );
			$('#show_birth_date').attr("style", "");
			$('#show_birth_date').parent().removeClass('has-error');
			$('.help-block').remove();
			$('#show_birth_date').addClass( "valid" );
			$('#show_birth_date').parent().addClass('has-success');

			var startDate = new Date(dateText);
			var date = $(this).datepicker('getDate');
			var selectedYear = date.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
			if(diff > 100)
				alert('You have selected '+selectedYear+' as your birth year');
			if(diff >=18){
				$('#is_adult').val('0');
				$('#end_is_adult').val('1');
				$('#guardiant_main').html('');
			}
			else if(diff < 18){
				$('#is_adult').val('1');
				$('#end_is_adult').val('0');
				$('#guardiant_main').html($('#guardiant_main_hidden').html());
				$.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
				$('.numeric').numeric();
                $('.numeric_share').numeric({decimal:"."});
				$('#show_guardian_birth_date').datepicker({
					dateFormat: 'dd/mm/yy',
					numberOfMonths: 1,
					//maxDate: "-18Y",
					yearRange: "-150:+0",
					altField: "#guardian_birth_date",
					altFormat: "yy-mm-dd",
					changeMonth: true,
					changeYear: true,
					onSelect: function (dateText, inst) {
						$('#show_guardian_birth_date').removeClass( "error" );
						$('#show_guardian_birth_date').attr("style", "");
						$('#show_guardian_birth_date').parent().removeClass('has-error');
						$('.help-block').remove();
						$('#show_guardian_birth_date').addClass( "valid" );
						$('#show_guardian_birth_date').parent().addClass('has-success');

						var startDate = new Date(dateText);
						var date = $(this).datepicker('getDate');
						var selectedYear = date.getFullYear();
						var today = new Date();
						var todayYear = today.getFullYear();
						var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
						if(diff > 100)
							alert('You have selected '+selectedYear+' as birth year');
						else if(diff>=18)
							$("#end_guardian_is_adult").prop("checked", true);
						else
							$("#end_guardian_is_adult").prop("checked", false);
					}
				});
				guardian_is_adult_onchange();
				is_foreign_address();
				copy_nominee_address();
				copy_nominee_guardian_address();
			}
			else{
				$('#guardiant_main').html('');
			}
		}	
	});

}

function guardian_is_adult_onchange(){
	$('#end_guardian_is_adult').change(function()
	{
		$("#show_guardian_birth_date").val("");
		$("#guardian_birth_date").val("");
		
	});
}

function add_nominee(){
    var MaxInputs1 = 5;
    var TelephoneAddWrap   = $("#TelephoneAddWrap");
    var AddButton = $("#TelephoneAddBtn");
    var num_phone_numbers = $('#num_phone_numbers').val();
    var x = TelephoneAddWrap.length;
    var FieldCount=1;

    $(AddButton).click(function (e)
    {
        
    if(num_phone_numbers < MaxInputs1)
    {
       FieldCount++;
       $(TelephoneAddWrap).append('<div class="row"><div class="divider"></div><div class="col-xs-plus"><span class="plus_sign">+</span></div><div class="col-lg-2"><input type="text" class="form-control numeric" name="end_phone_isd[]" placeholder="ISD" value="91" maxlength="3" title="Please enter Telephone ISD" alt="Please enter Telephone ISD"/></div><div class="col-lg-3"><input type="text" class="form-control numeric" name="end_phone_std[]" placeholder="STD" maxlength="5" title="Please enter Telephone STD" alt="Please enter Telephone STD"/></div><div class="col-lg-5 last"><input type="text" class="form-control numeric"  name="end_phone_number[]" placeholder="Phone Number" maxlength="10" title="Please enter Telephone number" alt="Please enter Telephone number" /></div><a href="#" class="btn-delete TelephoneRemove"  title="Please click to remove the entered Telephone number" alt="Please click to remove the entered Telephone number">&ndash;</a></div></div>');
       num_phone_numbers++; 
       $('#num_phone_numbers').val(num_phone_numbers);
    }
    else if(num_phone_numbers == MaxInputs1)
            alert('You can not add more than '+MaxInputs1+' telephone phone numbers');

    $('.numeric').numeric();
    $('.numeric_share').numeric({decimal:"."});
    return false;
    });
    $("body").on("click",".TelephoneRemove", function(e)
    { 
    if( num_phone_numbers > 1 ) {
        $(this).parent('div').remove();
        num_phone_numbers--;
    }
    return false;
    });



    var MaxInputs3       = 3; //maximum input boxes allowed
    var MobileAddWrap   = $("#MobileAddWrap"); //Input boxes wrapper ID
    var MobAddButton       = $("#MobileAddBtn"); //Add button ID
    var x = MobileAddWrap.length; //initlal text box count
    var num_mobile_numbers = $('#num_mobile_numbers').val();
    var FieldCount=1; //to keep track of text box added
    $(MobAddButton).click(function (e)  //on add input button click
    {
            if(num_mobile_numbers < MaxInputs3) //max input box allowed
            {
                FieldCount++; //text box added increment
                //add input box
                $(MobileAddWrap).append('<div class="row"><div class="divider"></div><div class="col-xs-plus"><span class="plus_sign">+</span></div><div class="col-lg-2"><input type="text" class="form-control numeric" name="end_mobile_isd[]" placeholder="ISD" value="91" maxlength="3" title="Please enter Mobile ISD" alt="Please enter Mobile ISD"  /></div><div class="col-lg-8 last"><input type="text" class="form-control numeric" name="end_mobile_number[]" placeholder="Mobile Number" maxlength="10"  title="Please enter Mobile number" alt="Please enter Mobile number"/></div><a href="#" class="btn-delete MobileRemove"  title="Please click to remove the entered Mobile number" alt="Please click to remove the entered Mobile number">&ndash;</a></div>');
                num_mobile_numbers++; //text box increment
                $('#num_mobile_numbers').val(num_mobile_numbers);
            }
            else if(num_mobile_numbers == MaxInputs3)
                alert('You can not add more than '+MaxInputs3+' mobile numbers');

            $('.numeric').numeric();
            $('.numeric_share').numeric({decimal:"."});
    return false;
    });
    $("body").on("click",".MobileRemove", function(e){ //user click on remove text
            if( num_mobile_numbers > 1 ) {
                    $(this).parent('div').remove(); //remove text box
                    num_mobile_numbers--; //decrement textbox
            }
    return false;
    });
    

    var MaxInputs5       = 3; //maximum input boxes allowed
    var EmailAddWrap    = $("#EmailAddWrap"); //Input boxes wrapper ID
    var EmailAddButton  = $("#EmailAddBtn"); //Add button ID
    var x = EmailAddWrap.length; //initlal text box count
    var num_emails = $('#num_emails').val();
    var FieldCount=1; //to keep track of text box added
    $(EmailAddButton).click(function (e)  //on add input button click
    {
      if(num_emails < MaxInputs5) //max input box allowed
        {
            FieldCount++; //text box added increment
            //add input box
            $(EmailAddWrap).append('<div class="row"><div class="divider"></div><div class="col-lg-11 last"><input type="text" class="form-control" name="end_email[]" placeholder="Email" title="Please enter Nominee Email" alt="Please enter Nominee Email" /></div><a href="#" class="btn-delete EmailRemove" title="Please click to remove the entered Email Ids" alt="Please click to remove the entered Email Ids">&ndash;</a></div>');
            num_emails++; //text box increment
            $('#num_emails').val(num_emails);
        }
        else if(num_emails == MaxInputs5)
            alert('You can not add more than '+MaxInputs5+' Email ids');
    return false;
    });
    $("body").on("click",".EmailRemove", function(e){ //user click on remove text
            if( num_emails > 1 ) {
                    $(this).parent('div').remove(); //remove text box
                    num_emails--; //decrement textbox
            }
    return false;
    });
}

function on_ready_functions()
{
    $.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
    $(".sel_ownership_type").change(function(){
		
        var o_type = $(this).val();
	    if(o_type=="single" || o_type==""){
            /*
			if($("#joint_account_count").val()>0)
				$("#div_joint_account_holders_hidden").html($("#div_joint_account_holders").html());
            $("#div_joint_account_holders").html("");
            */
            if($("#joint_account_count").val()>0){
                if(!confirm("You are changing the ownership type from 'Joint' to 'Single'.\nThe data entered for Joint Account Holder shall be cleared if you continue.\nPlease click on 'OK' to continue or else click on 'Cancel'.")){
                    $(":radio[value=joint]").prop("checked",true);
                    return false;
                }
            }
            $("#div_joint_account_holders").html("");
            $("#joint_account_count").val("0"); 
            account_count = actual_account_count = total_count = 1;
            return;
        }
        if($("#joint_account_count").val()==0)
        {
			var filtervars = {
                do_what:'get_joint_accounts_html',
                fxd_id:$("#fxd_id").val(),
                auth_token: $("#auth_token_name").val()
            };

            $.ajax({
                type: "POST",
                url: "_ajax_files/fixed-deposit-details_ajax.php",
                data: filtervars,
                dataType: "html",
                success: function(html){
                    $(".a_add_account").remove();
                    $("#joint_account_count").val(actual_account_count);
                    $("#total_count").val(parseInt(total_count));
                    $("#div_joint_account_holders").html(html);
                    $.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
                    add_functions();
                    $('.numeric').numeric();
                    $('.numeric_share').numeric({decimal:"."});
                    is_foreign_address();
					add_date_functions($("#joint_account_count").val());
                }
            });
        }else{
		//	alert('else'+$("#div_joint_account_holders_hidden").html());
            $("#div_joint_account_holders").html($("#div_joint_account_holders_hidden").html());
            add_date_functions_for_all($("#joint_account_count").val());
            add_guardian_birth_date_for_all($("#joint_account_count").val());
			add_guardian_functions($("#joint_account_count").val());
            //add_guardian_functions($("#joint_account_count").val());
            add_functions();
        }
    });
	 if($("#is_adult").val()==1 || $("#end_is_adult").val()==0  ){
		$('#guardiant_main').html($('#guardiant_main_hidden').html());
		$.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
		$('.numeric').numeric();
        $('.numeric_share').numeric({decimal:"."});
		$('#show_guardian_birth_date').datepicker({
			dateFormat: 'dd/mm/yy',
			numberOfMonths: 1,
			//maxDate: "-18Y",
			yearRange: "-150:+0",
			altField: "#guardian_birth_date",
			altFormat: "yy-mm-dd",
			changeMonth: true,
			changeYear: true,
			onSelect: function (dateText, inst) {
				var startDate = new Date(dateText);
				var date = $(this).datepicker('getDate');
				var selectedYear = date.getFullYear();
				var today = new Date();
				var todayYear = today.getFullYear();
				var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
				if(diff > 100)
					alert('You have selected '+selectedYear+' as birth year');
				else if(diff>=18)
					$("#end_guardian_is_adult").prop("checked", true);
				else
					$("#end_guardian_is_adult").prop("checked", false);
			}
		});
		guardian_is_adult_onchange();
		is_foreign_address();
		copy_nominee_address();
		copy_nominee_guardian_address();
    }

	$('#end_is_adult').change(function()
	{
		$("#show_birth_date").val("");
		$("#birth_date").val("");
		if(this.value == 0){
			$("#is_adult").val('1');
			
			$('#guardiant_main').html($('#guardiant_main_hidden').html());
			$('#show_guardian_birth_date').datepicker({
					dateFormat: 'dd/mm/yy',
					numberOfMonths: 1,
					//maxDate: "-18Y",
					yearRange: "-150:+0",
					altField: "#guardian_birth_date",
					altFormat: "yy-mm-dd",
					changeMonth: true,
					changeYear: true,
					onSelect: function (dateText, inst) {
						$('#show_guardian_birth_date').removeClass( "error" );
						$('#show_guardian_birth_date').attr("style", "");
						$('#show_guardian_birth_date').parent().removeClass('has-error');
						$('.help-block').remove();
						$('#show_guardian_birth_date').addClass( "valid" );
						$('#show_guardian_birth_date').parent().addClass('has-success');

						var startDate = new Date(dateText);
						var date = $(this).datepicker('getDate');
						var selectedYear = date.getFullYear();
						var today = new Date();
						var todayYear = today.getFullYear();
						var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
						if(diff > 100)
							alert('You have selected '+selectedYear+' as birth year');
						else if(diff>=18)
							$("#end_guardian_is_adult").prop("checked", true);
						else
							$("#end_guardian_is_adult").prop("checked", false);
					}
				});
				copy_nominee_address();
				copy_nominee_guardian_address();
				guardian_is_adult_onchange();
		}
		else{
			$("#is_adult").val('0');
			$('#guardiant_main').html('');
		}
	});

    //toggle_other_state('state','state_other');
    $( ".sel_country" ).trigger( "change" );
    add_functions();
}

function add_functions()
{
	is_foreign_address();
	$('.numeric').numeric();
    $('.numeric_share').numeric({decimal:"."});

    /* Beneficiary */
    $(".a_beneficiary").unbind("click");
    $(".a_beneficiary").click(function(){
        var current_beneficiary = $(this).attr("rec_id");
        beneficiary_count++;
        actual_beneficiary_count++;
        var filtervars = {
            do_what:'add_beneficiary',
            beneficiary_count:beneficiary_count,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/fixed-deposit-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $(".a_beneficiary").remove();
                $("#div_beneficiary").append(html);
                add_functions();
            }
        });
    });

/* Beneficiary
	$(".a_beneficiarynew").unbind("click");
    $(".a_beneficiarynew").click(function(){
		var ben_id, share, other_info, ben_name;
		var ben_exist = false;
		var bcount = $("#tot_ben").val();
		var total_share = $('#total_share').val();
		ben_name = $("#sel_beneficiary option:selected").text();
		ben_id = $("#sel_beneficiary").val();
		share = $("#txt_beneficiary_share").val();
		other_info = $("#txt_other_info").val();
		total_share = parseInt(total_share) + parseInt(share);
		if(ben_id<=0){
			alert("Please select a Beneficiary.");
			return false;
		}
		else if(parseInt(share)<=0 || share=="" || share>100){
			alert("Please enter valid Share.");
			return false;
		}
		else if(total_share > 100)
		{
			alert("Beneficiary total Percentage Share exceeds 100%");
			return false;
		}
		else if(total_share == 100)
		{
			$('#beneficiaryAddBtnNew').hide();
		}
		$('.cls_ben').each(function( index ) {  
			if(ben_id == this.value){
				alert('Beneficiary already exists.');
				ben_exist = true;
			}
		}); 
		if(ben_exist == true)
		{
			$('#beneficiaryAddBtnNew').show();
			return false;
		}

		if(bcount == 1){
			$('#no_beneficiary').remove();
		}
		
		$("#tb_con_ben").append('<tr id="tr_con_ben_'+bcount+'"><td class="ben_new_account_numbering">'+bcount+'</td><td>'+ben_name+'<input type="hidden" class="w120px" name="ben_id[]" value="0"/><input type="hidden" class="cls_ben" name="beneficiary[]" value="'+ben_id+'"></td><td>'+share+'%<input type="hidden" name="beneficiary_share[]" value="'+share+'"></td><td>'+other_info+'<input type="hidden" name="other_info[]" value="'+other_info+'"></td><td><a href="javascript:void(0);" class="a_removebeneficiarynew" trid="tr_con_ben_'+bcount+'" trshare="'+share+'">Delete</a></td></tr>');
		
		$("#tot_ben").val(parseInt(bcount)+1);
		$('#total_share').val(total_share);
		add_functions();
		clear_add_beneficiary();
		change_jah_ben_numbering();
    });

	function clear_add_beneficiary()
	{
		$('#sel_beneficiary').val('');
		$('#txt_beneficiary_share').val('');
		$('#txt_other_info').val('');
		
	}

	  */
     $(".a_removebeneficiarynew").unbind("click");
    $(".a_removebeneficiarynew").click(function(){
		if( !confirm("Are you sure you want to delete this Beneficiary?") )
            return;
		else{
		var bcount		= $("#tot_ben").val();
		var trshare		= $(this).attr("trshare");
		var total_share = $('#total_share').val();
        var trid =  $(this).attr("trid");
		$("#"+$(this).attr("trid")).remove();
		total_share = parseInt(total_share) - parseInt(trshare);
		$('#total_share').val(total_share);
		$("#tot_ben").val(bcount-1);
		$('#beneficiaryAddBtnNew').show();
		
		alert('Beneficiary deleted successfully');
		change_jah_ben_numbering();
		}
    });
	
    /* add remove account holder */
    $(".a_add_account").unbind("click");
    $(".a_add_account").click(function(){
        account_count++;
        actual_account_count++;
        var filtervars = {
            do_what:'add_joint_account_holder',
            account_count:account_count,
            auth_token: $("#auth_token_name").val()
        };
        $("#joint_account_count").val(actual_account_count);
        $.ajax({
            type: "POST",
            url: "_ajax_files/fixed-deposit-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $(".a_add_account").remove();
                $("#div_joint_account_holders").append(html);
                $("#total_count").val(account_count);
                add_functions();
                $.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
                $('.numeric').numeric();
                $('.numeric_share').numeric({decimal:"."});
                is_foreign_address();
				add_date_functions(account_count);
				copy_address();
            }
        });
    });

    $(".a_removeaccount").unbind("click");
    $(".a_removeaccount").click(function(){
        actual_account_count--;
        if(actual_account_count==0){
            actual_account_count++;
            alert("You can not delete this account holder, Please add at-least joint account holder.");
            return;
        }

        $("#joint_account_count").val(actual_account_count);
        var remove_id = $(this).attr("remove_id");
        $("#account_"+remove_id).remove();
        var temp = $('#div_joint_account_holders .a_removeaccount').length;
        if(temp==0)
            temp=1;
        temp --;
        $('#span_account_'+$('.a_removeaccount').eq(temp).attr('remove_id')).html('<a href="javascript:void(0);" class="btn btn-primary btn-sm btn-add-large a_add_account">+ Add Joint Account Holder</a>');

        add_functions();
    });
    
    /* add remove account holder ends*/

    /* Email box */
    $(".a_jah_email").unbind("click");
    $(".a_jah_email").click(function(){
        var current_account = $(this).attr("rec_id");
        if(email_count[current_account]===undefined)
            email_count[current_account] = 2;
        else
            email_count[current_account]++;

        if(actual_email_count[current_account]===undefined)
            actual_email_count[current_account] = 2;
        else
            actual_email_count[current_account]++;

        if(actual_email_count[current_account]==4){
            actual_email_count[current_account] = 3;
            alert("You can not add more than 3 emails");
            return;
        }
        var filtervars = {
            do_what:'add_jah_email',
            jah_email_count:email_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/fixed-deposit-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#JahEmailAddWrap_"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removejahemail").unbind("click");
    $(".a_removejahemail").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_email_count[current_account]===undefined)
            actual_email_count[current_account] = 1;
        else
            actual_email_count[current_account]--;
        $(this).parent('div').remove();
    });
    /* Email box ends */
    
    /* add remove phone */
    $(".a_phone").unbind("click");
    $(".a_phone").click(function(){
        var current_account = $(this).attr("rec_id");
        if(phone_count[current_account]===undefined)
            phone_count[current_account] = 2;
        else
            phone_count[current_account]++;

        if(actual_phone_count[current_account]===undefined)
            actual_phone_count[current_account] = 2;
        else
            actual_phone_count[current_account]++;
        if(actual_phone_count[current_account]==6){
            actual_phone_count[current_account] = 5;
            alert("You can not add more than 5 phone numbers");
            return;
        }
        var filtervars = {
            do_what:'add_phone',
            phone_count:phone_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/fixed-deposit-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#TelephoneAddWrap_"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removephone").unbind("click");
    $(".a_removephone").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_phone_count[current_account]===undefined)
            actual_phone_count[current_account] = 1;
        else
            actual_phone_count[current_account]--;
        $(this).parent('div').remove();
    });
    /* add remove phone ends */

    /* Office Phone */
    $(".a_office_phone").unbind("click");
    $(".a_office_phone").click(function(){
        var current_account = $(this).attr("rec_id");
        if(office_phone_count[current_account]===undefined)
            office_phone_count[current_account] = 2;
        else
            office_phone_count[current_account]++;

        if(actual_office_phone_count[current_account]===undefined)
            actual_office_phone_count[current_account] = 2;
        else
            actual_office_phone_count[current_account]++;

        if(actual_office_phone_count[current_account]==6){
            actual_office_phone_count[current_account] = 5;
            alert("You can not add more than 5 office numbers");
            return;
        }
        var filtervars = {
            do_what:'add_office_phone',
            phone_count:office_phone_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/fixed-deposit-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#InputsWrapperOffice_"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removeofficephone").unbind("click");
    $(".a_removeofficephone").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_office_phone_count[current_account]===undefined)
            actual_office_phone_count[current_account] = 1;
        else
            actual_office_phone_count[current_account]--;
        $(this).parent('div').remove();
        //$("#"+remove_id).remove();
    });
    /* Office Phone ends */

    /* Fax */
    $(".a_fax").unbind("click");
    $(".a_fax").click(function(){
        var current_account = $(this).attr("rec_id");
        if(fax_count[current_account]===undefined)
            fax_count[current_account] = 2;
        else
            fax_count[current_account]++;

        if(actual_fax_count[current_account]===undefined)
            actual_fax_count[current_account] = 2;
        else
            actual_fax_count[current_account]++;

        if(actual_fax_count[current_account]==6){
            actual_fax_count[current_account] = 5;
            alert("You can not add more than 5 fax numbers");
            return;
        }
        var filtervars = {
            do_what:'add_fax',
            phone_count:fax_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/fixed-deposit-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#MobileAddWrap"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removefax").unbind("click");
    $(".a_removefax").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_fax_count[current_account]===undefined)
            actual_fax_count[current_account] = 1;
        else
            actual_fax_count[current_account]--;
        $(this).parent('div').remove();
        //$("#"+remove_id).remove();
    });
    /* Fax ends */

    /* Mobile box */
    $(".a_mobile").unbind("click");
    $(".a_mobile").click(function(){
        var current_account = $(this).attr("rec_id");
        if(mobile_count[current_account]===undefined)
            mobile_count[current_account] = 2;
        else
            mobile_count[current_account]++;

        if(actual_mobile_count[current_account]===undefined)
            actual_mobile_count[current_account] = 2;
        else
            actual_mobile_count[current_account]++;

        if(actual_mobile_count[current_account]==4){
            actual_mobile_count[current_account] = 3;
            alert("You can not add more than 3 mobile numbers");
            return;
        }
        var filtervars = {
            do_what:'add_mobile',
            phone_count:mobile_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/fixed-deposit-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#MobileAddWrap_"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removemobile").unbind("click");
    $(".a_removemobile").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_mobile_count[current_account]===undefined)
            actual_mobile_count[current_account] = 1;
        else
            actual_mobile_count[current_account]--;
        $(this).parent('div').remove();
        //$("#"+remove_id).remove();
    });
    /* Mobile box ends */
 
    
    /* Beneficiary */
    $(".a_removebeneficiary").unbind("click");
    $(".a_removebeneficiary").click(function(){
        //alert(actual_beneficiary_count);
        if(actual_beneficiary_count==1){
            //actual_beneficiary_count++;
            alert("You can not delete this account holder, Please add at-least joint account holder.");
            return;
        }
        var remove_id = $(this).attr("remove_id");
        actual_beneficiary_count--;
        $("#div_beneficiary_"+remove_id).remove();

        $("#account_"+remove_id).remove();
        var temp = $('.a_removebeneficiary').length;
        temp --;
        
        $('#span_beneficiary_'+$('.a_removebeneficiary').eq(temp).attr('remove_id')).html('<a class="btn btn-primary btn-sm btn-add-large a_beneficiary" id="beneficiaryAddBtn" title="Add beneficiary" alt="Add beneficiary" href="javascript:void(0);">+ Add Beneficiary</a>');
        add_functions();
    });
    /* Beneficiary */
    $(".jah_is_adult").change(function(){
        var account_id = $(this).attr("ac_id");
        $('#show_ejd_birth_date_'+account_id).val("");
        $('#ejd_birth_date_'+account_id).val("");
        if($(this).val()=="1"){
            $("#ejd_guardian_"+$(this).attr("ac_id")).html("");
        }
        else{
            var ac_id = $(this).attr("ac_id");
            if($('input[name=guardian_id_'+ac_id+']').val() == 0 || $('input[name=guardian_id_'+ac_id+']').val() == undefined ){
                add_guardian($(this).attr("ac_id"));
            }else{
                $('#div_joint_account_holders #ejd_guardian_'+ac_id).html($('#div_joint_account_holders_hidden  #ejd_guardian_'+ac_id).html());
                add_guardian_functions(account_id);
            }
        }
    });
	copy_address();
	copy_nominee_address();
	copy_nominee_guardian_address();
    change_jah_ben_numbering();
}

function add_nominee_guardian(){
    var GuardianMaxInputs1 = 5;
    var GuardianTelephoneAddWrap   = $("#TelephoneAddWrap2");
    var GuardianAddButton = $("#TelephoneAddBtn2");
    var guardian_num_phone_numbers = $('#guardian_num_phone_numbers').val();
    var x = GuardianTelephoneAddWrap.length;
    var GuardianTelephoneFieldCount=1;

    $(GuardianAddButton).click(function (e)
    {
        if(guardian_num_phone_numbers < GuardianMaxInputs1)
        {
           GuardianTelephoneFieldCount++;
           $(GuardianTelephoneAddWrap).append('<div class="row"><div class="divider"></div><div class="col-xs-plus"><span class="plus_sign">+</span></div><div class="col-lg-2"><input type="text" class="form-control numeric" name="end_guardian_phone_isd[]" value="91" placeholder="ISD" maxlength="3" title="Please enter guardian Telephone ISD" alt="Please enter guardian Telephone ISD" /></div><div class="col-lg-3"><input type="text" class="form-control numeric" name="end_guardian_phone_std[]" placeholder="STD" maxlength="5" title="Please enter guardian Telephone STD" alt="Please enter guardian Telephone STD" /></div><div class="col-lg-5 last"><input type="text" class="form-control numeric"  name="end_guardian_phone_number[]"  placeholder="Phone Number" title="Please enter guardian Telephone number" alt="Please enter guardian Telephone number" maxlength="10"/></div><a href="#" class="btn-delete TelephoneRemove2" title="Please click to remove the entered Telephone number" alt="Please click to remove the entered Telephone number" >&ndash;</a></div></div>');
           guardian_num_phone_numbers++; 
           $('#guardian_num_phone_numbers').val(guardian_num_phone_numbers);
        }
        else if(guardian_num_phone_numbers == GuardianMaxInputs1)
                alert('You can not add more than '+GuardianMaxInputs1+' telephone phone numbers');

        $('.numeric').numeric();
        $('.numeric_share').numeric({decimal:"."});
        return false;
    });
    $("body").on("click",".TelephoneRemove2", function(e)
    { 
        if( guardian_num_phone_numbers > 1 ) {
            $(this).parent('div').remove();
            guardian_num_phone_numbers--;
        }
        return false;
    });

    
    var GuardianMaxInputs3       = 3; //maximum input boxes allowed
    var GuardianMobileAddWrap   = $("#MobileAddWrap2"); //Input boxes wrapper ID
    var GuardianMobileAddButton       = $("#MobileAddBtn2"); //Add button ID
    var x = GuardianMobileAddWrap.length; //initlal text box count
    var guardian_num_mobile_numbers = $('#guardian_num_mobile_numbers').val();
    var GuardianMobileFieldCount=1; //to keep track of text box added
    $(GuardianMobileAddButton).click(function (e)  //on add input button click
    {
            if(guardian_num_mobile_numbers < GuardianMaxInputs3) //max input box allowed
            {
                GuardianMobileFieldCount++; //text box added increment
                //add input box
                $(GuardianMobileAddWrap).append('<div class="row"><div class="divider"></div><div class="col-xs-plus"><span class="plus_sign">+</span></div><div class="col-lg-2"><input type="text" class="form-control numeric" name="end_guardian_mobile_isd[]" placeholder="ISD" value="91" title="Please enter guardian Mobile ISD" alt="Please enter guardian Mobile ISD" maxlength="3"/></div><div class="col-lg-8 last"><input type="text" class="form-control numeric" name="end_guardian_mobile_number[]" placeholder="Mobile Number" title="Please enter guardian Mobile number" alt="Please enter guardian Mobile number" maxlength="10"/></div><a href="#" class="btn-delete MobileRemove2" title="Please click to remove the entered Mobile number" alt="Please click to remove the entered Mobile number" >&ndash;</a></div>');
                
                guardian_num_mobile_numbers++; //text box increment
                $('#guardian_num_mobile_numbers').val(guardian_num_mobile_numbers);
            }
            else if(guardian_num_mobile_numbers == GuardianMaxInputs3)
                alert('You can not add more than '+GuardianMaxInputs3+' mobile numbers');

        $('.numeric').numeric();
        $('.numeric_share').numeric({decimal:"."});
    return false;
    });
    $("body").on("click",".MobileRemove2", function(e){ //user click on remove text
            if( guardian_num_mobile_numbers > 1 ) {
                    $(this).parent('div').remove(); //remove text box
                    guardian_num_mobile_numbers--; //decrement textbox
            }
    return false;
    });


    var GuradianMaxInputs5 = 3;
    var GuradianEmailAddWrap    = $("#EmailAddWrap2");
    var GuradianEmailAddButton  = $("#EmailAddBtn2");
    var guardian_num_emails = $('#guardian_num_emails').val();
    var x = GuradianEmailAddWrap.length;
    var GuradianFieldCount=1;
    
    $(GuradianEmailAddButton).click(function (e)
    {
        if(guardian_num_emails < GuradianMaxInputs5){
            GuradianFieldCount++;
            $(GuradianEmailAddWrap).append('<div class="row"><div class="divider"></div><div class="col-lg-11 last"><input type="text" class="form-control" name="end_guardian_email[]" placeholder="Email" maxlength="254"  title="Please enter Nominee guardian Email" alt="Please enter Nominee guardian Email"/></div><a href="#" class="btn-delete EmailRemove2" title="Please click to remove the entered Email Ids" alt="Please click to remove the entered Email Ids">&ndash;</a></div>');
            guardian_num_emails++; 
            $('#guardian_num_emails').val(guardian_num_emails);
        }
        else if(guardian_num_emails == GuradianMaxInputs5)
            alert('You can not add more than '+GuradianMaxInputs5+' Email ids');
        return false;
    });
    
    $("body").on("click",".EmailRemove2", function(e)
    {
        if( guardian_num_emails > 1 ) 
        {
            $(this).parent('div').remove();
            guardian_num_emails--;
        }
        return false;
    });
}

function add_date_functions(account_id)
{
	$('#show_ejd_birth_date_'+account_id).datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		minDate: "-150Y",
		//maxDate: new Date(),
		yearRange: "-150:+0",
		altField: "#ejd_birth_date_"+account_id,
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect: function (dateText, inst) {
			var startDate = new Date(dateText);
			var date = $(this).datepicker('getDate');
			var selectedYear = startDate.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
			if(diff > 100)
				alert('You have selected '+selectedYear+' as birth year');
			else if(diff < 18){
				//add_guardian(account_id);
				//$('#ejd_is_adult_'+account_id).val("0");
                var ac_id = account_count;
                if($('input[name=guardian_id_'+ac_id+']').val() == 0 || $('input[name=guardian_id_'+ac_id+']').val() == undefined ){
                    add_guardian(account_count);
                    $('#ejd_is_adult_'+account_count).val("0");
                }else{
                    $('#ejd_is_adult_'+account_count).val("0");
                    $('#div_joint_account_holders #account_'+ac_id+' #jointholderAddWrap #ejd_guardian_'+ac_id).html($('#div_joint_account_holders_hidden #account_'+ac_id+' #jointholderAddWrap #ejd_guardian_'+ac_id).html());
                    add_guardian_functions(account_id);
                }
			}
			else{
				$('#ejd_is_adult_'+account_id).val("1");
                //$('#div_joint_account_holders_hidden #account_'+account_id+' #jointholderAddWrap #ejd_guardian_'+account_id).html($('#div_joint_account_holders #account_'+account_id+' #jointholderAddWrap #ejd_guardian_'+account_id).html());
				$('#ejd_guardian_'+account_id).html('');
			}
		}	
	});	
}

function add_date_functions_for_all(account_id){
    for(var i = 1; i <= account_id; i++){
        $('#show_ejd_birth_date_'+i).removeClass('hasDatepicker');
        add_date_functions(i);
    }
}

function add_guardian(account_id)
{
	var filtervars = {
		do_what:'add_guardian',
		account_id: account_id,
        auth_token: $("#auth_token_name").val()
	};
	$.ajax({
		type: "POST",
		url: "_ajax_files/fixed-deposit-details_ajax.php",
		data: filtervars,
		dataType: "html",
		success: function(html){
			$('#ejd_guardian_'+account_id).html(html);
			$.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
			$('.numeric').numeric();
            $('.numeric_share').numeric({decimal:"."});
            add_guardian_birth_date(account_id);
            add_guardian_functions(account_id);
		}
	});
}

function add_guardian_birth_date_for_all(account_id){
    for(var i=1;i<=account_id;i++)
    {
        $('#show_guardian_birth_date_'+i).removeClass('hasDatepicker');
        $('#show_guardian_birth_date_'+i).datepicker({
            dateFormat: 'dd/mm/yy',
            numberOfMonths: 1,
            //maxDate: "-18Y",
            yearRange: "-150:+0",
            altField: "#guardian_birth_date_"+i,
            altFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            onSelect: function (dateText, inst) {
                var date = $('#show_guardian_birth_date_'+account_id).datepicker('getDate');
                var today = new Date();
                var todayYear = today.getFullYear();
                var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));

                if(diff > 100)
                    alert('You have selected '+selectedYear+' as birth year');

                if(diff>=18)
                    $("#guardian_is_adult_"+account_id).prop("checked", true);
                else
                    $("#guardian_is_adult_"+account_id).prop("checked", false);
            }
        });
        //add_guardian_birth_date(i);
    }
}

function add_guardian_birth_date(account_id){
    $('#show_guardian_birth_date_'+account_id).datepicker({
        dateFormat: 'dd/mm/yy',
        numberOfMonths: 1,
        //maxDate: "-18Y",
        yearRange: "-150:+0",
        altField: "#guardian_birth_date_"+account_id,
        altFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        onSelect: function (dateText, inst) {
            var date = $('#show_guardian_birth_date_'+account_id).datepicker('getDate');
            var today = new Date();
            var todayYear = today.getFullYear();
            var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));

            if(diff > 100)
                alert('You have selected '+selectedYear+' as birth year');

            if(diff>=18)
                $("#guardian_is_adult_"+account_id).prop("checked", true);
            else
                $("#guardian_is_adult_"+account_id).prop("checked", false);
        }
    });
}

function add_guardian_functions(account_id)
{
	is_foreign_address();
	/*$('#show_guardian_birth_date_'+account_id).datepicker("destroy");
	$('#show_guardian_birth_date_'+account_id).datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		maxDate: "-18Y",
		yearRange: "-150:+0",
		altField: "#guardian_birth_date_"+account_id,
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect: function (dateText, inst) {
			var startDate = new Date(dateText);
			var date = $(this).datepicker('getDate');
			var selectedYear = startDate.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
			if(diff > 100)
				alert('You have selected '+selectedYear+' as birth year');
		}
	});*/

	

	$(".grdnCorrespondenceAdd").click(function(){
		var acc_id = $(this).attr("account_id");
		if($(this).is(":checked")){

			//----------------------------------------------- changes 8-8-14 --------------------------------------------- //
			$('#guardian_correspondence_is_same_as_permanent_'+acc_id).prop('checked', $('#guardian_permanent_is_same_as_permanent_'+acc_id).prop('checked'));
			$('#guardian_correspondence_is_same_as_correspondence_'+acc_id).prop('checked', $('#guardian_permanent_is_same_as_correspondence_'+acc_id).prop('checked'));
			$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+acc_id).val($('#guardian_permanent_is_same_as_permanent_or_correspondence_'+acc_id).val());
			
			//----------------------------------------------- End changes 8-8-14 --------------------------------------------- //

			//----------------------------------------------- changes 10-8-14 --------------------------------------------- //
			$('#guardian_correspondence_address_line1_'+acc_id).val($('#guardian_permanent_address_line1_'+acc_id).val());
			$('#guardian_correspondence_address_line2_'+acc_id).val($('#guardian_permanent_address_line2_'+acc_id).val());
			$('#guardian_correspondence_address_line3_'+acc_id).val($('#guardian_permanent_address_line3_'+acc_id).val());
			$('#guardian_correspondence_city_village_town_'+acc_id).val($('#guardian_permanent_city_village_town_'+acc_id).val());
			$('#guardian_correspondence_country_'+acc_id).val($('#guardian_permanent_country_'+acc_id).val());
			$('#guardian_correspondence_state_'+acc_id).val($('#guardian_permanent_state_'+acc_id).val());

			$('#guardian_correspondence_address_line1_'+acc_id).prop('readonly', true);
			$('#guardian_correspondence_address_line2_'+acc_id).prop('readonly', true);
			$('#guardian_correspondence_address_line3_'+acc_id).prop('readonly', true);
			$('#guardian_correspondence_city_village_town_'+acc_id).prop('readonly', true);
			$('#guardian_correspondence_country_'+acc_id).prop('disabled', true);
			$('#guardian_correspondence_state_'+acc_id).prop('disabled', true);
			$('#guardian_correspondence_zipcode_'+acc_id).prop('readonly', true);
			$('#guardian_correspondence_state_other_'+acc_id).prop('readonly', true);

			$('#guardian_correspondence_is_same_as_permanent_'+acc_id).prop('disabled', true);
			$('#guardian_correspondence_is_same_as_correspondence_'+acc_id).prop('disabled', true);
			
			var checked;
			if($('#guardian_permanent_is_foreign_address_'+acc_id).prop('checked'))
				checked = 1;
			else
				checked = 0;

			populate_country_disable($('#guardian_permanent_country_'+acc_id).val(), "guardian_correspondence_country_"+acc_id, "span_guardian_correspondence_country_"+acc_id, checked, "");
			$('#guardian_correspondence_state_'+acc_id).val($('#guardian_permanent_state_'+acc_id).val());
			populate_states_disable($('#guardian_permanent_country_'+acc_id).val(), "guardian_correspondence_state_"+acc_id, "span_guardian_correspondence_state_"+acc_id,$('#guardian_permanent_state_'+acc_id).val(), "", "guardian_correspondence_state_"+acc_id);

		
			if($('#guardian_permanent_span_other_state_'+acc_id).is(':visible')){
                $('#guardian_correspondence_span_other_state_'+acc_id).show();
            }else{
                $('#guardian_correspondence_span_other_state_'+acc_id).hide();
            }

			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('checked',$('#guardian_permanent_is_foreign_address_'+acc_id).prop('checked'));
			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('disabled', true);
			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('readonly', true);
			
			$('#guardian_correspondence_is_foreign_address_'+acc_id).val($('#guardian_permanent_is_foreign_address_'+acc_id).val());
			$('#guardian_correspondence_zipcode_'+acc_id).val($('#guardian_permanent_zipcode_'+acc_id).val());
			$('#guardian_correspondence_state_other_'+acc_id).val($('#guardian_permanent_state_other_'+acc_id).val());
			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('disabled', true);
			if(checked == 1){
				$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation', 'length');
				$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation-length', '5-25');
				$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation-error-msg', 'Please enter valid Zipcode for joint account holder address');
			}
			else{
				$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation', 'length');
				$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation-length', '6-6');
				$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation-error-msg', 'Please enter valid Pincode for joint account holder address');
			}
			//is_foreign_address();
			//----------------------------------------------- changes 10-8-14 --------------------------------------------- //

		}else{
			
			//----------------------------------------------- changes 8-8-14 --------------------------------------------- //
			$('#guardian_correspondence_is_same_as_permanent_'+acc_id).prop('checked', false);
			$('#guardian_correspondence_is_same_as_correspondence_'+acc_id).prop('checked', false);
			$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+acc_id).val('');
			
			//----------------------------------------------- End changes 8-8-14 --------------------------------------------- //


			//----------------------------------------------- changes 10-8-14 --------------------------------------------- //
			populate_country(102, "guardian_correspondence_country_"+acc_id, "span_guardian_correspondence_country_"+acc_id, 0, "");
			populate_states(102, "guardian_correspondence_state_"+acc_id, "span_guardian_correspondence_state_"+acc_id, "", "");
			$("#guardian_correspondence_state_"+acc_id).val('');
			$("#guardian_correspondence_state_"+acc_id).prop('selected', '');
			$('#guardian_correspondence_state_other_'+acc_id).val('');
			$('#guardian_correspondence_span_other_state_'+acc_id).attr('style', 'display:none;');
			$('#guardian_correspondence_zipcode_'+acc_id).prop('maxlength', '6');
			$('#guardian_correspondence_zipcode_'+acc_id).addClass('numeric');
			$('.numeric').numeric();

			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('disabled', false);
			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('checked', false);
			$('#guardian_correspondence_address_line1_'+acc_id).val('');
			$('#guardian_correspondence_address_line1_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_address_line2_'+acc_id).val('');
			$('#guardian_correspondence_address_line2_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_address_line3_'+acc_id).val('');
			$('#guardian_correspondence_address_line3_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_city_village_town_'+acc_id).val('');
			$('#guardian_correspondence_city_village_town_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_zipcode_'+acc_id).val('');

			$('#guardian_correspondence_country_'+acc_id).prop('disbaled', false);
			$('#guardian_correspondence_state_'+acc_id).prop('disbaled', false);

			$('#guardian_correspondence_is_same_as_permanent_'+acc_id).prop('disabled', false);
			$('#guardian_correspondence_is_same_as_correspondence_'+acc_id).prop('disabled', false);
			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_zipcode_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_state_other_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation', '');
			$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation-length', '');
			$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation-error-msg', '');

            $('#guardian_correspondence_is_same_as_permanent_'+acc_id).prop('disabled', $('#guardian_permanent_is_same_as_permanent_'+acc_id).prop('disabled'));
            $('#guardian_correspondence_is_same_as_correspondence_'+acc_id).prop('disabled', $('#guardian_permanent_is_same_as_correspondence_'+acc_id).prop('disabled'));
			is_foreign_address();
			//----------------------------------------------- End changes 10-8-14 --------------------------------------------- //
		}
	});
	
	copy_guardian_address();
	copy_guardian_correspondence_address();
	copy_nominee_address();
	copy_nominee_guardian_address();
}

function check_minor_guardian(account_id)
{

    var startDate = new Date($("#show_guardian_birth_date_"+account_id).val());
    if(startDate!=""){
        var date = $('#show_guardian_birth_date_'+account_id).datepicker('getDate');
        var selectedYear = startDate.getFullYear();
        var today = new Date();
        var todayYear = today.getFullYear();
        var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
        if((diff < 18 && $('#guardian_is_adult_'+account_id).is(':checked')) || (diff > 18 && !$('#guardian_is_adult_'+account_id).is(':checked'))){

            $("#show_guardian_birth_date_"+account_id).val("");
            $("#guardian_birth_date_"+account_id).val("");
        }
    }
}

function copy_address()
{
	$('.same_per_cor_address').unbind("click");
	$('.same_per_cor_address').click(function(){
		var click_account_id = $(this).attr("account_count");
		
		var for_address = $(this).attr("for_address");
		if($(this).val()=="P"){
			if($('#is_same_as_permanent_'+click_account_id).is(":checked")){
				$('#is_same_as_correspondence_'+click_account_id).prop('checked', false);
				//$('#is_same_as_permanent_'+click_account_id).prop('disabled', false);
				$('#is_same_as_permanent_or_correspondence_'+click_account_id).val('P');
				var filtervars = {
					do_what:'populate_address',
					address_value:"P",
					address_for:"jointaccountholder",
					account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#populate_'+for_address+'_address_'+click_account_id).html(html);
                        $.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
						add_functions();
					}
				});
			}
			else {
				
				//$('#is_same_as_correspondence_'+click_account_id).prop('disabled', false);		
				$('#is_same_as_permanent_'+click_account_id).prop('checked', false);
				$('#is_same_as_permanent_or_correspondence_'+click_account_id).val('');
				
				
				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#ejd_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#ejd_is_foreign_address_'+click_account_id).prop('checked', false);
				$('#ejd_address_line1_'+click_account_id).val('');
				$('#ejd_address_line1_'+click_account_id).prop('readonly', false);
				$('#ejd_address_line2_'+click_account_id).val('');
				$('#ejd_address_line2_'+click_account_id).prop('readonly', false);
				$('#ejd_address_line3_'+click_account_id).val('');
				$('#ejd_address_line3_'+click_account_id).prop('readonly', false);
				$('#ejd_city_village_town_'+click_account_id).val('');
				$('#ejd_city_village_town_'+click_account_id).prop('readonly', false);
				$('#ejd_zipcode_'+click_account_id).val('');
				$('#ejd_zipcode_'+click_account_id).prop('readonly', false);

				$('#ejd_country_'+click_account_id).prop('disbaled', false);
				
				populate_country(102, "ejd_country_"+click_account_id, "span_ejd_country_"+click_account_id, 0, "");
				populate_states(102, "ejd_state_"+click_account_id, "span_ejd_state_"+click_account_id, "", "");
				$("#ejd_state_"+click_account_id).val('');
				$("#ejd_state_"+click_account_id).prop('selected', '');
				$('#ejd_state_other_'+click_account_id).val('');
				$('#ejd_state_other_'+click_account_id).prop('readonly', false);
				$('#span_ejd_state_other_'+click_account_id).attr('style', 'display:none;');
				$('#ejd_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#ejd_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- //
				
			}
		}
		else if($(this).val()=="C")
		{
			if($('#is_same_as_correspondence_'+click_account_id).is(":checked")){
				$('#is_same_as_permanent_'+click_account_id).prop('checked', false);
				//$('#is_same_as_correspondence_'+click_account_id).prop('disabled', false);
				$('#is_same_as_permanent_or_correspondence_'+click_account_id).val('C');
				var filtervars = {
					do_what:'populate_address',
					address_value:"C",
					address_for:"jointaccountholder",
					account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#populate_'+for_address+'_address_'+click_account_id).html(html);
                        $.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
						add_functions();
					}
				});
			}
			else {
				//$('#is_same_as_permanent_'+click_account_id).prop('disabled', false);		
				$('#is_same_as_correspondence_'+click_account_id).prop('checked', false);
				$('#is_same_as_permanent_or_correspondence_'+click_account_id).val('');

				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#ejd_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#ejd_is_foreign_address_'+click_account_id).prop('checked', false);
				$('#ejd_address_line1_'+click_account_id).val('');
				$('#ejd_address_line1_'+click_account_id).prop('readonly', false);
				$('#ejd_address_line2_'+click_account_id).val('');
				$('#ejd_address_line2_'+click_account_id).prop('readonly', false);
				$('#ejd_address_line3_'+click_account_id).val('');
				$('#ejd_address_line3_'+click_account_id).prop('readonly', false);
				$('#ejd_city_village_town_'+click_account_id).val('');
				$('#ejd_city_village_town_'+click_account_id).prop('readonly', false);
				$('#ejd_zipcode_'+click_account_id).val('');
				$('#ejd_zipcode_'+click_account_id).prop('readonly', false);

				$('#ejd_country_'+click_account_id).prop('disbaled', false);
				
				populate_country(102, "ejd_country_"+click_account_id, "span_ejd_country_"+click_account_id, 0, "");
				populate_states(102, "ejd_state_"+click_account_id, "span_ejd_state_"+click_account_id, "", "");
				$("#ejd_state_"+click_account_id).val('');
				$("#ejd_state_"+click_account_id).prop('selected', '');
				$('#ejd_state_other_'+click_account_id).val('');
				$('#span_ejd_state_other_'+click_account_id).attr('style', 'display:none;');
				$('#ejd_state_other_'+click_account_id).prop('readonly', false);
				$('#ejd_state_other_'+click_account_id).prop('disabled', false);
				$('#ejd_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#ejd_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- //
			}
		}
	});	
}


function copy_guardian_address()
{
	$('.guardian_same_per_address').click(function(){
		
		var click_account_id = $(this).attr("account_count");
		var for_address = $(this).attr("for_address");
		

		if($(this).val()=="P"){
			clear_is_same_as_guardian(click_account_id);
			if($('#guardian_permanent_is_same_as_permanent_'+click_account_id).is(":checked")){
				$('#guardian_permanent_is_same_as_correspondence_'+click_account_id).prop('checked', false);
				//$('#guardian_permanent_is_same_as_permanent_'+click_account_id).prop('disabled', false);
				$('#guardian_permanent_is_same_as_permanent_or_correspondence_'+click_account_id).val('P');
				var filtervars = {
					do_what:'populate_address',
					address_value:"P",
					address_for:"guardianpermanent",
					account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#guardian_populate_'+for_address+'_address_'+click_account_id).html(html);
						add_functions();
					}
				});
			}
			else {
				clear_is_same_as_guardian(click_account_id);
				//$('#guardian_permanent_is_same_as_correspondence_'+click_account_id).prop('disabled', false);		
				$('#guardian_permanent_is_same_as_permanent_'+click_account_id).prop('checked', false);
				$('#guardian_permanent_is_same_as_permanent_or_correspondence_'+click_account_id).val('');

				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#guardian_permanent_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#guardian_permanent_is_foreign_address_'+click_account_id).prop('checked', false);
				$('#guardian_permanent_address_line1_'+click_account_id).val('');
				$('#guardian_permanent_address_line2_'+click_account_id).val('');
				$('#guardian_permanent_address_line3_'+click_account_id).val('');
				$('#guardian_permanent_city_village_town_'+click_account_id).val('');
				$('#guardian_permanent_zipcode_'+click_account_id).val('');

				$('#guardian_permanent_address_line1_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_address_line2_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_address_line3_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_city_village_town_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_zipcode_'+click_account_id).prop('readonly', false);

				$('#guardian_permanent_country_'+click_account_id).prop('disbaled', false);
				
				populate_country(102, "guardian_permanent_country_"+click_account_id, "span_guardian_permanent_country_"+click_account_id, 0, "");
				populate_states(102, "guardian_permanent_state_"+click_account_id, "span_guardian_permanent_state_"+click_account_id, "", "");
				$("#guardian_permanent_state_"+click_account_id).val('');
				$("#guardian_permanent_state_"+click_account_id).prop('selected', '');
				$('#guardian_permanent_state_other_'+click_account_id).val('');
				$('#guardian_permanent_state_other_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_span_other_state_'+click_account_id).attr('style', 'display:none;');
				$('#guardian_permanent_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#guardian_permanent_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- // 
			}
		}
		else if($(this).val()=="C")
		{
			clear_is_same_as_guardian(click_account_id);
			if($('#guardian_permanent_is_same_as_correspondence_'+click_account_id).is(":checked")){
				$('#guardian_permanent_is_same_as_permanent_'+click_account_id).prop('checked', false);
				//$('#guardian_permanent_is_same_as_correspondence_'+click_account_id).prop('disabled', false);
				$('#guardian_permanent_is_same_as_permanent_or_correspondence_'+click_account_id).val('C');
				var filtervars = {
					do_what:'populate_address',
					address_value:"C",
					address_for:"guardianpermanent",
					account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#guardian_populate_'+for_address+'_address_'+click_account_id).html(html);
                        $.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
						add_functions();
					}
				});
			}
			else {
				clear_is_same_as_guardian(click_account_id);
				//$('#guardian_permanent_is_same_as_permanent_'+click_account_id).prop('disabled', false);		
				$('#guardian_permanent_is_same_as_correspondence_'+click_account_id).prop('checked', false);
				$('#guardian_permanent_is_same_as_permanent_or_correspondence_'+click_account_id).val('');

				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#guardian_permanent_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#guardian_permanent_is_foreign_address_'+click_account_id).prop('checked', false);
				
				$('#guardian_permanent_address_line1_'+click_account_id).val('');
				$('#guardian_permanent_address_line2_'+click_account_id).val('');
				$('#guardian_permanent_address_line3_'+click_account_id).val('');
				$('#guardian_permanent_city_village_town_'+click_account_id).val('');
				$('#guardian_permanent_zipcode_'+click_account_id).val('');

				$('#guardian_permanent_address_line1_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_address_line2_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_address_line3_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_city_village_town_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_zipcode_'+click_account_id).prop('readonly', false);

				$('#guardian_permanent_country_'+click_account_id).prop('disbaled', false);
				
				populate_country(102, "guardian_permanent_country_"+click_account_id, "span_guardian_permanent_country_"+click_account_id, 0, "");
				populate_states(102, "guardian_permanent_state_"+click_account_id, "span_guardian_permanent_state_"+click_account_id, "", "");
				$("#guardian_permanent_state_"+click_account_id).val('');
				$("#guardian_permanent_state_"+click_account_id).prop('selected', '');
				$('#guardian_permanent_state_other_'+click_account_id).val('');
				$('#guardian_permanent_state_other_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_span_other_state_'+click_account_id).attr('style', 'display:none;');
				$('#guardian_permanent_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#guardian_permanent_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- // 
			}
		}
	});	

}


function copy_guardian_correspondence_address()
{
	$('.guardian_same_cor_address').click(function(){
		var click_account_id = $(this).attr("account_count");
		var for_address = $(this).attr("for_address");
		if($(this).val()=="P"){
			if($('#guardian_correspondence_is_same_as_permanent_'+click_account_id).is(":checked")){
				$('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('checked', false);
				//$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('disabled', false);
				$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+click_account_id).val('P');
				var filtervars = {
					do_what:'populate_address',
					address_value:"P",
					address_for:"guardiancorrespondence",
					account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#guardian_populate_'+for_address+'_address_'+click_account_id).html(html);
                        $.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
						add_functions();
					}
				});
			}
			else {
				//$('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('disabled', false);		
				$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('checked', false);
				//$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('disabled', false);
				$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+click_account_id).val('');

				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#guardian_correspondence_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#guardian_correspondence_is_foreign_address_'+click_account_id).prop('checked', false);
				
				$('#guardian_correspondence_address_line1_'+click_account_id).val('');
				$('#guardian_correspondence_address_line1_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_address_line2_'+click_account_id).val('');
				$('#guardian_correspondence_address_line2_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_address_line3_'+click_account_id).val('');
				$('#guardian_correspondence_address_line3_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_city_village_town_'+click_account_id).val('');
				$('#guardian_correspondence_city_village_town_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_zipcode_'+click_account_id).val('');
				$('#guardian_correspondence_zipcode_'+click_account_id).prop('readonly', false);
				
				$('#guardian_correspondence_country_'+click_account_id).prop('disbaled', false);
				populate_country(102, "guardian_correspondence_country_"+click_account_id, "span_guardian_correspondence_country_"+click_account_id, 0, "");
				populate_states(102, "guardian_correspondence_state_"+click_account_id, "span_guardian_correspondence_state_"+click_account_id, "", "");
				$("#guardian_correspondence_state_"+click_account_id).val('');
				$("#guardian_correspondence_state_"+click_account_id).prop('selected', '');
				$('#guardian_correspondence_state_other_'+click_account_id).val('');
				$('#guardian_correspondence_state_other_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_span_other_state_'+click_account_id).attr('style', 'display:none;');
				$('#guardian_correspondence_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#guardian_correspondence_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- // 
			}
		}
		else if($(this).val()=="C")
		{
			if($('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).is(":checked")){
				$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('checked', false);
				//$('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('disabled', false);
				$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+click_account_id).val('C');
				var filtervars = {
					do_what:'populate_address',
					address_value:"C",
					address_for:"guardiancorrespondence",
					account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#guardian_populate_'+for_address+'_address_'+click_account_id).html(html);
                        $.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
						add_functions();
					}
				});
			}
			else {
				//$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('disabled', false);		
				$('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('checked', false);
				//$('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('disabled', false);
				$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+click_account_id).val('');

				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#guardian_correspondence_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#guardian_correspondence_is_foreign_address_'+click_account_id).prop('checked', false);
				
				$('#guardian_correspondence_address_line1_'+click_account_id).val('');
				$('#guardian_correspondence_address_line1_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_address_line2_'+click_account_id).val('');
				$('#guardian_correspondence_address_line2_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_address_line3_'+click_account_id).val('');
				$('#guardian_correspondence_address_line3_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_city_village_town_'+click_account_id).val('');
				$('#guardian_correspondence_city_village_town_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_zipcode_'+click_account_id).val('');
				$('#guardian_correspondence_zipcode_'+click_account_id).prop('readonly', false);
				
				$('#guardian_correspondence_country_'+click_account_id).prop('disbaled', false);
				populate_country(102, "guardian_correspondence_country_"+click_account_id, "span_guardian_correspondence_country_"+click_account_id, 0, "");
				populate_states(102, "guardian_correspondence_state_"+click_account_id, "span_guardian_correspondence_state_"+click_account_id, "", "");
				$("#guardian_correspondence_state_"+click_account_id).val('');
				$("#guardian_correspondence_state_"+click_account_id).prop('selected', '');
				$('#guardian_correspondence_state_other_'+click_account_id).val('');
				$('#guardian_correspondence_state_other_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_span_other_state_'+click_account_id).attr('style', 'display:none;');
				$('#guardian_correspondence_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#guardian_correspondence_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- // 
			}
		}
	});	
}

function clear_is_same_as_guardian(click_account_id)
{
	if($('#grdnCorrespondenceAdd_'+click_account_id).is(':checked')){
			$('#grdnCorrespondenceAdd_'+click_account_id).prop('checked', false);
			
			$('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('disabled', false);		
			$('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('checked', false);	
			$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('checked', false);
			$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('disabled', false);
			$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+click_account_id).val('');

			//------------------------------------- Changes 10/08/14 ----------------------------------------- //
			$('#guardian_correspondence_is_foreign_address_'+click_account_id).prop('disabled', false);
			$('#guardian_correspondence_is_foreign_address_'+click_account_id).prop('checked', false);
			
			$('#guardian_correspondence_address_line1_'+click_account_id).val('');
			$('#guardian_correspondence_address_line1_'+click_account_id).prop('readonly', false);
			$('#guardian_correspondence_address_line2_'+click_account_id).val('');
			$('#guardian_correspondence_address_line2_'+click_account_id).prop('readonly', false);
			$('#guardian_correspondence_address_line3_'+click_account_id).val('');
			$('#guardian_correspondence_address_line3_'+click_account_id).prop('readonly', false);
			$('#guardian_correspondence_city_village_town_'+click_account_id).val('');
			$('#guardian_correspondence_city_village_town_'+click_account_id).prop('readonly', false);
			$('#guardian_correspondence_zipcode_'+click_account_id).val('');
			$('#guardian_correspondence_zipcode_'+click_account_id).prop('readonly', false);
			
			$('#guardian_correspondence_country_'+click_account_id).prop('disbaled', false);
			populate_country(102, "guardian_correspondence_country_"+click_account_id, "span_guardian_correspondence_country_"+click_account_id, 0, "");
			populate_states(102, "guardian_correspondence_state_"+click_account_id, "span_guardian_correspondence_state_"+click_account_id, "", "");
			$("#guardian_correspondence_state_"+click_account_id).val('');
			$("#guardian_correspondence_state_"+click_account_id).prop('selected', '');
			$('#guardian_correspondence_state_other_'+click_account_id).val('');
			$('#guardian_correspondence_state_other_'+click_account_id).prop('readonly', false);
			$('#guardian_correspondence_span_other_state_'+click_account_id).attr('style', 'display:none;');
			$('#guardian_correspondence_zipcode_'+click_account_id).prop('maxlength', '6');
			$('#guardian_correspondence_zipcode_'+click_account_id).addClass('numeric');
			$('.numeric').numeric();
			is_foreign_address();
		}
}

function copy_nominee_address()
{
    $('.nominee_same_per_address').click(function(){
		var click_account_id = $(this).attr("account_count");
        var for_address = $(this).attr("for_address");
        if($(this).val()=="P"){
            if($('#nominee_is_same_as_permanent').is(":checked")){
                $('#nominee_is_same_as_correspondence').prop('checked', false);
                //$('#nominee_is_same_as_permanent').prop('disabled', false);
                $('#end_nom_addr_copied_from').val('P');
                var filtervars = {
                    do_what:'populate_address',
                    address_value:"P",
                    address_for:"nominee",
                    account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
                };
                $.ajax({
                    type: "POST",
                    url: "_ajax_files/get_address_ajax.php",
                    data: filtervars,
                    dataType: "html",
                    success: function(html){
                        $('#populate_'+for_address+'_address').html(html);
                        $.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
                        add_functions();
                    }
                });
            }
            else {
                //$('#nominee_is_same_as_correspondence').prop('disabled', false);      
                $('#nominee_is_same_as_permanent').prop('checked', false);
                $('#end_nom_addr_copied_from').val('');
				//----------------------- changes 8-8-14 ------------------//
				$('#end_is_foreign_address').prop('disabled', false);
				$('#end_is_foreign_address').prop('checked', false);
				$('#end_address_line1').prop('readonly', false);
				$('#end_address_line2').prop('readonly', false);
				$('#end_address_line3').prop('readonly', false);
				$('#end_city_village_town').prop('readonly', false);
				$('#end_zipcode').prop('readonly', false);
				$('#end_country').prop('disabled', false);
				$('#end_state').prop('disabled', false);
				$('#end_state_other').prop('readonly', false);

				$('#end_address_line1').val('');
				$('#end_address_line2').val('');
				$('#end_address_line3').val('');
				$('#end_city_village_town').val('');
				$('#end_zipcode').val('');
				populate_country(102, "end_country", "span_end_country", 0, "");
				populate_states(102, "end_state", "span_end_state", "", "end_state");
				$('#end_state_other').val('');
				$('#span_end_state_other').attr('style', 'display:none;');
				$('.numeric').numeric();
				is_foreign_address();
				//----------------------- End changes 8-8-14 ---------------------//
            }
        }
        else if($(this).val()=="C")
        {
            if($('#nominee_is_same_as_correspondence').is(":checked")){
                $('#nominee_is_same_as_permanent').prop('checked', false);
                //$('#nominee_is_same_as_correspondence').prop('disabled', false);
                $('#end_nom_addr_copied_from').val('C');
                var filtervars = {
                    do_what:'populate_address',
                    address_value:"C",
                    address_for:"nominee",
                    account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
                };
                $.ajax({
                    type: "POST",
                    url: "_ajax_files/get_address_ajax.php",
                    data: filtervars,
                    dataType: "html",
                    success: function(html){
                        $('#populate_'+for_address+'_address').html(html);
                        $.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
                        add_functions();
                    }
                });
            }
            else {
                //$('#nominee_is_same_as_permanent').prop('disabled', false);       
                $('#nominee_is_same_as_correspondence').prop('checked', false);
                $('#end_nom_addr_copied_from').val('');
				//----------------------- changes 8-8-14 ------------------//
				$('#end_is_foreign_address').prop('disabled', false);
				$('#end_is_foreign_address').prop('checked', false);
				$('#end_address_line1').prop('readonly', false);
				$('#end_address_line2').prop('readonly', false);
				$('#end_address_line3').prop('readonly', false);
				$('#end_city_village_town').prop('readonly', false);
				$('#end_zipcode').prop('readonly', false);
				$('#end_country').prop('disabled', false);
				$('#end_state').prop('disabled', false);
				$('#end_state_other').prop('readonly', false);

				$('#end_address_line1').val('');
				$('#end_address_line2').val('');
				$('#end_address_line3').val('');
				$('#end_city_village_town').val('');
				$('#end_zipcode').val('');
				populate_country(102, "end_country", "span_end_country", 0, "");
				populate_states(102, "end_state", "span_end_state", "", "end_state");
				$('#end_state_other').val('');
				$('#span_end_state_other').attr('style', 'display:none;');
				$('.numeric').numeric();
				is_foreign_address();
				//----------------------- End changes 8-8-14 ---------------------//
            }
        }
		/*else{
			$('#nominee_is_same_as_permanent').prop('disabled', false);       
			$('#nominee_is_same_as_correspondence').prop('checked', false);
			$('#end_nom_addr_copied_from').val('');
			$('#end_is_foreign_address').prop('checked', false);
		}*/
    }); 
}

function copy_nominee_guardian_address()
{
    $('.nominee_guardian_same_per_address').click(function(){
		var click_account_id = $(this).attr("account_count");
        var for_address = $(this).attr("for_address");
        if($(this).val()=="P"){
            if($('#guardian_nominee_is_same_as_permanent').is(":checked")){
                $('#guardian_nominee_is_same_as_correspondence').prop('checked', false);
                //$('#guardian_nominee_is_same_as_permanent').prop('disabled', false);
                $('#end_guardian_addr_copied_from').val('P');
                var filtervars = {
                    do_what:'populate_address',
                    address_value:"P",
                    address_for:"nomineeguardian",
                    account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
                };
                $.ajax({
                    type: "POST",
                    url: "_ajax_files/get_address_ajax.php",
                    data: filtervars,
                    dataType: "html",
                    success: function(html){
                        $('#populate_'+for_address+'_address').html(html);
                        $.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
                        add_functions();
                    }
                });
            }
            else {
                //$('#guardian_nominee_is_same_as_correspondence').prop('disabled', false);      
                $('#guardian_nominee_is_same_as_permanent').prop('checked', false);
                $('#end_guardian_addr_copied_from').val('');

				//----------------------- changes 8-8-14 ------------------//
				$('#end_guardian_is_foreign_address').prop('disabled', false);
				$('#end_guardian_is_foreign_address').prop('checked', false);
				$('#end_guardian_address_line1').prop('readonly', false);
				$('#end_guardian_address_line2').prop('readonly', false);
				$('#end_guardian_address_line3').prop('readonly', false);
				$('#end_guardian_city_village_town').prop('readonly', false);
				$('#end_guardian_zipcode').prop('readonly', false);
				$('#end_guardian_country').prop('disabled', false);
				$('#end_guardian_state').prop('disabled', false);
				$('#end_guardian_state_other').prop('readonly', false);

				$('#end_guardian_address_line1').val('');
				$('#end_guardian_address_line2').val('');
				$('#end_guardian_address_line3').val('');
				$('#end_guardian_city_village_town').val('');
				$('#end_guardian_zipcode').val('');
				populate_country(102, "end_guardian_country", "span_end_guardian_country", 0, "");
				populate_states(102, "end_guardian_state", "span_end_guardian_state", "", "end_guardian_state");
				$('#end_guardian_state_other').val('');
				$('#span_end_guardian_state_other').attr('style', 'display:none;');
				$('.numeric').numeric();
				is_foreign_address();
				//----------------------- End changes 8-8-14 ---------------------//
            }
        }
        else if($(this).val()=="C")
        {
            if($('#guardian_nominee_is_same_as_correspondence').is(":checked")){
                $('#guardian_nominee_is_same_as_permanent').prop('checked', false);
                $('#guardian_nominee_is_same_as_correspondence').prop('disabled', false);
                //$('#end_guardian_addr_copied_from').val('C');
                var filtervars = {
                    do_what:'populate_address',
                    address_value:"C",
                    address_for:"nomineeguardian",
                    account_id:click_account_id,
                    auth_token: $("#auth_token_name").val()
                };
                $.ajax({
                    type: "POST",
                    url: "_ajax_files/get_address_ajax.php",
                    data: filtervars,
                    dataType: "html",
                    success: function(html){
                        $('#populate_'+for_address+'_address').html(html);
                        $.validate({form:"#frmBankAccount", onSuccess:validate_fixeddeposits});
                        add_functions();
                    }
                });
            }
            else {
                //$('#guardian_nominee_is_same_as_permanent').prop('disabled', false);       
                $('#guardian_nominee_is_same_as_correspondence').prop('checked', false);
                $('#end_guardian_addr_copied_from').val('');
				//----------------------- changes 8-8-14 ------------------//
				$('#end_guardian_is_foreign_address').prop('disabled', false);
				$('#end_guardian_is_foreign_address').prop('checked', false);
				$('#end_guardian_address_line1').prop('readonly', false);
				$('#end_guardian_address_line2').prop('readonly', false);
				$('#end_guardian_address_line3').prop('readonly', false);
				$('#end_guardian_city_village_town').prop('readonly', false);
				$('#end_guardian_zipcode').prop('readonly', false);
				$('#end_guardian_country').prop('disabled', false);
				$('#end_guardian_state').prop('disabled', false);
				$('#end_guardian_state_other').prop('readonly', false);

				$('#end_guardian_address_line1').val('');
				$('#end_guardian_address_line2').val('');
				$('#end_guardian_address_line3').val('');
				$('#end_guardian_city_village_town').val('');
				$('#end_guardian_zipcode').val('');
				populate_country(102, "end_guardian_country", "span_end_guardian_country", 0, "");
				populate_states(102, "end_guardian_state", "span_end_guardian_state", "", "end_guardian_state");
				$('#end_guardian_state_other').val('');
				$('#span_end_guardian_state_other').attr('style', 'display:none;');
				$('.numeric').numeric();
				is_foreign_address();
				//----------------------- End changes 8-8-14 ---------------------//
            }
        }
		else{
			$('#guardian_nominee_is_same_as_permanent').prop('disabled', false);       
			$('#guardian_nominee_is_same_as_correspondence').prop('checked', false);
			$('#end_guardian_addr_copied_from').val('');
			$('#end_is_foreign_address').prop('checked', false);
			$('.numeric').numeric();
            $('.numeric_share').numeric({decimal:"."});
			is_foreign_address();
		}
    }); 
}


function validate_fixeddeposits(){
		$("select").prop("disabled", false); 
		//$("input").prop("readonly", false); 
		//--------------------------- changes 8-8-14 -------------------------- //
		$('#end_address_line1').prop('readonly', false);
		$('#end_address_line2').prop('readonly', false);
		$('#end_address_line3').prop('readonly', false);
		$('#end_city_village_town').prop('readonly', false);
		$('#end_state_other').prop('readonly', false);
		$('#end_zipcode').prop('readonly', false);
		$('#end_zipcode').attr('data-validation', '');
		$('#end_zipcode').attr('data-validation-length', '');
		$('#end_zipcode').attr('data-validation-msg', '');
		$('#end_zipcode').prop('readonly', false);
		$('input[name=end_state_other]').prop('readonly', false);
		$('#end_country').prop('disabled', false);
		$('#end_state').prop('disabled', false);
		$('#end_is_foreign_address').prop('disabled', false);



		$('#end_guardian_address_line1').prop('readonly', false);
		$('#end_guardian_address_line2').prop('readonly', false);
		$('#end_guardian_address_line3').prop('readonly', false);
		$('#end_guardian_city_village_town').prop('readonly', false);
		$('#end_guardian_state_other').prop('readonly', false);
		$('#end_guardian_zipcode').prop('readonly', false);
		$('input[name=end_guardian_state_other]').prop('readonly', false);
		$('#end_guardian_country').prop('disabled', false);
		$('#end_guardian_state').prop('disabled', false);
		$('#end_guardian_is_foreign_address').prop('disabled', false);
		

		$("select").prop("disabled", false); 
		$("input").prop("readonly", false); 
		$("input").prop("disabled", false); 

		return true;

		//--------------------------- End changes 8-8-14 -------------------------- //
}