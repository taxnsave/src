var phone_count = new Array();
var office_phone_count = new Array();
var fax_count = new Array();
var mobile_count = new Array();
var email_count = new Array();

var actual_phone_count = new Array();
var actual_office_phone_count = new Array();
var actual_fax_count = new Array();
var actual_mobile_count = new Array();
var actual_email_count = new Array();

var account_count = 1;
var actual_account_count = 1;

var beneficiary_count = 1;
var actual_beneficiary_count = 1;
var total_count = 1;

$(document).ready(function(){
    if(($("#ast_id").val()>0 && $('input:radio[name=ownership_type]:checked').val()=="joint") || ($("#joint_account_count").val()>0 && $('input:radio[name=ownership_type]:checked').val()=="joint")){
        $("#div_joint_account_holders").html($("#div_joint_account_holders_hidden").html());
		add_functions();
		$('.numeric').numeric();
		$('.numeric_share').numeric({decimal:"."});
		is_foreign_address();
		add_date_functions($("#joint_account_count").val());
	}
    $('.numeric').numeric();
	$('.numeric_share').numeric({decimal:"."});
    on_ready_functions();
    /* Edit bank account */
    $(".a_edit").click(function(){
        var ast_id = $(this).attr("rec_id");
        $("#hid_ast_id").val(ast_id);
        document.frmHidden.submit();
    });


    $(".a_delete").click(function(){
        if( !confirm("Are you sure you want to delete this other asset details?") )
            return;
            
        $("#loading").show();
        
        var filtervars = {
            do_what:'del_otherasset',
            delete_ids: $(this).attr("rec_id"),
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/other-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                alert("Other asset details deleted successfully");
                window.location.reload('other-assets-details.php');
            }
        });
    });


});

function on_ready_functions()
{
    $.validate();
	
	$('#loan_against_asset').click(function(){
		if($(this).is(":checked")){
			$('#span_lender_details').show();
			$('#lender_details').attr('data-validation', 'length');
			$('#lender_details').attr('data-validation-length', '1-200');
			$('#lender_details').attr('data-validation-error-msg', 'Please enter valid details of the Lender');
			$('#lender_details').val('');
		}else{
			$('#span_lender_details').hide();
			$('#lender_details').attr('data-validation', '');
			$('#lender_details').attr('data-validation-length', '');
			$('#lender_details').attr('data-validation-error-msg', '');
			$('#lender_details').val('');
		}
	});

    $('#asset_type').change(function(){
        var selected  = $('#asset_type option:selected').text();
        var selected_lower = selected.toLowerCase(); 
        if(selected_lower == 'others' || selected_lower == 'other')
            $('#span_other1_asset').show();
        else
            $('#span_other1_asset').hide();
    });


	$('#is_this_prop_mortgaged').click(function(){
		if($(this).is(":checked")){
			$('#span_prop_mortgaged_name').show();
			$('#prop_mortgaged_name').attr('data-validation', 'length');
			$('#prop_mortgaged_name').attr('data-validation-length', '1-200');
			$('#prop_mortgaged_name').attr('data-validation-error-msg', 'Please enter valid Name with whom it is Mortgaged');
			$('#prop_mortgaged_name').val('');
		}else{
			$('#span_prop_mortgaged_name').hide();
			$('#prop_mortgaged_name').attr('data-validation', '');
			$('#prop_mortgaged_name').attr('data-validation-length', '');
			$('#prop_mortgaged_name').attr('data-validation-error-msg', '');
			$('#prop_mortgaged_name').val('');
		}
	});



    $(".sel_ownership_type").change(function(){
        var o_type = $(this).val();
        if(o_type=="single" || o_type==""){
			if($("#joint_account_count").val()>0)
				$("#div_joint_account_holders_hidden").html($("#div_joint_account_holders").html());
            $("#div_joint_account_holders").html("");
            return;
        }
        if($("#joint_account_count").val()==0)
        {
            var filtervars = {
                do_what:'get_joint_accounts_html',
                sbm_id:$("#sbm_id").val(),
                auth_token: $("#auth_token_name").val()
            };

            $.ajax({
                type: "POST",
                url: "_ajax_files/other-assets-details_ajax.php",
                data: filtervars,
                dataType: "html",
                success: function(html){
                    $(".a_add_account").remove();
                    $("#joint_account_count").val(actual_account_count);
                    $("#total_count").val(parseInt(total_count));
                    $("#div_joint_account_holders").html(html);
                    $.validate();
                    add_functions();
                    $('.numeric').numeric();
					$('.numeric_share').numeric({decimal:"."});
                    is_foreign_address();
					add_date_functions($("#joint_account_count").val());
                }
            });
        }else{
            $("#div_joint_account_holders").html($("#div_joint_account_holders_hidden").html());
			add_guardian_functions($("#total_count").val());
            add_functions();
        }
    });
    
    //toggle_other_state('state','state_other');
    $( ".sel_country" ).trigger( "change" );
    add_functions();
}

function add_functions()
{
	$.validate();
    /* Beneficiary */
    $(".a_beneficiary").unbind("click");
    $(".a_beneficiary").click(function(){
        var current_beneficiary = $(this).attr("rec_id");
        beneficiary_count++;
        actual_beneficiary_count++;
        var filtervars = {
            do_what:'add_beneficiary',
            beneficiary_count:beneficiary_count,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/other-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $(".a_beneficiary").remove();
                $("#div_beneficiary").append(html);
                add_functions();
            }
        });
    });


	
	



    /* add remove account holder */
    $(".a_add_account").unbind("click");
    $(".a_add_account").click(function(){
        account_count++;
        actual_account_count++;
        var filtervars = {
            do_what:'add_joint_account_holder',
            account_count:account_count,
            auth_token: $("#auth_token_name").val()
        };
        $("#joint_account_count").val(actual_account_count);
        $.ajax({
            type: "POST",
            url: "_ajax_files/other-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $(".a_add_account").remove();
                $("#div_joint_account_holders").append(html);
                $("#total_count").val(account_count);
                add_functions();
                $.validate();
                $('.numeric').numeric();
				$('.numeric_share').numeric({decimal:"."});
                is_foreign_address();
				add_date_functions(account_count);
            }
        });
    });

    $(".a_removeaccount").unbind("click");
    $(".a_removeaccount").click(function(){
        actual_account_count--;
        if(actual_account_count==0){
            actual_account_count++;
            alert("You can not delete this account holder, Please add at-least joint account holder.");
            return;
        }

        $("#joint_account_count").val(actual_account_count);
        var remove_id = $(this).attr("remove_id");
        $("#account_"+remove_id).remove();
        var temp = $('.a_removeaccount').length;
        temp --;
        $('#span_account_'+$('.a_removeaccount').eq(temp).attr('remove_id')).html('<a href="javascript:void(0);" class="btn btn-primary btn-sm btn-add-large a_add_account">+ Add Joint Account Holder</a>');

        add_functions();
    });
    
    /* add remove account holder ends*/

    /* Email box */
    $(".a_jah_email").unbind("click");
    $(".a_jah_email").click(function(){
        var current_account = $(this).attr("rec_id");
        if(email_count[current_account]===undefined)
            email_count[current_account] = 2;
        else
            email_count[current_account]++;

        if(actual_email_count[current_account]===undefined)
            actual_email_count[current_account] = 2;
        else
            actual_email_count[current_account]++;

        if(actual_email_count[current_account]==4){
            actual_email_count[current_account] = 3;
            alert("You can not add more than 3 emails");
            return;
        }
        var filtervars = {
            do_what:'add_jah_email',
            jah_email_count:email_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/other-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#JahEmailAddWrap_"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removejahemail").unbind("click");
    $(".a_removejahemail").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_email_count[current_account]===undefined)
            actual_email_count[current_account] = 1;
        else
            actual_email_count[current_account]--;
        $(this).parent('div').remove();
    });
    /* Email box ends */
    
    /* add remove phone */
    $(".a_phone").unbind("click");
    $(".a_phone").click(function(){
        var current_account = $(this).attr("rec_id");
        if(phone_count[current_account]===undefined)
            phone_count[current_account] = 2;
        else
            phone_count[current_account]++;

        if(actual_phone_count[current_account]===undefined)
            actual_phone_count[current_account] = 2;
        else
            actual_phone_count[current_account]++;
        if(actual_phone_count[current_account]==6){
            actual_phone_count[current_account] = 5;
            alert("You can not add more than 5 phone numbers");
            return;
        }
        var filtervars = {
            do_what:'add_phone',
            phone_count:phone_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/other-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#TelephoneAddWrap_"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removephone").unbind("click");
    $(".a_removephone").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_phone_count[current_account]===undefined)
            actual_phone_count[current_account] = 1;
        else
            actual_phone_count[current_account]--;
        $(this).parent('div').remove();
    });
    /* add remove phone ends */

    /* Office Phone */
    $(".a_office_phone").unbind("click");
    $(".a_office_phone").click(function(){
        var current_account = $(this).attr("rec_id");
        if(office_phone_count[current_account]===undefined)
            office_phone_count[current_account] = 2;
        else
            office_phone_count[current_account]++;

        if(actual_office_phone_count[current_account]===undefined)
            actual_office_phone_count[current_account] = 2;
        else
            actual_office_phone_count[current_account]++;

        if(actual_office_phone_count[current_account]==6){
            actual_office_phone_count[current_account] = 5;
            alert("You can not add more than 5 office numbers");
            return;
        }
        var filtervars = {
            do_what:'add_office_phone',
            phone_count:office_phone_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/other-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#InputsWrapperOffice_"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removeofficephone").unbind("click");
    $(".a_removeofficephone").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_office_phone_count[current_account]===undefined)
            actual_office_phone_count[current_account] = 1;
        else
            actual_office_phone_count[current_account]--;
        $(this).parent('div').remove();
        //$("#"+remove_id).remove();
    });
    /* Office Phone ends */

    /* Fax */
    $(".a_fax").unbind("click");
    $(".a_fax").click(function(){
        var current_account = $(this).attr("rec_id");
        if(fax_count[current_account]===undefined)
            fax_count[current_account] = 2;
        else
            fax_count[current_account]++;

        if(actual_fax_count[current_account]===undefined)
            actual_fax_count[current_account] = 2;
        else
            actual_fax_count[current_account]++;

        if(actual_fax_count[current_account]==6){
            actual_fax_count[current_account] = 5;
            alert("You can not add more than 5 fax numbers");
            return;
        }
        var filtervars = {
            do_what:'add_fax',
            phone_count:fax_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/other-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#MobileAddWrap"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removefax").unbind("click");
    $(".a_removefax").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_fax_count[current_account]===undefined)
            actual_fax_count[current_account] = 1;
        else
            actual_fax_count[current_account]--;
        $(this).parent('div').remove();
        //$("#"+remove_id).remove();
    });
    /* Fax ends */

    /* Mobile box */
    $(".a_mobile").unbind("click");
    $(".a_mobile").click(function(){
        var current_account = $(this).attr("rec_id");
        if(mobile_count[current_account]===undefined)
            mobile_count[current_account] = 2;
        else
            mobile_count[current_account]++;

        if(actual_mobile_count[current_account]===undefined)
            actual_mobile_count[current_account] = 2;
        else
            actual_mobile_count[current_account]++;

        if(actual_mobile_count[current_account]==4){
            actual_mobile_count[current_account] = 3;
            alert("You can not add more than 3 mobile numbers");
            return;
        }
        var filtervars = {
            do_what:'add_mobile',
            phone_count:mobile_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/other-assets-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#MobileAddWrap_"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removemobile").unbind("click");
    $(".a_removemobile").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_mobile_count[current_account]===undefined)
            actual_mobile_count[current_account] = 1;
        else
            actual_mobile_count[current_account]--;
        $(this).parent('div').remove();
        //$("#"+remove_id).remove();
    });
    /* Mobile box ends */
 
    
    /* Beneficiary */
    $(".a_removebeneficiary").unbind("click");
    $(".a_removebeneficiary").click(function(){
        //alert(actual_beneficiary_count);
        if(actual_beneficiary_count==1){
            //actual_beneficiary_count++;
            alert("This is the default beneficary share and You cannot delete else you will need to define atleast one beneficary share.");
            return;
        }
        var remove_id = $(this).attr("remove_id");
        actual_beneficiary_count--;
        $("#div_beneficiary_"+remove_id).remove();

        $("#account_"+remove_id).remove();
        var temp = $('.a_removebeneficiary').length;
        temp --;
        
        $('#span_beneficiary_'+$('.a_removebeneficiary').eq(temp).attr('remove_id')).html('<a class="btn btn-primary btn-sm btn-add-large a_beneficiary" id="beneficiaryAddBtn" title="Add beneficiary" alt="Add beneficiary" href="javascript:void(0);">+ Add Beneficiary</a>');
        add_functions();
    });
    /* Beneficiary */
	

	  $(".a_removebeneficiarynew").unbind("click");
    $(".a_removebeneficiarynew").click(function(){
		if( !confirm("Are you sure you want to delete this Beneficiary?") )
            return;
		else{
		var bcount		= $("#tot_ben").val();
		var trshare		= $(this).attr("trshare");
		var total_share = $('#total_share').val();
        var trid =  $(this).attr("trid");
		$("#"+$(this).attr("trid")).remove();
		total_share = parseInt(total_share) - parseInt(trshare);
		$('#total_share').val(total_share);
		$("#tot_ben").val(bcount-1);
		$('#beneficiaryAddBtnNew').show();
		
		alert('Beneficiary deleted successfully');
		change_jah_ben_numbering();
		}
    });


	$(".jah_is_adult").change(function(){
		var account_id = $(this).attr("ac_id");
		$('#show_ejd_birth_date_'+account_id).val("");
		$('#ejd_birth_date_'+account_id).val("");
		if($(this).val()=="1")
			$("#ejd_guardian_"+$(this).attr("ac_id")).html("");
		else{
			add_guardian($(this).attr("ac_id"));
		}
	});

    change_jah_ben_numbering();
}

function add_date_functions(account_id)
{
	$('#show_ejd_birth_date_'+account_id).datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		minDate: "-150Y",
		maxDate: new Date(),
		yearRange: "-150:+0",
		altField: "#ejd_birth_date_"+account_id,
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect: function (dateText, inst) {
			var startDate = new Date(dateText);
			var date = $(this).datepicker('getDate');
			var selectedYear = startDate.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
			if(diff > 100)
				alert('You have selected '+selectedYear+' as birth year');
			else if(diff < 18){
				add_guardian(account_id);
				$('#ejd_is_adult_'+account_id).val("0");
			}
			else{
				$('#ejd_is_adult_'+account_id).val("1");
				$('#ejd_guardian_'+account_id).html('');
			}
		}	
	});	
}

function add_guardian(account_id)
{
	var filtervars = {
		do_what:'add_guardian',
		account_id: account_id,
        auth_token: $("#auth_token_name").val()
	};
	$.ajax({
		type: "POST",
		url: "_ajax_files/bank-account-details_ajax.php",
		data: filtervars,
		dataType: "html",
		success: function(html){
			$('#ejd_guardian_'+account_id).html(html);
			$.validate();
			$('.numeric').numeric();
			$('.numeric_share').numeric({decimal:"."});
			add_guardian_functions(account_id);
		}
	});
}

function add_guardian_functions(account_id)
{
	$('#show_guardian_birth_date_'+account_id).datepicker("destroy");
	$('#show_guardian_birth_date_'+account_id).datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		//maxDate: "-18Y",
		yearRange: "-150:+0",
		altField: "#guardian_birth_date_"+account_id,
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect: function (dateText, inst) {
			var startDate = new Date(dateText);
			var selectedYear = startDate.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=  todayYear-selectedYear;
			if(diff > 100)
				alert('You have selected '+selectedYear+' as birth year');
		}
	});

	$(".grdnCorrespondenceAdd").click(function(){
		var acc_id = $(this).attr("account_id");
		if($(this).is(":checked")){
			$('#guardian_correspondence_address_line1_'+acc_id).val($('#guardian_permanent_address_line1_'+acc_id).val());
			$('#guardian_correspondence_address_line2_'+acc_id).val($('#guardian_permanent_address_line2_'+acc_id).val());
			$('#guardian_correspondence_address_line3_'+acc_id).val($('#guardian_permanent_address_line3_'+acc_id).val());
			$('#guardian_correspondence_city_village_town_'+acc_id).val($('#guardian_permanent_city_village_town_'+acc_id).val());
			$('#guardian_correspondence_country_'+acc_id).val($('#guardian_permanent_country_'+acc_id).val());
			$('#guardian_correspondence_state_'+acc_id).val($('#guardian_permanent_state_'+acc_id).val());
			
			populate_states($('#guardian_correspondence_country_'+acc_id).val(), "guardian_correspondence_state_"+acc_id, "span_guardian_correspondence_state_"+acc_id,$('#guardian_permanent_state_'+acc_id).val());

		
			if($('#guardian_permanent_span_other_state_'+acc_id).is(':visible')){
                $('#guardian_correspondence_span_other_state_'+acc_id).show();
            }else{
                $('#guardian_correspondence_span_other_state_'+acc_id).hide();
            }


			$('#guardian_correspondence_state_other_'+acc_id).val($('#guardian_permanent_state_other_'+acc_id).val());
			$('#guardian_correspondence_zipcode_'+acc_id).val($('#guardian_permanent_zipcode_'+acc_id).val());
		}else{
			$('#guardian_correspondence_address_line1_'+acc_id).val('');
			$('#guardian_correspondence_address_line2_'+acc_id).val('');
			$('#guardian_correspondence_address_line3_'+acc_id).val('');
			$('#guardian_correspondence_city_village_town_'+acc_id).val('');
			$('#guardian_correspondence_country_'+acc_id).val('');
			$('#guardian_correspondence_state_'+acc_id).val('');
			$('#guardian_correspondence_state_other_'+acc_id).val('');
			$('#guardian_correspondence_zipcode_'+acc_id).val('');
		}
	});
}