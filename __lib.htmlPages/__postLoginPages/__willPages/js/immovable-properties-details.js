var phone_count = new Array();
var office_phone_count = new Array();
var fax_count = new Array();
var mobile_count = new Array();
var email_count = new Array();

var actual_phone_count = new Array();
var actual_office_phone_count = new Array();
var actual_fax_count = new Array();
var actual_mobile_count = new Array();
var actual_email_count = new Array();

var account_count = 1;
var actual_account_count = 1;

var beneficiary_count = 1;
var actual_beneficiary_count = 1;
var total_count = 1;

$(document).ready(function(){
	if(($("#prp_id").val()>0 && $('input:radio[name=ownership_type]:checked').val()=="joint") ||   ($("#joint_account_count").val()>0 && $('input:radio[name=ownership_type]:checked').val()=="joint")){
		$("#div_joint_account_holders").html($("#div_joint_account_holders_hidden").html());
		add_functions();
		$('.numeric').numeric();
		is_foreign_address();
		/*add_date_functions($("#joint_account_count").val());
		add_guardian_functions($("#joint_account_count").val());*/

		add_date_functions($("#joint_account_count").val());
        add_guardian_birth_date_for_all($("#joint_account_count").val());
        add_guardian_functions($("#joint_account_count").val());
	}
	
	if($('#is_this_prop_mortgaged').is(":checked")){
		$('#span_prop_mortgaged_name').show();
		$('#prop_mortgaged_name').attr('data-validation', 'length');
		$('#prop_mortgaged_name').attr('data-validation-length', '1-200');
		$('#prop_mortgaged_name').attr('data-validation-error-msg', 'Please enter valid Name with whom it is Mortgaged');
		
	}else{
		$('#span_prop_mortgaged_name').hide();
		$('#prop_mortgaged_name').attr('data-validation', '');
		$('#prop_mortgaged_name').attr('data-validation-length', '');
		$('#prop_mortgaged_name').attr('data-validation-error-msg', '');
	}

    on_ready_functions();
    /* Edit bank account */
    $(".a_edit").click(function(){
        var prp_id = $(this).attr("rec_id");
        $("#hid_prp_id").val(prp_id);
        document.frmHidden.submit();
    });
	


    $(".a_delete").click(function(){
        if( !confirm("Are you sure you want to delete this Property details?") )
            return;
            
        $("#loading").show();
        
        var filtervars = {
            do_what:'del_property',
            delete_ids: $(this).attr("rec_id"),
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/immovable-properties-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                alert("Property details deleted successfully.");
                window.location.href='immovable-properties-details';
            }
        });
    });
	
	
});

function on_ready_functions()
{
	$(".numeric_dobule").numeric();
    $.validate({form:"#frmProperty", onSuccess:validate_immovable});

	$('#is_this_prop_mortgaged').click(function(){
		if($(this).is(":checked")){
			$('#span_prop_mortgaged_name').show();
			$('#prop_mortgaged_name').attr('data-validation', 'length');
			$('#prop_mortgaged_name').attr('data-validation-length', '1-200');
			$('#prop_mortgaged_name').attr('data-validation-error-msg', 'Please enter valid Name with whom it is Mortgaged');
		}else{
			$('#span_prop_mortgaged_name').hide();
			$('#prop_mortgaged_name').attr('data-validation', '');
			$('#prop_mortgaged_name').attr('data-validation-length', '');
			$('#prop_mortgaged_name').attr('data-validation-error-msg', '');
		}
	});
	
	$('.is_property_address_same').click(function(){
		if(this.value=="P"){
			if($('#is_same_as_permanent').is(":checked")){
			
				$('#is_same_as_correspondence').prop('checked', false);
				$('#is_same_as_permanent').prop('disabled', false);
				$('#is_same_as_permanent_or_correspondence').val('P');
				var filtervars = {
					do_what:'populate_address',
					address_value:"P",
                	auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/immovable-properties-details_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						if(html=="foreign"){
							alert('Immovable property address should not be a Foreign address');
							$('#is_same_as_permanent').prop('checked', false);
							$('#is_same_as_correspondence').prop('readonly', false);
							$('#is_same_as_correspondence').prop('disabled', false);
							$('#address_line1').val('');
							$('#address_line2').val('');
							$('#address_line3').val('');
							$('#city_village_town').val('');
							$('#zipcode').val('');
							$('#country').val(102);
							populate_country(102, "country", "span_country", 0, "");
							populate_states(102, "state", "span_state", "", "state");
							$('#state_other').val('');
							$('#span_state_other').attr('style', 'display:none;');
							$("select").prop("disabled", false); 
							$("input").prop("readonly", false); 
						}
						else
							$('#populate_address').html(html);
						
						add_functions();
						add_date_functions($("#joint_account_count").val());
					}
				});
			}
			else {
				$('#is_same_as_correspondence').prop('disabled', false);		
				$('#is_same_as_permanent').prop('checked', false);
				$('#is_same_as_permanent_or_correspondence').val('');

				$('#address_line1').val('');
				$('#address_line2').val('');
				$('#address_line3').val('');
				$('#city_village_town').val('');
				$('#zipcode').val('');
				$('#address_line1').prop('readonly', false);		
				$('#address_line2').prop('readonly', false);	
				$('#address_line3').prop('readonly', false);	
				$('#city_village_town').prop('readonly', false);	
				$('#zipcode').prop('readonly', false);	
				$('#country').val(102);
				$('#country').prop('disabled', false);	
				$('#state').prop('disabled', false);	
				$('#state_other').prop('readonly', false);	
				populate_country(102, "country", "span_country", 0, "");
				populate_states(102, "state", "span_state", "", "state");
				$('#state').val("");
				$('#span_state_other').attr('style', 'display:none;');
				
			}
		}
		else if(this.value=="C"){
			if($('#is_same_as_correspondence').is(":checked")){
			
				$('#is_same_as_permanent').prop('checked', false);
				$('#is_same_as_correspondence').prop('disabled', false);
				$('#is_same_as_permanent_or_correspondence').val('C');
				var filtervars = {
					do_what:'populate_address',
					address_value:"C",
                	auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/immovable-properties-details_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						if(html=="foreign"){
							alert('Immovable property address should not be a Foreign address');
							$('#is_same_as_correspondence').prop('checked', false);
							$('#is_same_as_permanent').prop('readonly', false);
							$('#is_same_as_permanent').prop('disabled', false);
							$('#address_line1').val('');
							$('#address_line2').val('');
							$('#address_line3').val('');
							$('#city_village_town').val('');
							$('#zipcode').val('');
							$('#country').val(102);
							populate_country(102, "country", "span_country", 0, "");
							populate_states(102, "state", "span_state", "", "state");
							$('#state_other').val('');
							$('#span_state_other').attr('style', 'display:none;');
							$("select").prop("disabled", false); 
							$("input").prop("readonly", false); 
						}
						else
							$('#populate_address').html(html);

						add_functions();
						add_date_functions($("#joint_account_count").val());
					}
				});
			}
			else {
				$('#is_same_as_permanent').prop('disabled', false);		
				$('#is_same_as_correspondence').prop('checked', false);
				$('#is_same_as_permanent_or_correspondence').val('');
				
				$('#address_line1').val('');
				$('#address_line2').val('');
				$('#address_line3').val('');
				$('#city_village_town').val('');
				$('#zipcode').val('');
				$('#address_line1').prop('readonly', false);		
				$('#address_line2').prop('readonly', false);	
				$('#address_line3').prop('readonly', false);	
				$('#city_village_town').prop('readonly', false);	
				$('#zipcode').prop('readonly', false);	
				$('#country').val(102);
				$('#country').prop('disabled', false);	
				$('#state').prop('disabled', false);
				$('#state').val("");	
				$('#state_other').prop('readonly', false);	
				populate_country(102, "country", "span_country", 0, "");
				populate_states(102, "state", "span_state", "", "state");

				$('#span_state_other').attr('style', 'display:none;');
				
			}
		}		
	});

    $(".sel_ownership_type").change(function(){
        var o_type = $(this).val();
        if(o_type=="single" || o_type==""){
        	/*
			if($("#joint_account_count").val()>0)
				$("#div_joint_account_holders_hidden").html($("#div_joint_account_holders").html());
			$("#div_joint_account_holders").html("");
			*/
			if($("#joint_account_count").val()>0){
				if(!confirm("You are changing the ownership type from 'Joint' to 'Single'.\nThe data entered for Joint Account Holder shall be cleared if you continue.\nPlease click on 'OK' to continue or else click on 'Cancel'.")){
					$(":radio[value=joint]").prop("checked",true);
					return false;
				}
			}
			$("#div_joint_account_holders").html("");
			$("#joint_account_count").val("0");	
			account_count = actual_account_count = total_count = 1;
            return;
        }
        if($("#joint_account_count").val()==0)
        {
            var filtervars = {
                do_what:'get_joint_accounts_html',
                sbm_id:$("#sbm_id").val(),
                auth_token: $("#auth_token_name").val()
            };

            $.ajax({
                type: "POST",
                url: "_ajax_files/immovable-properties-details_ajax.php",
                data: filtervars,
                dataType: "html",
                success: function(html){
                    $(".a_add_account").remove();
                    $("#joint_account_count").val(actual_account_count);
                    $("#total_count").val(parseInt(total_count));
                    $("#div_joint_account_holders").html(html);
                    $.validate({form:"#frmProperty", onSuccess:validate_immovable});
                    add_functions();
					$('.numeric').numeric();
                    is_foreign_address();
					add_date_functions($("#joint_account_count").val());
                }
            });
        }else{
            $("#div_joint_account_holders").html($("#div_joint_account_holders_hidden").html());
			/*add_guardian_functions($("#total_count").val());
            add_functions();
			is_foreign_address();
			add_date_functions($("#joint_account_count").val());*/

			add_date_functions_for_all($("#joint_account_count").val());
            //add_guardian_functions($("#joint_account_count").val());
            add_guardian_birth_date_for_all($("#joint_account_count").val());
			add_guardian_functions($("#joint_account_count").val());
            add_functions();
        }
    });
    
    //toggle_other_state('state','state_other');
    $( ".sel_country" ).trigger( "change" );
    add_functions();
}

function add_functions()
{
	is_foreign_address();
	$('.numeric').numeric();
    /* Beneficiary */
    $(".a_beneficiary").unbind("click");
    $(".a_beneficiary").click(function(){
        var current_beneficiary = $(this).attr("rec_id");
        beneficiary_count++;
        actual_beneficiary_count++;
        var filtervars = {
            do_what:'add_beneficiary',
            beneficiary_count:beneficiary_count,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/immovable-properties-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $(".a_beneficiary").remove();
                $("#div_beneficiary").append(html);
                add_functions();
            }
        });
    });

	
	 /* Beneficiary
	$(".a_beneficiarynew").unbind("click");
    $(".a_beneficiarynew").click(function(){
		var ben_id, share, other_info, ben_name;
		var ben_exist = false;
		var bcount = $("#tot_ben").val();
		var total_share = $('#total_share').val();
		ben_name = $("#sel_beneficiary option:selected").text();
		ben_id = $("#sel_beneficiary").val();
		share = $("#txt_beneficiary_share").val();
		other_info = $("#txt_other_info").val();
		total_share = parseInt(total_share) + parseInt(share);
		if(ben_id<=0){
			alert("Please select a Beneficiary.");
			return false;
		}
		else if(parseInt(share)<=0 || share=="" || share>100){
			alert("Please enter valid Share.");
			return false;
		}
		else if(total_share > 100)
		{
			alert("Beneficiary total Percentage Share exceeds 100%");
			return false;
		}
		else if(total_share == 100)
		{
			$('#beneficiaryAddBtnNew').hide();
		}
		$('.cls_ben').each(function( index ) {  
			if(ben_id == this.value){
				alert('Beneficiary already exists.');
				ben_exist = true;
			}
		}); 
		if(ben_exist == true)
		{
			$('#beneficiaryAddBtnNew').show();
			return false;
		}

		if(bcount == 1){
			$('#no_beneficiary').remove();
		}
		
		$("#tb_con_ben").append('<tr id="tr_con_ben_'+bcount+'"><td class="ben_new_account_numbering">'+bcount+'</td><td>'+ben_name+'<input type="hidden" class="w120px" name="ben_id[]" value="0"/><input type="hidden" class="cls_ben" name="beneficiary[]" value="'+ben_id+'"></td><td>'+share+'%<input type="hidden" name="beneficiary_share[]" value="'+share+'"></td><td>'+other_info+'<input type="hidden" name="other_info[]" value="'+other_info+'"></td><td><a href="javascript:void(0);" class="a_removebeneficiarynew" trid="tr_con_ben_'+bcount+'" trshare="'+share+'">Delete</a></td></tr>');
		
		$("#tot_ben").val(parseInt(bcount)+1);
		$('#total_share').val(total_share);
		add_functions();
		clear_add_beneficiary();
		change_jah_ben_numbering();
    });

 */

	$(".a_removebeneficiarynew").unbind("click");
    $(".a_removebeneficiarynew").click(function(){
		if( !confirm("Are you sure you want to delete this Beneficiary?") )
            return;
		else{
		var bcount		= $("#tot_ben").val();
		var trshare		= $(this).attr("trshare");
		var total_share = $('#total_share').val();
        var trid =  $(this).attr("trid");
		$("#"+$(this).attr("trid")).remove();
		total_share = parseInt(total_share) - parseInt(trshare);
		$('#total_share').val(total_share);
		$("#tot_ben").val(bcount-1);
		$('#beneficiaryAddBtnNew').show();
		
		alert('Beneficiary deleted successfully');
		change_jah_ben_numbering();
		}
    });



    /* add remove account holder */
    $(".a_add_account").unbind("click");
    $(".a_add_account").click(function(){
        account_count++;
        actual_account_count++;
        var filtervars = {
            do_what:'add_joint_account_holder',
            account_count:account_count,
            auth_token: $("#auth_token_name").val()
        };
        $("#joint_account_count").val(actual_account_count);
        $.ajax({
            type: "POST",
            url: "_ajax_files/immovable-properties-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $(".a_add_account").remove();
                $("#div_joint_account_holders").append(html);
                $("#total_count").val(account_count);
                add_functions();
                $.validate({form:"#frmProperty", onSuccess:validate_immovable});
                $('.numeric').numeric();
                is_foreign_address();
				add_date_functions(account_count);
				copy_address();
            }
        });
    });

    $(".a_removeaccount").unbind("click");
    $(".a_removeaccount").click(function(){
        actual_account_count--;
        if(actual_account_count==0){
            actual_account_count++;
            alert("You can not delete this account holder, Please add at-least joint account holder.");
            return;
        }

        $("#joint_account_count").val(actual_account_count);
        var remove_id = $(this).attr("remove_id");
        $("#account_"+remove_id).remove();
        var temp = $('#div_joint_account_holders .a_removeaccount').length;
        if(temp==0)
            temp=1;
        temp --;
        $('#span_account_'+$('.a_removeaccount').eq(temp).attr('remove_id')).html('<a href="javascript:void(0);" class="btn btn-primary btn-sm btn-add-large a_add_account">+ Add Joint Account Holder</a>');

        add_functions();
    });
    
    /* add remove account holder ends*/
    
    /* add remove phone */
    $(".a_phone").unbind("click");
    $(".a_phone").click(function(){
        var current_account = $(this).attr("rec_id");
        if(phone_count[current_account]===undefined)
            phone_count[current_account] = 2;
        else
            phone_count[current_account]++;

        if(actual_phone_count[current_account]===undefined)
            actual_phone_count[current_account] = 2;
        else
            actual_phone_count[current_account]++;
        if(actual_phone_count[current_account]==6){
            actual_phone_count[current_account] = 5;
            alert("You can not add more than 5 phone numbers");
            return;
        }
        var filtervars = {
            do_what:'add_phone',
            phone_count:phone_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/immovable-properties-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#TelephoneAddWrap_"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removephone").unbind("click");
    $(".a_removephone").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_phone_count[current_account]===undefined)
            actual_phone_count[current_account] = 1;
        else
            actual_phone_count[current_account]--;
        $(this).parent('div').remove();
    });
    /* add remove phone ends */

    /* Office Phone */
    $(".a_office_phone").unbind("click");
    $(".a_office_phone").click(function(){
        var current_account = $(this).attr("rec_id");
        if(office_phone_count[current_account]===undefined)
            office_phone_count[current_account] = 2;
        else
            office_phone_count[current_account]++;

        if(actual_office_phone_count[current_account]===undefined)
            actual_office_phone_count[current_account] = 2;
        else
            actual_office_phone_count[current_account]++;

        if(actual_office_phone_count[current_account]==6){
            actual_office_phone_count[current_account] = 5;
            alert("You can not add more than 5 office numbers");
            return;
        }
        var filtervars = {
            do_what:'add_office_phone',
            phone_count:office_phone_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/immovable-properties-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#InputsWrapperOffice_"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removeofficephone").unbind("click");
    $(".a_removeofficephone").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_office_phone_count[current_account]===undefined)
            actual_office_phone_count[current_account] = 1;
        else
            actual_office_phone_count[current_account]--;
        $(this).parent('div').remove();
        //$("#"+remove_id).remove();
    });
    /* Office Phone ends */

    /* Fax */
    $(".a_fax").unbind("click");
    $(".a_fax").click(function(){
        var current_account = $(this).attr("rec_id");
        if(fax_count[current_account]===undefined)
            fax_count[current_account] = 2;
        else
            fax_count[current_account]++;

        if(actual_fax_count[current_account]===undefined)
            actual_fax_count[current_account] = 2;
        else
            actual_fax_count[current_account]++;

        if(actual_fax_count[current_account]==6){
            actual_fax_count[current_account] = 5;
            alert("You can not add more than 5 fax numbers");
            return;
        }
        var filtervars = {
            do_what:'add_fax',
            phone_count:fax_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/immovable-properties-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#MobileAddWrap"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removefax").unbind("click");
    $(".a_removefax").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_fax_count[current_account]===undefined)
            actual_fax_count[current_account] = 1;
        else
            actual_fax_count[current_account]--;
        $(this).parent('div').remove();
        //$("#"+remove_id).remove();
    });
    /* Fax ends */

    /* Mobile box */
    $(".a_mobile").unbind("click");
    $(".a_mobile").click(function(){
        var current_account = $(this).attr("rec_id");
        if(mobile_count[current_account]===undefined)
            mobile_count[current_account] = 2;
        else
            mobile_count[current_account]++;

        if(actual_mobile_count[current_account]===undefined)
            actual_mobile_count[current_account] = 2;
        else
            actual_mobile_count[current_account]++;

        if(actual_mobile_count[current_account]==4){
            actual_mobile_count[current_account] = 3;
            alert("You can not add more than 3 mobile numbers");
            return;
        }
        var filtervars = {
            do_what:'add_mobile',
            phone_count:mobile_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/immovable-properties-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#MobileAddWrap_"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removemobile").unbind("click");
    $(".a_removemobile").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_mobile_count[current_account]===undefined)
            actual_mobile_count[current_account] = 1;
        else
            actual_mobile_count[current_account]--;
        $(this).parent('div').remove();
        //$("#"+remove_id).remove();
    });
    /* Mobile box ends */

 
    /* Email box */
    $(".a_jah_email").unbind("click");
    $(".a_jah_email").click(function(){
        var current_account = $(this).attr("rec_id");
        if(email_count[current_account]===undefined)
            email_count[current_account] = 2;
        else
            email_count[current_account]++;

        if(actual_email_count[current_account]===undefined)
            actual_email_count[current_account] = 2;
        else
            actual_email_count[current_account]++;

        if(actual_email_count[current_account]==4){
            actual_email_count[current_account] = 3;
            alert("You can not add more than 3 emails");
            return;
        }
        var filtervars = {
            do_what:'add_jah_email',
            jah_email_count:email_count[current_account],
            account_count:current_account,
            auth_token: $("#auth_token_name").val()
        };

        $.ajax({
            type: "POST",
            url: "_ajax_files/immovable-properties-details_ajax.php",
            data: filtervars,
            dataType: "html",
            success: function(html){
                $("#JahEmailAddWrap_"+current_account).append(html);
                add_functions();
            }
        });
    });

    $(".a_removejahemail").unbind("click");
    $(".a_removejahemail").click(function(){
        var current_account = $(this).attr("rec_id");
        var remove_id = $(this).attr("remove_id");
        if(actual_email_count[current_account]===undefined)
            actual_email_count[current_account] = 1;
        else
            actual_email_count[current_account]--;
        $(this).parent('div').remove();
    });
    /* Email box ends */


    /* Beneficiary */
    $(".a_removebeneficiary").unbind("click");
    $(".a_removebeneficiary").click(function(){
        //alert(actual_beneficiary_count);
        if(actual_beneficiary_count==1){
            //actual_beneficiary_count++;
            alert("This is the default beneficary share and You cannot delete else you will need to define atleast one beneficary share.");
            return;
        }
        var remove_id = $(this).attr("remove_id");
        actual_beneficiary_count--;
        $("#div_beneficiary_"+remove_id).remove();

        $("#account_"+remove_id).remove();
        var temp = $('.a_removebeneficiary').length;
        temp --;
        
        $('#span_beneficiary_'+$('.a_removebeneficiary').eq(temp).attr('remove_id')).html('<a class="btn btn-primary btn-sm btn-add-large a_beneficiary" id="beneficiaryAddBtn" title="Add beneficiary" alt="Add beneficiary" href="javascript:void(0);">+ Add Beneficiary</a>');
        add_functions();
    });
    /* Beneficiary */

	$(".jah_is_adult").change(function(){
		var account_id = $(this).attr("ac_id");
		$('#show_ejd_birth_date_'+account_id).val("");
		$('#ejd_birth_date_'+account_id).val("");
		if($(this).val()=="1")
			$("#ejd_guardian_"+$(this).attr("ac_id")).html("");
		else{
			var ac_id = $(this).attr("ac_id");
            if($('input[name=guardian_id_'+ac_id+']').val() == 0 || $('input[name=guardian_id_'+ac_id+']').val() == undefined ){
                add_guardian($(this).attr("ac_id"));
            }else{
                $('#div_joint_account_holders #ejd_guardian_'+ac_id).html($('#div_joint_account_holders_hidden  #ejd_guardian_'+ac_id).html());
                add_guardian_functions(ac_id);
            }
		}
	});
	copy_address();
    change_jah_ben_numbering();
}

function add_date_functions(account_id)
{
	$('#show_ejd_birth_date_'+account_id).datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		minDate: "-150Y",
		//maxDate: new Date(),
		yearRange: "-150:+0",
		altField: "#ejd_birth_date_"+account_id,
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect: function (dateText, inst) {
			var startDate = new Date(dateText);
			var date = $(this).datepicker('getDate');
			var selectedYear = startDate.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
			if(diff > 100)
				alert('You have selected '+selectedYear+' as birth year');
			else if(diff < 18){
				add_guardian(account_id);
				$('#ejd_is_adult_'+account_id).val("0");
			}
			else{
				$('#ejd_is_adult_'+account_id).val("1");
				$('#ejd_guardian_'+account_id).html('');
			}
		}	
	});	
}

function add_date_functions(account_id)
{
	$('#show_ejd_birth_date_'+account_id).datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		minDate: "-150Y",
		//maxDate: new Date(),
		yearRange: "-150:+0",
		altField: "#ejd_birth_date_"+account_id,
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect: function (dateText, inst) {
			var startDate = new Date(dateText);
			var date = $(this).datepicker('getDate');
			var selectedYear = startDate.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
			if(diff > 100)
				alert('You have selected '+selectedYear+' as birth year');
			else if(diff < 18){
				add_guardian(account_id);
				$('#ejd_is_adult_'+account_id).val("0");
			}
			else{
				$('#ejd_is_adult_'+account_id).val("1");
				$('#ejd_guardian_'+account_id).html('');
			}
		}	
	});	
}

function add_date_functions_for_all(account_id){
    for(var i = 1; i <= account_id; i++){
        $('#show_ejd_birth_date_'+i).removeClass('hasDatepicker');
        add_date_functions(i);
    }
}

function add_guardian(account_id)
{
	var filtervars = {
		do_what:'add_guardian',
		account_id: account_id,
        auth_token: $("#auth_token_name").val()
	};
	$.ajax({
		type: "POST",
		url: "_ajax_files/immovable-properties-details_ajax.php",
		data: filtervars,
		dataType: "html",
		success: function(html){
			$('#ejd_guardian_'+account_id).html(html);
			$.validate({form:"#frmProperty", onSuccess:validate_immovable});
			$('.numeric').numeric();
			add_guardian_birth_date(account_id);
            add_guardian_functions(account_id);
		}
	});
}

function add_guardian_birth_date_for_all(account_id){
    for(var i=1;i<=account_id;i++)
    {
        $('#show_guardian_birth_date_'+i).removeClass('hasDatepicker');
        /*$('#show_guardian_birth_date_'+i).datepicker({
            dateFormat: 'dd/mm/yy',
            numberOfMonths: 1,
            //maxDate: "-18Y",
            yearRange: "-150:+0",
            altField: "#guardian_birth_date_"+i,
            altFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            onSelect: function (dateText, inst) {
                var startDate = new Date(dateText);
                var selectedYear = startDate.getFullYear();
                var today = new Date();
                var todayYear = today.getFullYear();
                var diff=  todayYear-selectedYear;
                if(diff > 100)
                    alert('You have selected '+selectedYear+' as birth year');
            }
        });*/
        add_guardian_birth_date(i);
    }
}

function add_guardian_birth_date(account_id){
    $('#show_guardian_birth_date_'+account_id).datepicker({
        dateFormat: 'dd/mm/yy',
        numberOfMonths: 1,
        //maxDate: "-18Y",
        yearRange: "-150:+0",
        altField: "#guardian_birth_date_"+account_id,
        altFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        onSelect: function (dateText, inst) {
            var date = $('#show_guardian_birth_date_'+account_id).datepicker('getDate');
            var today = new Date();
            var todayYear = today.getFullYear();
            var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));

            if(diff > 100)
                alert('You have selected '+selectedYear+' as birth year');

            if(diff>=18)
                $("#guardian_is_adult_"+account_id).prop("checked", true);
            else
                $("#guardian_is_adult_"+account_id).prop("checked", false);
        }
    });
}

function add_guardian_functions(account_id)
{
	is_foreign_address();
	/*$('#show_guardian_birth_date_'+account_id).datepicker({
		dateFormat: 'dd/mm/yy',
		numberOfMonths: 1,
		maxDate: "-18Y",
		yearRange: "-150:+0",
		altField: "#guardian_birth_date_"+account_id,
		altFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		onSelect: function (dateText, inst) {
			var startDate = new Date(dateText);
			var selectedYear = startDate.getFullYear();
			var today = new Date();
			var todayYear = today.getFullYear();
			var diff=  todayYear-selectedYear;
			if(diff > 100)
				alert('You have selected '+selectedYear+' as birth year');
		}
	});*/

	$(".grdnCorrespondenceAdd").click(function(){
		var acc_id = $(this).attr("account_id");
		if($(this).is(":checked")){

			//----------------------------------------------- changes 8-8-14 --------------------------------------------- //
			$('#guardian_correspondence_is_same_as_permanent_'+acc_id).prop('checked', $('#guardian_permanent_is_same_as_permanent_'+acc_id).prop('checked'));
			$('#guardian_correspondence_is_same_as_correspondence_'+acc_id).prop('checked', $('#guardian_permanent_is_same_as_correspondence_'+acc_id).prop('checked'));
			$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+acc_id).val($('#guardian_permanent_is_same_as_permanent_or_correspondence_'+acc_id).val());
			
			//----------------------------------------------- End changes 8-8-14 --------------------------------------------- //

			//----------------------------------------------- changes 10-8-14 --------------------------------------------- //
			$('#guardian_correspondence_address_line1_'+acc_id).val($('#guardian_permanent_address_line1_'+acc_id).val());
			$('#guardian_correspondence_address_line2_'+acc_id).val($('#guardian_permanent_address_line2_'+acc_id).val());
			$('#guardian_correspondence_address_line3_'+acc_id).val($('#guardian_permanent_address_line3_'+acc_id).val());
			$('#guardian_correspondence_city_village_town_'+acc_id).val($('#guardian_permanent_city_village_town_'+acc_id).val());
			$('#guardian_correspondence_country_'+acc_id).val($('#guardian_permanent_country_'+acc_id).val());
			$('#guardian_correspondence_state_'+acc_id).val($('#guardian_permanent_state_'+acc_id).val());

			$('#guardian_correspondence_address_line1_'+acc_id).prop('readonly', true);
			$('#guardian_correspondence_address_line2_'+acc_id).prop('readonly', true);
			$('#guardian_correspondence_address_line3_'+acc_id).prop('readonly', true);
			$('#guardian_correspondence_city_village_town_'+acc_id).prop('readonly', true);
			$('#guardian_correspondence_country_'+acc_id).prop('disabled', true);
			$('#guardian_correspondence_state_'+acc_id).prop('disabled', true);
			$('#guardian_correspondence_zipcode_'+acc_id).prop('readonly', true);
			$('#guardian_correspondence_state_other_'+acc_id).prop('readonly', true);

			$('#guardian_correspondence_is_same_as_permanent_'+acc_id).prop('disabled', true);
			$('#guardian_correspondence_is_same_as_correspondence_'+acc_id).prop('disabled', true);
			
			var checked;
			if($('#guardian_permanent_is_foreign_address_'+acc_id).prop('checked'))
				checked = 1;
			else
				checked = 0;

			populate_country_disable($('#guardian_permanent_country_'+acc_id).val(), "guardian_correspondence_country_"+acc_id, "span_guardian_correspondence_country_"+acc_id, checked, "");
			$('#guardian_correspondence_state_'+acc_id).val($('#guardian_permanent_state_'+acc_id).val());
			populate_states_disable($('#guardian_permanent_country_'+acc_id).val(), "guardian_correspondence_state_"+acc_id, "span_guardian_correspondence_state_"+acc_id,$('#guardian_permanent_state_'+acc_id).val(), "", "guardian_correspondence_state_"+acc_id);

		
			if($('#guardian_permanent_span_other_state_'+acc_id).is(':visible')){
                $('#guardian_correspondence_span_other_state_'+acc_id).show();
            }else{
                $('#guardian_correspondence_span_other_state_'+acc_id).hide();
            }

			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('checked',$('#guardian_permanent_is_foreign_address_'+acc_id).prop('checked'));
			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('disabled', true);
			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('readonly', true);
			
			$('#guardian_correspondence_is_foreign_address_'+acc_id).val($('#guardian_permanent_is_foreign_address_'+acc_id).val());
			$('#guardian_correspondence_zipcode_'+acc_id).val($('#guardian_permanent_zipcode_'+acc_id).val());
			$('#guardian_correspondence_state_other_'+acc_id).val($('#guardian_permanent_state_other_'+acc_id).val());
			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('disabled', true);
			if(checked == 1){
				$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation', 'length');
				$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation-length', '5-25');
				$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation-error-msg', 'Please enter valid Zipcode for joint account holder address');
			}
			else{
				$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation', 'length');
				$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation-length', '6-6');
				$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation-error-msg', 'Please enter valid Pincode for joint account holder address');
			}
			is_foreign_address();
			//----------------------------------------------- changes 10-8-14 --------------------------------------------- //

		}else{
			
			//----------------------------------------------- changes 8-8-14 --------------------------------------------- //
			$('#guardian_correspondence_is_same_as_permanent_'+acc_id).prop('checked', false);
			$('#guardian_correspondence_is_same_as_correspondence_'+acc_id).prop('checked', false);
			$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+acc_id).val('');
			
			//----------------------------------------------- End changes 8-8-14 --------------------------------------------- //


			//----------------------------------------------- changes 10-8-14 --------------------------------------------- //
			populate_country(102, "guardian_correspondence_country_"+acc_id, "span_guardian_correspondence_country_"+acc_id, 0, "");
			populate_states(102, "guardian_correspondence_state_"+acc_id, "span_guardian_correspondence_state_"+acc_id, "", "");
			$("#guardian_correspondence_state_"+acc_id).val('');
			$("#guardian_correspondence_state_"+acc_id).prop('selected', '');
			$('#guardian_correspondence_state_other_'+acc_id).val('');
			$('#guardian_correspondence_span_other_state_'+acc_id).attr('style', 'display:none;');
			$('#guardian_correspondence_zipcode_'+acc_id).prop('maxlength', '6');
			$('#guardian_correspondence_zipcode_'+acc_id).addClass('numeric');
			$('.numeric').numeric();

			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('disabled', false);
			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('checked', false);
			$('#guardian_correspondence_address_line1_'+acc_id).val('');
			$('#guardian_correspondence_address_line1_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_address_line2_'+acc_id).val('');
			$('#guardian_correspondence_address_line2_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_address_line3_'+acc_id).val('');
			$('#guardian_correspondence_address_line3_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_city_village_town_'+acc_id).val('');
			$('#guardian_correspondence_city_village_town_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_zipcode_'+acc_id).val('');

			$('#guardian_correspondence_country_'+acc_id).prop('disbaled', false);
			$('#guardian_correspondence_state_'+acc_id).prop('disbaled', false);

			$('#guardian_correspondence_is_same_as_permanent_'+acc_id).prop('disabled', false);
			$('#guardian_correspondence_is_same_as_correspondence_'+acc_id).prop('disabled', false);
			$('#guardian_correspondence_is_foreign_address_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_zipcode_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_state_other_'+acc_id).prop('readonly', false);
			$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation', '');
			$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation-length', '');
			$('#guardian_correspondence_zipcode_'+acc_id).attr('data-validation-error-msg', '');

			$('#guardian_correspondence_is_same_as_permanent_'+acc_id).prop('disabled', $('#guardian_permanent_is_same_as_permanent_'+acc_id).prop('disabled'));
			$('#guardian_correspondence_is_same_as_correspondence_'+acc_id).prop('disabled', $('#guardian_permanent_is_same_as_correspondence_'+acc_id).prop('disabled'));
			is_foreign_address();
			//----------------------------------------------- End changes 10-8-14 --------------------------------------------- //
		}
	});

	copy_guardian_address();
	copy_guardian_correspondence_address();
}

function check_minor_guardian(account_id)
{

    var startDate = new Date($("#show_guardian_birth_date_"+account_id).val());
    if(startDate!=""){
        var date = $('#show_guardian_birth_date_'+account_id).datepicker('getDate');
        var selectedYear = startDate.getFullYear();
        var today = new Date();
        var todayYear = today.getFullYear();
        var diff=Math.floor((today - date) / (1000 * 60 * 60 * 24 *365.25));
        if((diff < 18 && $('#guardian_is_adult_'+account_id).is(':checked')) || (diff > 18 && !$('#guardian_is_adult_'+account_id).is(':checked'))){

            $("#show_guardian_birth_date_"+account_id).val("");
            $("#guardian_birth_date_"+account_id).val("");
        }
    }
}

function copy_address()
{
	$('.same_per_cor_address').unbind("click");
	$('.same_per_cor_address').click(function(){
		var click_account_id = $(this).attr("account_count");
		
		var for_address = $(this).attr("for_address");
		if($(this).val()=="P"){
			if($('#is_same_as_permanent_'+click_account_id).is(":checked")){
				$('#is_same_as_correspondence_'+click_account_id).prop('checked', false);
				//$('#is_same_as_permanent_'+click_account_id).prop('disabled', false);
				$('#is_same_as_permanent_or_correspondence_'+click_account_id).val('P');
				var filtervars = {
					do_what:'populate_address',
					address_value:"P",
					address_for:"jointaccountholder",
					account_id:click_account_id,
                	auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#populate_'+for_address+'_address_'+click_account_id).html(html);
                        $.validate({form:"#frmProperty", onSuccess:validate_immovable});
						add_functions();
					}
				});
			}
			else {
				
				//$('#is_same_as_correspondence_'+click_account_id).prop('disabled', false);		
				$('#is_same_as_permanent_'+click_account_id).prop('checked', false);
				$('#is_same_as_permanent_or_correspondence_'+click_account_id).val('');
				
				
				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#ejd_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#ejd_is_foreign_address_'+click_account_id).prop('checked', false);
				$('#ejd_address_line1_'+click_account_id).val('');
				$('#ejd_address_line1_'+click_account_id).prop('readonly', false);
				$('#ejd_address_line2_'+click_account_id).val('');
				$('#ejd_address_line2_'+click_account_id).prop('readonly', false);
				$('#ejd_address_line3_'+click_account_id).val('');
				$('#ejd_address_line3_'+click_account_id).prop('readonly', false);
				$('#ejd_city_village_town_'+click_account_id).val('');
				$('#ejd_city_village_town_'+click_account_id).prop('readonly', false);
				$('#ejd_zipcode_'+click_account_id).val('');
				$('#ejd_zipcode_'+click_account_id).prop('readonly', false);

				$('#ejd_country_'+click_account_id).prop('disbaled', false);
				
				populate_country(102, "ejd_country_"+click_account_id, "span_ejd_country_"+click_account_id, 0, "");
				populate_states(102, "ejd_state_"+click_account_id, "span_ejd_state_"+click_account_id, "", "");
				$("#ejd_state_"+click_account_id).val('');
				$("#ejd_state_"+click_account_id).prop('selected', '');
				$('#ejd_state_other_'+click_account_id).val('');
				$('#ejd_state_other_'+click_account_id).prop('readonly', false);
				$('#span_ejd_state_other_'+click_account_id).attr('style', 'display:none;');
				$('#ejd_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#ejd_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- //
				
			}
		}
		else if($(this).val()=="C")
		{
			if($('#is_same_as_correspondence_'+click_account_id).is(":checked")){
				$('#is_same_as_permanent_'+click_account_id).prop('checked', false);
				//$('#is_same_as_correspondence_'+click_account_id).prop('disabled', false);
				$('#is_same_as_permanent_or_correspondence_'+click_account_id).val('C');
				var filtervars = {
					do_what:'populate_address',
					address_value:"C",
					address_for:"jointaccountholder",
					account_id:click_account_id,
                	auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#populate_'+for_address+'_address_'+click_account_id).html(html);
                        $.validate({form:"#frmProperty", onSuccess:validate_immovable});
						add_functions();
					}
				});
			}
			else {
				//$('#is_same_as_permanent_'+click_account_id).prop('disabled', false);		
				$('#is_same_as_correspondence_'+click_account_id).prop('checked', false);
				$('#is_same_as_permanent_or_correspondence_'+click_account_id).val('');

				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#ejd_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#ejd_is_foreign_address_'+click_account_id).prop('checked', false);
				$('#ejd_address_line1_'+click_account_id).val('');
				$('#ejd_address_line1_'+click_account_id).prop('readonly', false);
				$('#ejd_address_line2_'+click_account_id).val('');
				$('#ejd_address_line2_'+click_account_id).prop('readonly', false);
				$('#ejd_address_line3_'+click_account_id).val('');
				$('#ejd_address_line3_'+click_account_id).prop('readonly', false);
				$('#ejd_city_village_town_'+click_account_id).val('');
				$('#ejd_city_village_town_'+click_account_id).prop('readonly', false);
				$('#ejd_zipcode_'+click_account_id).val('');
				$('#ejd_zipcode_'+click_account_id).prop('readonly', false);

				$('#ejd_country_'+click_account_id).prop('disbaled', false);
				
				populate_country(102, "ejd_country_"+click_account_id, "span_ejd_country_"+click_account_id, 0, "");
				populate_states(102, "ejd_state_"+click_account_id, "span_ejd_state_"+click_account_id, "", "");
				$("#ejd_state_"+click_account_id).val('');
				$("#ejd_state_"+click_account_id).prop('selected', '');
				$('#ejd_state_other_'+click_account_id).val('');
				$('#span_ejd_state_other_'+click_account_id).attr('style', 'display:none;');
				$('#ejd_state_other_'+click_account_id).prop('readonly', false);
				$('#ejd_state_other_'+click_account_id).prop('disabled', false);
				$('#ejd_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#ejd_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- //
			}
		}
	});	
}


function copy_guardian_address()
{
	$('.guardian_same_per_address').click(function(){
		
		var click_account_id = $(this).attr("account_count");
		var for_address = $(this).attr("for_address");
		

		if($(this).val()=="P"){
			clear_is_same_as_guardian(click_account_id);
			if($('#guardian_permanent_is_same_as_permanent_'+click_account_id).is(":checked")){
				$('#guardian_permanent_is_same_as_correspondence_'+click_account_id).prop('checked', false);
				//$('#guardian_permanent_is_same_as_permanent_'+click_account_id).prop('disabled', false);
				$('#guardian_permanent_is_same_as_permanent_or_correspondence_'+click_account_id).val('P');
				var filtervars = {
					do_what:'populate_address',
					address_value:"P",
					address_for:"guardianpermanent",
					account_id:click_account_id,
                	auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#guardian_populate_'+for_address+'_address_'+click_account_id).html(html);
						add_functions();
					}
				});
			}
			else {
				clear_is_same_as_guardian(click_account_id);
				//$('#guardian_permanent_is_same_as_correspondence_'+click_account_id).prop('disabled', false);		
				$('#guardian_permanent_is_same_as_permanent_'+click_account_id).prop('checked', false);
				$('#guardian_permanent_is_same_as_permanent_or_correspondence_'+click_account_id).val('');

				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#guardian_permanent_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#guardian_permanent_is_foreign_address_'+click_account_id).prop('checked', false);
				$('#guardian_permanent_address_line1_'+click_account_id).val('');
				$('#guardian_permanent_address_line2_'+click_account_id).val('');
				$('#guardian_permanent_address_line3_'+click_account_id).val('');
				$('#guardian_permanent_city_village_town_'+click_account_id).val('');
				$('#guardian_permanent_zipcode_'+click_account_id).val('');

				$('#guardian_permanent_address_line1_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_address_line2_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_address_line3_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_city_village_town_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_zipcode_'+click_account_id).prop('readonly', false);

				$('#guardian_permanent_country_'+click_account_id).prop('disbaled', false);
				
				populate_country(102, "guardian_permanent_country_"+click_account_id, "span_guardian_permanent_country_"+click_account_id, 0, "");
				populate_states(102, "guardian_permanent_state_"+click_account_id, "span_guardian_permanent_state_"+click_account_id, "", "");
				$("#guardian_permanent_state_"+click_account_id).val('');
				$("#guardian_permanent_state_"+click_account_id).prop('selected', '');
				$('#guardian_permanent_state_other_'+click_account_id).val('');
				$('#guardian_permanent_state_other_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_span_other_state_'+click_account_id).attr('style', 'display:none;');
				$('#guardian_permanent_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#guardian_permanent_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- // 
			}
		}
		else if($(this).val()=="C")
		{
			clear_is_same_as_guardian(click_account_id);
			if($('#guardian_permanent_is_same_as_correspondence_'+click_account_id).is(":checked")){
				$('#guardian_permanent_is_same_as_permanent_'+click_account_id).prop('checked', false);
				//$('#guardian_permanent_is_same_as_correspondence_'+click_account_id).prop('disabled', false);
				$('#guardian_permanent_is_same_as_permanent_or_correspondence_'+click_account_id).val('C');
				var filtervars = {
					do_what:'populate_address',
					address_value:"C",
					address_for:"guardianpermanent",
					account_id:click_account_id,
                	auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#guardian_populate_'+for_address+'_address_'+click_account_id).html(html);
                        $.validate({form:"#frmProperty", onSuccess:validate_immovable});
						add_functions();
					}
				});
			}
			else {
				clear_is_same_as_guardian(click_account_id);
				//$('#guardian_permanent_is_same_as_permanent_'+click_account_id).prop('disabled', false);		
				$('#guardian_permanent_is_same_as_correspondence_'+click_account_id).prop('checked', false);
				$('#guardian_permanent_is_same_as_permanent_or_correspondence_'+click_account_id).val('');

				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#guardian_permanent_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#guardian_permanent_is_foreign_address_'+click_account_id).prop('checked', false);
				
				$('#guardian_permanent_address_line1_'+click_account_id).val('');
				$('#guardian_permanent_address_line2_'+click_account_id).val('');
				$('#guardian_permanent_address_line3_'+click_account_id).val('');
				$('#guardian_permanent_city_village_town_'+click_account_id).val('');
				$('#guardian_permanent_zipcode_'+click_account_id).val('');

				$('#guardian_permanent_address_line1_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_address_line2_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_address_line3_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_city_village_town_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_zipcode_'+click_account_id).prop('readonly', false);

				$('#guardian_permanent_country_'+click_account_id).prop('disbaled', false);
				
				populate_country(102, "guardian_permanent_country_"+click_account_id, "span_guardian_permanent_country_"+click_account_id, 0, "");
				populate_states(102, "guardian_permanent_state_"+click_account_id, "span_guardian_permanent_state_"+click_account_id, "", "");
				$("#guardian_permanent_state_"+click_account_id).val('');
				$("#guardian_permanent_state_"+click_account_id).prop('selected', '');
				$('#guardian_permanent_state_other_'+click_account_id).val('');
				$('#guardian_permanent_state_other_'+click_account_id).prop('readonly', false);
				$('#guardian_permanent_span_other_state_'+click_account_id).attr('style', 'display:none;');
				$('#guardian_permanent_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#guardian_permanent_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- // 
			}
		}
	});	

}


function copy_guardian_correspondence_address()
{
	$('.guardian_same_cor_address').click(function(){
		var click_account_id = $(this).attr("account_count");
		var for_address = $(this).attr("for_address");
		if($(this).val()=="P"){
			if($('#guardian_correspondence_is_same_as_permanent_'+click_account_id).is(":checked")){
				$('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('checked', false);
				//$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('disabled', false);
				$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+click_account_id).val('P');
				var filtervars = {
					do_what:'populate_address',
					address_value:"P",
					address_for:"guardiancorrespondence",
					account_id:click_account_id,
                	auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#guardian_populate_'+for_address+'_address_'+click_account_id).html(html);
                        $.validate({form:"#frmProperty", onSuccess:validate_immovable});
						add_functions();
					}
				});
			}
			else {
				//$('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('disabled', false);		
				$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('checked', false);
				//$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('disabled', false);
				$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+click_account_id).val('');

				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#guardian_correspondence_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#guardian_correspondence_is_foreign_address_'+click_account_id).prop('checked', false);
				
				$('#guardian_correspondence_address_line1_'+click_account_id).val('');
				$('#guardian_correspondence_address_line1_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_address_line2_'+click_account_id).val('');
				$('#guardian_correspondence_address_line2_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_address_line3_'+click_account_id).val('');
				$('#guardian_correspondence_address_line3_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_city_village_town_'+click_account_id).val('');
				$('#guardian_correspondence_city_village_town_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_zipcode_'+click_account_id).val('');
				$('#guardian_correspondence_zipcode_'+click_account_id).prop('readonly', false);
				
				$('#guardian_correspondence_country_'+click_account_id).prop('disbaled', false);
				populate_country(102, "guardian_correspondence_country_"+click_account_id, "span_guardian_correspondence_country_"+click_account_id, 0, "");
				populate_states(102, "guardian_correspondence_state_"+click_account_id, "span_guardian_correspondence_state_"+click_account_id, "", "");
				$("#guardian_correspondence_state_"+click_account_id).val('');
				$("#guardian_correspondence_state_"+click_account_id).prop('selected', '');
				$('#guardian_correspondence_state_other_'+click_account_id).val('');
				$('#guardian_correspondence_state_other_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_span_other_state_'+click_account_id).attr('style', 'display:none;');
				$('#guardian_correspondence_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#guardian_correspondence_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- // 
			}
		}
		else if($(this).val()=="C")
		{
			if($('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).is(":checked")){
				$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('checked', false);
				//$('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('disabled', false);
				$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+click_account_id).val('C');
				var filtervars = {
					do_what:'populate_address',
					address_value:"C",
					address_for:"guardiancorrespondence",
					account_id:click_account_id,
                	auth_token: $("#auth_token_name").val()
				};
				$.ajax({
					type: "POST",
					url: "_ajax_files/get_address_ajax.php",
					data: filtervars,
					dataType: "html",
					success: function(html){
						$('#guardian_populate_'+for_address+'_address_'+click_account_id).html(html);
                        $.validate({form:"#frmProperty", onSuccess:validate_immovable});
						add_functions();
					}
				});
			}
			else {
				//$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('disabled', false);		
				$('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('checked', false);
				//$('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('disabled', false);
				$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+click_account_id).val('');

				//------------------------------------- Changes 10/08/14 ----------------------------------------- //
				$('#guardian_correspondence_is_foreign_address_'+click_account_id).prop('disabled', false);
				$('#guardian_correspondence_is_foreign_address_'+click_account_id).prop('checked', false);
				
				$('#guardian_correspondence_address_line1_'+click_account_id).val('');
				$('#guardian_correspondence_address_line1_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_address_line2_'+click_account_id).val('');
				$('#guardian_correspondence_address_line2_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_address_line3_'+click_account_id).val('');
				$('#guardian_correspondence_address_line3_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_city_village_town_'+click_account_id).val('');
				$('#guardian_correspondence_city_village_town_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_zipcode_'+click_account_id).val('');
				$('#guardian_correspondence_zipcode_'+click_account_id).prop('readonly', false);
				
				$('#guardian_correspondence_country_'+click_account_id).prop('disbaled', false);
				populate_country(102, "guardian_correspondence_country_"+click_account_id, "span_guardian_correspondence_country_"+click_account_id, 0, "");
				populate_states(102, "guardian_correspondence_state_"+click_account_id, "span_guardian_correspondence_state_"+click_account_id, "", "");
				$("#guardian_correspondence_state_"+click_account_id).val('');
				$("#guardian_correspondence_state_"+click_account_id).prop('selected', '');
				$('#guardian_correspondence_state_other_'+click_account_id).val('');
				$('#guardian_correspondence_state_other_'+click_account_id).prop('readonly', false);
				$('#guardian_correspondence_span_other_state_'+click_account_id).attr('style', 'display:none;');
				$('#guardian_correspondence_zipcode_'+click_account_id).prop('maxlength', '6');
				$('#guardian_correspondence_zipcode_'+click_account_id).addClass('numeric');
				$('.numeric').numeric();
				is_foreign_address();
				//------------------------------------- End Changes 10/08/14 ----------------------------------------- // 
			}
		}
	});	
}

function clear_is_same_as_guardian(click_account_id)
{
	if($('#grdnCorrespondenceAdd_'+click_account_id).is(':checked')){
			$('#grdnCorrespondenceAdd_'+click_account_id).prop('checked', false);
			
			$('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('disabled', false);		
            $('#guardian_correspondence_is_same_as_correspondence_'+click_account_id).prop('checked', false);   
			$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('checked', false);
			$('#guardian_correspondence_is_same_as_permanent_'+click_account_id).prop('disabled', false);
			$('#guardian_correspondence_is_same_as_permanent_or_correspondence_'+click_account_id).val('');

			//------------------------------------- Changes 10/08/14 ----------------------------------------- //
			$('#guardian_correspondence_is_foreign_address_'+click_account_id).prop('disabled', false);
			$('#guardian_correspondence_is_foreign_address_'+click_account_id).prop('checked', false);
			
			$('#guardian_correspondence_address_line1_'+click_account_id).val('');
			$('#guardian_correspondence_address_line1_'+click_account_id).prop('readonly', false);
			$('#guardian_correspondence_address_line2_'+click_account_id).val('');
			$('#guardian_correspondence_address_line2_'+click_account_id).prop('readonly', false);
			$('#guardian_correspondence_address_line3_'+click_account_id).val('');
			$('#guardian_correspondence_address_line3_'+click_account_id).prop('readonly', false);
			$('#guardian_correspondence_city_village_town_'+click_account_id).val('');
			$('#guardian_correspondence_city_village_town_'+click_account_id).prop('readonly', false);
			$('#guardian_correspondence_zipcode_'+click_account_id).val('');
			$('#guardian_correspondence_zipcode_'+click_account_id).prop('readonly', false);
			
			$('#guardian_correspondence_country_'+click_account_id).prop('disbaled', false);
			populate_country(102, "guardian_correspondence_country_"+click_account_id, "span_guardian_correspondence_country_"+click_account_id, 0, "");
			populate_states(102, "guardian_correspondence_state_"+click_account_id, "span_guardian_correspondence_state_"+click_account_id, "", "");
			$("#guardian_correspondence_state_"+click_account_id).val('');
			$("#guardian_correspondence_state_"+click_account_id).prop('selected', '');
			$('#guardian_correspondence_state_other_'+click_account_id).val('');
			$('#guardian_correspondence_state_other_'+click_account_id).prop('readonly', false);
			$('#guardian_correspondence_span_other_state_'+click_account_id).attr('style', 'display:none;');
			$('#guardian_correspondence_zipcode_'+click_account_id).prop('maxlength', '6');
			$('#guardian_correspondence_zipcode_'+click_account_id).addClass('numeric');
			$('.numeric').numeric();
			is_foreign_address();
		}
}





function validate_immovable(){
		$("select").prop("disabled", false); 
		$("input").prop("readonly", false); 
		$("input").prop("disabled", false); 
		
		//--------------------------- changes 8-8-14 -------------------------- //
		
		$('#end_address_line1').prop('readonly', false);
		$('#end_address_line2').prop('readonly', false);
		$('#end_address_line3').prop('readonly', false);
		$('#end_city_village_town').prop('readonly', false);
		$('#end_state_other').prop('readonly', false);
		$('#end_zipcode').prop('readonly', false);
		$('input[name=end_state_other]').prop('readonly', false);
		$('#end_country').prop('disabled', false);
		$('#end_state').prop('disabled', false);
		$('#end_is_foreign_address').prop('disabled', false);



		$('#end_guardian_address_line1').prop('readonly', false);
		$('#end_guardian_address_line2').prop('readonly', false);
		$('#end_guardian_address_line3').prop('readonly', false);
		$('#end_guardian_city_village_town').prop('readonly', false);
		$('#end_guardian_state_other').prop('readonly', false);
		$('#end_guardian_zipcode').prop('readonly', false);
		$('input[name=end_guardian_state_other]').prop('readonly', false);
		$('#end_guardian_country').prop('disabled', false);
		$('#end_guardian_state').prop('disabled', false);
		$('#end_guardian_is_foreign_address').prop('disabled', false);
		

		

		return true;

		//--------------------------- End changes 8-8-14 -------------------------- //
	}