// Beneficiary script start
// $(document).ready(function() {
//     //$.validate();


//     // $('#chkcorrespondence').checked()
// // alert("hi");
//     $("#chkcorrespondence").click(function() {
//         alert("hi");
//         if(!$(this).is(":checked"))
//         alert('you are unchecked ' + $(this).val());
//     });

//     $("#test").click(function() {
//         alert("hi");
//     }) 




//     var permanent_state = '0';
//     var permanent_addr_copied_from = $('#permanent_addr_copied_from').val();

//     if (permanent_addr_copied_from == "P" || permanent_addr_copied_from == "C") {
//         populate_states_disable($("#permanent_country").val(), "permanent_state", "span_permanent_state", permanent_state, "", "permanent_state");

//     } else {
//         if ($("#permanent_country").val() > 0)
//             populate_states($("#permanent_country").val(), "permanent_state", "span_permanent_state", permanent_state, "");
//     }


//     var correspondence_addr_copied_from = $('#correspondence_addr_copied_from').val();
//     var chkcorrespondence = $('#chkcorrespondence').is(':checked');
//     if (correspondence_addr_copied_from == "P" || correspondence_addr_copied_from == "C" || chkcorrespondence == true) {
//         populate_states_disable($("#correspondence_country").val(), "correspondence_state", "span_correspondence_state", "0", "", "correspondence_state");

//     } else {
//         if ($("#correspondence_country").val() > 0)
//             populate_states($("#correspondence_country").val(), "correspondence_state", "span_correspondence_state", "0", "");
//     }


//     var guardian_permanent_addr_copied_from = $('#guardian_permanent_addr_copied_from').val();
//     if (guardian_permanent_addr_copied_from == "P" || guardian_permanent_addr_copied_from == "C") {
//         populate_states_disable($("#guardian_permanent_country").val(), "guardian_permanent_state", "span_guardian_permanent_state", "6", "", "guardian_permanent_state");
//     } else {
//         if ($("#guardian_permanent_country").val() > 0)
//             populate_states($("#guardian_permanent_country").val(), "guardian_permanent_state", "span_guardian_permanent_state", "6", "");
//     }

//     var guardian_correspondence_addr_copied_from = $('#guardian_correspondence_addr_copied_from').val();
//     var grdnCorrespondenceAdd = $('#grdnCorrespondenceAdd').is(':checked');

//     if (guardian_correspondence_addr_copied_from == "P" || guardian_correspondence_addr_copied_from == "C" || grdnCorrespondenceAdd == true) {
//         populate_states_disable($("#guardian_correspondence_country").val(), "guardian_correspondence_state", "span_guardian_correspondence_state", "6", "", "guardian_correspondence_state");
//     } else {
//         if ($("#guardian_correspondence_country").val() > 0) {
//             populate_states($("#guardian_correspondence_country").val(), "guardian_correspondence_state", "span_guardian_correspondence_state", "6", "");
//         }
//     }
// });

// $(document).ready(function() {
//     $.validate({ form: "#frmBeneficiary", onSuccess: validate_beneficiary });
// });
// $('#btn-update').click(function() {
//     $.validate({ form: "#frmBeneficiary", onSuccess: validate_beneficiary });
// });
// $('#btn-add').click(function() {
//     $.validate({ form: "#frmBeneficiary", onSuccess: validate_beneficiary });
// });

// Beneficiary script end


// Executor details script
$(document).ready(function() {
    var dowhat = 'other';
    if ($("#permanent_country").val() > 0) {
        if (dowhat != 'other') {

            populate_states_disable($("#permanent_country").val(), "permanent_state", "span_permanent_state", "0", "", "permanent_state");
            populate_states_disable($("#correspondence_country").val(), "correspondence_state", "span_correspondence_state", "0", "", "correspondence_state");
            //alert('hiii');
        } else {
            $('#show_birth_date').datepicker({
                dateFormat: 'dd/mm/yy',
                numberOfMonths: 1,
                minDate: "-150Y",
                //maxDate: "-18Y",
                yearRange: "-150:+0",
                altField: "#birth_date",
                altFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true,
                onSelect: function(dateText, inst) {
                    var startDate = new Date(dateText);
                    var selectedYear = startDate.getFullYear();
                    var today = new Date();
                    var todayYear = today.getFullYear();
                    var diff = todayYear - selectedYear;
                    if (diff > 100)
                        alert('You have selected ' + selectedYear + ' as birth year');
                }
            });

            var permanent_state = '0';
            var permanent_addr_copied_from = $('#permanent_addr_copied_from').val();

            if (permanent_addr_copied_from == "P" || permanent_addr_copied_from == "C") {
                populate_states_disable($("#permanent_country").val(), "permanent_state", "span_permanent_state", permanent_state, "", "permanent_state");

            } else {
                if ($("#permanent_country").val() > 0) {
                    populate_states($("#permanent_country").val(), "permanent_state", "span_permanent_state", permanent_state, "toggle_other_state('permanent_state', 'permanent_span_other_state')");
                }
            }

            var correspondence_state = '0';
            var correspondence_addr_copied_from = $('#correspondence_addr_copied_from').val();
            var is_correspondence_same_as_permanent = $('#is_correspondence_same_as_permanent').is(':checked');

            if (correspondence_addr_copied_from == "P" || correspondence_addr_copied_from == "C" || is_correspondence_same_as_permanent == "true") {
                populate_states_disable($("#correspondence_country").val(), "correspondence_state", "span_correspondence_state", correspondence_state, "", "correspondence_state");

            } else {
                if ($("#correspondence_country").val() > 0)
                    populate_states($("#correspondence_country").val(), "correspondence_state", "span_correspondence_state", correspondence_state, "toggle_other_state('correspondence_state', 'correspondence_span_other_state')");
            }
            //populate_states($("#permanent_country").val(), "permanent_state", "span_permanent_state","0");
            //populate_states($("#correspondence_country").val(), "correspondence_state", "span_correspondence_state","0");
        }

    }
});

$(document).ready(function() {
    // Bank Account Lockers
    $('#show_ejd_birth_date_1').datepicker({
        dateFormat: 'dd/mm/yy',
        numberOfMonths: 1,
        minDate: "-150Y",
        //maxDate: new Date(),
        yearRange: "-150:+0",
        altField: "#ejd_birth_date_1",
        altFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        onSelect: function(dateText, inst) {
            var startDate = new Date(dateText);
            var date = $(this).datepicker('getDate');
            var selectedYear = startDate.getFullYear();
            var today = new Date();
            var todayYear = today.getFullYear();
            var diff = Math.floor((today - date) / (1000 * 60 * 60 * 24 * 365.25));
            if (diff > 100)
                alert('You have selected ' + selectedYear + ' as birth year');
            else if (diff < 18) {
                var ac_id = 1;
                if ($('input[name=guardian_id_' + ac_id + ']').val() == 0 || $('input[name=guardian_id_' + ac_id + ']').val() == undefined) {
                    add_guardian(1);
                    $('#ejd_is_adult_1').val("0");
                } else {
                    $('#ejd_is_adult_1').val("0");
                    $('#div_joint_account_holders  #ejd_guardian_' + ac_id).html($('#div_joint_account_holders_hidden #ejd_guardian_' + ac_id).eq(0).html());
                    add_guardian_functions(ac_id);
                }
            } else {
                $('#ejd_is_adult_1').val("1");
                $('#ejd_guardian_1').html('');
            }
        }
    });

    // Bank Account Lockers benificary

    $('#show_ejd_birth_date_1').datepicker({
        dateFormat: 'dd/mm/yy',
        numberOfMonths: 1,
        minDate: "-150Y",
        //maxDate: new Date(),
        yearRange: "-150:+0",
        altField: "#ejd_birth_date_1",
        altFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        onSelect: function(dateText, inst) {
            var startDate = new Date(dateText);
            var date = $(this).datepicker('getDate');
            var selectedYear = startDate.getFullYear();
            var today = new Date();
            var todayYear = today.getFullYear();
            var diff = Math.floor((today - date) / (1000 * 60 * 60 * 24 * 365.25));
            if (diff > 100)
                alert('You have selected ' + selectedYear + ' as birth year');
            else if (diff < 18) {
                var ac_id = 1;
                if ($('input[name=guardian_id_' + ac_id + ']').val() == 0 || $('input[name=guardian_id_' + ac_id + ']').val() == undefined) {
                    add_guardian(1);
                    $('#ejd_is_adult_1').val("0");
                } else {
                    $('#ejd_is_adult_1').val("0");
                    $('#div_joint_account_holders  #ejd_guardian_' + ac_id).html($('#div_joint_account_holders_hidden #ejd_guardian_' + ac_id).eq(0).html());
                    add_guardian_functions(ac_id);
                }
            } else {
                $('#ejd_is_adult_1').val("1");
                $('#ejd_guardian_1').html('');
            }
        }
    });
});
// Bank Account Lockers script end
// 
// 
// 
// Mutual fund det script start
account_count = 1;
actual_account_count = 1;
beneficiary_count = 1;
actual_beneficiary_count = 1;

$(window).load(function() {
    for (var i = 1; i <= account_count; i++) {
        var temp = $('#actual_phone_count_' + i).val();
        actual_phone_count[i] = $('#actual_phone_count_' + i).val();
        actual_office_phone_count[i] = $('#actual_office_count_' + i).val();
        actual_mobile_count[i] = $('#actual_mobile_count_' + i).val();
        actual_email_count[i] = $('#actual_email_count_' + i).val();
        actual_fax_count[i] = $('#actual_fax_count_' + i).val();
    }
});







$(document).ready(function() {
    $.validate({ form: "#frmBankAccount", onSuccess: validate_stocksbonds });
});
$('#btn-update').click(function() {
    $.validate({ form: "#frmBankAccount", onSuccess: validate_stocksbonds });
});
$('#btn-add').click(function() {
    $.validate({ form: "#frmBankAccount", onSuccess: validate_stocksbonds });
});
// Mutual fund det script end

// Bond detail script start here