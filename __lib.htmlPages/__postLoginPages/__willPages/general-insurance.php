<?php
	include_once("../../../__lib.includes/config.inc.php");
	$wgi_data		= $willProfile->getWillDetails('will_general_insurance');
	$bene_data_all	= $willProfile->getWillBenDetails('will_beneficiary');
	
?>
<form action="data_submit/frmForm.php" name="" id="" method="post" class="frmCurrent">
    <input type="hidden" name="form_id" value="9">
    <div class="agreement-row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="bnkacAddWrap">
                        <div>
                            <!-- do not delte this div -->
                            <h3>Policy Details(General insurance)</h3>
                            <div class="panel panel-body sub-panel">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Type of policy <span class="mandatory_star">*</span></label>
                                        <br>
                                        <input type="text" class="form-control" name="policy_type" value="<?= $wgi_data->gi_pol_type; ?>" placeholder="Name of the Insurance Company" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Name of the Insurance Company" title="Please enter Name of the Insurance Company" alt="Please enter Name of the Insurance Company" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Policy Number <span class="mandatory_star">*</span></label><br>
                                        <input type="text" class="form-control" name="policy_number" value="<?= $wgi_data->gi_pol_num; ?>" placeholder="Policy Number" maxlength="20" title="Please enter Policy number" alt="Please enter Policy number" data-validation="" data-validation-length="1-20" data-validation-error-msg="Please enter valid Policy number" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Company name <span class="mandatory_star">*</span></label><br>
                                        <input type="text" class="form-control" name="gi_com_name" value="<?= $wgi_data->gi_com_name; ?>" placeholder="Policy Number" maxlength="20" title="Please enter Policy number" alt="Please enter Policy number" data-validation="" data-validation-length="1-20" data-validation-error-msg="Please enter valid Policy number" required/>
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div class="row">
                                    
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Start Date <span class="mandatory_star">*</span></label><br>
                                        <input type="date" id="policy_start_date" name="policy_start_date" class="form-control" value="<?= $wgi_data->gi_start_date; ?>" placeholder="Policy Start Date" title="Please select Policy Start Date" alt="Please select Policy Start Date" data-validation="" data-validation-length="1-20" data-validation-error-msg="Please select valid Policy Start Date" required/>
                                        <!-- <input type="hidden" name="start_date" id="policy_start_date" class="form-control" value="0000-00-00" /> -->
                                        <!-- <input type="hidden" name="birth_date" id="birth_date" class="form-control" value="1997-03-26" /> -->
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory">Maturity Date </label>
                                        <br>
                                        <!-- <label class="mandatory mandatory_label">Policy Maturity Date <span class="mandatory_star">*</span></label><br> -->
                                        <input type="date" id="maturity_date" name="maturity_date" class="form-control" value="<?= $wgi_data->gi_mat_date; ?>" placeholder="Policy Maturity Date" title="Please select Policy Maturity Date" alt="Please select Policy Maturity Date" data-validation="" data-validation-length="10-10" data-validation-error-msg="Please select valid Policy Maturity Date" />
                                        <!-- <input type="hidden" name="maturity_date" id="maturity_date" class="form-control" value="0000-00-00" placeholder="Maturity date" /> -->
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Amount <span class="mandatory_star">*</span></label><br>
                                        <input type="text" class="form-control" name="gi_amount" value="<?= $wgi_data->gi_amount; ?>" placeholder="Policy Number" maxlength="20" title="Please enter Policy number" alt="Please enter Policy number" data-validation="" data-validation-length="1-20" data-validation-error-msg="Please enter valid Policy number" required/>
                                    </div>
                                </div>
                                <!-- row END -->
                                <div class="divider"></div>
                            </div>
                            <!-- panel-body END -->
                        </div>
                    </div>
                </div>
                <!-- col-xs-12 END -->
            </div>
            <!-- row END -->
            <input type="hidden" name="has_nominee" value="no">
            <div class="row">
                <div class="col-xs-12">
                    <div id="guardiant_main">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="div_beneficiary">
                        <div id="div_beneficiary_1">
                            <h4>
										<strong>Beneficiaries</strong>
									</h4>
                            <div class="panel panel-body sub-panel">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Beneficiary <span class="mandatory_star">*</span></label>
                                        <select name="sel_beneficiary" id="sel_beneficiary" class="input-select form-control" title='Please select a Beneficiary' alt='Please select a Beneficiary'>
                                            <option value="">Please select</option>
                                            <?php 
                                                $cnt=1;
                                                while($row = mysql_fetch_array($bene_data_all)) {
                                                    $html = "<option value='" . $cnt . "'>" .$row['f_name'] . "</option>";
                                                    
                                                    $cnt+= 1;
                                                    echo $html;
                                                } 
                                                ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Percentage Share to be allotted <span class="mandatory_star">*</span></label>
                                        <input type="text" class="form-control numeric_share" name="txt_beneficiary_share" id="txt_beneficiary_share" placeholder="Percentage Share to be allotted" maxlength="5" title="Please enter Percentage Share to be allotted" alt="Please enter Percentage Share to be allotted" value="" />
                                    </div>
                                </div>
                                <div>
                                    <br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add Beneficiary" title="Add Beneficiary" id="beneficiaryAddBtnNew" class="btn btn-primary btn-sm btn-add-large a_beneficiarynew">+ Add Beneficiary</a></span></div>
                                <br/>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Relation</th>
                                            <th>Share</th>
                                            <th><a href="" class="btn btn-danger">Reset all</a></th>
                                        </tr>
                                    </thead>
                                    <tbody id="tb_con_ben">
                                        <tr id="no_beneficiary">
                                            <td colspan="5"></td>
                                        </tr>
                                        <input type="hidden" name="tot_ben" id="tot_ben" value="1" />
                                        <input type="hidden" id="total_share" value="0">
                                    </tbody>
                                </table>
                            </div>
                            <div>
<!--                                 <br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add More Beneficiary" title="Add More Beneficiary" id="" class="btn btn-success btn-sm">+ Add More</a></span> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">&nbsp;</div>
            <div class="bottom_buttons row">
                <div class="div_prev_button">
                    <input type="button" name="previous" id="btn-previous" value="&laquo; Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="pension-fund" />
                </div>
                <div class="div_center_buttons">
                    <!-- <input type="submit" name="add_lic" id="btn-add" value="Add / Save" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " /> -->
                    <!-- <input type="submit" name="update" id="btn-update" value="Update" title="Click here to Update the changed data" alt="Click here to Update the changed data" class="btn-submit btn-submit-disabled" disabled/> -->
                </div>
                <div class="div_next_button" style="margin-right: -30px; text-align: right;">
                    <input type="submit" name="add_gi" id="btn-add" value="Save & Next »" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " />
							<!-- <input type="button" name="next" id="btn-next" value="Next &raquo;" title="Click here to go to Next page" alt="Click here to go to Next page" class="btn-submit btn-move" next_page="other-assets-details"/> -->
						</div><br><br><br>
            </div>
            <div class="row">
                <!-- <input type="submit" name="sumbit" value="Submit" class="btn-submit btn-submit-disabled" title="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" alt="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" disabled/>		 -->
            </div>
        </div>
    </div>
</form>