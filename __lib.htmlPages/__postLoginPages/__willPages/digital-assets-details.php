<?php

	include_once("../../../__lib.includes/config.inc.php");
	$wda_data		= $willProfile->getWillDetails('will_digital_assets');
	$bene_data_all	= $willProfile->getWillBenDetails('will_beneficiary');
	
?>
<form action="data_submit/frmForm.php" name="frmDigitalasset" id="frmDigitalasset" method="post" class="frmCurrent has-validation-callback">
    <div class="agreement-row">
        <input type="hidden" name="form_id" value="11">
        <div class="container">
            <!-- Listing STARTS -->
            <!-- Listing END -->
            <h3>Add IPR</h3>
            <div class="row">
                <div class="col-xs-12">
                    <div id="bnkacAddWrap">
                        <div>
                            <!-- do not delte this div -->
                            <div class="panel panel-body">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Type of IPR <span class="mandatory_star">*</span></label>
                                        <?php $selected=$wda_data->dig_asset_type; ?>
                                        <select name="asset_type" id="asset_type" class="input-select form-control" title="Please select Type of asset" alt="Please select Type of asset" data-validation="length" data-validation-length="1-3" data-validation-error-msg="Please select Type of Asset" required>
                                            <option value="" <?php echo ($selected=="" ) ? "selected" : "" ; ?> >Please select</option>
                                            <option value="1" <?php echo ($selected==1 ) ? "selected" : "" ; ?> >Digital Photograph</option>
                                            <option value="2" <?php echo ($selected==2 ) ? "selected" : "" ; ?> >Software Code</option>
                                            <option value="3" <?php echo ($selected==3 ) ? "selected" : "" ; ?> >Software Patent</option>
                                            <option value="4" <?php echo ($selected==4 ) ? "selected" : "" ; ?> >Others</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Amount <span class="mandatory_star">*</span></label>
                                        <input class="form-control numeric_share" name="amount" id="amount" placeholder="Percentage Share to be allotted" maxlength="5" title="Please enter Percentage Share to be allotted" alt="Please enter Percentage Share to be allotted" value="<?= $wda_data->dig_asset_amount; ?>" type="text" required>
                                    </div>
                                    <div class="col-xs-4">
                                        <span id="span_other_asset" style="display:none;">
                                        <label class="mandatory  mandatory_label">In case of Others (If not listed) 
                                        <!-- <span class="mandatory_star">*</span> -->
                                        </label>
                                        <input class="form-control" name="others" id="others" value="" maxlength="60" placeholder="In case of Others" title="Please enter other Digital Asset type" alt="Please enter other Digital Asset type" type="text">
                                        </span>
                                    </div>
                                </div>
                                <div class="divider"></div>
                            </div>
                            <!-- panel-body END -->
                            <div class="row">&nbsp;</div>
                            <div id="div_beneficiary">
                                <div id="div_beneficiary_1">
                                    <h4>
                  <strong>Beneficiaries</strong>
                </h4>
                                    <div class="panel panel-body sub-panel">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <label class="mandatory mandatory_label">Beneficiary <span class="mandatory_star">*</span></label>
                                                <select name="sel_beneficiary" id="sel_beneficiary" class="input-select form-control" title="Please select a Beneficiary" alt="Please select a Beneficiary">
                                                    <option value="">Please select</option>
                                                    <?php 
                                                $cnt=1;
                                                while($row = mysql_fetch_array($bene_data_all)) {
                                                    $html = "<option value='" . $cnt . "'>" .$row['f_name'] . "</option>";
                                                    
                                                    $cnt+= 1;
                                                    echo $html;
                                                } 
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-4">
                                                <label class="mandatory mandatory_label">Percentage Share to be allotted <span class="mandatory_star">*</span></label>
                                                <input class="form-control numeric_share" name="txt_beneficiary_share" id="txt_beneficiary_share" placeholder="Percentage Share to be allotted" maxlength="5" title="Please enter Percentage Share to be allotted" alt="Please enter Percentage Share to be allotted" value="" type="text">
                                            </div>
                                            <!-- <div class="col-xs-4">
                                                <label class="mandatory">Any Other Information</label>
                                                <input class="form-control" name="txt_other_info" id="txt_other_info" maxlength="200" placeholder="Any Other Information" title="Please enter Any Other Information" alt="Please enter Any Other Information" value="" type="text">
                                            </div> -->
                                        </div>
                                        <div>
                                            <br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add Beneficiary" title="Add Beneficiary" id="beneficiaryAddBtnNew" class="btn btn-primary btn-sm btn-add-large a_beneficiarynew">+ Add Beneficiary</a></span></div>
                                        <br>
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Relation</th>
                                                    <th>Share</th>
                                                    <th><a href="" class="btn btn-danger">Reset all</a></th>
                                                </tr>
                                            </thead>
                                            <tbody id="tb_con_ben">
                                                <tr id="no_beneficiary">
                                                    <td colspan="5"></td>
                                                </tr>
                                                <input name="tot_ben" id="tot_ben" value="1" type="hidden">
                                                <input id="total_share" value="0" type="hidden">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div>
<!--                                         <br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add More Beneficiary" title="Add More Beneficiary" id="" class="btn btn-success btn-sm">+ Add More</a></span> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- row END -->
                        <div class="row">&nbsp;</div>
                        <div class="bottom_buttons row">
                            <div class="div_prev_button">
                                <input name="previous" id="btn-previous" value="« Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="bank-account-details" type="button">
                            </div>
                            <div class="div_center_buttons">
                                <!-- <input name="add_digital_assets" id="btn-add" value="Add / Save" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " type="submit"> -->
                                <!-- <input name="update" id="btn-update" value="Update" title="Click here to Update the changed data" alt="Click here to Update the changed data" class="btn-submit btn-submit-disabled" disabled="" type="submit"> -->
                            </div>
                            <div class="div_next_button" style="margin-right: -30px; text-align: right;">
                                <input name="add_digital_assets" id="btn-add" value="Save & Next »" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " type="submit">
                                <!-- <input name="next" id="btn-next" value="Next »" title="Click here to go to Next page" alt="Click here to go to Next page" class="btn-submit btn-move" next_page="fixed-deposit-details" type="button"> -->
                                <br>
                                <br>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                        <!-- <div class="row">
                            <input name="txt_other_submit" id="txt_other_submit" value="0" type="hidden">
                            <input name="txt_redirect_page" id="txt_redirect_page" value="" type="hidden">
                            <input name="sumbit" value="Submit" class="btn-submit btn-submit-disabled" title="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" alt="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" disabled="" type="submit"> </div>
                        <div class="row">&nbsp;</div> -->
                    </div>
                </div>
</form>