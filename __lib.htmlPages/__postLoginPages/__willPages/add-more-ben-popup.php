<?php
	$bene_data		= $willProfile->getWillDetails('will_beneficiary');
?>
<!-- Modal -->
  <div class="modal fade" id="moreBeneficiary" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">One More Beneficiary</h4>
        </div>
        <div class="modal-body">
          
    <div class="agreement-row">
        <div>
            <div class="container">
                <!-- Listing END -->
                <form name="frmBeneficiary" id="frmBeneficiary" action="data_submit/frmForm.php" method="post" class="frmCurrent has-validation-callback">
                    <div class="row">
                        <div class="col-xs-9">
                            <div id="bnfcryAddWrap">
                                <!-- do not delte this div -->
                                <div class="panel panel-body">
                                    <div class="row">
                                        <div id="individual_main">
                                            <div class="divider"></div>
                                            <label class="mandatory mandatory_label">Name of the Beneficiary <span class="mandatory_star">*</span></label>
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <select name="title" id="title" class="input-select form-control" title="Please select appropriate title from the dropdown list. Drop-down of the standard Titles normally used in writing Will" alt="Please select appropriate title from the dropdown list. Drop-down of the standard Titles normally used in writing Will" data-validation="length" data-validation-length="1-20" data-validation-error-msg="Please select a Title" onchange="javascript: title_change();" required>
                                                        <option value="" <?=( $bene_data->title =="" )?"selected":"" ?>>Title</option>
                                                        <option value="1" <?=( $bene_data->title ==1 )?"selected":"" ?>>Mr.</option>
                                                        <option value="2" <?=( $bene_data->title ==2)?"selected":"" ?>>Mrs.</option>
                                                        <option value="3" <?=( $bene_data->title ==3 )?"selected":"" ?>>Ms.</option>
                                                        <option value="10" <?=( $bene_data->title ==10 )?"selected":"" ?>>Master</option>
                                                        <option value="23" <?=( $bene_data->title ==23 )?"selected":"" ?>>Kumar</option>
                                                        <option value="24" <?=( $bene_data->title ==24 )?"selected":"" ?>>Kumari</option>
                                                    </select>
                                                </div>
                                                <div class="col-xs-3">
                                                    <input class="form-control" name="firstname" id="firstname" value="<?= $bene_data->f_name ?>" placeholder="First Name" maxlength="33" title="Only alphabets are allowed. Abbreviations and initials to be avoided" alt="Only alphabets are allowed. Abbreviations and initials to be avoided" type="text">
                                                </div>
                                                <div class="col-xs-3">
                                                    <input class="form-control" name="middlename" id="middlename" value="<?= $bene_data->m_name ?>" placeholder="Middle Name" maxlength="33" title="Only alphabets are allowed" alt="Only alphabets are allowed" type="text">
                                                </div>
                                                <div class="col-xs-3">
                                                    <input class="form-control" name="lastname" id="lastname" value="<?= $bene_data->l_name ?>" placeholder="Last Name / Surname" maxlength="33" title="Only alphabets and single apphostophe (') is allowed as special characters. For e.g. D'Silva, D'Souza. If you have a single name, please enter here" alt="Only alphabets and single apphostophe (') is allowed as special characters. For e.g. D'Silva, D'Souza. If you have a single name, please enter here" data-validation="length" data-validation-length="2-33" data-validation-error-msg="Please enter valid Last Name / Surname" type="text">
                                                </div>
                                            </div>
                                            <div class="divider"></div>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label class="cnd-mandatory mandatory_label">Date of Birth <span class="mandatory_star">*</span></label>
                                                    <input id="show_birth_date" name="birth_date" class="form-control hasDatepicker" value="<?= $bene_data->date_of_birth ?>" placeholder="Birth date" title="Please select Date of Birth. Click inside the box to view calender and select the date" alt="Please select Date of Birth. Click inside the box to view calender and select the date" type="date" required>
                                                    <!-- <input name="birth_date" id="birth_date" class="form-control" value="<?= $bene_data->date_of_birth ?>" placeholder="Birth date" type="hidden"> -->
                                                </div>
                                                <div class="col-xs-2">
                                                    <label class="cnd-mandatory mandatory_label">Age <span class="mandatory_star">*</span>
                                                    </label>
                                                    <input type="text" id="ben_age" name="ben_age" class="form-control" value="<?= $bene_data->ben_age; ?>" placeholder="Age" title='Age' alt='Age' required/>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label class="mandatory_label">Relationship with <strong><a href="#" data-toggle="modal" data-target="#testator">testator</a></strong> <span class="mandatory_star">*</span></label>
                                                    <br>
                                                    <select name="relationship_with_testator" id="relationship_with_testator" class="input-select form-control" title="Select beneficiary’s relationship with the Testator from the drop-down list. For e.g. If Testator is defining his son as beneficiary, select ‘Relationship with testator’ as ‘Son’" alt="Select beneficiary’s relationship with the Testator from the drop-down list. For e.g. If Testator is defining his son as beneficiary, select ‘Relationship with testator’ as ‘Son’" data-validation="length" data-validation-length="1-3" data-validation-error-msg="Please select a relationship" onchange="javascript: toggle_other_relation('relationship_with_testator', 'span_beneficary_relationship');" required>
                                                        <option value="" selected>Please select</option>
                                                        <option value="2" <?=( $bene_data->rel_with_testator == 2)? "selected":"" ?>>Spouse</option>
                                                        <option value="3" <?=( $bene_data->rel_with_testator == 3)? "selected":"" ?>>Son</option>
                                                        <option value="4" <?=( $bene_data->rel_with_testator == 4)? "selected":"" ?>>Daughter</option>
                                                        <option value="5" <?=( $bene_data->rel_with_testator == 5)? "selected":"" ?>>Mother</option>
                                                        <option value="6" <?=( $bene_data->rel_with_testator == 6)? "selected":"" ?>>Father</option>
                                                        <option value="7" <?=( $bene_data->rel_with_testator == 7)? "selected":"" ?> >Brother</option>
                                                        <option value="8" <?=( $bene_data->rel_with_testator == 8)? "selected":"" ?>>Sister</option>
                                                        <option value="19" <?=( $bene_data->rel_with_testator == 19)? "selected":"" ?>>Grand Daughter</option>
                                                        <option value="20" <?=( $bene_data->rel_with_testator == 20)? "selected":"" ?>>Grandson</option>
                                                        <option value="21" <?=( $bene_data->rel_with_testator == 21)? "selected":"" ?>>Grand Father</option>
                                                        <option value="22" <?=( $bene_data->rel_with_testator == 22)? "selected":"" ?>>Grand Mother</option>
                                                        <option value="23" <?=( $bene_data->rel_with_testator == 23)? "selected":"" ?>>Son-in-Law</option>
                                                        <option value="24" <?=( $bene_data->rel_with_testator == 24)? "selected":"" ?>>Daughter-in-law</option>
                                                        <option value="99" <?=( $bene_data->rel_with_testator == 99)? "selected":"" ?>>Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- row END -->
                                </div>
                                <!-- panel-body END -->
                                <h4><strong>Permanent Address</strong></h4>
                                <div class="panel panel-body">
                                    <div id="individual_main_perm_add">
                                        <!-- <div class="row">
                                            <div class="col-xs-6">
                                                &nbsp;&nbsp;&nbsp;
                                                <input class="same_per_address" name="permanent_is_same_as_permanent" value="P" id="permanent_is_same_as_permanent" account_count="" for_address="permanent" title="Select if is this Address same as your Permanent Address?" alt="Select if is this Address same as your Permanent Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
                                                <label class="mandatory">Is this Address same as your Permanent Address1?</label>
                                            </div>
                                            <div class="col-xs-6">
                                                &nbsp;&nbsp;&nbsp;
                                                <input class="same_per_address" name="permanent_is_same_as_correspondence" value="C" id="permanent_is_same_as_correspondence" account_count="" for_address="permanent" title="Select if is this Address same as your Correspondence Address?" alt="Select if is this Address same as your Correspondence Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
                                                <label class="mandatory">Is this Address same as your Correspondence Address?</label>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none;">
                                            <div class="col-xs-6">
                                                &nbsp;&nbsp;&nbsp;
                                                <input name="permanent_addr_copied_from" id="permanent_addr_copied_from" value="" type="hidden">
                                            </div>
                                        </div>
                                        <div class="divider"></div> -->
                                        <div id="populate_permanent_address">
                                            <div class="divider"></div>
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <label class="mandatory mandatory_label">Address Line1 <span class="mandatory_star">*</span></label>
                                                    <input class="form-control" name="permanent_address_line1" id="permanent_address_line1" value="<?= $bene_data->per_addr_line1; ?>" placeholder="Address Line1" maxlength="60" title="Please enter Flat / Floor / Door / Block Number as applicable" alt="Please enter Flat / Floor / Door / Block Number as applicable" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1 for permanent address" type="text" required>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label class="mandatory mandatory_label">Address Line2 <span class="mandatory_star">*</span></label>
                                                    <input class="form-control" name="permanent_address_line2" id="permanent_address_line2" value="<?= $bene_data->per_addr_line2; ?>" placeholder="Address Line2" maxlength="60" title="Please enter Name of Premises / Building of Permanent Address" alt="Please enter Name of Premises / Building of Permanent Address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2 for permanent address" type="text" required>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label>Address Line3</label>
                                                    <input class="form-control" name="permanent_address_line3" id="permanent_address_line3" value="<?= $bene_data->per_addr_line3; ?>" placeholder="Address Line3" maxlength="60" title="Please enter name of Road / Street / Lane as applicable " alt="Please enter name of Road / Street / Lane as applicable " type="text">
                                                </div>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label class="mandatory_label">City / Village / Town <span class="mandatory_star">*</span></label>
                                                    <input name="permanent_city_village_town" id="permanent_city_village_town" class="form-control" value="<?= $bene_data->per_city; ?>" placeholder="City / Village / Town" maxlength="60" title="Please enter name of City / Village / Town as applicable" alt="Please enter name of City / Village / Town as applicable" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town for permanent address" type="text" required>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label class="mandatory mandatory_label">State <span class="mandatory_star">*</span></label>
                                                    <span id="span_permanent_state">
                                                        <?php $corre_state=$bene_data->per_state; ?>
                                                        <select name="permanent_state" id="permanent_state" class="input-select form-control" title="Please select appropriate State" alt="Please select appropriate State" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please select a valid State" required>
                                                        <option value="35" <?php echo ($corre_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($corre_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($corre_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($corre_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($corre_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($corre_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($corre_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($corre_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($corre_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($corre_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($corre_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($corre_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($corre_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($corre_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($corre_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($corre_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($corre_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($corre_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($corre_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($corre_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($corre_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($corre_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($corre_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($corre_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($corre_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($corre_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($corre_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($corre_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($corre_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($corre_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($corre_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($corre_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($corre_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($corre_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($corre_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($corre_state == 19)?"selected":"" ?>>West Bengal</option>
                                                    </select></span>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label class="mandatory mandatory_label">Pincode / Zipcode <span class="mandatory_star">*</span></label>
                                                    <input class="form-control  numeric" name="permanent_zipcode" id="permanent_zipcode" placeholder="Pincode" value="<?= $bene_data->per_zip; ?>" data-validation="length" maxlength="6" data-validation-length="6-6" data-validation-error-msg="Please enter valid Pincode / Zipcode for permanent address" title="Please enter PIN Code for India if Permanent Address is in India, Zip Code for address outside India" alt="Please enter PIN Code for India if Permanent Address is in India, Zip Code for address outside India" type="text" required>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label class="mandatory mandatory_label">Country <span class="mandatory_star">*</span></label>
                                                    <span id="span_permanent_country">
                                            <select name="permanent_country" id="permanent_country" class="input-select form-control select-country-list" title="Please select appropriate Country for permanent Address" alt="Please select appropriate Country for permanent Address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please select a valid Country for permanent address" other_state_txt="permanent_state_other" onchange="javascript: populate_states(this.value,'permanent_state', 'span_permanent_state', '102', 'toggle_other_state(\'permanent_state\', \'span_permanent_state_other\')'); toggle_other_state_country(this.value, 'span_permanent_state_other', {'state_id':''}); ;" required><option value="102" selected="selected">India</option></select>                                         </span>
                                                </div>
                                                <div class="col-xs-3">
                                                    <span id="span_permanent_state_other" style="display:none;" validatemsg="Please enter other State for Permanent address">
                                            <label class="cnd-mandatory mandatory_label">Other State (If not listed) <span class="mandatory_star">*</span></label>
                                                    <input name="permanent_state_other" id="permanent_state_other" class="form-control" value="" placeholder="Other state" maxlength="60" title="Please enter appropriate State for Permanent Address, if country is other than India" alt="Please enter appropriate State for Permanent Address, if country is other than India" type="text">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Permanent Address END -->
                                    </div>
                                </div>
                                <h4>
                                <strong>Correspondence  Address</strong>
                                <span class="mandatory">Is the Correspondence Address same as Permanent Address?
                                <input id="chkcorrespondence" name="is_correspondence_same_as_permanent" value="1" title="Select the checkbox if Correspondence Address is same as Permanent  Address" alt="Select the checkbox if Correspondence Address is same as Permanent  Address" type="checkbox">
                                </span>
                            </h4>
                                <div class="panel panel-body correspondenceAdd">
                                    <div class="row" style="display:none">
                                        <div class="col-xs-6" >
                                            &nbsp;&nbsp;&nbsp;
                                            <input class="same_cor_address" name="correspondence_is_same_as_permanent" value="P" id="correspondence_is_same_as_permanent" account_count="" for_address="correspondence" title="Select if is this Address same as your Permanent Address?" alt="Select if is this Address same as your Permanent Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
                                            <label class="mandatory">Is this Address same as your Permanent Address?</label>
                                        </div>
                                        <div class="col-xs-6">
                                            &nbsp;&nbsp;&nbsp;
                                            <input class="same_cor_address" name="correspondence_is_same_as_correspondence" value="C" id="correspondence_is_same_as_correspondence" account_count="" for_address="correspondence" title="Select if is this Address same as your Correspondence Address?" alt="Select if is this Address same as your Correspondence Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
                                            <label class="mandatory">Is this Address same as your Correspondence Address?</label>
                                        </div>
                                    </div>
                                    <div class="row" style="display:none;">
                                        <div class="col-xs-6">
                                            &nbsp;&nbsp;&nbsp;
                                            <input name="correspondence_addr_copied_from" id="correspondence_addr_copied_from" value="" type="hidden">
                                        </div>
                                    </div>
                                    <div class="divider"></div>
                                    <div id="populate_correspondence_address">
                                        <div class="divider"></div>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <label class="mandatory mandatory_label">Address Line1 <span class="mandatory_star">*</span></label>
                                                <input class="form-control" id="correspondence_address_line1" name="correspondence_address_line1" placeholder="Address Line1" value="<?= $bene_data->corre_addr_line1 ; ?>" maxlength="60" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1 for correspondence address" title="Please enter Flat / Floor / Door / Block Number as applicable" alt="Please enter Flat / Floor / Door / Block Number as applicable" type="text" required>
                                            </div>
                                            <div class="col-xs-4">
                                                <label class="mandatory mandatory_label">Address Line2 <span class="mandatory_star">*</span></label>
                                                <input class="form-control" id="correspondence_address_line2" name="correspondence_address_line2" placeholder="Street name" value="<?= $bene_data->corre_addr_line2; ?>" maxlength="60" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2 for correspondence address" title="Please enter Name of Premises / Building of correspondence Address" alt="Please enter Name of Premises / Building of correspondence Address" type="text" required>
                                            </div>
                                            <div class="col-xs-4">
                                                <label>Address Line3</label>
                                                <input class="form-control" id="correspondence_address_line3" name="correspondence_address_line3" maxlength="60" placeholder="Address Line3" value="<?= $bene_data->corre_addr_line3; ?>" title="Please enter name of Road / Street / Lane as applicable " alt="Please enter name of Road / Street / Lane as applicable " type="text" required>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label class="mandatory_label">City / Village / Town <span class="mandatory_star">*</span></label>
                                                <input class="form-control" id="correspondence_city_village_town" name="correspondence_city_village_town" placeholder="City / Village / Town" value="<?= $bene_data->corre_city; ?>" maxlength="60" title="Please enter name of City / Village / Town as applicable" alt="Please enter name of City / Village / Town as applicable" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town for correspondence address" type="text" required>
                                            </div>
                                            <div class="col-xs-2">
                                                <label class="mandatory mandatory_label">State <span class="mandatory_star">*</span></label>
                                                <span id="span_correspondence_state">
                                                    <?php $curr_state=$bene_data->corre_state; ?>
                                    <select name="correspondence_state" id="correspondence_state" class="input-select form-control" title='Please select appropriate State for Permanent Address' alt='Please select appropriate State for Permanent Address' data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please select a valid State for permanent address" onchange="javascript: toggle_other_state('correspondence_state', 'span_other_state');"  required>
                                        <option value="35" selected <?php echo ($curr_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($curr_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($curr_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($curr_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($curr_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($curr_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($curr_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($curr_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($curr_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($curr_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($curr_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($curr_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($curr_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($curr_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($curr_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($curr_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($curr_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($curr_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($curr_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($curr_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($curr_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($curr_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($curr_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($curr_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($curr_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($curr_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($curr_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($curr_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($curr_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($curr_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($curr_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($curr_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($curr_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($curr_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($curr_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($curr_state == 19)?"selected":"" ?>>West Bengal</option>
                                    </select>
                                                </span>
                                            </div>
                                            <div class="col-xs-3">
                                                <span id="span_correspondence_state_other" style="display:none;" validatemsg="Please enter other State for correspondence address">
                                            <label class="cnd-mandatory mandatory_label">Other State (If not listed) 
                                                <!-- <span class="mandatory_star">*</span> -->
                                            </label>
                                                <input class="form-control" id="correspondence_state_other" name="correspondence_state_other" placeholder="Other State" value="" title="Please enter appropriate State for Correspondence Address, if country is other than India" alt="Please enter appropriate State for Correspondence Address, if country is other than India" maxlength="60" type="text">
                                                </span>
                                            </div>
                                            <div class="col-xs-2">
                                                <label class="mandatory mandatory_label">Pincode / Zipcode <span class="mandatory_star">*</span></label>
                                                <input name="correspondence_zipcode" id="correspondence_zipcode" class="form-control numeric" value="<?= $bene_data->corre_zip; ?>" placeholder="Pincode" data-validation="length" maxlength="6" data-validation-length="6-6" data-validation-error-msg="Please enter valid Pincode / Zipcode for correspondence address" title="Please enter PIN Code for India if Correspondence Address is in India, Zip Code for address outside India" alt="Please enter PIN Code for India if Correspondence Address is in India, Zip Code for address outside India" type="text" required>
                                            </div>
                                            <div class="col-xs-2">
                                                <label class="mandatory mandatory_label">Country <span class="mandatory_star">*</span></label>
                                                <span id="span_correspondence_country">
                                        <select name="correspondence_country" id="correspondence_country" class="input-select form-control select-country-list" title="Please select appropriate Country for Correspondence Address" alt="Please select appropriate Country for Correspondence Address" other_state_txt="correspondence_state_other" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please select a valid Country for correspondence address" onchange="javascript: populate_states(this.value,'correspondence_state', 'span_correspondence_state', '102', 'toggle_other_state(\'correspondence_state\', \'span_correspondence_state_other\')'); toggle_other_state_country(this.value, 'span_correspondence_state_other', {'state_id':''}); ;" required><option value="102" selected="selected">India</option></select>                                      </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Permanent Address END -->
                                </div>
                            </div>
                            <!-- panel-body END -->
                            <!-- *** Note for Developer: ***
                                Don't show below code if beneficiary age is above 18
                             -->
                            <div id="guardiant_main">
                                <div class="grdn-details" id="fdJoinGuardiantAddWrap">
                                    <h3>Details of Guardian</h3>
                                    <p class="note">
                                        Note: It is nnly required to fill below information under these two conditions:
                                        <br> 1. If the Beneficiary is less than 18 years of age, the details of the Guardian have to be captured.
                                        <br> 2. Incase of mentally incapacitated, details of Guardian have to be captured.
                                    </p>
                                    <div class="panel panel-body sub-panel">
                                        <div class="row">
                                            <div class="col-xs-9">
                                                <label class="mandatory mandatory_label">Name of Guardian </label>
                                                <div class="row">
                                                    <div class="col-xs-2">
                                                        <select name="guardian_title" id="guardian_title" class="input-select form-control required" title="Please select appropriate title from the dropdown list. Drop-down of the standard Titles normally used in writing Will" alt="Please select appropriate title from the dropdown list. Drop-down of the standard Titles normally used in writing Will" data-validation="length" data-validation-length="1-20" data-validation-error-msg="Please select a Title" onchange="javascript: title_change();">
                                                            <option value="" <?=( $bene_data->gd_title =="" )?"selected":"" ?>>Title</option>
                                                            <option value="1" <?=( $bene_data->gd_title ==1 )?"selected":"" ?>>Mr.</option>
                                                            <option value="2" <?=( $bene_data->gd_title ==2)?"selected":"" ?>>Mrs.</option>
                                                            <option value="3" <?=( $bene_data->gd_title ==3 )?"selected":"" ?>>Ms.</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <input name="guardian_firstname" id="guardian_firstname" value="<?= $bene_data->gd_f_name; ?>" placeholder="First Name" class="form-control" maxlength="33" title="Only alphabets are allowed. Abbreviations and initials to be avoided" alt="Only alphabets are allowed. Abbreviations and initials to be avoided" type="text">
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <input name="guardian_middlename" id="guardian_middlename" value="<?= $bene_data->gd_m_name; ?>" placeholder="Middle Name" class="form-control" maxlength="33" title="Only alphabets are allowed" alt="Only alphabets are allowed" type="text">
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <input name="guardian_lastname" id="guardian_lastname" value="<?= $bene_data->gd_l_name; ?>" placeholder="Last Name / Surname" class="form-control" maxlength="33" title="Only alphabets and single apphostophe (') is allowed as special characters. For e.g. D'Silva, D'Souza. If you have a single name, please enter here" alt="Only alphabets and single apphostophe (') is allowed as special characters. For e.g. D'Silva, D'Souza. If you have a single name, please enter here" data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please enter valid guardian Last Name / Surname" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="row">
                                            <div class="col-xs-9">
                                                <label class="mandatory mandatory_label">Guardian's Father Name </label>
                                                <div class="row">
                                                    <div class="col-xs-2">
                                                        <select name="guardian_fat_title" id="guardian_fat_title" class="input-select form-control required" title="Please select appropriate title from the dropdown list. Drop-down of the standard Titles normally used in writing Will" alt="Please select appropriate title from the dropdown list. Drop-down of the standard Titles normally used in writing Will" data-validation="length" data-validation-length="1-20" data-validation-error-msg="Please select a Title" onchange="javascript: title_change();" >
                                                            <option value="" <?=( $bene_data->gd_fat_title =="" )?"selected":"" ?>>Title</option>
                                                            <option value="1" <?=( $bene_data->gd_fat_title ==1 )?"selected":"" ?>>Mr.</option>
                                                            <option value="2" <?=( $bene_data->gd_fat_title ==2)?"selected":"" ?>>Mrs.</option>
                                                            <option value="3" <?=( $bene_data->gd_fat_title ==3 )?"selected":"" ?>>Ms.</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <input name="guardian_fat_firstname" id="guardian_fat_firstname" value="<?= $bene_data->gd_fat_f_name; ?>" placeholder="First Name" class="form-control" maxlength="33" title="Only alphabets are allowed. Abbreviations and initials to be avoided" alt="Only alphabets are allowed. Abbreviations and initials to be avoided" type="text">
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <input name="guardian_fat_middlename" id="guardian_fat_middlename" value="<?= $bene_data->gd_fat_m_name; ?>" placeholder="Middle Name" class="form-control" maxlength="33" title="Only alphabets are allowed" alt="Only alphabets are allowed" type="text">
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <input name="guardian_fat_lastname" id="guardian_fat_lastname" value="<?= $bene_data->gd_fat_l_name; ?>" placeholder="Last Name / Surname" class="form-control" maxlength="33" title="Only alphabets and single apphostophe (') is allowed as special characters. For e.g. D'Silva, D'Souza. If you have a single name, please enter here" alt="Only alphabets and single apphostophe (') is allowed as special characters. For e.g. D'Silva, D'Souza. If you have a single name, please enter here" data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please enter valid guardian Last Name / Surname" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="row">
                                            <div class="col-xs-9">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <label >Date of Birth </label>
                                                        <!-- <input id="show_guardian_birth_date" class="form-control hasDatepicker" value="" placeholder="Guardian Birth date" title="Please select Date of Birth. Click inside the box to view calender and select the date" alt="Please select Date of Birth. Click inside the box to view calender and select the date" type="text"> -->
                                                        <input name="guardian_birth_date" id="guardian_birth_date" class="form-control" value="<?= $bene_data->gd_d_of_b; ?>" placeholder="Guardian Birth date" type="date" >
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <label >Age </label>
                                                        <input type="text" id="per_info_age" name="gd_age" class="form-control" value="<?= $bene_data->gd_age; ?>" placeholder="Age" title='Age' alt='Age' />
                                                    </div>
                                                    <div class="col-xs-3 reg-date">
                                                        <label>OR </label> &nbsp;
                                                        <input name="guardian_is_adult" id="guardian_is_adult" value="1" onclick="check_minor_guardian()" title="If you don't remember Date of Birth, please select if Guardian is adult" alt="If you don't remember Date of Birth, please select if Guardian is adult" type="checkbox"> <span class="mandatory mandatory_label"> Guardian Is Adult</span>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <label >Gender </label>
                                                        <?php $selected_gender=$bene_data->gd_gender; ?>
                                                        <select name="guardian_gender" id="guardian_gender" class="input-select form-control" title="Please select appropriate gender from the dropdown list" alt="Please select appropriate gender from the dropdown list" data-validation="length" data-validation-length="1-20" data-validation-error-msg="Please enter valid guardian Gender">
                                                            <option value="" <?php echo ($selected_gender == "")?"selected":""; ?> >Please select</option>
                                                            <option value="1" <?php echo ($selected_gender == 1)?"selected":""; ?> >Male</option>
                                                            <option value="2" <?php echo ($selected_gender == 2)?"selected":""; ?> >Female</option>
                                                            <option value="3" <?php echo ($selected_gender == 3)?"selected":""; ?> >Transgender</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="col-xs-9">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label >Nationality </label>
                                                    <select name="guardian_nationality" id="guardian_nationality" class="input-select form-control" title="" >
                                                        <option value="" <?=( $bene_data->gd_nationality == "")?"selected":"" ?> >Please Select</option>
                                                        <option value="indian" <?=( $bene_data->gd_nationality == "indian")?"selected":"" ?> >Indian</option>
                                                        <option value="other" <?=( $bene_data->gd_nationality == "other")?"selected":"" ?> >Other</option>
                                                    </select>
                                                </div>
                                                <div class="col-xs-3">
                                                    <label>Occupation</label>
                                                    <?php $selected_occuption=$bene_data->gd_occupation; ?>
                                                    <select name="guardian_occupation" id="guardian_occupation" class="input-select form-control" title="" >
                                                        <option value="" <?php echo ($selected_occuption == "")?"selected":""; ?> >Please select</option>
                                                        <option value="service" <?php echo ($selected_occuption == "service")?"selected":""; ?> >Service</option>
                                                        <option value="business" <?php echo ($selected_occuption == "business")?"selected":""; ?> >Business</option>
                                                        <option value="other" <?php echo ($selected_occuption == "other")?"selected":""; ?> >Other</option>
                                                    </select>
                                                </div>
                                                <div class="col-xs-3">
                                                    <label>Religion </label>
                                                    <select name="guardian_religious" id="guardian_religious" class="input-select form-control" title="" >
                                                        <option value="" <?=( $bene_data->gd_religious == "")?"selected":"" ?> >Please Select</option>
                                    <option value="hindu" <?=( $bene_data->gd_religious == "hindu")?"selected":"" ?> >Hindu</option>
                                    <option value="other" <?=( $bene_data->gd_religious == "other")?"selected":"" ?> >Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- row END -->
                                    </div>
                                    <!-- panel-body END -->
                                    <h4><strong>Guardian - Permanent Address</strong></h4>
                                    <div class="panel panel-body sub-panel">
                                        <div class="row" style="display:none">
                                            <div class="col-xs-6">
                                                &nbsp;&nbsp;&nbsp;
                                                <input class="guardian_same_per_address" name="guardian_permanent_is_same_as_permanent" value="P" id="guardian_permanent_is_same_as_permanent" account_count="" for_address="guardian_permanent" checked="" title="Select if is this Address same as your Permanent Address?" alt="Select if is this Address same as your Permanent Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
                                                <label >Is this Address same as your Permanent Address?</label>
                                            </div>
                                            <!-- <div class="col-xs-6">
                                                &nbsp;&nbsp;&nbsp;
                                                <input class="guardian_same_per_address" name="guardian_permanent_is_same_as_correspondence" value="C" id="guardian_permanent_is_same_as_correspondence" account_count="" for_address="guardian_permanent" title="Select if is this Address same as your Correspondence Address?" alt="Select if is this Address same as your Correspondence Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
                                                <label >Is this Address same as your Correspondence Address?</label>
                                            </div> -->
                                        </div>
                                        <div class="row" style="display:none">
                                            <div class="col-xs-6">
                                                &nbsp;&nbsp;&nbsp;
                                                <input name="guardian_permanent_addr_copied_from" id="guardian_permanent_addr_copied_from" value="P" type="hidden">
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div id="populate_guardian_permanent_address">
                                            <div class="divider"></div>
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <label >Address Line1 </label>
                                                    <input name="guardian_permanent_address_line1" id="guardian_permanent_address_line1" class="form-control" value="<?= $bene_data->gd_per_addr_line1; ?>" placeholder="Address Line1" maxlength="60" title="Please enter Flat / Floor / Door / Block Number of the Guardian's Permanent Address" alt="Please enter Address Line1 for guardian permanent address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1 for guardian permanent address" type="text" required>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label >Address Line2 </label>
                                                    <input name="guardian_permanent_address_line2" id="guardian_permanent_address_line2" class="form-control" value="<?= $bene_data->gd_per_addr_line2; ?>" placeholder="Address Line2" maxlength="60" title="Please enter Name of Premises / Building of the Gaurdian's Permanent Address" alt="Please enter Name of Premises / Building of the Gaurdian's Permanent Address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2 for guardian permanent address" type="text" required>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label>Address Line3</label>
                                                    <input name="guardian_permanent_address_line3" id="guardian_permanent_address_line3" class="form-control" value="<?= $bene_data->gd_per_addr_line3; ?>" placeholder="Address Line3" maxlength="60" title="Please enter name of Road / Street / Lane of the Gaurdian's Permanent Address" alt="Please enter name of Road / Street / Lane of the Gaurdian's Permanent Address" type="text">
                                                </div>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label class="mandatory_label">City / Village / Town </label>
                                                    <input name="guardian_permanent_city_village_town" id="guardian_permanent_city_village_town" class="form-control" value="<?= $bene_data->gd_per_city; ?>" placeholder="City / Village / Town" maxlength="60" title="Please enter name of City / Village / Town of the Gaurdian's Permanent Address" alt="Please enter name of City / Village / Town of the Gaurdian's Permanent Address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town for guardian permanent address" type="text" required>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label >State </label>
                                                    <span id="span_guardian_permanent_state">
                                                        <?php $gdcur_state=$bene_data->gd_per_state; ?>
                                                        <select name="guardian_permanent_state" id="guardian_permanent_state" class="input-select form-control" title="Please select appropriate State" alt="Please select appropriate State" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please select a valid State" required>
                                                        <option value="35" selected <?php echo ($gdcur_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($gdcur_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($gdcur_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($gdcur_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($gdcur_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($gdcur_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($gdcur_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($gdcur_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($gdcur_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($gdcur_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($gdcur_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($gdcur_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($gdcur_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($gdcur_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($gdcur_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($gdcur_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($gdcur_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($gdcur_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($gdcur_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($gdcur_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($gdcur_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($gdcur_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($gdcur_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($gdcur_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($gdcur_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($gdcur_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($gdcur_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($gdcur_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($gdcur_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($gdcur_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($gdcur_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($gdcur_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($gdcur_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($gdcur_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($gdcur_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($gdcur_state == 19)?"selected":"" ?>>West Bengal</option>
                                    </select>
                                                    </select>
                                                </span>
                                                </div>
                                                <!-- <div class="col-xs-3">
                                                    <span id="guardian_permanent_span_other_state" style="display:none;" validatemsg="Please enter other State for guardian permanent address">
                                            <label class="cnd-mandatory mandatory_label">Other state (If not listed) </label>
                                                    <input name="guardian_permanent_state_other" id="guardian_permanent_state_other" class="form-control" value="" placeholder="Other state" maxlength="60" title="Please enter Other State for permanent address" alt="Please enter appropriate State for Guardian's Permanent Address, if country is other than India" type="text" required>
                                                    </span>
                                                </div> -->
                                                <div class="col-xs-2">
                                                    <label >Pincode / Zipcode </label>
                                                    <input name="guardian_permanent_zipcode" id="guardian_permanent_zipcode" class="form-control numeric" value="<?= $bene_data->gd_per_zip; ?>" placeholder="Pincode" title="Please enter PIN Code for India if Gaurdian's Permanent Address is in India, Zip Code for address outside India" alt="Please enter Pincode / Zipcode for guardian permanent address" data-validation="length" maxlength="6" data-validation-length="6-6" data-validation-error-msg="Please enter valid Pincode / Zipcode for guardian permanent address" type="text" required>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label >Country </label>
                                                    <span id="span_guardian_permanent_country">
                                        <select name="guardian_permanent_country" id="guardian_permanent_country" class="input-select form-control" title="Please select appropriate Country for Guardian's Permanent Address" alt="Please select appropriate Country for Guardian's Permanent Address" data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please select a Country for guardian permanent address" onchange="javascript: populate_states(this.value,'guardian_permanent_state', 'span_guardian_permanent_state', '102', 'toggle_other_state(\'guardian_permanent_state\', \'guardian_permanent_span_other_state\')');; toggle_other_state_country(this.value, 'guardian_permanent_span_other_state', {'state_id':''});;" required><option value="102" selected="selected">India</option></select>                                     </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Permanent Address END -->
                                    <h4>
                                <strong>Guardian - Correspondence  Address</strong>
                                <span>Is the Correspondence Address same as Permanent Address ?
                                <input name="is_guardian_correspondence_same_as_guardian_permanent" id="grdnCorrespondenceAdd" title="Select the checkbox if Guardian's Correspondence Address is same as Permanent  Address" alt="Select the checkbox if Guardian's Correspondence Address is same as Permanent  Address" type="checkbox"></span>
                            </h4>
                                    <div class="panel panel-body sub-panel grdnCorrespondenceAdd">
                                        <!--<div class="row">
                                    <div class="col-xs-6">
                                        <label>Is this Address a Foreign Address?</label>
                                         <input type="checkbox" name="guardian_correspondence_is_foreign_address" id="guardian_correspondence_is_foreign_address" value="1" title='Select if Guardian's Correspondence Address is outside India' class="is_foreign_address"  alt='Select if Guardian's Correspondence Address is outside India' changezip="guardian_correspondence_zipcode" country="guardian_correspondence_country" state="guardian_correspondence_state" span_country="span_guardian_correspondence_country" span_state="span_guardian_correspondence_state" other_state="guardian_correspondence_span_other_state" />
                                    </div>
                                </div>
                                <div class="divider"></div> -->
                                        <div class="row" style="display: none">
                                            <div class="col-xs-6">
                                                &nbsp;&nbsp;&nbsp;
                                                <input class="guardian_same_cor_address" name="guardian_correspondence_is_same_as_permanent" value="P" id="guardian_correspondence_is_same_as_permanent" account_count="" for_address="guardian_correspondence" checked="" title="Select if is this Address same as your Permanent Address?" alt="Select if is this Address same as your Permanent Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
                                                <label >Is this Address same as your Permanent Address?</label>
                                            </div>
                                            <div class="col-xs-6">
                                                &nbsp;&nbsp;&nbsp;
                                                <input class="guardian_same_cor_address" name="guardian_correspondence_is_same_as_correspondence" value="C" id="guardian_correspondence_is_same_as_correspondence" account_count="" for_address="guardian_correspondence" title="Select if is this Address same as your Correspondence Address?" alt="Select if is this Address same as your Correspondence Address?" type="checkbox">&nbsp;&nbsp;&nbsp;
                                                <label >Is this Address same as your Correspondence Address?</label>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none;">
                                            <div class="col-xs-6">
                                                &nbsp;&nbsp;&nbsp;
                                                <input name="guardian_correspondence_addr_copied_from" id="guardian_correspondence_addr_copied_from" value="P" type="hidden">
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div id="populate_guardian_correspondence_address">
                                            <div class="divider"></div>
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <label >Address Line1 </label>
                                                    <input name="guardian_correspondence_address_line1" id="guardian_correspondence_address_line1" class="form-control" value="<?= $bene_data->gd_corre_addr_line1; ?>" placeholder="Address Line1" maxlength="60" title="Please enter Flat / Floor / Door / Block Number of the Gaurdian's Correspondence Address" alt="Please enter Flat / Floor / Door / Block Number of the Gaurdian's Correspondence Address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1 for guardian correspondence address" type="text" required>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label >Address Line2 </label>
                                                    <input name="guardian_correspondence_address_line2" id="guardian_correspondence_address_line2" class="form-control" value="<?= $bene_data->gd_corre_addr_line2; ?>" placeholder="Address Line2" maxlength="60" title="Please enter Name of Premises / Building of the Gaurdian's Correspondence Address" alt="Please enter Name of Premises / Building of the Gaurdian's Correspondence Address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2 for guardian correspondence address" type="text" required>
                                                </div>
                                                <div class="col-xs-4">
                                                    <label>Address Line3</label>
                                                    <input name="guardian_correspondence_address_line3" id="guardian_correspondence_address_line3" class="form-control" value="<?= $bene_data->gd_corre_addr_line3; ?>" placeholder="Address Line3" maxlength="60" title="Please enter name of Road / Street / Lane of the Gaurdian's Correspondence Address" alt="Please enter name of Road / Street / Lane of the Gaurdian's Correspondence Address" type="text">
                                                </div>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label class="mandatory_label">City / Village / Town </label>
                                                    <input name="guardian_correspondence_city_village_town" id="guardian_correspondence_city_village_town" class="form-control" value="<?= $bene_data->gd_corre_city; ?>" placeholder="City / Village / Town" maxlength="60" title="Please enter City / Village / Town for correspondence address" alt="Please enter City / Village / Town for correspondence address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town for guardian correspondence address" type="text" required>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label >State </label>
                                                    <span id="span_guardian_correspondence_state">
                                    <?php $gdcorr_state=$bene_data->gd_corre_state; ?>
                                                        <select name="guardian_correspondence_state" id="guardian_correspondence_state" class="input-select form-control" title="Please select appropriate State" alt="Please select appropriate State" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please select a valid State" required>
                                                        <option value="35" selected <?php echo ($gdcorr_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($gdcorr_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($gdcorr_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($gdcorr_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($gdcorr_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($gdcorr_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($gdcorr_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($gdcorr_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($gdcorr_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($gdcorr_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($gdcorr_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($gdcorr_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($gdcorr_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($gdcorr_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($gdcorr_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($gdcorr_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($gdcorr_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($gdcorr_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($gdcorr_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($gdcorr_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($gdcorr_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($gdcorr_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($gdcorr_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($gdcorr_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($gdcorr_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($gdcorr_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($gdcorr_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($gdcorr_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($gdcorr_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($gdcorr_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($gdcorr_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($gdcorr_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($gdcorr_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($gdcorr_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($gdcorr_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($gdcorr_state == 19)?"selected":"" ?>>West Bengal</option>
                                                    </select>
                                                </span>
                                                </div>
                                                <!-- <div class="col-xs-3">
                                                    <span id="guardian_correspondence_span_other_state" style="display:none;" validatemsg="Please enter other State for guardian correspondence address">
                                                <label class="cnd-mandatory mandatory_label">Other state (if not listed)</label>
                                                    <input name="guardian_correspondence_state_other" id="guardian_correspondence_state_other" class="form-control" value="" placeholder="Other state" maxlength="60" title="Please enter appropriate State for Guardian's Correspondence Address, if country is other than India" alt="Please enter appropriate State for Guardian's Correspondence Address, if country is other than India" type="text" required>
                                                    </span>
                                                </div> -->
                                                <div class="col-xs-2">
                                                    <label >Pincode / Zipcode </label>
                                                    <input name="guardian_correspondence_zipcode" id="guardian_correspondence_zipcode" class="form-control numeric" value="<?= $bene_data->gd_corre_zip; ?>" placeholder="Pincode" title="Please enter PIN Code for India if Guardian's Correspondence Address is in India, Zip Code for address outside India" alt="Please enter PIN Code for India if Guardian's Correspondence Address is in India, Zip Code for address outside India" data-validation="length" maxlength="6" data-validation-length="6-6" data-validation-error-msg="Please enter valid Pincode / Zipcode for guardian correspondence address" type="text" required>
                                                </div>
                                                <div class="col-xs-2">
                                                    <label >Country </label>
                                                    <span id="span_guardian_correspondence_country">
                                            <select name="guardian_correspondence_country" id="guardian_correspondence_country" class="input-select form-control" title="Please select appropriate Country for Gaurdian's Correspondence Address" alt="Please select appropriate Country for Gaurdian's Correspondence Address" data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please enter valid Country for guardian correspondence address" onchange="javascript: populate_states(this.value,'guardian_correspondence_state', 'span_guardian_correspondence_state', '102', 'toggle_other_state(\'guardian_correspondence_state\', \'guardian_correspondence_span_other_state\')');" required><option value="102" selected="selected">India</option></select>                                           </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Permanent Address END -->
                                    <div class="panel panel-body sub-panel mrg20T">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <label class="mandatory_label">Relationship of the Guardian with the Beneficiary </label>
                                                <br>
                                                <select name="guardian_beneficary_relationship" id="guardian_beneficary_relationship" class="input-select form-control" title="Please select appropriate 'Relationship of the Guardian with the Beneficiary' from the dropdown list" alt="Please select appropriate 'Relationship of the Guardian with the Beneficiary' from the dropdown list" data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please select appropriate 'Relationship of the Guardian with the Beneficiary' from the dropdown list" onchange="javascript: toggle_other_relation('guardian_beneficary_relationship', 'span_guardian_beneficary_relationship');" required>
                                                    <option value="">Please select</option>
                                                    <option value="2">Spouse</option>
                                                    <option value="3">Son</option>
                                                    <option value="4">Daughter</option>
                                                    <option value="5">Mother</option>
                                                    <option value="6">Father</option>
                                                    <option value="7" selected="selected">Brother</option>
                                                    <option value="8">Sister</option>
                                                    <option value="19">Grand Daughter</option>
                                                    <option value="20">Grandson</option>
                                                    <option value="21">Grand Father</option>
                                                    <option value="22">Grand Mother</option>
                                                    <option value="23">Son-in-Law</option>
                                                    <option value="24">Daughter-in-law</option>
                                                    <option value="99">Other</option>
                                                </select>
                                            </div>
                                            <!-- <div class="col-xs-6">
                                                <span id="span_guardian_beneficary_relationship" style="display:none;">
                                        <label class="cnd-mandatory mandatory_label">Other Relation (If not listed) </label>
                                                <div class="row">
                                                    <div class="col-xs-6 last">
                                                        <input class="form-control" name="guardian_beneficary_other_relationship" id="guardian_beneficary_other_relationship" value="" placeholder="Other Relationship" title="Please enter Other Relation" alt="Please enter Other Relation" data-validation="" data-validation-length="" data-validation-error-msg="" type="text" required>
                                                    </div>
                                                </div>
                                                </span>
                                            </div> -->
                                        </div>
                                    </div>
                                    <!-- panel-body END -->
                                </div>
                                <!-- grdn-details END -->
                            </div>
                            <div class="bottom_buttons">
                                <div class="div_next_button" style="text-align: right;">
                                    <input name="addnewben" id="btnaddnewben" value="Save »" title="Click here to Add the entered data" alt="Click here to Add the entered data" class="btn-submit " type="submit">
                                    <!-- <input name="next" id="btn-next" value="Next »" class="btn-submit btn-move" next_page="executor-details" title="Click here to go to Next page" alt="Click here to go to Next page" form_id="frmBeneficiary" type="button"> -->
                                </div>
                                <br>
                            </div>
                            <!-- <input name="txt_other_submit" id="txt_other_submit" value="0" type="hidden">
<input name="txt_redirect_page" id="txt_redirect_page" value="" type="hidden">
<input name="sumbit" value="Submit" class="btn-submit btn-submit-disabled" title="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" alt="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" type="submit">  -->
                        </div>
                        <!-- col-xs-12 END -->
                    </div>
                    <!-- row END -->
                </form>
                <div class="row">&nbsp;</div>
        </div>
    </div>
    <!-- row END -->
    </div>
    </div>
        </div>
      </div>
      
    </div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript">

$("#chkcorrespondence").click(function() {
	   if($(this).is(":checked")){
		   $('#correspondence_address_line1').val($('#permanent_address_line1').val());
	       $('#correspondence_address_line2').val($('#permanent_address_line2').val());
	       $('#correspondence_address_line3').val($('#permanent_address_line3').val());
	       $('#correspondence_city_village_town').val($('#permanent_city_village_town').val());
	       $('#correspondence_state').val($('#permanent_state').val());
	       $('#correspondence_zipcode').val($('#permanent_zipcode').val());
	       $('#correspondence_country').val($('#permanent_country').val());
	       $('.correspondenceAdd input, .correspondenceAdd select').attr('disabled','disabled');
	       alert("hi");
	   } else {
	       $('.correspondenceAdd input, .correspondenceAdd select').removeAttr('disabled');
	   }
});

$("#grdnCorrespondenceAdd").click(function() {
	   if($(this).is(":checked")){
		   $('#guardian_correspondence_address_line1').val($('#guardian_permanent_address_line1').val());
	       $('#guardian_correspondence_address_line2').val($('#guardian_permanent_address_line2').val());
	       $('#guardian_correspondence_address_line3').val($('#guardian_permanent_address_line3').val());
	       $('#guardian_correspondence_city_village_town').val($('#guardian_permanent_city_village_town').val());
	       $('#guardian_correspondence_state').val($('#guardian_permanent_state').val());
	       $('#guardian_correspondence_zipcode').val($('#guardian_permanent_zipcode').val());
	       $('#guardian_correspondence_country').val($('#guardian_permanent_country').val());
	       $('.grdnCorrespondenceAdd input, .grdnCorrespondenceAdd select').attr('disabled','disabled');
	       alert("hi");
	   } else {
	       $('.grdnCorrespondenceAdd input, .grdnCorrespondenceAdd select').removeAttr('disabled');
	   }
});

</script>
  