<?php
	include_once("../../../__lib.includes/config.inc.php");
	$wli_data		= $willProfile->getWillDetails('will_locker_info');
	$bene_data_all	= $willProfile->getWillBenDetails('will_beneficiary');  
?>
<form action="data_submit/frmForm.php" method="post">
    <input type="hidden" name="form_id" value="2">
    <div class="agreement-row">
        <div class="container">
            <div class="panel panel-body">
                <div class="row">
                    <div class="col-xs-4">
                        <label class="mandatory ">Locker Number
                            <span class="mandatory_star">*</span></label>
                        <input class="form-control" name="locker_number" value="<?= $wli_data->locker_number; ?>" placeholder="Locker Number" title="Please enter Locker Number" alt="Please enter Locker Number" maxlength="18" data-validation="" data-validation-length="1-18" data-validation-error-msg="Please enter valid Locker Number" type="text" >
                    </div>
                    <div class="col-xs-4">
                        <label class="mandatory mandatory_label">Name of Bank <span class="mandatory_star">*</span></label>
                        <input class="form-control" name="bank_name" value="<?= $wli_data->locker_bank_name; ?>" placeholder="Name of the bank" maxlength="99" data-validation="length" data-validation-length="1-99" data-validation-error-msg="Please enter valid Bank Name" title="Please enter Bank Name" alt="Please enter Bank Name" type="text" >
                    </div>
                    <div class="col-xs-4">
                        <label class="mandatory mandatory_label">Branch Name <span class="mandatory_star">*</span></label>
                        <input class="form-control" name="branch_name" value="<?= $wli_data->locker_branch_name; ?>" placeholder="Branch name" maxlength="30" data-validation="length" data-validation-length="1-30" data-validation-error-msg="Please enter valid Branch Name" title="Please enter Branch Name" alt="Please enter Branch Name" type="text" >
                    </div>
                </div>
                <!-- row END -->
                <div class="divider"></div>
                <div class="divider"></div>
                <div class="row">
                    <div class="col-xs-4">
                        <label class="mandatory">Address Line1
                            <span class="mandatory_star">*</span></label>
                        <input class="form-control" name="address_line1" value="<?= $wli_data->locker_addr_line1; ?>" placeholder="Address Line1" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1" title="Please enter Address Line1" alt="Please enter Address Line1" type="text" >
                    </div>
                    <div class="col-xs-4">
                        <label class="mandatory">Address Line2
                            <span class="mandatory_star">*</span></label>
                        <input class="form-control" name="address_line2" value="<?= $wli_data->locker_addr_line2; ?>" placeholder="Address Line2" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2" title="Please enter Address Line2" alt="Please enter Address Line2" type="text" >
                    </div>
                    <div class="col-xs-4">
                        <label class="mandatory">Address Line3</label>
                        <input class="form-control" name="address_line3" value="<?= $wli_data->locker_addr_line3; ?>" placeholder="Address Line3" maxlength="60" title="Please enter Address Line3" alt="Please enter Address Line3" type="text">
                    </div>
                </div>
                <!-- row END -->
                <div class="divider"></div>
                <div class="row">
                    <div class="col-xs-3">
                        <label class="mandatory">City / Village / Town
                            <span class="mandatory_star">*</span></label>
                        <input class="form-control" name="city_village_town" value="<?= $wli_data->locker_city; ?>" placeholder="City / Village / Town" maxlength="60" title="Please enter City / Village / Town" alt="Please enter City / Village / Town" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town" type="text" >
                    </div>
                    <div class="col-xs-2">
                        <label class="mandatory ">Pincode
                            <span class="mandatory_star">*</span></label>
                        <input class="form-control numeric" name="zipcode" value="<?= $wli_data->locker_zipcode; ?>" placeholder="Pincode" maxlength="6" data-validation="" data-validation-length="6-6" data-validation-error-msg="Please enter valid Pincode" title="Please enter Pincode" alt="Please enter Pincode" type="text" >
                    </div>
                    <div class="col-xs-2">
                        <label class="mandatory ">State
                            <!-- <span class="mandatory_star">*</span> --></label>
                        <span id="span_state">
                            <?php $corre_state=$wli_data->locker_state; ?>
                            <select name="state" id="state" class="input-select form-control" title="Please select appropriate State" alt="Please select appropriate State" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please select a valid State" >  
                                <option value="35" <?php echo ($corre_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($corre_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($corre_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($corre_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($corre_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($corre_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($corre_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($corre_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($corre_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($corre_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($corre_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($corre_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($corre_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($corre_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($corre_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($corre_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($corre_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($corre_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($corre_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($corre_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($corre_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($corre_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($corre_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($corre_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($corre_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($corre_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($corre_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($corre_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($corre_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($corre_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($corre_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($corre_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($corre_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($corre_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($corre_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($corre_state == 19)?"selected":"" ?>>West Bengal</option>

                            </select>
                        </span>
                    </div>
                    <div class="col-xs-3" style="display:none">
                        <label class="cnd-mandatory ">Other State (If not listed) <span class="mandatory_star">*</span></label>
                        <input class="form-control" name="state_other" value="" placeholder="Mention State Name" maxlength="60" title="Please enter a Other State" alt="Please enter a Other State" type="text" >
                    </div>
                    <div class="col-xs-2">
                        <label class="mandatory ">Country
                            <!-- <span class="mandatory_star">*</span> --></label>
                        <select name="country" id="country" class="input-select form-control" title="Please select a Country" alt="Please select a Country" data-validation="" data-validation-length="1-33" data-validation-error-msg="Please select a valid Country" onchange="javascript: populate_states(this.value,'state', 'span_state', '0','toggle_other_state(\'state\',\'state_other\')'); toggle_other_state_country(this.value, 'state_other');;">
                            <option value="102" selected="selected">India</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                                <div class="col-xs-4">
                                    <label class="mandatory  mandatory_label">% of ownership<span class="mandatory_star">*</span></label>
                                    <input class="form-control" name="ownership_perc_locker" value="<?= $wli_data->locker_ownership_perc; ?>" placeholder="% of ownership" maxlength="30" data-validation="length" data-validation-length="1-30" data-validation-error-msg="Please enter valid percentage of locker ownership" title="Please enter percentage of locker ownership" alt="Please enter percentage of locker ownership" type="text" required>
                                </div>
                            </div>
            </div>
            <!-- panel-body END -->
            <div class="grdn-details" id="ejd_guardian_1">
            </div>
            <!-- grdn-details END -->
            
            <div id="div_beneficiary">
        <div id="div_beneficiary_1">
            <h4>
									<strong>Beneficiaries</strong>
								</h4>
            <div class="panel panel-body sub-panel">
                <div class="row">
                    <div class="col-xs-4">
                        <label class="mandatory mandatory_label">Beneficiary <span class="mandatory_star">*</span></label>
                        <select name="sel_beneficiary" id="sel_beneficiary" class="input-select form-control" title="Please select a Beneficiary" alt="Please select a Beneficiary">
                            <option value="">Please select</option>
                            <?php 
                                                $cnt=1;
                                                while($row = mysql_fetch_array($bene_data_all)) {
                                                    $html = "<option value='" . $cnt . "'>" .$row['f_name'] . "</option>";
                                                    
                                                    $cnt+= 1;
                                                    echo $html;
                                                } 
                                                ?>
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <label class="mandatory mandatory_label">Percentage Share to be allotted <span class="mandatory_star">*</span></label>
                        <input class="form-control numeric_share" name="txt_beneficiary_share" id="txt_beneficiary_share" placeholder="Percentage Share to be allotted" maxlength="5" title="Please enter Percentage Share to be allotted" alt="Please enter Percentage Share to be allotted" value="" type="text">
                    </div>
                    <div class="col-xs-4">
                        <label class="mandatory">Any Other Information</label>
                        <input class="form-control" name="txt_other_info" id="txt_other_info" maxlength="200" placeholder="Any Other Information" title="Please enter Any Other Information" alt="Please enter Any Other Information" value="" type="text">
                    </div>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Share</th>
                            <th>Relation</th>
                            <th><a href="" class="btn btn-danger">Reset all</a></th>
                        </tr>
                    </thead>
                    <tbody id="tb_con_ben">
                        <tr id="no_beneficiary">
                            <td colspan="5"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="div_prev_button">
                <input name="previous" id="btn-previous" value="« Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="executor-details" type="button">
            </div>
            <div class="pull-right">
                <!--<br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add More Beneficiary" title="Add More Beneficiary" id="" class="btn btn-success btn-sm">+ Add More</a></span>-->
                <input name="add_locker" id="btn-add" value="Save & Next »" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " type="submit">
            </div>
        </div>
    </div>  
    
        </div>
    </div>

</form>