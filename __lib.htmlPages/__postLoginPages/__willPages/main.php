<?php
include_once("../../../__lib.includes/config.inc.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Will</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Moved style -->
    <link media="all" type="text/css" rel="stylesheet" href="css/jquery-ui.css">
    <link media="all" type="text/css" rel="stylesheet" href="css/jquery.datetimepicker.css">


  </head>
  <body>
    <!-- main-container start -->
    <div class="container">
      <!-- first row start -->
      <div class="row">
        <!-- col-md-12 start  -->
        <div class="col-md-12">
          <!-- tab header start here  -->
          <div class="with-nav-tabs panel-default">
              <ul class="nav nav-pills">
                  <li class="active"><a data="dashboard.php" data-toggle="tab" class="btn btn-default">Personal Information</a></li>
                  <li><a data="beneficiary-details.php" data-toggle="tab" class="btn btn-success">Beneficiaries</a></li>
                  <li><a data="executor-details.php" data-toggle="tab" class="btn btn-info">Executors</a></li>
                  <li><a data="bank-tab.php" data-toggle="tab" class="btn btn-warning">Bank Accounts</a></li>
                  <li><a data="fixed-deposit.php" data-toggle="tab" class="btn btn-warning">Fixed Deposit</a></li>
                  <li><a data="mutual-tab.php" data-toggle="tab" class="btn btn-danger">Securities / MF / Bonds</a></li>
                  <li><a data="immovable-properties-details.php" data-toggle="tab" class="btn btn-danger">Immovable Properties</a></li>
                  <li><a data="business-details.php" data-toggle="tab" class="btn btn-danger">Business</a></li>
              </ul>
              <br>
              <ul class="nav nav-pills">
                  <li><a data="retire-tab.php" data-toggle="tab" class="btn btn-danger">Retirement Funds</a></li>
                  <li><a data="general-insurance.php" data-toggle="tab" class="btn btn-danger">General Insurance</a></li>
                  <li><a data="life-insurance-policies.php" data-toggle="tab" class="btn btn-danger">Life Insurance Policies</a></li>
                  <li><a data="jewellery-details.php" data-toggle="tab" class="btn btn-danger">Jewellery</a></li>
                  <li><a data="body-organ.php" data-toggle="tab" class="btn btn-danger">Body Organ</a></li>
                  <li><a data="pet-animal.php" data-toggle="tab" class="btn btn-danger">Pet Animal</a></li>
                  <li><a data="other-assets-tab.php" data-toggle="tab" class="btn btn-danger">Other Assets</a></li>
                  <li><a data="liabilities-tab.php" data-toggle="tab" class="btn btn-danger">Liabilities</a></li>
                  <li><a data="custodian-details.php" data-toggle="tab" class="btn btn-danger">Custodian</a></li>
                  <!-- <li><a data="non-beneficiaries.php" data-toggle="tab" class="btn btn-danger">Non Beneficiaries</a></li>
                  <li><a data="contingent-tab.php" data-toggle="tab" class="btn btn-danger">Contingent Beneficiary / <br> Special Instructions </a></li> -->
              </ul>
              <br>
              <ul class="nav nav-pills">
                  <li><a data="witness-details.php" data-toggle="tab" class="btn btn-danger">Witiness</a></li>
              </ul>
          </div>
        <!-- tab header end here -->

        <!-- tab body start here -->
        <div class="panel-body">

        </div>
        <!-- tab body end here -->

        </div>
        <!-- col-md-12 end -->
      </div>
      <!-- first row end -->
    </div>
    <!-- main-container end -->
  </body>
</html>

<!-- Moved script start-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.form-validator.js"></script>
<script src="js/jquery.numeric.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/stocks-bonds-details.js" lang="javascript"></script>
<!-- Moved script end-->

    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.datetimepicker.js"></script>
    <!-- <link rel="stylesheet" href="css/style.css"> -->
    <script type="text/javascript" src="js/myScript.js"></script>
    <style media="screen">

    </style>
    <script type="text/javascript">
      $(document).ready(function(){

        // if($(".nav-pills li a").data('clicked')){
        //     alert("Hi...");
        // } else {
        //   alert("Hello...");
        // }
        $('.nav-pills li a').click(function(){
          var list = $(this).attr("data");
          // alert(list);
          $.ajax({
            url: list,
            type: 'get',
            success: function(data){
              $('.panel-body').html(data);
            },
            error: function(error){

            }
          });
        });

        // First load time
        $.ajax({
            url: 'dashboard.php',
            type: 'get',
            success: function(data){
              $('.panel-body').html(data);
            },
            error: function(error){

            }
          });


    //      $('#abc').click(function() {
    //       alert("d");
    //     var list = $(this).attr("data");
    //     $.ajax({
    //         url: list,
    //         type: 'get',
    //         success: function(data) {
    //             $('.panel-body-small').html(data);
    //         },
    //         error: function(error) {

    //         }
    //     });
    // });

    //      $('#abc1').click(function() {
    //       alert("d");
    //     var list = $(this).attr("data");
    //     $.ajax({
    //         url: list,
    //         type: 'get',
    //         success: function(data) {
    //             $('.panel-body-small').html(data);
    //         },
    //         error: function(error) {

    //         }
    //     });
    // });
$("#chkcorrespondence").on('click',function(e){
  e.preventDefault();
        if($(this).is(":checked")){
            alert("Hi");
        } else {
            alert("Hello");
        }
    });





$( function() {
  // alert("hi");
  $( "input[type=date]" ).datepicker();
});

$( function() {
  // alert("hi");
  $(".mandatory_star").css("color", "red");
});

});
    </script>
