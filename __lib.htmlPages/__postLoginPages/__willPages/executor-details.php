<?php 
	$exe_data		= $willProfile->getWillDetails('will_executor');
?>
<div class="agreement-row">
    <div class="container">
        <!-- Listing STARTS -->
        <div class="row">
            <div class="col-xs-12">
<!--                 <h4><strong>Executor</strong> -->
                    <!-- &nbsp; &nbsp; &nbsp;<a href="#" data-toggle="modal" class="blue-italic-cities" data-target="#myModal">Definition?</a> -->
                </h4>
                <div class="panel panel-body" style="display: none">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Executor Name</th>
                                <th>Executor Type</th>
                                <th>Professional / Acquaintance</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?= $exe_data->fk_user_id; ?></td>
                                <td>
                                    <?= $exe_data->exe_f_name; ?>
                                    <?= $exe_data->exe_m_name; ?>
                                    <?= $exe_data->exe_l_name; ?>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Listing END -->
        <h3 id="opt_status">Executor</h3>
        <form action="data_submit/frmForm.php" name="frmexecutordetails" method="post" id="frmexecutordetails" class="frmCurrent has-validation-callback">
            <!-- <input name="auth_token" id="auth_token_name" value="TVRBekxqVTFMakUyT0M0eE5UWnRiek15WWpkbmNHODJNamhsWkdZeWFtcG1hbWMxWlhZeE13PT0=" type="hidden"> -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-body">
                        <input id="executor_count" name="executor_count" value="0" type="hidden">
                    </div>
                    <!-- panel-body END -->
                    <div>
                        <div id="executor_details">
                            <input class="" name="executordetails_types_process" id="executordetails_types_process" value="1" type="hidden">
                            <input name="exc_id" id="exc_id" value="0" type="hidden">
                            <input name="executor_option" id="executor_option" value="other" type="hidden">
                            <div class="executor MentallyYes">
                                <div class="panel panel-body sub-panel">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <!-- If 'Others' selected in above select -->
                                            <label class="mandatory mandatory_label">Name of Executor<span class="mandatory_star">*</span>
                                            </label>
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <?php $selected=$exe_data->exe_title; ?>
                                                    <select name="executor_title" id="executor_title" class="input-select form-control" title="Please select a Title" alt="Please select a Title" data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please select a Title." onchange="javascript: title_change();" required>
                                                        <option value="" <?php echo ($selected=="" ) ? "selected" : "" ; ?> >Title</option>
                                                        <option value="1" <?php echo ($selected==1 ) ? "selected" : "" ; ?> >Mr. </option>
                                                        <option value="2" <?php echo ($selected==2 ) ? "selected" : "" ; ?> >Mrs. </option>
                                                        <option value="3" <?php echo ($selected==3 ) ? "selected" : "" ; ?> >Ms. </option>
                                                        <option value="10" <?php echo ($selected==3 ) ? "selected" : "" ; ?> >Master </option>
                                                        <option value="23" <?php echo ($selected==3 ) ? "selected" : "" ; ?> >Kumar </option>
                                                        <option value="24" <?php echo ($selected==3 ) ? "selected" : "" ; ?> >Kumari </option>
                                                    </select>
                                                </div>
                                                <div class="col-xs-3">
                                                    <input class="form-control" placeholder="First Name" id="firstname" name="firstname" maxlength="33" value="<?= $exe_data->exe_f_name; ?>" title="Please enter First Name" alt="Please enter First Name" type="text">
                                                </div>
                                                <div class="col-xs-3">
                                                    <input class="form-control" placeholder="Middle Name" id="middlename" name="middlename" maxlength="33" value="<?= $exe_data->exe_m_name; ?>" title="Please enter Middle Name" alt="Please enter Middle Name" type="text">
                                                </div>
                                                <div class="col-xs-3">
                                                    <input class="form-control" placeholder="Last Name / Surname" id="lastname" name="lastname" maxlength="33" data-validation="length" data-validation-length="2-33" data-validation-error-msg="Please enter valid Last Name / Surname" value="<?= $exe_data->exe_l_name; ?>" title="Only alphabets and single apphostophe (') is allowed as special characters. For e.g. D'Silva, D'Souza. If you have a single name, please enter here" alt="Only alphabets and single apphostophe (') is allowed as special characters. For e.g. D'Silva, D'Souza. If you have a single name, please enter here" type="text">
                                                </div>
                                            </div>
                                            <div class="row">
                                            <div class="col-xs-12">
                                                <label class="mandatory mandatory_label">Executor's Father Name <span class="mandatory_star">*</span></label>
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <?php $exe_fat_tit=$exe_data->exe_fat_title;?>
                                                        <select name="executor_fat_name_title" id="executor_fat_name_title" class="input-select form-control required" title="Please select appropriate title from the dropdown list. Drop-down of the standard Titles normally used in writing Will" alt="Please select appropriate title from the dropdown list. Drop-down of the standard Titles normally used in writing Will" data-validation="length" data-validation-length="1-20" data-validation-error-msg="Please select a Title" onchange="javascript: title_change();" required>
                                                            <option value="" <?php echo ($exe_fat_tit=="" ) ? "selected" : "" ; ?> >Title</option>
                                                            <option value="1" <?php echo ($exe_fat_tit==1 ) ? "selected" : "" ; ?> >Mr. </option>
                                                            <option value="10" <?php echo ($exe_fat_tit==10 ) ? "selected" : "" ; ?> >Master </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <input name="executor_fat_firstname" id="executor_fat_firstname" value="<?= $exe_data->exe_fat_f_name; ?>" placeholder="First Name" class="form-control" maxlength="33" title="Only alphabets are allowed. Abbreviations and initials to be avoided" alt="Only alphabets are allowed. Abbreviations and initials to be avoided" type="text">
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <input name="executor_fat_middlename" id="executor_fat_middlename" value="<?= $exe_data->exe_fat_m_name; ?>" placeholder="Middle Name" class="form-control" maxlength="33" title="Only alphabets are allowed" alt="Only alphabets are allowed" type="text">
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <input name="executor_fat_lastname" id="executor_fat_lastname" value="<?= $exe_data->exe_fat_l_name; ?>" placeholder="Last Name / Surname" class="form-control" maxlength="33" title="Only alphabets and single apphostophe (') is allowed as special characters. For e.g. D'Silva, D'Souza. If you have a single name, please enter here" alt="Only alphabets and single apphostophe (') is allowed as special characters. For e.g. D'Silva, D'Souza. If you have a single name, please enter here" data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please enter valid guardian Last Name / Surname" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <label class="cnd-mandatory mandatory_label">Occupation <span class="mandatory_star">*</span></label>
                                                    <select name="executor_occupation" id="executor_occupation" class="input-select form-control" title="" required>
                                                        <option value="" <?=( $exe_data->exe_occupation == "")?"selected":"" ?> >Please Select</option>
                                    <option value="teacher" <?=( $exe_data->exe_occupation == "teacher")?"selected":"" ?> >Teacher</option>
                                    <option value="other" <?=( $exe_data->exe_occupation == "other")?"selected":"" ?> >Other</option>
                                                    </select>
                                                </div>
                                                <div class="col-xs-3">
                                                    <label class="cnd-mandatory mandatory_label">Religion <span class="mandatory_star">*</span></label>
                                                    <?php $selected_rel = $exe_data->exe_religious ?>
                                                    <select name="executor_religious" id="executor_religious" class="input-select form-control" title="" required>
                                                        <option value="" <?=( $selected_rel == "")?"selected":"" ?> >Please select</option>
<option value="1" <?=( $selected_rel == 1)?"selected":"" ?>>Buddhist</option>
<option value="2" <?=( $selected_rel == 2)?"selected":"" ?>>Christian</option>
<option value="3" <?=( $selected_rel == 3)?"selected":"" ?> >Hindu</option>
<option value="4" <?=( $selected_rel == 4)?"selected":"" ?>>Islam</option>
<option value="5" <?=( $selected_rel == 5)?"selected":"" ?>>Jain</option>
<option value="6" <?=( $selected_rel == 6)?"selected":"" ?>>Judaism</option>
<option value="7" <?=( $selected_rel == 7)?"selected":"" ?>>Parsi</option>
<option value="8" <?=( $selected_rel == 8)?"selected":"" ?>>Sikh</option>
<option value="10" <?=( $selected_rel == 10)?"selected":"" ?>>Other</option>
                                                    </select>
                                                </div>
                                                <div class="col-xs-3">
                                                    <label class="cnd-mandatory mandatory_label">Nationality <span class="mandatory_star">*</span></label>
                                                    <select name="executor_nationality" id="executor_nationality" class="input-select form-control" title="" required>
                                                        <option value="" <?=( $exe_data->exe_nationality == "")?"selected":"" ?> >Please Select</option>
                                    <option value="Indian" <?=( $exe_data->exe_nationality == "indian")?"selected":"" ?> >Indian</option>
                                    <option value="Other" <?=( $exe_data->exe_nationality == "other")?"selected":"" ?> >Other</option>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="divider"></div>
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label class="mandatory mandatory_label">Gender <span class="mandatory_star">*</span>
                                            </label>
                                            <select name="gender" id="gender" class="input-select form-control" title="Please select appropriate gender from the dropdown list" alt="Please select appropriate gender from the dropdown list" data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please select gender." required>
                                                <option value="" <?php echo ($exe_data->exe_gender == "")?"selected":""; ?> >Please select</option>
                                                <option value="1" <?php echo ($exe_data->exe_gender == 1)?"selected":""; ?> >Male</option>
                                                <option value="2" <?php echo ($exe_data->exe_gender == 2)?"selected":""; ?> >Female</option>
                                                <option value="3" <?php echo ($exe_data->exe_gender == 3)?"selected":""; ?> >Transgender</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="cnd-mandatory mandatory_label">Age <span class="mandatory_star">*</span>
                                            </label>
                                            <input type="text" id="executor_age" name="executor_age" class="form-control" value="<?= $exe_data->exe_age; ?>" placeholder="Age" title='Age' alt='Age' required/>
                                        </div>
                                        <div class="col-xs-3">
                                            <label class="mandatory mandatory_label">Do you have relation with executor ?<span class="mandatory_star">*</span>
                                            </label>
                                            <select name="relationship_with_testator" id="relationship_with_testator" class="input-select form-control" title="Please select appropriate 'Relationship with Testaor' from the dropdown list" alt="Please select appropriate 'Relationship with Testaor' from the dropdown list" data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please select a Relationship with Testator" onchange="javascript: toggle_other_relation('relationship_with_testator', 'span_beneficary_relationship');" required>
                                                <option value="" selected>Please select</option>
                                                        <option value="2" <?=( $exe_data->exe_rel_with_tes == 2)? "selected":"" ?>>Spouse</option>
                                                        <option value="3" <?=( $exe_data->exe_rel_with_tes == 3)? "selected":"" ?>>Son</option>
                                                        <option value="4" <?=( $exe_data->exe_rel_with_tes == 4)? "selected":"" ?>>Daughter</option>
                                                        <option value="5" <?=( $exe_data->exe_rel_with_tes == 5)? "selected":"" ?>>Mother</option>
                                                        <option value="6" <?=( $exe_data->exe_rel_with_tes == 6)? "selected":"" ?>>Father</option>
                                                        <option value="7" <?=( $exe_data->exe_rel_with_tes == 7)? "selected":"" ?> >Brother</option>
                                                        <option value="8" <?=( $exe_data->exe_rel_with_tes == 8)? "selected":"" ?>>Sister</option>
                                                        <option value="19" <?=( $exe_data->exe_rel_with_tes == 19)? "selected":"" ?>>Grand Daughter</option>
                                                        <option value="20" <?=( $exe_data->exe_rel_with_tes == 20)? "selected":"" ?>>Grandson</option>
                                                        <option value="21" <?=( $exe_data->exe_rel_with_tes == 21)? "selected":"" ?>>Grand Father</option>
                                                        <option value="22" <?=( $exe_data->exe_rel_with_tes == 22)? "selected":"" ?>>Grand Mother</option>
                                                        <option value="23" <?=( $exe_data->exe_rel_with_tes == 23)? "selected":"" ?>>Son-in-Law</option>
                                                        <option value="24" <?=( $exe_data->exe_rel_with_tes == 24)? "selected":"" ?>>Daughter-in-law</option>
                                                        <option value="99" <?=( $exe_data->exe_rel_with_tes == 99)? "selected":"" ?>>Other</option>
                                            </select>
                                        </div>
                                        
                                        <div class="col-xs-4 last">
                                            <span id="span_beneficary_relationship" style="display:none;">
                                            <label class="cnd-mandatory mandatory_label">Other Relation (If not listed) 
                                                <!-- <span class="mandatory_star">*</span> -->
                                            </label>
                                            <div class="row">
                                                <div class="col-xs-8 last">
                                                    <input class="form-control" name="other_relationship" value="" placeholder="Relationship" type="text">
                                                </div>
                                            </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <h4><strong>Permanent Address</strong></h4>
                                <div class="panel panel-body sub-panel">
                                    <div class="row" style="display:none">
                                        <div class="col-xs-6">
                                            &nbsp;&nbsp;&nbsp;
                                            <input class="same_per_address" name="permanent_is_same_as_permanent" value="P" id="permanent_is_same_as_permanent" account_count="" for_address="permanent" title="Select if is this Address same as your Permanent Address?" alt="Select if is this Address same as your Permanent Address?" type="checkbox" checked>&nbsp;&nbsp;&nbsp;
                                            <label class="mandatory">Is this Address same as your Permanent Address?</label>
                                        </div>
                                        <div class="col-xs-6">
                                            &nbsp;&nbsp;&nbsp;
                                            <input class="same_per_address" name="permanent_is_same_as_correspondence" value="C" id="permanent_is_same_as_correspondence" account_count="" for_address="permanent" title="Select if is this Address same as your Correspondence Address?" alt="Select if is this Address same as your Correspondence Address?" type="checkbox" checked>&nbsp;&nbsp;&nbsp;
                                            <label class="mandatory">Is this Address same as your Correspondence Address?</label>
                                        </div>
                                    </div>
                                    <div class="row" style="display:none;">
                                        <div class="col-xs-6">
                                            &nbsp;&nbsp;&nbsp;
                                            <input name="permanent_addr_copied_from" id="permanent_addr_copied_from" value="" type="hidden">
                                        </div>
                                    </div>
                                    <div class="divider"></div>
                                    <div id="populate_permanent_address">
                                        <div class="divider"></div>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <label class="mandatory mandatory_label">Address Line1 <span class="mandatory_star">*</span>
                                                </label>
                                                <input class="form-control" placeholder="Address Line1" id="permanent_address_line1" name="permanent_address_line1" maxlength="60" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1" value="<?= $exe_data->exe_per_addr_line1 ; ?>" title="Please enter Flat / Floor / Door / Block Number as applicable" alt="Please enter Flat / Floor / Door / Block Number as applicable" type="text" required>
                                            </div>
                                            <div class="col-xs-4">
                                                <label class="mandatory mandatory_label">Address Line2 <span class="mandatory_star">*</span>
                                                </label>
                                                <input class="form-control" placeholder="Address Line2" id="permanent_address_line2" name="permanent_address_line2" maxlength="60" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter Address Line2" value="<?= $exe_data->exe_per_addr_line2; ?>" title="Please enter Name of Premises / Building of Permanent Address" alt="Please enter Name of Premises / Building of Permanent Address" type="text" required>
                                            </div>
                                            <div class="col-xs-4">
                                                <label>Address Line3</label>
                                                <input class="form-control" placeholder="Address Line3" id="permanent_address_line3" name="permanent_address_line3" maxlength="60" value="<?= $exe_data->exe_per_addr_ine3; ?>" title="Please enter name of Road / Street / Lane as applicable" alt="Please enter name of Road / Street / Lane as applicable" type="text">
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label class="mandatory_label">City / Village / Town <span class="mandatory_star">*</span>
                                                </label>
                                                <input class="form-control" placeholder="City / Village / Town" id="permanent_city_village_town" name="permanent_city_village_town" maxlength="60" value="<?= $exe_data->exe_per_city; ?>" title="Please select a City / Village / Town for permanent address" alt="Please select a City / Village / Town for permanent address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter valid name of City / Village / Town for permanent address" type="text" required>
                                            </div>
                                            <div class="col-xs-2">
                                                <label class="mandatory mandatory_label">State <span class="mandatory_star">*</span>
                                                </label>
                                                <span id="span_permanent_state">
                                                <?php $corre_state=$exe_data->exe_per_state; ?>
                                                <select name="permanent_state" id="permanent_state" class="input-select form-control" title="Please select appropriate State" alt="Please select appropriate State" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please select a valid State" onchange="javascript: toggle_other_state('permanent_state', 'permanent_span_other_state');" required>
                                                    <option value="35" <?php echo ($corre_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($corre_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($corre_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($corre_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($corre_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($corre_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($corre_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($corre_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($corre_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($corre_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($corre_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($corre_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($corre_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($corre_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($corre_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($corre_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($corre_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($corre_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($corre_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($corre_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($corre_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($corre_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($corre_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($corre_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($corre_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($corre_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($corre_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($corre_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($corre_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($corre_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($corre_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($corre_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($corre_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($corre_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($corre_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($corre_state == 19)?"selected":"" ?>>West Bengal</option>
                                                </select>
                                                </span>
                                            </div>
                                            <div class="col-xs-3">
                                                <span id="permanent_span_other_state" style="display:none;" validatemsg="Please enter other State for Permanent address">
                                                <label class="cnd-mandatory mandatory_label">Other State (If not listed) 
                                                    <!-- <span class="mandatory_star">*</span> -->
                                                </label>
                                                <input class="form-control" placeholder="Mention State Name" id="permanent_state_other" name="permanent_state_other" value="" title="Please enter State Name" alt="Please enter State Name" maxlength="60" type="text">
                                                </span>
                                            </div>
                                            <div class="col-xs-2">
                                                <label class="mandatory mandatory_label">Pincode / ZipCode <span class="mandatory_star">*</span>
                                                </label>
                                                <input class="form-control numeric" placeholder="Pincode" id="permanent_zipcode" name="permanent_zipcode" maxlength="6" data-validation="length" data-validation-length="5-6" data-validation-error-msg="Please enter valid Pincode" value="<?= $exe_data->exe_per_zip; ?>" title="Please enter PIN Code for India if Permanent Address is in India, Zip Code for address outside India" alt="Please enter PIN Code for India if Permanent Address is in India, Zip Code for address outside India" type="text" required>
                                            </div>
                                            <div class="col-xs-2">
                                                <label class="mandatory mandatory_label">Country <span class="mandatory_star">*</span>
                                                </label>
                                                <span id="span_permanent_country">
                                                            <select name="permanent_country" id="permanent_country" class="input-select form-control" title="Please select appropriate Country for Permanent Address" alt="Please select appropriate Country for Permanent Address" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please select a valid Country for permanent address" onchange="javascript: populate_states(this.value,'permanent_state', 'span_permanent_state', '0', 'toggle_other_state(\'permanent_state\', \'permanent_span_other_state\')'); toggle_other_state_country(this.value, 'permanent_span_other_state'); toggle_other_state('permanent_state', 'permanent_span_other_state');" required><option value="102" selected="selected">India</option></select>                                                            </span>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="row">
                                            <div class="col-xs-3">
                                                    <label class="cnd-mandatory mandatory_label">ALTE
                                                        <!-- <span class="mandatory_star">*</span> -->
                                                    </label>
                                                    <select name="" id="" class="input-select form-control" title="">
                                                        <option value="">Please select</option>
                                                        <option value="1" selected="selected">Option 1</option>
                                                        <option value="2">Option 2</option>
                                                        <option value="3">Option 3</option>
                                                    </select>
                                                </div>
                                                <div class="col-xs-3">
                                            <label class="mandatory mandatory_label">Relation of executor with testor ?<span class="mandatory_star">*</span>
                                            </label>
                                            <select name="rel_with_testator" id="rel_with_testator" class="input-select form-control" title="Please select appropriate 'Relationship with Testaor' from the dropdown list" alt="Please select appropriate 'Relationship with Testaor' from the dropdown list" data-validation="length" data-validation-length="1-33" data-validation-error-msg="Please select a Relationship with Testator" onchange="javascript: toggle_other_relation('rel_with_testator', 'span_beneficary_relationship');" required>
                                                <option value="" selected>Please select</option>
                                                        <option value="2" <?=( $exe_data->rel_with_tes == 2)? "selected":"" ?>>Spouse</option>
                                                        <option value="3" <?=( $exe_data->rel_with_tes == 3)? "selected":"" ?>>Son</option>
                                                        <option value="4" <?=( $exe_data->rel_with_tes == 4)? "selected":"" ?>>Daughter</option>
                                                        <option value="5" <?=( $exe_data->rel_with_tes == 5)? "selected":"" ?>>Mother</option>
                                                        <option value="6" <?=( $exe_data->rel_with_tes == 6)? "selected":"" ?>>Father</option>
                                                        <option value="7" <?=( $exe_data->rel_with_tes == 7)? "selected":"" ?> >Brother</option>
                                                        <option value="8" <?=( $exe_data->rel_with_tes == 8)? "selected":"" ?>>Sister</option>
                                                        <option value="19" <?=( $exe_data->rel_with_tes == 19)? "selected":"" ?>>Grand Daughter</option>
                                                        <option value="20" <?=( $exe_data->rel_with_tes == 20)? "selected":"" ?>>Grandson</option>
                                                        <option value="21" <?=( $exe_data->rel_with_tes == 21)? "selected":"" ?>>Grand Father</option>
                                                        <option value="22" <?=( $exe_data->rel_with_tes == 22)? "selected":"" ?>>Grand Mother</option>
                                                        <option value="23" <?=( $exe_data->rel_with_tes == 23)? "selected":"" ?>>Son-in-Law</option>
                                                        <option value="24" <?=( $exe_data->rel_with_tes == 24)? "selected":"" ?>>Daughter-in-law</option>
                                                        <option value="99" <?=( $exe_data->rel_with_tes == 99)? "selected":"" ?>>Other</option>
                                            </select>
                                        </div>
                                        </div>
                                        
                                    </div>
                                </div>
<!--                                 <button type="button" data-toggle="modal" data-target="#moreExecutor">Add more executors</button> -->
                                <!-- Permanent Address END -->
                            </div>
                        </div>
                    </div>
                    <!-- panel-body END -->
                </div>
            </div>
            <!-- executor END -->
            <div id="executor_details1" style="display:none;">
            </div>
            <p>&nbsp;</p>
            <div class="bottom_buttons">
                <div class="div_prev_button">
                    <input name="previous" id="btn-previous" value="« Prev" class="btn-submit btn-move" next_page="beneficiary-details" title="Please click to go on Previous page" alt="Please click to go on Previous page" type="button">
                </div>
                <div class="div_center_buttons">
                    <!-- <input name="btn-executor" id="btn-add" value="Add / Save" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " type="submit"> -->
                    <!-- <input name="add-executor" id="btn-add" value="Add / Save" class="btn-submit" title="Please click to Add / Save the entered data" alt="Please click to Add / Save the entered data" type="submit"> -->
                    <!-- <input name="update" id="btn-update" value="Update" class="btn-submit btn-submit-disabled" disabled="" type="submit"> -->
                </div>
                <div class="div_next_button" style="text-align: right;">
                    <input name="add-executor" id="btn-add" value="Save & Next »" class="btn-submit" title="Please click to Add / Save the entered data" alt="Please click to Add / Save the entered data" type="submit">
                            <!-- <input name="next" id="btn-next" value="Next »" class="btn-submit btn-move" next_page="bank-account-details" title="Please click to go to Next page" alt="Please click to go to Next page" type="button"> -->
                        </div>
                <br>
                <br>
                <br>
            </div>
            <input name="txt_other_submit" id="txt_other_submit" value="0" type="hidden">
            <input name="txt_redirect_page" id="txt_redirect_page" value="" type="hidden">
            <!-- <input name="sumbit" value="Submit" class="btn-submit btn-submit-disabled" title="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" alt="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" disabled="" type="submit"> </div> -->
            <!-- col-xs-12 END -->
            <!-- row END -->
        </form>
        
    </div>
</div>
<?php include "executor-details-add-more-popup.php"; ?>