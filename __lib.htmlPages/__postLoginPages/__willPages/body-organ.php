<?php
	include_once("../../../__lib.includes/config.inc.php");
	$wbo_data		= $willProfile->getWillDetails('will_body_organ');
    
?>
<form action="data_submit/frmForm.php" name="frmBodyOrg" id="frmBodyOrg" method="post" class="frmCurrent">
    <input type="hidden" name="form_id" value="9">
    <div class="agreement-row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="bnkacAddWrap">
                        <div>
                            <!-- do not delte this div -->
                            <h3>Body Organ</h3>
                            <div class="panel panel-body sub-panel">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Body Organ Name <span class="mandatory_star">*</span></label>
                                        <br>
                                        <input type="text" class="form-control" name="borgan_name" value="<?= $wbo_data->borgan_name; ?>" placeholder="Policy Number" maxlength="20" title="Please enter Policy number" alt="Please enter Policy number" data-validation="" data-validation-length="1-20" data-validation-error-msg="Please enter valid Policy number" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        <label class="mandatory mandatory_label">Name of hospital <span class="mandatory_star">*</span></label><br>
                                        <input type="text" class="form-control" name="borgan_hos_name" value="<?= $wbo_data->borgan_hos_name; ?>" placeholder="Policy Number" maxlength="20" title="Please enter Policy number" alt="Please enter Policy number" data-validation="" data-validation-length="1-20" data-validation-error-msg="Please enter valid Policy number" required/>
                                    </div>
                                </div>
                                <div class="divider"></div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <label class="mandatory ">Address Line1
                                        <span class="mandatory_star">*</span></label>
                                    <input class="form-control" name="bodorg_address_line1" value="<?= $wbo_data->borgan_addr_line1; ?>" placeholder="Address Line1" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line1" title="Please enter Address Line1" alt="Please enter Address Line1" type="text" required>
                                </div>
                                <div class="col-xs-4">
                                    <label class="mandatory ">Address Line2
                                        <span class="mandatory_star">*</span></label>
                                    <input class="form-control" name="bodorg_address_line2" value="<?= $wbo_data->borgan_addr_line2; ?>" placeholder="Address Line2" maxlength="60" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid Address Line2" title="Please enter Address Line2" alt="Please enter Address Line2" type="text" required>
                                </div>
                                <div class="col-xs-4">
                                    <label class="mandatory">Address Line3</label>
                                    <input class="form-control" name="bodorg_address_line3" value="<?= $wbo_data->borgan_addr_line3; ?>" placeholder="Address Line3" maxlength="60" title="Please enter Address Line3" alt="Please enter Address Line3" type="text">
                                </div>
                            </div>
                            <!-- row END -->
                            <div class="divider"></div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <label class="mandatory">City / Village / Town
                                        <!-- <span class="mandatory_star">*</span> --></label>
                                    <input class="form-control" name="bodorg_city_village_town" value="<?= $wbo_data->borgan_city; ?>" placeholder="City / Village / Town" maxlength="60" title="Please enter City / Village / Town" alt="Please enter City / Village / Town" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please enter valid City / Village / Town" type="text">
                                </div>
                                <div class="col-xs-2">
                                    <label class="mandatory ">State
                                        <!-- <span class="mandatory_star">*</span> --></label>
                                    <span id="span_state">
                                        <?php $corre_state=$wbo_data->borgan_state; ?>
                                        <select name="bodorg_state" id="bodorg_state" class="input-select form-control" title="Please select appropriate State" alt="Please select appropriate State" data-validation="" data-validation-length="1-60" data-validation-error-msg="Please select a valid State">
                                        <option value="35" <?php echo ($corre_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($corre_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($corre_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($corre_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($corre_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($corre_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($corre_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($corre_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($corre_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($corre_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($corre_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($corre_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($corre_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($corre_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($corre_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($corre_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($corre_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($corre_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($corre_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($corre_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($corre_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($corre_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($corre_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($corre_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($corre_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($corre_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($corre_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($corre_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($corre_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($corre_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($corre_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($corre_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($corre_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($corre_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($corre_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($corre_state == 19)?"selected":"" ?>>West Bengal</option>
                                        </select>
                                    </span>
                                </div>
                                <div class="col-xs-3">
                                    <span id="span_state_other" style="display:none;" validatemsg="Please enter a Other State">
                      <label class="cnd-mandatory">Other State (If not listed) 
                        <!-- <span class="mandatory_star">*</span> -->
                    </label>
                                    <input class="form-control" name="bodorg_state_other" value="" placeholder="Mention State Name" maxlength="60" title="Please enter a Other State" alt="Please enter a Other State" type="text">
                                    </span>
                                </div>
                                <div class="col-xs-2">
                                    <label class="mandatory ">Pincode
                                        <!-- <span class="mandatory_star">*</span> --></label>
                                    <input class="form-control numeric" name="bodorg_zipcode" value="<?= $wbo_data->borgan_zipcode; ?>" placeholder="Pincode" maxlength="6" data-validation="" data-validation-length="6-6" data-validation-error-msg="Please enter valid Pincode." title="Please enter Pincode" alt="Please enter Pincode" type="text">
                                </div>
                                <div class="col-xs-2">
                                    <label class="mandatory ">Country
                                        <!-- <span class="mandatory_star">*</span> --></label>
                                    <select name="bodorg_country" id="bodorg_country" class="input-select form-control" title="Please select a Country" alt="Please select a Country" data-validation="" data-validation-length="1-33" data-validation-error-msg="Please select a valid Country" onchange="javascript: populate_states(this.value,'state', 'span_state', '0','toggle_other_state(\'state\',\'state_other\')'); toggle_other_state_country(this.value, 'span_state_other');;">
                                        <option value="102" selected="selected">India</option>
                                    </select>
                                </div>
                            </div>
                            </div>
                            <!-- panel-body END -->
                        </div>
                    </div>
                </div>
                <!-- col-xs-12 END -->
            </div>
            <!-- row END -->
            <input type="hidden" name="has_nominee" value="no">
            <div class="row">
                <div class="col-xs-12">
                    <div id="guardiant_main">
                    </div>
                </div>
            </div>
        
            <div class="row">&nbsp;</div>
            <div class="bottom_buttons row">
                <div class="div_prev_button">
                    <input type="button" name="previous" id="btn-previous" value="&laquo; Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="pension-fund" />
                </div>
                <div class="div_next_button" style="margin-right: -30px; text-align: right;">
                    <input type="submit" name="add_bod_organ" id="btn-add" value="Save & Next »" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " />
						</div><br><br><br>
            </div>
            <div class="row">
            </div>
        </div>
    </div>
</form>