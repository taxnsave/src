<?php

	include_once("../../../__lib.includes/config.inc.php");
	$wip_data		= $willProfile->getWillDetails('will_immovable_properties');
	$bene_data_all	= $willProfile->getWillBenDetails('will_beneficiary');
   
?>
<form action="data_submit/frmForm.php" name="frmProperty" id="frmProperty" method="post" class="frmCurrent has-validation-callback">
    <div class="agreement-row">
        <div class="container">
            <input type="hidden" name="form_id" value="6">
            <h3>Property</h3>
            <div class="row">
                <input name="immovableproperties_process" value="1" type="hidden">
                <input name="prp_id" id="prp_id" value="0" type="hidden">
                <input name="joint_account_count" id="joint_account_count" value="1" type="hidden">
                <input name="total_count" id="total_count" value="1" type="hidden">
                <input name="auth_token" id="auth_token_name" value="TVRBMkxqSXlNeTR4T1RrdU1UYzNkVGh5YjJoa016QTNOR2wyYWpWa2EyWTJZM1ZpTkd4dmJEVT0=" type="hidden">
                <input name="btn_clicked" id="btn_clicked" value="next" type="hidden">
                <div class="col-xs-12">
                    <div id="bnkacAddWrap">
                        <div>
                            <!-- do not delte this div -->
                            <!--<h3>Property Details<span>&nbsp;</span></h3> -->
                            <div class="panel panel-body">
                                <div class="row">
                                    <div class="col-xs-4">
                                    <?php $corre_state = $wip_data->prop_type; ?>
                                        <label class="mandatory">Type of the Property </label>
                                        <select class="form-control" name="property_type" id="property_type" required>
                                            <option value="">Select</option>
                                            <option value="Ag land" <?php echo ($corre_state == "Ag land")?"selected":"" ?>>Ag land</option>
                                            <option value="Non Ag. Land" <?php echo ($corre_state == "Non Ag. Land")?"selected":"" ?>>Non Ag. Land</option>
                                            <option value="C. Building" <?php echo ($corre_state == "C. Building")?"selected":"" ?>>C. Building</option>
                                            <option value="R. building" <?php echo ($corre_state == "R. building")?"selected":"" ?>>R. building</option>
                                            <option value="Factory" <?php echo ($corre_state == "Factory")?"selected":"" ?>>Factory</option>
                                            <option value="Other" <?php echo ($corre_state == "Other")?"selected":"" ?>>Other</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- row END -->
                                <div class="divider"></div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        &nbsp;&nbsp;&nbsp;
<!--                                         <input name="is_same_as_permanent" value="P" id="is_same_as_permanent" title="Please select if Property Address is same as your Permanent Address" alt="Please select if Property Address is same as your Permanent Address" class="is_property_address_same" type="checkbox">&nbsp;&nbsp;&nbsp; -->
<!--                                         <label class="mandatory">Is Property Address same as your Permanent Address 1?</label> -->
                                    </div>
                                </div>
                                <div class="row" style="display:none;">
                                    <div class="col-xs-6">
                                        &nbsp;&nbsp;&nbsp;
                                        <input name="is_same_as_permanent_or_correspondence" id="is_same_as_permanent_or_correspondence" value="" type="hidden">
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div id="populate_address">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label class="mandatory mandatory_label">Address Line1 <span class="mandatory_star">*</span></label>
                                            <input class="form-control" name="address_line1" id="address_line1" value="<?= $wip_data->prop_addr_line1; ?>" placeholder="Address Line1" maxlength="60" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter Address Line1" title="Please enter Address Line1" alt="Please enter Address Line1" type="text" required>
                                        </div>
                                        <div class="col-xs-4">
                                            <label class="mandatory mandatory_label">Address Line2 <span class="mandatory_star">*</span></label>
                                            <input class="form-control" name="address_line2" id="address_line2" value="<?= $wip_data->prop_addr_line2; ?>" placeholder="Address Line2" maxlength="60" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter Address Line2" title="Please enter Address Line2" alt="Please enter Address Line2" type="text" required>
                                        </div>
                                        <div class="col-xs-4">
                                            <label>Address Line3</label>
                                            <input class="form-control" name="address_line3" id="address_line3" value="<?= $wip_data->prop_addr_line3; ?>" placeholder="Address Line3" maxlength="60" title="Please enter Address Line3" alt="Please enter Address Line3" type="text">
                                        </div>
                                    </div>
                                    <!-- row END -->
                                    <div class="divider"></div>
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label class="mandatory_label">City / Village / Town <span class="mandatory_star">*</span></label>
                                            <input class="form-control" name="city_village_town" id="city_village_town" value="<?= $wip_data->prop_city; ?>" placeholder="City / Village / Town" maxlength="60" title="Please enter City / Village / Town" alt="Please enter City / Village / Town" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please enter City / Village / Town" type="text" required>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="mandatory mandatory_label">Pincode <span class="mandatory_star">*</span></label>
                                            <input class="form-control numeric" name="zipcode" id="zipcode" value="<?= $wip_data->prop_zipcode; ?>" placeholder="Pincode" maxlength="6" data-validation="length" data-validation-length="6-6" data-validation-error-msg="Please enter valid Pincode" title="Please enter Pincode" alt="Please enter Pincode" type="text" required>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="mandatory mandatory_label">Country <span class="mandatory_star">*</span></label>
                                            <select name="country" id="country" class="input-select form-control" title="Please select a Country" alt="Please select a Country" data-validation="length" data-validation-length="1-3" data-validation-error-msg="Please select Country" onchange="javascript: populate_states_disable(this.value,'state', 'span_state', '0','toggle_other_state(\'state\',\'state_other\')'); toggle_other_state_country(this.value, 'state_other');" required>
                                                <option value="102" selected="selected">India</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="mandatory mandatory_label">State <span class="mandatory_star">*</span></label>
                                            <span id="span_state">
                                            <?php $curr_state=$wip_data->prop_state; ?>
                                                <select name="state" id="state" class="input-select form-control" title="Please select appropriate State" alt="Please select appropriate State" data-validation="length" data-validation-length="1-60" data-validation-error-msg="Please select a valid State" required>
                                                <option value="35" selected <?php echo ($curr_state == 35)?"selected":"" ?>>Andaman and Nicobar Islands</option>
                                        <option value="25" <?php echo ($curr_state == 25)?"selected":"" ?>>Daman and Diu</option>
                                        <option value="28" <?php echo ($curr_state == 28)?"selected":"" ?>>Andhra Pradesh</option>
                                        <option value="12" <?php echo ($curr_state == 12)?"selected":"" ?>>Arunachal Pradesh</option>
                                        <option value="18" <?php echo ($curr_state == 18)?"selected":"" ?>>Assam</option>
                                        <option value="10" <?php echo ($curr_state == 10)?"selected":"" ?>>Bihar</option>
                                        <option value="4" <?php echo ($curr_state == 4)?"selected":"" ?>>Chandigarh</option>
                                        <option value="22" <?php echo ($curr_state == 22)?"selected":"" ?>>Chhattisgarh</option>
                                        <option value="26" <?php echo ($curr_state == 26)?"selected":"" ?>>Dadra and Nagar Haveli</option>
                                        <option value="7" <?php echo ($curr_state == 7)?"selected":"" ?>>Delhi</option>
                                        <option value="30" <?php echo ($curr_state == 30)?"selected":"" ?>>Goa</option>
                                        <option value="24" <?php echo ($curr_state == 24)?"selected":"" ?>>Gujarat</option>
                                        <option value="6" <?php echo ($curr_state == 6)?"selected":"" ?> >Haryana</option>
                                        <option value="2" <?php echo ($curr_state == 2)?"selected":"" ?>>Himachal Pradesh</option>
                                        <option value="1" <?php echo ($curr_state == 1)?"selected":"" ?>>Jammu and Kashmir</option>
                                        <option value="20" <?php echo ($curr_state == 20)?"selected":"" ?>>Jharkhand</option>
                                        <option value="29" <?php echo ($curr_state == 29)?"selected":"" ?>>Karnataka</option>
                                        <option value="32" <?php echo ($curr_state == 32)?"selected":"" ?>>Kerala</option>
                                        <option value="31" <?php echo ($curr_state == 31)?"selected":"" ?>>Lakshadweep</option>
                                        <option value="23" <?php echo ($curr_state == 23)?"selected":"" ?>>Madhya Pradesh</option>
                                        <option value="27" <?php echo ($curr_state == 27)?"selected":"" ?>>Maharashtra</option>
                                        <option value="14" <?php echo ($curr_state == 14)?"selected":"" ?>>Manipur</option>
                                        <option value="17" <?php echo ($curr_state == 17)?"selected":"" ?>>Meghalaya</option>
                                        <option value="15" <?php echo ($curr_state == 15)?"selected":"" ?>>Mizoram</option>
                                        <option value="13" <?php echo ($curr_state == 13)?"selected":"" ?>>Nagaland</option>
                                        <option value="21" <?php echo ($curr_state == 21)?"selected":"" ?>>Odisha</option>
                                        <option value="34" <?php echo ($curr_state == 34)?"selected":"" ?>>Puducherry</option>
                                        <option value="3" <?php echo ($curr_state == 3)?"selected":"" ?>>Punjab</option>
                                        <option value="8" <?php echo ($curr_state == 8)?"selected":"" ?>>Rajasthan</option>
                                        <option value="11" <?php echo ($curr_state == 11)?"selected":"" ?>>Sikkim</option>
                                        <option value="33" <?php echo ($curr_state == 33)?"selected":"" ?>>Tamil Nadu</option>
                                        <option value="36" <?php echo ($curr_state == 36)?"selected":"" ?>>Telangana</option>
                                        <option value="16" <?php echo ($curr_state == 16)?"selected":"" ?>>Tripura</option>
                                        <option value="5" <?php echo ($curr_state == 5)?"selected":"" ?>>Uttarakhand</option>
                                        <option value="9" <?php echo ($curr_state == 9)?"selected":"" ?>>Uttar Pradesh</option>
                                        <option value="19" <?php echo ($curr_state == 19)?"selected":"" ?>>West Bengal</option>
                                            </select>
                                        </span>
                                        </div>
                                        <!-- <div class="col-xs-3">
                                            <span id="span_state_other" style="display:none" validatemsg="Please enter Other State">
                            <label class="cnd-mandatory mandatory_label">Other State (If not listed)<span class="mandatory_star">*</span></label>
                                            <input class="form-control" name="state_other" value="" placeholder="Mention State Name" maxlength="60" title="Please enter Other State" alt="Please enter Other State" type="text" required>
                                            </span>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label class="mandatory mandatory_label">Measurement of the property <span class="mandatory_star">*</span></label>
                                        <input class="form-control" value="<?php echo $wip_data->prop_mes; ?>" name="prop_measurement" id="prop_measurement"  type="text">
                                    </div>
                                    <div class="col-xs-3">
                                    <?php $corre_state = $wip_data->prop_own_status; ?>
                                        <label class="mandatory mandatory_label">Owner Status<span class="mandatory_star">*</span></label>
                                        <select name="ownership_status" id="ownership_status" class="form-control" required>
                                            <option value="">Select one</option>
                                            <option value="single" <?php echo ($curr_state == "single")?"selected":"" ?>>Single</option>
                                            <option value="joint" <?php echo ($curr_state == "joint")?"selected":"" ?>>Joint</option>
                                        </select>
                                        
                                    </div>
                                    <div class="col-xs-3">
                                        <label class="mandatory mandatory_label">Owner status in %<span class="mandatory_star">*</span></label>
                                          <input class="form-control" value="<?php echo $wip_data->prop_own_perc; ?>" name="ownership_perc" id="ownership_perc"  type="text">
                                        
                                    </div>
                                </div>
                                <!-- <div class="row">
                                    <div class="col-xs-3">
                                                <label>Loan on the Property in Lacs (<img src="https://www.ezeewill.com/images/rupee.png" alt="" />)</label>
                                                <input type="text" class="form-control numeric_double" name="loan_on_property" value="0.00" placeholder="Loan on Property" maxlength="19" title="Please enter Loan on property" alt="Please enter Loan on property">
                                            </div> 
                                    <div class="col-xs-3">
                                        &nbsp;&nbsp;&nbsp;
                                        <input name="is_this_prop_mortgaged" id="is_this_prop_mortgaged" value="1" title="Please select if this Property is Mortgaged" alt="Please select if this Property is Mortgaged" type="checkbox">&nbsp;&nbsp;&nbsp;
                                        <label class="mandatory">Is this Property Mortgaged?</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <span id="span_prop_mortgaged_name" style="display:none;">
                          <label class="mandatory mandatory_label">Name with whom it is Mortgaged <span class="mandatory_star">*</span></label>
                                        <textarea class="form-control" name="prop_mortgaged_name" title="Please enter the Name with whom it is Mortgaged" alt="Please enter the Name with whom it is Mortgaged" id="prop_mortgaged_name" maxlength="200" data-validation="" data-validation-length="" data-validation-error-msg=""></textarea>
                                        </span>
                                    </div>
                                </div> -->
                                <div class="divider"></div>
                                <!-- <div class="row">
                                    <div class="col-xs-6">
                                        <label class="mandatory">Mobile Number </label>
                                        <div id="MobileAddWrap_1">
                                            <div class="row" id="mobile_1_1">
                                                <div class="col-xs-2">
                                                    <input class="form-control numeric" name="ejd_mobile_isd[]" id="ejd_mobile_isd_1" value="91" placeholder="ISD" maxlength="3" data-validation="" data-validation-length="2-3" data-validation-error-msg="Please enter valid Mobile ISD" title="Please enter Mobile ISD for Joint account holder" alt="Please enter Mobile ISD for Joint account holder" type="text">
                                                </div>
                                                <div class="col-xs-8 last">
                                                    <input class="form-control numeric" name="ejd_mobile_number[]" id="ejd_mobile_number_1" value="<?= $wip_data->prop_mob_no; ?>" placeholder="Mobile" maxlength="10" data-validation="" data-validation-length="8-10" data-validation-error-msg="Please enter mobile no." title="Please enter Mobile Number for Joint account holder" alt="Please enter Mobile Number for Joint account holder" type="text">
                                                </div>
                                                <a class=" btn-add a_mobile" id="AddMoreFileBoxMobile" href="javascript:void(0);" rec_id="1" title="Click to Add Mobile" alt="Click to Add Mobile">+</a>
                                            </div>
                                            <input id="actual_mobile_count_1" value="1" type="hidden">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <label class="mandatory">Email</label>
                                        <div id="JahEmailAddWrap_1">
                                            <div class="row" id="jah_email_1_1">
                                                <div class="col-xs-8 last">
                                                    <input class="form-control" name="ejd_email[]" id="ejd_email_1" value="<?= $wip_data->prop_email; ?>" placeholder="Email" maxlength="254" data-validation="" data-validation-length="1-254" data-validation-error-msg="Please enter valid Email for Joint account holder" title="Please enter Email for Joint account holder" alt="Please enter Email for Joint account holder" type="text">
                                                </div>
                                                <a class=" btn-add a_jah_email" id="AddMoreFileBoxJahEmail" href="javascript:void(0);" rec_id="1" title="Click to Add Email" alt="Click to Add Email">+</a>
                                            </div>
                                            <input id="actual_email_count_1" value="1" type="hidden">
                                        </div>
                                    </div>
                                </div> -->
                                <div class="divider"></div>
                                <!-- <div class="row">
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <label class="cnd-mandatory mandatory_label">Date of Birth <span class="mandatory_star">*</span></label>
                                                <input name="ejd_birth_date" id="show_ejd_birth_date_1" class="form-control hasDatepicker" value="<?= $wip_data->prop_d_o_b; ?>" placeholder="Birth date" title="Please select a Birth date" alt="Please select a Birth date" ac_id="1" type="text">
                                            </div>
                                            <div class="col-xs-1">
                                                <br>
                                                <label class="cnd-mandatory">OR</label>
                                            </div>
                                            <div class="col-xs-2">
                                                <label class="cnd-mandatory mandatory_label">Adult / Minor <span class="mandatory_star">*</span></label>
                                                <select class="input-select form-control jah_is_adult" id="ejd_is_adult_1" name="ejd_is_adult[]" title="Please select if you are Adult/Minor" alt="Please select if you are Adult/Minor" ac_id="1">
                                                    <option value="1" selected="">Adult</option>
                                                    <option value="0">Minor</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <!-- panel-body END -->
                            <div class="row">&nbsp;</div>
                            <div id="div_beneficiary">
                                <div id="div_beneficiary_1">
                                    <h4>
              <strong>Beneficiaries</strong>
            </h4>
                                    <div class="panel panel-body sub-panel">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <label class="mandatory mandatory_label">Beneficiary 
                                                <!-- <span class="mandatory_star">*</span> -->
                                                </label>
                                                <select name="sel_beneficiary" id="sel_beneficiary" class="input-select form-control" title="Please select a Beneficiary" alt="Please select a Beneficiary">
                                                    <option value="">Please select</option>
                                                    <?php 
                                                $cnt=1;
                                                while($row = mysql_fetch_array($bene_data_all)) {
                                                    $html = "<option value='" . $cnt . "'>" .$row['f_name'] . "</option>";
                                                    
                                                    $cnt+= 1;
                                                    echo $html;
                                                } 
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-4">
                                                <label class="mandatory mandatory_label">Percentage Share to be alloted 
                                                <span class="mandatory_star">*</span>
                                                </label>
                                                <input class="form-control numeric_share" name="txt_beneficiary_share" id="txt_beneficiary_share" placeholder="Percentage Share to be alloted" maxlength="5" title="Please enter Percentage Share to be alloted" alt="Please enter Percentage Share to be alloted" value="" type="text">
                                            </div>
                                        </div>
                                        <div>
                                            <br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add Beneficiary" title="Add Beneficiary" id="beneficiaryAddBtnNew" class="btn btn-primary btn-sm btn-add-large a_beneficiarynew">+ Add Beneficiary</a></span>
                                        </div>
                                        <br>
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Share</th>
                                                    <th>Relation</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tb_con_ben">
                                                <tr id="no_beneficiary">
                                                    <td colspan="5"></td>
                                                </tr>
                                                <input name="tot_ben" id="tot_ben" value="1" type="hidden">
                                                <input id="total_share" value="0" type="hidden">
                                            </tbody>
                                        </table>                   
                                    </div>
                                    <div>
<!--                                         <br><span id="span_beneficiary_1"><a href="javascript:void(0);" alt="Add More Beneficiary" title="Add More Beneficiary" id="" class="btn btn-success btn-sm">+ Add More</a></span> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- row END -->
                        <div class="row">&nbsp;</div>
                        <div class="bottom_buttons row">
                            <div class="div_prev_button">
                                <input name="previous" id="btn-previous" value="« Prev" title="Click here to go on Previous page" alt="Click here to go on Previous page" class="btn-submit btn-move" next_page="public-provident-fund" type="button">
                            </div>
                            <div class="div_center_buttons">
                                <!-- <input name="add_immovable" id="btn-add" value="Add / Save" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " type="submit"> -->
                                <!-- <input name="update" id="btn-update" value="Update" title="Click here to Update the changed data" alt="Click here to Update the changed data" class="btn-submit btn-submit-disabled" disabled="" type="submit"> -->
                            </div>
                            <div class="div_next_button" style="margin-right: -30px; text-align: right;">
                                <input name="add_immovable" id="btn-add" value="Save & Next »" title="Click here to Add / Save the entered data" alt="Click here to Add / Save the entered data" class="btn-submit " type="submit">
              <!-- <input name="next" id="btn-next" value="Next »" title="Click here to go to Next page" alt="Click here to go to Next page" class="btn-submit btn-move" next_page="mutual-funds-details" type="button"> -->
            </div><br><br><br>
                        </div>
                        <!-- <div class="row">
          <input name="txt_other_submit" id="txt_other_submit" value="0" type="hidden">
<input name="txt_redirect_page" id="txt_redirect_page" value="" type="hidden">
<input name="sumbit" value="Submit" class="btn-submit btn-submit-disabled" title="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" alt="Submit Button will be enabled after completion of compulsory tabs marked with top orange border" disabled="" type="submit">                            </div> -->
                        <div class="row">&nbsp;</div>
                        <div class="divider"></div>
                        <!-- <div class="row">
                            <div class="col-xs-12">
                                <input name="all_securities" id="all_securities" value="1" title="Please select if All Physical Shares held by the user is to be distributed as per the Beneficiary details provided below." alt="Please select if All Physical Shares held by the user is to be distributed as per the Beneficiary details provided below." type="checkbox">&nbsp;&nbsp;&nbsp;
                                <label>All Physical Shares held by the user is to be distributed as per the Beneficiary details provided below.</label>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <!-- gen-row END -->
            <!-- <script type="text/javascript" src="/js/stocks-bonds-details.js" lang="javascript"></script>
            <script src="/js/form-validator/jquery.form-validator.js"></script>
            <script src="/js/jquery.numeric.js"></script>
            <link media="all" type="text/css" rel="stylesheet" href="/css/jquery-ui.css">
            <script type="text/javascript" src="/js/jquery-ui.js" lang="javascript"></script> -->
            <!-- <script>
            account_count = 1;
            actual_account_count = 1;
            beneficiary_count = 1;
            actual_beneficiary_count = 1;

            $(window).load(function() {
                for (var i = 1; i <= account_count; i++) {
                    var temp = $('#actual_phone_count_' + i).val();
                    actual_phone_count[i] = $('#actual_phone_count_' + i).val();
                    actual_office_phone_count[i] = $('#actual_office_count_' + i).val();
                    actual_mobile_count[i] = $('#actual_mobile_count_' + i).val();
                    actual_email_count[i] = $('#actual_email_count_' + i).val();
                    actual_fax_count[i] = $('#actual_fax_count_' + i).val();
                }
            });
            </script>
            <script>
            $(document).ready(function() {
                $.validate({ form: "#frmBankAccount", onSuccess: validate_stocksbonds });
            });
            $('#btn-update').click(function() {
                $.validate({ form: "#frmBankAccount", onSuccess: validate_stocksbonds });
            });
            $('#btn-add').click(function() {
                $.validate({ form: "#frmBankAccount", onSuccess: validate_stocksbonds });
            });
            </script> -->
        </div>
    </div>
</form>