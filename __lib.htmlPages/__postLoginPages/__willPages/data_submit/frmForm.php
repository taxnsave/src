<?php
include_once("../../../../__lib.includes/config.inc.php");
include 'toDatabase.php';
extract($_POST);
$obj = new Update();
//print_r($CONFIG->loggedUserId);
// personla information
if(isset($_POST['next']) || isset($_POST['update']) )
{
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_personal_information';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'pi_panaadharnum' => trim($panaadharnum),
    'pi_title' => trim($title),
    'pi_f_name' => trim($firstname),
    'pi_m_name' => trim($middlename),
    'pi_l_name' => trim($lastname),
    'pi_gd_rel' => trim($father_mother_husband_relation),
    'pi_gd_title' => trim($father_mother_husband_title),
    'pi_gd_f_name' => trim($father_mother_husband_firstname),
    'pi_gd_mid_name' => trim($father_mother_husband_middlename),
    'pi_gd_l_name' => trim($father_mother_husband_lastname),
    'pi_d_o_b' => trim($birth_date),
    'pi_per_info_age' => trim($per_info_age),
    'pi_adult' => trim($is_adult),
    'pi_gender' => trim($gender),
    'pi_religion' => trim($pi_religion),
    'pi_religion_other' => trim($pi_religion_other),
    // 'pi_marital' => trim($marital_status),
    // 'pi_anniv_date' => trim($anniversary_date),
    'pi_panaadharnumper' => trim($panaadharnumper),
    'pi_per_addr_line1' => trim($permanent_address_line1),
    'pi_per_addr_line2' => trim($permanent_address_line2),
    'pi_per_addr_line3' => trim($permanent_address_line3),
    'pi_per_city' => trim($permanent_city_village_town),
    'pi_per_zip' => trim($permanent_zipcode),
    'pi_per_country' => trim($permanent_country),
    'pi_per_state' => trim($permanent_state),
    // 'per_state_oth' => trim($permanent_state_other),
    'core_same_per' => trim($is_correspondence_same_as_permanent),
    'corre_addr_line1' => trim($correspondence_address_line1),
    'corre_addr_line2' => trim($correspondence_address_line2),
    'corre_addr_line3' => trim($correspondence_address_line3),
    'corre_city' => trim($correspondence_city_village_town),
    'corre_zip' => trim($correspondence_zipcode),
    'corre_country' => trim($correspondence_country),
    'corre_state' => trim($correspondence_state),
    'corre_other_state' => trim($correspondence_state_other),
    // 'tel_isd' => trim(implode(',',$phone_isd)),
    // 'tel_std' => trim(implode(',',$phone_std)),
    // 'tel_no' => trim(implode(',',$phone_number)),
    // 'off_no_isd' => trim(implode(',',$office_phone_isd)),
    // 'off_no_std' => trim(implode(',',$office_phone_std)),
    // 'off_no' => trim(implode(',',$office_phone_number)),
    'mob_isd' => trim(implode(',',$mobile_isd)),
    'mob_no' => trim(implode(',',$mobile_number)),
    'email' => trim($email),
    'landlineno' => trim($landlineno),
    'pan' => trim($pancard),
    'aadhaar' => trim($adharcard),
    'nationality' => trim($nationality),
    // 'other_nationality' => trim($other_nationality),
    'pi_place' => trim($per_place),
    'pi_date' => trim($per_date),
    // 'caste' => trim($caste),
    'creation_date' => date("Y-m-d H:m:s a")
  );
  //print_r($dataarray);
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=bene";
}
//start page 
else if(isset($_POST['start'])) {
    $redirectdata = "&newpage=pi";
    
}
// beneficiary form
else if(isset($_POST['add']))
{
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_beneficiary';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    // 'is_individual' => trim($is_individual),
    'title' => trim($title),
    'f_name' => trim($firstname),
    'm_name' => trim($middlename),
    'l_name' => trim($lastname),
    'date_of_birth' => trim($birth_date),
    'ben_age' => trim($ben_age),
    // 'minor' => trim($is_minor),
    // 'gender' => trim($gender),
    // 'mentally' => trim($mentally_incapacitated),
    'rel_with_testator' => trim($relationship_with_testator),
    // 'other_rel' => trim($other_relationship),
    // 'per_same_per' => trim($permanent_addr_copied_from),
    // 'per_co_from' => trim($permanent_addr_copied_from),
    'per_addr_line1' => trim($permanent_address_line1),
    'per_addr_line2' => trim($permanent_address_line2),
    'per_addr_line3' => trim($permanent_address_line3),
    'per_city' => trim($permanent_city_village_town),
    'per_zip' => trim($permanent_zipcode),
    'per_country' => trim($permanent_country),
    'per_state' => trim($permanent_state),
    // 'per_state_oth' => trim($permanent_state_other),
    'corre_co_from' => trim($correspondence_addr_copied_from),
    'corre_addr_line1' => trim($correspondence_address_line1),
    'corre_addr_line2' => trim($correspondence_address_line2),
    'corre_addr_line3' => trim($correspondence_address_line3),
    'corre_city' => trim($correspondence_city_village_town),
    'corre_zip' => trim($correspondence_zipcode),
    'corre_country' => trim($correspondence_country),
    'corre_state' => trim($correspondence_state),
    // 'corre_other_state' => trim($correspondence_state_other),
    // 'tel_isd' => trim(implode(',',$phone_isd)),
    // 'tel_std' => trim(implode(',',$phone_std)),
    // 'tel_no' => trim(implode(',',$phone_number)),
    // 'off_no_isd' => trim(implode(',',$office_phone_isd)),
    // 'off_no_std' => trim(implode(',',$office_phone_std)),
    // 'off_no' => trim(implode(',',$office_phone_number)),
    // 'mob_isd' => trim(implode(',',$mobile_isd)),
    // 'mob_no' => trim(implode(',',$mobile_number)),
    // 'email' => trim(implode(',',$email)),
    // 'religion' => trim($religion),
    // 'religion_other' => trim($religion_other),
    // 'caste' => trim($caste),
    'gd_title' => trim($guardian_title),
    'gd_f_name' => trim($guardian_firstname),
    'gd_m_name' => trim($guardian_middlename),
    'gd_l_name' => trim($guardian_lastname),

    'gd_fat_title'  => trim($guardian_fat_title),
    'gd_fat_f_name' => trim($guardian_fat_firstname),
    'gd_fat_m_name' => trim($guardian_fat_middlename),
    'gd_fat_l_name' => trim($guardian_fat_lastname),

    'gd_d_of_b' => trim($guardian_birth_date),
    'gd_age' => trim($gd_age),
    'gd_is_adult' => trim($guardian_is_adult),
    'gd_gender' => trim($guardian_gender),
    'gd_nationality' => trim($guardian_nationality),
    'gd_occupation' => trim($guardian_occupation),
    'gd_religious' => trim($guardian_religious),
    //'gd_per_addr_co_from' => trim($guardian_permanent_addr_copied_from),
    'gd_per_addr_line1' => trim($guardian_permanent_address_line1),
    'gd_per_addr_line2' => trim($guardian_permanent_address_line2),
    'gd_per_addr_line3' => trim($guardian_permanent_address_line3),
    'gd_per_city' => trim($guardian_permanent_city_village_town),
    'gd_per_zip' => trim($guardian_permanent_zipcode),
    'gd_per_country' => trim($guardian_permanent_country),
    'gd_per_state' => trim($guardian_permanent_state),
    // 'gd_per_state_oth' => trim($guardian_permanent_state_other),
    'is_gd_corre_s_as_gd_per' => trim($is_guardian_correspondence_same_as_guardian_permanent),
    'gd_corre_addr_co_from' => trim($guardian_correspondence_addr_copied_from),
    'gd_corre_addr_line1' => trim($guardian_correspondence_address_line1),
    'gd_corre_addr_line2' => trim($guardian_correspondence_address_line2),
    'gd_corre_addr_line3' => trim($guardian_correspondence_address_line3),
    'gd_corre_city' => trim($guardian_correspondence_city_village_town),
    'gd_corre_zip' => trim($guardian_correspondence_zipcode),
    'gd_corre_country' => trim($guardian_correspondence_country),
    'gd_corre_state' => trim($guardian_correspondence_state),
    // 'gd_corre_state_oth' => trim($guardian_correspondence_state_other),
    'gd_bene_rel' => trim($guardian_beneficary_relationship),
    // 'gd_bene_rel_oth' => trim($guardian_beneficary_other_relationship)
    // 'gd_tel_isd' => trim(implode(',',$guardian_phone_isd)),
    // 'gd_tel_std' => trim(implode(',',$guardian_phone_std)),
    // 'gd_tel_no' => trim(implode(',',$guardian_phone_number)),
    // 'gd_off_no_isd' => trim(implode(',',$guardian_office_phone_isd)),
    // 'gd_off_no_std' => trim(implode(',',$guardian_office_phone_std)),
    // 'gd_off_no' => trim(implode(',',$guardian_office_phone_number)),
    // 'gd_mob_no_isd' => trim(implode(',',$guardian_mobile_isd)),
    // 'gd_mob_no' => trim(implode(',',$guardian_mobile_number)),
    // 'gd_email' => trim(implode(',',$guardian_email)),
    // 'gd_other_info' => trim($other_info)
  );

  $obj->insertDbRecordnew($tbname,$dataarray);
  $redirectdata = "&newpage=bene";
}
else if(isset($_POST['editbene']))
{
    $user_id = $CONFIG->loggedUserId;
    $tbname ='will_beneficiary';
    $dataarray   = array(
        'fk_user_id' => $user_id,
        // 'is_individual' => trim($is_individual),
        'title' => trim($title),
        'f_name' => trim($firstname),
        'm_name' => trim($middlename),
        'l_name' => trim($lastname),
        'date_of_birth' => trim($birth_date),
        'ben_age' => trim($ben_age),
        // 'minor' => trim($is_minor),
        // 'gender' => trim($gender),
        // 'mentally' => trim($mentally_incapacitated),
        'rel_with_testator' => trim($relationship_with_testator),
        // 'other_rel' => trim($other_relationship),
        // 'per_same_per' => trim($permanent_addr_copied_from),
        // 'per_co_from' => trim($permanent_addr_copied_from),
        'per_addr_line1' => trim($permanent_address_line1),
        'per_addr_line2' => trim($permanent_address_line2),
        'per_addr_line3' => trim($permanent_address_line3),
        'per_city' => trim($permanent_city_village_town),
        'per_zip' => trim($permanent_zipcode),
        'per_country' => trim($permanent_country),
        'per_state' => trim($permanent_state),
        // 'per_state_oth' => trim($permanent_state_other),
        'corre_co_from' => trim($correspondence_addr_copied_from),
        'corre_addr_line1' => trim($correspondence_address_line1),
        'corre_addr_line2' => trim($correspondence_address_line2),
        'corre_addr_line3' => trim($correspondence_address_line3),
        'corre_city' => trim($correspondence_city_village_town),
        'corre_zip' => trim($correspondence_zipcode),
        'corre_country' => trim($correspondence_country),
        'corre_state' => trim($correspondence_state),
        // 'corre_other_state' => trim($correspondence_state_other),
        // 'tel_isd' => trim(implode(',',$phone_isd)),
        // 'tel_std' => trim(implode(',',$phone_std)),
        // 'tel_no' => trim(implode(',',$phone_number)),
        // 'off_no_isd' => trim(implode(',',$office_phone_isd)),
        // 'off_no_std' => trim(implode(',',$office_phone_std)),
        // 'off_no' => trim(implode(',',$office_phone_number)),
        // 'mob_isd' => trim(implode(',',$mobile_isd)),
        // 'mob_no' => trim(implode(',',$mobile_number)),
        // 'email' => trim(implode(',',$email)),
        // 'religion' => trim($religion),
        // 'religion_other' => trim($religion_other),
        // 'caste' => trim($caste),
        'gd_title' => trim($guardian_title),
        'gd_f_name' => trim($guardian_firstname),
        'gd_m_name' => trim($guardian_middlename),
        'gd_l_name' => trim($guardian_lastname),
        
        'gd_fat_title'  => trim($guardian_fat_title),
        'gd_fat_f_name' => trim($guardian_fat_firstname),
        'gd_fat_m_name' => trim($guardian_fat_middlename),
        'gd_fat_l_name' => trim($guardian_fat_lastname),
        
        'gd_d_of_b' => trim($guardian_birth_date),
        'gd_age' => trim($gd_age),
        'gd_is_adult' => trim($guardian_is_adult),
        'gd_gender' => trim($guardian_gender),
        'gd_nationality' => trim($guardian_nationality),
        'gd_occupation' => trim($guardian_occupation),
        'gd_religious' => trim($guardian_religious),
        //'gd_per_addr_co_from' => trim($guardian_permanent_addr_copied_from),
        'gd_per_addr_line1' => trim($guardian_permanent_address_line1),
        'gd_per_addr_line2' => trim($guardian_permanent_address_line2),
        'gd_per_addr_line3' => trim($guardian_permanent_address_line3),
        'gd_per_city' => trim($guardian_permanent_city_village_town),
        'gd_per_zip' => trim($guardian_permanent_zipcode),
        'gd_per_country' => trim($guardian_permanent_country),
        'gd_per_state' => trim($guardian_permanent_state),
        // 'gd_per_state_oth' => trim($guardian_permanent_state_other),
        'is_gd_corre_s_as_gd_per' => trim($is_guardian_correspondence_same_as_guardian_permanent),
        'gd_corre_addr_co_from' => trim($guardian_correspondence_addr_copied_from),
        'gd_corre_addr_line1' => trim($guardian_correspondence_address_line1),
        'gd_corre_addr_line2' => trim($guardian_correspondence_address_line2),
        'gd_corre_addr_line3' => trim($guardian_correspondence_address_line3),
        'gd_corre_city' => trim($guardian_correspondence_city_village_town),
        'gd_corre_zip' => trim($guardian_correspondence_zipcode),
        'gd_corre_country' => trim($guardian_correspondence_country),
        'gd_corre_state' => trim($guardian_correspondence_state),
        // 'gd_corre_state_oth' => trim($guardian_correspondence_state_other),
        'gd_bene_rel' => trim($guardian_beneficary_relationship),
        // 'gd_bene_rel_oth' => trim($guardian_beneficary_other_relationship)
        // 'gd_tel_isd' => trim(implode(',',$guardian_phone_isd)),
        // 'gd_tel_std' => trim(implode(',',$guardian_phone_std)),
        // 'gd_tel_no' => trim(implode(',',$guardian_phone_number)),
        // 'gd_off_no_isd' => trim(implode(',',$guardian_office_phone_isd)),
        // 'gd_off_no_std' => trim(implode(',',$guardian_office_phone_std)),
        // 'gd_off_no' => trim(implode(',',$guardian_office_phone_number)),
        // 'gd_mob_no_isd' => trim(implode(',',$guardian_mobile_isd)),
        // 'gd_mob_no' => trim(implode(',',$guardian_mobile_number)),
        // 'gd_email' => trim(implode(',',$guardian_email)),
        // 'gd_other_info' => trim($other_info)
    );
    echo $curr_bene_id;
    $obj->updateBenDbRecord($tbname,$dataarray,$curr_bene_id);
    $redirectdata = "&newpage=bene";
}
else if(isset($_POST['benenext']))
{
    $redirectdata = "&newpage=exec";
}
// executor form
else if(isset($_POST['add-executor'])){

  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_executor';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    // 'exe_appo_sta' => trim($is_individual),
    'exe_title' => trim($executor_title),
    'exe_f_name' => trim($firstname),
    'exe_m_name	' => trim($middlename),
    'exe_l_name' => trim($lastname),

    'exe_fat_title' => trim($executor_fat_name_title),
    'exe_fat_f_name' => trim($executor_fat_firstname),
    'exe_fat_m_name ' => trim($executor_fat_middlename),
    'exe_fat_l_name' => trim($executor_fat_lastname),

    'exe_nationality' => trim($executor_nationality),
    'exe_occupation' => trim($executor_occupation),
    'exe_religious' => trim($executor_religious),
    // 'exe_d_o_b' => trim($birth_date),
    // 'exe_is_adult' => trim($is_minor),
    'exe_gender' => trim($gender),
    'exe_age' => trim($executor_age),
    'exe_rel_with_tes' => trim($relationship_with_testator),
    'rel_with_tes' => trim($rel_with_testator),
    'exe_other_rel' => trim($other_relationship),
    'exe_per_adr_is_same_per' => trim($permanent_is_same_as_permanent),
    'exe_per_is_same_corre' => trim($permanent_is_same_as_correspondence),
    'exe_per_addr_co_frm' => trim($permanent_addr_copied_from),
    // 'exe_per_is_forei_addr' => trim($permanent_is_foreign_address),
    'exe_per_addr_line1' => trim($permanent_address_line1),
    'exe_per_addr_line2' => trim($permanent_address_line2),
    'exe_per_addr_ine3' => trim($permanent_address_line3),
    'exe_per_city' => trim($permanent_city_village_town),
    'exe_per_zip' => trim($permanent_zipcode),
    'exe_per_country' => trim($permanent_country),
    'exe_per_state' => trim($permanent_state),
    // 'exe_per_state_oth' => trim($permanent_state_other)
    // 'exe_is_corre_sa_as_per' => trim($is_correspondence_same_as_permanent),
    // 'exe_corre_is_same_as_per' => trim($correspondence_is_same_as_permanent),
    // 'exe_corre_is_same_as_corre' => trim($correspondence_is_same_as_correspondence),
    // 'exe_corre_addr_co_frm' => trim($correspondence_addr_copied_from),
    // 'exe_corre_is_fore_addr' => trim($correspondence_is_foreign_address),
    // 'exe_corre_addr_line1' => trim($correspondence_address_line1),
    // 'exe_corre_addr_line2' => trim($correspondence_address_line2),
    // 'exe_corre_addr_line3' => trim($correspondence_address_line3),
    // 'exe_corre_city' => trim($correspondence_city_village_town),
    // 'exe_corre_zip' => trim($correspondence_zipcode),
    // 'exe_corre_country' => trim($correspondence_country),
    // 'exe_corre_state' => trim($correspondence_state),
    // 'exe_corre_state_oth' => trim($correspondence_state_other),
    // 'exe_tel_isd' => trim(implode(',',$phone_isd)),
    // 'exe_tel_std' => trim(implode(',',$phone_std)),
    // 'exe_tel_no' => trim(implode(',',$phone_number)),
    // 'exe_off_no_isd' => trim(implode(',',$office_phone_isd)),
    // 'exe_off_no_std' => trim(implode(',',$office_phone_std)),
    // 'exe_off_no' => trim(implode(',',$office_phone_number)),
    // 'exe_mob_no_isd' => trim(implode(',',$mobile_isd)),
    // 'exe_mob_no' => trim(implode(',',$mobile_number)),
    // 'exe_email' => trim(implode(',',$email)),
  );

  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=ba&subpage2=ba";
}
// // bank information
else if(isset($_POST['bank-btn'])){

  //bank-account table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_bank_accounts';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'bnk_acc_no' => trim($account_number),
    'bnk_acc_type' => trim($account_type),
    // 'bnk_ifsc' => trim($ifsc_code),
    'bnk_bank_name' => trim($bank_name),
    'bnk_branch_name' => trim($branch_name),
    'bnk_acc_addr_line1' => trim($address_line1),
    'bnk_acc_addr_line2' => trim($address_line3),
    'bnk_acc_addr_line3' => trim($address_line3),
    'bnk_acc_city' => trim($city_village_town),
    'bnk_acc_zip' => trim($zipcode),
    'bnk_acc_country' => trim($country),
    'bnk_acc_state' => trim($state),
    // 'bnk_state_other' => trim($state_other)
    // 'ownership_type' => trim($ownership_type)
      'bnk_ownership_perc' => trim($ownership_perc)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=ba&subpage2=lo";
  
  
// // joint account table
//   // if ($ownership_type === "joint") {
//   //   $tbname ='will_joint_account_holder';
//   //   for($i=0; $i<count($ejd_hld_id); $i++){
//   //     $dataarray   = array(
//   //       'fk_user_id' => $user_id,
//   //       'fk_form_id' => $form_id,
//   //       'jh_title' => trim($ejd_title[$i]),
//   //       'jh_f_name' => trim($ejd_firstname[$i]),
//   //       'jh_m_name' => trim($ejd_middlename[$i]),
//   //       'jh_l_name' => trim($ejd_lastname[$i]),
//   //       'jh_same_as_per' => trim($is_same_as_permanent[$i]),
//   //       'jh_same_as_corre' => trim($is_same_as_correspondence[$i]),
//   //       'jh_sa_as_per_or_corre' => trim($is_same_as_permanent_or_correspondence[$i]),
//   //       'jh_forig_addr' => trim($ejd_is_foreign_address[$i]),
//   //       'jh_addr_line1' => trim($ejd_address_line1[$i]),
//   //       'jh_addr_line2' => trim($ejd_address_line2[$i]),
//   //       'jh_addr_line3' => trim($ejd_address_line3[$i]),
//   //       'jh_addr_city' => trim($ejd_city_village_town[$i]),
//   //       'jh_addr_zip' => trim($ejd_zipcode[$i]),
//   //       'jh_addr_country' => trim($ejd_country[$i]),
//   //       'jh_state' => trim($ejd_state[$i]),
//   //       'jh_state_oth' => trim($ejd_state_other[$i]),
//   //       'jh_tel_isd' => trim($ejd_phone_isd[$i]),
//   //       'jh_tel_std' => trim($ejd_phone_std[$i]),
//   //       'jh_tel_no' => trim($ejd_phone_number[$i]),
//   //       'jh_off_isd' => trim($ejd_office_phone_isd[$i]),
//   //       'jh_off_std' => trim($ejd_office_phone_std[$i]),
//   //       'jh_off_no' => trim($ejd_office_phone_number[$i]),
//   //       'jh_mob_isd' => trim($ejd_mobile_isd[$i]),
//   //       'jh_mob_no' => trim($ejd_mobile_number[$i]),
//   //       'jh_email' => trim($ejd_email[$i]),
//   //       'jh_d_o_b' => trim($ejd_birth_date[$i]),
//   //       'jh_adult' => trim($ejd_is_adult[$i])
//   //     );

//   //     $obj->insertDbRecord($tbname,$dataarray);
//   //   }
//   // }

// //beneficiary-percentage table
//   // $tbname ='will_beneficiary_percentage';
//   // $dataarray   = array(
//   //   'fk_user_id' => $user_id,
//   //   'fk_ben_id' => trim($sel_beneficiary),
//   //   'fk_form_id' => trim($form_id),
//   //   'bene_percentage' => trim($txt_beneficiary_share),
//   //   'ben_other_info' => trim($txt_other_info)
//   // );
//   // $obj->insertDbRecord($tbname,$dataarray);
}else if(isset($add_locker)){
  $obj = new update();

  //locker-account table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_locker_info';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'locker_number' => trim($locker_number),
    // 'locker_ifsc' => trim($ifsc_code),
    'locker_bank_name' => trim($bank_name),
    'locker_branch_name' => trim($branch_name),
    'locker_addr_line1' => trim($address_line1),
    'locker_addr_line2' => trim($address_line2),
    'locker_addr_line3' => trim($address_line3),
    'locker_city' => trim($city_village_town),
    'locker_zipcode' => trim($zipcode),
    'locker_country' => trim($country),
    'locker_state' => trim($state),
    // 'locker_oth_state' => trim($state_other),
    // 'locker_ownership_type' => trim($ownership_type)
      'locker_ownership_perc' => trim($ownership_perc_locker)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=ba&subpage2=fd";
  
// // joint account table
//   if ($ownership_type === "joint") {
//     $tbname ='will_joint_account_holder';
//     for($i=0; $i<count($ejd_hld_id); $i++){
//       // $dataarray   = array(
//       //   'fk_user_id' => $user_id,
//       //   'fk_form_id' => $form_id,
//       //   'jh_title' => trim($ejd_title[$i]),
//       //   'jh_f_name' => trim($ejd_firstname[$i]),
//       //   'jh_m_name' => trim($ejd_middlename[$i]),
//       //   'jh_l_name' => trim($ejd_lastname[$i]),
//       //   'jh_same_as_per' => trim($is_same_as_permanent[$i]),
//       //   'jh_same_as_corre' => trim($is_same_as_correspondence[$i]),
//       //   'jh_sa_as_per_or_corre' => trim($is_same_as_permanent_or_correspondence[$i]),
//       //   'jh_forig_addr' => trim($ejd_is_foreign_address[$i]),
//       //   'jh_addr_line1' => trim($ejd_address_line1[$i]),
//       //   'jh_addr_line2' => trim($ejd_address_line2[$i]),
//       //   'jh_addr_line3' => trim($ejd_address_line3[$i]),
//       //   'jh_addr_city' => trim($ejd_city_village_town[$i]),
//       //   'jh_addr_zip' => trim($ejd_zipcode[$i]),
//       //   'jh_addr_country' => trim($ejd_country[$i]),
//       //   'jh_state' => trim($ejd_state[$i]),
//       //   'jh_state_oth' => trim($ejd_state_other[$i]),
//       //   'jh_tel_isd' => trim($ejd_phone_isd[$i]),
//       //   'jh_tel_std' => trim($ejd_phone_std[$i]),
//       //   'jh_tel_no' => trim($ejd_phone_number[$i]),
//       //   'jh_off_isd' => trim($ejd_office_phone_isd[$i]),
//       //   'jh_off_std' => trim($ejd_office_phone_std[$i]),
//       //   'jh_off_no' => trim($ejd_office_phone_number[$i]),
//       //   'jh_mob_isd' => trim($ejd_mobile_isd[$i]),
//       //   'jh_mob_no' => trim($ejd_mobile_number[$i]),
//       //   'jh_email' => trim($ejd_email[$i]),
//       //   'jh_d_o_b' => trim($ejd_birth_date[$i]),
//       //   'jh_adult' => trim($ejd_is_adult[$i])
//       // );

//       $obj->insertDbRecord($tbname,$dataarray);
//     }
//   }

// //beneficiary-percentage table
//   $tbname ='will_beneficiary_percentage';
//   // $dataarray   = array(
//   //   'fk_user_id' => $user_id,
//   //   'fk_ben_id' => trim($sel_beneficiary),
//   //   'fk_form_id' => trim($form_id),
//   //   'bene_percentage' => trim($txt_beneficiary_share),
//   //   'ben_other_info' => trim($txt_other_info)
//   // );
//   $obj->insertDbRecord($tbname,$dataarray);
// }else if(isset($add_fixed)){
//   $obj = new update();

//   //locker-account table
//   $user_id = $CONFIG->loggedUserId;
//   $tbname ='will_fd_rd_info';
//   // $dataarray   = array(
//   //   'fk_user_id' => $user_id,
//   //   'fd_account_number' => trim($fdr_no),
//   //   'fd_ifsc' => trim($ifsc_code),
//   //   'fd_bank_name' => trim($bank_name),
//   //   'fd_branch_name' => trim($branch_name),
//   //   'fd_addr_line1' => trim($address_line1),
//   //   'fd_addr_line2' => trim($address_line3),
//   //   'fd_addr_line3' => trim($address_line3),
//   //   'fd_city' => trim($city_village_town),
//   //   'fd_zipcode' => trim($zipcode),
//   //   'fd_country' => trim($country),
//   //   'fd_state' => trim($state),
//   //   'fd_other_state' => trim($state_other),
//   //   'fd_ownership' => trim($ownership_type)
//   // );
//   $obj->updateDbRecord($tbname,$dataarray);
// // joint account table
//   if ($ownership_type === "joint") {
//     $tbname ='will_joint_account_holder';
//     for($i=0; $i<count($ejd_hld_id); $i++){
//       // $dataarray   = array(
//       //   'fk_user_id' => $user_id,
//       //   'fk_form_id' => $form_id,
//       //   'jh_title' => trim($ejd_title[$i]),
//       //   'jh_f_name' => trim($ejd_firstname[$i]),
//       //   'jh_m_name' => trim($ejd_middlename[$i]),
//       //   'jh_l_name' => trim($ejd_lastname[$i]),
//       //   'jh_same_as_per' => trim($is_same_as_permanent[$i]),
//       //   'jh_same_as_corre' => trim($is_same_as_correspondence[$i]),
//       //   'jh_sa_as_per_or_corre' => trim($is_same_as_permanent_or_correspondence[$i]),
//       //   'jh_forig_addr' => trim($ejd_is_foreign_address[$i]),
//       //   'jh_addr_line1' => trim($ejd_address_line1[$i]),
//       //   'jh_addr_line2' => trim($ejd_address_line2[$i]),
//       //   'jh_addr_line3' => trim($ejd_address_line3[$i]),
//       //   'jh_addr_city' => trim($ejd_city_village_town[$i]),
//       //   'jh_addr_zip' => trim($ejd_zipcode[$i]),
//       //   'jh_addr_country' => trim($ejd_country[$i]),
//       //   'jh_state' => trim($ejd_state[$i]),
//       //   'jh_state_oth' => trim($ejd_state_other[$i]),
//       //   'jh_tel_isd' => trim($ejd_phone_isd[$i]),
//       //   'jh_tel_std' => trim($ejd_phone_std[$i]),
//       //   'jh_tel_no' => trim($ejd_phone_number[$i]),
//       //   'jh_off_isd' => trim($ejd_office_phone_isd[$i]),
//       //   'jh_off_std' => trim($ejd_office_phone_std[$i]),
//       //   'jh_off_no' => trim($ejd_office_phone_number[$i]),
//       //   'jh_mob_isd' => trim($ejd_mobile_isd[$i]),
//       //   'jh_mob_no' => trim($ejd_mobile_number[$i]),
//       //   'jh_email' => trim($ejd_email[$i]),
//       //   'jh_d_o_b' => trim($ejd_birth_date[$i]),
//       //   'jh_adult' => trim($ejd_is_adult[$i])
//       // );

//       $obj->insertDbRecord($tbname,$dataarray);
//     }
//   }

// //beneficiary-percentage table
//   $tbname ='will_beneficiary_percentage';
//   // $dataarray   = array(
//   //   'fk_user_id' => $user_id,
//   //   'fk_ben_id' => trim($sel_beneficiary),
//   //   'fk_form_id' => trim($form_id),
//   //   'bene_percentage' => trim($txt_beneficiary_share),
//   //   'ben_other_info' => trim($txt_other_info)
//   // );
//   // $obj->insertDbRecord($tbname,$dataarray);



// }else if(isset($add_ppf)){
//   $obj = new update();

//   //locker-account table
//   $user_id = $CONFIG->loggedUserId;
//   $tbname ='will_ppf_info';
//   // $dataarray   = array(
//   //   'fk_user_id' => $user_id,
//   //   'ppf_account_type' => trim($ppf_ownership),
//   //   'ppf_account_number' => trim($account_number),
//   //   'pf_post_ofc_account' => trim($post_account_number),
//   //   'ppf_ifsc' => trim($ifsc_code),
//   //   'ppf_bank_name' => trim($bank_name),
//   //   'ppf_branch_name' => trim($branch_name),
//   //   'ppf_post_ofc_name' => trim($post_office_name),
//   //   'ppf_addr_line1' => trim($address_line1),
//   //   'ppf_addr_line2' => trim($address_line2),
//   //   'ppf_addr_line3' => trim($address_line3),
//   //   'ppf_city' => trim($city_village_town),
//   //   'ppf_zipcode' => trim($zipcode),
//   //   'ppf_country' => trim($country),
//   //   'ppf_state' => trim($state),
//   //   'ppf_state_other' => trim($state_other),
//   //   'has_nominee' => trim($has_nominee),
//   //   'ppf_end_title' => trim($end_title),
//   //   'ppf_end_fname' => trim($end_firstname),
//   //   'ppf_end_mname' => trim($end_middlename),
//   //   'ppf_end_lname' => trim($end_lastname),
//   //   'ppf_end_d_o_b' => trim($end_birth_date),
//   //   'ppf_end_adult' => trim($end_is_adult),
//   //   'ppf_end_nom_addr_co_frm' => trim($end_nom_addr_copied_from),
//   //   'ppf_end_addr_line1' => trim($end_address_line1),
//   //   'ppf_end_addr_line2' => trim($end_address_line2),
//   //   'ppf_end_addr_line3' => trim($end_address_line3),
//   //   'ppf_end_city' => trim($end_city_village_town),
//   //   'ppf_end_zipcode' => trim($end_zipcode),
//   //   'ppf_end_country' => trim($end_country),
//   //   'ppf_end_state' => trim($end_state),
//   //   'ppf_end_oth_state' => trim($end_state_other),
//   //   'ppf_end_tel_isd' => trim(implode(',',$end_phone_isd)),
//   //   'ppf_end_tel_std' => trim(implode(',',$end_phone_std)),
//   //   'ppf_end_tel_no' => trim(implode(',',$end_phone_number)),
//   //   'ppf_end_mob_isd' => trim(implode(',',$end_mobile_isd)),
//   //   'ppf_end_mob_no' => trim(implode(',',$end_mobile_number)),
//   //   'ppf_end_email' => trim(implode(',',$end_email)),
//   //   'ppf_end_gd_title' => trim($end_guardian_title),
//   //   'ppf_end_gd_fname' => trim($end_guardian_firstname),
//   //   'ppf_end_gd_mname' => trim($end_guardian_middlename),
//   //   'ppf_end_gd_lname' => trim($end_guardian_lastname),
//   //   'ppf_end_gd_birth' => trim($end_guardian_birth_date),
//   //   'ppf_end_gd_addr_co_frm' => trim($end_guardian_addr_copied_from),
//   //   'ppf_end_gd_addr_line1' => trim($end_guardian_address_line1),
//   //   'ppf_end_gd_addr_line2' => trim($end_guardian_address_line2),
//   //   'ppf_end_gd_addr_line3' => trim($end_guardian_address_line3),
//   //   'ppf_end_gd_city' => trim($end_guardian_city_village_town),
//   //   'ppf_end_gd_zipcode' => trim($end_guardian_zipcode),
//   //   'ppf_end_gd_country' => trim($end_guardian_country),
//   //   'ppf_end_gd_state' => trim($end_guardian_state),
//   //   'ppf_end_gd_state_oth' => trim($end_guardian_state_other),
//   //   'ppf_end_gd_tel_isd' => trim(implode(',',$end_guardian_phone_isd)),
//   //   'ppf_end_gd_tel_std' => trim(implode(',',$end_guardian_phone_std)),
//   //   'ppf_end_gd_tel_no' => trim(implode(',',$end_guardian_phone_number)),
//   //   'ppf_end_gd_mob_isd' => trim(implode(',',$end_guardian_mobile_isd)),
//   //   'ppf_end_gd_mob_no' => trim(implode(',',$end_guardian_mobile_number)),
//   //   'ppf_end_gd_email' => trim(implode(',',$end_guardian_email))
//   // );
//   $obj->updateDbRecord($tbname,$dataarray);

//   $tbname ='will_beneficiary_percentage';
//   // $dataarray   = array(
//   //   'fk_user_id' => $user_id,
//   //   'fk_ben_id' => trim($sel_beneficiary),
//   //   'fk_form_id' => trim($form_id),
//   //   'bene_percentage' => trim($txt_beneficiary_share),
//   //   'ben_other_info' => trim($txt_other_info)
//   // );
//   // $obj->insertDbRecord($tbname,$dataarray);
// }else if(isset($add_sercurity)){
//   $obj = new update();

//   //securities-table
//   // $user_id = $CONFIG->loggedUserId;
//   // $tbname ='will_securities';
//   // $dataarray   = array(
//   //   'fk_user_id' => $user_id,
//   //   'sec_type' => trim($securities_format),
//   //   'dp_name' => trim($name_of_depository),
//   //   'dp_parti_name' => trim($participants_name),
//   //   'dp_id' => trim($dp_id),
//   //   'dp_ownership_type' => trim($ownership_type),
//   //   'demat_company_name' => trim($company_name),
//   //   'demat_folio_num' => trim($dmat_folio_number)
//   // );
//   $obj->updateDbRecord($tbname,$dataarray);

//   // beneficiaries
//   // $tbname ='will_beneficiary_percentage';
//   // $dataarray   = array(
//   //   'fk_user_id' => $user_id,
//   //   'fk_ben_id' => trim($sel_beneficiary),
//   //   'fk_form_id' => trim($form_id),
//   //   'bene_percentage' => trim($txt_beneficiary_share),
//   //   'ben_other_info' => trim($txt_other_info)
//   // );
//   $obj->insertDbRecord($tbname,$dataarray);

//   //gurardian
//   $tbname ='will_guardian';
//   // $dataarray = array(
//   //   'fk_user_id' => $user_id,
//   //   'fk_form_id' => trim($form_id),
//   //   'gd_title' => trim($end_guardian_title),
//   //   'gd_fname' => trim($end_guardian_firstname),
//   //   'gd_mname' => trim($end_guardian_middlename),
//   //   'gd_lname' => trim($end_guardian_lastname),
//   //   'gd_birth' => trim($end_guardian_birth_date),
//   //   'gd_addr_co_frm' => trim($end_guardian_addr_copied_from),
//   //   'gd_addr_line1' => trim($end_guardian_address_line1),
//   //   'gd_addr_line2' => trim($end_guardian_address_line2),
//   //   'gd_addr_line3' => trim($end_guardian_address_line3),
//   //   'gd_city' => trim($end_guardian_city_village_town),
//   //   'gd_zipcode' => trim($end_guardian_zipcode),
//   //   'gd_country' => trim($end_guardian_country),
//   //   'gd_state' => trim($end_guardian_state),
//   //   'gd_state_oth' => trim($end_guardian_state_other),
//   //   'gd_tel_isd' => trim(implode(',',$end_guardian_phone_isd)),
//   //   'gd_tel_std' => trim(implode(',',$end_guardian_phone_std)),
//   //   'gd_tel_no' => trim(implode(',',$end_guardian_phone_number)),
//   //   'gd_mob_isd' => trim(implode(',',$end_guardian_mobile_isd)),
//   //   'gd_mob_no' => trim(implode(',',$end_guardian_mobile_number)),
//   //   'gd_email' => trim(implode(',',$end_guardian_email))
//   // );
// $obj->insertDbRecord($tbname,$dataarray);
//   //joint account type
//   if ($ownership_type === "joint") {
//   //   $tbname ='will_joint_account_holder';
//   //   for($i=0; $i<count($ejd_hld_id); $i++){
//   //     $dataarray   = array(
//   //       'fk_user_id' => $user_id,
//   //       'fk_form_id' => $form_id,
//   //       'jh_title' => trim($ejd_title[$i]),
//   //       'jh_f_name' => trim($ejd_firstname[$i]),
//   //       'jh_m_name' => trim($ejd_middlename[$i]),
//   //       'jh_l_name' => trim($ejd_lastname[$i]),
//   //       'jh_same_as_per' => trim($is_same_as_permanent[$i]),
//   //       'jh_same_as_corre' => trim($is_same_as_correspondence[$i]),
//   //       'jh_sa_as_per_or_corre' => trim($is_same_as_permanent_or_correspondence[$i]),
//   //       'jh_forig_addr' => trim($ejd_is_foreign_address[$i]),
//   //       'jh_addr_line1' => trim($ejd_address_line1[$i]),
//   //       'jh_addr_line2' => trim($ejd_address_line2[$i]),
//   //       'jh_addr_line3' => trim($ejd_address_line3[$i]),
//   //       'jh_addr_city' => trim($ejd_city_village_town[$i]),
//   //       'jh_addr_zip' => trim($ejd_zipcode[$i]),
//   //       'jh_addr_country' => trim($ejd_country[$i]),
//   //       'jh_state' => trim($ejd_state[$i]),
//   //       'jh_state_oth' => trim($ejd_state_other[$i]),
//   //       'jh_tel_isd' => trim($ejd_phone_isd[$i]),
//   //       'jh_tel_std' => trim($ejd_phone_std[$i]),
//   //       'jh_tel_no' => trim($ejd_phone_number[$i]),
//   //       'jh_off_isd' => trim($ejd_office_phone_isd[$i]),
//   //       'jh_off_std' => trim($ejd_office_phone_std[$i]),
//   //       'jh_off_no' => trim($ejd_office_phone_number[$i]),
//   //       'jh_mob_isd' => trim($ejd_mobile_isd[$i]),
//   //       'jh_mob_no' => trim($ejd_mobile_number[$i]),
//   //       'jh_email' => trim($ejd_email[$i]),
//   //       'jh_d_o_b' => trim($ejd_birth_date[$i]),
//   //       'jh_adult' => trim($ejd_is_adult[$i])
//   //     );

//   //     $obj->insertDbRecord($tbname,$dataarray);
//   //   }
//   // }
}else if(isset($add_mutual)){
  $obj = new update();
  //securities-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_mutual_fund';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'mutl_comp_name' => trim($mutualfund_name),
    'mutl_amount' => trim($mf_amount),
    // 'mutl_sche_name' => trim($scheme_details),
    // 'mutl_folio_no' => trim($dmat_folio_number),
    // 'mutl_owner_type' => trim($ownership_type)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=smb&subpage2=sh";

  // beneficiaries
  // $tbname ='will_beneficiary_percentage';
  // $dataarray   = array(
  //   'fk_user_id' => $user_id,
  //   'fk_ben_id' => trim($sel_beneficiary),
  //   'fk_form_id' => $form_id,
  //   'bene_percentage' => trim($txt_beneficiary_share),
  //   'ben_other_info' => trim($txt_other_info)
  // );
  // $obj->insertDbRecord($tbname,$dataarray);
//   //gurardian
// //   $tbname ='will_guardian';
// //   $dataarray = array(
// //     'fk_user_id' => $user_id,
// //     'fk_form_id' => trim($form_id),
// //     'gd_title' => trim($end_guardian_title),
// //     'gd_fname' => trim($end_guardian_firstname),
// //     'gd_mname' => trim($end_guardian_middlename),
// //     'gd_lname' => trim($end_guardian_lastname),
// //     'gd_birth' => trim($end_guardian_birth_date),
// //     'gd_addr_co_frm' => trim($end_guardian_addr_copied_from),
// //     'gd_addr_line1' => trim($end_guardian_address_line1),
// //     'gd_addr_line2' => trim($end_guardian_address_line2),
// //     'gd_addr_line3' => trim($end_guardian_address_line3),
// //     'gd_city' => trim($end_guardian_city_village_town),
// //     'gd_zipcode' => trim($end_guardian_zipcode),
// //     'gd_country' => trim($end_guardian_country),
// //     'gd_state' => trim($end_guardian_state),
// //     'gd_state_oth' => trim($end_guardian_state_other),
// //     'gd_tel_isd' => trim(implode(',',$end_guardian_phone_isd)),
// //     'gd_tel_std' => trim(implode(',',$end_guardian_phone_std)),
// //     'gd_tel_no' => trim(implode(',',$end_guardian_phone_number)),
// //     'gd_mob_isd' => trim(implode(',',$end_guardian_mobile_isd)),
// //     'gd_mob_no' => trim(implode(',',$end_guardian_mobile_number)),
// //     'gd_email' => trim(implode(',',$end_guardian_email))
// //   );
// // $obj->insertDbRecord($tbname,$dataarray);
//   //joint account type
//   if ($ownership_type === "joint") {
//     // $tbname ='will_joint_account_holder';
//     // for($i=0; $i<count($ejd_hld_id); $i++){
//     //   $dataarray   = array(
//     //     'fk_user_id' => $user_id,
//     //     'fk_form_id' => $form_id,
//     //     'jh_title' => trim($ejd_title[$i]),
//     //     'jh_f_name' => trim($ejd_firstname[$i]),
//     //     'jh_m_name' => trim($ejd_middlename[$i]),
//     //     'jh_l_name' => trim($ejd_lastname[$i]),
//     //     'jh_same_as_per' => trim($is_same_as_permanent[$i]),
//     //     'jh_same_as_corre' => trim($is_same_as_correspondence[$i]),
//     //     'jh_sa_as_per_or_corre' => trim($is_same_as_permanent_or_correspondence[$i]),
//     //     'jh_forig_addr' => trim($ejd_is_foreign_address[$i]),
//     //     'jh_addr_line1' => trim($ejd_address_line1[$i]),
//     //     'jh_addr_line2' => trim($ejd_address_line2[$i]),
//     //     'jh_addr_line3' => trim($ejd_address_line3[$i]),
//     //     'jh_addr_city' => trim($ejd_city_village_town[$i]),
//     //     'jh_addr_zip' => trim($ejd_zipcode[$i]),
//     //     'jh_addr_country' => trim($ejd_country[$i]),
//     //     'jh_state' => trim($ejd_state[$i]),
//     //     'jh_state_oth' => trim($ejd_state_other[$i]),
//     //     'jh_tel_isd' => trim($ejd_phone_isd[$i]),
//     //     'jh_tel_std' => trim($ejd_phone_std[$i]),
//     //     'jh_tel_no' => trim($ejd_phone_number[$i]),
//     //     'jh_off_isd' => trim($ejd_office_phone_isd[$i]),
//     //     'jh_off_std' => trim($ejd_office_phone_std[$i]),
//     //     'jh_off_no' => trim($ejd_office_phone_number[$i]),
//     //     'jh_mob_isd' => trim($ejd_mobile_isd[$i]),
//     //     'jh_mob_no' => trim($ejd_mobile_number[$i]),
//     //     'jh_email' => trim($ejd_email[$i]),
//     //     'jh_d_o_b' => trim($ejd_birth_date[$i]),
//     //     'jh_adult' => trim($ejd_is_adult[$i])
//     //   );

//     //   $obj->insertDbRecord($tbname,$dataarray);
//     // }
//   }
}else if(isset($add_bond)){
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_bond_details';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'bond_comany_name' => trim($company_name),
    'bond_scheme_name' => trim($scheme_details),
    'bond_demat_no' => trim($bond_dmat_number),
    'bond_bank_name' => trim($bond_bank_name),
    'bond_amount' => trim($bond_amount),
    // 'bond_owner_type' => trim($ownership_type)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=ip";
//   // beneficiaries
//   $tbname ='will_beneficiary_percentage';
//   $dataarray   = array(
//     'fk_user_id' => $user_id,
//     'fk_ben_id' => trim($sel_beneficiary),
//     'fk_form_id' => $form_id,
//     'bene_percentage' => trim($txt_beneficiary_share),
//     'ben_other_info' => trim($txt_other_info)
//   );
//   $obj->insertDbRecord($tbname,$dataarray);

//   //gurardian
// //   $tbname ='will_guardian';
// //   $dataarray = array(
// //     'fk_user_id' => $user_id,
// //     'fk_form_id' => trim($form_id),
// //     'gd_title' => trim($end_guardian_title),
// //     'gd_fname' => trim($end_guardian_firstname),
// //     'gd_mname' => trim($end_guardian_middlename),
// //     'gd_lname' => trim($end_guardian_lastname),
// //     'gd_birth' => trim($end_guardian_birth_date),
// //     'gd_addr_co_frm' => trim($end_guardian_addr_copied_from),
// //     'gd_addr_line1' => trim($end_guardian_address_line1),
// //     'gd_addr_line2' => trim($end_guardian_address_line2),
// //     'gd_addr_line3' => trim($end_guardian_address_line3),
// //     'gd_city' => trim($end_guardian_city_village_town),
// //     'gd_zipcode' => trim($end_guardian_zipcode),
// //     'gd_country' => trim($end_guardian_country),
// //     'gd_state' => trim($end_guardian_state),
// //     'gd_state_oth' => trim($end_guardian_state_other),
// //     'gd_tel_isd' => trim(implode(',',$end_guardian_phone_isd)),
// //     'gd_tel_std' => trim(implode(',',$end_guardian_phone_std)),
// //     'gd_tel_no' => trim(implode(',',$end_guardian_phone_number)),
// //     'gd_mob_isd' => trim(implode(',',$end_guardian_mobile_isd)),
// //     'gd_mob_no' => trim(implode(',',$end_guardian_mobile_number)),
// //     'gd_email' => trim(implode(',',$end_guardian_email))
// //   );
// // $obj->insertDbRecord($tbname,$dataarray);
//   //joint account type
//   if ($ownership_type === "joint") {
//     // $tbname ='will_joint_account_holder';
//     // for($i=0; $i<count($ejd_hld_id); $i++){
//     //   $dataarray   = array(
//     //     'fk_user_id' => $user_id,
//     //     'fk_form_id' => $form_id,
//     //     'jh_title' => trim($ejd_title[$i]),
//     //     'jh_f_name' => trim($ejd_firstname[$i]),
//     //     'jh_m_name' => trim($ejd_middlename[$i]),
//     //     'jh_l_name' => trim($ejd_lastname[$i]),
//     //     'jh_same_as_per' => trim($is_same_as_permanent[$i]),
//     //     'jh_same_as_corre' => trim($is_same_as_correspondence[$i]),
//     //     'jh_sa_as_per_or_corre' => trim($is_same_as_permanent_or_correspondence[$i]),
//     //     'jh_forig_addr' => trim($ejd_is_foreign_address[$i]),
//     //     'jh_addr_line1' => trim($ejd_address_line1[$i]),
//     //     'jh_addr_line2' => trim($ejd_address_line2[$i]),
//     //     'jh_addr_line3' => trim($ejd_address_line3[$i]),
//     //     'jh_addr_city' => trim($ejd_city_village_town[$i]),
//     //     'jh_addr_zip' => trim($ejd_zipcode[$i]),
//     //     'jh_addr_country' => trim($ejd_country[$i]),
//     //     'jh_state' => trim($ejd_state[$i]),
//     //     'jh_state_oth' => trim($ejd_state_other[$i]),
//     //     'jh_tel_isd' => trim($ejd_phone_isd[$i]),
//     //     'jh_tel_std' => trim($ejd_phone_std[$i]),
//     //     'jh_tel_no' => trim($ejd_phone_number[$i]),
//     //     'jh_off_isd' => trim($ejd_office_phone_isd[$i]),
//     //     'jh_off_std' => trim($ejd_office_phone_std[$i]),
//     //     'jh_off_no' => trim($ejd_office_phone_number[$i]),
//     //     'jh_mob_isd' => trim($ejd_mobile_isd[$i]),
//     //     'jh_mob_no' => trim($ejd_mobile_number[$i]),
//     //     'jh_email' => trim($ejd_email[$i]),
//     //     'jh_d_o_b' => trim($ejd_birth_date[$i]),
//     //     'jh_adult' => trim($ejd_is_adult[$i])
//     //   );

//     //   $obj->insertDbRecord($tbname,$dataarray);
//     // }
//   }
}else if(isset($add_immovable)){
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_immovable_properties';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    // 'prop_name' => trim($property_name),
    'prop_type' => trim($property_type),
    // 'prop_same_as_per' => trim(implode(',',$is_same_as_permanent)),
    // 'prop_same_as_corre' => trim(implode(',',$is_same_as_correspondence)),
    // 'prop_same_as_per_or_corre' => trim(implode(',',$is_same_as_permanent_or_correspondence)),
    'prop_addr_line1' => trim($address_line1),
    'prop_addr_line2' => trim($address_line2),
    'prop_addr_line3' => trim($address_line3),
    'prop_city' => trim($city_village_town),
    'prop_zipcode' => trim($zipcode),
    'prop_country' => trim($country),
    'prop_state' => trim($state),
    'prop_mes' => trim($prop_measurement),
    'prop_own_status' => trim($ownership_status),
    // 'prop_state_oth' => trim($state_other),
    // 'prop_mortgaged_status' => trim($is_this_prop_mortgaged),
    // 'prop_mortgaged_name' => trim($prop_mortgaged_name),
    // 'prop_owner_type' => trim($ownership_type)
      'prop_own_perc' => trim($ownership_perc)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=bu";

//   // beneficiaries
//   $tbname ='will_beneficiary_percentage';
//   $dataarray   = array(
//     'fk_user_id' => $user_id,
//     'fk_ben_id' => trim($sel_beneficiary),
//     'fk_form_id' => $form_id,
//     'bene_percentage' => trim($txt_beneficiary_share),
//     'ben_other_info' => trim($txt_other_info)
//   );
//   $obj->insertDbRecord($tbname,$dataarray);

//   //gurardian
// //   $tbname ='will_guardian';
// //   $dataarray = array(
// //     'fk_user_id' => $user_id,
// //     'fk_form_id' => trim($form_id),
// //     'gd_title' => trim($end_guardian_title),
// //     'gd_fname' => trim($end_guardian_firstname),
// //     'gd_mname' => trim($end_guardian_middlename),
// //     'gd_lname' => trim($end_guardian_lastname),
// //     'gd_birth' => trim($end_guardian_birth_date),
// //     'gd_addr_co_frm' => trim($end_guardian_addr_copied_from),
// //     'gd_addr_line1' => trim($end_guardian_address_line1),
// //     'gd_addr_line2' => trim($end_guardian_address_line2),
// //     'gd_addr_line3' => trim($end_guardian_address_line3),
// //     'gd_city' => trim($end_guardian_city_village_town),
// //     'gd_zipcode' => trim($end_guardian_zipcode),
// //     'gd_country' => trim($end_guardian_country),
// //     'gd_state' => trim($end_guardian_state),
// //     'gd_state_oth' => trim($end_guardian_state_other),
// //     'gd_tel_isd' => trim(implode(',',$end_guardian_phone_isd)),
// //     'gd_tel_std' => trim(implode(',',$end_guardian_phone_std)),
// //     'gd_tel_no' => trim(implode(',',$end_guardian_phone_number)),
// //     'gd_mob_isd' => trim(implode(',',$end_guardian_mobile_isd)),
// //     'gd_mob_no' => trim(implode(',',$end_guardian_mobile_number)),
// //     'gd_email' => trim(implode(',',$end_guardian_email))
// //   );
// // $obj->insertDbRecord($tbname,$dataarray);
//   //joint account type
//   if ($ownership_type === "joint") {
//     // $tbname ='will_joint_account_holder';
//     // for($i=0; $i<count($ejd_hld_id); $i++){
//     //   $dataarray   = array(
//     //     'fk_user_id' => $user_id,
//     //     'fk_form_id' => $form_id,
//     //     'jh_title' => trim($ejd_title[$i]),
//     //     'jh_f_name' => trim($ejd_firstname[$i]),
//     //     'jh_m_name' => trim($ejd_middlename[$i]),
//     //     'jh_l_name' => trim($ejd_lastname[$i]),
//     //     'jh_same_as_per' => trim($is_same_as_permanent[$i]),
//     //     'jh_same_as_corre' => trim($is_same_as_correspondence[$i]),
//     //     'jh_sa_as_per_or_corre' => trim($is_same_as_permanent_or_correspondence[$i]),
//     //     'jh_forig_addr' => trim($ejd_is_foreign_address[$i]),
//     //     'jh_addr_line1' => trim($ejd_address_line1[$i]),
//     //     'jh_addr_line2' => trim($ejd_address_line2[$i]),
//     //     'jh_addr_line3' => trim($ejd_address_line3[$i]),
//     //     'jh_addr_city' => trim($ejd_city_village_town[$i]),
//     //     'jh_addr_zip' => trim($ejd_zipcode[$i]),
//     //     'jh_addr_country' => trim($ejd_country[$i]),
//     //     'jh_state' => trim($ejd_state[$i]),
//     //     'jh_state_oth' => trim($ejd_state_other[$i]),
//     //     'jh_tel_isd' => trim($ejd_phone_isd[$i]),
//     //     'jh_tel_std' => trim($ejd_phone_std[$i]),
//     //     'jh_tel_no' => trim($ejd_phone_number[$i]),
//     //     'jh_off_isd' => trim($ejd_office_phone_isd[$i]),
//     //     'jh_off_std' => trim($ejd_office_phone_std[$i]),
//     //     'jh_off_no' => trim($ejd_office_phone_number[$i]),
//     //     'jh_mob_isd' => trim($ejd_mobile_isd[$i]),
//     //     'jh_mob_no' => trim($ejd_mobile_number[$i]),
//     //     'jh_email' => trim($ejd_email[$i]),
//     //     'jh_d_o_b' => trim($ejd_birth_date[$i]),
//     //     'jh_adult' => trim($ejd_is_adult[$i])
//     //   );

//     //   $obj->insertDbRecord($tbname,$dataarray);
//     // }
//   }
}else if(isset($add_provident_fund)){
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_retirement_funds';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'pf_account_number' => trim($account_number),
    'pf_company_name' => trim($pf_company_name),
    'pf_addr_line1' => trim($pf_address_line1),
    'pf_addr_line2' => trim($pf_address_line2),
    'pf_addr_line3' => trim($pf_address_line3),
    'pf_city' => trim($pf_city_village_town),
    'pf_state' => trim($pf_state),
    'pf_zipcode' => trim($pf_zipcode),
    'pf_country' => trim($pf_country),
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=rf&subpage2=pen";

//   // beneficiaries
//   $tbname ='will_beneficiary_percentage';
//   $dataarray   = array(
//     'fk_user_id' => $user_id,
//     'fk_ben_id' => trim($sel_beneficiary),
//     'fk_form_id' => $form_id,
//     'bene_percentage' => trim($txt_beneficiary_share),
//     'ben_other_info' => trim($txt_other_info)
//   );
//   $obj->insertDbRecord($tbname,$dataarray);

}else if(isset($add_pension_fund)){
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_pension_funds';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'pen_plan_name' => trim($pen_plan_name),
    'pen_acc_num' => trim($pen_account_number),
    'pen_com_name' => trim($pen_company_name),
    'pen_addr_line1' => trim($pen_address_line1),
    'pen_addr_line2' => trim($pen_address_line2),
    'pen_addr_line3' => trim($pen_address_line3),
    'pen_city' => trim($pen_city_village_town),
    'pen_state' => trim($pen_state),
    'pen_zipcode' => trim($pen_zipcode),
    'pen_country' => trim($pen_country),
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=rf&subpage2=gra";

//   // beneficiaries
//   $tbname ='will_beneficiary_percentage';
//   $dataarray   = array(
//     'fk_user_id' => $user_id,
//     'fk_ben_id' => trim($sel_beneficiary),
//     'fk_form_id' => $form_id,
//     'bene_percentage' => trim($txt_beneficiary_share),
//     'ben_other_info' => trim($txt_other_info)
//   );
//   $obj->insertDbRecord($tbname,$dataarray);
}else if(isset($add_gratuity_fund)){
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_gratuity';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'gra_acc_num' => trim($account_number),
    'gra_com_name' => trim($gra_company_name),
    'gra_addr_line1' => trim($gra_address_line1),
    'gra_addr_line2' => trim($gra_address_line2),
    'gra_addr_line3' => trim($gra_address_line3),
    'gra_city' => trim($gra_city_village_town),
    'gra_state' => trim($gra_state),
    'gra_zipcode' => trim($gra_zipcode),
    'gra_country' => trim($gra_country),
  );
  $redirectdata = "&newpage=assets&subpage=gi";
  $obj->updateDbRecord($tbname,$dataarray);
}else if(isset($add_lic)){
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_lic';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'lic_company_name' => trim($issuer_name),
    'policy_number' => trim($policy_number),
    'policy_start_date' => trim($start_date),
    'policy_maturity_date' => trim($maturity_date),
    'lic_branch_name' => trim($lic_branch_name),
    'sum_assured' => trim($sum_assured)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=je";

//   // beneficiaries
//   $tbname ='will_beneficiary_percentage';
//   $dataarray   = array(
//     'fk_user_id' => $user_id,
//     'fk_ben_id' => trim($sel_beneficiary),
//     'fk_form_id' => $form_id,
//     'bene_percentage' => trim($txt_beneficiary_share),
//     'ben_other_info' => trim($txt_other_info)
//   );
//   $obj->insertDbRecord($tbname,$dataarray);
}else if(isset($add_gi)){
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_general_insurance';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'gi_pol_type' => trim($policy_type),
    'gi_pol_num' => trim($policy_number),
    'gi_com_name' => trim($gi_com_name),
    'gi_start_date' => trim($policy_start_date),
    'gi_mat_date' => trim($maturity_date),
    'gi_amount' => trim($gi_amount)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=li";
}else if(isset($add_other_assets)){
    $obj = new update();
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_other_assets';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'oasset_name' => trim($oa_name),
    'oasset_own_type' => trim($oa_ownership),
    'oasset_desc' => trim($oa_desc),
    'oasset_addr_line1' => trim($oa_address_line1),
    'oasset_addr_line2' => trim($oa_address_line2),
    'oasset_addr_line3' => trim($oa_address_line3),
    'oasset_city' => trim($oa_city_village_town),
    'oasset_state' => trim($oa_state),
    // 'oasset_state_other' => trim($oa_state_other),
    'oasset_zipcode' => trim($oa_zipcode),
    'oasset_country' => trim($oa_country),
      'asset_own_perc' => trim($asset_own_perc)
  // $obj = new update();
  // //bond-table
  // $user_id = $CONFIG->loggedUserId;
  // $tbname ='will_assets';
  // $dataarray   = array(
  //   'fk_user_id' => $user_id,
  //   // 'asset_type' => trim($asset_type),
  //   // 'asset_others' => trim($others),
  //   // 'asset_details' => trim($asset_details),
  //   'asset_loan_status' => trim($loan_against_asset),
  //   'lender_detail' => trim($lender_details)
  //   // 'is_prop_mortgaged' => trim($is_this_prop_mortgaged),
  //   // 'mortgaged_detail' => trim($prop_mortgaged_name)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=oa&subpage2=ipr";
  

//   // beneficiaries
//   $tbname ='will_beneficiary_percentage';
//   $dataarray   = array(
//     'fk_user_id' => $user_id,
//     'fk_ben_id' => trim($sel_beneficiary),
//     'fk_form_id' => $form_id,
//     'bene_percentage' => trim($txt_beneficiary_share),
//     'ben_other_info' => trim($txt_other_info)
//   );
//   $obj->insertDbRecord($tbname,$dataarray);
}else if(isset($add_digital_assets)){
  // exit;
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_digital_assets';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'dig_asset_type' => trim($asset_type),
    'dig_asset_other' => trim($others),
    'dig_asset_amount' => trim($amount),
    // 'dig_asset_desc' => trim($description),
    // 'dig_asset_owner_type' => trim($ownership_type)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=oa&subpage2=veh";

//   // beneficiaries
//   $tbname ='will_beneficiary_percentage';
//   $dataarray   = array(
//     'fk_user_id' => $user_id,
//     'fk_ben_id' => trim($sel_beneficiary),
//     'fk_form_id' => $form_id,
//     'bene_percentage' => trim($txt_beneficiary_share),
//     'ben_other_info' => trim($txt_other_info)
//   );
//   $obj->insertDbRecord($tbname,$dataarray);

//   //joint account type
//   if ($ownership_type === "joint") {
//     $tbname ='will_joint_account_holder';
//     for($i=0; $i<count($ejd_hld_id); $i++){
//       // $dataarray   = array(
//       //   'fk_user_id' => $user_id,
//       //   'fk_form_id' => $form_id,
//       //   'jh_title' => trim($ejd_title[$i]),
//       //   'jh_f_name' => trim($ejd_firstname[$i]),
//       //   'jh_m_name' => trim($ejd_middlename[$i]),
//       //   'jh_l_name' => trim($ejd_lastname[$i]),
//       //   'jh_same_as_per' => trim($is_same_as_permanent[$i]),
//       //   'jh_same_as_corre' => trim($is_same_as_correspondence[$i]),
//       //   'jh_sa_as_per_or_corre' => trim($is_same_as_permanent_or_correspondence[$i]),
//       //   'jh_forig_addr' => trim($ejd_is_foreign_address[$i]),
//       //   'jh_addr_line1' => trim($ejd_address_line1[$i]),
//       //   'jh_addr_line2' => trim($ejd_address_line2[$i]),
//       //   'jh_addr_line3' => trim($ejd_address_line3[$i]),
//       //   'jh_addr_city' => trim($ejd_city_village_town[$i]),
//       //   'jh_addr_zip' => trim($ejd_zipcode[$i]),
//       //   'jh_addr_country' => trim($ejd_country[$i]),
//       //   'jh_state' => trim($ejd_state[$i]),
//       //   'jh_state_oth' => trim($ejd_state_other[$i]),
//       //   'jh_tel_isd' => trim($ejd_phone_isd[$i]),
//       //   'jh_tel_std' => trim($ejd_phone_std[$i]),
//       //   'jh_tel_no' => trim($ejd_phone_number[$i]),
//       //   'jh_off_isd' => trim($ejd_office_phone_isd[$i]),
//       //   'jh_off_std' => trim($ejd_office_phone_std[$i]),
//       //   'jh_off_no' => trim($ejd_office_phone_number[$i]),
//       //   'jh_mob_isd' => trim($ejd_mobile_isd[$i]),
//       //   'jh_mob_no' => trim($ejd_mobile_number[$i]),
//       //   'jh_email' => trim($ejd_email[$i]),
//       //   'jh_d_o_b' => trim($ejd_birth_date[$i]),
//       //   'jh_adult' => trim($ejd_is_adult[$i])
//       // );

//       // $obj->insertDbRecord($tbname,$dataarray);
//     }
//   }
}else if(isset($add_liability)){
  // exit;
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_liabilities';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'liabi_type' => trim($liability_type),
    'liabi_inst_name' => trim($institution_name),
    'amount' => trim($lib_amount),
    'liabi_prop_mes' => trim($lib_prop_mes),
    // 'liabi_inst_as_guarantor' => trim($institution_as_guarantor),
    'liabi_addr_line1' => trim($address_line1),
    'liabi_addr_line2' => trim($address_line2),
    'liabi_addr_line3' => trim($address_line3),
    'liabi_city' => trim($city_village_town),
    'liabi_zipcode' => trim($zipcode),
    'liabi_country' => trim($country),
    'liabi_state' => trim($state),
    // 'liabi_state_oth' => trim($state_other),

    'ind_addr_line1' => trim($ind_address_line1),
    'ind_addr_line2' => trim($ind_address_line2),
    'ind_addr_line3' => trim($ind_address_line3),
    'ind_city' => trim($ind_city_village_town),
    'ind_zipcode' => trim($ind_zipcode),
    'ind_country' => trim($ind_country),
    'ind_state' => trim($ind_state),
    // 'ind_state_oth' => trim($ind_state_other),

    'liabi_start_date' => trim($start_date),
    'liabi_closing_date' => trim($closing_date),
    'liabi_account_no' => trim($account_number),
    'liabi_int_rate' => trim($int_rate),
    // 'liabi_payee_type' => trim($pay_from_estate)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=liab&subpage2=adli";

//   // beneficiaries
//   $tbname ='will_beneficiary_percentage';
//   $dataarray   = array(
//     'fk_user_id' => $user_id,
//     'fk_ben_id' => trim($sel_beneficiary),
//     'fk_form_id' => $form_id,
//     'bene_percentage' => trim($txt_beneficiary_share),
//     'ben_other_info' => trim($txt_other_info)
//   );
//   $obj->insertDbRecord($tbname,$dataarray);
//   }else if(isset($add_non_beneficiary)){
//   $obj = new update();
//   //bond-table
//   $user_id = $CONFIG->loggedUserId;
//   $tbname ='will_non_beneficiaries';
//   if (count($title) > 0) {
//     for($i=0; $i<count($title);  $i++){
//     $dataarray   = array(
//       'fk_user_id' => $user_id,
//       'non_bene_title' => trim($title[$i]),
//       'non_bene_fname' => trim($benificiary_firstname[$i]),
//       'non_bene_mname' => trim($benificiary_middlename[$i]),
//       'non_bene_lname' => trim($benificiary_lastname[$i]),
//       'non_bene_rel_with_test' => trim($relationship_with_testator[$i]),
//       'non_bene_other_rel' => trim($other_relationship[$i]),
//       'non_bene_other_info' => trim($other_info[$i])
//     );
//     $obj->insertDbRecord($tbname,$dataarray);
//   }
// }
// }else if(isset($add_contingent_bene)){
//   $obj = new update();
//   //bond-table
//   $user_id = $CONFIG->loggedUserId;
//   $tbname ='will_contingent_beneficiary';
//   if (count($contigency_clause) > 0) {
//     for($i=0; $i<count($contigency_clause);  $i++){
//     $dataarray   = array(
//       'fk_user_id' => $user_id,
//       'conti_clause' => trim($contigency_clause[$i]),
//       'conti_prim_bene' => trim($primary_beneficiary[$i])
//     );
//     $obj->insertDbRecord($tbname,$dataarray);
//   }

//   // beneficiaries
//   $tbname ='will_beneficiary_percentage';
//   $dataarray   = array(
//     'fk_user_id' => $user_id,
//     'fk_ben_id' => trim($sel_beneficiary),
//     'fk_form_id' => $form_id,
//     'bene_percentage' => trim($txt_beneficiary_share),
//     'ben_other_info' => trim($txt_other_info)
//   );
//   $obj->insertDbRecord($tbname,$dataarray);
// }
// }else if(isset($add_special_clause)){
//   $obj = new update();
//   //bond-table
//   $user_id = $CONFIG->loggedUserId;
//   $tbname ='will_special_clause';
//   if (count($special_clause) > 0) {
//     for($i=0; $i<count($special_clause);  $i++){
//     $dataarray   = array(
//       'fk_user_id' => $user_id,
//       'special_instruction' => trim($special_clause[$i])
//     );
//     $obj->insertDbRecord($tbname,$dataarray);
//   }
}else if(isset($add_additonal_liability)){
  // exit;
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_add_liabilities';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'addlib_type' => trim($liability_type),
    //'addlib_amount' => trim($lib_amount),
    'addlib_loan_amount' => trim($lib_loan_amount),
    'addlib_bank_name' => trim($lib_bank_name),
    'addlib_branch_name' => trim($lib_branch_name),
    'addlib_loan_acc_no' => trim($account_number),
    'addlib_pro_name' => trim($lib_prop_name),
    // 'liabi_inst_as_guarantor' => trim($institution_as_guarantor),
    'addlib_addr_line1' => trim($address_line1),
    'addlib_addr_line2' => trim($address_line2),
    'addlib_addr_line3' => trim($address_line3),
    'addlib_city' => trim($city_village_town),
    'addlib_zipcode' => trim($zipcode),
    'addlib_country' => trim($country),
    'addlib_state' => trim($state),
    // 'addlib_state_oth' => trim($state_other)
    // 'liabi_payee_type' => trim($pay_from_estate)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=nb  ";
  
  
}else if(isset($fd_btn)){
  // exit;
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_fixed_deposit';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'fd_account_number' => trim($fd_account_number),
    'fd_bank_name' => trim($fd_bank_name),
    'fd_branch_name' => trim($fd_branch_name),
    'fd_addr_line1' => trim($fd_address_line1),
    'fd_addr_line2' => trim($fd_address_line2),
    'fd_addr_line3' => trim($fd_address_line3),
    'fd_city' => trim($fd_city_village_town),
    'fd_state' => trim($fd_state),
    // 'fd_state_oth' => trim($fd_state_other),
    'fd_zipcode' => trim($fd_zipcode),
    'fd_country' => trim($fd_country)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=smb&subpage2=mf";

}else if(isset($add_share)){
  // exit;
  $obj = new update();
  // bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_share';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'share_type' => trim($share_type),
    'share_amount' => trim($share_amount),
    'share_demat_num' => trim($share_dmat_number),
    'share_com_name' => trim($share_company_name),
    'share_bank_name' => trim($share_bank_name)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=smb&subpage2=bo";
  
}else if(isset($add_business)){
  // exit;
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_business';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'bus_type' => trim($bzns_act_type),
    'bus_own_type' => trim($bzns_owner_type),
    'bus_com_name' => trim($bzns_company_name),
    'bus_addr_line1' => trim($bzns_address_line1),
    'bus_addr_line2' => trim($bzns_address_line2),
    'bus_addr_line3' => trim($bzns_address_line3),
    'bus_city' => trim($bzns_city_village_town),
    'bus_state' => trim($bzns_state),
    // 'fbus_state_oth' => trim($bzns_state_other),
    'bus_zipcode' => trim($bzns_zipcode),
    'bus_country' => trim($bzns_country),
    'busi_own_perc'  => trim($businesss_ownership_perc)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=rf";
  
}else if(isset($add_jewel)){
  // exit;
  $obj = new update();
  // bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_jewellery';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'jwl_type' => trim($jwl_type),
    'jwl_name' => trim($jwl_name),
    'jwl_amount' => trim($jwl_amount),
    'jwl_desc' => trim($jwl_description)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=bo";
}else if(isset($add_bod_organ)){
  // exit;
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_body_organ';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    // 'borg_type' => trim($bzns_act_type),
    'borgan_name' => trim($borgan_name),
    'borgan_hos_name' => trim($borgan_hos_name),
    'borgan_addr_line1' => trim($bodorg_address_line1),
    'borgan_addr_line2' => trim($bodorg_address_line2),
    'borgan_addr_line3' => trim($bodorg_address_line3),
    'borgan_city' => trim($bodorg_city_village_town),
    'borgan_state' => trim($bodorg_state),
    // 'borg_state_oth' => trim($fd_state_other),
    'borgan_zipcode' => trim($bodorg_zipcode),
    'borgan_country' => trim($bodorg_country)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=pa";
}else if(isset($add_custodian)){
  // exit;
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_custodian';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    // 'borg_type' => trim($bzns_act_type),
    'custodian_name' => trim($cust_name),
    'custodian_father_name' => trim($cust_fat_name),
    'custodian_addr_line1' => trim($cust_address_line1),
    'custodian_addr_line2' => trim($cust_address_line2),
    'custodian_addr_line3' => trim($cust_address_line3),
    'custodian_city' => trim($cust_city_village_town),
    'custodian_state' => trim($cust_state),
    // 'custodian_state_oth' => trim($cust_state_other),
    'custodian_zipcode' => trim($cust_zipcode),
    'custodian_country' => trim($cust_country),
    'custodian_age' => trim($cust_age),
    'custodian_religion' => trim($cust_religion),
    'custodian_nationality' => trim($nationality)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=wi";
}else if(isset($add_pet_animal)){
  // exit;
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_pet_animal';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'pa_type' => trim($peet_animal_type),
    'pa_name' => trim($peet_animal_name)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=oa";
}else if(isset($add_vechile)){
  // exit;
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_vechile';
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'vechile_name' => trim($vec_name),
    'vechile_ownership' => trim($vec_ownership),
    'vechile_reg_num' => trim($vec_reg_num),
    'vechile_color' => trim($vec_color),
    'vechile_addr_line1' => trim($vec_address_line1),
    'vechile_addr_line2' => trim($vec_address_line2),
    'vechile_addr_line3' => trim($vec_address_line3),
    'vechile_city' => trim($vec_city_village_town),
    'vechile_state' => trim($vec_state),
    // 'vechile_state_other' => trim($vechile_state_other),
    'vechile_zipcode' => trim($vec_zipcode),
    'vechile_country' => trim($vec_country)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=assets&subpage=liab";
  
}else if(isset($add_witness)){
  // exit;
  $obj = new update();
  //bond-table
  $user_id = $CONFIG->loggedUserId;
  $tbname ='will_witness';
  
  //echo ($witness1_name);
 
  $dataarray   = array(
    'fk_user_id' => $user_id,
    'witness1_name' => trim($witness1_name),
    'witness1_phone' => trim($witness1_phone),
    //'witness1_email' => trim($witness1_email),
    'witness1_addr_line1' => trim($witness1_addr_line1),
    'witness1_addr_line2' => trim($witness1_addr_line2),
    'witness1_addr_line3' => trim($witness1_addr_line3),
    'witness1_city' => trim($witness1_city_village_town),
    'witness1_state' => trim($witness1_state),
    // 'witness1_state_other' => trim($witness1_state_other),
    'witness1_zipcode' => trim($witness1_zipcode),
    'witness1_country' => trim($witness1_country),
// witness 2 data
    'witness2_name' => trim($witness2_name),
    'witness2_phone' => trim($witness2_phone),
    //'witness2_email' => trim($witness2_email),
    'witness2_addr_line1' => trim($witness2_addr_line1),
    'witness2_addr_line2' => trim($witness2_addr_line2),
    'witness2_addr_line3' => trim($witness2_addr_line3),
    'witness2_city' => trim($witness2_city_village_town),
    'witness2_state' => trim($witness2_state),
    // 'witness2_state_other' => trim($vechile_state_other),
    'witness2_zipcode' => trim($witness2_zipcode),
    'witness2_country' => trim($witness2_country)
  );
  $obj->updateDbRecord($tbname,$dataarray);
  $redirectdata = "&newpage=willpay";
  
}else if (isset($add_more_ben)) {
    $user_id = $CONFIG->loggedUserId;
    $tbname ='will_beneficiary';
    $dataarray   = array(
    'fk_user_id' => $user_id,
    'title' => trim($title),
    'f_name' => trim($firstname),
    'm_name' => trim($middlename),
    'l_name' => trim($lastname),
    'date_of_birth' => trim($birth_date),
    'ben_age' => trim($ben_age),
    'rel_with_testator' => trim($relationship_with_testator),
    'per_addr_line1' => trim($permanent_address_line1),
    'per_addr_line2' => trim($permanent_address_line2),
    'per_addr_line3' => trim($permanent_address_line3),
    'per_city' => trim($permanent_city_village_town),
    'per_zip' => trim($permanent_zipcode),
    'per_country' => trim($permanent_country),
    'per_state' => trim($permanent_state),
    'corre_co_from' => trim($correspondence_addr_copied_from),
    'corre_addr_line1' => trim($correspondence_address_line1),
    'corre_addr_line2' => trim($correspondence_address_line2),
    'corre_addr_line3' => trim($correspondence_address_line3),
    'corre_city' => trim($correspondence_city_village_town),
    'corre_zip' => trim($correspondence_zipcode),
    'corre_country' => trim($correspondence_country),
    'corre_state' => trim($correspondence_state),
    'gd_title' => trim($guardian_title),
    'gd_f_name' => trim($guardian_firstname),
    'gd_m_name' => trim($guardian_middlename),
    'gd_l_name' => trim($guardian_lastname),
    'gd_fat_title'  => trim($guardian_fat_title),
    'gd_fat_f_name' => trim($guardian_fat_firstname),
    'gd_fat_m_name' => trim($guardian_fat_middlename),
    'gd_fat_l_name' => trim($guardian_fat_lastname),
    'gd_d_of_b' => trim($guardian_birth_date),
    'gd_age' => trim($gd_age),
    'gd_is_adult' => trim($guardian_is_adult),
    'gd_gender' => trim($guardian_gender),
    'gd_nationality' => trim($guardian_nationality),
    'gd_occupation' => trim($guardian_occupation),
    'gd_religious' => trim($guardian_religious),
    'gd_per_addr_co_from' => trim($guardian_permanent_addr_copied_from),
    'gd_per_addr_line1' => trim($guardian_permanent_address_line1),
    'gd_per_addr_line2' => trim($guardian_permanent_address_line2),
    'gd_per_addr_line3' => trim($guardian_permanent_address_line3),
    'gd_per_city' => trim($guardian_permanent_city_village_town),
    'gd_per_zip' => trim($guardian_permanent_zipcode),
    'gd_per_country' => trim($guardian_permanent_country),
    'gd_per_state' => trim($guardian_permanent_state),
    'is_gd_corre_s_as_gd_per' => trim($is_guardian_correspondence_same_as_guardian_permanent),
    'gd_corre_addr_co_from' => trim($guardian_correspondence_addr_copied_from),
    'gd_corre_addr_line1' => trim($guardian_correspondence_address_line1),
    'gd_corre_addr_line2' => trim($guardian_correspondence_address_line2),
    'gd_corre_addr_line3' => trim($guardian_correspondence_address_line3),
    'gd_corre_city' => trim($guardian_correspondence_city_village_town),
    'gd_corre_zip' => trim($guardian_correspondence_zipcode),
    'gd_corre_country' => trim($guardian_correspondence_country),
    'gd_corre_state' => trim($guardian_correspondence_state),
    'gd_bene_rel' => trim($guardian_beneficary_relationship)
  );
  $obj->insertDbRecord($tbname,$dataarray);
}
    $pageurl = $CONFIG->siteurl . "mySaveTax/" . "?module_interface=" . $commonFunction->setPage('create_will');
    echo "<script>location.href='" . $pageurl.$redirectdata . "'</script>";
		exit;
 ?>
 
 
