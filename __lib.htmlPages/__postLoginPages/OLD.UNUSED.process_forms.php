<?php
	//echo "<pre>";print_r($_REQUEST);
	$formDataFromDB = $documentFiles->getFormData($_REQUEST[formsDataID]);
	//print_r($formDataFromDB);
?>
<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo $CONFIG->siteurl;?>mySaveTax/">Home</a>
							</li>
                            <li>
								<i class="ace-icon fa fa-list list-icon"></i>
								<a href="<?php echo $CONFIG->siteurl;?>mySaveTax/?module_interface=<?php echo $commonFunction->setPage('forms_list');?>">Uploaded Forms</a>
							</li>
							<li class="active">Fill ITR</li>
						</ul><!-- /.breadcrumb -->

						<?php include("form.search.php");?>

						<!-- /section:basics/content.searchbox -->
					</div>
					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">						
						<div class="row">
							<div class="col-xs-12">
                            	<div class="space-4"></div>
									<div class="tab-pane active">											
										<div class="row">
                                            <div class="col-xs-12">
                                                <h4 class="orange">																																
                                                    <span class="middle">Fill ITR</span>
                                                </h4>  
                                                <div class="hr hr-8 dotted"></div>                                              
                                            </div>
										</div>       
                                        <div class="space-20"></div>                                                                       
                                        <div id="user-profile-2" class="user-profile">
                                        <div class="tabbable">
											<ul class="nav nav-tabs padding-18">
												<li class="active">
													<a data-toggle="tab" href="#form_16_part_a">																																						
                                                        	<i class="orange ace-icon fa fa-book bigger-120"></i>
															<strong>FORM 16 - PART A</strong>
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#form_16_part_b">
														<i class="orange ace-icon fa fa-book bigger-120"></i>
														<strong>Form 16 - PART B</strong>
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#deduction">
														<i class="orange ace-icon fa fa-bolt bigger-120"></i>
														<strong>Other Deduction</strong>
													</a>
												</li>
                                                <li>
													<a data-toggle="tab" href="#paid_tax">
														<i class="orange ace-icon fa fa-filter bigger-120"></i>
														<strong>Paid Taxes</strong>
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#income_source">
														<i class="orange ace-icon fa fa-lightbulb-o bigger-120"></i>
														<strong>Other Income</strong>
													</a>
												</li>
                                                <li>
													<a data-toggle="tab" href="#personal_info">
														<i class="orange ace-icon fa fa-user bigger-120"></i>
														<strong>Personal Info</strong>
													</a>
												</li>
                                                 <li>
													<a data-toggle="tab" href="#fill_itr">
														<i class="orange ace-icon fa fa-cloud-upload  bigger-120"></i>
														<strong>Fill ITR</strong>
													</a>
												</li>
											</ul>
                                          </div>
                                          <div class="tab-content no-border padding-24">
												<div id="form_16_part_a" class="tab-pane in active">
                                                	<div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                            <div class="profile-user-info profile-user-info-striped">
												<div class="profile-info-row">
													<div class="profile-info-name"> Name and Address of the Deductor </div>
													<div class="profile-info-value">
														<span><?php echo ucwords(str_replace("_"," ",str_replace("#","<br />",$formDataFromDB[0]['employer_address']))); ?></span>
													</div>
                                                    <div class="profile-info-name"> Name and Address of the Deductee </div>
													<div class="profile-info-value">
														<span><?php echo ucwords(str_replace("_"," ",str_replace("#"," <br />",trim($formDataFromDB[0]['employee_address'])))); ?></span>
													</div>
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> PAN of the Deductor </div>
													<div class="profile-info-value">
														<span><?php echo strtoupper(str_replace("_"," ",str_replace("#","<br />",trim($formDataFromDB[0]['deductor_pan'])))); ?></span>
													</div>
                                                    <div class="profile-info-name"> TAN of the deductor </div>
													<div class="profile-info-value">
														<span><?php echo strtoupper(str_replace("_"," ",str_replace("#","<br />",trim($formDataFromDB[0]['deductor_tan'])))); ?></span>
													</div>
												</div>                                                
                                                <div class="profile-info-row">
													<div class="profile-info-name"> PAN of the deductee </div>
													<div class="profile-info-value">
														<span id="calcAge"><?php echo strtoupper(str_replace("_"," ",str_replace("#","<br />",trim($formDataFromDB[0]['employee_pan'])))); ?></span>
													</div>
                                                    <div class="profile-info-name"> Assessment Year </div>
													<div class="profile-info-value">
														<span id="calcAge"><?php echo trim($formDataFromDB[0]['assessment_year']); ?></span>
													</div>
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> Period </div>
													<div class="profile-info-value">
														<span id="calcAge"><?php 
															preg_match('/from#(.*?)#/', $formDataFromDB[0]['period_with_employer'], $from);	
															if(count($from) == 0)
															{
																preg_match('/from#(.*)/', $formDataFromDB[0]['period_with_employer'], $from);	
																preg_match('/to#(.*?)#/', $formDataFromDB[0]['period_with_employer'], $to);
															}
															else
															{
																preg_match('/#to#(.*)/', $formDataFromDB[0]['period_with_employer'], $to);	
															}
															echo "From - ".strtoupper($from[1])."<br>To - ".strtoupper($to[1]); ?></span>
													</div>
                                                    <div class="profile-info-name"> <strong>Total TDS Deposited</strong> </div>
													<div class="profile-info-value">
														<span id="calcAge"><strong>INR <?php echo trim($formDataFromDB[0]['total_tds_deposited']); ?></strong></span>
													</div>
												</div>
											</div>
                                        	</div>                                            
                                        </div>
                                                </div>
                                                <div id="form_16_part_b" class="tab-pane in ">
                                                	<div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                            <div class="profile-user-info profile-user-info-striped">
												<div class="profile-info-row">
													<div class="profile-info-name"> Balance (1-2) </div>
													<div class="profile-info-value">
														<input type="text" name="bal_1_2" id="bal_1_2" value="<?php echo ucwords(str_replace("_"," ",str_replace("#","<br />",$formDataFromDB[0]['balance_1_2']))); ?>">
													</div>                                                       
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> <strong>Deduction</strong><br />(a)&nbsp;Entertainment Allowance </div>
													<div class="profile-info-value">
														<input type="text" name="bal_1_2" id="bal_1_2" value="<?php echo ucwords(str_replace("_"," ",str_replace("#"," <br />",trim($formDataFromDB[0]['ded_enter_allowance'])))); ?>">
													</div>                                               
													<div class="profile-info-name">  <strong>Deduction</strong><br />                                                 
                                                    (b)&nbsp;Tax on employemnet </div>
													<div class="profile-info-value">
														<input type="text" name="bal_1_2" id="bal_1_2" value="<?php echo ucwords(str_replace("_"," ",str_replace("#"," <br />",trim($formDataFromDB[0]['ded_tax_employment'])))); ?>">
													</div>												    
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> <strong>Deduction(80c)</strong><br />(a)&nbsp;PF </div>
													<div class="profile-info-value">
														<input type="text" name="bal_1_2" id="bal_1_2" value="<?php echo ucwords(str_replace("_"," ",str_replace("#"," <br />",trim($formDataFromDB[0]['ded_80c_pf'])))); ?>">
													</div>                                               
													<div class="profile-info-name">  <strong>Deduction(80c)</strong><br />(b)&nbsp;LIC </div>
													<div class="profile-info-value">
														<input type="text" name="bal_1_2" id="bal_1_2" value="<?php echo ucwords(str_replace("_"," ",str_replace("#"," <br />",trim($formDataFromDB[0]['ded_80c_lic'])))); ?>">
													</div>
												    
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> <strong>Deduction(80c)</strong><br />(c)&nbsp;Tution Fee </div>
													<div class="profile-info-value">
														<input type="text" name="bal_1_2" id="bal_1_2" value="<?php echo ucwords(str_replace("_"," ",str_replace("#"," <br />",trim($formDataFromDB[0]['ded_80c_tution_fee'])))); ?>">
													</div>                                               
													<div class="profile-info-name">  <strong>Deduction(80c)</strong><br />(d)&nbsp;Home Loan </div>
													<div class="profile-info-value">
														<input type="text" name="bal_1_2" id="bal_1_2" value="<?php echo ucwords(str_replace("_"," ",str_replace("#"," <br />",trim($formDataFromDB[0]['ded_80c_home_loan'])))); ?>">
													</div>												    
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> <strong>Deduction(80c)</strong><br />(c)&nbsp;Other </div>
													<div class="profile-info-value">
														<input type="text" name="other_80c_1" id="other_80c_1" value="<?php echo ucwords(str_replace("_"," ",str_replace("#"," <br />",trim($formDataFromDB[0]['80c_other1'])))); ?>">
													</div>                                               
													<div class="profile-info-name">  <strong>Deduction(80c)</strong><br />(d)&nbsp;Other</div>
													<div class="profile-info-value">
														<input type="text" name="other_80c_2" id="other_80c_2" value="<?php echo ucwords(str_replace("_"," ",str_replace("#"," <br />",trim($formDataFromDB[0]['80c_other2'])))); ?>">
													</div>												    
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> <strong>Total 80c (a+b+c+d)</strong></div>
													<div class="profile-info-value"><div class="space-4"></div>
														<span><strong>INR <?php echo (str_replace("rs ","",str_replace("rs_","",$formDataFromDB[0]['ded_80c_pf']))+
																				 str_replace("rs ","",str_replace("rs_","",$formDataFromDB[0]['ded_80c_lic']))+
																				 str_replace("rs ","",str_replace("rs_","",$formDataFromDB[0]['ded_80c_tution_fee']))+
																				 str_replace("rs ","",str_replace("rs_","",$formDataFromDB[0]['ded_80c_home_loan'])));?></strong></span>
													</div>   
                                                    <div class="profile-info-name"> <strong>Total 80c Qualifying</strong></div>
													<div class="profile-info-value"><div class="space-4"></div>
														<span><strong>INR 1,50,000.00</strong></span>
													</div>                                              													
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> <strong>Chapter VI-A</strong><br />(a)&nbsp;80D</div>
													<div class="profile-info-value">
														<input type="text" name="bal_1_2" id="bal_1_2" value="<?php echo ucwords(str_replace("_"," ",str_replace("#"," <br />",trim($formDataFromDB[0]['ded_80d'])))); ?>">
													</div>                                               
													<div class="profile-info-name">  <strong>Chapter VI-A</strong><br />(b)&nbsp;80G </div>
													<div class="profile-info-value"><div class="space-4"></div>
														<input type="text" name="bal_1_2" id="bal_1_2" value="<?php echo ucwords(str_replace("-_donations_50%_exemptrs_","",str_replace("#"," <br />",trim($formDataFromDB[0]['ded_80g'])))); ?>">
													</div>												    
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> <strong>Chapter VI-A</strong><br />(c)&nbsp;80CCG </div>
													<div class="profile-info-value">
														<input type="text" name="bal_1_2" id="bal_1_2" value="<?php echo ucwords(str_replace("_"," ",str_replace("#"," <br />",trim($formDataFromDB[0]['ded_80ccg'])))); ?>">
													</div>                                               
													<div class="profile-info-name">  <strong>Chapter VI-A Qualifying</strong></div>
													<div class="profile-info-value"><div class="space-4"></div>
														<input type="text" name="bal_1_2" id="bal_1_2" value="<?php echo ucwords(str_replace(":rs_","",str_replace("#"," <br />",trim($formDataFromDB[0]['ded_sec_iva'])))); ?>">
													</div>												    
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> <strong>Deductible Amount under Chapter VI-A & 80C</strong></div>
													<div class="profile-info-value"><div class="space-4"></div>
														<span><strong><?php echo str_replace(":rs_","",trim($formDataFromDB[0]['ded_sec_iva']))+150000; ?></strong></span>
													</div>																							    
												</div>
											</div>
                                        	</div>
                                            
                                        </div>
                                                </div>
                                                <div id="deduction" class="tab-pane in ">
                                                </div>
                                                <div id="paid_tax" class="tab-pane in ">
                                                <button type="file"><strong>Upload Form 26AS</strong></button>
                                                </div>                                                
                                                <div id="income_source" class="tab-pane in ">
                                                </div>
                                                <div id="personal_info" class="tab-pane in ">
                                                <div class="profile-user-info profile-user-info-striped">
												<div class="profile-info-row">
													<div class="profile-info-name"> Father's Name </div>
													<div class="profile-info-value">
														<input type="text" id="fath_name" value="<?php echo trim($profileInfo[father_name]); ?>" />
													</div>
												</div>
                                                <div class="profile-info-row">
													<div class="profile-info-name"> Date Of Birth </div>
													<div class="profile-info-value">
														<input type="text" id="dob" value="<?php echo trim($profileInfo[dob]); ?>" />
													</div>
												</div>                                                                                               
                                                <div class="profile-info-row">
													<div class="profile-info-name"> Gender </div>
													<div class="profile-info-value">
														<span>
														<div class="radio">
													<label>
														<input type="radio" class="ace" name="form-gender-radio" onchange="ajaxCallKeyValue('sex',this.value);" value="Male"  <?php if($profileInfo[sex] == "Male") echo "checked"; ?>>
														<span class="lbl"> Male</span>
													</label>
                                                    <label>
														<input type="radio" class="ace" name="form-gender-radio" onchange="ajaxCallKeyValue('sex',this.value);" value="Female"  <?php if($profileInfo[sex] == "Female") echo "checked"; ?>>
														<span class="lbl"> Female</span>
													</label>                                                    
												</div>
													</span>
													</div>
												</div>                                                                                           																		
												<div class="profile-info-row">
													<div class="profile-info-name"> Address </div>
													<div class="profile-info-value">
                                                    	<input type="text" id="address1" value="<?php echo trim($profileInfo[address1]); ?>" />
                                                    </div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> City </div>
													<div class="profile-info-value">
                                                    <input type="text" id="city1" value="<?php echo trim($profileInfo[city]); ?>" />													
													</div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> State </div>
													<div class="profile-info-value">
                                                    	<input type="text" id="state" value="<?php echo trim($profileInfo[state]); ?>" />															
													</div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> Zip </div>
													<div class="profile-info-value">
                                                    	<input type="text" id="pincode" value="<?php echo trim($profileInfo[pincode]); ?>" />																
													</div>
												</div>
												<div class="profile-info-row">
													<div class="profile-info-name"> Country </div>
													<div class="profile-info-value">
                                                    	<input type="text" id="country" value="<?php echo trim($profileInfo[country]); ?>" />														
													</div>
												</div>
                                                 <div class="profile-info-row">
													<div class="profile-info-name"> PAN </div>
													<div class="profile-info-value">
                                                    	<input type="text" id="pan" value="<?php echo trim($profileInfo[pan_number]); ?>" />														
													</div>
												</div>  
                                                 <div class="profile-info-row">
													<div class="profile-info-name"> Aadhaar No. </div>
													<div class="profile-info-value">
                                                    	<input type="text" id="aadhar" value="<?php echo trim($profileInfo[aadhaar_no]); ?>" />
													</div>
												</div>  
											</div>
                                                </div>
                                                <div id="fill_itr" class="tab-pane in ">
                                                </div>
                                          </div>
                                        </div>                                                                               
                                        <div class="space-20"></div>
                                    </div>																
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div>