<script>var QUERY_STRING = '<?php echo $_SERVER['QUERY_STRING']; ?>';</script>
<!-- basic scripts -->
		<!--[if !IE]> -->
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/jquery/dist/jquery.min.js"></script>
		<!-- <![endif]-->
		<!--[if IE]>
<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/jquery.1x/dist/jquery.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/_mod/jquery.mobile.custom/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/bootstrap/dist/js/bootstrap.min.js"></script>
		<!--[if lte IE 8]>
		  <script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/ExplorerCanvas/excanvas.min.js"></script>
		<![endif]-->
        <?php //if($CONFIG->pageName != "itr_forms") { ?>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/_mod/jquery-ui.custom/jquery-ui.custom.min.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/jqueryui-touch-punch/jquery.ui.touch-punch.min.js"></script>
         <script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/jquery-ui.js"></script>
         <script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/jquery-ui.custom.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/jquery.ui.touch-punch.js"></script>
        <script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/bootbox.js/bootbox.min.js"></script>
        
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/Flot/jquery.flot.min.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/Flot/jquery.flot.pie.min.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/Flot/jquery.flot.resize.min.js"></script>		
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/src/elements.scroller.js"></script>	
        <?php //} ?>	
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/src/ace.js"></script>
        <script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/src/elements.fileinput.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/src/ace.basics.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/src/ace.scrolltop.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/src/ace.ajax-content.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/src/ace.sidebar.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/src/ace.sidebar-scroll-1.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/src/ace.widget-box.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/src/ace.settings.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/js/src/ace.settings-skin.js"></script>
       	<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/jquery-validation/dist/jquery.validate.min.js"></script>
        <?php if($CONFIG->pageName == "profile" || $CONFIG->pageName == "search") { ?>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/select2/dist/js/select2.min.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/_mod/x-editable/bootstrap-editable.min.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/_mod/x-editable/ace-editable.min.js"></script>
		<script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/postloginAssets/components/jquery.gritter/js/jquery.gritter.min.js"></script>                        
		<?php } ?>
		<?php if($_SESSION[$CONFIG->sessionPrefix.'page_name'] == "itr_forms") { ?>
        <script src="<?php echo $CONFIG->siteurl;?>__UI.assets/plupload/js/plupload.full.js"></script>
        <script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/js/itr_functions.js"></script>			
		 <?php } ?>       
        <script src="<?php echo $CONFIG->siteurl; ?>__UI.assets/js/js_function.php"></script>		
		<script type="text/javascript"> ace.vars['base'] = '.'; </script>		