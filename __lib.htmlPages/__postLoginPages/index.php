<?php
	header("Access-Control-Allow-Origin: *");	
	include("../../__lib.includes/config.inc.php");			//$pagename = $_GET[page] || $_GET[module_interface];	
	$_SESSION['oPageAccess'] = 2;
	if(!($_SESSION[$CONFIG->sessionPrefix.'loginstatus'])) { header("HTTP/1.1 401 Unauthorized");header("Location: $CONFIG->siteurl");exit;}
	
	$pagename = $commonFunction->getPage($_GET[module_interface]);	
	 			
	if ($pagename == "") {
		$pagename = "home";
	}
	//print_r($_REQUEST);//exit;
	$CONFIG->pageName = $pagename;
	
	$profileInfo = $customerProfile->getCustomerInfo($CONFIG->loggedUserId); 
	if($profileInfo['profile_image'] == '')
		$loggedUserImage = $CONFIG->siteurl."__UI.assets/postloginAssets/avatars/avatar2.png";
	else
	 	$loggedUserImage = $CONFIG->customerProfileImgURL.$profileInfo['profile_image'];
	
	$page = $pagename.".php";
	if (strpos($pagename, 'modal') !== false) 
	{
    	include($page);
		exit;
	}
	$_SESSION[$CONFIG->sessionPrefix.'page_name'] = $pagename;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />		
        <?php include_once('inc.header_includes.php')?>	
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <title>Save Tax | Manage Wealth</title>
	</head>
	<body class="skin-2">
<?php
	include("inc.header.php");
?>	
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
				<?php 
				include("inc.left.php"); 
			
				include($page);

				include("inc.footer.php"); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<?php include("inc.footer.js.php"); ?>
	</body>
</html>