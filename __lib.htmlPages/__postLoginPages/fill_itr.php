<?php
	//echo "<pre>";
	//print_r($_SESSION);
	//print_r($_REQUEST);
	//$CONFIG->customerId
	//$CONFIG->currentAY = $_REQUEST[ay]."-".(1+$_REQUEST[ay]);
	$documentFiles->trfrFrm16DataToMainDB($_SESSION[$CONFIG->sessionPrefix.'_ITR_ID']);
	$ayYears = explode('-',$CONFIG->currentAY);
?>
<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo $CONFIG->siteurl;?>mySaveTax">Home</a>
							</li>
							<li class="active">Fill ITR</li><li class="active">Assessment Year</li>
						</ul><!-- /.breadcrumb -->
						<?php include("form.search.php");?>
						<!-- /section:basics/content.searchbox -->
					</div>
					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">												
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="widget-box transparent">
                                    <div class="widget-header widget-header-flat">
                                        <h4 class="widget-title orange">
                                            <i class="ace-icon fa fa-file-o green"></i>
                                            Assessment Year
                                        </h4>                                                     
                                    </div>
                                </div><!-- /.widget-box -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-xs-12">
                            	<div class="space-8"></div>
                                    <div class="widget-box">
                                        <div class="widget-header widget-header-flat">
                                            <h4 class="widget-title">Choose Assessment Year</h4>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">  
<form method="post" action="?module_interface=<?php echo $commonFunction->setPage('itr_forms');?>">
                                                <div class="row">
                                                    <div id="task-tab" class="tab-pane active col-xs-9">
                                                        <ul id="tasks" class="item-list">
                                                            <li class="item-orange clearfix">
<div class="body">
	<div class="name">
		<input type="hidden" name="new" class="ace" checked value="1" />
    	<input type="radio" name="ay" class="ace" checked value="<?php echo $ayYears[0] ;?>" />
        <span class="lbl"> <strong>A.Y. <?php echo $CONFIG->currentAY;?></strong></span></a>
	</div>

	<div class="text">
    	<span class="red"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The <strong>Financial Year (F.Y.)</strong> is <span class="green">
		<?php echo ($ayYears[0]-1)."-".$ayYears[0];?></span>
         , which starts from <strong class="green">01/04/<?php echo ($ayYears[0]-1);?></strong> and ends on 
         <strong class="green">31/03/<?php echo $ayYears[0];?></strong></b></span>
    </div>
</div>
														    </li>
								
															</ul>
														</div>
													</div>
                                <div class="row">
                                <div class="pull-right">
                                    <button id="loading-btn" type="submit" class="btn btn-success" data-loading-text="Loading...">&nbsp;&nbsp;Save & Countinue&nbsp;&nbsp;</button>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                                </div>
</form>
												</div>
											</div>
									</div>
                            </div>
                      </div>
					</div>
				</div>
			</div>